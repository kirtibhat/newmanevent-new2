<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * @description: This controller is used to manage login, registration
 * @create date: 21-Nov-2013
 * @auther: Lokendra Meena
 * @email: lokendrameena@cdnsol.com
 * 
 */ 

class Admin extends MX_Controller{

	function __construct(){
		parent::__construct();
		$this->load->library('factory',$this);
		$this->load->model(array('admin_model'));
		$this->load->language(array('admin'));
		$this->form_validation->set_error_delimiters('<div class="server_msg_show">', '</div>');
		$this->session_check->checkSession(); 		
	}

	
	//----------------------------------------------------------------------------
	
    /*
     * @default load 
     * @description: This function is used to manage regsitrant listing
     * @load: login view
     * @return: void
     *  
     */
    
    public function registrantmanager(){
		 $this->factory->registrantmanager();
	}
	
	//----------------------------------------------------------------------------
	
    /*
     * @access: public
     * @description: This function is used to delete event register user data
     * @return: void
     *  
     */
	
	public function deleteeventuser(){
		//call factory method for delete event user data
		$this->factory->deleteeventuser();
	}


	
	//----------------------------------------------------------------------------
	
    /*
     * @default load 
     * @description: This functiona is used to payment transfer and show summary
     * @load: login view
     * @return: void
     */
    
    public function paymentstransfers(){
		
		 $this->factory->paymentstransfers();
		
	}

	//----------------------------------------------------------------------------
	
    /*
     * @default load 
     * @description: This function is used corporate request for sponsor and exhibitor
     * @load: login view
     * @return: void
     *  
     */
    
    public function corporaterequests(){
		
		 $this->factory->corporaterequests();
	}
	
    //----------------------------------------------------------------------------
	
   /*
    * @access: public
    * @description: This function is used to change sponsor status
    * @return: void
    *  
    */
	
	public function changecorporatetatus(){
		//call factory method for to change sponsor status
		$this->factory->changecorporatetatus();
	}
	
	// ------------------------------------------------------------------------ 
	
	/*
	 * @description: This function is used to set custome error without any field on sever side
	 * @return false
	 */
	
	public function custom_error_set($str){
		$this->form_validation->set_message('custom_error_set', '%s');
		return FALSE;
	}
	
	
	
   
}

/* End of file admin.php */
/* Location: ./system/application/modules/admin/controllers/admin.php */
