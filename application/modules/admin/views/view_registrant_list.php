<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
	
<div class="row-fluid">
	<div class="eventcontainerbg pb0">
		<div class="headingbg">
			<div class="currenteventH bg_none pl0">
				<?php echo lang('admin_full_registrant_list'); ?>
			</div>
		</div>
		<div class="panel-body">
			<div class="TablecontentHolder tabledatascrolling">
				<div class="content">
					<table class="admintable tablecuston_scroll" cellpadding="0" cellspacing="0" border="0">
					<tbody>
					<?php if(!empty($registrantlistdata)){  ?>
						<thead>
						<tr>
							<th>
								<i class="icon-sort"></i>
								<a href="javascript:void(0)" class="clr_white change_order" id="registration_booking_number" orderby="asc"><?php echo lang('admin_regis_booking'); ?></a>
							</th>
							<th>
								<?php echo lang('admin_regis_invoice'); ?>
							</th>
							<th>
								<i class="icon-sort-up"></i>
								<a href="javascript:void(0)" class="clr_white change_order" id="name_email" orderby="asc">
									<?php echo lang('admin_regis_name_email'); ?>
								</a>
							</th>
							<th>
								<i class="icon-sort"></i>
								<a href="javascript:void(0)" class="clr_white change_order" id="registrant_id" orderby="asc">
									<?php echo lang('admin_regis_rego_type'); ?>
								</a>	
							</th>
							<th>
								<i class="icon-sort"></i>
								<a href="javascript:void(0)" class="clr_white change_order" id="registrant_total_price" orderby="asc">
									<?php echo lang('admin_regis_total'); ?>
								</a>
							</th>
							<th>
								<i class="icon-sort-up"></i>
								<a href="javascript:void(0)" class="clr_white change_order" id="payment_status" orderby="asc">
									<?php echo lang('admin_regis_payment_status'); ?>
								</a>	
							</th>
						
							<th>
								&nbsp;
							</th>
						</tr>
						</thead>
						<?php
							foreach($registrantlistdata as $registrantlist ){
						?>	
						<tr>
						<td>
								<?php echo $registrantlist->registration_booking_number; ?>
						</td>
						
						<td>
							<a href="#" class="greytooltip_del greytooltip_dollar_home"><i class="icon-big-eye show_tool_tip" title="<?php echo lang('comm_preview_msg'); ?>"></i>&nbsp;</a>
						</td>
						
						<td>
							
							<div>
								<?php 
									$userFirstName = 'None';
									$userEmail = 'None';
									$getRegistrantFields =  getRegistrantFields($registrantlist->id);
									
									if(!empty($getRegistrantFields)){
										$userFirstName = $getRegistrantFields[1];
										$userEmail = $getRegistrantFields[3];
									}
									echo $userFirstName;
								?>
								
							</div>
							<div>
								<?php echo $userEmail; ?>
							</div>
						</td>
						
						<td>
							<?php echo $registrantlist->registrant_name; ?>
						</td>
						<td>
							<?php echo number_format($registrantlist->total_amount,2); ?>
						</td>
						
						<td>
							<?php 	
								if($registrantlist->payment_status==0){
									//unpaid condition
									echo lang('admin_regis_unpaid');
								}elseif($registrantlist->payment_status==1){
									//paid condition
									echo lang('admin_regis_paid');
								}else{
									//Cancelled condition	
									echo lang('admin_regis_cancelled');
								} 
							?>
						</td>
						
						<td>
							
							  
							<a href="#" class="greytooltip_del greytooltip_dollar_home"><span class="btn_icon dollar_btn_icon show_tool_tip" title="<?php echo lang('comm_preview_msg'); ?>" id="example5">&nbsp;</span></a>
							<a href="#" class="greytooltip_home greytooltip_edit_home"><span class="btn_icon edit_btn_icon show_tool_tip"  title="<?php echo lang('comm_preview_msg'); ?>" id="example2">&nbsp;</span></a> 
							<a href="#" class="greytooltip_del greytooltip_file_home"><span class="btn_icon delet_btn_icon cross_btn ml_0 show_tool_tip"  title="<?php echo lang('comm_preview_msg'); ?>" id="example4">&nbsp;</span></a>

							
							<!----
							<span class="greentooltip">
								<a href="#" class="show_tool_tip" title="Edit"><img src="<?php echo base_url('templates/system/images'); ?>/edit_icon.png"></a>
							</span>
							<span class="greentooltip_del">
								<a href="javascript:void(0)"  class="show_tool_tip delete_register_user"  deleteid="<?php echo $registrantlist->id; ?>" title="Delete"><img src="<?php echo base_url('templates/system/images'); ?>/delet_icon.png"></a>
							</span>
							
							--->
						</td>
					</tr>
					
					<?php }  }else{ ?>
						
						<thead>
							<tr>
								<th colspan="9">
									<?php echo lang('admin_regis_no_registant'); ?>
								</th>
							</tr>
						</thead>
					<?php	
					} ?>
					</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="spacer10"></div>
<?php
	if($recordsCount > 0){
		echo $paginationShow;
	}
 ?>
