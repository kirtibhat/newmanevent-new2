<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<div class="spacer15">
</div>
<div class="row-fluid">
	<div class="headingh1">
		<?php echo lang('admin_corportate_request'); ?>
	</div>
	<div class="spacer40">
	</div>
</div>

<div class="spacer15">
</div>
<div class="loadviewsection">
	<?php echo $this->load->view('view_corporate_list'); ?>
</div>

<script type="text/javascript">

	//record pagination
	recordpagination('admin/corporaterequests');

	//delete sponsor request
	customconfirm('delete_sponsor','admin/deletesponsorrequest')

	//change booth status
	$(document).on('click','.booth_status',function(){
		var sponsorid 	= $(this).attr('sponsorid');
		var boothstatus = parseInt($(this).val());
		
		if(boothstatus==1){
			//condition for confirm button
			$('#approved'+sponsorid).attr('disabled',true);
			$('#unapproved'+sponsorid).attr('disabled',false);
		}else{
			//condition for cancel button
			$('#unapproved'+sponsorid).attr('disabled',true);
			$('#approved'+sponsorid).attr('disabled',false);
		}
		
		var url = baseUrl+'admin/changesponsorstatus';
		var fromData = {'sponsorid':sponsorid,'boothstatus':boothstatus };
		$.ajax({
		  type:'POST',
		  data:fromData,
		  url: url,
		  dataType: 'json',
		  cache: false,
		  beforeSend: function( ) {
				//open loader
				loaderopen();
			},
		  success: function(data){	
				//check data 
				if(data){
					loaderclose();
					var status =  (boothstatus==1)?'Approved':'Unapproved';
					$('#booth_status_'+sponsorid).html(status);
				}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				//hide loader
				loaderclose();
				custom_popup('Request failed.',false);
			}
		});
	});
	

</script>



