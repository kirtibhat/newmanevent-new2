<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
	
<div class="row-fluid">
	<div class="eventcontainerbg">
		<div class="headingbg">
			<div class="currenteventH bg_none pl0">
				<?php echo lang('admin_full_list_exhibitor'); ?>
			</div>
		</div>
		<div class="panel-body">
			<div class="TablecontentHolder tabledatascrolling">
				<div class="content">
					<table class="admintable tablecuston_scroll" cellpadding="0" cellspacing="0" border="0">
					<thead>
					<tr>
						<th>
							<?php echo lang('admin_exhib_booking_history'); ?>
						</th>
						<th>
							<?php echo lang('admin_exhib_event_name'); ?>
						</th>
						<th>
							<?php echo lang('admin_exhib_sponsor_name'); ?>
						</th>
						<th>
							<?php echo lang('admin_exhib_name_email'); ?>
						</th>
						<th>
							<?php echo lang('admin_exhib_booth_position'); ?>
						</th>
						<th>
							<?php echo lang('admin_exhib_status'); ?>
						</th>
						<th>
							<?php echo lang('admin_exhib_status_change'); ?>
						</th>
						<th>
							<?php echo lang('admin_exhib_modify_delete'); ?>
						</th>
					</tr>
					</thead>
					<tbody>
					<?php if(!empty($exhibitorlistdata)){ 
						foreach($exhibitorlistdata as $exhibitorlist ){
					?>	
						<tr>
						<td>
							<?php echo $exhibitorlist->id; ?>
						</td>
				
						<td>
							<?php echo $exhibitorlist->event_title; ?>
						</td>
						
						<td>
							<?php echo $exhibitorlist->exhibitor_name; ?>
						</td>
						
						<td>
							<div>
								<?php echo $exhibitorlist->firstname; ?>
							</div>
							<div>
								<?php echo $exhibitorlist->email; ?>
							</div>
						</td>
						<td>
							<?php echo $exhibitorlist->booth_position; ?>
						</td>
						
						<td>
							<span id="booth_status_<?php echo $exhibitorlist->id; ?>">
							<?php 
								switch($exhibitorlist->booth_status){
									case '1':
									echo 'Approved';	
									break;
									case '2':
									echo 'Unapproved';	
									break;
									default:
									echo 'Pending';
								}	
							?>
							</span>
						</td>
						<td>
							
							<div class="checkradiobg">
								<div class="radio admin">
                                <input <?php echo ($exhibitorlist->booth_status=='1')?'checked disabled="true" ':''; ?>  id="approved<?php echo $exhibitorlist->id; ?>" exhibitorid='<?php echo $exhibitorlist->id; ?>' type="radio" name="booth_status<?php echo $exhibitorlist->id; ?>" value="1" class="radio booth_status">
                                <label for="approved<?php echo $exhibitorlist->id; ?>" class="width_105px"><?php echo lang('admin_exhib_approved'); ?></label>
                           		
                           		<div class="clearfix"></div>
                                <input <?php echo ($exhibitorlist->booth_status=='2')?'checked disabled="true" ':''; ?> id="unapproved<?php echo $exhibitorlist->id; ?>" exhibitorid='<?php echo $exhibitorlist->id; ?>' type="radio" name="booth_status<?php echo $exhibitorlist->id; ?>" value="2" class="booth_status">
                                <label for="unapproved<?php echo $exhibitorlist->id; ?>" class="width_105px"><?php echo lang('admin_exhib_unapproved'); ?></label>
                               </div>
                            </div>
                            
                    	</td>
						<td>
							<span class="greentooltip_del">
								<a href="javascript:void(0)"  class="show_tool_tip delete_exhibitor"  deleteid="<?php echo $exhibitorlist->id; ?>" title="Delete"><img src="<?php echo base_url('templates/system/images'); ?>/delet_icon.png"></a>
							</span>
						</td>
					</tr>
					
					<?php }  } ?>
					</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>


<div class="spacer10"></div>
<?php
	if($recordsCount > 0){
		echo $paginationShow;
	}
 ?>
