<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//search form 
$formDelegateSearch = array(
		'name'	 => 'formDelegateSearch',
		'id'	 => 'formDelegateSearch',
		'method' => 'post',
		'class'  => 'form-horizontal mt10',
	);
	
$searchField = array(
		'name'	=> 'searchField',
		'value'	=> '',
		'id'	=> 'searchField',
		'placeholder'	=> 'Search',
		'type'	=> 'text',
		'class'	=> 'adminsearch',
	);
$searchedData = array(
		'name'	=> 'searchedData',
		'value'	=> '0',
		'id'	=> 'searchedData',
		'type'	=> 'hidden',
	);	
?>
<div class="spacer40">
</div>
<div class="row-fluid">
	<div class="headingh1">
		<?php echo lang('admin_registrant_manager'); ?>
	</div>
	<div class="spacer40">
	</div>
	
</div>

<div class="row-fluid">
	
	<?php  

		echo form_open(base_url('report'),$formDelegateSearch); 

		echo form_input($searchField); 
		echo form_input($searchedData); 
		
		$extraSave 	= 'class="adminsubmit" ';
		echo form_submit('save','',$extraSave);

		echo form_close(); 
	?>
	
</div>
<div class="spacer40">
</div>
<div class="loadviewsection">
	<?php echo $this->load->view('view_registrant_list'); ?>
</div>

<script type="text/javascript">
	
	recordpagination('admin/registrantmanager','formDelegateSearch');

	//record search 
	searchrecord('formDelegateSearch','admin/registrantmanager');
	
	//delete register user record
	customconfirm('delete_register_user','admin/deleteeventuser');
	
		
	$(document).on('click','.change_order',function(){	
		
		//get colomn order action
		var orderingAction = $(this).attr('id');
		var orderBy = $(this).attr('orderby');
		
		if($(this).prev().hasClass("icon-sort")){
			$(this).prev().removeClass("icon-sort").addClass("icon-sort-up");
			$(this).attr('orderby','asc');
		}else{
			$(this).prev().removeClass("icon-sort-up").addClass("icon-sort");
			$(this).attr('orderby','desc');
		}
		
			var fromData={"columnName":orderingAction,"orderBy":orderBy};
			var postUrl = 'admin/registrantmanager';
			var url = baseUrl+postUrl;
				$.ajax({
					type:'POST',
					data:fromData,
					url: url,
					cache: false,
					beforeSend: function( ) {
						//open loader
						loaderopen();
					},
					success: function(data){	
						//check data 
						if(data){
							loaderclose();
							$(".loadviewsection").html(data);
							//set one if user searched 
							$("#searchedData").val('1');
						}
						//call tooltip
						$('.show_tool_tip').tooltip();
						
						//scroll 
						$('.tabledatascrolling').perfectScrollbar({wheelSpeed:100});
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) {
						//hide loader
						loaderclose();
						custom_popup('Request failed.',false);
					}
				});
				
			return false;
		
	});
</script>
