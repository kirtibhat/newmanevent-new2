<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

	
?>

	<div class="row-fluid">
		<div class="eventcontainerbg current_event_list ">
			<div class="headingbg">
				<div class="currenteventH ptr currenteventheader">
					<?php echo lang('admin_corporate_pending'); ?>
				</div>
				
			</div>
			<div id="show_event_list">
				<div class="panel-body">
					<div class="TablecontentHolder tabledatascrolling">
						<div class="content">
							<table class="admintable tablecuston_scroll" cellpadding="0" cellspacing="0" border="0">
					<thead>
					<tr>
						<th>
							<?php echo lang('admin_corporate_organisa'); ?>
						</th>
						<th>
							<?php echo lang('admin_corporate_contact'); ?>
						</th>
						<th>
							<?php echo lang('admin_corporate_email'); ?>
						</th>
						<th>
							<?php echo lang('admin_corporate_request'); ?>
						</th>
						<th>
							&nbsp;
						</th>
					</tr>
					</thead>
						<tbody>
					<?php if(!empty($corporaterequestdata)){ 
						foreach($corporaterequestdata as $corporaterequest ){
					?>	
						<tr>
						<td>
							<?php echo $corporaterequest->event_title; ?>
						</td>
				
						<td>
							<?php echo $corporaterequest->name; ?>
						</td>
						
						<td>
							<?php echo $corporaterequest->email; ?>
						</td>
						
					
						<td>
							<?php 
								//show request by condition
								if(!empty($corporaterequest->sponsor_id)){
									echo $corporaterequest->sponsor_name; 
								}elseif(!empty($corporaterequest->exhibitor_id)){
									echo $corporaterequest->exhibitor_name; 
								}
							?>
						</td>
						
						<td>
							<!---
								<span class="greentooltip_del">
									<a href="javascript:void(0)"  class="show_tool_tip delete_sponsor"  deleteid="<?php //echo $corporaterequest->id; ?>" title="Delete"><img src="<?php echo base_url('templates/system/images'); ?>/delet_icon.png"></a>
								</span>
							--->

							<a href="javascript:void(0)" class="greytooltip_del greytooltip_dollar_home" id ><i class="icon-big-eye show_tool_tip" title="<?php echo lang('comm_preview_msg'); ?>"></i>&nbsp;</a>
							<a href="javascript:void(0)" id="<?php echo $corporaterequest->id; ?>" status="1" class="greytooltip_home greytooltip_edit_home booth_status"><span class="btn_icon edit_btn_icon show_tool_tip"  title="<?php echo lang('comm_preview_msg'); ?>" id="example2">&nbsp;</span></a> 
							<a href="javascript:void(0)" id="<?php echo $corporaterequest->id; ?>" status="2" class="greytooltip_del greytooltip_file_home booth_status"><span class="btn_icon delet_btn_icon cross_btn ml_0 show_tool_tip"  title="<?php echo lang('comm_preview_msg'); ?>" id="example4">&nbsp;</span></a>
						
						</td>
					</tr>
					
					<?php }  }else{ ?>
							
							<tr>
							<td colspan="5">
								<h3><?php echo lang('admin_no_corporate_request'); ?></h3>
							</td>
						</tr>
						
					<?php } ?>
					
					</tbody>
				
						
					</table>
						</div>
				</div>
			</div>
			</div>
		</div>
	</div>
	
	<div class="spacer40"> </div>

	
	<div class="row-fluid">
	<div class="eventcontainerbg bg_dee6e7 past_event_list eventcontainerbg_radius">
		<div class="headingbg">
			<div class="past_event ptr pasteventheader">
				<?php echo lang('admin_corporate_past'); ?>
			</div>
			<!---
			<div class="addanother_event">
				<a data-toggle="modal" data-target="#myModal">Add another Event <img src="images/addanotherevent.png"></a>
			</div>--->
		</div>
		<div class="panel-body dn" id="past_event_list">
			<div class="TablecontentHolder tabledatascrolling">
				<div class="content">
					<table class="admintable tablecuston_scroll" cellpadding="0" cellspacing="0" border="0">
				<thead>
					<tr>
						<th>
							<?php echo lang('admin_corporate_organisa'); ?>
						</th>
						<th>
							<?php echo lang('admin_corporate_contact'); ?>
						</th>
						<th>
							<?php echo lang('admin_corporate_email'); ?>
						</th>
						<th>
							<?php echo lang('admin_corporate_request'); ?>
						</th>
						<th>
							&nbsp;
						</th>
					</tr>
				</thead>
					<tbody>
					<?php if(!empty($corporaterequestdata)){ 
						foreach($corporaterequestdata as $corporaterequest ){
					?>	
						<tr>
						<td>
							<?php echo $corporaterequest->event_title; ?>
						</td>
				
						<td>
							<?php echo $corporaterequest->name; ?>
						</td>
						
						<td>
							<?php echo $corporaterequest->email; ?>
						</td>
						
					
						<td>
							<?php 
								//show request by condition
								if(!empty($corporaterequest->sponsor_id)){
									echo $corporaterequest->sponsor_name; 
								}elseif(!empty($corporaterequest->exhibitor_id)){
									echo $corporaterequest->exhibitor_name; 
								}
							?>
						</td>
						
						<td>
							<!---
								<span class="greentooltip_del">
									<a href="javascript:void(0)"  class="show_tool_tip delete_sponsor"  deleteid="<?php //echo $corporaterequest->id; ?>" title="Delete"><img src="<?php echo base_url('templates/system/images'); ?>/delet_icon.png"></a>
								</span>
							--->

							<a href="javascript:void(0)" class="greytooltip_del greytooltip_dollar_home"><i class="icon-big-eye show_tool_tip" title="<?php echo lang('comm_preview_msg'); ?>"></i>&nbsp;</a>
							<a href="javascript:void(0)" class="greytooltip_home greytooltip_edit_home "><span class="btn_icon edit_btn_icon show_tool_tip"  title="<?php echo lang('comm_preview_msg'); ?>" id="example2">&nbsp;</span></a> 
							<a href="javascript:void(0)" class="greytooltip_del greytooltip_file_home "><span class="btn_icon delet_btn_icon cross_btn ml_0 show_tool_tip"  title="<?php echo lang('comm_preview_msg'); ?>" id="example4">&nbsp;</span></a>

						
							
						
						</td>
					</tr>
					
					<?php }  }else{ ?>
							
							<tr>
							<td colspan="5">
								<h3><?php echo lang('admin_no_corporate_request'); ?></h3>
							</td>
						</tr>
						
					<?php } ?>	
					</tbody>
				
				</table>
				</div>
			</div>
		</div>
	</div>
</div>
