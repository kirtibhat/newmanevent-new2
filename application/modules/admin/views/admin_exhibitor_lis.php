<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<div class="spacer40">
</div>
<div class="row-fluid">
	<div class="headingh1">
		<?php echo lang('admin_admin'); ?>
	</div>
	<div class="spacer40">
	</div>
	<div class="commpnpara">
		<?php echo lang('admin_regis_list_title'); ?>
	</div>
</div>
<div class="spacer15">
</div>

<div class="spacer15">
</div>
<div class="loadviewsection">
	<?php echo $this->load->view('view_exhibitor_list'); ?>
</div>


<script type="text/javascript">

	//record pagination
	recordpagination('admin/exhibitorrequest');

	//delete exhibitor request
	customconfirm('delete_exhibitor','admin/deleteexhibitorrequest')

	//change booth status
	$(document).on('click','.booth_status',function(){
		var exhibitorid = $(this).attr('exhibitorid');
		var boothstatus = parseInt($(this).val());
		
		if(boothstatus==1){
			//condition for confirm button
			$('#approved'+exhibitorid).attr('disabled',true);
			$('#unapproved'+exhibitorid).attr('disabled',false);
		}else{
			//condition for cancel button
			$('#unapproved'+exhibitorid).attr('disabled',true);
			$('#approved'+exhibitorid).attr('disabled',false);
		}
		
		var url = baseUrl+'admin/changeexhibitorstatus';
		var fromData = {'exhibitorid':exhibitorid,'boothstatus':boothstatus };
		$.ajax({
		  type:'POST',
		  data:fromData,
		  url: url,
		  dataType: 'json',
		  cache: false,
		  beforeSend: function( ) {
				//open loader
				loaderopen();
			},
		  success: function(data){	
				//check data 
				if(data){
					loaderclose();
					var status =  (boothstatus==1)?'Approved':'Unapproved';
					$('#booth_status_'+exhibitorid).html(status);
				}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				//hide loader
				loaderclose();
				custom_popup('Request failed.',false);
			}
		});
	});
	

</script>



