<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


$formPaymentFundTrans = array(
		'name'	 => 'formPaymentFundTrans',
		'id'	 => 'formPaymentFundTrans',
		'method' => 'post',
		'class'  => 'form-horizontal',
	);
	
$paymentPaidTo = array(
		'name'	=> 'paymentPaidTo',
		'value'	=> '',
		'id'	=> 'paymentPaidTo',
		'type'	=> 'text',
		'required'	=> '',
	);	

$paymentContactName = array(
		'name'	=> 'paymentContactName',
		'value'	=> '',
		'id'	=> 'paymentContactName',
		'type'	=> 'text',
		'required'	=> '',
	);	
	
$paymentDescription = array(
		'name'	=> 'paymentDescription',
		'value'	=> '',
		'id'	=> 'paymentDescription',
		'type'	=> 'text',
		'required'	=> '',
	);	

$paymentDate	= array(	
		'name'	=> 'paymentDate',
		'value'	=> '',
		'id'	=> 'paymentDate',
		'type'	=> 'text',
		'class'	=> 'width50per datetimepicker',
		'readonly'	=> true,
	);		
	
$paymentAmount	= array(	
		'name'	=> 'paymentAmount',
		'value'	=> '',
		'id'	=> 'paymentAmount',
		'type'	=> 'text',
		'class'	=> 'width50per unitPriceEnter',
	);		
	
$paymentGSTInclude = array(
		'name'	=> 'paymentGSTInclude',
		'value'	=> '',
		'id'	=> 'paymentGSTInclude',
		'type'	=> 'text',
		'class'	=> 'width50per',
		'required'	=> '',
	);

$paymentTotalAmount = array(
		'name'	=> 'paymentTotalAmount',
		'value'	=> '',
		'id'	=> 'paymentTotalAmount',
		'class'	=> 'width50per',
		'type'	=> 'text',
		'required'	=> '',
	);	

$paymentAuthorisedBy = array(
		'name'	=> 'paymentAuthorisedBy',
		'value'	=> '',
		'id'	=> 'paymentAuthorisedBy',
		'class'	=> 'width50per',
		'type'	=> 'text',
		'required'	=> '',
	);	

	
						
?>

<div class="commonform_bg">
	<div class="headingbg bg_F89728 brd_botm_0px">
		<?php echo lang('admin_payments_fund_transfers');?>
	</div>
	<?php echo form_open($this->uri->uri_string(),$formPaymentFundTrans); ?>
		
		<div class="form-horizontal">
		<div class="control-group mb10">
			
			<div class="control-group mb10">
				<label class="control-label" for="inputEmail">
					<?php echo lang('admin_payments_paid_to');?>
					<font class="mandatory_cls">*</font>
				</label>
				<div class="controls">
					<?php echo form_input($paymentPaidTo); ?>
					<?php echo form_error('paymentPaidTo'); ?>
				</div>
			</div>
			<div class="control-group mb10">
				<label class="control-label" for="inputEmail">
					<?php echo lang('admin_payments_contact_name');?>
					<font class="mandatory_cls">*</font>
				</label>
				<div class="controls">
					<?php echo form_input($paymentContactName); ?>
					<?php echo form_error('paymentContactName'); ?>
				</div>
			</div>
			<div class="control-group mb10">
				<label class="control-label" for="inputEmail">
					<?php echo lang('admin_payments_description');?>
					<font class="mandatory_cls">*</font>
				</label>
				<div class="controls">
					<?php echo form_input($paymentDescription); ?>
					<?php echo form_error('paymentDescription'); ?>
				</div>
			</div>
			<div class="control-group mb10">
				<label class="control-label" for="inputEmail">
					<?php echo lang('admin_payments_payment_date');?>
					<font class="mandatory_cls">*</font>
				</label>
				<div class="controls">
					<?php echo form_input($paymentDate); ?>
					<?php echo form_error('paymentDate'); ?>
				</div>
			</div>
			<div class="control-group mb10">
				<label class="control-label" for="inputEmail">
					<?php echo lang('admin_payments_ammount');?>
					<font class="mandatory_cls">*</font>
				</label>
				<div class="controls">
					<?php echo form_input($paymentAmount); ?>
					<?php echo form_error('paymentAmount'); ?>
				</div>
			</div>
			<div class="control-group mb10">
				<label class="control-label" for="inputEmail">
					<?php echo lang('admin_payments_gst_included');?>
				</label>
				<div class="controls">
					<?php echo form_input($paymentGSTInclude); ?>
					<?php echo form_error('paymentGSTInclude'); ?>
				</div>
			</div>
			<div class="control-group mb10">
				<label class="control-label" for="inputEmail">
					<?php echo lang('admin_payments_total_ammount');?>
				</label>
				<div class="controls">
					<?php echo form_input($paymentTotalAmount); ?>
					<?php echo form_error('paymentTotalAmount'); ?>
				</div>
			</div>
			<div class="control-group mb10">
				<label class="control-label" for="inputEmail">
					<?php echo lang('admin_payments_authorised_by');?>
					<font class="mandatory_cls">*</font>
				</label>
				<div class="controls">
					<?php echo form_input($paymentAuthorisedBy); ?>
					<?php echo form_error('paymentAuthorisedBy'); ?>
				</div>
			</div>
		
		</div>
		<div class="row-fluid mt20">
			<?php 
				
				//button show
				$formButton['showbutton']   = array('save','reset');
				$formButton['labletext'] 	= array('save'=>lang('comm_save_changes'),'reset'=>lang('comm_reset'));
				$formButton['buttonclass']  = array('save'=>'submitbtn_cus mr_16','reset'=>'submitbtn_cus reset_form');
				$this->load->view('event/common_save_reset_button',$formButton);
			?>
		</div>
	</div>
	
	<?php echo form_close(); ?>
</div>
<script type="text/javascript">
//call for data save
ajaxdatasave('formPaymentFundTrans','<?php echo $this->uri->uri_string(); ?>',false,true,true,false,true,false,false);
</script>
