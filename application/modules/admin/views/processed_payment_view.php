<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?><?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
	
<div class="row-fluid">
	<div class="eventcontainerbg">
		<div class="headingbg">
			<div class="currenteventH bg_none pl0">
				<?php echo lang('admin_processed_payments'); ?>
			</div>
		</div>
		<div class="panel-body">
			<div class="TablecontentHolder tabledatascrolling">
				<div class="content">
					<table class="admintable tablecuston_scroll" cellpadding="0" cellspacing="0" border="0">
					<tbody>
					<?php if(!empty($paymentfundslist)){  ?>
						<thead>
						<tr>
							<th>
									<?php echo lang('admin_payments_list_date'); ?>
							</th>
							<th>
									<?php echo lang('admin_payments_list_paid_to'); ?>
							</th>
							<th>
									<?php echo lang('admin_payments_list_contact'); ?>
							</th>
							<th>
									<?php echo lang('admin_payments_list_description'); ?>
							</th>
							<th>
									<?php echo lang('admin_payments_list_amount'); ?>
							</th>
						
						</tr>
						</thead>
						<?php
							foreach($paymentfundslist as $paymentfunds ){
						?>	
						<tr>
						<td>
								<?php echo  dateFormate($paymentfunds->payment_date, "d/m/Y"); ?>
						</td>
						
						<td>
								<?php echo ucwords($paymentfunds->paid_to); ?>
						</td>
						
						<td>
								<?php echo ucwords($paymentfunds->contact_name); ?>
						</td>
						
						<td>
								<?php echo $paymentfunds->description; ?>
						</td>
						<td>
								<?php echo number_format($paymentfunds->total_amount,2); ?>
						</td>
					
					</tr>
					
					<?php }  }else{ ?>
						
						<thead>
							<tr>
								<th colspan="9">
									<?php echo lang('admin_payments_no_found'); ?>
								</th>
							</tr>
						</thead>
					<?php	
					} ?>
					</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="spacer10"></div>


