<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<div class="spacer15">
</div>
<div class="row-fluid">
	<div class="headingh1">
		<?php echo lang('admin_corportate_request'); ?>
	</div>
	<div class="spacer40">
	</div>
</div>

<div class="spacer15">
</div>
<div class="loadviewsection">
	<?php echo $this->load->view('view_corporate_list'); ?>
</div>

<script type="text/javascript">

	//change booth status
	$(".booth_status").click(function(){
		
		var requestid 	= $(this).attr('id');
		var status 	= $(this).attr('status');
		
		
		var url = baseUrl+'admin/changecorporatetatus';
		var fromData = {'requestid':requestid,'status':status};
		$.ajax({
		  type:'POST',
		  data:fromData,
		  url: url,
		  dataType: 'json',
		  async: true,
		  cache: false,
		  beforeSend: function( ) {
				//open loader
				loaderopen();
			},
		  success: function(data){	
				//check data 
				if(data){
					//show text in loader
					showlaoderwithmessag(data.msg);
					
					//after show text and page will refresh 2 second delay
					refreshPgeDelay();
				}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				//hide loader
				loaderclose();
				custom_popup('Request failed.',false);
			}
		});
		
	});
	

</script>



