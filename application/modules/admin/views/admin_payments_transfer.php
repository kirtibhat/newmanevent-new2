<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>


<div class="spacer40">
</div>
<div class="row-fluid">
	<div class="headingh1">
		<?php echo lang('admin_payments_transfers'); ?>
	</div>
</div>
<div class="spacer40">
</div>
<div class="row-fluid mt20">
	<div class="span7">
		
		<?php $this->load->view('payment_funds_transfers_view'); ?>
		<!-- Payment & Funds Transfers -->
	</div>
</div>

<?php $this->load->view('processed_payment_view'); ?>

<script type="text/javascript">

var gstRate = parseInt('<?php echo (!empty($eventdetails->gst_rate))?$eventdetails->gst_rate:"0"; ?>');
	
/*----This function is used to unit price enter and show payment option-----*/
unitPriceManage('unitPriceEnter',false,'paymentGSTInclude','paymentTotalAmount');

</script>
