<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 * This class is used to manage for admin controller logic and data
 *
 * @package   admin manage
 * @author    Lokendra Meena
 * @email     lokendrameena@cdnsol.com
 * @since     Version 1.0
 * @filesource
 */
 
 
class Factory{
	 
	//set limit of show records 
	private $limit = '10';  
	private $ci    = NULL;  
	
	function __construct($ci){
		//create instance object
		$this->ci = $ci;
		$this->ci->load->library(array('head','template','pagination_custom'));
		$this->ci->load->model(array('admin_model'));
	}
	
	
	//----------------------------------------------------------------------------
	
    /*
     * @access: public
     * @default load 
     * @description: This function is used to manage regsitrant listing
     * @load: login view
     * @return: void
     *  
     */
    
    public function registrantmanager(){
		
		$page 			= ($this->ci->input->post('page'))?$this->ci->input->post('page'):'0';
		
		if($page) 
			$start = ($page - 1) * $this->limit; 	//first item to display on this page
		else
			$start = 0;	
	
		//get record by page wise
		$registrantlist = $this->ci->admin_model->registrantlist($start,$this->limit);
		
		//echo "<pre>";
		//print_r($registrantlist);die();
		
		
		//get total record
		$numerOfPages		 = $this->ci->admin_model->registrantlist();
		
		//set pagination config details
		$this->ci->pagination_custom->limit 	= $this->limit;
		$this->ci->pagination_custom->postPage 	= $page;
		$this->ci->pagination_custom->totalRecords = $numerOfPages;
		$this->ci->pagination_custom->initialize();
		$data['paginationShow'] 		= $this->ci->pagination_custom->pagination_link();
		$data['registrantlistdata']  	= $registrantlist;
		$data['recordsCount'] 			= count($registrantlist);
		
		if($this->ci->input->is_ajax_request()){
			$this->ci->load->view('view_registrant_list',$data);
		}else{
			$this->ci->template->load('template','admin_registrant_list',$data,true);
		}
	}
	
	/*
	 * @access: public
     * @description: This function is used to delete event register user data
     * @return: void
     *  
     */ 
	
	public function deleteeventuser(){
		
		$deleteId = $this->ci->input->post('deleteId');
		
		//delete data form contact person
		$whereCondi1 = array('id'=>$deleteId);
		$whereCondi2 = array('registered_user_id'=>$deleteId);
		//delete record form frnt_breakouts table
		$this->ci->common_model->deleteRowFromTabel('frnt_breakouts',$whereCondi2);
		//delete record form frnt_form_field_value table
		$this->ci->common_model->deleteRowFromTabel('frnt_form_field_value',$whereCondi2);
		
		//delete record form frnt_side_event_guest table
		$getFrntSideEvent = $this->ci->common_model->getDataFromTabel('frnt_side_event','id',$whereCondi2);
		if(!empty($getFrntSideEvent)){
			foreach($getFrntSideEvent as $frntSideEvent){
				$whereCondi3 = array('frnt_side_event_id'=>$frntSideEvent->id);
				$this->ci->common_model->deleteRowFromTabel('frnt_side_event_guest',$whereCondi3);
			}
		}
		
		//delete record form frnt_side_event table
		$this->ci->common_model->deleteRowFromTabel('frnt_side_event',$whereCondi2);
		
		//delete record form nm_frnt_registrant table
		$this->ci->common_model->deleteRowFromTabel('frnt_registrant',$whereCondi1);
		
		$msg = lang('admin_msg_event_record_delete');
		$returnArray=array('msg'=>$msg,'is_success'=>'true');
		set_global_messages($returnArray);
		echo json_encode($returnArray);
	}
	
	//--------------------------------------------------------------------------------
	
	/* 
	* @access: public
	* @description: This functiona is used to payment transfer and show summary
	* @return void
	*/ 

	public function paymentstransfers(){
		
		if($this->ci->input->is_ajax_request()){
			
			$this->_paymentFundsInsert();
					
		}else{
			//calling view show
			$this->_paymentstransfersView();
		}
	}	
	
	
	//-------------------------------------------------------------------------
	
	/*
	 * @access: private
	 * @description: This function is used to show payment transfered view
	 * @return: void 
	 */ 
	 
	private function _paymentstransfersView(){
		 
		//get logged in id 
		$userId 					 = isLoginUser();
		
		//get current event id
		$eventId 					 = currentEventId();
		
		//decode entered eventid
		$eventId = decode($eventId); 
		
		$whereCondi2				 = array('user_id'=>$userId, 'event_id'=>$eventId);
		$whereEvent					 = array('event_id'=>$eventId);
		$data['paymentfundslist'] 	 = $this->ci->common_model->getDataFromTabel('payment_funds_transfer','*',$whereCondi2);
		
		//get event details
		$eventdetails			 = $this->ci->common_model->getDataFromTabel('event_invoice','gst_rate',$whereEvent);
		
		if(!empty($eventdetails)){
			$eventdetails = $eventdetails[0];
		}
		
		$data['eventdetails'] 		= $eventdetails;
		$data['userId'] 			= $userId;
		$this->ci->template->load('template','admin_payments_transfer',$data,TRUE);
	}
	
	
	//-------------------------------------------------------------------------
	
	/*
	 *  @access: private
	 *	@description: This function is used to payment funds manage insert 
	 *	@return: void 
	 */ 
	 
	private function _paymentFundsInsert(){
		 
	    //get logged in user id
		$userId 			= isLoginUser();
		
		//get current event id
		$eventId 			= currentEventId();
		
		//decode entered eventid
		$eventId = decode($eventId); 
		
		//This function is used to validate
		$this->_paymentFundsValidate();	
		
		if ($this->ci->form_validation->run($this->ci))
		{
			//call for save my profile data save
			$this->_paymentFundsSave($userId,$eventId);
			
			$msg = lang('admin_msg_payment_fund_transfered');
			//after redirect show message
			$msgArray = array('msg'=>$msg,'is_success'=>'true');
			set_global_messages($msgArray);
			//json message
			echo json_message('msg',$msg);
		}
		else
		{
			$errors = $this->ci->form_validation->error_array();
			echo json_message('msg',$errors,'is_success','false');
		}
	}
	
	
	//-------------------------------------------------------------------------
	
	/*
	 * @access: private
	 * @description: This function is used to validate payment funds
	 * @return: void 
	 */ 
	private function _paymentFundsValidate(){
		
		// set ruls for form filed
		$this->ci->form_validation->set_rules('paymentPaidTo', 'paid to', 'trim|required');
		$this->ci->form_validation->set_rules('paymentContactName', 'contact name', 'trim|required');
		$this->ci->form_validation->set_rules('paymentDescription', 'description', 'trim|required');
		$this->ci->form_validation->set_rules('paymentDate', 'payment date', 'trim|required');
		$this->ci->form_validation->set_rules('paymentAmount', 'amount', 'trim|required');
		$this->ci->form_validation->set_rules('paymentGSTInclude', 'GST include', 'trim|required');
		$this->ci->form_validation->set_rules('paymentTotalAmount', 'total amount', 'trim|required');
		$this->ci->form_validation->set_rules('paymentAuthorisedBy', 'authorised by', 'trim|required');
	}
	
	
	// ------------------------------------------------------------------------ 
	
	/*
	 * @access: private
	 * @description: This function is used to save payment funds details
	 * @return void
	 * 
	 */ 
	
	private function _paymentFundsSave($userId,$eventId){
		
		// get post data
		$updateData['user_id']				 = $userId;
		$updateData['event_id']				 = $eventId;
		$updateData['paid_to']		 		 = $this->ci->input->post('paymentPaidTo');
		$updateData['contact_name'] 		 = $this->ci->input->post('paymentContactName');
		$updateData['description'] 			 = $this->ci->input->post('paymentDescription');
		$updateData['payment_date']  		 = dateFormate($this->ci->input->post('paymentDate'),'Y-m-d H:i');
		$updateData['amount']  			 	 = $this->ci->input->post('paymentAmount');
		$updateData['gst_include'] 			 = $this->ci->input->post('paymentGSTInclude');
		$updateData['total_amount'] 		 = $this->ci->input->post('paymentTotalAmount');
		$updateData['authorised_by']  	 	 = $this->ci->input->post('paymentAuthorisedBy');

		$this->ci->common_model->addDataIntoTabel('payment_funds_transfer',$updateData);	
	}
	
	
	//--------------------------------------------------------------------------------
	
	/* 
	 * @access: public
	 * @description: This function is used corporate request for sponsor and exhibitor
	 * @return void
	 */ 
	
	public function corporaterequests(){
		
		$page = ($this->ci->input->post('page'))?$this->ci->input->post('page'):'0';
		
		if($page) 
			$start = ($page - 1) * $this->limit; 			//first item to display on this page
		else
			$start = 0;	
	
		//get record by page wise
		$corporaterequestlist = $this->ci->admin_model->corporaterequestslist($start,$this->limit);
		
		//get total record
		$numerOfPages				 = $this->ci->admin_model->corporaterequestslist();
		
		//set pagination config details
		$this->ci->pagination_custom->limit 	= $this->limit;
		$this->ci->pagination_custom->postPage 	= $page;
		$this->ci->pagination_custom->totalRecords = $numerOfPages;
		$this->ci->pagination_custom->initialize();
		$data['paginationShow'] 		= $this->ci->pagination_custom->pagination_link();
		$data['corporaterequestdata'] 	= $corporaterequestlist;
		$data['recordsCount'] 			= count($corporaterequestlist);
		
		if($this->ci->input->is_ajax_request()){
			$this->ci->load->view('view_corporate_list',$data);
		}else{
			$this->ci->template->load('template','admin_corporate_list',$data,true);
		}
	}
	
	
	
	//-------------------------------------------------------------------------
	
	/*
    * @access: public
    * @description: This function is used to change corporate request status
    * @return: void
    *  
    */ 
	
	public function changecorporatetatus(){
		
		$requestid 	 = $this->ci->input->post('requestid');
		$status 	 = $this->ci->input->post('status');
		$updateData  = array('request_status'=>$status);
		$where 		 = array('id'=>$requestid);
		$this->ci->common_model->updateDataFromTabel('sponsor_exhibitor_booth_request',$updateData,$where);	
		$requestsData = $this->ci->admin_model->corporaterequestsdata($requestid);	
		
		if(!empty($requestsData)){
			$userData['firstname']  = $requestsData->name;
			$userData['useremail']  = $requestsData->email;
			$userData['ispassword'] = $requestsData->password_required;
			$userData['password']   = $requestsData->password;
		}
		
		if($status=='1'){
			//call approve method 
			$this->_corporaterequestapproved($userData);
			$msg = lang('admin_msg_corporate_request_approved');
		}else{
			//call approve cancel 
			$this->_corporaterequestcancel($userData);
			$msg = lang('admin_msg_corporate_request_decline');
		}
		
		
		$returnArray=array('msg'=>$msg,'is_success'=>'true');
		echo json_encode($returnArray);
	}
	
	//-----------------------------------------------------------------------
	
	/*
	 * @access: private
	 * @description: this function is used to notification email relate to corporate request approved
	 * @param: userData
	 * @return void
	 */ 
	
	private function _corporaterequestapproved($userData){
		
		//set email data
		if(isset($userData['ispassword']) && $userData['ispassword']=="1"){
			$cancelMessage 		    = 'We are confirme you, your request is approved, you may further process by below password. <br><br>';
			$cancelMessage 		   .= '<b>Password:</b> '.$userData['password'];
		}else{
			$cancelMessage 		   = 'We are confirme your request is approved, you may further process now.';
		}
		$userEmail 			   = $userData['useremail']; 
		$data['firstName']     = $userData['firstname']; 
		$data['cancelMessage'] = $cancelMessage; 
		$subject = 'Newmanevents: corporate request approved.';
		$this->ci->load->library('email');
		$this->ci->email->from($this->ci->config->item('from_email'));
		$this->ci->email->to($userEmail);
		$this->ci->email->subject($subject);
		$message = $this->ci->load->view('email/corporate_request_approve',$data,true);
		$this->ci->email->message($message);
		$this->ci->email->send(); 
	}
	
	//-----------------------------------------------------------------------
	
	/*
	 * @access: private
	 * @description: this function is used to notification email relate to corporate request cancel
	 * @param: userData
	 * @return void
	 */ 
	
	private function _corporaterequestcancel($userData){
		
		//set email data
		$cancelMessage		   = 'We are canceling your corporate request.';
		$userEmail 			   = $userData['useremail']; 
		$data['firstName']     = $userData['firstname']; 
		$data['cancelMessage'] = $cancelMessage; 
		$subject = 'Newmanevents: corporate request canceled.';
		$this->ci->load->library('email');
		$this->ci->email->from($this->ci->config->item('from_email'));
		$this->ci->email->to($userEmail);
		$this->ci->email->subject($subject);
		$message = $this->ci->load->view('email/corporate_request_cancel',$data,true);
		$this->ci->email->message($message);
		$this->ci->email->send(); 
	}
	
	
	
}


/* End of file factory.php */
/* Location: ./system/application/modules/admin/libraries/factory.php */
