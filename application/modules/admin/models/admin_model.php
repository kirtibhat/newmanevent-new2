<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Admin_model extends CI_model{


	private $tablenmuser					= 'user';
	private $tablenmfrntregistrant			= 'frnt_registrant';
	private $tablenmevent					= 'event';
	private $tablenmfrntpaymentdetails		= 'frnt_payment_details';
	private $tablennmeventregistrants		= 'event_registrants';
	private $tablenmsideevent				= 'side_event';
	private $tablesponsexhibooths			= 'sponsor_exhibitor_booths';
	private $tablenmsponsor					= 'sponsor';
	private $tablenmexhibitor				= 'exhibitor';
    
	function __construct(){
		parent::__construct();	
			
	}
	
	/*
	 * @description: This function is used to get all registrant data of all event 
	 * @return: object
	 */ 
	
	
	public function registrantlist($start='',$limit=''){
		
		//get current event id
		$eventId = currentEventId();
		//decode entered eventid
		$eventId = decode($eventId); 
		
		$searchField 	= $this->input->post('searchField');
		
		$columnName     = ($this->input->post('columnName'))?$this->input->post('columnName'):'';
		$orderBy    	= ($this->input->post('orderBy'))?$this->input->post('orderBy'):'';
		
		$this->db->select('frtrgst.id,frtrgst.registrant_id, frtrgst.payment_status, frtrgst.registration_booking_number,event.event_title,frnt_payment_details.total_amount,frnt_payment_details.payment_date,event_registrants.registrant_name');
		$this->db->from('nm_frnt_registrant as frtrgst');
		$this->db->join($this->tablenmuser, 'frtrgst.user_id = user.id');
		$this->db->join($this->tablenmevent, 'user.event_id = event.id');
		$this->db->join($this->tablenmfrntpaymentdetails, 'frtrgst.payment_id = frnt_payment_details.id');
		$this->db->join($this->tablennmeventregistrants, 'frtrgst.registrant_id = event_registrants.id');
		
		//serach show by type
		if(!empty($searchField)){
			$this->db->like('event_registrants.registrant_name', $searchField); 
		}
		
		
		$this->db->where('frtrgst.payment_status ','1'); //check payment paid status
		$this->db->where('frtrgst.event_id ',$eventId); //check event id
		$this->db->where('frtrgst.is_active ','1'); //check payment confirm status
		if(!empty($columnName)){
			$this->db->order_by("frtrgst.".$columnName, $orderBy); 
		}else{
			$this->db->order_by("frtrgst.id", "desc"); 
		}
		if(!empty($limit)){
			$this->db->limit($limit,$start);
		}
		$query = $this->db->get();
		return $query->result();
	}
	
	/*
	 * @description: This function is used to get all request sponsor list data
	 * @return: object
	 */ 
	
	public function corporaterequestslist($start='',$limit=''){	
		$this->db->select('sebr.*, e.event_title, es.sponsor_name, ee.exhibitor_name');
		$this->db->from('sponsor_exhibitor_booth_request as sebr');
		$this->db->join($this->tablenmevent.' as e', 'e.id = sebr.event_id','left');
		$this->db->join($this->tablenmsponsor.' as es', 'es.id = sebr.sponsor_id','left');
		$this->db->join($this->tablenmexhibitor.' as ee', 'ee.id = sebr.exhibitor_id','left');
		$this->db->order_by("id", "desc"); 
		if(!empty($limit)){
			$this->db->limit($limit,$start);
		}
		$query = $this->db->get();
		return $query->result();
	}
	
	
	/*
	 * @description: This function is used to get corporate request  data
	 * @param: requestId
	 * @return: object
	 */ 
	
	public function corporaterequestsdata($requestId){	
		$this->db->select('sebr.*, e.event_title, es.password_required,es.password');
		$this->db->from('sponsor_exhibitor_booth_request as sebr');
		$this->db->join($this->tablenmevent.' as e', 'e.id = sebr.event_id','left');
		$this->db->join($this->tablenmsponsor.' as es', 'es.id = sebr.sponsor_id','left');
		$query = $this->db->get();
		return $query->row();
	}
	
}
?>
