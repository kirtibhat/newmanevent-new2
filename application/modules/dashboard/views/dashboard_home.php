<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
$formAddEvent = array(
    'name'          => 'formAddEvent',
    'id'            => 'formAddEvent',
    'method'        => 'post',
    'class'         => 'form-horizontal mt10',
);
$eventName = array(
    'name'          => 'eventName',
    'value'         => '',
    'id'            => 'eventName',
    'type'          => 'text',
    'placeholder'   => 'Event Name',
    'autocomplete'  => 'off',
    'class'         => 'xxlarge_input trim_check',
);
$eventStartDate = array(
    'name'          => 'eventStartDate',
    'value'         => '',
    'id'            => 'eventStartDate',
    'type'          => 'text',
    'placeholder'   => 'Event Start Date',
    'class'         => ' medium_input s_datepicker',
    'autocomplete'  => 'off',
    'readonly'      => true
    //'data-format'   => 'yyyy-MM-dd hh:mm:ss',
  );
$eventEndDate = array(
    'name'          => 'eventEndDate',
    'value'         => '',
    'id'            => 'eventEndDate',
    'type'          => 'text',
    'placeholder'   => 'Event End Date',
    'class'         => 'medium_input e_datepicker',
    'autocomplete'  => 'off',
    'readonly'      => true
    //'data-format'   => 'yyyy-MM-dd hh:mm:ss',
  );
  
?>
<!-- New Event Popup -->
<div class="modal fade newevent" id="newevent">
<div class="modal-dialog">
  <div class="modal-content">
    <div class="modal-header">
      <h4 class="modal-title"><?php echo lang('dash_add_event_title'); ?></h4>
    </div>
    <!-- Start model body -->
    <div class="modal-body">
      <?php  echo form_open(base_url('event/addevent'),$formAddEvent); ?>
      <div class="modal-body-content">
        
        <div class="row">
            <div class="labelDiv pull-left">
                <label class="form-label text-right"><?php echo lang('dash_event_name'); ?><span aria-required="true" class="required">*</span></label>
                <span class="info_btn"><span class="field_info xsmall"><?php echo lang('event_title_info'); ?></span></span>
            </div>
            <?php echo form_input($eventName); ?>
             <span id="error-eventName"></span>
        </div>
        
        <div class="row">
            <div class="labelDiv pull-left">
                <label class="form-label text-right"><?php echo lang('dash_start_date'); ?><span aria-required="true" class="required">*</span></label>
               
            </div>
            <div class="clearFix" id="datepicker1">
                <?php echo form_input($eventStartDate); ?>
                <span id="error-eventStartDate"></span>
                <span class="medium_icon"> <i class="icon-calender cal1"></i> </span>			
                
           </div>     
        </div>
        
        <div class="row">
            <div class="labelDiv pull-left">
                <label class="form-label text-right"><?php echo lang('dash_end_date'); ?><span aria-required="true" class="required">*</span></label>
                
            </div>
             <div class="clearFix" id="datepicker2">
                <?php echo form_input($eventEndDate); ?>
                <span id="error-eventEndDate"></span>
                <span class="medium_icon"> <i class="icon-calender cal2"></i> </span>				
                
            </div>
        </div>
         
      </div>
      <div class="modal-footer">
        <div class="pull-right">
          <?php 
                $extraCancel    = 'class="popup_cancel btn-normal btn" data-dismiss="modal" ';
                $extraSave      = 'class="submitbtn btn-normal btn" ';
                echo form_button('cancel',lang('comm_cancle'),$extraCancel);
                echo form_submit('save',lang('comm_save'),$extraSave);
            ?>
        </div>
      </div>
      
      <input type="hidden" value="loginpost" name="formaction">
      <?php echo form_close(); ?>
    </div>
    <!-- End model body -->
  </div>
</div>
</div>
<!-- New Event Popup End -->

<div class="page_content">
<div class="container">
<div class="row">
	<div class="col-10 dashboard_content">
     <div class="heading">
        <h1 class="font-xlarge">My Events </h1>
      </div>
    	<?php echo lang('dashboard_intro_text'); ?>
    </div>
    <div class="col-5">
    <div class="dashlogo">
    <img src="<?php echo IMAGE; ?>dashboard_event.png" width="208" height="389"> 
    </div>
    </div>
</div> 
 <!--     Current & past event panel      --> 
  <div class="row">
    <div class="col-15">
		<div class="col-5">
		<button class="btn-icon btn-icon-large btn mL35" data-toggle="modal" data-target=".newevent"> <span class="large_icon"><i class="icon-add"></i></span><?php echo lang('dash_add_event'); ?></button>
		</div>
      <!--Current Event Panel -->
      <div class="panel open" id="panel0">
      <div class="panel_header">Current Events</div>
      <div class="panel_contentWrapper" style="display:block;">
      	<!--sub menu one-->
        <div class="tableWrapper">
              <div class="table">
              	<table>
                    <thead>
                      <tr>
                          <th><?php echo lang('dash_id_no'); ?></th>
                          <th><?php echo lang('dash_name_of_event'); ?></th>
                          <th><?php echo lang('dash_event_date'); ?></th>
                          <th class="delegate_limit">
                              <?php echo ($this->config->item('accountType')==1) ? lang('dash_event_limit_free') : lang('dash_event_limit'); ?>  
                          </th>
                          <th class="registered_date">
                               <?php echo ($this->config->item('accountType')==1) ? lang('dash_number_of_rego_free') : lang('dash_number_of_rego'); ?>
                          </th>
                          <th class="action_column">&nbsp;</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php if($getcurrenteventslist){
                                foreach($getcurrenteventslist as $eventslist){ 
                                ?>
                                <tr id="event_row<?php echo $eventslist->event_number; ?>">
                                    <td>
                                        <?php echo $eventslist->operand_number.'-'.$eventslist->event_number; ?>
                                    </td>
                                    <td>
                                        <?php echo $eventslist->event_title; 
                                        if($eventslist->is_approved == 0){
										   echo '<span class="edraft pull-right">(Draft)</span>';
										}
                                        ?>
                                    </td>
                                    <td><?php echo dateFormate($eventslist->starttime,'d/m/Y'); ?></td>
                                    <td><?php echo $eventslist->number_of_rego; ?></td>
                                    <td>
                                        <?php 
                                            $whereCondi = array('event_id'=>$eventslist->id,'is_active'=>'1');
                                            $getResult = getDataFromTabel('frnt_registrant','id',$whereCondi);
                                            echo (!empty($getResult))?count($getResult):'0';
                                        ?>
                                    </td>
                                    <td>
                                        <!-- Action buttons -->
                                        <div class="editButtonGroup">
                                            <!-- button copy -->
                                            <a type="copy_btn button" class="btn" href="javascript:void(0)"><span class="medium_icon"> <i class="icon-copy"></i> </span></a>
                                           
                                            <!-- button edit -->
                                            <a href="<?php echo base_url('event/eventdetails/'.encode($eventslist->id)); ?>" type="button" class="btn eventsetup_btn"><span class="medium_icon"> <i class="icon-edit"></i> </span></a>
                                            
                                            <!-- button report -->
                                            
                                            <a href="javascript:void(0)" type="button" class="btn reports_btn"><span class="medium_icon"> <i class="icon-quick_list"></i> </span></a>
                                            
                                            <!-- button admin -->
                                            
                                            <a href="javascript:void(0)" type="button" class="btn admin_btn"><span class="medium_icon"> <i class="icon-admin"></i> </span></a>
                                            
                                            <!-- button delete -->
                                            <?php if($eventslist->is_approved == 0){ ?>
                                            
                                            <a href="javascript:void(0)" deleteId="<?php echo $eventslist->id; ?>" type="button" class="btn red delete_btn delete_event"><span class="medium_icon"> <i class="icon-delete"></i> </span></a>
                                            
                                            <?php } ?>
                                        </div>
                                    </td>
                                </tr>
                                    
                            <?php }
                            }else { ?>
                                <tr>
                                    <td colspan="7">
                                        No events found.
                                    </td>
                                </tr>
                            <?php } ?>
                    </tbody>     
                  </table>
                  <div class="tableFoot"></div>
              </div>             
            </div>
        </div>
      </div>
      <!-- End panel current event -->  
      
       <!--Past Event Panel -->
      <div class="panel" id="panel1">
      <div class="panel_header">Past Events</div>
      <div class="panel_contentWrapper">
      	<!--sub menu one-->
        <div class="tableWrapper">
              <div class="table">
              	<table>
                    <thead>
                      <tr>
                          <th><?php echo lang('dash_id_no'); ?></th>
                          <th><?php echo lang('dash_name_of_event'); ?></th>
                          <th><?php echo lang('dash_event_date'); ?></th>
                          <th class="delegate_limit">
                              <?php echo ($this->config->item('accountType')==1) ? lang('dash_event_limit_free') : lang('dash_event_limit'); ?>  
                          </th>
                          <th class="registered_date">
                               <?php echo ($this->config->item('accountType')==1) ? lang('dash_total_rego_free') : lang('dash_total_rego'); ?>
                          </th>
                          <th class="action_column">&nbsp;</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php if($getpasteventslist){
                                foreach($getpasteventslist as $eventslist){ ?>
                                <tr id="event_row<?php echo $eventslist->event_number; ?>">
                                    <td>
                                        <?php echo $eventslist->operand_number.'-'.$eventslist->event_number; ?>
                                    </td>
                                    <td><?php echo $eventslist->event_title;?></td>
                                    <td><?php echo dateFormate($eventslist->starttime,'d/m/Y'); ?></td>
                                    <td><?php echo $eventslist->number_of_rego; ?></td>
                                    <td>
                                        <?php 
                                            $whereCondi = array('event_id'=>$eventslist->id,'is_active'=>'1');
                                            $getResult = getDataFromTabel('frnt_registrant','id',$whereCondi);
                                            echo (!empty($getResult))?count($getResult):'0';
                                        ?>
                                    </td>
                                    <td>
                                        <!-- Action buttons -->
                                        <div class="editButtonGroup">
                                            <!-- button copy -->
                                            <a type="copy_btn button" class="btn" href="javascript:void(0)"><span class="medium_icon"> <i class="icon-copy"></i> </span></a>
                                           
                                            <!-- button edit -->
                                            <a href="<?php echo base_url('event/eventdetails/'.encode($eventslist->id)); ?>" type="button" class="btn eventsetup_btn"><span class="medium_icon"> <i class="icon-edit"></i> </span></a>
                                            
                                            <!-- button report -->
                                            
                                            <a href="javascript:void(0)" type="button" class="btn reports_btn"><span class="medium_icon"> <i class="icon-quick_list"></i> </span></a>
                                            
                                            <!-- button admin -->
                                            
                                            <a href="javascript:void(0)" type="button" class="btn admin_btn"><span class="medium_icon"> <i class="icon-admin"></i> </span></a>
                                            
                                        </div>
                                    </td>
                                </tr>
                                    
                            <?php }
                            }else { ?>
                                <tr>
                                    <td colspan="7">
                                        No events found. 
                                    </td>
                                </tr>
                            <?php } ?>
                    </tbody>     
                  </table>
                  <div class="tableFoot"></div>
              </div>             
            </div>
        </div>
      </div>
      <!-- End panel past event -->  
      
    </div>
    <div class="clearFix display_inline w_100 mT10"></div>	
    </div>
  </div>
</div>
</div>
<!--start custom tooltip-->
<div id="tooltip_wrapper"></div>
<!--end custom tooltip-->
<script>
popupopen('add_event_model','newevent');
$(document).ready(function() {
    $('#eventEndDate').on('changeDate', function(e){
    var startdateval= $("#eventStartDate").val();
        var enddateval= $("#eventEndDate").val();
            if(startdateval=="")
            { 
                $("#start_date_error").removeClass('dn');
                $("#eventEndDate").val(''); 
            }
            else{
                $("#end_date_error").addClass('dn');
            }
        var sDate = Date.parse(startdateval);
        var eDate2 = Date.parse(enddateval);
            if (sDate > eDate2) {
                $("#end_date_error").removeClass('dn');
                $("#eventEndDate").val(''); 
            }else{
                $("#end_date_error").addClass('dn');
            }
        return true;
    });
    
});
//create event save 
ajaxdatasave('formAddEvent','event/addevent',false,true,false,true,false,false,false,false,true,'newevent',true,'');
//Delete events
customconfirm('delete_event','event/deletesingleevent','','','',true,true);
</script>


