<?php
class Dashboard_model extends CI_model{


    private $tablenmevent= 'event';
    private $tablenmuser= 'user';
    
    
    function __construct(){
        parent::__construct();      
       
    }
    
    //--------------------------------------------------------------------------
    
    /*
     * @descript: This function is used to showing current evetns list
     * @return object
     * 
     */ 
    
    function getcurrenteventslist($userId){
        
        $this->db->select('e.*,u.operand_number');
        $this->db->from($this->tablenmevent.' as e ');
        $this->db->join($this->tablenmuser.' as u', 'u.id = e.user_id');
        $this->db->order_by('id','DESC');
        $this->db->where('e.user_id',$userId);
        $this->db->where('e.endtime >=',date('Y-m-d'));
        $query = $this->db->get();
        return $query->result();
    }
    
    //------------------------------------------------------------------------
    
    /*
     * @descript: This function is used to showing past evetns list
     * @return object
     * 
     */ 
    
    function getpasteventslist($userId){
        
        
        $this->db->select('e.*,u.operand_number');
        $this->db->from($this->tablenmevent.' as e ');
        $this->db->join($this->tablenmuser.' as u', 'u.id = e.user_id');
        $this->db->order_by('id','DESC');
        $this->db->where('e.user_id',$userId);
        $this->db->where('e.endtime <',date('Y-m-d'));
        $query = $this->db->get();
        return $query->result();
    }
    
    //-----------------------------------------------------------------------   
    
}

?>
