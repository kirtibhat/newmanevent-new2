<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 * This class is used to manage for manage dashboard
 *
 * @package   dashboard manage
 * @author    Lokendra Meena
 * @email     lokendrameena@cdnsol.com
 * @since     Version 1.0
 * @filesource
 */
 
 
class Factory{
	 
	//set limit of show records 
	private $ci = NULL;  
	
	function __construct($ci){
		//create instance object
		$this->ci = $ci;
	}
	
    // ------------------------------------------------------------------------ 
	
	/*
	 * @access: public
     * @default load 
     * @description: This function is used to show current and past event list
     * @load: home view
     * @return: void
     */
     
	public function index(){
		$userId = isLoginUser();
		$data['getcurrenteventslist'] = $this->ci->dashboard_model->getcurrenteventslist($userId);
		$data['getpasteventslist'] 	  = $this->ci->dashboard_model->getpasteventslist($userId);
		$this->ci->template->load('template','dashboard_home',$data, TRUE);
	}
	 
	
	
}


/* End of file factory.php */
/* Location: ./system/application/modules/dashboard/libraries/factory.php */
