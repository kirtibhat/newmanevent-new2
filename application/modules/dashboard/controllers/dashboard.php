<?php

/*
 * @description: Manage dashboard
 * auther: lokendra meena
 * email: lokendrameena@cdnsol.com
 */ 

class Dashboard extends MX_Controller{

	function __construct(){
		parent::__construct();
		$this->load->library('factory',$this);
		$this->load->library(array('head','template'));
		$this->load->model(array('dashboard_model'));
		$this->load->language(array('dashboard'));
		$this->session_check->checkSession(); 
	}

    /*
    * @access: public
    * @default load 
    * @description: This function is used to show current and past event list
    * @load: home view
    * @return: void
    */
     
	public function index(){
		$this->factory->index();
	}
	
		
  
}

/* End of file messages.php */
/* Location: ./system/application/modules/dashboard/dashboard.php */
?>
