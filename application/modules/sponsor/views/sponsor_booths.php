 <?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
//--------To show sponsor packages booths------------------------------------------------------//	

   $sponsor_id='';
   $sponsorName = (isset($sponsorname) && !empty($sponsorname))?$sponsorname:'';
   $sponsorPrice = (isset($sponsorprice) && !empty($sponsorprice))?$sponsorprice:'';


  
 
  // $eventId=(isset($event_id) && !empty($event_id))?$event_id:'';
    $user_id=frntUserLogin('sponsor');
  
   
  $sponsor_id =  $sponsorId ;
 
   $reqstatus;
$sponsorDetails;


if(!empty($floorPlanPDF))
			{
				$floorPlanPDF=base_url().$floorPlanPDF[0]->floor_plan_path.$floorPlanPDF[0]->floor_plan;
			}
	
$title="";
$pendingClass='';
$purchasebooth=false;

//$sponsor_id=$booth->sponsor_id;	
	 
?> 
<div class="container mt40lesseight container_main">
	<?php
	      //to load header
          $this->load->view('common/header'); 
         
           //to load sponsor popup
           $this->load->view('sponsor_login_popup'); 
          
           // to load password popup
           $this->load->view('sponsor_password_popup');
           
           // to show selected booth in popup 
          $this->load->view('booth_popup'); 
          
          // to load sponsor password request form popup
          $this->load->view('sponsor_pass_request_popup');

	?>
    <div class="row-fluid mt10">
		
    	<div class="span7 signupresponsive" id="frontendleft">
			<?php
		  $this->load->view('sponsor_login_popup'); 
		  
		  if($user_id == ''){ 
		  ?>
		   <a class="submitbtn_cus mt10 mr0 bg_741372 add_login_form_field" data-toggle="modal" data-target="#sponsor_booth_popup" ><?php echo lang('frntLogin'); ?></a>
		 <?php
		   }else{ if ($user_id > 0){ 
		 ?>
		  <a href="<?php echo base_url().'sponsor/logout';?>" class="submitbtn_cus mt10 mr0 bg_741372 " ><?php echo lang('frntLogout'); ?></a>
		<?php }} ?> <br><br>
        <div class="container-fluid signupbg">
                	<div class="accorianbg bdrbN">
                            <div class="accordion-group bg_00aec5">
								
                                <div class="accordion-heading">									 
                                    <div href="#ContactPersonDetails" data-parent="#monogram-acc" class="accordion-toggle">
								    <?php echo $sponsorDetails->sponsor_name; ?>
                                    </div>
                                </div>
                                
                                <div class="accordion-heading exhi_Darkblue">
                                    <div href="#ContactPersonDetails" data-parent="#monogram-acc">
                                    <?php echo $sponsorDetails->description ; ?> <span class="fs14">(+GST)</span>
                                    </div>
                                </div>
                                
                                <div class="accordion-body">
                                <div class="innerbody exhilisting">
                                <ul>
                                	<li>1 x Exhibition booth 2m wide x 3.5m deep</li>
                                    <li>2 x Walls, in Velcro Fabric</li>
                                    <li>2 x Lights, 120 watt spotlights</li>
                                    <li>1 x Signage, Fascia panel with organisations name</li>
                                    <li>2 x Power outlet, 5 amp general purpose outlet</li>
                                    <li>2 x Complimentary delegate registrations</li>
                                    <li>2 x Complimentary tickets to Dinner and Drinks</li>
                                </ul>
                                
                                <ul class="linkcolor">
                              <li><a href="javascript:void(0)">Acknoledgement of your organisation on the summit 
                              	website and a hyperlink to your organisations website</a></li>
								<li><a href="javascript:void(0)">Your organisations name on the Expo passport</a></li>
                                <li><a href="javascript:void(0)">Your organisations will recieve one complimentary satchel</a></li>
                                <li><a href="javascript:void(0)">Your organisation will have access to delegate information</a></li>
                                </ul>
                                
                                <div class="formheading">Select which booth you would like</div>
                                <div class="mendotryfield">Please note if there is a number missing it is already sold.</div>
                                <?php
                                   
                                    if(isset($floorPlanPDF) && !empty($floorPlanPDF)) 
                                    {
										 
                                      ?>
                                         <a href="<?php echo $floorPlanPDF;?>" target="_blank" >
											<button id="closebtn" class="submitbtn_cus mt10 mr0 bg_741372 fleft" type="button">SHOW PDF OF FLOOR PLAN</button>
										</a>     
                                      <?php
							        }
							        else
							        {
										?>
											<button id="closebtn" class="submitbtn_cus mt10 mr0 bg_741372 fleft noPDFMSG" type="button">SHOW PDF OF FLOOR PLAN</button>
										<?php
									}
								
                                 ?>
                             
                               <div class="clearfix"></div>
                                <div class="mt20"></div>
                                <?php
           //to apply css on booths                      
				
				
			/*	if(!empty($selectedbooths))
				{
					foreach($selectedbooths as $booth)
					{
							if($booth->booth_status==1 && $booth->payment_status==1)
							{
								$purchaseBooths=$booth->booth_id;
							}
							$selectBoothArray=$booth->booth_id;
					}
				}
$selectBoothArray=array();
				 $purchaseBooths=array();   */          
								
                               
                                if(isset($sponsorbooths) && !empty($sponsorbooths))
							    {
								    foreach($sponsorbooths as $booth)
								    {
										$boothId= $booth['id'];
										$getSearchData = search($selectedbooths, 'booth_id', $boothId);
									
										//if not empty the apply condition
										if(!empty($getSearchData))
										{
										$getSearchData = $getSearchData['0'];
										
										if($getSearchData['payment_status'] == 1){
												$pendingClass='hide';
											
											 }
											
												
												// foreach($selectedbooths as $selecbooth)
												// {
									// echo $selecbooth['booth_id'] ;
									// echo $getSearchData['booth_id'] ;
								//	 echo '<pre>';
								//	print_r(search($sponsorsetbooths, 'id', $getSearchData['booth_id']));	
									/*if(search($sponsorsetbooths, 'id', $getSearchData['booth_id']))
									{ 
										echo $booth->id;
									
										if(in_array($getSearchData['booth_id'],$sponsorbooths)){
											 $pendingClass='slectedbooth';
											}
										
										
										
										}*/
											//$pendingClass='';
											
											
												/*
												 */
												 
												// }	
										 
										}
									
										/*if(isset($selectBoothArray) && !empty($selectBoothArray))
										{
											 if(in_array($booth->id,$selectBoothArray))
											 {
												 $pendingClass='pendingbooth';
												 $title='Pending Request';
										     }
										     
										}
										*/
										
										//to check user selected booth
										$pendingClass = "";
										if(!empty($getSearchData)){
											if($getSearchData['payment_status'] =='1'){
												//$pendingClass='hide';
												
												$pendingClass='hide';
											 }else{
											$pendingClass='slectedbooth';
											//$pendingClass='hide';
										}}
									
										//to check purchase booth
										/*if(isset($purchaseBooths) && !empty($purchaseBooths))
										{
											 if(in_array($booth->id,$purchaseBooths))
											 {
												 $purchasebooth=true;
										     }
										}*/
										
									
									     ?>
<input type="hidden" name="userreq" id="selected_booths" value="<?php echo $boothId.''.$pendingClass; ?>">
<input type="hidden" name="selected_booth_id" id="selected_booth_id" value="<?php echo $boothId; ?>">	 
<input type="hidden" name="current_booth_id" id="current_booth_id" value="<?php echo $boothId;?>">
									     <a title="<?php echo $title; ?>" class="floreplane_btn selectbooth booth<?php echo $boothId.' '.$pendingClass; ?>"><?php echo $booth['booth_position']; ?></a>
                                      
                                         <input type="hidden" name="" class="" value="<?php echo $boothId; ?>">	 	  
                                         <input type="hidden" name="" id="booth_position<?php echo $boothId;?>" value="<?php echo $booth['booth_position']; ?>">	 	  
                                    
									     <?php 
								      
							        } 
						        }
					             ?>
					           
					            
								  <div class="clearfix"></div>
				 
				  
				  <?php				 
					
				//if added additional item is not empty
				if(!empty($getadditionaltems)){ ?>
					<h2 >Add additional purchase </h2>
				<?php foreach($getadditionaltems as $getitem){					
				// search if add_purch_id is exist in added additional item array
				$getadditionlData = search($getadditionaltems , 'add_purch_id', $getitem['add_purch_id']);
				
				 } }
				 
				 if(!empty($additionaltems)){
				 foreach ($additionaltems as $purchaseitem ){ 
				// search if add_purch_id is exist in added additional item array to select checkbox	
				$checkedid = search($getadditionaltems , 'add_purch_id', $purchaseitem['id']);
									
					 if($checkedid){
						$checked ='checked="true"';
					}else{$checked= '';
					   }				   
					   ?>
					  
			  <div class="pymtregdetail mt20">
				<div class="tableheading_2 pt2pb2">
					
				<input type="checkbox" <?php echo $checked; ?>  name="additem[]" class="additem" id="additem<?php echo $purchaseitem['id']; ?>" value="<?php echo $purchaseitem['id'];echo "_"; echo $purchaseitem['total_price']; ?>">
				
				<label class="paymentcond">
					<?php echo $purchaseitem['name']; 
					echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
					echo $purchaseitem['total_price'].'.00';  ?>
					</label>
	
				</div>
	   		 </div><?php    } }	?>
					
					 
				<br>		  
				
				<?php  
				  if(!empty($benifits)){ ?>
					  <h2 >Add Benifits</h2>  
				<?php   foreach($benifits as $benifit) { ?>				 
				 <div class="pymtregdetail mt20">
					<div class="tableheading_2 pt2pb2">
									
									
									
									<input type="checkbox" name="benifit" class="benifit" id="benifit_<?php echo $benifit->id; ?>" value="<?php echo $benifit->id; ?>">
									<input type="hidden" name="benifit" class="benifit" id="benifit_<?php echo $benifit->id; ?>" value="valueable">
									
									<label class="paymentcond"><?php echo $benifit->item_title; ?></label>    
						 
				 </div>	
				  </div>	<?php  } 
									}  ?>
								 
								   <a href="<?php echo base_url().'sponsor/index/'.encode($eventId);?>">    <button id="closebtn" class="submitbtn_cus mt10 mr10 bg_741372 booth_back_btn" type="button">BACK</button></a>
                                   
                                <?php   if($user_id != ''){ ?>
                                   
                                   <a href="<?php echo base_url().'sponsor/sponsorhome/'.encode($sponsor_id);?>"><button id="closebtn" class="submitbtn_cus mt10 mr10 bg_741372" type="button">Pay</button></a>
                                   <button id="closebtn" class="submitbtn_cus mt10 mr10 bg_741372 saveSponsorBooth" type="button">SAVE</button>
                               <?php } ?>
                                </div>  
                                  <input type="hidden" name="sponsor_id" id="sponsor_id" value="<?php echo $sponsor_id; ?>">
                                  <input type="hidden" name="userreq" id="userreq" value="<?php echo $userreq; ?>">
                                  <input type="hidden" name="userreq" id="booth_sponsor_id" value="<?php echo $sponsor_id; ?>">
                                  	  
                                  <input type="hidden" name="user_id" id="user_id" value="<?php echo $user_id; ?>">	
                                  <input type="hidden" name="is_password_enter" id="is_password_enter" value="<?php echo $verify_pass; ?>">	
                                    	  
									
									<input type="hidden" name="password_required" id="password_required" value="<?php echo $sponsorDetails->password_required; ?>">
									<input type="hidden" name="subject_to_approval" id="subject_to_approval" value="<?php echo $sponsorDetails->subject_to_approval; ?>">
									<input type="hidden" name="reqstatus" id="reqstatus" value="<?php echo $reqstatus; ?>">
									<input type="hidden" name="userselectionlimit" id="userselectionlimit" value="<?php echo $userselectionlimit; ?>">
									<input type="hidden" name="limit_package" id="limit_package" value="<?php echo $sponsorDetails->limit_package; ?>">
                                 
                                 
                                 <div class="clearfix"></div>
                                
                                </div>
                                </div>
                            </div>
                </div> <!-- /accordionbg -->
            </div>
        </div>
        
        <div class="span5 addbgcontainer pull-right spnncerresponsive" id="frontendright">
        	<div class="addbg"> <div class="addcontent">Advertisement</div></div>
            <div class="addbg"> <div class="addcontent">Advertisement</div></div>
        </div>
         <!-- /sponcerbg -->
         
         
         
        
        <div class="clearfix"></div>
   
    <div class="clearfix"></div>
    <div class="spacer40"></div>    
</div> <!-- /container -->

                          

 


<script type="text/javascript">
	
var IMAGE='<?php echo IMAGE; ?>';
	 
$(document).ready(function() {
	
	 var rightHeight= $("#frontendright").height();
	 var leftHeight= $("#frontendleft").height();
	 var getWindowWidth = $( window ).width();
	 
	 $('.back_btn').hide();
	if(getWindowWidth > 960){
		if(leftHeight > rightHeight){
			$("#frontendright").css('height',leftHeight);
		}else{
			$("#frontendleft").css('height',rightHeight);
		}
	}
	
	$('input:radio').screwDefaultButtons({
		image: 'url("'+IMAGE+'foontendradio.png")',
		width: 17,
		height: 17,
	});
	
	$('input:checkbox').screwDefaultButtons({
		image: 'url("'+IMAGE+'frontendcheck.png")',
		width: 17,
		height: 17,
	});
	
	
	
    
  
 $('.benifit').click(function() {	
		 
		 var getCheckboxStaus	= $(this).find('input:checked').is(":checked");
		 var getFieldId			= $(this).find('input').val();
		 var sponsor_id			= $('#sponsor_id').val();
		 var user_id			= $('#user_id').val();
		 if(user_id==0){
			 return false;
			 }
		 if(getCheckboxStaus){
				var checkboxCheccked = 1;
			}else{
				var checkboxCheccked = 0;
			}	
		 	
			$.ajax({
			  type: 'POST',
			  url:  baseUrl+'sponsor/savebenifits',
			  data: {"checkboxCheccked":checkboxCheccked,"getFieldId":getFieldId,"sponsor_id":sponsor_id,"user_id":user_id},
			  success: function(msg) {
				  
						
				 }, 
			});
			
		});

	
});
</script>

