<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

 
 $eventId=(isset($eventId) && !empty($eventId))?$eventId:'';

 ?>

<div class="modal fade frontendpopup dn" id="add_login_form_field_popup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close closebtnfront" data-dismiss="modal" aria-hidden="true">&times;</button>
        <div class="commonS_circle ml0">
                	<div class="lockicon_frontend"></div>
                </div>
        <h4 class="modal-title"><?php echo lang('frntLogin'); ?></h4>
        <div class="clearfix"></div>
      </div>
       <div class="modal-body">
             <?php echo lang('PleaseEnterEmailPassMsg'); ?>
       <div class="modelinner pl0 pr0">
       		
				<form name="sponsorlogin" id="sponsorlogin" method="post" action="" class="form-horizontal mt10"> 
                    <label><?php echo lang('frntEmail'); ?></label>
                        <div class="position_R">
                         <input type="text" name="login_email" id="login_email" value="" class="loginrequired email" placeholder="<?php echo lang('frntEmail'); ?>*">
				     
				        <label class="efformessage loginvalidate frontend_popup_error"><span class="errorarrow"></span><?php echo lang('frntRequiredMsg'); ?></label>
                      </div>
                    
                    <div class="spacer15"></div>
                    
                     <label><?php echo lang('frntPassword'); ?></label>
                           <div class="position_R">
                    	   <input type="password" name="login_password" id="login_password" value="" class="loginrequired" placeholder="<?php echo lang('frntPassword'); ?>*" >
						
						  <label class="efformessage loginvalidate frontend_popup_error"><span class="errorarrow"></span><?php echo lang('frntRequiredMsg');?></label>
                         </div>
						
						<input type="hidden" name="event_id" id="event_id" value="<?php echo $eventId; ?>">
						  
                    <div class="spacer15"></div>
                    
                     <button id="" class="submitbtn_cus mt10 mr0 bg_741372" type="submit"><?php echo lang('frntLogin'); ?></button>
                     
                     <?php 
                      $curlink = $this->router->fetch_method();;
                     
                     if($curlink == 'sponsorbooths'){ ?>
						 
							<a href="<?php echo base_url().'sponsor/sponsorhome/'.encode($sponsorId);?>"><button id="closebtn" class="submitbtn_cus mt10 mr10 bg_741372" type="button"><?php echo 'Registration';?></button></a> 
                   	
					<?php }  ?>
                     
                     
                          
           </form>
         
        </div>
       </div>
      
    </div>
  </div>
</div>

<!-- close login popup code -->

<script type="text/javascript">
	
 $('.loginvalidate').hide();


/**
  code for submit form data 
*/
 
 $('.loadershow').hide();
 $('.contact_email').hide();
 $('.confirm_password_msg').hide();	

//for drop down list

$('.selectpicker').selectpicker(); 

	
</script>	
	
