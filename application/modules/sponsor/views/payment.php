<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$isEventExpired=(isset($is_event_expired) && !empty($is_event_expired))?$is_event_expired:''; 
$docPath=(isset($termConditionDetails) && !empty($termConditionDetails))?$termConditionDetails[0]->document_path:'';
$docFile=(isset($termConditionDetails) && !empty($termConditionDetails))?$termConditionDetails[0]->document_file:'';
     
//it display expiry year more than 19 year from current year.
$sponsorName 		 = (!empty($sponsorDetails->sponsor_name))?$sponsorDetails->sponsor_name:'';
$totalAmount		 = (!empty($sponsorDetails->total_price))?$sponsorDetails->total_price:'0';
$isApprovalRequest   = (!empty($sponsorDetails->is_approval_request))?$sponsorDetails->is_approval_request:'0';
$totalGST    	     = (!empty($sponsorDetails->gst))?$sponsorDetails->gst:'0';

$purchseBoothCount = 0;
$notPurchseBoothCount = 0;


if(!empty($sponsorBoothRequst)){
	foreach($sponsorBoothRequst as $sponsorBooth){
		if($sponsorBooth->payment_status==1){
		$purchseBoothCount++;
		}else{
			$notPurchseBoothCount++;
		}
	}
}

$totalAmount = $notPurchseBoothCount * $totalAmount;
//$totalAmount = $purchseBoothCount * $totalAmount;
$totalGST 	 = $purchseBoothCount * $totalGST;


?>
 
 <div class="accordion-group step2">
		<div class="accordion-heading">
			<div class="accordion-toggle" data-toggle="collapse" data-parent="#monogram-acc" href="#Payment">
			Step 3 - Payment
			</div>
		</div>
		<div class="accordion-body collapse" id="Payment">
		<div class="innerbody">
		
			
			<div class="tableheading position_R min-height44">
			<div class="printpara">Please confirm these details are correct.</div>
			<div class="printicon pull-right"></div>
			</div>
			
			
			 <div class="pymtregdetail">
				<table>
			      
				<tr class="headingrow">
					<th><?php echo lang('registrationDetailsMsg'); ?></th>
					<th><?php echo lang('frntTotalMsg'); ?></th>
					<th></th>
				</tr>
				 <tr id="registered_user">	 
				   <td></td>
				   <td></td>
				   <td></td>
				  </tr>
				   
				<tr id="paymentRegis" >
					<td colspan="3"></td>
			    </tr>
			    <tr id="sideeventPayment" >
					<td colspan="3"></td>
			    </tr>
			    
			    <tr id="breakoutrow" >
					<td colspan="3"></td>
			    </tr>
			    <tr id="subtotalrow" >
					<td colspan="3"></td>
			    </tr>
			    
			      
					<tr class="headingrow bg_lightblue">
					<th class="text-right"><?php echo lang('invoiceBalanceMsg'); ?></th>
					<th>$<span class="pay_amt"><?php //echo $totalAmount; ?>.00</span></th>
					<th></th>
					</tr>
					<tr>
					<td>
							<?php 
						  
							  if(!empty($sponsorBoothRequst)){
								foreach($sponsorBoothRequst as $booth){
								//this condition for not purchased booth	
								if($booth->payment_status==0){	
									 ?>
									     <a class="floreplane_btn selectbooth booth<?php echo $booth->id; ?>"><?php echo $booth->booth_position; ?></a>
             						 <input type="hidden" name="item[]"  value="<?php echo $booth->booth_id; ?>">		  
									 	
								  <?php									  
									} 
								 } 
							 }			     	
			     	      ?>			
					
					
					</td>
					<td>$<?php echo $totalAmount; ?>.00</td>
					<td>&nbsp;</td>
				</tr>
				
				<tr class="headingrow">	
					<th>purchased Booths</th>
					<th></th>
					<th></th>
				</tr>	
				<tr>
				<td><?php 
						  if(!empty($sponsorBoothRequst)){
								
							foreach($sponsorBoothRequst as $boothPurchased){
								
								//this condition for purchased booth
								if($boothPurchased->payment_status==1){	 
									
									if(isset($purchaseBooths) && !empty($purchaseBooths))
										{
											 if(in_array($boothPurchased->id,$purchaseBooths))
											 {
												 $purchasebooth=true;
										     }
										}
								 ?>
										 <a class="floreplane_btn pendingbooth " <?php  echo $boothPurchased->id; ?>"><?php echo $boothPurchased->booth_position; ?></a>
								  <?php	
								  
									} 
								 } 
							 }
			     	
			     	      ?></td>
				<td> <div class="spacer15"></div>
				
				
				
				
				</td>
				<td><div class="spacer15"></div></td>
				</tr>
					
					<tr class="headingrow bg_lightblue">
					<th class="text-right"><?php echo lang('gstIncludedMsg');  ?></th>
					<th>$<span class=""><?php echo $totalAmount; ?>.00</span></th>
					<th></th>
					</tr>
					
					<tr class="headingrow bg_lightblue">
					<th class="text-right"><?php echo lang('discountForGroupBookingMsg'); ?></th>
					<th>$0.00</th>
					<th></th>
					</tr>
					
					<tr class="headingrow">
					<th class="text-right"><?php echo lang('balanceOwingMsg'); ?></th>
					<th>$<span class="pay_amt"><?php echo $totalAmount; ?>.00</span></th>
					<th></th>
					</tr>
				
				 
			 </table>
			 
		
			 </div><!------ /payment registration detail ------>
			 
			 <div class="pymtregdetail mt20">
				<div class="tableheading_2 pt2pb2">
								<input type="checkbox" name="ex1" id="">
								<label class="paymentcond">I have read, understood and agreed to the 
								terms and conditions. <br><a href="#">Click here to read</a></label>    
				 </div>
				 
				 <div class="tableheading_2 pt2pb2 mt6">
								<input type="checkbox" name="ex1" id="">
								<label class="paymentcond">Send Invoice/Confirmation email to Registrant and Organiser.</label>    
				 </div> 
				 
			 </div>
			 
			 <div class="finapayment sideSgestcont pl0">
				<div class="heading">Payment</div>
			   
			   <div class="tableheading_2">
								<input type="radio" name="ex7" id="ex1_a">
								<label>Credit Card</label>
					</div>
					 <div class="formoentform_container pl25">
					<div class="position_R">
									<input type="text" name="card_name" value ="ravindra" placeholder="Credit Card Name">
					 </div>
					 
				  
							<div class="position_R mb10">	
								<input id="spinner" name="card_type" max="10" min="0" value="Visa" class="width100per">
							   </div>    
					  
						
						
					 <div class="position_R">
									<input type="text" name="card_number" value="4840225861310547" placeholder="Credit Card Number">
					 </div>
					 
					 <div class="position_R">
									<input type="text" name="ccv" value="111" placeholder="CCV" class="maxW70">
					 </div>
					 
					 <div class="position_R">
					 <div class="pull-left maxW70">
					 <input id="spinner" name="expire_month" max="10" min="0" value="09" class="width100per">
					 </div>
					 <div class="pull-left maxwidth100 ml25">
					 <input id="spinner" name="expire_year" max="2022" min="1990" value="2017" class="width100per">
					 </div>
					<div class="expiredate"> Expiry Date</div>
					 <div class="clearfix"></div> 
					 </div>
					 
					   <div class="clearfix"></div> 
					   <div class="spacer15"></div>
					 <div class="mb20">
					 <?php
					   //to check for appropaymentval password
					   if(isset($totalAmount) && !empty($totalAmount))
						{   
							$payAmount = array(
							'type'      =>'hidden',
							'name'      => 'pay_amount',
							'id'        => 'pay_amount',
							'value'     => $totalAmount,

							);	
							$regisAmount = array(
							'type'      =>'hidden',
							'name'      => 'regis_pay_amount',
							'id'        => 'regis_pay_amount',
							'value'     => $totalAmount,
							);
							echo form_input($regisAmount);
							echo form_input($payAmount);	
						}
					   
					  
					
			 $registration_included = $sponsorDetails->registration_included;
			
					  $attach_registration = $sponsorDetails->attach_registration;
					//  print_r($sponsorDetails);
					//  die;
					    $approvalPassword = ($isApprovalRequest=='1')?'approval_required':'';
					   
					 	$extraSave 	= 'class="frontendsubmitbtn mr0" ispyament="'.$registration_included.'" isattachregist="'. $attach_registration.'" id=""';
						echo form_submit('submit','SUBMIT & PAY',$extraSave);
					
					
					 ?>
					 
					 <div class="clearfix"></div> 
					 </div>
					 </div>
			 </div>			 
			</div>
			
		</div>
		</div>
		</div>
		</div>
		</div>
		 <!--<div class="span5 addbgcontainer pull-right spnncerresponsive" id="frontendright">
        	<div class="addbg"> <div class="addcontent">Advertisement</div></div>
            <div class="addbg"> <div class="addcontent">Advertisement</div></div>
        </div>-->
	
	  
<script type="text/javascript">	  


    var IMAGE='<?php echo IMAGE; ?>';
    $(document).ready(function() {
	  /* $.ajax({
		  type: "POST",
		  url: baseUrl+'sideevent/createpaymentform',
			success: function(data) {
			
			 $('#registered_user').after(data);
		    
			}
	   });*/
	});
	
	$('input:radio').screwDefaultButtons({
		image: 'url("'+IMAGE+'foontendradio.png")',
		width: 17,
		height: 17,
	});
	
	$('input:checkbox').screwDefaultButtons({
		image: 'url("'+IMAGE+'frontendcheck.png")',
		width: 17,
		height: 17,
	});	 
	//to hide loader 
	$('.loadershow').hide();
	//to make spinner		
	guestspinner();
	 $('.selectpicker').selectpicker(); 
	 $('#cheque_fields').hide(); 
     $('.credit_card_field').hide(); 
    
</script>	
	 
