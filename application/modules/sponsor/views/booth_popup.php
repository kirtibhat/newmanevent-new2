
<?php
//--------This popup used to confirmation of selected booth------------------------------------------------------//	
?>

<div class="modal fade frontendpopup" id="booth_popup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close closebtnfront" data-dismiss="modal" aria-hidden="true">&times;</button>
        <div class="commonS_circle ml0">
                	<div class="speakar_frontend"></div>
                </div>
        <h4 class="modal-title">Sponsor Booth</h4>
        <div class="clearfix"></div>
      </div>
       <div class="modal-body">
           You have selected to purchase booth number
       <div class="modelinner pl0 pr0">
        <a href="#" class="floreplane_popupbtn"><div id="select_booth_position"></div></a>
        <div class="clearfix"></div>
        <div class="spacer10"></div>
       		<form class="form-horizontal mt10">
                     <button id="closebtn" class="submitbtn_cus mt10 mr0 bg_741372 continueSelectedBooth" type="button" data-dismiss="modal" aria-hidden="true">CONTINUE</button>
                     <button id="closebtn" class="submitbtn_cus mt10 mr10 bg_741372 cancelSelectBooth" type="button" data-dismiss="modal" aria-hidden="true">CANCEL</button>
                     <button id="closebtn" class="submitbtn_cus mt10 mr10 bg_741372 back_btn" type="button" data-dismiss="modal" aria-hidden="true">BACK</button>

        	</form>
        </div>
       </div>
      
    </div>
  </div>
</div>
