<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
//--------personal details form---------//	
	
$sponsorId = decode($this->uri->segment(3));

	$registeredId=0; 
	$eventId = (isset($eventId) && !empty($eventId))?$eventId:'';
	$registeredId = (isset($selectRegi) && !empty($selectRegi))?$selectRegi:'';	
	$user_id=frntUserLogin('sponsor'); 
	
/*	$formSponsorPersonal = array(
		'name'	 => 'formSponsorPersonal',
		'id'	 => 'formSponsorPersonal',
		'method' => 'post',
		'class'  => 'form-vertical',
	);
	*/
	
	?>
                          
	<div class="accordion-group step2">
		    <div class="accordion-heading">
			<div class="accordion-toggle regi_form" data-toggle="collapse" data-parent="#monogram-acc" href="#sponsorPersonalDetails"  >
				<?php  echo  lang('step2PersonalDetailsHeading');?>
				
			</div>
		    </div>
		   </div> 
	    <div class="accordion-body collapse" id="sponsorPersonalDetails">
			<div class="innerbody">
			    <div class="paragraph"><?php echo lang('PleaseNoteThatFirstMsg'); ?></div>
			     <div class="mendotryfield"><?php echo lang('allFieldsWithMsg'); ?></div>

				 <div class="formheading"><?php echo lang('personalDetailsMsg');  ?></div>
								
				  <div class="formoentform_container">
				  <?php // echo form_open($this->uri->uri_string(),$formSponsorPersonal); ?>
				   <div id="form_fileds">
				 <?php 
				
				 $registration_included = $sponsorDetails->registration_included;
				 if($registration_included == 1){
				  $this->load->view('ajax_personal_detail_fields'); }?>
				   </div>
				     <?php // echo form_close(); ?>	  
				</div>
				
					<!-- close formoentform_container div -->
				<input type="hidden" name="countryName" id="countryName" value="">
				<input type="hidden" name="stateName" id="stateName" value="">
                 <input type="hidden" name="cityName" id="cityName" value="">
                 


				

		      
	    </div>
	</div>  
  <!-- close accordion-group div --> 
 
<script>
	
	//set master state list data by state selection
    $(document).on('change','#contact_country',function() {


		var getCountryName = $(this).find("option:selected").text();

		var countryId = $(this).val();
		var required=$('#state_required').val();
		
		sendData = {'countryId':countryId,'required':required};
		postUrl = 'sideevent/masterstatelist';
	
		doFrontendAjaxRequest(postUrl,sendData,'contact_state');
		

		//$('#state').html('<option valu="">Select State</option>');
		//$('#city').html('<option valu="">Select City</option>');
		
		$('.selectpicker').selectpicker(); 
		$('#countryName').val(getCountryName);
		

	});
   
	//set master city list data by state selection
	$(document).on('change','#contact_state',function() {
	//$('#contact_state').change(function(){
	    
	    var getStateName = $(this).find("option:selected").text();
 
		var stateId = $(this).val();
		var required=$('#state_required').val();
		sendData = {'stateId':stateId,'required':required};
		postUrl = 'sideevent/mastercitylist';
		doFrontendAjaxRequest(postUrl,sendData,'contact_city');
		$('#stateName').val(getStateName);
	});	
	 
    $(document).on('change','#contact_city',function() {
	//$('#contact_city').change(function(){
		
	    var getCityName = $(this).find("option:selected").text();
      
		var stateId = $(this).val();
		sendData = {'stateId':stateId};
		postUrl = 'sideevent/mastercitylist';
		doFrontendAjaxRequest(postUrl,sendData,'contactCity');
		$('#cityName').val(getCityName);
	});	
	
     
	
</script>

