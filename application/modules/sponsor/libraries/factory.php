<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 * This class is used to manage for sponsor controller logic and data
 * index ,	sponsorbooths,	savesponsorbooth,	sponsorhome,	savecontactperson,
 * sponsorLogin,	emailverification,	sponsorshippayment,	logout,	sponsorpass
 * sponsorpassrequest,	savepersonaldetails,	sponssortypepayment,	savebenifits
 */
 
 
class Factory{
	
	//set limit of show records 
	private $ci = NULL;
	
	function __construct($ci){
		//create instance object
		$this->ci = $ci;
	}
	
	/*
	* @access: public 
    * @description: This function is used for prepare index page for event 
    * @return: void
    *  
    */
	 public function index()
	{
		//to get event id from url
		$eventId =decode($this->ci->uri->segment(3));
		$user_id =frntUserLogin('sponsor');
		$eventId =eventId();
		 
		if($user_id)
		{
		// get user details for logged in user  
		$whereEventId=array('event_id'=>$eventId); 
		$data['userDetails']=$this->ci->common_model->getDataFromTabel('user','*',array('id'=>$user_id));
			if(!empty($data['userDetails']))
				{
				$data['userDetails'] = $data['userDetails'][0];
				}
		}
		//to check valid event id
			if(empty($eventId))
				{
				redirect(base_url('frontend/index/'));
				}
		//get sponsor details
		$sponsorDetails=$this->ci->common_model->getDataFromTabel('sponsor','*',array('event_id'=>$eventId));
			if(!$sponsorDetails)
				{
				$msg='No sponsor package available for this event.';
				$msgArray = array('msg'=>$msg,'is_success'=>'false');
				set_global_messages($msgArray);	
				redirect(base_url().'frontend/index/'.encode($eventId));
				}
		$data['sponsorDetails']=$sponsorDetails;
		//to show popup baground color
		$data['popupHeaderBg'] ='bg_00aec5';  //set popup bg color
		
		$data['sponsorSoldPackages']=$this->ci->sponsor_model->getSoldSponsorPackageIds($eventId);
	    $data['event_id']=$eventId; 
		
		// to get customize form details for hreader section
		$where=array('event_id'=>$eventId);
		$data['customize_form']=$this->ci->common_model->getDataFromTabel('customize_forms','*',$where);
		 
		$data['eventId'] = $eventId ;
	     
        //to load sponsor packages file
		$this->ci->default_template->load('default_template','sponsor_packages',$data,TRUE);
	}
	
	/*
	 * @access: public 
     * @description: This function is used for prepare sponsor booth view ,with additional item 
     * *benifits, booth availablity 
     * @return: void
     *  
    */
    public function sponsorbooths()
    { 
		$eventId = eventId();	
		// pass event id to view
		$data['eventId']= $eventId ;
	    //to get sponsor id  and decode encoded url id	
	    $sponsorId=decode($this->ci->uri->segment(3));
	    $data['sponsorId'] =  $sponsorId;	
	    $user_id=frntUserLogin('sponsor');
        $data['benifits'] = $this->ci->common_model->getDataFromTabel('sponsor_benefits','*',array('sponsor_id '=>$sponsorId));
		$data['popupHeaderBg'] ='bg_00aec5';  //set popup bg color
		//to get sponsorbooths
	    $data['sponsorbooths']= $this->ci->common_model->getDataFromTabel('sponsor_exhibitor_booths','*',array('sponsor_id '=>$sponsorId),'','','','','',true);
	    $data['sponsorsetbooths']= $this->ci->common_model->getDataFromTabel('sponsor_exhibitor_booths','*',array('sponsor_id '=>$sponsorId),'','','','','',true);
		//to get sponsorbooths details
	    $sponsorDetails = $this->ci->common_model->getDataFromTabel('sponsor','*',array('id '=>$sponsorId));
         
			if(!empty($sponsorDetails))
				{
				$data['sponsorDetails'] = $sponsorDetails[0];
				}
		
		$additionaltems = $this->ci->common_model->getDataFromTabel('sponsor_additional_purchase','*',array('sponsor_id '=>$sponsorId),'','','','','',$resultInArray=true);
        $getadditionaltems = $this->ci->common_model->getDataFromTabel('frnt_sponsor_additional_purchase','*',array('user_id '=>$user_id),'','','','','',$resultInArray=true);
		$data['getadditionaltems'] = $getadditionaltems;
			if(!empty($additionaltems))
				{
				$data['additionaltems'] = $additionaltems;
				}
        // get sponsor request sent data
		$sponsorreqstatus = $this->ci->common_model->getDataFromTabel('sponsor_exhibitor_booth_request','*',array('sponsor_id '=>$sponsorId ,'user_id'=> $user_id));
		//check sponsoro request check here
		$isRequestSent  = '0';
		$requestStatus  = '0';
			if(!empty($sponsorreqstatus))
				{
				$sponsorreqstatus = $sponsorreqstatus[0];
				$isRequestSent    = '1';
				$requestStatus   = $sponsorreqstatus->request_status;
				}
		$data['userreq']   = $isRequestSent;
		$data['reqstatus'] = $requestStatus;
		//to get select axhibitor booth
	    // $data['selectedbooths'] = $this->common_model->getDataFromTabelWhereWhereIn('frnt_sponsor_booth','booth_id,booth_status,user_id,payment_status',array('sponsor_id '=>$sponsorId),'booth_status',array('0','1'),'payment_status',array('0','1'));
	    $data['selectedbooths'] = $this->ci->common_model->getDataFromTabelWhereWhereIn('frnt_sponsor_booth','booth_id,booth_status,user_id,payment_status',array('sponsor_id '=>$sponsorId),'payment_status',array('0','1'),'','','array');
		$data['floorPlanPDF']=$this->ci->common_model->getDataFromTabel('exhibitor_floor_plan','floor_plan,floor_plan_path',array('event_id'=>$eventId));
		//to get user selected sponsor booth
		$data['userSelectedBooth']=$this->ci->sponsor_model->getUserSelectBooth($sponsorId);
		$data['userselectionlimit'] = count($data['userSelectedBooth']);
			if($this->ci->session->userdata('verify_pass'))
				{
				$data['verify_pass'] = "1";
				}
			else
				{
				$data['verify_pass'] = "0";
				}		
		// to get customize form details for hreader section
		$where=array('event_id'=>$eventId);
		$data['customize_form']=$this->ci->common_model->getDataFromTabel('customize_forms','*',$where);
		//to load exhibiotor booths file
		$this->ci->default_template->load('default_template','sponsor_booths',$data,TRUE);
  }  
  
	/*
	* @access: public 
    * @description: This function is used for save user selccted booth  
    * @return: void
    *  
    */
	public function savesponsorbooth()
    {
		 $boothIds=''; 
		 $userData='';
		 $sponsorId =''; 
		 $response='';
		 //get limit for booth purchasing	
		 $limit_package = $this->ci->input->post('limit_package');	 
		 // get selected booth IDs
		 $boothIds=$this->ci->input->post('boothIds');
		 $user_id=frntUserLogin('sponsor');
		 $sponsorId = $this->ci->input->post('sponsor_id');		
		
		//get purchase addtional item IDs and and their respective price
		$add_purch_ids = $this->ci->input->post('chkvals');	
		$chkprice = $this->ci->input->post('chkprice');	
		
			if(!empty($add_purch_ids)) 
				{
				foreach($add_purch_ids as $add_purch_id)
					{
					//saprate to recived mixed id and price 
					$id_price = explode('_',$add_purch_id);
									
					//echo $add_purch_id;						
					$data['user_id']=$user_id;
					$data['sponsor_id']=$sponsorId;
					//make explode data to use ready for query
					$data['add_purch_id']= $id_price[0];
					$data['total_price']= $id_price[1];
					//to add sponsor booth												 
					$userpurchase=$this->ci->common_model->addDataIntoTabel('frnt_sponsor_additional_purchase', $data);
					}	
				}
		if(isset($boothIds))
		{	 
		$selectedBooths = explode(',',$boothIds);
			if(!empty($selectedBooths))
			{
				foreach($selectedBooths as $boothId)
				{  
				//to get user sponsor booth details
				$userbooth= $this->ci->common_model->getDataFromTabelWhereWhereIn('frnt_sponsor_booth','booth_id',array('booth_id'=>$boothId),'booth_status',array('0','1'));
				//that booth for which user has already request or booth is exist in following table
				$userenteredbooth= $this->ci->common_model->getDataFromTabelWhereWhereIn('frnt_sponsor_booth','sponsor_id',array('sponsor_id'=>$sponsorId),'booth_status',array('0','1'));
				$userenteredbth = count($userenteredbooth);
				//check here if user is selecting booth that is more than set packega limit in sponsor table
				if($userenteredbth < $limit_package)
					{
					$msg='Sponsor booth submission has been sent sucessfully.';
					$bthdata['sponsor_id']=$sponsorId;
					//enter in this condition if purchase request not available for that booth in database
					if(empty($userbooth))
						{
						$bthdata['user_id']=$user_id;
						$bthdata['booth_id']=$boothId;
						$bthdata['booth_status']='0';
						//to add sponsor booth
						$userData=$this->ci->common_model->addDataIntoTabel('frnt_sponsor_booth', $bthdata);
						}									
					} 
				else 
					{
					$msg='Sponsor booth request limit has exceed.';
					$bthdata['sponsor_id']=$sponsorId;
					}
				}
			}
		}
		//remove user unselected booth
		// $response=$this->sponsor_model->removeSponsorBooth($sponsorId); 		
		$is_success='true';	
	    $msg='Sponsor booth request has been updated sucessfully.';
		if($userbooth!='')
			{
			$msg='Request limit is crossed .';
			}   
	    if($response)
			{
			$msg='Sponsor booth request has been removed sucessfully.';
			}
	    if($userData>0)
			{
			$msg='Sponsor booth request has been sent sucessfully.';
			}
		$msgArray = array('msg'=>$msg,'is_success'=>$is_success);
		set_global_messages($msgArray);
		echo json_encode($msgArray);		
	}
		
	/*
	 * @access: public 
     * @description: This function is used for prepare sponsor home view
     * with payment and personal detail view  
     * @return: void
     *  
    */
	public function sponsorhome()
		{
		//to show popup baground color
		$data['popupHeaderBg'] ='bg_00aec5'; 
		//to get loogedin user id
		$userId=frntUserLogin('sponsor');
		//get event id by current open event
		$eventId = eventId();
		$data['eventId'] = $eventId ;
		//get current opend sposnor id from url
		$sponsorId=decode($this->ci->uri->segment(3));
		//get sposnor details 
		$sponsorDetail = $this->ci->common_model->getDataFromTabel('sponsor','*',array('id'=>$sponsorId));
		//assing mutli dimentional array into varible 
		if(!empty($sponsorDetail))
			{
			$sponsorDetail =  $sponsorDetail[0];
			} 
		// get sponsor booth data for payment status "1" and "0"
		$sponsorBoothRequst = $this->ci->sponsor_model->sponsorboothrequstdata($sponsorId);
		//send sponsor data and sponsor booth requst data  to view		
		$data['sponsorDetails'] = $sponsorDetail;			
		$data['sponsorBoothRequst'] = $sponsorBoothRequst;
		//to get loggedin user details
		$userDetails = $this->ci->common_model->getDataFromTabel('user','*',array('id'=>$userId));
		//assign  multiple array into single varible
		if(!empty($userDetails))
			{
			$userDetails = $userDetails[0];
			}     
		
		//get personal deatils data by call
		$data['personalfielddata']  =  $this->_getpersonalfields($sponsorId);

		// to get customize form details for hreader section
		$where=array('event_id'=>$eventId);
		$data['customize_form'] =$this->ci->common_model->getDataFromTabel('customize_forms','*',$where);
		//set user id,user details, and sponsor idfor view
		$data['userId'] = $userId;
		$data['sponsorId'] = $sponsorId;
		$data['userDetails'] = $userDetails;
		//to load exhibiotor booths file
		$this->ci->default_template->load('default_template','sponsor_home',$data,TRUE);
		}
	
	/*
	 * @access: private 
     * @description: This function is used to help for prepare sponsor home view
     * with payment and personal detail view  and called by sponsor home function
     * @return: void
     *  
    */	
	private function _getpersonalfields($sponsorId="0"){
		
		//get sponsor data by sponsor id
		$sponsorWhere=array('id'=>$sponsorId);
		$sponsorData=$this->ci->common_model->getDataFromTabel('sponsor','*',$sponsorWhere);
		
		if(!empty($sponsorData))
			{
			$sponsorData   =   $sponsorData[0];
			$eventId 	   =   $sponsorData->event_id; // get event id from sponsor table
			$registrantId  =   $sponsorData->attach_registration; // get registrant id from sponsor table
		
			}
		else
			{
			return true;
			}
		//to get loggedIn user Id
        $user_id=frntUserLogin('sponsor');
		// define field data variable	
		$filed_data='';
		//to get custom form fileds for this form and eventId
		$where=array('event_id'=>$eventId,'registrant_id'=>$registrantId);
		$fields=$this->ci->common_model->getDataFromTabelWhereWhereIn('custom_form_fields','*',$where,'field_status',array('1','2'));
		if(!empty($fields))
		{ 
		$fieldName='';
		$placeholder='';
		foreach($fields as $field)
		{ 
			$registeredId = frntUserLogin('sponsor');
			$fieldValue='';
			if($registeredId>0)
				{
				//to get form fields values
				$where=array('field_id'=>$field->id,'registered_user_id'=>$registeredId);
				$registeredDetails=$this->ci->common_model->getDataFromTabel('frnt_form_field_value','*',$where);
				if($registeredDetails)
					{
					$fieldValue=$registeredDetails[0]->value;
					}
				}
			// to create placeholder of field
			$placeholder=ucfirst($field->field_name);
			$fieldName=strtolower(str_replace(' ','_',$field->field_name));
			$required='';
			if($field->field_status=='2')
				{
				$required='required_field';
				$placeholder=ucfirst($field->field_name.'*');
				}
			//to add country falg for fax & mobile phone
			if($fieldName=='mobile_phone' || $fieldName=='fax')
				{
				$required=$required.' flag_text_field codeFlag';
				}
			$default=$field->default_value;
			$data['defaultArray']=json_decode($default);
			
			// to send data to create personal details form for user
			$data['field_id']=$field->id;
			$data['field_name']=$fieldName;
			$data['field_value']=$fieldValue;
			$data['place_holder']=$placeholder;
			$data['field_required']=$required;
			$data['field_type']=$field->field_type;
			// $data['mastercountrydata']=$mastercountrydata;
			//$data['statedata']=$statedata;
			
			//to add html content for personal details form
			$filed_data[]=$data;
		}
		// end of the first foreach 
		return $filed_data;
		}
	 } 
		
	/*
	 * @access: public 
     * @description: This function is used to save upper contact person form in sponsor home 
     * 
     *  
    */
    public function savecontactperson()
	{
	//to check post of the form data 
    $post=$this->ci->input->post();         
	if($post && isset($post['contact_first_name']))
		{
		 $eventId=$this->ci->input->post('regis_event_id');
		 $user_id=frntUserLogin('sponsor');			 
		 //check contact form fields validation 
		 $this->ci->sponsor_model->checkContactFormFields(); 
			 
		 // to get from data in array
		 $userDetail['username'] =$this->ci->input->post('contact_email');
		 $userDetail['user_title'] =$this->ci->input->post('contact_user_title');
		 $userDetail['firstname'] = $this->ci->input->post('contact_first_name');
		 $userDetail['lastname'] = $this->ci->input->post('contact_surname');
		 $userDetail['email'] =$this->ci->input->post('contact_email');
		 $userDetail['phone'] =$this->ci->input->post('contactphone');
		 $userDetail['mobile'] =$this->ci->input->post('contact_mobile');
		 $userDetail['company_name'] =$this->ci->input->post('contact_cmp_name');
		 $userDetail['position'] =$this->ci->input->post('contact_position');
			  
		if(strlen($this->ci->input->post('contact_password'))>0)
			{
			$userDetail['password'] = md5($this->ci->input->post('contact_password'));
			}
		// to check user login bu id then update form details
		if($user_id)
			{
			$whereUserId=array('id'=>$user_id);
			$this->ci->common_model->updateDataFromTabel('user', $userDetail, $whereUserId);
			echo json_encode(array('msg' => 'Update successfully.', 'is_success'=>'1'));
			die();
			}
		else
			{
			$userDetail['event_id']=$eventId;
			$userDetail['user_type_id'] ='3';
			//to add data in user table and get user id
			$user_id=$this->ci->common_model->addDataIntoTabel('user', $userDetail);
			//send email to registered user for email verification
			$from=$this->ci->config->item('admin_email');
			$to=$this->ci->input->post('contact_email');
			$subject='Email varification';
			$siteurl=base_url().'sponsor/emailverification/'.encode($user_id);
			$emailData = array(
			'emailId'=>$to,
			'password'=>$this->ci->input->post('contact_password'),
			'activationURl'=>$siteurl,
			);
			//function to send mail
			sendMail($from, $to, $subject,$emailData);
			$msg='Add successfully. Please check your email for verification.';
			$msgArray = array('msg'=>$msg,'is_success'=>'true');
			set_global_messages($msgArray);
			echo json_encode(array('msg' => '', 'is_success'=>'true'));
			die();
			}
		echo json_encode(array('msg' => 'Failed , Please Try Again.', 'is_success'=>'false'));  
		die();
		}
    }
        
    /*
     * @access: public 
     * @description: This function is used to login user in sponsore section  
     * 
     *  
    */	
	public function sponsorLogin()
		{
			
		$usertype = $this->ci->config->item('sponsor_user_type');		
		$validuser = FALSE;
		 $this->ci->frnt_auth->login($usertype);
		
    }
    
    /*
     * @access: public 
     * @description: This function is used to check that email is verify or not  
     * 
     *  
    */	
    
    public function emailverification()
		{
		$userId = decode($this->ci->uri->segment(3));		
		//get user details on the basis of user id
		$whereArray=array('id'=>$userId);
		$userdetails = $this->ci->common_model->getDataFromTabel('user','event_id,email_verify',$whereArray);
  
		if($userdetails)
			{
			//to check account already verified
			if($userdetails[0]->email_verify==1)
				{
				$msg='Your email address already verified.';
				$msgArray = array('msg'=>$msg,'is_success'=>'true');
				set_global_messages($msgArray);
				redirect(base_url().'frontend/index/'.encode($userdetails[0]->event_id));
				}
			//to update status for verified email user
			$data['email_verify']='1'; 
			$this->ci->common_model->updateDataFromTabel('user',$data,array('id'=>$userId));
					
			//set message  
			$msg='Email address has been verified successfully.';
			$msgArray = array('msg'=>$msg,'is_success'=>'true');
			set_global_messages($msgArray);
			redirect(base_url().'frontend/index/'.encode($userdetails[0]->event_id));
			}
		redirect(base_url('frontend/index/'));
	}
	
	 /*
	 * @access: public 
     * @description: This function is used to load sponsor payment popup  
     * 
     *  
    */
	public function sponsorshippayment()
		{
		 $this->ci->load->view('sponsorship_payment_popup');
	
	}
	
	 /*
	 * @access: public 
     * @description: This function is used to logout 
     *  
    */	
	public function logout()
    {
	$this->ci->session->unset_userdata('userid');
	$this->ci->session->unset_userdata('username');
	$this->ci->session->unset_userdata('firstname');
	$this->ci->session->unset_userdata('sponsorid');
	//$this->session->sess_destroy();
	redirect(base_url('frontend/index/'));
	return true;	 
	}	
	
	/*
	 * @access: public 
     * @description: This function is used to to verify booth purchaseing passoword 
     *  
	 */	
	public function sponsorpass()
	{
	//encreapt pass 
	$getPassword=$this->ci->input->post('sponsor_password');			
	$sponsorid = $this->ci->input->post('sponsorid');		
    $eventId=$this->ci->input->post('event_id');            
	//get sponsor details
	$where=array('id' => $sponsorid );
	$userDetails=$this->ci->common_model->getDataFromTabel('sponsor','*',array('id' => $sponsorid));		   
	$dbpasword = $userDetails[0]->password;				
	if($dbpasword == $getPassword)
		{   
		$data = array('verify_pass' => TRUE );
		$data['verify_pass'] = $this->ci->session->set_userdata($data);					
		$msg='Verified successfully.';
		$msgArray = array('msg'=>$msg,'is_success'=>'true');
		set_global_messages($msgArray);
		echo json_encode(array('msg' => $msg, 'status' => '1'));
		}
	else 
		{
		echo json_encode(array('msg' => 'Wrong password.', 'status' => '0'));
		die;
		}
	 }
	 
	 /*
	 * @access: public 
     * @description: This function is used to to request booth purchaseing passoword 
     *  
    */	
	public function sponsorpassrequest() 
		{
		$data['event_id']	=getEventId('sponsorid');
		$data['sponsor_id'] =$this->ci->input->post('sponsor_id');
		$data['request_type']=$this->ci->input->post('request_type');
        // form data of new sponsor booth request  
		$data['name']=$this->ci->input->post('sponsor_pass_name');
		$data['email']=$this->ci->input->post('sponsor_pass_email');
		$data['message']=$this->ci->input->post('sponsor_pass_message');	
		$data['user_id']=frntUserLogin('sponsor');          
		$userpassrequest=$this->ci->common_model->addDataIntoTabel('sponsor_exhibitor_booth_request', $data);
		if($userpassrequest)
			{  					
			$msg='your Request has been sent successfully.';
			$msgArray = array('msg'=>$msg,'is_success'=>'true');
			set_global_messages($msgArray);
			echo json_encode(array('msg' => '', 'status' => '1'));
			}
		else
			{
			echo json_encode(array('msg' => 'Please Try Again.', 'status' => '0'));
			die;
			}
		}
		
	/*
	 * @access: public 
     * @description: This function is used to to save perosonal detail form  
     *  
    */		
	public function savepersonaldetails()
		{
		//to check post of the form data 
		$post=$this->ci->input->post(); 
		if($post && isset($post['contact_first_name']))
			{
			$eventId=$this->ci->input->post('regis_event_id');
			$user_id=frntUserLogin('sponsor');
			//check contact form fields validation 
			$this->ci->sponsor_model->checkContactFormFields(); 
			// to get from data in array
			$userDetail['username'] =$this->ci->input->post('contact_email');
			$userDetail['user_title'] =$this->ci->input->post('contact_user_title');
			$userDetail['firstname'] = $this->ci->input->post('contact_first_name');
			$userDetail['lastname'] = $this->ci->input->post('contact_surname');
			$userDetail['email'] =$this->ci->input->post('contact_email');
			$userDetail['phone'] =$this->ci->input->post('contactphone');
			$userDetail['mobile'] =$this->ci->input->post('contact_mobile');
			$userDetail['company_name'] =$this->ci->input->post('contact_cmp_name');
			$userDetail['position'] =$this->ci->input->post('contact_position');
		if(strlen($this->ci->input->post('contact_password'))>0)
			{
			$userDetail['password'] = md5($this->ci->input->post('contact_password'));
			}			   
			// to check user login bu id then update form details
		if($user_id)
			{
			$whereUserId=array('id'=>$user_id);
			$this->ci->common_model->updateDataFromTabel('user', $userDetail, $whereUserId);
			echo json_encode(array('msg' => 'Update successfully.', 'is_success'=>'1'));
			die();
			}
		else
			{
			$userDetail['event_id']=$eventId;
			$userDetail['user_type_id'] ='3';
			//to add data in user table and get user id
			$user_id=$this->ci->common_model->addDataIntoTabel('user', $userDetail);
			//send email to registered user for email verification
			$from=$this->ci->config->item('admin_email');
			$to=$this->ci->input->post('contact_email');
			$subject='Email varification';				
			$siteurl=base_url().'sponsor/emailverification/'.encode($user_id);				
			$emailData = array(
			'emailId'=>$to,
			'password'=>$this->ci->input->post('contact_password'),
			'activationURl'=>$siteurl,
			); 
			//function to send mail
			sendMail($from, $to, $subject,$emailData);
			$msg='Add successfully. Please check your email for verification.';
			$msgArray = array('msg'=>$msg,'is_success'=>'true');
			set_global_messages($msgArray);
			echo json_encode(array('msg' => '', 'is_success'=>'true'));
			die();
			}
			echo json_encode(array('msg' => 'Failed , Please Try Again.', 'is_success'=>'false'));  
			die();		
			}
		}
		
	/*
	 * @access: public 
     * @description: This function is used to to save perosonal detail form in sponsor home section  
     *
	 */
	public function savessponsorpersonal()
		{ 
		$regi_user_id='';	  
		// to get user id if user login and take user event id 
		$user_id=frntUserLogin('sponsor');		
		// to get event_id of login user
		$eventId = eventId();  	    
		// to check post data
		$post=$this->ci->input->post();	
		$requiredfield = $this->ci->input->post('email');
		if($requiredfield == '')
			{
			return false;
			}
		else
			{
			$sponsorId = $this->ci->input->post('sponsorId');
			$sponsorWhere=array('id'=>$sponsorId);
			$sponsorData=$this->ci->common_model->getDataFromTabel('sponsor','*',$sponsorWhere);
	
			if(!empty($sponsorData))
				{
				$sponsorData   =   $sponsorData[0];
				$eventId 	   =   $sponsorData->event_id; // get event id from sponsor table
				$registrantId  =   $sponsorData->attach_registration; // get registrant id from sponsor table
				}			
			//save  registrant in  table
			$regi_user_id=$this->ci->sponsor_model->saveSelectRegistrant($registrantId);
			$userdata=$this->ci->sponsor_model->savePersonalDetailsFormValue($regi_user_id,'save', $registrantId);
			return true;
		}
	}
	
	/*
	 * @access: public 
     * @description: This function is used to to save perosonal detail form with payment by calling above function in sponsor home section  
     *  
    */ 
	public function sponssortypepayment()
		{
		if($this->savessponsorpersonal()==false)
		{
			
			$msg='please fill personal detail form.';
			$msgArray = array('msg'=>$msg,'is_success'=>'false');
			set_global_messages($msgArray);			 
			echo json_encode(array('msg' =>'', 'status'=>'1'));
			return true;
		}
		$user_id=frntUserLogin('sponsor');	        
		$eventId=getEventId('sponsorid');        
        $userDataArray=array();         	
        $post=$this->ci->input->post();
     
		if(isset($post['card_type']) && isset($post['card_number']) && isset($post['ccv']))
			{ 				      
				//$userDataArray['amt']=$this->ci->input->post('pay_amount');
				$userDataArray['amt']=$this->ci->input->post('pay_amount');
				$userDataArray['card_type']=$this->ci->input->post('card_type');
				$userDataArray['card_number']=$this->ci->input->post('card_number');
				$userDataArray['exp_date']=$this->ci->input->post('expire_month').$this->ci->input->post('expire_year');
				$userDataArray['ccv']=$this->ci->input->post('ccv');						
			} 	 
		$pamentOption = $this->ci->common_model->getDataFromTabel('payment_gateway','merchant_id',array('event_id'=>$eventId));
	   	if(!empty($pamentOption))
			{			  
				$userDataArray['merchant_id']=$pamentOption[0]->merchant_id;
				//to payment by paypal from payment pro library	
				$this->ci->payments_pro->do_direct_payment($userDataArray);
				return true;			 
			}
		else
			{	
				$msg=lang('merchantIdMsg').'Please try again.';
				$msgArray = array('msg'=>$msg,'is_success'=>'false');
				set_global_messages($msgArray);			 
				echo json_encode(array('msg' =>'', 'status'=>'1'));
				return true;
			}	
	}
	
	/*
	 * @access: public 
     * @description: This function is used to to save user selected benifits in sponsor home section  
     *  
    */

	public function savebenifits()
		{
		$post=$this->input->post();		
		$getFieldId 		= $this->ci->input->post('getFieldId');
		$checkboxCheccked	= $post['checkboxCheccked'];
		$sponsor_id			= $post['sponsor_id'];
		$user_id			= $post['user_id'];		
		
		if($checkboxCheccked==1)
			{
				$data['user_id']=$user_id;
				$data['sponsor_id']=$sponsor_id;
				$data['benifit_id']=$getFieldId;
				//to add sponsor booth
				$saveData=$this->ci->common_model->addDataIntoTabel('frnt_add_benifits', $data);
			}
		else
			{
				$response=$this->ci->common_model->deleteRowFromTabel('frnt_add_benifits',array('benifit_id'=>$getFieldId,'user_id'=>$user_id));
			} 
	}
/* End of file factory.php */
/* Location: ./system/application/modules/sponsor/libraries/factory.php */
}
