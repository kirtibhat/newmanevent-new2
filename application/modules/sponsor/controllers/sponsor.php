<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * @description: This controller is used to manage login, registration
 * @auther: Rajendra aptidar
 * @email: rajendrapatidar@cdnsol.com
 * 
 */ 
 
class Sponsor extends MX_Controller{

	function __construct(){
		parent::__construct();
		$this->load->library('factory',$this);
		$this->load->library(array('default_template','payments_pro','head'));
		$this->load->model(array('sponsor_model'));
		$this->form_validation->set_error_delimiters('<div class="server_msg_show">', '</div>');
		$this->load->language(array('frontend_sponsor','common'));
		$this->load->helper('frontend');
		$this->load->library('frnt_auth',$this);
	}
	
//-----------------------------------------------------------------------------------------------
	/*
	 * @access: public
	 * @description: This function is used for Registration for event 
     * @return: void
     *  
	 */

    public function index()
    {	
		$this->factory->index();
	}
	
//-----------------------------------------------------------------------------------------------
	
	/*
	* @access: public
	* @description  : This function is used to get sponsor package booths
    * @return        : booths for sponsor
    * @param         : sponsorid 
    */
    
    public function sponsorbooths()
    { 
		$this->factory->sponsorbooths();
	}	
	
//-----------------------------------------------------------------------------------------------
	/*
	* @access: public  
	* @description  : This function is used to save sponsor booths	* 
    * @return        : NULL
    * @param         : boothId
    */
    
    public function savesponsorbooth()
    {
		$this->factory->savesponsorbooth();
	}
	
//-----------------------------------------------------------------------------------------------
	/* 
	 * @access: public
	 * @description  : This function is used to load sponsor home
     * @return        : void
     * @param         : sponsorId 
     */
    
	public function sponsorhome()
	{
		$this->factory->sponsorhome();
	}

//-----------------------------------------------------------------------------------------------
	/* 
	 * @access: public
	 * @description  : This function is used to save conatct person detail
     * @return       : void
     */
    
    public function savecontactperson()
    {
		$this->factory->savecontactperson(); 
    }
    
//-----------------------------------------------------------------------------------------------
	/*
	* @access: public 
	* @description: This function is used to  sponsor login
    * @return:message
    */
    public function sponsorLogin()
    {
		$this->factory->sponsorLogin();	 
    }

//-----------------------------------------------------------------------------------------------
	/*
	* @access: public 
	* @description: This function is used to varificatrion of sponsor email
	* @return     : void
	*
	*/
	public function emailverification()
	{
		$this->factory->emailverification();
	}
	
//-----------------------------------------------------------------------------------------------
	/*
	 * @access: public
     * @description: This function is used to payment for sponsorship
     * @return: void
     *  
     */
	public function sponsorshippayment()
	{
		$this->factory->sponsorshippayment();
	}
 
//-----------------------------------------------------------------------------------------------
	/*
	 * @access: public 
     * @description: This function is used to logout
     * @return: void
     *  
     */
    public function logout()
    {
		$this->factory->logout();
	}
	
//-----------------------------------------------------------------------------------------------
	/*
	 * @access: public
     * @description: This function is used to password confirmation popup for booth purchasing
     * @return: void
     *  
     */ 
	public function sponsorpass()
    {
		$this->factory->sponsorpass();
    }
    
//-----------------------------------------------------------------------------------------------
    /*
     * @access: public 
     * @description: This function is used to request for password or approval for booth purchasing
     * @return: void
     *  
     */     
	public function sponsorpassrequest()
    {
		$this->factory->sponsorpassrequest();
    }
    
//-----------------------------------------------------------------------------------------------    
	/*
	 * @access: public 
     * @description: This function is used to save personal detail 
     * @return: void
     *  
     */ 
    public function savepersonaldetails()
    {
		$this->factory->savepersonaldetails();
    }
    
//-----------------------------------------------------------------------------------------------  
	/*
	 * @access: public 
     * @description: This function is used to make payment with pesonal detail saving
     * @return: void
     *  
     */
	public function sponssortypepayment()
	{
		$this->factory->sponssortypepayment();
	}
 
//-----------------------------------------------------------------------------------------------
	/*
	 * @access: public 
     * @description: This function is used to save user selected  benifits in sponsor booth 
     * @return: void
     *  
     */
	public function savebenifits()
	{ 
		$this->factory->savebenifits();
	}

//-----------------------------------------------------------------------------------------------
}

/* End of file sponsor.php */
/* Location: ./system/application/modules/sponsor/sponsor.php */

?>
