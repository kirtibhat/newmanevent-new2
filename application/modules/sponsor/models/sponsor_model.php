<?php

class Sponsor_model extends CI_model{
	
	
	private $tablenmeventregistrants='event_registrants';
    private $tablenmuser='user';
    private $tablenmevent= 'event';
	
	function __construct()
	{
	parent::__construct();
	}
	
	//..........................................................................................................
	/* @description  : This function is used to remove sponsor booths
    * @return        : true/false
    * @param         : sponsorId 
    */
    
    function removeSponsorBooth($sponsorId)
    {
		$response=false;
		$user_id=frntUserLogin('sponsor');
		$post=$this->input->post();
		if(!isset($post['boothIds']))
		{
		redirect(base_url('frontend/index/'));
		}
		$boothIds=$this->input->post('boothIds');
		$selectedBooths=explode(',',$boothIds);
		
		//get user booth 
		$userbooths= $this->common_model->getDataFromTabelWhereWhereIn('frnt_sponsor_booth','booth_id',array('sponsor_id'=>$sponsorId,'user_id'=>$user_id),'booth_status',array('0','2'));
		
		if(!empty($userbooths))
		{
			foreach($userbooths as $booth)
			{
				if(!in_array($booth->booth_id,$selectedBooths))
				{	//remove booth 
				 $response=$this->common_model->deleteRowFromTabel('frnt_sponsor_booth',array('booth_id'=>$booth->booth_id,'user_id'=>$user_id));
				}
			}
		}
		return $response;			 
	}
    
    //..........................................................................................................
	/* @description  : This function is used to check sold package.
    * @return        : true/false
    * @param         :  eventId
    */
    function getSoldSponsorPackageIds($eventId)
    {
		 //create array for purchase packages
		$purchasePackages=array();		 
		$user_id=frntUserLogin('sponsor');
		 		 
		//get sponsor package by eventId
		$sponsors=$this->common_model->getDataFromTabel('sponsor','id',array('event_id'=>$eventId));
    
		if(!empty($sponsors))
		{	
			foreach($sponsors as  $sponsor)
			{
				 $issoldPackage='yes';
				 $selectedBooths=$this->common_model->getDataFromTabel('frnt_sponsor_booth','booth_id,booth_status',array('sponsor_id'=>$sponsor->id));
				if(!empty($selectedBooths))
				{
					foreach($selectedBooths as $booth)
					{
					//check approved booth
					if($booth->booth_status!='1')
					{    
					  $issoldPackage='no';
					  break;
					}	
					}				
					if($issoldPackage=='yes') 
					{   
					//get sponsor package id
					$purchasePackages[]=$sponsor->id;
					}
			    }
			} // end of the outer loop
		} return $purchasePackages;	
	}
	
	//..........................................................................................................

	/* @description  : This function is used to get user select  sponsor booths
    * @return        : select booth ids
    * @param         : sponsorId
    */
     function getUserSelectBooth($sponsorId)
     {
		//to create array to take user selected booth ids
		$boothIdArray=array();
		//to get user select sponsor booths
		$user_id=frntUserLogin('sponsor');
	    $selectedbooths= $this->common_model->getDataFromTabelWhereWhereIn('frnt_sponsor_booth','booth_id',array('sponsor_id '=>$sponsorId,'user_id'=>$user_id),'booth_status',array('0','1'));
	    
	    if(!empty($selectedbooths))
	    {
		  foreach($selectedbooths as $booth)
		  {
		   $boothIdArray[]=$booth->booth_id;
		  }
		}
		return $boothIdArray; 
	}
    
	//..........................................................................................................

	/*@description  : This function is used to check personal contact form fields
    * @return        : Validation message
    * @param         :  
    */
    
    function checkContactFormFields()
    {
		
		 $eventId=$this->input->post('regis_event_id');
		 $user_id=frntUserLogin('sponsor');	  
		
		 $this->form_validation->set_rules('contact_first_name', 'contact first name', 'trim|required');
		 $this->form_validation->set_rules('contact_surname', 'contact surname', 'trim|required');
		 $this->form_validation->set_rules('contactphone', 'contact phone', 'trim|required');
		 $this->form_validation->set_rules('contact_cmp_name', 'contact Company name', 'trim|required');
		 $this->form_validation->set_rules('contact_position', 'contact position', 'trim|required');
			 if(!$user_id)
			 {
			 $this->form_validation->set_rules('contact_email', 'contact email', 'trim|required');
			 $this->form_validation->set_rules('contact_password', 'contact Password', 'trim|required');
			 $this->form_validation->set_rules('contact_confirm_password', 'contact Confirm Password', 'trim|required');
			 }

		if (!$this->form_validation->run())
		 {    
		 echo json_encode(array('msg' => validation_errors(), 'is_success'=>'false'));
		 die;
		 }

			if($this->input->post('contact_password')!=$this->input->post('contact_confirm_password')){
			 echo json_encode(array('msg' => 'Password Not Match', 'is_success'=>'false'));
			 die;
			}
		 // to check email exist for this event
		 $email = $this->input->post('contact_email');
		 $is_exist =$this->common_model->getDataFromTabel('user','id',array('email'=>$email,'event_id'=>$eventId,'user_type_id'=>'3'));
			 if ($user_id=='' && !empty($is_exist)) 
			 {
			 echo json_encode(array('msg' => 'Email Already Used Please Try Another.', 'is_success'=>'false'));
			 die;
			 }
	}


	
  function saveSelectRegistrant($registrantId)
    {
		
	// to save day wise registrant in nm_event_registrant_daywise table
	$userData='';
	//$daywiseEarlyBird='';
	$user_id=frntUserLogin('sponsor');
	//$daysid= explode(',',$regisDayId);
	// $days=$daysid[0];
	$regi['user_id']=$user_id;
	$eventId = eventId();		
	$regi['registrant_id']=$registrantId;
	$regi['event_id']=$eventId;  // add current event id  
	// to save or update selected registrant in  nm_frnt_registrant table
	//to get 12 digint number as operand_number (organizer_number),event_number, registrant_number
	$bookinNumber =  $this->getDigitNumber($registrantId,$eventId);
	$registrationNumber = getMasterNumber('registration',$eventId); //get event serial number
		if(!empty($bookinNumber))
		{				
		//it create sixteen digit number as operand_number (organizer_number),event_number, registrant_number and registrationNumber
		$regi['registration_booking_number']=$bookinNumber->operand_number.'-'.$bookinNumber->event_number.'-'.$bookinNumber->registrant_number.'-'.$registrationNumber;
		//to save registration number
		$regi['registration_number'] =$registrationNumber;
		}
	$regi['registrants_type']='1';
	//save new registrant data in frnt_registrant table
	$userData=$this->common_model->addDataIntoTabel('frnt_registrant', $regi);		  
	return $userData;
	}


function savePersonalDetailsFormValue($regis_user_id,$condition='',$registrantId,$registered_id='')
	{   
         
		 $user_id=frntUserLogin('sponsor');
		 $userData='';
	     $add='0';
		//to get eventId
		$eventId = eventId();
		// to get dynamic form for id,name
		$formDetails=$this->common_model->getDataFromTabel('form','id,name');
		if($formDetails)
		{
			foreach($formDetails as $form)
			{   //get custom form fields value
				$formwhere=array('event_id'=>$eventId,'registrant_id'=>$registrantId,'form_id'=>$form->id);
				$fieldDetails=$this->common_model->getDataFromTabelWhereWhereIn('custom_form_fields','*',$formwhere,'field_status',array('1','2'));
			if($fieldDetails)
				{
					foreach($fieldDetails as $field)
					{
					$fieldName = str_replace(' ','_',$field->field_name);
					$fieldValue =$this->input->post($fieldName);
					$formdata['field_id'] = $field->id;
					$formdata['user_id'] =$user_id;
					$formdata['form_id'] = $form->id;
					//to convert in json formate and add country ,city, state
					if($fieldName=='address')
					{
						$address2=$this->input->post('address');
						$country=$this->input->post('contact_country');
						$state=$this->input->post('contact_state');
						$city=$this->input->post('contact_city');
						//to get name of country
						$countryName=$this->input->post('countryName');
						$stateName=$this->input->post('stateName');
						$cityName=$this->input->post('cityName');
						$addressArray = array('address1' =>$fieldValue, 'address2' =>$address2, 'country' =>$country, 'state' =>$state, 'city' =>$city);
                        $addressNameArray=array('address1'=>$fieldValue,'address2' =>$address2,'cityName'=>$cityName,'stateName'=>$stateName,'countryName'=>$countryName);
                        $regsultArray=array('addressId'=>$addressArray,'addressName'=>$addressNameArray);
                        $address= json_encode($regsultArray); 
						$formdata['value'] = $address;
						}
						else
						{
						$formdata['value'] = $fieldValue;
						}
						if($condition=='save')
						{
						//add personal form fields value
						$formdata['registered_user_id']=$regis_user_id;
						$userData= $this->common_model->addDataIntoTabel('frnt_form_field_value', $formdata);
						}
						else
						{
						$fieldDetails=$this->common_model->getDataFromTabel('frnt_form_field_value','id',array('field_id'=>$field->id,'registered_user_id'=>$registered_id));
                        if(empty($fieldDetails) && $add=='0')
                        {
						$add='1';
						$this->common_model->deleteRowFromTabel('frnt_form_field_value',array('registered_user_id'=>$registered_id));
						} 
						if($add=='0')
						{
						// update fields value
						 $whereId=array('field_id'=>$field->id,'registered_user_id'=>$registered_id);
						 $userData=$this->common_model->updateDataFromTabel('frnt_form_field_value',$formdata,$whereId);
						 }
						else
						{
						$formdata['registered_user_id'] = $registered_id;
						$this->common_model->addDataIntoTabel('frnt_form_field_value',$formdata);
						}
					  }
					}
				}
			}//end outer loop
		}
		return $userData; 
	}
 
 
 function savePaymentDetails($proArray)
	{
		$user_id=frntUserLogin('sponsor');
		//get event id
		$eventId=getEventId('sponsorid');
			if(count($proArray)>0 && $proArray['ACK']=='Success')
			{
			$bookingId=randomnumber('8');
			$data['total_amount']=$proArray['AMT'];
			$data['received_amount']=$proArray['AMT'];
			$data['booking_id']=$bookingId;
			$data['payment_type']='credit_card';
			$data['transaction_id']=$proArray['TRANSACTIONID'];
			$data['payment_date']=date('Y-m-d H:s:i');
			$data['user_id']=$user_id;
			//insert payment details in payment table
			$paymentId=$this->common_model->addDataIntoTabel('frnt_payment_details', $data);
			//update payment id in sponsor booth table
			$transaction_id = $proArray['TRANSACTIONID'];
			$getpayment_Id = $this->common_model->getDataFromTabel('frnt_payment_details','id',array('transaction_id'=>$transaction_id));
			$boothId = $this->input->post('item');			
			$getIdpayment = $getpayment_Id['0']->id;			
			$boothpaymentid= array('payment_id' => $getIdpayment ,'payment_status' => 1 );
			$addpaymentid = $this->common_model->updateDataFromTabelWhereIn('frnt_sponsor_booth', $boothpaymentid ,array('booth_status' =>1),'booth_id',$boothId);
				if(!empty($paymentId))
				{
				//to update status of payment
				$paymentData['payment_status']='1';
				$paymentData['payment_id']=$paymentId;
				//update frnt_registrant data
				$frntregistrant=$this->common_model->updateDataFromTabel('frnt_registrant',$paymentData,array('payment_status'=>'0','user_id'=>$user_id));
				//send mail to user
				//$mailResponse=$this->sendRegistrantDetailsMail($paymentId);		
						if(!empty($paymentId))
						{
						$transaction='Transaction Id:- '.$proArray['TRANSACTIONID'];
						$noteMessage='Please note your booking id for future references.<bt> Your registrant details has been also sent to your email address.';
						$amt='Amount:- $'.$proArray['AMT'];
						$msg=lang('paymentTransactionMsg').'<br>'.$transaction.'<br>'.'BookingId:- '.$bookingId.'<br>'.$amt.'<br><br><small>'.$noteMessage.'</small>';
						$msgArray = array('msg'=>$msg,'is_success'=>'true');
						set_global_messages($msgArray);
						echo json_encode(array('msg' =>'', 'status'=>'1'));
						return true; 
						}
						else
						{
						$msg='Transaction failed.Please try again.';
						$msgArray = array('msg'=>$msg,'is_success'=>'true');
						set_global_messages($msgArray);
						echo json_encode(array('msg' =>'', 'status'=>'1'));
						return true; 
						}
				}
			}
		else
		{
			//when transaction failed
			$msg='Transaction failed.Please try again.';
			$msgArray = array('msg'=>$msg,'is_success'=>'true');
			set_global_messages($msgArray);
			echo json_encode(array('msg' =>'', 'status'=>'1'));
			return true; 
		}
	}
	
	//--------------------------------------------------------------------------
	
	/*
	 * @descript: This function is used to get frnt booth details  
	 * @param $sideeventRegisId
	 * @return array
	 * 
	 */ 
	
	function sponsorboothrequstdata($sponsorId){
		
		$this->db->select('fsb.*, seb.booth_position');
		$this->db->from('frnt_sponsor_booth as fsb');
		$this->db->join('sponsor_exhibitor_booths as seb', 'seb.id = fsb.booth_id');
		$this->db->where('fsb.sponsor_id',$sponsorId);
		$this->db->where('fsb.booth_status','1');
		$this->db->order_by('seb.booth_position','ase');
		$query = $this->db->get();
		return $query->result();
	}
	
	
	function getDigitNumber($registrant_id,$eventId)
    {	//to get eventId
		$this->db->select('te.event_number,te.user_id,tu.operand_number,ter.registrant_number');
		$this->db->from($this->tablenmeventregistrants.' as ter');
	    $this->db->from($this->tablenmuser.' as tu');
		$this->db->join($this->tablenmevent.' as te','te.user_id =tu.id','left');
		$this->db->where('te.id',$eventId);
		$this->db->where('ter.id',$registrant_id);
		$query = $this->db->get();
		$result= $query->row();
		return $result;
	}
	
     
}

?>
