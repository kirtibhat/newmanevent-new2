<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

$formRegister = array(
		'name'	 => 'formRegister',
		'id'	 => 'formRegister',
		'method' => 'post',
		'class'  => 'wpcf7-form',
	);
	
$regisName = array(
		'name'	=> 'regisName',
		'value'	=> '',
		'id'	=> 'regisName',
		'type'	=> 'text',
		'required'	=> '',
		'placeholder'	=> lang('home_regisName').' ('.lang('home_regisRequired').')',
		'autocomplete' => 'off',
		'tabindex' => '1',
	);
$regisEmail = array(
		'name'	=> 'regisEmail',
		'value'	=> '',
		'id'	=> 'regisEmail',
		'type'	=> 'email',
		//'onkeyup'=> 'email_avila_chk()',
		'placeholder'	=> lang('home_regisEmail').' ('.lang('home_regisRequired').')',
		'required'	=> '',
		'autocomplete' => 'off',
		'tabindex' => '2',
	);
$regisPassword = array(
		'name'	=> 'regisPassword',
		'value'	=> '',
		'id'	=> 'regisPassword',
		'type'	=> 'password',
		'required'	=> '',
		 'maxlength' =>'25',
		'placeholder'	=> lang('home_regisPassword').' ('.lang('home_regisRequired').')',
		'tabindex' => '3',
	);
$regisConfirmPassword = array(
		'name'	=> 'regisConfirmPassword',
		'value'	=> '',
		'id'	=> 'regisConfirmPassword',
		'type'	=> 'password',
		'placeholder'	=> lang('home_regisConfirmPassword').' ('.lang('home_regisRequired').')',
		'required'	=> '',
		'tabindex' => '4',
		 'maxlength' =>'25',
		
	);		
/*
$regisMale = array(
		'name'	=> 'gender',
		'id'	=> 'regisMale',
		'type'	=> 'radio',
		'class'	=> 'radioleft',
	//	'required'	=> '',
		'value'	=> 'male',
		'tabindex' => '5',
	);
$regisFemale = array(
		'name'	=> 'gender',
		'id'	=> 'regisFemale',
		'type'	=> 'radio',
		'class' => 'radioleft',
	//	'required'	=> '',
		'value'	=> 'female',
	);	
	
$regisMonthArray = array(
                  ''  => 'Month',
                  '1' => 'January',
                  '2' => 'February',
                  '3' => 'March',
                  '4' => 'April',
                  '5' => 'May',
                  '6' => 'June',
                  '7' => 'July',
                  '8' => 'August',
                  '9' => 'September',
                  '10' => 'October',
                  '11' => 'November',
                  '12' => 'December',
                );	
*/

?>

<div class="modal fade homelogin_popup dn" id="registration_popup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<?php echo form_open(base_url('home/register'),$formRegister); ?>
		<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close colosebtnlogin" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"><?php echo lang('home_headinRegister'); ?></h4>
			</div>
			<!-- modelbodyscroll -->
			<div class="modal-body " >
				<div class="modelinner">
					<div class="control-group mb10 loginbtninput_home">
							
							
							<div class="position_R">
								<?php echo form_input($regisName); ?>
						    </div>
                             <div class="position_R">
								<?php echo form_input($regisEmail); ?>
								<div class="dn emailavail">This email already exists</div>
                            </div>
                          
							<?php echo form_input($regisPassword); ?>
							<?php echo form_input($regisConfirmPassword); ?>
					<!--	<div class="gdtext">
							<div class="pl8">
								<?php //echo lang('home_regisGender'); ?> (<?php //echo lang('home_regisRequired'); ?>)
							</div>
							<div class="checkradiobg homelogincheck mt10">
								<div class="radio">
									<?php //echo form_radio($regisMale); ?>
									<label for="regisMale">Male &nbsp; &nbsp;</label>
									<?php //echo form_radio($regisFemale); ?>
									<label for="regisFemale">Female &nbsp; &nbsp;</label>
								</div>
							</div>
							<div class="clearfix">
							</div>
						</div>
						<div class="gdtext mt30">
							<div class="pl8">
								<?php //echo lang('home_regisDOB'); ?> (<?php //echo lang('home_regisRequired'); ?>)
							</div>
							<div class="pull-left select-style register selectdash_select popuphomeselect mt10">
								<?php 
									//$other = 'id="regisMonth" required="required" tabindex = "6"';
									//echo form_dropdown('regisMonth', $regisMonthArray, '',$other);
								?>
							</div>
							<div class="pull-left select-style register selectdash_select popuphomeselect mt10">
								<?php 
								 /*	$regisDayArray['']    = 'Day';
									for($i=1;$i<=31;$i++){
										$regisDayArray[$i]   = $i;
									}
								  
									$other 				= 'id="regisDay" required=" " tabindex = "7" ';
									echo form_dropdown('regisDay', $regisDayArray, '',$other); */
								?>
							</div>
							<div class="pull-left select-style register selectdash_select popuphomeselect mt10">
								<?php 
								/*	$regisYearArray['']    = 'Year';
									  for($i=date("Y");$i>=1910;$i--){
										$regisYearArray[$i]   = $i;
									}
									
									$other				= 'id="regisYear"  required=" " tabindex = "8" ';
									echo form_dropdown('regisYear', $regisYearArray, '',$other); */
								?>
							</div>
							<div class="clearfix">
							</div>
						</div> -->
						<div class="mt0 mb_20">
							<?php 
								$extra 	= 'class="homepopup_login submitbtn" tabindex = "9" ';
								echo form_submit('regisSubmit',$this->lang->line('home_regisButtonRegister'),$extra);
								
								$extra 	= 'class="homepopup_login submitbtn ml_10" tabindex = "10" ';
								echo form_reset('regisReset',$this->lang->line('home_regisButtonReset'),$extra);
							?>
							<div class="clearfix">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php echo form_close(); ?>
</div>

	
<script type="text/javascript" language="javascript">
	
	//open register popup by click on 
	$(document).on('click','.user_register',function(){	
		//open login popup
		open_model_popup('registration_popup');
		//set class and id in attribute
		$("#ajax_open_popup_html").attr('openbox','registration_popup');	
		$("#ajax_open_popup_html").attr('clickbox','user_register');	
		
		$("#regisName").focus();
		
		//get click type
		var type = $(this).attr('type');
		if(type==undefined){
			//reset form
			$('#formRegister')[0].reset();
		}
		
	});
	
	//call function for uesr login
	doRegistration();	
	
	//*------check register email id--------*/
	$(document).on('blur','#regisEmail',function(){
	
		var regisEmail =$.trim($(this).val());
	
		if(isValidEmailAddress(regisEmail) && regisEmail!="") {
			fieldData = { regisEmail : regisEmail };
			var url = baseUrl+'home/checkuseremail';
			$.post(url,fieldData, function(data) {
			  if(data){
						if(data.is_success=='true'){
							$('.emailavail').show().html(data.msg).addClass('successemail').removeClass('erroremail');
						}else{
							//$('#regisEmail').val('');
							$('.emailavail').show().html(data.msg).addClass('erroremail').removeClass('successemail');
						}	
						$('.emailavail').slideUp(6000);
					}
			},"json");
		} 
	}); 
		
</script>
