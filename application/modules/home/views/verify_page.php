<?php echo $verify_email_status = $this->uri->segment(3); ?>
<?php //if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<div class="container">
  <div class="homepagebgReg">
    <div class="row bgIconRow">  
    	<div class="col-5 bg-register">
        	<div class="regBoxWrap">
            	<div class="static-panel">
            	<h4 class="font-large">Congratulations</h4>
                </div>
                <form method="post" class="pad35 mt_15" accept-charset="utf-8" data-parsley-validate="">                
                    <div class="regBoxContent">
                <p class="font-small mB30">
                    <?php
                        if($verify_email_status==1){ echo "Your email has been verified successfully."; }
                        if($verify_email_status==2){ echo "Your email is already verified."; }
                    ?>
                </p>    
                                 
                </div> 
                  <div class="home-logo pull-right"><img src="<?php echo IMAGE; ?>dashboard_event.png" class=""></div>  
				</form> 
                 <div class="static-footer pull-left">
            	 <a href="<?php echo base_url(); ?>" class="btn-nav prevI btn pull-right">Close<i class="icon-rightarrow"></i></span></a>
                </div>
            </div>
        </div> 
        <div class="col-10 bgicon">
        <div class="pull-right">
       <i class="icon-newman-logo"></i>
       </div> 
        </div>   	  
    </div>
  </div>
