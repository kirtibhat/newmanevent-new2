<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
$formTroubleLogging = array(
    'name' => 'formTroubleLogging',
    'id' => 'formTroubleLogging',
    'method' => 'post',
    'class' => 'wpcf7-form',
    'data-parsley-validate' => '',
);
$troubleFirstName = array(
    'name' => 'troubleFirstName',
    'value' => '',
    'id' => 'troubleFirstName',
    'type' => 'text',
    'class' => 'xlarge_input input100',
    'placeholder' => "John",
    'required' => '',
    'size' => '40',
    'autocomplete' => 'off',
    'data-parsley-required-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
);
$troubleLastName = array(
    'name' => 'troubleLastName',
    'value' => '',
    'id' => 'troubleLastName',
    'type' => 'text',
    'class' => 'xlarge_input input100',
    'placeholder' => "Smith",
    'required' => '',
    'size' => '40',
    'autocomplete' => 'off',
    'data-parsley-required-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
);
$troubleLoginEmail = array(
    'name' => 'troubleLoginEmail',
    'value' => '',
    'id' => 'troubleLoginEmail',
    'type' => 'email',
    'class' => 'xlarge_input input100',
    'placeholder' => "Email Address",
    'required' => '',
    'size' => '40',
    'autocomplete' => 'off',
    'data-parsley-required-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
);
$troubleLoginMessage = array(
    'name' => 'troubleLoginMessage',
    'value' => '',
    'id' => 'troubleLoginMessage',
    'type' => 'textarea',
    'class' => 'xlarge_input input100 form-textarea custom_li',
    'rows' => '3',
    'cols' => '500',
    'placeholder' => "This is my message...",
    'required' => '',
    'data-parsley-required-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
);
?>

<!-- contact us page -->
<div class="modal fade" id="troublelogin_popup">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">        
        <h4 class="modal-title"><?php echo lang('home_trouble_login'); ?></h4>
      </div>
      <!-- Start Model body -->
      <div class="modal-body">
		<?php echo form_open(base_url('home/troublelogging'), $formTroubleLogging); ?>
        <div class="modal-body-content">
          <div class="row-fluid">
              <div class="labelDiv">
                <label class="form-label" for="firstname"><?php echo lang('home_trouble_login_firstname'); ?></label>
              </div>
              <?php echo form_input($troubleFirstName); ?>				
          </div>
          <div class="clearFix display_inline w_100 mT15"></div>
          <div class="row-fluid">
              <div class="labelDiv">
                <label class="form-label" for="surname"><?php echo lang('home_trouble_login_surname'); ?></label>
              </div>
              <?php echo form_input($troubleLastName); ?>
          </div>

          <div class="clearFix display_inline w_100 mT15"></div>
          <div class="row-fluid">
              <div class="labelDiv">
                <label class="form-label" for="email"><?php echo lang('home_trouble_login_email'); ?></label>
              </div>
              <?php echo form_input($troubleLoginEmail); ?>
          </div>
          <div class="clearFix display_inline w_100 mT15"></div>
          <div class="row-fluid">
              <div class="labelDiv">
                <label class="form-label" for="phonen"><?php echo lang('home_trouble_login_message'); ?></label>
              </div>
			  <?php echo form_textarea($troubleLoginMessage); ?>
          </div>
        </div>
        <div class="modal-footer">
            <div class="pull-right">
                <?php
					$sendData['buttonArray'] = array(
						'submit' => array('value' => $this->lang->line('comm_send')),
						'cancle' => true);
					$this->load->view('common/common_button', $sendData);
				?>        
            </div>         	
        </div>     
        <input type="hidden" value="loginpost" name="formaction">
        <?php echo form_close(); ?>   
      </div>            
    <!-- End Model body -->
    </div>
  </div>
</div>
<!--end of contactus popup-->



