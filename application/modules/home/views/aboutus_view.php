<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<div class="container">
<!-- about us page -->
<div class="row-fluid-15">
    <div id="page_content" class="col-9 getnme_logo_wrap">
        <h1 class="page-title"><?php echo lang('home_header_about_us') ?></h1>
        <p class="page-paragraph">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sit amet est mi. Phasellus sollicitudin sit amet libero tincidunt adipiscing. Suspendisse potenti. Proin facilisis, dolor at tristique rhoncus, est justo facilisis sapien, et faucibus neque sapien ac diam. Pellentesque volutpat odio sit amet adipiscing rutrum. Nam vitae interdum sapien. Praesent at nulla</p>
        
        <div class="panel" id="panel0">
        	<div class="panel_header verydark_color">
            	<?php echo lang('home_header_about_us_header1') ?>
            </div>
            <div class="panel_contentWrapper">
            	<div class="panel_content">
                    <p class="mB15">
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sit amet est mi. Phasellus sollicitudin sit amet libero tincidunt adipiscing. Suspendisse potenti. Proin facilisis, dolor at tristique rhoncus, est justo facilisis sapien, et faucibus neque sapien ac diam. Pellentesque volutpat odio sit amet adipiscing rutrum. Nam vitae interdum sapien. Praesent at nulla.
                    </p>
                </div>
                <!--end of panel body-->
            </div>
        </div>
		<div class="panel" id="panel0">
        	<div class="panel_header verydark_color">
            	<?php echo lang('home_header_about_us_header2') ?>
            </div>
            <div class="panel_contentWrapper">
            	<div class="panel_content">
                    <p class="mB15">
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sit amet est mi. Phasellus sollicitudin sit amet libero tincidunt adipiscing. Suspendisse potenti. Proin facilisis, dolor at tristique rhoncus, est justo facilisis sapien, et faucibus neque sapien ac diam. Pellentesque volutpat odio sit amet adipiscing rutrum. Nam vitae interdum sapien. Praesent at nulla.
                    </p>
                </div>
                <!--end of panel body-->
            </div>
        </div>
        <div class="panel" id="panel0">
        	<div class="panel_header verydark_color">
            	<?php echo lang('home_header_about_us_header3') ?>
            </div>
            <div class="panel_contentWrapper">
            	<div class="panel_content">
                    <p class="mB15">
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sit amet est mi. Phasellus sollicitudin sit amet libero tincidunt adipiscing. Suspendisse potenti. Proin facilisis, dolor at tristique rhoncus, est justo facilisis sapien, et faucibus neque sapien ac diam. Pellentesque volutpat odio sit amet adipiscing rutrum. Nam vitae interdum sapien. Praesent at nulla.
                    </p>
                </div>
                <!--end of panel body-->
            </div>
        </div>
		<div class="col-6 getnme_logo">
            <img class="getlogow pull-right" src="<?php echo IMAGE; ?>logo_getnewman.svg">
        </div>
    </div>
</div>
  <!--end of home page content--> 
  
</div>
<!-- content container-->

