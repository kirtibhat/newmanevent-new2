<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
$isRememberMe = false;
if ($this->input->cookie('loginUsername') != "" && $this->input->cookie('loginPassword') != "") { $isRememberMe = true; }
$formLogin = array(
'name'          => 'formLogin',
'id'            => 'formLogin',        
'method'        => 'post',
'class'         => 'wpcf7-form aa',        
);
$loginUsername = array(
'name'          => 'loginUsername',
'value'         => $this->input->cookie('loginUsername'),
'id'            => 'loginUsername',
'type'          => 'email',
'class'         => 'xxlarge_input',
//'placeholder'   => lang('lable_email_address'),
'size'          => '40',
'autocomplete'  => 'off',
'tabindex'      => '1',
);
$loginPassword = array(
'name'          => 'loginPassword',
'value'         => base64_decode($this->input->cookie('loginPassword')),
'id'            => 'loginPassword',
'type'          => 'password',
//'placeholder'   => lang('home_login_password'),
'class'         => 'xxlarge_input',
'size'          => '40',
'maxlength'     => '25',   
'tabindex'      => '2',
);
$rememberMe = array(
'name'          => 'rememberMe',
'value'         => '1',
'id'            => 'rememberme',
'type'          => 'checkbox',
'class'         => 'checkBox',
'checked'       => $isRememberMe,
);

$authUrlGplus   = !empty($authUrlGplus) ? $authUrlGplus : '';
$login_url      = !empty($login_url) ? $login_url : '';

?>
<div class="modal fade login_popup" id="login_popup">
  <div class="modal-dialog modal-sm-as">
    <div class="modal-content">
      <div class="modal-header group_color">
        <h4 class="modal-title"><?php echo lang('home_login_title'); ?></h4>
      </div>
<!--
      <div class="infobar">
      <p></p>
      <span class="info_btn"><span class="field_info xsmall">The Access ID# is a unique identifier assigned to every registrant using this site. Use it to access, edit, cancel, pay and rebook items in your registration.</span></span>
      </div>
-->
      <?php echo form_open(base_url('home/login'), $formLogin); ?>
      <div class="modal-body">
        <div class="modal-body-content">
          <div class="col-3 width_custom">
          <a class="btn-icon btn loginButton" href="<?php echo $login_url; ?>"><?php echo lang('login_with_'); ?> <span class="small_icon bgwhite"> <i class="icon-facebook "></i> </span></a>
          </div>
          <div class="col-3 width_custom">
          <a class="btn-icon btn loginButton" href="<?php echo $authUrlGplus; ?>"><?php echo lang('login_with_'); ?> <span class="small_icon bgwhite"> <i class="icon-3-3 "></i> </span></a>
          </div>
          <div class="createbtn login-btn">		
         <span> OR </span> <span class="medium_icon mR5 mL5"> <i class="icon-Newman "></i> </span> <span> ID </span> 
         </div> 			
        
        
        <div class="row"> 			
            <span id="error-login_email_error"></span>
        </div>
        <div class="clearFix display_inline"></div>
        <div class="row">
        <div class="labelDiv pull-left">
          <label class="form-label text-right"><?php echo lang('lable_email_address'); ?><span class="required">*</span></label>
        </div>
        <?php echo form_input($loginUsername); ?>
        <span id="error-loginUsername"></span>
        </div>
        
        <div class="row">
        <div class="labelDiv pull-left">
          <label class="form-label text-right"><?php echo lang('home_login_password'); ?><span class="required">*</span></label>
        </div>
        <?php echo form_input($loginPassword); ?>
        <span id="error-loginPassword"></span>
        <a data-dismiss="modal" class="forgot_password pull-right frgt_login mT5" href="javascript:void(0)" ><?php echo lang('home_forgot_password'); ?></a> 
        </div>
        <div class="clearFix display_inline w_100 mT10"></div>
        
        <div class="row">	
         <?php echo form_checkbox($rememberMe); ?>
 		 <label class="form-label" for="rememberme"><span>Remember Me</span></label>
         </div>
        </div>
        <div class="modal-footer">
        <a data-dismiss="modal" class="user_register pull-left frgt_login font-xsmall" id="create_account" href="javascript:void(0)"><?php echo lang('home_create_account'); ?></a>
          <div class="pull-right">
            <button class="btn-normal btn" tabindex="4"  data-dismiss="modal">Cancel</button>
            <button type="submit" tabindex="3" name="loginsubmit" id="loginsubmit" class="btn-normal btn popup_login submitbtn">Login</button>
          </div>
        </div>
      </div>
      <?php echo form_hidden('formaction', 'loginpost') ?>	
      <input type="hidden" name="is_refrer_url" value="0" id="is_refrer_url">
      <?php echo form_close(); ?>
    </div>
  </div>
</div>
