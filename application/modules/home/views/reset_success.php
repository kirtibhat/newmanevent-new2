<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

$formResetPassword = array(

		'name'	 => 'formResetPass',
		'id'	 => 'formResetPass',
		'method' => 'post',
		'class'  => 'wpcf7-form',
	);
  
?>

<!-- Modal -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="passwordreset_popup" class="modal fade dn">
  <div class="modal-dialog modal-sm-as">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title"><?php echo lang('reset_sucsess_title'); ?></h4>
      </div>
      <form class="wpcf7-form" method="post" id="paswordreset" name="paswordreset" accept-charset="utf-8" action="">
	   <div class="modal-body">
		<div class="modal-body-content">
		  <p class="font-small mB30"><?php echo lang('reset_password_text'); ?></br> <?php echo lang('reset_password_text1'); ?></p>
		  <div class="clearfix"></div>		
		</div>
      </div>
        <div class="modal-footer">
		  <div class="pull-right">                 
			<input type="button" class="btn-normal btn reset_password_close" value="Close" name="close1">
		  </div>
        </div>
		  <input type="hidden" value="loginpost" name="formaction">
	  </form>     
    </div>
  </div>
</div>
<!-- Modal -->

