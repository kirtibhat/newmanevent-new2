<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

$formForgotPassword = array(
    'name'      => 'formForgotPassword',
    'id'        => 'formForgotPassword',
    'method'    => 'post',
    'class'     => 'wpcf7-form',
);

$forgotEmail = array(
    'name'      => 'forgotEmail',
    'value'     => '',
    'id'        => 'forgotEmail',
    'type'      => 'email',
    'class'     => 'xlarge_input input100',
    //'placeholder' => lang('home_forgot_email'),
    'size'      => '40',
    'autocomplete' => 'off',
);
?>
<!-- Modal -->
<div class="modal fade" id="forgot_popup">
  <div class="modal-dialog">
    <!-- Login Form -->
    <div class="modal-content">
     <?php echo form_open(base_url('home/forgotpassword'), $formForgotPassword); ?>
      <div class="modal-header">        
        <h4 class="modal-title"><?php echo lang('home_forgot_title'); ?></h4>
      </div>
      <div class="infobar">
        <p class="dn" id="forgot_pass_info">Please enter your email address and we'll send you an email with a link to reset your password.</p>
        <span class="info_btn" onclick="showHideInfoBar('forgot_pass_info');"></span>
      </div>
      <div class="modal-body">
        <div class="modal-body-content">
          <div class="row-fluid">
              <div class="col-fluid-15">
                <div class="labelDiv pull-left">
                  <label class="form-label"><?php echo lang('home_forgot_email'); ?><span class="required" aria-required="true">*</span></label>
                </div>
              </div>
            <div class="col-fluid-15">
                <?php echo form_input($forgotEmail); ?>		
                <span id="error-forgotEmail"></span>	
            </div>
          </div>
        </div>
        <div class="modal-footer">
            <div class="pull-right">
                <div class="btn_wrapper">
					<?php
					$sendData['buttonArray'] = array(
						'submit' => array('value' => $this->lang->line('comm_send')),
						'cancle' => true);
					$this->load->view('common/common_button', $sendData);
					?>
				</div>       
            </div>         	
        </div>     
      </div>        
    <input type="hidden" value="loginpost" name="formaction">
    <?php echo form_close(); ?>      
    </div>
    <!-- End Model Content -->
  </div>
</div>

