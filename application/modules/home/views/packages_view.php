<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<div class="container">
<!-- about us page -->
<div class="row-fluid-15">
    <div id="page_content" class="col-9 getnme_logo_wrap">
        <h1 class="page-title"><?php echo lang('home_header_packages') ?></h1>
        <p class="page-paragraph">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sit amet est mi. Phasellus sollicitudin sit amet libero tincidunt adipiscing. Suspendisse potenti. Proin facilisis, dolor at tristique rhoncus, est justo facilisis sapien, et faucibus neque sapien ac diam. Pellentesque volutpat odio sit amet adipiscing rutrum. Nam vitae interdum sapien. Praesent at nulla</p>
        
        <div class="panel" id="panel0">
        	<div class="panel_header verydark_color">
            	<?php echo lang('home_comparison') ?>
            </div>
            <div class="panel_contentWrapper">
            	<div class="panel_content">
                    <p class="mB15">
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sit amet est mi. Phasellus sollicitudin sit amet libero tincidunt adipiscing. Suspendisse potenti. Proin facilisis, dolor at tristique rhoncus, est justo facilisis sapien, et faucibus neque sapien ac diam. Pellentesque volutpat odio sit amet adipiscing rutrum. Nam vitae interdum sapien. Praesent at nulla.
                    </p>
                    <div class="row-fluid">
                      <div class="col1_3">
                          <h3>Casual</h3>
                          <ul>
                              <li><span class="plus_icon">+</span>Starts with 1 User</li>
                              <li><span class="plus_icon">+</span>Up to 3 Users*</li>
                              <li><span class="plus_icon">+</span>Unlimited Clients</li>
                              <li><span class="plus_icon">+</span>Unlimited Projects</li>
                              <li><span class="plus_icon">+</span>Unlimited Invoicing</li>
                          </ul>
                          <a href="javascript:void(0)" id="apply_casual" class="btn-normal btn">Apply</a>
                      </div>
                      <!--end of col3-->
                      <div class="col1_3">
                          <h3>Solo</h3>
                          <ul>
                              <li><span class="plus_icon">+</span>Starts with 1 User</li>
                              <li><span class="plus_icon">+</span>Up to 3 Users*</li>
                              <li><span class="plus_icon">+</span>Unlimited Clients</li>
                              <li><span class="plus_icon">+</span>Unlimited Projects</li>
                              <li><span class="plus_icon">+</span>Unlimited Invoicing</li>
                          </ul>
                          <a href="javascript:void(0)" id="apply_solo" class="btn-normal btn">Apply</a>
                      </div>
                      <!--end of col3-->
    
                      <div class="col1_3">
                          <h3>Enterprise</h3>
                          <ul>
                              <li><span class="plus_icon">+</span>Starts with 1 User</li>
                              <li><span class="plus_icon">+</span>Up to 3 Users*</li>
                              <li><span class="plus_icon">+</span>Unlimited Clients</li>
                              <li><span class="plus_icon">+</span>Unlimited Projects</li>
                              <li><span class="plus_icon">+</span>Unlimited Invoicing</li>
                          </ul>
                          <a href="javascript:void(0)" id="apply_enterprise" class="btn-normal btn">Apply</a>
                      </div>
                      <!--end of col3-->
                    </div>
                    <!--end of row-->
                    <div class="row-fluid">
                      <div class="free_content">
                            <h3>Free<span class="comma">,</span></h3>
                            <ul>
                                <li><span class="plus_icon">+</span>1 User<span class="comma">,</span></li>
                                <li><span class="plus_icon">+</span>Up to 9 Users*<span class="comma">,</span></li>
                                <li><span class="plus_icon">+</span>4 Clients<span class="comma">,</span></li>
                                <li><span class="plus_icon">+</span>2 Projects</li>
                            </ul>
                            <a href="<?php echo base_url();?>home/freeRegistration" id="apply_free" class="btn-normal btn">Apply</a>
                        </div>
                    </div>
                    <!--end of row-->
                </div>
                <!--end of panel body-->
            </div>
        </div>
        
        <div class="panel" id="panel0">
        	<div class="panel_header enterprise_color">
            	Enterprise
            </div>
            <div class="panel_contentWrapper">
            	<div class="panel_content">
                    <p class="mB15">
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sit amet est mi. Phasellus sollicitudin sit amet libero tincidunt adipiscing. Suspendisse potenti. Proin facilisis, dolor at tristique rhoncus, est justo facilisis sapien, et faucibus neque sapien ac diam. Pellentesque volutpat odio sit amet adipiscing rutrum. Nam vitae interdum sapien. Praesent at nulla.
                    </p>
                    <div class="row-fluid">
                      <div class="col1_3 w_100">
                          <h3>Enterprise</h3>
                          <ul>
                              <li><span class="plus_icon">+</span>Starts with 1 User</li>
                              <li><span class="plus_icon">+</span>Up to 3 Users*</li>
                              <li><span class="plus_icon">+</span>Unlimited Clients</li>
                              <li><span class="plus_icon">+</span>Unlimited Projects</li>
                              <li><span class="plus_icon">+</span>Unlimited Invoicing</li>
                              <li><span class="plus_icon">+</span>Feature list including exclusive offers and upgrades on other account types</li>
                              <li><span class="plus_icon">+</span>Feature list including exclusive offers and upgrades on other account types</li>
                              <li><span class="plus_icon">+</span>Feature list including exclusive offers and upgrades on other account types</li>
                              <li><span class="plus_icon">+</span>Feature list including exclusive offers and upgrades on other account types</li>
                              <li><span class="plus_icon">+</span>Feature list including exclusive offers and upgrades on other account types</li>
                          </ul>
                          <a href="javascript:void(0)" class="btn-normal btn" id="apply_enterprise">Apply</a>
                      </div>
                      <!--end of col3-->
                    </div>
                    <!--end of row-->
                </div>
                <!--end of panel body-->
            </div>
        </div>
        <div class="panel" id="panel0">
        	<div class="panel_header solo_color">
            	Solo
            </div>
            <div class="panel_contentWrapper">
            	<div class="panel_content">
                    <p class="mB15">
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sit amet est mi. Phasellus sollicitudin sit amet libero tincidunt adipiscing. Suspendisse potenti. Proin facilisis, dolor at tristique rhoncus, est justo facilisis sapien, et faucibus neque sapien ac diam. Pellentesque volutpat odio sit amet adipiscing rutrum. Nam vitae interdum sapien. Praesent at nulla.
                    </p>
                    <a href="javascript:void(0)" class="btn-normal btn package_btn" id="apply_solo">Apply</a>
                </div>
                <!--end of panel body-->
            </div>
        </div>
        <div class="panel" id="panel0">
        	<div class="panel_header free_color">
            	Casual
            </div>
            <div class="panel_contentWrapper">
            	<div class="panel_content">
                    <p class="mB15">
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sit amet est mi. Phasellus sollicitudin sit amet libero tincidunt adipiscing. Suspendisse potenti. Proin facilisis, dolor at tristique rhoncus, est justo facilisis sapien, et faucibus neque sapien ac diam. Pellentesque volutpat odio sit amet adipiscing rutrum. Nam vitae interdum sapien. Praesent at nulla.
                    </p>
                    <a href="javascript:void(0)" class="btn-normal btn package_btn" id="apply_casual">Apply</a>
                </div>
                <!--end of panel body-->
            </div>
        </div>  
        <div class="panel" id="panel0">
        	<div class="panel_header free_color">
            	Free
            </div>
            <div class="panel_contentWrapper">
            	<div class="panel_content">
                    <p class="mB15">
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sit amet est mi. Phasellus sollicitudin sit amet libero tincidunt adipiscing. Suspendisse potenti. Proin facilisis, dolor at tristique rhoncus, est justo facilisis sapien, et faucibus neque sapien ac diam. Pellentesque volutpat odio sit amet adipiscing rutrum. Nam vitae interdum sapien. Praesent at nulla.
                    </p>
                    <a href="<?php echo base_url();?>home/freeRegistration" id="apply_free" class="btn-normal btn package_btn">Apply</a>
                </div>
                <!--end of panel body-->
            </div>
        </div>
        
    </div>
    <div class="col-6 getnme_logo">
            <img class="getlogow pull-right" src="<?php echo IMAGE; ?>logo_getnewman.svg">
        </div>
</div>
<!--end of home page content-->   
</div>
<!-- content container-->
