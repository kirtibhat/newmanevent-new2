<div class="container">
<!-- home delegate page -->
<div class="row-fluid-15">
    <div id="page_content" class="col-9">
        <h1 class="page-title">Delegates</h1>
        <p class="page-paragraph">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sit amet est mi. Phasellus sollicitudin sit amet libero tincidunt adipiscing. Suspendisse potenti. Proin facilisis, dolor at tristique rhoncus, est justo facilisis sapien, et faucibus neque sapien ac diam. Pellentesque volutpat odio sit amet adipiscing rutrum. Nam vitae interdum sapien. Praesent at nulla</p>
    </div>
</div>
<div class="row-fluid-15">

    <div id="page_content" class="col-15 filter_row">
        <div class="feature_box delegates_color">
            <h4>
                <a>Delegates</a>
            </h4>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sit amet est mi. Phasellus sollicitudin sit amet libero tincidunt adipiscing. Suspendisse potenti.</p>
        </div>

        <div class="feature_box delegates_color">
            <h4>
                <a>Quick Hotel Reservations</a>
            </h4>
            <p>
           	  <span>
                  <span class="delegates_bg">
                      <img src="<?php echo IMAGE; ?>reservation.png">
                  </span>
              </span>
              Lorem ipsum dolor sit amet adelami uspendisse potenti.<a href="#"> read more</a>
            </p>
        </div>

        <div class="feature_box delegates_color">
            <h4>
                <a class="">Great Contact Potential</a>
            </h4>
            <p>
              <span>
              	<span>
                  <img src="<?php echo IMAGE; ?>potential.png">
                </span>
              </span>
              Lorem ipsum dolor sit amet adelami uspendisse potenti.<a href="#"> read more</a>
            </p>
        </div>

        <div class="feature_box delegates_color">
            <h4>
                <a class="">Customise Your Stay</a>
            </h4>
            <p>
              <span>
              	<span>
                  <img src="<?php echo IMAGE; ?>your_stay.png">
                </span>
              </span>
              Lorem ipsum dolor sit amet adelami uspendisse potenti.<a href="#"> read more</a>

            </p>
        </div>

    </div>
</div>  
<!--end of home page content--> 
</div>
<!-- content container-->
