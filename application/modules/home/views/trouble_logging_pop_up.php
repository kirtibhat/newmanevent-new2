<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="trouble_login_popup" class="modal fade homelogin_popup dn">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header ">
        <h4 class="medium dt-large modal-title">Email Sent!</h4>
      </div>

      <div class="modal-body small">
        <div class="modelinner info_popup">      
            <div class="control-group mb10 loginbtninput_home">
			  <p class="mb_30 popup_message">An email has been sent to the email address supplied with instructions on how to reset your password.</p>
			  <p class="mb_30 popup_message">It should arrive there within a few minutes.</p>
              <div class="clearfix"></div>
              
              <div class="btn_wrapper">
                  <input type="submit" class="pull-right medium" value="Close" name="close" data-dismiss="modal">
              </div>

            </div>
            <input type="hidden" value="loginpost" name="formaction">
        </div>
      </div>
    </div>
  </div>
</div>
