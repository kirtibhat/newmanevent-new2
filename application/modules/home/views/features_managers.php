<div class="container">
<!-- about us page -->
<div class="row-fluid-15">
    <div id="page_content" class="col-9">
        <h1 class="page-title">Managers</h1>
        <p class="page-paragraph">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sit amet est mi. Phasellus sollicitudin sit amet libero tincidunt adipiscing. Suspendisse potenti. Proin facilisis, dolor at tristique rhoncus, est justo facilisis sapien, et faucibus neque sapien ac diam. Pellentesque volutpat odio sit amet adipiscing rutrum. Nam vitae interdum sapien. Praesent at nulla</p>
    </div>
</div>
<div class="row-fluid-15">

    <div id="page_content" class="col-15 filter_row">
        <div class="feature_box">
            <h4>
                <a>Managers</a>
            </h4>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sit amet est mi. Phasellus sollicitudin sit amet libero tincidunt adipiscing. Suspendisse potenti.</p>
        </div>

        <div class="feature_box">
            <h4>
                <a>Convenience at your fingertips</a>
            </h4>
            <p>
              <span>
                  <span>
	                  <img src="<?php echo IMAGE; ?>fingure.png">
                  </span>
              </span>
              Lorem ipsum dolor sit amet adelami uspendisse potenti.<a href="#"> read more</a>
            </p>
        </div>

        <div class="feature_box">
            <h4>
                <a class="">Real-time Event Tracking</a>
            </h4>
            <p>
              <span>
              	<span class="managers_bg">
                  <img src="<?php echo IMAGE; ?>calender.png">
                </span>
              </span>
              Lorem ipsum dolor sit amet adelami uspendisse potenti.<a href="#"> read more</a>
            </p>
        </div>

        <div class="feature_box">
            <h4>
                <a>Ease of Info Dissemination</a>
            </h4>
            <p>
              <span>
              	<span>
                  <img src="<?php echo IMAGE; ?>dissemination.png">
                </span>
              </span>
              Lorem ipsum dolor sit amet adelami uspendisse potenti.<a href="#"> read more</a>
            </p>
        </div>

        <div class="feature_box">
            <h4>
                <a>Resources</a>
            </h4>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sit amet est mi. Phasellus sollicitudin sit amet libero tincidunt adipiscing. Suspendisse potenti.</p>
        </div>
		
        <div class="feature_box">
            <h4>
                <a class="">Great Contact Potential</a>
            </h4>
            <p>
              <span>
              	<span>
                  <img src="<?php echo IMAGE; ?>potential.png">
                </span>
              </span>
              Lorem ipsum dolor sit amet adelami uspendisse potenti.<a href="#"> read more</a>
            </p>
        </div>
        
        <div class="feature_box">
            <h4>
                <a>Quick Hotel Reservations</a>
            </h4>
            <p>
           	  <span>
                  <span class="managers_bg">
                      <img src="<?php echo IMAGE; ?>reservation.png">
                  </span>
              </span>
              Lorem ipsum dolor sit amet adelami uspendisse potenti.<a href="#"> read more</a>
            </p>
        </div>
		 <div class="feature_box">
            <h4>
                <a>Hotel Transfer Options</a>
            </h4>
            <p>
              <span>
              	<span class="managers_bg">
                  <img src="<?php echo IMAGE; ?>transfer.png">
                </span>
              </span>
              Lorem ipsum dolor sit amet adelami uspendisse potenti.<a href="#"> read more</a>
            </p>
        </div>
        <div class="feature_box">
            <h4>
                <a class="">Wide Charity Network</a>
            </h4>
            <p>
              <span>
                <img src="<?php echo IMAGE; ?>charity_network.png">
              </span>
              Sed sit amet est mi.Phasellus sollicitudin sit amet libero.<a href="#"> read more</a>
            </p>
        </div>
		<div class="feature_box">
            <h4>
                <a class="">Discounted Airfares</a>
            </h4>
            <p>
              <span>
                  <img src="<?php echo IMAGE; ?>airfares.png">
              </span>
              Sed sit amet est mi.Phasellus sollicitudin sit amet libero tincidunt adipiscing sitamet libero.<a href="#"> read more</a>
            </p>
        </div>
    </div>
</div>  
<!--end of home page content--> 
</div>
<!-- content container-->
