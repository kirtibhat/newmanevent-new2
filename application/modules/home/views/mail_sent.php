<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

$formForgotPassword = array(
    'name'	 => 'formForgotPassword',
    'id'	 => 'formForgotPassword',
    'method' => 'post',
    'class'  => 'wpcf7-form',
);
	
$forgotEmail = array(
    'name'	=> 'forgotEmail',
    'value'	=> '',
    'id'	=> 'forgotEmail',
    'type'	=> 'email',
    'class' => 'small',
    'placeholder'	=> lang('home_forgot_email'),
    'required'	=> '',
    'size' => '40',
    'autocomplete' => 'off',
    
);
	
?>
<!-- Modal -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="mail_sent" class="modal fade homelogin_popup dn">
  <div class="modal-dialog modal-sm-as">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Email Sent</h4>
      </div>
        <div class="modal-body">
            <div class="modal-body-content">
              <p class="font-small mB30" id="useremailid"></p>
              <p class="font-small mB30"><?php echo lang('home_forgot_emailsent_description'); ?></p>
              <div class="clearfix"></div>
            
            <input type="hidden" value="loginpost" name="formaction">    
        </div>
      </div>
        <div class="modal-footer">
              <div class="pull-right">                 
                <button class="btn-normal btn" type="button" name="logincancel" data-dismiss="modal">Close</button>
              </div>

        </div>
    </div>
  </div>
</div>
<!-- Modal -->
