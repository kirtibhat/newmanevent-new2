<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<!--end of container-->
<div class="container pt68" id="">

<!-- about us page -->
<div class="row-fluid-15">
    <div  id="page_content" class="span9">
        <h1 class="page-title xlarge"><?php echo lang('home_header_features') ?></h1>
        <p class="small"><?php echo lang('common_lipsum_text'); ?></p>
    </div>
</div>
<div class="row-fluid-15">

    <div  id="page_content" class="span15 filter_row">
        <div class="feature_box small managers">
            <h4 class="medium dt-large ">
                <a ><?php echo lang('home_header_features_managers') ?></a>
            </h4>
            <p><?php echo lang('common_lipsum_text_short'); ?></p>
        </div>

        <div class="feature_box small delegates">
            <h4 class="medium dt-large ">
                <a><?php echo lang('home_header_features_delegates') ?></a>
            </h4>
            <p><?php echo lang('common_lipsum_text_short'); ?></p>
        </div>

        <div class="feature_box small suppliers">
            <h4 class="medium dt-large ">
                <a><?php echo lang('home_header_features_suppliers') ?></a>
            </h4>
            <p><?php echo lang('common_lipsum_text_short'); ?></p>
        </div>

        <div class="feature_box small clients" >
            <h4 class="medium dt-large ">
                <a><?php echo lang('home_header_features_clients') ?></a>
            </h4>
            <p><?php echo lang('common_lipsum_text_short'); ?></p>
        </div>

        <div class="feature_box small resources">
            <h4 class="medium dt-large ">
                <a><?php echo lang('home_header_features_resources') ?></a>
            </h4>
            <p><?php echo lang('common_lipsum_text_short'); ?></p>
        </div>
        
        <!--second row-->

        <div class="feature_box small managers">
            <h4 class="medium dt-large drow">
                <a class=""><?php echo lang('features_convenience_fingertips') ?></a>
            </h4>
            <p>
              <span>
                  <span>
	                  <img src="<?php echo IMAGE; ?>/fingure.png"/>
                  </span>
              </span>
              <?php echo lang('common_lipsum_text_short_short') ?><a href="#"> <?php echo lang('common_read_more') ?></a>
            </p>
        </div>

        <div class="feature_box small delegates">
            <h4 class="medium dt-large drow">
                <a class=""><?php echo lang('features_quick_hotel_reservation') ?></a>
            </h4>
            <p>
           	  <span>
                  <span class="delegates_bg">
                      <img src="<?php echo IMAGE; ?>/reservation.png"/>
                  </span>
              </span>
              <?php echo lang('common_lipsum_text_short_short') ?><a href="#"> <?php echo lang('common_read_more') ?></a>
            </p>
        </div>

        <div class="feature_box small suppliers">
            <h4 class="medium dt-large drow">
                <a class=""><?php echo lang('features_hotel_transfer_options') ?></a>
            </h4>
            <p>
              <span>
              	<span  class="suppliers_bg">
                  <img src="<?php echo IMAGE; ?>/transfer.png"/>
                </span>
              </span>
              <?php echo lang('common_lipsum_text_short_short') ?><a href="#"> <?php echo lang('common_read_more') ?></a>
            </p>
        </div>

        <div class="feature_box small clients" >
            <h4 class="medium dt-large drow">
                <a class=""><?php echo lang('features_wide_charity_network') ?></a>
            </h4>
            <p>
              <span>
                <img src="<?php echo IMAGE; ?>/charity_network.png"/>
              </span>
              <?php echo lang('common_lipsum_text_short1') ?><a href="#"> <?php echo lang('common_read_more') ?></a>
            </p>
        </div>

        <div class="feature_box small resources">
            <h4 class="medium dt-large drow">
                <a class=""><?php echo lang('features_discounted_airfares') ?></a>
            </h4>
            <p>
              <span>
                  <img src="<?php echo IMAGE; ?>/airfares.png"/>
              </span>
              <?php echo lang('common_lipsum_text_short2') ?><a href="#"> <?php echo lang('common_read_more') ?></a>
            </p>
        </div>
        
        <!--third row-->

        <div class="feature_box small managers">
            <h4 class="medium dt-large drow">
                <a class=""><?php echo lang('features_realtime_event_tracking') ?></a>
            </h4>
            <p>
              <span>
              	<span  class="managers_bg">
                  <img src="<?php echo IMAGE; ?>/calender.png"/>
                </span>
              </span>
              <?php echo lang('common_lipsum_text_short_short') ?><a href="#"> <?php echo lang('common_read_more') ?></a>
            </p>
        </div>

        <div class="feature_box small delegates">
            <h4 class="medium dt-large drow">
                <a class=""><?php echo lang('features_great_contact_potential') ?></a>
            </h4>
            <p>
              <span>
              	<span>
                  <img src="<?php echo IMAGE; ?>/potential.png"/>
                </span>
              </span>
              <?php echo lang('common_lipsum_text_short_short') ?><a href="#"> <?php echo lang('common_read_more') ?></a>
            </p>
        </div>

        <div class="feature_box small suppliers">
            <h4 class="medium dt-large drow">
                <a class=""><?php echo lang('features_the_best_venues') ?></a>
            </h4>
            <p>
              <span>
              	<span>
                  <img src="<?php echo IMAGE; ?>/best_venues.png"/>
                </span>
              </span>
              <?php echo lang('common_lipsum_text_short_short') ?><a href="#"> <?php echo lang('common_read_more') ?></a>
            </p>
        </div>

        <div class="feature_box small clients" >
            <h4 class="medium dt-large drow">
                <a class=""><?php echo lang('features_secure_online_onations') ?></a>
            </h4>
            <p>
              <span>
              	<span>
                  <img src="<?php echo IMAGE; ?>/donations.png"/>
                </span>
              </span>
              <?php echo lang('common_lipsum_text_short_short') ?><a href="#"> <?php echo lang('common_read_more') ?></a>
            </p>
        </div>

        <div class="feature_box small resources">
            <h4 class="medium dt-large drow">
                <a class=""><?php echo lang('features_awards_and_recognition') ?></a>
            </h4>
            <p>
              <span>
              	<span class="resources_bg">
                  <img src="<?php echo IMAGE; ?>/awards.png"/>
                </span>
              </span>
              <?php echo lang('common_lipsum_text_short3') ?><a href="#"> <?php echo lang('common_read_more') ?></a>
            </p>
        </div>
        
        <!--fourth row-->

        <div class="feature_box small managers">
            <h4 class="medium dt-large drow">
                <a class=""><?php echo lang('features_ease_dissemination') ?></a>
            </h4>
            <p>
              <span>
              	<span>
                  <img src="<?php echo IMAGE; ?>/dissemination.png"/>
                </span>
              </span>
              <?php echo lang('common_lipsum_text_short_short') ?><a href="#"> <?php echo lang('common_read_more') ?></a>
            </p>
        </div>

        <div class="feature_box small delegates">
            <h4 class="medium dt-large drow">
                <a class=""><?php echo lang('features_customise_stay') ?></a>
            </h4>
            <p>
              <span>
              	<span>
                  <img src="<?php echo IMAGE; ?>/your_stay.png"/>
                </span>
              </span>
              <?php echo lang('common_lipsum_text_short_short') ?><a href="#"> <?php echo lang('common_read_more') ?></a>
            </p>
        </div>

        <div class="feature_box small suppliers">
            <h4 class="medium dt-large drow">
                <a class=""><?php echo lang('features_event_promo_materials') ?></a>
            </h4>
            <p>
              <span>
              	<span>
                  <img src="<?php echo IMAGE; ?>/materials.png"/>
                </span>
              </span>
              <?php echo lang('common_lipsum_text_short_short') ?><a href="#"> <?php echo lang('common_read_more') ?></a>
            </p>
        </div>

        <div class="feature_box small clients" >
            <h4 class="medium dt-large drow">
                <a class=""><?php echo lang('features_secure_fee_shelters') ?></a>
            </h4>
            <p>
              <span>
              	<span class="clients_bg">
                  <img src="<?php echo IMAGE; ?>/shelters.png"/>
               	</span>
              </span>
              <?php echo lang('common_lipsum_text_short_short') ?><a href="#"> <?php echo lang('common_read_more') ?></a>
            </p>
        </div>

        <div class="feature_box small resources">
            <h4 class="medium dt-large drow">
                <a class=""><?php echo lang('features_24hour_turnaround') ?></a>
            </h4>
            <p>
              <span>
                  <img src="<?php echo IMAGE; ?>/wow.png"/>
              </span>
              <?php echo lang('common_lipsum_text_short4') ?><a href="#"> <?php echo lang('common_read_more') ?></a>
            </p>
        </div>
        

    </div>
</div>
  
  <!--end of home page content--> 
  
</div>
<!-- content container-->
