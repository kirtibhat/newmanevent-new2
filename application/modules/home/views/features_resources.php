<div class="container">
<!-- resources page -->
<div class="row-fluid-15">
    <div id="page_content" class="col-9">
        <h1 class="page-title">Resources</h1>
        <p class="page-paragraph">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sit amet est mi. Phasellus sollicitudin sit amet libero tincidunt adipiscing. Suspendisse potenti. Proin facilisis, dolor at tristique rhoncus, est justo facilisis sapien, et faucibus neque sapien ac diam. Pellentesque volutpat odio sit amet adipiscing rutrum. Nam vitae interdum sapien. Praesent at nulla</p>
    </div>
</div>
<div class="row-fluid-15">
    <div id="page_content" class="col-15 filter_row">
        <div class="feature_box resource_color">
            <h4>
                <a>Resources</a>
            </h4>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sit amet est mi. Phasellus sollicitudin sit amet libero tincidunt adipiscing. Suspendisse potenti.</p>
        </div>
        <div class="feature_box resource_color">
            <h4>
                <a class="">Discounted Airfares</a>
            </h4>
            <p>
              <span>
                  <img src="<?php echo IMAGE; ?>airfares.png">
              </span>
              Sed sit amet est mi.Phasellus sollicitudin sit amet libero tincidunt adipiscing sitamet libero.<a href="#"> read more</a>
            </p>
        </div>
        <div class="feature_box resource_color">
            <h4>
                <a class="">Awards and Recognition</a>
            </h4>
            <p>
              <span>
              	<span class="resources_bg">
                  <img src="<?php echo IMAGE; ?>awards.png">
                </span>
              </span>
              Ramet libero tincidunt adipiscing sitamet libero.<a href="#"> read more</a>
            </p>
        </div>
        <div class="feature_box resource_color">
            <h4>
                <a class="">24-hour Turnaround</a>
            </h4>
            <p>
              <span>
                  <img src="<?php echo IMAGE; ?>wow.png">
              </span>
              Phasellus sollicitudin sit amet libero tinmet libero.<a href="#"> read more</a>
            </p>
        </div>
    </div>
</div>  
  <!--end of home page content--> 
</div>
<!-- content container-->
