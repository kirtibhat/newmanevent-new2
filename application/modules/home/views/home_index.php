<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');  ?>
<div class="page_content">
	<div class="container">
    	<div class="mainBanner">
            <img src="<?php echo IMAGE;?>home-banner.jpg">
        </div>
        <div class="row">
        	<div class="col-9">                                
            	<ul class="tabNav" id="pills-first">
                      <li class="active"><a href="#tab_a"><span class="medium_icon"> <i class="icon-rightarrow"></i></span>Free</a></li>
                      <li class=""><a href="#tab_b"><span class="medium_icon"> <i class="icon-rightarrow"></i></span>Casual</a></li>
                      <li class=""><a href="#tab_c"><span class="medium_icon"> <i class="icon-rightarrow"></i></span>Solo</a></li>
                      <li class=""><a href="#tab_d"><span class="medium_icon"> <i class="icon-rightarrow"></i></span>Enterprice</a></li>
                    </ul>
                    <div class="filers">                    
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_a">
                            <div class="col-fluid-15">
                            <div class="col-fluid-12 home_tab_content">
								<h1>Free <span>Account</span></h1>
                                <p>
                                If your event is free to attend, then this system is free as well.
                                You might have a wedding, a Board meeting or a party to
                                organise and this system lets you send out invitations and
                                to monitor responses.
                                </p>
                                <p>
                                You can have multiple categories for invitations and
                                customise the look as well.
                                </p>
                            </div>
                            <div class="col-fluid-3 Price_side">
                            	<h1>$0.00
                                <span>per ticket</span>
                                <span>(+GST)</span>
                                </h1>
                            </div>
                            </div>
                            <div class="col-fluid-3 mT21 pull-right">                            	
                               <a href="<?php echo base_url(); ?>home/freeRegistration" class="btn-orange btn pull-left">Apply</a>
                            </div>                       
                        </div>
                        <div class="tab-pane" id="tab_b">
                        	<div class="col-fluid-15">
                            <div class="col-fluid-12 home_tab_content">
								<h1>Casual <span>Account</span></h1>
                                <p>
                                If you are occasionally running or managing events that 
                                attract a fee, you have the choice of setting up a fee
                                based event.
                                </p>
                                <p>
                               You are charged for each delegate that attends, with a
                            minimum of 25 tickets. You can purchase additional
                            tickets in batches of 25, as you need them.
                                </p>
                            </div>
                            <div class="col-fluid-3 Price_side">
                            	<h1>$5.00
                                <span>per ticket</span>
                                <span>(+GST)</span>
                                </h1>
                            </div>
                            </div>
                            <div class="col-fluid-3 mT21 pull-right">                            	
                                <button class="btn-orange btn pull-left">Apply</button>
                            </div>   
                        </div>
                        <div class="tab-pane" id="tab_c">
                        	<div class="col-fluid-15">
                            <div class="col-fluid-12 home_tab_content">
								<h1>Solo <span>Account</span></h1>
                                <p>
                                If you are a lone PCO or event manager, this is the best	
								option for you.
                                </p>
                                <p>
                               You can run multiple events and use a range of resources
								for a single monthly fee.
                                </p>
                            </div>
                            <div class="col-fluid-3 Price_side">
                            	<h1>$60.00
                                <span>per ticket</span>
                                <span>(+GST)</span>
                                </h1>
                            </div>
                            </div>
                            <div class="col-fluid-3 mT21 pull-right">                            	
                                <button class="btn-orange btn pull-left">Apply</button>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_d">
							<div class="col-fluid-15">
                            <div class="col-fluid-12 home_tab_content">
								<h1>Enterprise <span>Account</span></h1>
                                <p>
                               If you have a team of people, then this account gives you 
							   maximum flexibility and management power.
                                </p>
                                <p>
                             As the account holder, you can manage your team and see
                             all of the events. As an operator, you can link in with other 
                             team members to best manage the event.
                                </p>
                            </div>
                            <div class="col-fluid-3 Price_side">
                            	<h1>$60.00
                                <span>per ticket</span>
                                <span>(+GST)</span>
                                </h1>
                            </div>
                            </div>
                            <div class="col-fluid-3 mT21 pull-right">                            	
                                <button class="btn-orange btn pull-left">Apply</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-6">
            <div class="background_logo">
            	<div>
            	<p>One stop access to event management starts HERE</p>
               <span><button id="show"  class="btn-orangeWhite">HOW IT WORKS</button></span>
               <span><a href="<?php echo base_url(); ?>home/freeRegistration"><button class="btn-orange ">FREE SIGN UP</button></a></span>
                </div>
            </div>
            </div>
        </div>
    </div>
</div>
<?php $this->session->unset_userdata('referrer_url'); ?>

