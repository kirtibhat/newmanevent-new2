<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

$formRegister = array(
		'name'	 => 'formRegister',
		'id'	 => 'formRegister',
		'method' => 'post',
		'class'  => 'wpcf7-form',
	);
	
$regisName = array(
		'name'	=> 'regisName',
		'value'	=> '',
		'id'	=> 'regisName',
		'type'	=> 'text',
		'required'	=> '',
		'placeholder'	=> lang('home_regisName').' ('.lang('home_regisRequired').')',
		'autocomplete' => 'off',
		'tabindex' => '1',
	);
$regisEmail = array(
		'name'	=> 'regisEmail',
		'value'	=> '',
		'id'	=> 'regisEmail',
		'type'	=> 'email',
		//'onkeyup'=> 'email_avila_chk()',
		'placeholder'	=> lang('home_regisEmail').' ('.lang('home_regisRequired').')',
		'required'	=> '',
		'autocomplete' => 'off',
		'tabindex' => '2',
	);
$regisPassword = array(
		'name'	=> 'regisPassword',
		'value'	=> '',
		'id'	=> 'regisPassword',
		'type'	=> 'password',
		'required'	=> '',
		 'maxlength' =>'25',
		'placeholder'	=> lang('home_regisPassword').' ('.lang('home_regisRequired').')',
		'tabindex' => '3',
	);
$regisConfirmPassword = array(
		'name'	=> 'regisConfirmPassword',
		'value'	=> '',
		'id'	=> 'regisConfirmPassword',
		'type'	=> 'password',
		'placeholder'	=> lang('home_regisConfirmPassword').' ('.lang('home_regisRequired').')',
		'required'	=> '',
		'tabindex' => '4',
		 'maxlength' =>'25',
		
	);	

?>

<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="registration_popup" class="modal fade homelogin_popup dn">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header color_skyblue">
				<h4 class="modal-title register_title">Casual Account</h4>
			</div>
			<div class="modal-body">
				<div class="modelinner">
						<div class="control-group mb10 ">
							<label for="firstname">First Name (Required)</label>
							<input type="text" autocomplete="off" size="40" required placeholder="John" id="firstname" value="" name="firstname">
							<label for="surname">Surname (required)</label>
							<input type="text" autocomplete="off" size="40" required placeholder="Smith" id="surname" value="" name="surname">
							<label for="organisation">Organisation (required)</label>
							<input type="text" autocomplete="off" size="40" required placeholder="Newman Events" id="organisation" value="" name="organisation">
							<label for="email">Email (required)</label>
							<input type="email" autocomplete="off" size="40" required placeholder="john.smith@newmanevents.com" id="email" value="" name="email">
							<label for="phoneno">Phone No. (required)</label>
							<div class="two_inputs_wrapper">
								<input type="text" autocomplete="off" size="40" required placeholder="02" id="phonecode" value="" name="phonecode">
								<input type="text" autocomplete="off" size="40" required placeholder="12549254" id="phoneno" value="" name="phoneno">
							</div>
							<label for="email" class="no_of_licenses" >No of Licenses (required)</label>
							<input type="email" class="no_of_licenses"  autocomplete="off" size="40" required placeholder="john.smith@newmanevents.com" id="email" value="" name="email">
							<label for="password">Password (minimum 6 Alpha/numeric combination)</label>
							<input type="password" autocomplete="off" size="40" required placeholder="Password" id="password" value="" name="password">
							<label for="confirmpassword">Confirm Password</label>
							<input type="password" autocomplete="off" size="40" required placeholder="Confirm Password" id="confirmpassword" value="" name="confirmpassword">
							<div class="clearfix">
							</div>
							<div class="btn_wrapper">
								<input type="submit" class="popup_login submitbtn pull-right" value="Send" name="loginsubmit">
								<input type="button" class="popup_cancel submitbtn pull-right" value="Cancel" name="logincancel" data-dismiss="modal">
							</div>
						</div>
						<input type="hidden" value="loginpost" name="formaction">
				</div>
			</div>
		</div>
	</div>
</div>
