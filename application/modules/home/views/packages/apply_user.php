<!-- Newman free account first step form -->
<div class="container">
  <div class="homepagebgReg">
    <div class="row bgIconRow">  
    	<div class="col-5 bg-register">
        	<div class="regBoxWrap">
            	<div class="static-panel">
            	<h4 class="font-large">Newman Account</h4>
                </div>
                <form method="post" class="pad35 mt_15" accept-charset="utf-8" data-parsley-validate="">                
                <div class="regBoxContent">
                <p class="font-small mB30"> You are about to set up your NEWMAN
                account.
                <br><br>
                You can log in with a Facebook or Google+
    account or set up a NEWMAN ID.</p>
    			<button class="btn-normal btn">Log in with <span class="small_icon bgwhite"> <i class="icon-facebook "></i> </span></button>
                <button class="btn-normal btn">Log in with <span class="small_icon bgwhite"> <i class="icon-3-3 "></i> </span></button>			
                  <div class="createbtn">		
                 <button class="btn-normal btn">Create <span class="small_icon bgwhite"> <i class="icon-Newman "></i> </span> Account </button>
                 </div>
                  <p class="font-small mB30 second_para">We want to be sure that you understand
                the terms as they relate to this type of
                account.
                </p>
                <div>
    			 <input type="checkbox" id="checkboxEample" name="checkboxEample" class="checkBox">
  				 <label class="form-label terms" for="checkboxEample">
                 <span class="para">I have read and agree with the<br>
                    <span class="link_terms">Terms & Conditions </span> for my <br>
                    Newman account.
                 </span>
                 </label> 
                 </div>            
                </div> 
                 <div class="home-logo pull-right"><img src="<?php echo IMAGE; ?>dashboard_event.png" class=""></div>  
				</form> 
                 <div class="static-footer pull-left">
            	 <button class="btn-nav prevI btn pull-right"> Next<i class="icon-rightarrow"></i></span> </button>
                </div>
            </div>
        </div> 
        <div class="col-10 bgicon">
        <div class="pull-right">
       <i class="icon-newman-logo"></i>
       </div> 
        </div>   	  
    </div>
  </div>
