<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="verify_email" class="modal fade homelogin_popup dn">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header ">
        <h4 class="medium dt-large modal-title"><?php echo lang('thanks_for_applying'); ?></h4>
      </div>

      <div class="modal-body small">
        <div class="modelinner info_popup">   
            <div class="control-group mb10 loginbtninput_home">
			  <p class="mb_30 popup_message"><?php echo lang('thanks_for_choosing_newman_event')?></p>
        <p class="mb_30 popup_message regular_font" id="verify_email_popup_content"></p>
              <div class="clearfix"></div>
              
              <div class="btn_wrapper">
                  <input type="submit" class="pull-right  medium" value="Close" name="close" data-dismiss="modal">
              </div>

            </div>
            <input type="hidden" value="loginpost" name="formaction">   
        </div>
      </div>
    </div>
  </div>
</div>
