<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
/*
 * Create Form Fields
 */ 
$formFreeRegistration = array(
'name'                  => 'free_registration',
'id'                    => 'free_registration',
'method'                => 'post',
//'class'                 => 'wpcf7-form free_form',
'accept-charset'        => 'utf-8',
);
$username = array(
'name'                  => 'registerUsername',
'value'                 => '',
'id'                    => 'registerUsername',
'type'                  => 'text',
'class'                 => 'large_input',
'placeholder'           => 'First Name',
'size'                  => '40',
'autocomplete'          => 'off',
'tabindex'              => '1',
);
$surname = array(
'name'                  => 'registerUserSurname',
'value'                 => '',
'id'                    => 'registerUserSurname',
'type'                  => 'text',
'class'                 => 'large_input',
'placeholder'           => 'Last Name',
'size'                  => '40',
'autocomplete'          => 'off',
'tabindex'              => '2',
);
$newman_nickname = array(
'name'                  => 'newman_nickname',
'value'                 => '',
'id'                    => 'newman_nickname',
'type'                  => 'text',
'class'                 => 'large_input',
'placeholder'           => 'Nickname',
'size'                  => '40',
'autocomplete'          => 'off',
'tabindex'              => '1',
);
$email = array(
'name'                  => 'email',
'value'                 => '',
'id'                    => 'email',
'type'                  => 'email',
'class'                 => 'large_input',
'placeholder'           => 'Email Address',
'size'                  => '40',
'autocomplete'          => 'off',
'tabindex'              => '3',
);
$confirm_email = array(
'name'                  => 'confirm_email',
'value'                 => '',
'id'                    => 'confirm_email',
'type'                  => 'text',
'class'                 => 'large_input confirm_email',
'placeholder'           => 'Confirm Email Address',
'size'                  => '40',
'autocomplete'          => 'off',
'tabindex'              => '4',
);

$organization = array(
'name'                  => 'organization',
'value'                 => '',
'id'                    => 'organization',
'type'                  => 'text',
'class'                 => 'large_input',
'placeholder'           => '',
'size'                  => '40',
'autocomplete'          => 'off',
'tabindex'              => '5',
);
$authUrlGplus   = !empty($authUrlGplus) ? $authUrlGplus : '';
$login_url      = !empty($login_url) ? $login_url : '';
$isParam        = $this->input->get('q');
if($isParam==1){
    $this->session->set_userdata('referrer_url', '1');
}
?>
<div id="get_login_hit_url" lang='<?php echo $login_url;  ?>' data-google_login="<?php echo $authUrlGplus;?>"></div>
<div class="container">
  <div class="homepagebgReg">
    <div class="row bgIconRow">  
    	<div class="col-5 bg-register">
            <?php echo form_open(base_url('home/freeRegistration'), $formFreeRegistration); ?>
        	<div class="regBoxWrap">
            	<div class="static-panel">
            	<h4 class="font-large"><?php echo lang('newman_account_registration_step1_title'); ?></h4>
                </div>
                <div class="pad35 mt_15">
                <div class="regBoxContent">
                <p class="font-small mB30"><?php echo lang('registration_step1_heading1'); ?>
                <br><br>
                <?php echo lang('registration_step1_heading2'); ?>
                </p>
                <a class="btn-icon btn fbbtnevent fblogin"><?php echo lang('login_with'); ?> 
                    <span class="small_icon bgwhite"> <i class="icon-facebook "></i></span>
                </a>
                
                <a class="btn-icon btn gplusbtnevent gmailLogin"><?php echo lang('login_with'); ?> 
                    <span class="small_icon bgwhite"> <i class="icon-3-3 "></i> </span>
                </a>	
                
                <div class="login-btn"><span> OR </span></div>
                		
                <div class="createbtn">		
                    <a class="btn-icon btn show_create_acc_form pull-none "><?php echo lang('create'); ?> 
                        <span class="small_icon bgwhite"> <i class="icon-Newman "></i> </span>    <?php echo lang('account'); ?> 
                    </a>
                </div>
                
                <div class="create_acc_form" style="display:none;">
                    <div class="row">
                    <div class="labelDiv pull-left">
                      <label class="form-label text-left"><?php echo lang('lable_firstname'); ?>
                      <span class="required" aria-required="true">*</span>
                      </label>
                    </div>
                    <?php echo form_input($username); ?>
                    <span id="error-registerUsername"></span>
                    </div>
                    <div class="row">
                    <div class="labelDiv pull-left">
                      <label class="form-label text-left"><?php echo lang('lable_surname'); ?>
                      <span class="required" aria-required="true">*</span>
                      </label>
                    </div>
                    <?php echo form_input($surname); ?>
                    <span id="error-registerUserSurname"></span>
                    </div>
                    <div class="row">
                    <div class="labelDiv pull-left">
                      <label class="form-label text-left"><?php echo lang('lable_email_address'); ?>
                      <span class="required" aria-required="true">*</span>
                      </label>
                    </div>
                    <?php echo form_input($email); ?>
                    <span id="error-email"></span>
                    </div>
                    <div class="row">
                    <div class="labelDiv pull-left">
                      <label class="form-label text-left"><?php echo lang('lable_confirm_email'); ?>
                      <span class="required" aria-required="true">*</span>
                      </label>
                    </div>
                    <?php echo form_input($confirm_email); ?>
                    <span id="error-confirm_email"></span>
                    </div>
                    <div class="row">
                    <div class="labelDiv pull-left">
                      <label class="form-label text-left"><?php echo lang('lable_organization'); ?>
                     
                      </label>
                    </div>
                </div>
                <!-- End form fields -->
                <?php echo form_input($organization); ?>
                <span id="error-dinner_dance6"></span>
                </div>                 
                </div>  
                <div class="termsevent" style="display:none;">
                 <p class="font-small mB30 second_para"><?php echo lang('register_paragraph1_bottom'); ?></p>
    			 <input type="checkbox" id="terms_checkbox" name="terms_checkbox" class="checkBox">
                 <label class="form-label terms" for="terms_checkbox"></label>
  				 <label class="form-label terms" for="">
                 <p class="para mL30">I have read and agree with the<br>
                 <span class="link_terms" data-toggle="modal" data-target=".terms_popup" >Terms & Conditions &nbsp; </span> for my <br>
                    Newman account.
                 <span id="error-email" class="w_100 set_error dn"><label id="email-error" class="error" for="email">Please select terms & conditions.</label></span>   
                 </p>
                 <!--<span class="para"><?php //echo lang('register_terms_paragraph2_bottom'); ?></span>-->
                 </label> 
                 </div>  
                
                <div class="home-logo pull-right"><img src="<?php echo IMAGE; ?>dashboard_event.png" class=""></div> 
                </div>
                <?php echo "<input type='hidden' id='set_checked_status' value='0'>"; ?>
                <?php echo form_hidden('registration_type', encode($this->config->item('free_registration'))) ?>
				<?php echo form_close(); ?>
                <div class="static-footer pull-left">
                <a class="btn-nav prevI btn pull-right next_btn_action dn" tabindex="6"> <?php echo lang('next_btn_title'); ?></span></a>
                <a class="btn-nav prevI btn pull-right checked_action dn" tabindex="6"> <?php echo lang('next_btn_title'); ?></span></a>
                </div>
            </div>
        </div> 
        <div class="col-10 bgicon">
        <div class="pull-right">
       <i class="icon-newman-logo"></i>
       </div> 
        </div>   	  
    </div>
  </div>
<!---Term Popup-->
<div class="modal fade terms_popup ">
	<div class="modal-dialog modal-sm-as">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Terms & Condition</h4>
			</div>
			
			<div class="infobar">
				<p class="info_Status"></p>
				<a class="info_btn" onclick="return false;">
				</a>
			</div>
			<div class="modal-body">
				<div class="modal-body-content">
				<div class="clearFix display_inline w_100 mT10"></div>
					<div class="row">	
					<?php echo lang('term_condition_text');?>
					</div>
				</div>
				<div class="modal-footer">
					<div class="pull-right">
						<button class="btn-normal btn accept_event" tabindex='4' data-dismiss="modal">Accept</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
