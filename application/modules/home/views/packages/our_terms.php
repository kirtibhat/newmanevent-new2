<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
$formFreeRegistration = array(
'name'          => 'registration_terms',
'id'            => 'registration_terms',
'method'        => 'post',
'class'         => 'wpcf7-form our_terms_form',
'accept-charset'        => 'utf-8',
//'data-parsley-validate' => '',
);

$our_terms = array(
'name'          => 'our_terms',
'value'         => 'check',
'id'            => 'our_terms',
'class'         => 'checkBox',
'required'      => '',
'size'          => '40',
'autocomplete'  => 'off',
'tabindex'      => '3',
);  
?>
<?php
$reg_type = $this->session->userdata('registration_type');
$user_id = $this->session->userdata('user_id');
$registration_step = $this->session->userdata('registration_step');
$terms_cls = '';
?>
<div class="container">
  <div class="homepagebgReg">
    <div class="row bgIconRow">  
    	<div class="col-5 bg-register">
             <?php echo form_open(base_url('home/freeTerms'), $formFreeRegistration); ?>
        	<div class="regBoxWrap">
            	<div class="static-panel">
            	<h4 class="font-large"><?php echo lang('terms_heading'); ?></h4>
                </div>
                <div class="pad35 mt_15">              
                <div class="regBoxContent">
                

                
                <div class="termsevent" style="">
                 <p class="font-small mB30 second_para"><?php echo lang('register_paragraph1_bottom'); ?></p>
    			 <?php echo form_checkbox($our_terms); ?>
                 <label class="form-label terms" for="our_terms"></label>
  				 <label class="form-label terms" for="">
                 <p class="para mL30">I have read and agree with the<br>
                 <span class="link_terms" data-toggle="modal" data-target=".terms_popup" >Terms & Conditions &nbsp; </span> for my <br>
                    Newman account.
                 <span id="error-email" class="w_100 set_error dn"><label id="email-error" class="error" for="email">Please select terms & conditions.</label></span>   
                 </p>
                 <!--<span class="para"><?php //echo lang('register_terms_paragraph2_bottom'); ?></span>-->
                 </label> 
                 </div>  
                
                 
                                 
                </div> 
                  <div class="home-logo pull-right"><img src="<?php echo IMAGE; ?>dashboard_event.png" class=""></div>  
                  <input type="hidden" name="terms_user_id" id="terms_user_id" value="<?php if(!empty($user_id)){ echo $this->session->userdata('user_id');}?>" >
				</div> 
                 <div class="static-footer pull-left">
            	 <button tabindex="6" type="submit" name="loginsubmit" id="loginsubmit" class="btn-nav prevI btn pull-right our_terms_next_event disable cursor_default" style="pointer-events:none;"> Next </span> </button>
                </div>
                <?php echo form_hidden('registration_type', encode($this->config->item('free_registration'))) ?>
				<?php echo form_close(); ?> 
            </div>
        </div> 
        <div class="col-10 bgicon">
        <div class="pull-right">
       <i class="icon-newman-logo"></i>
       </div> 
        </div>   	  
    </div>
  </div>
  <!--end of home page content-->   
<!---Term Popup-->
<div class="modal fade terms_popup ">
	<div class="modal-dialog modal-sm-as">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Terms & Condition</h4>
			</div>
			
			<div class="infobar">
				<p class="info_Status"></p>
				<a class="info_btn" onclick="return false;">
				</a>
			</div>
			<div class="modal-body">
				<div class="modal-body-content">
				<div class="clearFix display_inline w_100 mT10"></div>
					<div class="row">	
					<?php echo lang('term_condition_text');?>
					</div>
				</div>
				<div class="modal-footer">
					<div class="pull-right">
						<button class="btn-normal btn accept_terms" tabindex='4' data-dismiss="modal">Accept</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

