<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

$formSoloRegistration = array(
    'name' => 'casual_registration',
    'id' => 'registration',
    'method' => 'post',
    'class' => 'wpcf7-form solo_form',
    'accept-charset' => 'utf-8',
    'data-parsley-validate' => '',
);

$username = array(
    'name' => 'registerUsername',
    'value' => '',
    'id' => 'registerUsername',
    'type' => 'text',
    'class' => 'small',
    'placeholder' => 'John',
    'required' => '',
    'size' => '40',
    'autocomplete' => 'off',
    'tabindex' => '1',
    'data-parsley-required-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
);

$surname = array(
    'name' => 'registerUserSurname',
    'value' => '',
    'id' => 'registerUserSurname',
    'type' => 'text',
    'class' => 'small',
    'placeholder' => 'Smith',
    'required' => '',
    'size' => '40',
    'autocomplete' => 'off',
    'tabindex' => '2',
    'data-parsley-required-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
);

$organisation = array(
    'name' => 'organisation',
    'value' => '',
    'id' => 'organisation',
    'type' => 'text',
    'class' => 'small',
    'placeholder' => 'Newmen Events',
    'size' => '40',
    'autocomplete' => 'off',
    'tabindex' => '3',
);

$email = array(
    'name' => 'email',
    'value' => '',
    'id' => 'email_solo',
    'type' => 'email',
    'class' => 'small',
    'placeholder' => 'Email Address',
    'required' => '',
    'size' => '40',
    'autocomplete' => 'off',
    'tabindex' => '4',
    'data-parsley-required-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
);

$confirm_email = array(
    'name' => 'confirm_email',
    'value' => '',
    'id' => 'confirm_email',
    'type' => 'text',
    'class' => 'small',
    'placeholder' => 'Confirm Email Address',
    'required' => '',
    'size' => '40',
    'autocomplete' => 'off',
    'tabindex' => '5',
    'data-parsley-required-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
    'data-parsley-equalto' => '#email_solo',
    'data-parsley-equalto-message' => lang('common_confirm_email_equal'),
);
?>

<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="solo_popup" class="modal fade homelogin_popup dn">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header evtmanager_pop_header">
                <h4 class="medium dt-large modal-title"><?php echo lang('apply_solo'); ?></h4>
            </div>

            <div class="modal-body small">
                <div class="modelinner ls_pop_inner">
<?php echo form_open(base_url('home/registration'), $formSoloRegistration); ?>
                    <div class="control-group mb10">

                        <label for="Username"><?php echo lang('lable_firstname'); ?></label>
<?php echo form_input($username); ?>

                        <label for="Surname"><?php echo lang('lable_surname'); ?></label>
<?php echo form_input($surname); ?>

                        <label for="organisation"><?php echo lang('lable_organisation'); ?></label>
<?php echo form_input($organisation); ?>

                        <label for="emailaddress"><?php echo lang('lable_email_address'); ?></label>
<?php echo form_input($email); ?>

                        <label for="emailaddress2"><?php echo lang('lable_confirm_email'); ?></label>
<?php echo form_input($confirm_email); ?>

                        <label><?php echo lang('this_will_be_your_permanent_account_address'); ?></label>

                        <div class="clearfix"></div>

                        <div class="btn_wrapper">
                            <?php
                            $sendData['buttonArray'] = array('submit' => array('value' => $this->lang->line('comm_send')), 'cancle' => true, 'submit_tabindex' => '6', 'cancle_tabindex' => '7');
                            $this->load->view('common/common_button', $sendData);
                            ?>
                        </div>

                    </div>
<?php echo form_hidden('registration_type', encode($this->config->item('solo_registration'))) ?>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
