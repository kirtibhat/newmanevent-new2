<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

$formCasualRegistration = array(
    'name' => 'casual_registration',
    'id' => 'registration',
    'method' => 'post',
    'class' => 'wpcf7-form casual_form',
    'accept-charset' => 'utf-8',
    'data-parsley-validate' => '',
);

$username = array(
    'name' => 'registerUsername',
    'value' => '',
    'id' => 'registerUsername',
    'type' => 'text',
    'class' => 'xlarge_input input100',
    'placeholder' => 'John',
    'required' => '',
    'size' => '40',
    'autocomplete' => 'off',
    'tabindex' => '1',
    'data-parsley-required-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
);

$surname = array(
    'name' => 'registerUserSurname',
    'value' => '',
    'id' => 'registerUserSurname',
    'type' => 'text',
    'class' => 'xlarge_input input100',
    'placeholder' => 'Smith',
    'required' => '',
    'size' => '40',
    'autocomplete' => 'off',
    'tabindex' => '2',
    'data-parsley-required-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
);

$organisation = array(
    'name' => 'organisation',
    'value' => '',
    'id' => 'organisation',
    'type' => 'text',
    'class' => 'xlarge_input input100',
    'placeholder' => 'Newmen Events',
    'size' => '40',
    'autocomplete' => 'off',
    'tabindex' => '3',
);

$email = array(
    'name' => 'email',
    'value' => '',
    'id' => 'email_casual',
    'type' => 'email',
    'class' => 'xlarge_input input100',
    'placeholder' => 'Email Address',
    'required' => '',
    'size' => '40',
    'autocomplete' => 'off',
    'tabindex' => '4',
    'data-parsley-required-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
);

$confirm_email = array(
    'name' => 'confirm_email',
    'value' => '',
    'id' => 'confirm_email',
    'type' => 'text',
    'class' => 'xlarge_input input100',
    'placeholder' => 'Confirm Email Address',
    'required' => '',
    'size' => '40',
    'autocomplete' => 'off',
    'tabindex' => '5',
    'data-parsley-required-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
    'data-parsley-equalto' => '#email_casual',
    'data-parsley-equalto-message' => lang('common_confirm_email_equal'),
);
?>


<!-- contact us page -->
<div class="modal fade contactus_popup" id="casual_popup">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">        
        <h4 class="modal-title"><?php echo lang('apply_casual'); ?></h4>
      </div>
      <!-- Start Model body -->
      <div class="modal-body">
		<?php echo form_open(base_url('home/registration'), $formCasualRegistration); ?>
        <div class="modal-body-content">
          <div class="row-fluid">
              <div class="labelDiv">
                <label class="form-label" for="Username"><?php echo lang('lable_firstname'); ?></label>
              </div>
              <?php echo form_input($username); ?>				
          </div>
          <div class="clearFix display_inline w_100 mT15"></div>
          <div class="row-fluid">
              <div class="labelDiv">
                <label class="form-label" for="Surname"><?php echo lang('lable_surname'); ?></label>
              </div>
              <?php echo form_input($surname); ?>
          </div>

          <div class="clearFix display_inline w_100 mT15"></div>
          <div class="row-fluid">
              <div class="labelDiv">
                <label class="form-label" for="organisation"><?php echo lang('lable_organisation'); ?></label>
              </div>
              <?php echo form_input($organisation); ?>
          </div>
          
          <div class="clearFix display_inline w_100 mT15"></div>
          <div class="row-fluid">
              <div class="labelDiv">
                <label class="form-label" for="emailaddress"><?php echo lang('lable_email_address'); ?></label>
              </div>
			  <?php echo form_input($email); ?>
          </div>
          <div class="clearFix display_inline w_100 mT15"></div>
          <div class="row-fluid">
              <div class="labelDiv">
                <label class="form-label" for="emailaddress2"><?php echo lang('lable_confirm_email'); ?></label>
              </div>
			  <?php echo form_input($confirm_email); ?>
          </div>
           
          <div class="clearFix display_inline w_100 mT15"></div>
          <div class="row-fluid">
              <div class="labelDiv">
                <label class="form-label" for="emailaddress"><?php echo lang('this_will_be_your_permanent_account_address'); ?></label>
              </div>
			  
          </div>
          
        </div>
        <div class="modal-footer">
            <div class="pull-right">
               <?php
                  $sendData['buttonArray'] = array('submit' => array('value' => $this->lang->line('comm_send')), 'cancle' => true, 'submit_tabindex' => '6', 'cancle_tabindex' => '7');
					$this->load->view('common/common_button', $sendData);
				?>  
            </div>         	
        </div>     
        <?php echo form_hidden('registration_type', encode($this->config->item('casual_registration'))) ?>
        <?php echo form_close(); ?>   
      </div>            
    <!-- End Model body -->
    </div>
  </div>
</div>
<!--end of contactus popup-->
