<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

$formFreeRegistration = array(
    'name' => 'free_registration',
    'id' => 'free_registration',
    'method' => 'post',
    'class' => 'wpcf7-form free_form',
    'accept-charset' => 'utf-8',
    'data-parsley-validate' => '',
);

$username = array(
    'name' => 'registerUsername',
    'value' => '',
    'id' => 'registerUsername',
    'type' => 'text',
    'class' => 'large_input',
    'placeholder' => 'John',
    'required' => '',
    'size' => '40',
    'autocomplete' => 'off',
    'tabindex' => '1',
    'data-parsley-required-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
);
$surname = array(
    'name' => 'registerUserSurname',
    'value' => '',
    'id' => 'registerUserSurname',
    'type' => 'text',
    'class' => 'large_input',
    'placeholder' => 'Smith',
    'required' => '',
    'size' => '40',
    'autocomplete' => 'off',
    'tabindex' => '1',
    'data-parsley-required-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
);

$newman_nickname = array(
    'name' => 'newman_nickname',
    'value' => '',
    'id' => 'newman_nickname',
    'type' => 'text',
    'class' => 'large_input',
    'placeholder' => 'Nickname',
    'size' => '40',
    'autocomplete' => 'off',
    'tabindex' => '1',
);

$email = array(
    'name' => 'email',
    'value' => '',
    'id' => 'email',
    'type' => 'email',
    'class' => 'large_input',
    'placeholder' => 'Email Address',
    'required' => '',
    'size' => '40',
    'autocomplete' => 'off',
    'tabindex' => '1',
    'data-parsley-required-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
);

$confirm_email = array(
    'name' => 'confirm_email',
    'value' => '',
    'id' => 'confirm_email',
    'type' => 'text',
    'class' => 'large_input',
    'placeholder' => 'Confirm Email Address',
    'required' => '',
    'size' => '40',
    'autocomplete' => 'off',
    'tabindex' => '1',
    'data-parsley-required-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-equalto' => '#email',
    'data-parsley-equalto-message' => lang('common_confirm_email_equal'),
    'data-parsley-trigger' => 'change',
);

$organization = array(
    'name' => 'organization',
    'value' => '',
    'id' => 'organization',
    'type' => 'text',
    'class' => 'large_input',
    'placeholder' => 'John',
    'required' => '',
    'size' => '40',
    'autocomplete' => 'off',
    'tabindex' => '1',
    'data-parsley-required-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
);
?>

<div class="container">
  <div class="homepagebgReg">
    <div class="row bgIconRow">  
    	<div class="col-5 bg-register">
            <?php echo form_open(base_url('home/freeRegistration'), $formFreeRegistration); ?>
        	<div class="regBoxWrap">
            	<div class="static-panel">
            	<h4 class="font-large"><?php echo lang('newman_account_registration_step1_title'); ?></h4>
                </div>
                <div class="pad35 mt_15">
                <div class="regBoxContent">
                <p class="font-small mB30"><?php echo lang('registration_step1_heading1'); ?>
                <br><br>
                <?php echo lang('registration_step1_heading2'); ?>
                </p>
    			<button class="btn-normal btn"><?php echo lang('login_with'); ?><span class="small_icon bgwhite"> <i class="icon-facebook "></i> </span></button>
                <button class="btn-normal btn"><?php echo lang('login_with'); ?><span class="small_icon bgwhite"> <i class="icon-3-3 "></i> </span></button>			
                  <div class="createbtn">		
                 <button class="btn-normal btn"><?php echo lang('create'); ?><span class="small_icon bgwhite"> <i class="icon-Newman "></i> </span> <?php echo lang('account'); ?> </button>
                 </div>
                    <div class="row">
                    <div class="labelDiv pull-left">
                      <label class="form-label text-left"><?php echo lang('lable_firstname'); ?>
                      <span class="required" aria-required="true">*</span>
                      </label>
                    </div>
                    <?php echo form_input($username); ?>
                    <span id="error-dinner_dance6"></span>
                    </div>
                    <div class="row">
                    <div class="labelDiv pull-left">
                      <label class="form-label text-left"><?php echo lang('lable_surname'); ?>
                      <span class="required" aria-required="true">*</span>
                      </label>
                    </div>
                    <?php echo form_input($surname); ?>
                    <span id="error-dinner_dance6"></span>
                    </div>
                    <div class="row">
                    <div class="labelDiv pull-left">
                      <label class="form-label text-left"><?php echo lang('lable_email_address'); ?>
                      <span class="required" aria-required="true">*</span>
                      </label>
                    </div>
                    <?php echo form_input($email); ?>
                    <span id="error-dinner_dance6"></span>
                    </div>
                    <div class="row">
                    <div class="labelDiv pull-left">
                      <label class="form-label text-left"><?php echo lang('lable_confirm_email'); ?>
                      <span class="required" aria-required="true">*</span>
                      </label>
                    </div>
                    <?php echo form_input($confirm_email); ?>
                    <span id="error-dinner_dance6"></span>
                    </div>
                    <div class="row">
                    <div class="labelDiv pull-left">
                      <label class="form-label text-left"><?php echo lang('lable_organization'); ?>
                      <span class="required" aria-required="true">*</span>
                      </label>
                    </div>
                    <?php echo form_input($organization); ?>
                    <span id="error-dinner_dance6"></span>
                    </div>                 
                </div>  
                 <div class="home-logo pull-right"><img src="<?php echo IMAGE; ?>dashboard_event.png" class=""></div> 
				</div>
                 <div class="static-footer pull-left">
            	 <button class="btn-nav prevI btn pull-right" name="loginsubmit" id="loginsubmit" type="submit" tabindex="6"> <?php echo lang('next_btn_title'); ?><i class="icon-rightarrow"></i></span> </button>
                </div>
                 <?php echo form_hidden('registration_type', encode($this->config->item('free_registration'))) ?>
				<?php echo form_close(); ?>
            </div>
        </div> 
        <div class="col-10 bgicon">
        <div class="pull-right">
       <i class="icon-newman-logo"></i>
       </div> 
        </div>   	  
    </div>
  </div>
  <!--end of home page content-->   
