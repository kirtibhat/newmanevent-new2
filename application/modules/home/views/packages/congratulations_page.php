<?php $user_email = $this->session->userdata('user_email'); ?>
<?php //if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<div class="container">
  <div class="homepagebgReg">
    <div class="row bgIconRow">  
    	<div class="col-5 bg-register">
        	<div class="regBoxWrap">
            	<div class="static-panel">
            	<h4 class="font-large">Congratulations</h4>
                </div>
                <form method="post" class="pad35 mt_15" accept-charset="utf-8" data-parsley-validate="">                
                    <div class="regBoxContent">
                <p class="font-small mB30">You have finished setting up your account
                for your Newman ID.
                <br><br>
                An email has been sent to
                <?php echo $user_email; ?> with a link to
                validate your account.
                </p>    
                                 
                </div> 
                  <div class="home-logo pull-right"><img src="<?php echo IMAGE; ?>dashboard_event.png" class=""></div>  
				</form> 
                 <div class="static-footer pull-left">
            	 <a href="<?php echo base_url(); ?>" id="closebtn" class="btn-nav prevI btn pull-right">Close</span></a>
                </div>
            </div>
        </div> 
        <div class="col-10 bgicon">
        <div class="pull-right">
       <i class="icon-newman-logo"></i>
       </div> 
        </div>   	  
    </div>
  </div>
<script>
//~ var redirectUrl = localStorage.getItem("redirect_url");
//~ localStorage.removeItem("redirect_url");
//~ $('#closebtn').click(function(){
    //~ if(redirectUrl==undefined || redirectUrl=='' || redirectUrl==null) {
     //~ window.location = baseUrl;
    //~ }else {
        //~ window.location = redirectUrl;
    //~ }
//~ });
</script>
