<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<div class="container">
  <div class="homepagebgReg">
    <div class="row bgIconRow">  
    	<div class="col-5 bg-register">
        	<div class="regBoxWrap">
            	<div class="static-panel">
            	<h4 class="font-large">Our Terms</h4>
                </div>
                <form method="post" class="pad35 mt_15" accept-charset="utf-8" data-parsley-validate="">                
                <div class="regBoxContent">
                <p class="font-small mB30">We want to be sure that you understand
                the terms as they relate to this type of
                account.
                </p>
                <div>
    			 <input type="checkbox" id="termsncondition" name="termsncondition" class="checkBox">
  				 <label class="form-label terms" for="termsncondition">
                 <span class="para">I have read and agree with the<br>
                    <span class="link_terms">Terms & Conditions &nbsp;</span> for my <br>
                    Newman account.
                 </span>
                 </label>
                </div>    
                                 
                </div> 
                  <div class="home-logo pull-right"><img src="<?php echo IMAGE;?>dashboard_event.png" class=""></div>  
				</form> 
                 <div class="static-footer pull-left">
            	 <a href="javascript:void(0);" class="addlink" onclick="setTermsData();"><button type="button" class="btn-nav prevI btn pull-right disable cursor_default isConfirmTrue"> Next<i class="icon-rightarrow"></i></span> </button></a>
                </div>
            </div>
        </div> 
        <div class="col-10 bgicon">
        <div class="pull-right">
       <i class="icon-newman-logo"></i>
       </div> 
        </div>   	  
    </div>
  </div>
  </div>
<body>
<script>

var dashboardUrl                = '<?php echo base_url('dashboard'); ?>';
var userId                      = '<?php echo $this->session->userdata('userid'); ?>';
var setLoginOrRegisterCheckUrl  = '<?php echo base_url(); ?>home/setLoginOrRegisterCheck';	
</script>
