<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
$formFreeRegistration = array(
'name'          => 'free_registration_password',
'id'            => 'registration_password',
'method'        => 'post',
//'class'         => 'wpcf7-form user_password_form',
'accept-charset'=> 'utf-8',
);
$password = array(
'name'          => 'password',
'value'         => '',
'id'            => 'password',
'type'          => 'password',
'class'         => 'large_input',
'placeholder'   => 'Password',
'required'      => '',
'size'          => '40',
'autocomplete'  => 'off',
'tabindex'      => '1',
);

$confirm_password = array(
'name'          => 'confirm_password',
'value'         => '',
'id'            => 'confirm_password',
'type'          => 'password',
'class'         => 'large_input',
'placeholder'   => 'Confirm Email Password',
'required'      => '',
'size'          => '40',
'autocomplete'  => 'off',
'tabindex'      => '2',
); 

$reg_type   = $this->session->userdata('registration_type');
$user_id    = $this->session->userdata('user_id');
$registration_step = $this->session->userdata('registration_step');
$pass_cls   = '';
?>
<div class="container">
  <div class="homepagebgReg">
    <div class="row bgIconRow">  
    	<div class="col-5 bg-register">
            <?php echo form_open(base_url('home/freePassword'), $formFreeRegistration); ?>
        	<div class="regBoxWrap">
            	<div class="static-panel">
            	<h4 class="font-large"><?php echo lang('password_form_heading'); ?></h4>
                </div>
                  <div class="pad35 mt_15">              
                    <div class="regBoxContent">
                    <p class="font-small mB30"> 
                        <?php echo lang('password_form_paragraph1'); ?>
                    </p>
                    <div class="row">
                    <div class="labelDiv pull-left">
                      <label class="form-label text-left"><?php echo lang('password'); ?>
                      <span class="required" aria-required="true">*</span>
                      </label>
                    </div>
                    <?php echo form_input($password); ?>
                    <span id="error-password"></span>
                    </div>
                    <div class="row">
                    <div class="labelDiv pull-left">
                      <label class="form-label text-left"><?php echo lang('confirm_password'); ?>
                      <span class="required" aria-required="true">*</span>
                      </label>
                    </div>
                    <?php echo form_input($confirm_password); ?>
                    <span class="comment-input"><?php echo lang('password_form_paragraph2'); ?></span>
                    <span id="error-confirm_password"></span>
                    </div>
                     <div class="home-logo pull-right"><img src="<?php echo IMAGE; ?>dashboard_event.png" class=""></div> 
                     <input type="hidden" name="user_id" id="user_id" value="<?php if(!empty($user_id)){ echo $this->session->userdata('user_id');}?>" >                  
                </div>  
				</div>
                 <div class="static-footer pull-left">
            	 <button tabindex="6" class="btn-nav prevI btn pull-right" type="submit" name="loginsubmit" id="loginsubmit"> Next</span></button>
                 
                </div>
                <?php echo form_hidden('registration_type', encode($this->config->item('free_registration'))) ?>
            <?php echo form_close(); ?>
            </div>
        </div> 
        <div class="col-10 bgicon">
        <div class="pull-right">
       <i class="icon-newman-logo"></i>
       </div> 
        </div>   	  
    </div>
  </div>
