<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
$formContactUs = array(
    'name' => 'formContactUs',
    'id' => 'formContact',
    'method' => 'post',
    'class' => 'wpcf7-form',
    'data-parsley-validate' => '',
);

$contactFirstName = array(
    'name' => 'contactFirstName',
    'value' => '',
    'id' => 'contactFirstName',
    'type' => 'text',
    'class' => 'xlarge_input input100',
    'placeholder' => lang('contact_us_firstname'),
    'required' => '',
    'size' => '40',
    'autocomplete' => 'off',
    'tabindex' => '1',
    'data-parsley-error-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
);

$contactLastName = array(
    'name' => 'contactLastName',
    'value' => '',
    'id' => 'contactLastName',
    'type' => 'text',
    'class' => 'xlarge_input input100',
    'placeholder' => lang('contact_us_surname'),
    'required' => '',
    'size' => '40',
    'autocomplete' => 'off',
    'tabindex' => '2',
    'data-parsley-error-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
);

$contactEmail = array(
    'name' => 'contactEmail',
    'value' => '',
    'id' => 'contactEmail',
    'type' => 'email',
    'class' => 'xlarge_input input100',
    'placeholder' => lang('contact_us_email'),
    'required' => '',
    'size' => '40',
    'autocomplete' => 'off',
    'tabindex' => '3',
    'data-parsley-trigger' => 'keyup',
    'data-parsley-required-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
);

$contactPhone = array(
    'name' => 'contactPhone',
    'value' => '',
    'id' => 'mobile',
    'type' => 'text',
    'class' => 'xlarge_input input100',
    'placeholder' => '123456789',//lang('contact_us_phone_no'),
    'required' => '',
    'size' => '40',
    'tabindex' => '5',
    'data-parsley-required-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li ',
    'data-parsley-trigger' => 'keyup',
); 

$contactMessage = array(
    'name' => 'contactMessage',
    'value' => '',
    'id' => 'contactMessage',
    'type' => 'textarea',
    'class' => 'xlarge_input input100 form-textarea',
    'rows' => '3',
    'cols' => '500',
    'placeholder' => lang('contact_us_message'),
    'required' => '',
    'tabindex' => '6',
    'data-parsley-error-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
);
?>

<!-- contact us page -->
<div class="modal fade contactus_popup" id="contact_popup">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">        
        <h4 class="modal-title"><?php echo lang('contact_us_title'); ?></h4>
      </div>
      <!-- Start Model body -->
      <div class="modal-body">
		<?php echo form_open(base_url('home/contactus'), $formContactUs); ?>  
        <div class="modal-body-content">
          <div class="row-fluid">
              <div class="labelDiv">
                <label class="form-label" for="firstname"><?php echo lang('contact_us_firstname'); ?></label>
              </div>
              <?php echo form_input($contactFirstName); ?>				
          </div>
          <div class="clearFix display_inline w_100 mT15"></div>
          <div class="row-fluid">
              <div class="labelDiv">
                <label class="form-label" for="surname"><?php echo lang('contact_us_surname'); ?></label>
              </div>
              <?php echo form_input($contactLastName); ?>
          </div>

          <div class="clearFix display_inline w_100 mT15"></div>
          <div class="row-fluid">
              <div class="labelDiv">
                <label class="form-label" for="email"><?php echo lang('contact_us_email'); ?></label>
              </div>
              <?php echo form_input($contactEmail); ?>
          </div>

          <div class="clearFix display_inline w_100 mT15"></div>
          <div class="row-fluid">
              <div class="labelDiv">
                <label class="form-label" for="phonen"><?php echo lang('contact_us_phone_no'); ?></label>
              </div>
              <div class="phone_input">
                <?php //echo form_input($contactPhoneCode); ?>
                <?php echo form_input($contactPhone); ?>
                <a href="javascript:void(0)" class="phone_field_selector"></a>
              </div>
          </div>
		  <input type='hidden' name='mobile' id='input_mobile'>
          <input type='hidden' name='landline' id='input_landline'>	
          <div class="clearFix display_inline w_100 mT15"></div>
          <div class="row-fluid">
              <div class="labelDiv">
                <label class="form-label" for="phonen"><?php echo lang('contact_us_message'); ?></label>
              </div>
			  <?php echo form_textarea($contactMessage); ?>
          </div>
        </div>
        <div class="modal-footer">
            <div class="pull-right">
                <?php
					$sendData['buttonArray'] = array(
						'submit' => array('value' => $this->lang->line('comm_send')),
						'cancle' => true,
						'submit_tabindex' => '7',
						'cancle_tabindex' => '8');
					$this->load->view('common/common_button', $sendData);
                 ?>         
            </div>         	
        </div>     
        <input type="hidden" value="loginpost" name="formaction">
      <?php echo form_close(); ?>   
      </div>            
    <!-- End Model body -->
    </div>
  </div>
</div>
<!--end of contactus popup-->
