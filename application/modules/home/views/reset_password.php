<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
$formResetPassword = array(
    'name'          => 'formResetPass',
    'id'            => 'form_changepwd',
    'method'        => 'post',
    'class'         => 'wpcf7-form',
    'accept-charset' => 'utf-8',
);
$newpassword = array(
    'name'          => 'newpassword',
    'value'         => '',
    'id'            => 'newpassword',
    'type'          => 'password',
    'placeholder'   => lang('home_login_password'),
    'class'         => 'large_input',
    'size'          => '40',
    'maxlength'     => '25',
    'autocomplete' => 'off',
    'tabindex'      => '1',

);

$newpassword2 = array(
    'name'          => 'newpassword2',
    'value'         => '',
    'id'            => 'newpassword2',
    'type'          => 'password',
    'placeholder'   => lang('home_login_password'),
    'class'         => 'large_input',
    'size'          => '40',
    'maxlength'     => '25',
    'autocomplete'  => 'off',
    'tabindex'      => '1',
);
?>
<div class="container">
  <div class="homepagebgReg">
    <div class="row bgIconRow">  
    	<div class="col-5 bg-register">
            <?php echo form_open(base_url('home/reset_password'), $formResetPassword); ?>
        	<div class="regBoxWrap">
            	<div class="static-panel">
            	<h4 class="font-large"><?php //echo lang('password_form_heading'); ?></h4>
                </div>
                  <div class="pad35 mt_15">              
                    <div class="regBoxContent">
                    <p class="font-small mB30"> 
                        Please reset your password.
                    </p>
                    <div class="row">
                    <div class="labelDiv pull-left">
                      <label class="form-label text-left">New Password
                      <span class="required" aria-required="true">*</span>
                      </label>
                    </div>
                    <?php echo form_input($newpassword); ?>
                    <span id="error-newpassword"></span>
                    </div>
                    <div class="row">
                    <div class="labelDiv pull-left">
                      <label class="form-label text-left"><?php echo lang('confirm_password'); ?>
                      <span class="required" aria-required="true">*</span>
                      </label>
                    </div>
                    <?php echo form_input($newpassword2); ?>
                     <input type="hidden" name = "random_string" value="<?php echo $random_sting ?>" >
                    <?php //echo form_input($confirm_password); ?>
                    <span class="comment-input"><?php //echo form_input($confirm_password); ?><?php echo lang('password_form_paragraph2'); ?></span>
                    <span id="error-newpassword2"></span>
                    </div>
                     <div class="home-logo pull-right"><img src="<?php echo IMAGE; ?>dashboard_event.png" class=""></div> 
                     <input type="hidden" name="user_id" id="user_id" value="<?php //if(!empty($user_id)){ echo $this->session->userdata('user_id');}?>" >                  
                </div>  
				</div>
                 <div class="static-footer pull-left">
				 <input type="submit" name="formsubmit" value="Save" class="btn-nav prevI btn pull-right">
            	 
                 
                </div>
                <?php //echo form_hidden('registration_type', encode($this->config->item('free_registration'))) ?>
            <?php echo form_close(); ?>
            </div>
        </div> 
        <div class="col-10 bgicon">
        <div class="pull-right">
       <i class="icon-newman-logo"></i>
       </div> 
        </div>   	  
    </div>
  </div>
