<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * 
 * This class is used to manage for home controller, login, registration, forgot password
 * trouble login, other page like(contact us, abouts etc.)
 *
 * @package:   		auth
 * @author:    		Lokendra Meena
 * @email:  	   	lokendrameena@cdnsol.com
 * @create date:	21-Nov-2013
 * @since:     		Version 1.0
 * @filesource
 */
class Auth {

    private $ci = NULL;

    function __construct($ci) {
        //create instance object
        $this->ci = $ci;
    }

    //----------------------------------------------------------------------------

    /*
     * @default load 
     * @description: This function is used to user login eheck user exist
     * @load: login view
     * @return: void
     *  
     */

    public function index() {
         /*
         * FB login url
         */ 
        $this->ci->load->library('facebook', array(
            'appId'     => FB_APP_ID,
            'secret'    => FB_SECRET,  
        )); 
            
        $userSession = $this->ci->session->userdata('user_data');
        if($userSession == ''){
            $data['login_url']  = $this->ci->facebook->getLoginUrl(array(
                'display'       => 'popup',
                'redirect_uri'  => site_url('home/freeRegistration'), 
                'scope'         => array('email') 
                ));
        }
        /*
         * configration gplus
         */
        $this->ci->load->library('google');  
        $client = new Google_Client();
        $client->setClientId(CLIENT_ID_GPLUS);
        $client->setClientSecret(CLIENT_SECRET_GPLUS);
        $client->setRedirectUri(REDIRECT_URI_GPLUS);
        $client->addScope("email");
        $client->addScope("profile");
        $service = new Google_Service_Oauth2($client);
        
        if ($this->ci->input->get('code')) {
            $client->authenticate($this->ci->input->get('code'));
            $this->ci->session->set_userdata('access_token',$client->getAccessToken());
            header('Location: ' . filter_var(REDIRECT_URI_GPLUS, FILTER_SANITIZE_URL));
            exit;
        }

        if ($this->ci->session->userdata('access_token')) {
            $client->setAccessToken($this->ci->session->userdata('access_token'));
        } else {
            $authUrl = $client->createAuthUrl();
        }
        
        if (isset($authUrl)){ 
            $data['authUrlGplus'] = $authUrl;
        }
        
        $this->ci->session_check->CheckUserLoginSession();
        $this->ci->home_template->load('home_template', 'home_index',$data);
    }

    //--------------------------------------------------------------------------------

    /*
     * @access: public
     * @descrition: This function is used  for user login
     * @return void
     */


    public function userlogin() {

        //call for user login validate
        $this->_loginValidate();

        //check validation is true
        if ($this->ci->form_validation->run($this->ci)) {
            $username = $this->ci->input->post('loginUsername');
            $password = $this->ci->input->post('loginPassword');
            //call for user authentigation
            $this->_checkuserauth($username, $password);
        } else {
            //call for error message
            echo $this->_errormsg();
        }
    }

    //--------------------------------------------------------------------------------

    private function _checkuserauth($username, $password) {

        if ($this->ci->input->post('rememberMe')) {
            $usernameCookie = array(
                'name' => 'loginUsername',
                'value' => $username,
                'expire' => '2592000'
            );
            $this->ci->input->set_cookie($usernameCookie);
        } else {
            //delete remember me password
            delete_cookie("loginUsername");
            delete_cookie("loginPassword");
        }
        //assign normal password to md5 password
        $passwordMd5 = md5($password);
        if ($this->ci->home_model->checkuserexit($username, $passwordMd5)) {
            $userdata = $this->ci->home_model->checkuserexit($username, $passwordMd5);
        if ($userdata->status == '1') {
            if ($userdata->email_verify == '1') {
                if ($this->ci->input->post('rememberMe')) {
                    $passwordCookie = array(
                        'name' => 'loginPassword',
                        'value' => base64_encode($password),
                        'expire' => '2592000'
                    );
                    $this->ci->input->set_cookie($passwordCookie);
                }
                $userdata = $this->ci->home_model->checkuserexit($username, $passwordMd5);
                
                $this->ci->session->set_userdata('userid', $userdata->id);
                $this->ci->session->set_userdata('userdata', $userdata);
                $this->ci->session->set_flashdata('globalmsg', 'Login successfully.');
                $msg = lang('home_error_login_sucess');
                $is_refrer_url = $this->ci->input->post('is_refrer_url');
                
                if(!empty($is_refrer_url) && $is_refrer_url=='1') {
                    $is_refrer_url = getenv('HTTP_REFERER');
                }else{
                    $is_refrer_url = 'false';
                }
                $returnArray = array('msg' => $msg, 'is_success' => 'true','redirect_uri' => $is_refrer_url);
                //$returnArray = array('msg' => $msg, 'is_success' => 'true');
                //set_global_messages($returnArray);
                echo json_encode($returnArray);
            } else {
                $msg = lang('pending_email_msg');
                $returnArray = array('msg' => $msg, 'is_success' => 'false');
                echo json_encode($returnArray);
            }
        }else{
            $msg = lang('home_error_invalid_email');
            $returnArray = array('msg' => $msg, 'is_success' => 'false');
            echo json_encode($returnArray);
            
        }
            
        } else {
            $msg = lang('home_error_invalid_email');
            $returnArray = array('msg' => $msg, 'is_success' => 'false');
            echo json_encode($returnArray);
        }
    }

    //--------------------------------------------------------------------------------

    /*
     * @access: private
     * @description: This function is used set valiation rules for login
     * @return void
     */


    private function _loginValidate() {
        //set validation rules
        $this->ci->form_validation->set_rules('loginUsername', 'email address', 'trim|required|valid_email');
        $this->ci->form_validation->set_rules('loginPassword', 'password', 'trim|required|min_length[6]|max_length[25]');
    }

    //--------------------------------------------------------------------------------


    /*
     * @access: public
     * @descrition: This function is used  for user login
     * @return void
     */


    public function userregister() {

        //call for user login validate
        $this->_registerValidate();

        //check validation is true
        if ($this->ci->form_validation->run($this->ci)) {

            //check emailid exit or not
            if ($this->checkUserEmail()) {
                $msg = lang('home_error_email_already');
                $errorArray = array('msg' => $msg, 'is_success' => 'false');
                echo json_encode($errorArray);
                return true;
            }

            $data['firstname'] = $this->ci->input->post('regisName');
            $data['username'] = $this->ci->input->post('regisEmail');
            $data['email'] = $this->ci->input->post('regisEmail');
            $data['password'] = md5($this->ci->input->post('regisPassword'));
            $data['user_type_id'] = '1'; // this is type of register define in "nm_user_type_master" table
            $data['status'] = '0'; // this is by default user will be enable
            $data['email_verify'] = '1'; // this is by default user will be enable
            $data['status'] = '1'; // this is by default user will be enable
            $data['operand_number'] = getMasterNumber('operand'); // get operand number 
            $data['master_event_id'] = 'AA01'; // get operand number 
            //$data['gender'] 		 = $this->ci->input->post('gender');
            //$regisDay 			 = $this->ci->input->post('regisDay');
            //$regisMonth 			 = $this->ci->input->post('regisMonth');
            //$regisYear 			 = $this->ci->input->post('regisYear');
            //$data['dob'] 			 = $regisYear.'-'.$regisMonth.'-'.$regisDay; 
            $userdata = $this->ci->home_model->registerinsert($data);

            if ($userdata > 0) {

                //send varification email to user email
                //$this->_registerVerification($data);

                $msg = lang('home_error_register_success');
                $returnArray = array('msg' => $msg, 'is_success' => 'true');
            } else {
                $msg = lang('home_error_register_fail');
                $returnArray = array('msg' => $msg, 'is_success' => 'false');
            }
            echo json_encode($returnArray);
        } else {
            //call for error message
            echo $this->_errormsg();
        }
    }

    //--------------------------------------------------------------------------------

    /*
     * @access: private
     * @description: This function is used set valiation rules for register
     * @return void
     */

    private function _registerValidate() {
        //set validation rules
        $this->ci->form_validation->set_rules('regisName', 'name', 'trim|required');
        $this->ci->form_validation->set_rules('regisEmail', 'email', 'trim|required|valid_email');
        $this->ci->form_validation->set_rules('regisPassword', 'password', 'trim|required|matches[regisConfirmPassword]|min_length[5]|max_length[25]');
        $this->ci->form_validation->set_rules('regisConfirmPassword', 'confirm password', 'trim|required|min_length[5]|max_length[25]');
        /* 	$this->ci->form_validation->set_rules('gender', 'gender', 'trim|required');
          $this->ci->form_validation->set_rules('regisMonth', 'month', 'trim|required');
          $this->ci->form_validation->set_rules('regisDay', 'day', 'trim|required');
          $this->ci->form_validation->set_rules('regisYear', 'year', 'trim|required'); */
    }

    //--------------------------------------------------------------------------------

    /*
     * @access: private
     * @description: This function is used send account activation email
     * @return void
     */

    private function _registerVerification($emailData) {

        //set email data
        $userEmail = $emailData['email'];
        $emailData['activationUrl'] = base_url('home/accountactivation') . '/' . PHP_EOL . encode($userEmail);
        $subject = 'newmanevents account activation.';
        $this->ci->load->library('email');
        $this->ci->email->from($this->ci->config->item('from_email'));
        $this->ci->email->to($userEmail);
        $this->ci->email->subject($subject);
        $message = $this->ci->load->view('email_view/register_email', $emailData, true);
        $this->ci->email->message($message);
        $this->ci->email->send();
    }

    //--------------------------------------------------------------------------------
    // this is detais for the day registant 
    /*
     * @access: public
     * @description: This function is used to user account activation 
     * @return void
     */

    public function accountactivation($encryptCode) {

        //condition for user enter link with code 
        if (empty($encryptCode)) {
            $returnArray = array('msg' => lang('home_error_account_active_error'), 'is_success' => 'false');
            set_global_messages($returnArray);
            redirect(base_url());
        }

        $userEmail = decode($encryptCode); // decode encrypted code into emailid
        //get user data
        $where = array('email' => $userEmail, 'user_type_id' => '1');
        $userdata = $this->ci->common_model->getDataFromTabel('user', 'id,email_verify,status,email', $where);

        //check email id exist
        if (!empty($userdata)) {

            if (!empty($userdata)) {
                $userdata = $userdata[0];
            }
            $updateWhere = array('email' => $userdata->email, 'user_type_id' => '1');
            $updateData = array('email_verify' => '1', 'status' => '1');

            if ($userdata->email_verify == "0") {
                //update user data
                $this->ci->common_model->updateDataFromTabel('user', $updateData, $updateWhere);
                //set error message
                $returnArray = array('msg' => lang('home_error_account_active'), 'is_success' => 'true');
                set_global_messages($returnArray);
                redirect(base_url());
            } else {
                //set error message
                $returnArray = array('msg' => lang('home_error_account_active_already'), 'is_success' => 'false');
                set_global_messages($returnArray);
                redirect(base_url());
            }
        } else {
            //set error message
            $returnArray = array('msg' => lang('home_error_account_active_error'), 'is_success' => 'false');
            set_global_messages($returnArray);
            redirect(base_url());
        }
    }

    //---------------------------------------------------------------------------

    /*
     * @access: public
     * @description: This methos is used to send encrypted password to user email id 
     * @return: json
     */

    public function forgotpassword() {

        //set validation
        $this->ci->form_validation->set_rules('forgotEmail', 'email address', 'trim|required|valid_email');

        //check validation is true
        if ($this->ci->form_validation->run($this->ci)) {
            //set email data
            $userEmail = $this->ci->input->post('forgotEmail');

            //get user data
            $where = array('email' => $userEmail);
            $userdata = $this->ci->common_model->getDataFromTabel('user', 'id,firstname,email,password', $where);

            if (!empty($userdata)) {

                $userdata = $userdata[0];

                //prepare email send data
                $userName = ucfirst($userdata->firstname);
                $emailData['firstname'] = $userName;
                $random_string = base64_encode(randomnumber('4'));
                $emailData['random_string'] = $random_string;


                //Link send through email 

                $updateData['forgot_pass_random_string'] = $random_string;

                $updateCondi = array('email' => $userEmail);
                $this->ci->common_model->updateDataFromTabel('user', $updateData, $updateCondi);

                //insert old password in password summery table
                $insertData['user_id'] = $userdata->id;
                $insertData['old_password'] = $userdata->password;
                $this->ci->common_model->addDataIntoTabel('password_chagned_summery', $insertData);

/*
                //send email to user email via mandrill
                $this->ci->load->library('mandrill/email_template');
                $template_name = $this->ci->config->item('forgotpassword_template_name');
                $template_message = array(
                    'to' => array(
                        array(
                            'email' => $userEmail,
                            'name' => $userName,
                            'type' => 'to'
                        )
                    ),
                    'merge' => true,
                    'global_merge_vars' => array(
                        array(
                            'name' => 'first_name',
                            'content' => $userName
                        ),
                        array(
                            'name' => 'email',
                            'content' => $userEmail
                        ),
                        array(
                            'name' => 'contact_link',
                            'content' => base_url() . '?#contact_email'
                        ),
                        array(
                            'name' => 'reset_link',
                            'content' => base_url() . 'home/reset_password/' . $random_string
                        )
                    )
                );
                $this->ci->email_template->send_template_email($template_name, $template_message);
                //send email to user email via mandrill
 */   
                
                      /* Sendgrid */
                    $apiKey = ADMINNEWMAN_APIKEY;
                    $this->ci->load->helper('sendgrid');
                    $addTeam_templateId = ForGot_Pass; //
                    $paramArray = array(
                    'sub' => array(':firstname' => array($userName),':reset_link'=>array(base_url().'home/reset_password/'.$random_string),':contactLink'=>array(base_url().'?#contact_email'),'newman_url'=>array(base_url())),
                    'filters' => array('templates' => array('settings' => array('enable' => 1, 'template_id' => $addTeam_templateId)))
                    );
                    $tempSubject = '?';
                    $templateBody = '.';
                    $toId = $userEmail;//$admin_email;
                    sendgrid_mail_template($addTeam_templateId,$tempSubject,$templateBody,$toId,$paramArray,$apiKey,adminFromEmail);


                

                $secure_email_format = changeEmailIdFormat($userEmail); // this function change the email format like (n****@g***.com)
                $userEmailId = sprintf($this->ci->lang->line('forgot_email_sent_popup_success1'), $secure_email_format);
                //set success message
                $msg = lang('home_error_newpassword_send');
                $returnArray = array('msg' => $msg, 'is_success' => 'true', 'useremailid' => $userEmailId);
                echo json_encode($returnArray);
            } else {
                //set error message
                $msg = lang('home_error_newpassword_fail');
                $returnArray = array('msg' => $msg, 'is_success' => 'false');
                echo json_encode($returnArray);
            }
        } else {

            //call for error message
            echo $this->_errormsg();
        }
    }

    //---------------------------------------------------------------------------

    /*
     * @access: public
     * @description: This methos is used to send trouble logging request to admin
     * @return: json
     */

    public function troublelogging() {

        //set validation
        $this->ci->form_validation->set_rules('troubleFirstName', 'first name', 'trim|required');
        $this->ci->form_validation->set_rules('troubleLastName', 'surname name', 'trim|required');
        $this->ci->form_validation->set_rules('troubleLoginEmail', 'email address', 'trim|required|valid_email');
        $this->ci->form_validation->set_rules('troubleLoginMessage', 'message', 'trim|required');

        //check validation is true
        if ($this->ci->form_validation->run($this->ci)) {

            $userEmail = $this->ci->input->post('troubleLoginEmail');

            //get user data
            $where = array('email' => $userEmail);
            $userdata = $this->ci->common_model->getDataFromTabel('user', 'email', $where);

            if (!empty($userdata)) {

                $userdata = $userdata[0];

                $emailData['adminName'] = 'Admin';
                $emailData['firstname'] = $this->ci->input->post('troubleFirstName');
                $emailData['surnamename'] = $this->ci->input->post('troubleLastName');
                $emailData['emailAddress'] = $userEmail;
                $emailData['message'] = $this->ci->input->post('troubleLoginMessage');

                $subject = 'newmanevents trouble logging in request';
                $this->ci->load->library('email');
                $this->ci->email->from($this->ci->config->item('from_email'));
                $this->ci->email->to($this->ci->config->item('admin_email'));
                $this->ci->email->subject($subject);
                $message = $this->ci->load->view('email_view/trouble_logging_email', $emailData, true);
                $this->ci->email->message($message);
                $this->ci->email->send();

                //set success message
                $msg = lang('home_error_trouble_success');
                $returnArray = array('msg' => $msg, 'is_success' => 'true');
                echo json_encode($returnArray);
            } else {
                //set error message
                $msg = lang('home_error_trouble_fail');
                $returnArray = array('msg' => $msg, 'is_success' => 'false');
                echo json_encode($returnArray);
            }
        } else {

            //call for error message
            echo $this->_errormsg();
        }
    }

    //---------------------------------------------------------------------------

    /*
     * @access: public
     * @description: This methos is used to send contact us request to admin
     * @return: json
     */

    public function contactus() {

        //set validation 
        $this->ci->form_validation->set_rules('contactFirstName', lang('contact_first_name'), 'trim|required');
        $this->ci->form_validation->set_rules('contactLastName', lang('contact_surname'), 'trim|required');
        $this->ci->form_validation->set_rules('contactEmail', lang('contact_email'), 'trim|required|valid_email');
        //$this->ci->form_validation->set_rules('contactPhoneCode', lang('contact_phonecode'), 'trim|required|numeric|min_length[2]|max_length[2]');
        $this->ci->form_validation->set_rules('contactPhone', lang('contact_phoneno'), 'trim|required');
        $this->ci->form_validation->set_rules('contactMessage', lang('contact_message'), 'trim|required');

        //check validation is true
        if ($this->ci->form_validation->run($this->ci)) {
            $emailData['adminName'] = 'Admin';
            $emailData['firstname'] = $this->ci->input->post('contactFirstName');
            $emailData['surnamename'] = $this->ci->input->post('contactLastName');
            
            if($this->ci->input->post('mobile')) {
               $emailData['mobile'] = $this->ci->input->post('mobile');
            }  else {
               $emailData['mobile'] = $this->ci->input->post('contactPhone'); 
            }
            $emailData['contact'] = $emailData['mobile'];
            if($this->ci->input->post('landline')) {
               $emailData['contact'] .= '/'.$this->ci->input->post('landline');
            }  else if ($this->ci->input->post('mobile')!='' && $this->ci->input->post('landline')=='') {
                $emailData['contact'] .= '/'.$this->ci->input->post('contactPhone');
            }

            
            
            $emailData['emailAddress'] = $this->ci->input->post('contactEmail');
            $emailData['message'] = $this->ci->input->post('contactMessage');

            //send email to admin via mandrill
            $this->ci->load->library('mandrill/email_template');
            $template_name = $this->ci->config->item('contactus_user_template_name');
            $template_message = array(
                'to' => array(
                    array(
                        'email' => $emailData['emailAddress'],
                        'name' => ucfirst($emailData['firstname']),
                        'type' => 'to'
                    )
                ),
                'merge' => true,
                'global_merge_vars' => array(
                    array(
                        'name' => 'first_name',
                        'content' => ucfirst($emailData['firstname'])
                    ),
                    array(
                        'name' => 'contactus_message',
                        'content' => $emailData['message']
                    )
                )
            );
            $this->ci->email_template->send_template_email($template_name, $template_message);


            $admin_template_name = $this->ci->config->item('contactus_admin_template_name');
            $admin_template_message = array(
                'to' => array(
                    array(
                        'email' => $this->ci->config->item('admin_email'),
                        'name' => 'Admin',
                        'type' => 'to'
                    )
                ),
                'merge' => true,
                'global_merge_vars' => array(
                    array(
                        'name' => 'first_name',
                        'content' => ucfirst($emailData['firstname'])
                    ),
                    array(
                        'name' => 'last_name',
                        'content' => ucfirst($emailData['surnamename'])
                    ),
                    array(
                        'name' => 'phone',
                        'content' => $emailData['contact']
                    ),
                    array(
                        'name' => 'email',
                        'content' => $emailData['emailAddress']
                    ),
                    array(
                        'name' => 'contactus_message',
                        'content' => $emailData['message']
                    )
                )
            );
            $this->ci->email_template->send_template_email($admin_template_name, $admin_template_message);
            
            //send email to admin via mandrill                     
            //set success message
            $msg = lang('home_contact_us_success');
            $returnArray = array('msg' => $msg, 'is_success' => 'true');
            echo json_encode($returnArray);
        } else {

            //call for error message
            echo $this->_errormsg();
        }
    }

    //---------------------------------------------------------------------------

    /*
     * @access: private
     * @description: This methos is used to show set error for validation
     * @return: json
     */

    private function _errorMsg() {
        $error = $this->ci->form_validation->error_array();
        return json_encode(array('msg' => $error, 'is_success' => 'false'));
    }

    //--------------------------------------------------------------------------------


    /*
     * @access: public
     * @description: This methos is used to show set error for validation
     * @return: json
     */

    public function checkUserEmail() {
        $regisEmail = $this->ci->input->post('regisEmail');
        $where = array('email' => $regisEmail, 'user_type_id' => '1');
        $userdata = $this->ci->common_model->getDataFromTabel('user', 'email', $where);
        if ($userdata) {
            return true;
        } else {
            return false;
        }
    }

    //------------------------------------------------------------------------------------------

    /*
     * @description: This function is used to show abouts
     * @load: aboutus view
     * @return: void
     *  
     */

    function aboutus() {
        $this->ci->home_template->load('home_template', 'aboutus_view');
    }

    //------------------------------------------------------------------------------------------

    /*
     * @description: This function is used to show features
     * @load: aboutus view
     * @return: void
     *  
     */

    function features() {
        $this->ci->home_template->load('home_template', 'features');
    }
    
    /*
     * @description: This function is used to show feature managers
     * @load: managers view
     * @return: void
     *  
     */ 
    function managers() {
        $this->ci->home_template->load('home_template', 'features_managers');
    }
        
    /*
     * @description: This function is used to show feature delegates
     * @load: delegates view
     * @return: void
     *  
     */ 
    function delegates() {
        $this->ci->home_template->load('home_template', 'features_delegates');
    }
    
    /*
     * @description: This function is used to show feature suppliers
     * @load: suppliers view
     * @return: void
     *  
     */ 
    function suppliers() {
        $this->ci->home_template->load('home_template', 'features_suppliers');
    }
    
    /*
     * @description: This function is used to show feature clients
     * @load: clients view
     * @return: void
     *  
     */ 
    function clients() {
        $this->ci->home_template->load('home_template', 'features_clients');
    }
    
    /*
     * @description: This function is used to show feature resources
     * @load: resources view
     * @return: void
     *  
     */ 
    function resources() {
        $this->ci->home_template->load('home_template', 'features_resources');
    }

    /*
     * @description: This function is used to show packages
     * @load: packages view
     * @return: void
     *  
     */

    function packages() {
        $this->ci->home_template->load('home_template', 'packages_view');
    }

    public function alpha_numeric($str) {
        if (preg_match('#[0-9]#', $str) && preg_match('#[a-zA-Z]#', $str)) {
            return TRUE;
        }
        return FALSE;
    }

    function reset_password() {
        if ($this->ci->input->post('newpassword')) {
            $this->ci->form_validation->set_rules('newpassword', 'password', 'trim|required|matches[newpassword2]|min_length[6]|max_length[25]');
            $this->ci->form_validation->set_rules('newpassword2', 'confirm password', 'trim|required|min_length[6]|max_length[25]');
            if ($this->ci->form_validation->run($this->ci) && $this->alpha_numeric($this->ci->input->post('newpassword'))) {
                $new_pass = md5($this->ci->input->post('newpassword'));
                $random_string = $this->ci->input->post('random_string');

                // fetch user info from DB
                $where = array('forgot_pass_random_string' => $random_string);
                $fetchuserdata = $this->ci->common_model->getDataFromTabel('user', '*', $where);
                $emailData['firstname'] = $fetchuserdata[0]->firstname;
                $userEmail = $fetchuserdata[0]->email;
                
                $updateWhere = array('forgot_pass_random_string' => $random_string,);
                $updateData = array('password' => $new_pass, 'forgot_pass_random_string' => '');

                //update new password
                $this->ci->common_model->updateDataFromTabel('user', $updateData, $updateWhere);

                //send email to user via mandrill
                /*
                $this->ci->load->library('mandrill/email_template');
                $template_name = $this->ci->config->item('resetpassword_template_name');
                $template_message = array(
                    'to' => array(
                        array(
                            'email' => $userEmail,
                            'name' => $emailData['firstname'],
                            'type' => 'to'
                        )
                    ),
                    'merge' => true,
                    'global_merge_vars' => array(
                        array(
                            'name' => 'contact_link',
                            'content' => base_url() . '?#contact_email'
                        )
                    )
                );
                $this->ci->email_template->send_template_email($template_name, $template_message);
                */
                //send email to user via mandrill  
                    $apiKey = VENUES_APIKEY;
                    $this->ci->load->helper('sendgrid');
                    $reset_templateId = RESET_templateId; //
                    $paramArray = array(
                    'filters' => array('templates' => array('settings' => array('enable' => 1, 'template_id' => $reset_templateId)))
                    );
                    $tempSubject = '.';
                    $templateBody = '.';
                    $toId = $userEmail;//$admin_email;
                    sendgrid_mail_template($reset_templateId,$tempSubject,$templateBody,$toId,$paramArray,$apiKey,adminFromEmail);
                    
                echo json_encode(array('msg' => "Success", 'is_success' => 'true'));
            } else {
                echo json_encode(array('msg' => $this->ci->lang->line('password_reset_server_validation'), 'is_success' => 'false'));
            }
        } else {
            $view_data['msg'] = "Oops! Something went wrong. Please check the link.";
            $random_string = $this->ci->uri->segment(3);
            if ($random_string != "") {
                $where = array('forgot_pass_random_string' => $random_string);
                $userdata = $this->ci->common_model->getDataFromTabel('user', 'email', $where);
                if (!empty($userdata)) {
                    $view_data["secureEmailId"] = changeEmailIdFormat($userdata[0]->email);
                    $view_data["random_sting"] = $random_string;
                    $this->ci->home_template->load('home_template', 'reset_password', $view_data);
                   
                } else {
                    $this->ci->home_template->load('home_template', 'error_reset_paasword', $view_data);
                }
            } else {
                $this->ci->home_template->load('home_template', 'error_reset_paasword', $view_data);
            }
        }
    }

    //--------------------------------------------------------------------------------

    /*
     * @access: public
     * @description: This methos is used to destory login user data
     * 
     */

    public function logout() {
        $this->ci->load->library('facebook', array(
            'appId'     => FB_APP_ID,
            'secret'    => FB_SECRET, 
        ));
        $user = $this->ci->facebook->destroySession();
        $this->ci->session->unset_userdata('userid');
        $this->ci->session->unset_userdata('first_name');
        $this->ci->session->unset_userdata('last_name');
        $this->ci->session->unset_userdata('userLogo');
        $this->ci->session->unset_userdata('username');
        $this->ci->session->sess_destroy();
    }

    public function registration() {

        if ($this->ci->input->post('registration_type')) {
            $this->ci->form_validation->set_rules('registerUsername', 'Name', 'trim|required');
            $this->ci->form_validation->set_rules('registerUserSurname', 'Surname', 'trim|required');
            $this->ci->form_validation->set_rules('email', 'Email', 'trim|required|valid_email'); //|is_unique[user.email]
            $this->ci->form_validation->set_rules('confirm_email', 'Confirm email', 'trim|required|matches[email]');
            //$this->ci->form_validation->set_message('is_unique', lang('is_unique_email_custom_message'));
            if ($this->ci->form_validation->run($this->ci)) {
                $registration_type = decode($this->ci->input->post('registration_type'));
                $data = array(
                    'firstname' => $this->ci->input->post('registerUsername'),
                    'lastname' => $this->ci->input->post('registerUserSurname'),
                    'username' => $this->ci->input->post('email'),
                    'email' => $this->ci->input->post('email'),
                    'company_name' => $this->ci->input->post('organisation'),
                    'registration_type' => $registration_type
                );


                $User_id = $this->ci->common_model->addDataIntoTabel('user', $data);
                if ($User_id != "") {
                    $popup_content = sprintf($this->ci->lang->line('please_check_your_email_and_click_verify'), $data['email']);
                    $returnArray = array('msg' => 'success', 'is_success' => 'true','user_id' => $User_id, 'content' => $popup_content);
					if($registration_type == 1){
						$this->ci->session->set_userdata('registration_type', $registration_type);
						$this->ci->session->set_userdata('user_id', $User_id);
						$this->ci->session->set_userdata('registration_step', 2);
						$this->ci->session->set_userdata('first_name', $data['firstname']);
					}
					if($registration_type != 1){
						// Set email body
						$random_string = encode($User_id);

						//send email to user via mandrill
						/*
                        $this->ci->load->library('mandrill/email_template');
						$template_name = $this->ci->config->item('registration_template_name');
						$template_message = array(
							'to' => array(
								array(
									'email' => $data['email'],
									'name' => 'Admin',
									'type' => 'to'
								)
							),
							'merge' => true,
							'global_merge_vars' => array(
								array(
									'name' => 'activation_link',
									'content' => base_url() . 'home/verify_email/' . $random_string
								),
								array(
									'name' => 'contact_link',
									'content' => base_url() . '?#contact_email'
								),
								array(
									'name' => 'contact_first_name',
									'content' => $data['firstname']
								)
							)
						);
						$this->ci->email_template->send_template_email($template_name, $template_message);
                        */
                        
                        /* Sendgrid */
                    $this->ci->load->helper('sendgrid');
                    $apiKey = ADMINNEWMAN_APIKEY;                 
                    $facebook_url    = $this->ci->config->item('facebook_url'); #$config['facebook_url']
                    $twitter_url     = $this->ci->config->item('twitter_url'); #$config['facebook_url']
                    $g_plus_url      = $this->ci->config->item('g_plus_url'); #$config['facebook_url']
                    $newmanevent_url = $this->ci->config->item('newmanevent_url'); #$config['facebook_url']
                    
                    $addTeam_templateId = ADDTeam_templateId; //
                    $paramArray = array(
                    'sub' => array(':firstname' => array($data['firstname']),':activation_link'=>array(base_url().'home/verify_email/'.$random_string),':contactLink'=>array(base_url().'?#contact_email'),':newmanvenue_url'=>array($newmanevent_url),':twitter_url'=>array($twitter_url),':g_plus_url'=>array($g_plus_url),':facebook_url'=>array($facebook_url)),
                    'filters' => array('templates' => array('settings' => array('enable' => 1, 'template_id' => $addTeam_templateId)))
                    );
                    $tempSubject = '.';
                    $templateBody = '.';
                    $toId = $data['email'];//$admin_email;
                    sendgrid_mail_template($addTeam_templateId,$tempSubject,$templateBody,$toId,$paramArray,$apiKey,adminFromEmail);
                        
						//send email to user via mandrill
					}

                    echo json_encode($returnArray);
                } else {
                    echo json_encode(array('msg' => lang('record_insert_error'), 'is_success' => 'false'));
                }
            } else {
                echo $this->_errormsg();
            }
        }
    } // end function registration

    /* Apply free registration */
	public function freeRegistration() {
		$user_id = $this->ci->session->userdata('user_id');
		$registration_type = $this->ci->session->userdata('registration_type');
		$registration_step = $this->ci->session->userdata('registration_step');
		if(!empty($user_id) && !empty($registration_type) && !empty($registration_step)){
			if ($registration_step=='2'){
				redirect(base_url().'home/freePassword');
			} else if($registration_step=='3'){
				redirect(base_url().'home/freeTerms');
			}
		}

         /*
         * FB intregation
         */ 
        $this->ci->load->library('facebook', array(
            'appId'     => FB_APP_ID,
            'secret'    => FB_SECRET,  
        )); 
            
        $userSession = $this->ci->session->userdata('user_data');
        if($userSession == ''){
            $data['login_url']  = $this->ci->facebook->getLoginUrl(array(
                'display'       => 'popup',
                'redirect_uri'  => site_url('home/freeRegistration'), 
                'scope'         => array('email') 
                ));
        }
        $user = $this->ci->facebook->getUser();
        if ($user) {
            try {
                $data['user_profile'] = $this->ci->facebook->api('/me?fields=email,first_name,last_name');
                $picture = $this->ci->facebook->api("/me/picture","GET", array ('redirect' => false,'type' => 'large' ) );
            } catch (FacebookApiException $e) {
                $user = null;
            }
        } 
        if($user){
            $facebook_email = $data['user_profile']['email']; 	
            if($facebook_email != ''){
                $response = curlCall($this->ci->config->item('newmanUrl').'api/checkuseremail',array('email'=>$facebook_email));
                $response = json_decode($response);
                if($response->is_success == 'true'){
                    $saveData['firstname']  = $data['user_profile']['first_name'];
                    $saveData['lastname']   = $data['user_profile']['last_name'];
                    $saveData['email']      = $data['user_profile']['email'];
                    $saveData['fb_id']      = $data['user_profile']['id'];
                    $saveData['fb_image']   = $picture['data']['url'];
                    
                    $isCheckedRegister      = $this->ci->session->userdata('isCheckedRegister');
                    $status = $isCheckedRegister;
                    if($isCheckedRegister=='1'){
                        $saveData['isTermsNConditionTrue'] = 1;
                    }else {
                        $saveData['isTermsNConditionTrue'] = 0;
                    }
                    
                    $response = curlCall($this->ci->config->item('newmanUrl').'api/register',$saveData);
                    $response = json_decode($response);
                    if($response->is_success=='true'){
                        if($status==1) {
                            $this->ci->session->set_userdata('userid',$response->user_id);
                            $this->ci->session->set_userdata('first_name',$data['user_profile']['first_name']);
                            $this->ci->session->set_userdata('last_name',$data['user_profile']['last_name']);
                            $this->ci->session->set_userdata('userLogo',$picture['data']['url']);
                            redirect('dashboard');
                        }else {
                            $this->ci->session->set_userdata('userid',$response->user_id);
                            $this->ci->session->set_userdata('first_name',$data['user_profile']['first_name']);
                            $this->ci->session->set_userdata('last_name',$data['user_profile']['last_name']);
                            $this->ci->session->set_userdata('userLogo',$picture['data']['url']);
                            redirect('home/isLoginOrRegisterCheck');
                        }
                    }else{
                        return false;
                    }
                }else {
                    $reg_user_id        =   $response->user_data[0]->id;
                    $firstname          =   $response->user_data[0]->firstname;
                    $lastname           =   $response->user_data[0]->lastname;
                    $saveData['email']  =   $data['user_profile']['email'];
                    $saveData['fb_id']  =   $data['user_profile']['id'];
                    
                    $saveData['user_id'] = $response->user_data[0]->id;
                    if(empty($response->user_data[0]->userImageStatus)){
                        $saveData['logo_image'] = $picture['data']['url'];
                    }
                    
                    $status = $response->user_data[0]->isTermsNConditionTrue;
                    
                    $response = curlCall($this->ci->config->item('newmanUrl').'api/updateUser',$saveData);
                    $response = json_decode($response);
                    if($response->is_success=='true'){
                        if($status==1) {
                            $this->ci->session->set_userdata('userid',$reg_user_id);
                            $this->ci->session->set_userdata('first_name',$firstname);
                            $this->ci->session->set_userdata('last_name',$lastname);
                            redirect('dashboard');
                        }else {
                            $this->ci->session->set_userdata('userid',$reg_user_id);
                            $this->ci->session->set_userdata('first_name',$firstname);
                            $this->ci->session->set_userdata('last_name',$lastname);
                            redirect('home/isLoginOrRegisterCheck');
                        }
                    }				
            }
        }
        }
        
        
        /*
         * configration gplus
         */
        $this->ci->load->library('google');  
        $client = new Google_Client();
        $client->setClientId(CLIENT_ID_GPLUS);
        $client->setClientSecret(CLIENT_SECRET_GPLUS);
        $client->setRedirectUri(REDIRECT_URI_GPLUS);
        $client->addScope("email");
        $client->addScope("profile");
        $service = new Google_Service_Oauth2($client);
        
        if ($this->ci->input->get('code')) {
            $client->authenticate($this->ci->input->get('code'));
            $this->ci->session->set_userdata('access_token',$client->getAccessToken());
            header('Location: ' . filter_var(REDIRECT_URI_GPLUS, FILTER_SANITIZE_URL));
            exit;
        }

        if ($this->ci->session->userdata('access_token')) {
            $client->setAccessToken($this->ci->session->userdata('access_token'));
        } else {
            $authUrl = $client->createAuthUrl();
        }
        
        if (isset($authUrl)){ 
            $data['authUrlGplus'] = $authUrl;
        }else {
            $user = $service->userinfo->get();
            $this->gplusUserAuthentication($user);
        }
        /* End gplus code */
    
		if ($this->ci->input->post('registration_type')) {
            
            
            
            $this->ci->form_validation->set_rules('registerUsername', 'First Name', 'trim|required');
            $this->ci->form_validation->set_rules('registerUserSurname', 'Last Name', 'trim|required');
            $this->ci->form_validation->set_rules('email', 'Email', 'trim|required|valid_email'); //|is_unique[user.email]
            $this->ci->form_validation->set_rules('confirm_email', 'Confirm email', 'trim|required|matches[email]');
            if ($this->ci->form_validation->run($this->ci)) {
                $where_data = array('email'=>$this->ci->input->post('email'));
                //, 'user_type_id'=>1, 'registration_type'=>1)
                $user_details = $this->ci->common_model->getDataFromTabel('user','*',$where_data);
                if(is_array($user_details)){
                    $count = 1; 
                    } else {
                     $count = 0; 
                 }
				if($count > 0){
					echo json_encode(array('msg' => lang('is_unique_email_custom_message'), 'is_success' => 'false'));
				}else{
                    
					$registration_type = decode($this->ci->input->post('registration_type'));
					$data = array(
						'firstname'         => $this->ci->input->post('registerUsername'),
						'lastname'          => $this->ci->input->post('registerUserSurname'),
						'username'          => $this->ci->input->post('email'),
						'email'             => $this->ci->input->post('email'),
						'company_name'      => $this->ci->input->post('organization'),
						//'newman_nickname' => $this->ci->input->post('newman_nickname'),
						'user_type_id'      => 1,
						'operand_number'    => getMasterNumber('operand'),
						'master_event_id'   => 'AA01', // need to be dynamic based on 16 bits 
                        'status'            => '1',    
						'registration_type' => $registration_type
					);
					$User_id = $this->ci->common_model->addDataIntoTabel('user', $data);
                    
					if ($User_id != "") {
						$returnArray = array('msg' => 'success', 'is_success' => 'true','user_id' => $User_id,'url'=>'home/freePassword');
                        $this->ci->common_model->addDataIntoTabel('user_details', array('user_id'=>$User_id));
						$this->ci->session->set_userdata('registration_type', $registration_type);
						$this->ci->session->set_userdata('user_id', $User_id);
						$this->ci->session->set_userdata('registration_step', 2);
                        $this->ci->session->set_userdata('first_name',$data['firstname']);
                        $this->ci->session->set_userdata('user_email',$data['email']);
						echo json_encode($returnArray);
					} else {
						echo json_encode(array('msg' => lang('record_insert_error'), 'is_success' => 'false'));
					}
                }
            } else {
                echo $this->_errormsg();
            }
        } else {
			$this->ci->home_template->load('home_template', 'packages/apply_free',$data);
		}
    }
    // Funxction call from freeRegistration method for gplus registration // 
    public function gplusUserAuthentication($user='') {
        if(!empty($user)){
            $gplus_email = $user->email;
            if($gplus_email != ''){
                $response = curlCall($this->ci->config->item('newmanUrl').'api/checkuseremail',array('email'=>$gplus_email));
                $response = json_decode($response);
                
                if($response->is_success == 'true'){
                    $saveData['firstname']  = $user->givenName;
                    $saveData['lastname']   = $user->familyName;
                    $saveData['email']      = $user->email;
                    $saveData['gplus_id']   = $user->id;
                    $saveData['gplus_image'] = $user->picture;
                    
                    $isCheckedRegister = $this->ci->session->userdata('isCheckedRegister');
                    $status = $isCheckedRegister;
                    if($isCheckedRegister=='1'){
                        $saveData['isTermsNConditionTrue'] = 1;
                    }else {
                        $saveData['isTermsNConditionTrue'] = 0;
                    }
                    
                    $response = curlCall($this->ci->config->item('newmanUrl').'api/register',$saveData);
                    $response = json_decode($response);
                    
                    $reg_user_id    =   $response->user_id;
					$firstname      =   $saveData['firstname'];
					$lastname       =   $saveData['lastname'];
                    
                    if($response->is_success=='true'){
                       if($status==1) {
                            $this->ci->session->set_userdata('userid',$reg_user_id);
                            $this->ci->session->set_userdata('first_name',$firstname);
                            $this->ci->session->set_userdata('last_name',$lastname);
                            redirect('dashboard');
                        }else {
                            $this->ci->session->set_userdata('userid',$reg_user_id);
                            $this->ci->session->set_userdata('first_name',$firstname);
                            $this->ci->session->set_userdata('last_name',$lastname);
                            redirect('home/isLoginOrRegisterCheck');
                        }
                        
                    }else{
                        return false;
                    }
                }else {

                    $reg_user_id            =   $response->user_data[0]->id;
                    $firstname              =   $response->user_data[0]->firstname;
                    $lastname               =   $response->user_data[0]->lastname;
                    $saveData['email']      =   $user->email;
                    $saveData['gplus_id']   =   $user->id;
                    $saveData['user_id']    =   $response->user_data[0]->id;
                    if(empty($response->user_data[0]->userImageStatus)){
                        $saveData['logo_image'] = $user->picture;
                    }
                    
                    $this->ci->session->set_userdata('userLogo',$user->picture);
                    $status = $response->user_data[0]->isTermsNConditionTrue;
                    $response = curlCall($this->ci->config->item('newmanUrl').'api/updateUser',$saveData);
                    $response = json_decode($response);
                    if($response->is_success=='true'){
                        if($status==1) {
                            $this->ci->session->set_userdata('userid',$reg_user_id);
                            $this->ci->session->set_userdata('first_name',$firstname);
                            $this->ci->session->set_userdata('last_name',$lastname);
                            redirect('dashboard');
                        }else {
                            $this->ci->session->set_userdata('userid',$reg_user_id);
                            $this->ci->session->set_userdata('first_name',$firstname);
                            $this->ci->session->set_userdata('last_name',$lastname);
                            redirect('home/isLoginOrRegisterCheck');
                        }
                    }				
                }
            }
        }
    }



	public function freePassword() {
        //print_r($_POST);die;
		$user_id = $this->ci->session->userdata('user_id');
		$registration_type = $this->ci->session->userdata('registration_type');
		$registration_step = $this->ci->session->userdata('registration_step');
		if(!empty($user_id) && !empty($registration_type) && !empty($registration_step)){
			if($registration_step=='3'){
				redirect(base_url().'home/freeTerms');
			}
		}
		
		if (!empty($user_id) && !empty($registration_type) && !empty($registration_step) && $registration_step=='2') {
			if ($this->ci->input->post('registration_type')) {
				$this->ci->form_validation->set_rules('password', 'Password', 'trim|required');
				$this->ci->form_validation->set_rules('confirm_password', 'Confirm Password', 'trim|required');
				$this->ci->form_validation->set_rules('user_id', 'User ID', 'trim|required');
				if ($this->ci->form_validation->run($this->ci)){
					$password = $this->ci->input->post('password');
					$confirm_password = $this->ci->input->post('confirm_password');
					$user_id = $this->ci->input->post('user_id');
					if ($password == $confirm_password) {
						$data = array(
							'password' => md5($password)
						);
						$this->ci->common_model->updateDataFromTabel('user', $data, 'id',$user_id);
						$this->ci->session->set_userdata('registration_step', 3);
						$returnArray = array('msg' => 'success', 'is_success' => 'true','user_id' => $user_id,'url'=>'home/freeTerms');
						echo json_encode($returnArray);
					} else {
						echo json_encode(array('msg' => lang('record_insert_error'), 'is_success' => 'false'));
					}
				} else {
					echo $this->_errormsg();
				}
			} else {
				$this->ci->home_template->load('home_template', 'packages/user_password');
			}
		} else {
			redirect(base_url());
		}
	}

	public function freeTerms(){
         
        $user_id = $this->ci->session->userdata('user_id');
        $registration_type  = $this->ci->session->userdata('registration_type');
        $registration_step  = $this->ci->session->userdata('registration_step');
        $first_name         = $this->ci->session->userdata('first_name');
        $first_name  = ucfirst($first_name); 
        
        if(!empty($user_id) && !empty($registration_type) && !empty($registration_step)){
            if($registration_step=='2'){
                redirect(base_url().'home/freePassword');
            }
        }
        
        if (!empty($user_id) && !empty($registration_type) && !empty($registration_step) && $registration_step == '3') {
            
            if ($this->ci->input->post('registration_type')) {
                $this->ci->form_validation->set_rules('our_terms', 'Terms & Conditions', 'trim|required');
                if ($this->ci->form_validation->run($this->ci)){
                    $user_id = $this->ci->input->post('terms_user_id');
                    $returnArray = array('msg' => 'success', 'is_success' => 'true','url'=>'home/freeCongratulations');
                    
                    $user_details = $this->ci->common_model->getDataFromTabel('user','email','id',$user_id);
                    // Set email body
                    $random_string = encode($user_id);
                    /*
                    //send email to user via mandrill
                    $this->ci->load->library('mandrill/email_template');
                    $template_name = $this->ci->config->item('registration_template_name');
                    $template_message = array(
                        'to' => array(
                            array(
                                'email' => $user_details[0]->email,
                                'name' => 'Admin',
                                'type' => 'to'
                            )
                        ),
                        'merge' => true,
                        'global_merge_vars' => array(
                            array(
                                'name' => 'activation_link',
                                'content' => base_url() . 'home/verify_email/' . $random_string
                            ),
                            array(
                                'name' => 'contact_link',
                                'content' => base_url() . '?#contact_email'
                            ),
                            array(
                                'name' => 'contact_first_name',
                                'content' => $first_name
                            )
                        )
                    );
                    $this->ci->email_template->send_template_email($template_name, $template_message);
                    */
                      /* Sendgrid */
                    $this->ci->load->helper('sendgrid');
                    
                    $facebook_url    = $this->ci->config->item('facebook_url'); #$config['facebook_url']
                    $twitter_url     = $this->ci->config->item('twitter_url'); #$config['facebook_url']
                    $g_plus_url      = $this->ci->config->item('g_plus_url'); #$config['facebook_url']
                    $newmanevent_url = $this->ci->config->item('newmanevent_url'); #$config['facebook_url']
                    
                    
                    $addTeam_templateId = ADDTeam_templateId; //
                    
                    $apiKey = ADMINNEWMAN_APIKEY;
                    $paramArray = array(
                    'sub' => array(':firstname' => array($first_name),':activation_link'=>array(base_url().'home/verify_email/'.$random_string),':contactLink'=>array(base_url().'?#contact_email'),':newmanvenue_url'=>array($newmanevent_url),':twitter_url'=>array($twitter_url),':g_plus_url'=>array($g_plus_url),':facebook_url'=>array($facebook_url)),
                    'filters' => array('templates' => array('settings' => array('enable' => 1, 'template_id' => $addTeam_templateId)))
                    );
                    
                    $tempSubject = '.';
                    $templateBody = '.';
                    $toId =$user_details[0]->email;//$admin_email;
                    sendgrid_mail_template($addTeam_templateId,$tempSubject,$templateBody,$toId,$paramArray,$apiKey,adminFromEmail);
                    //send email to user via mandrill
                    $this->ci->session->unset_userdata('registration_step');
                    echo json_encode($returnArray);
                } else {
                    echo json_encode(array('msg' => lang('record_insert_error'), 'is_success' => 'false'));
                }
            } else {
                $this->ci->home_template->load('home_template', 'packages/our_terms');
            } 
        } else {
            redirect(base_url());
        }
	}
	
	public function freeCongratulations(){
		$user_id = $this->ci->session->userdata('user_id');
		$registration_type = $this->ci->session->userdata('registration_type');
		$registration_step = $this->ci->session->userdata('registration_step');
		if(!empty($user_id) && !empty($registration_type) && !empty($registration_step)){
			if ($registration_step=='2'){
				redirect(base_url().'home/freePassword');
			} else if($registration_step=='3'){
				redirect(base_url().'home/freeTerms');
			}
		}
		if (!empty($user_id) && !empty($registration_type)) {
			$this->ci->session->unset_userdata('user_id');
			$this->ci->session->unset_userdata('registration_type');
			$this->ci->home_template->load('home_template', 'packages/congratulations_page');
		} else {
			redirect(base_url());
		}
	}
	
	public function checkEmailExist(){
		$email = $this->ci->input->post('email');
		if (!empty($email)) {
			$user_details = $this->ci->common_model->getDataFromTabel('user','id',array('email'=>$email, 'user_type_id'=>1, 'registration_type'=>1));
			if(!empty($user_details[0]->id)){
				echo json_encode(array('msg' => lang('is_unique_email_custom_message'), 'is_success' => 'false'));
			} else {
				echo json_encode(array('is_success' => 'true'));
			}
		}
	}
	
	
    function verify_email() {
        $user_id = decode($this->ci->uri->segment(3));
        $where = array('id' => $user_id);
        $userdata = $this->ci->common_model->getDataFromTabel('user', 'email_verify', $where);
        $data = array();

        if (!empty($userdata)) {
            if ($userdata[0]->email_verify == 0) {
                $updateWhere = array('id' => $user_id);
                $updateData = array('email_verify' => '1');
                $this->ci->common_model->updateDataFromTabel('user', $updateData, $updateWhere);
                $data['msg'] = "Your email has been verified successfully.";
                redirect('home/success_veryfication/1');
            } else {
                redirect('home/success_veryfication/2');
                $data['msg'] = "Your email is already verified.";
            }
        } else {
            $data['msg'] = "!Oops, Something went wrong. Please check the URL.";
        }

        $this->ci->home_template->load('home_template', 'error_reset_paasword', $data);
    }
    
    
    
    public function uregister()
    {
             
        $postdata = $this->ci->input->get();
        /*
         * check email is exist
         * @param email
         */ 
        $emailCount = $this->ci->home_model->checkuseremailexit($postdata['email']);
        if( count($emailCount) > 0 ) {
            echo 'Already registered!';
        }else {
            $postdata['username'] = $postdata['email'];
            $postdata['password'] = md5($postdata['password']);
            $postdata['registration_type'] = '1';
            $postdata['user_type_id'] = '1'; // this is type of register define in "nm_user_type_master" table
            $postdata['email_verify'] = '1'; // this is by default user will be enable
            $postdata['status'] = '1'; // this is by default user will be enable
            $postdata['operand_number'] = getMasterNumber('operand'); // get operand number 
            $postdata['master_event_id'] = 'AA01';
            
            /* Insert new user */
            $this->ci->home_model->registerinsert($postdata);
            echo 'Successfully register';
        }
        
    }
    
     /*
     * Free account Step-1
     */ 
    public function freeAccount() {		
		$this->ci->home_template->load('home_template', 'packages/apply_free_account');
    }
    
     /*
     * Free account Step-1
     */ 
    public function applyUser() {		
		$this->ci->home_template->load('home_template', 'packages/apply_user');
    }
    
    public function isLoginOrRegisterCheck()
    {
        $data=array();
        $this->ci->home_template->load('home_template','packages/TermsConditionsCheck',$data,TRUE);
    }
    
     public function setLoginOrRegisterCheck(){
            $user_id = $this->ci->input->post('user_id');
            $saveData['user_id'] = $user_id;
            $response = curlCall($this->ci->config->item('newmanUrl').'api/updateUserbyParam',$saveData);
            //print_r($response);die;
	}

}

// end class


/* End of file auth.php */
/* Location: ./system/application/modules/home/libraries/auth.php */
