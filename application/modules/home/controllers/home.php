<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * @description: This controller is used to manage login, registration
 * @create date: 21-Nov-2013
 * @auther: Lokendra Meena
 * @email: lokendrameena@cdnsol.com
 * 
 */ 

class Home extends MX_Controller{

	function __construct(){
		parent::__construct();
		$this->load->library('auth',$this);
		$this->load->library(array('home_template','head'));
		$this->form_validation->set_error_delimiters('<div>', '</div>');
		$this->load->model('home_model');
		$this->load->helper('cookie');
		$this->load->language(array('home'));
	}

	//----------------------------------------------------------------------------
	
    /*
     * @access: public
     * @default load 
     * @description: This function is used to user login eheck user exist
     * @load: login view
     * @return: void
     *  
     */
    
    public function index(){
		$this->auth->index();
	} 
    
    //------------------------------------------------------------------------
     
    /*
     * @access: public
     * @description: This function is used to open login view and user id & password check
     * @load: login view
     * @return: void
     *  
     */
     
	public function login(){
		//call login method
		$this->auth->userlogin();
	}
	
	//----------------------------------------------------------------------------
	
	/*
	 * @access: public
     * @description: This function is used to manage register 
     * @load: register view
     * @return: void
     *  
     */
	
	public function register(){
		//call register method
		$this->auth->userregister();
	}
	
	//----------------------------------------------------------------------------
	
	/*
	 * @access: public
     * @description: This function is used to activation user account
     * @return: void
     *  
     */
	
	function accountactivation($encryptCode='')
	{
		//call for account activation method
		$this->auth->accountactivation($encryptCode);
	}
	
	//----------------------------------------------------------------------------
	
	/*
     * @description: This function is used to activation user account
     * @return: void
     *  
     */
	
	function forgotpassword()
	{
		//call for forgot password method
		$this->auth->forgotpassword();
	}
  
  function reset_password()
	{
    $this->auth->reset_password();
	}
	//----------------------------------------------------------------------------
	
	/*
	 * @access: public
     * @description: This function is used to send trouble logging information to admin
     * admin id define in config
     * @return: void
     *  
     */
	
	function troublelogging()
	{
		$this->auth->troublelogging();
	}
	
	//----------------------------------------------------------------------------
	
	/*
	 * @access: public
     * @description: This function is used to send contact us request to admin
     * @return: void
     *  
     */
	
	function contactus()
	{
        $this->auth->contactus();
	}
	
	//------------------------------------------------------------------------------------------
	
	/*
	 * @access: public
     * @description: This function is used to show abouts
     * @load: aboutus view
     * @return: void
     *  
     */
	
	function aboutus()
	{
		$this->auth->aboutus();
	}
	
	//------------------------------------------------------------------------------------------
	
	/*
	 * @access: public
     * @description: This function is used to show features
     * @load: aboutus view
     * @return: void
     *  
     */
	
	function features()
	{
		$this->auth->features();
	}
    
	/*
	 * @access: public
     * @description: This function is used to show feature delegates
     * @load: delegates view
     * @return: void
     *  
     */
	function delegates()
	{
		$this->auth->delegates();
	}
    
	/*
	 * @access: public
     * @description: This function is used to show feature suppliers
     * @load: suppliers view
     * @return: void
     *  
     */
	function suppliers()
	{
		$this->auth->suppliers();
	}
    
	/*
	 * @access: public
     * @description: This function is used to show feature clients
     * @load: clients view
     * @return: void
     *  
     */
	function clients()
	{
		$this->auth->clients();
	}
    
	/*
	 * @access: public
     * @description: This function is used to show feature resources
     * @load: resources view
     * @return: void
     *  
     */
	function resources()
	{
		$this->auth->resources();
	}
    
	/*
	 * @access: public
     * @description: This function is used to show feature managers
     * @load: managers view
     * @return: void
     *  
     */	
	function managers()
	{
		$this->auth->managers();
	}
  
  	/*
	 * @access: public
     * @description: This function is used to show packages
     * @load: packages view
     * @return: void
     *  
     */
  
  function packages()
	{
		$this->auth->packages();
	}
  
  
  /*
  * @access: public
  * @description: This function is used for all type (Casual, solo , free etc.) of registration
  * @load: 
  * @return: true (if Data is successfully inserted in DB ) / False ( Otherwise) 
  */
  function registration()
  {
    $this->auth->registration();
  }
  
  /*
  * @access: public
  * @description: This function is used for free type of registration
  * @load: 
  * @return: true (if Data is successfully inserted in DB ) / False ( Otherwise) 
  */
  function freeRegistration()
  {
    $this->auth->freeRegistration();
  }
  
  
  
  /*
  * @access: public
  * @description: This function is used for free type of Password saved
  * @load: 
  * @return: true (if Data is successfully inserted in DB ) / False ( Otherwise) 
  */
  function freePassword()
  {
    $this->auth->freePassword();
  }
  
  /*
  * @access: public
  * @description: This function is used for free type of term saved
  * @load: 
  * @return: true (if Data is successfully inserted in DB ) / False ( Otherwise) 
  */
  function freeTerms()
  {
    $this->auth->freeTerms();
  }
   /*
  * @access: public
  * @description: This function is used for free type Congratulations
  * @load: 
  * @return: true  
  */
  function freeCongratulations()
  {
    $this->auth->freeCongratulations();
  }
  
   /*
  * @access: public
  * @description: This function is used for check Email Exist
  * @load: 
  * @return: true  
  */
  function checkEmailExist()
  {
    $this->auth->checkEmailExist();
  }
  
  
  /*
  * @access: public
  * @description: This function is used for save password
  * @load: 
  * @return: true (if Data is successfully inserted in DB ) / False ( Otherwise) 
  */
  function save_password()
  {
    $this->auth->save_password();
  }
  
  /*
  * @access: public
  * @description: This function is used for check our terms
  * @load: 
  * @return: true  / False ( Otherwise) 
  */
  function our_terms()
  {
    $this->auth->our_terms();
  }
  
  /*
  * @access: public
  * @description: This function is used to verify email after registration
  *  
  */
  function verify_email()
  {
    $this->auth->verify_email();
  }
	
	//----------------------------------------------------------------------------
	 
	/*
	 * @access: public
	 * @description: This function is used to check email availability
	 * @retrun void
	 * 
	 */ 
	
	function checkuseremail(){
		if($this->auth->checkUserEmail())
			echo json_encode(array('msg'=>'This email already exists.','is_success' => 'false'));
		else
			echo json_encode(array('msg'=>'This email available.','is_success' => 'true'));
	}
	
	
	//----------------------------------------------------------------------------
	 
	/*
	 * @access: public
     * @description: This function is used to logout user form website 
     * @return: void
     *  
     */
     
    public function logout()
    {
		//call method for logout	
		$this->auth->logout();
		redirect(base_url());
	 }
     
     
  /*
  * @access: public
  * @description: This function is used for free type of registration
  * @load: 
  * @return: true (if Data is successfully inserted in DB ) / False ( Otherwise) 
  */
  
    public function uregister()
    {
    $this->auth->uregister();
    }
  
    
    
    function freeAccount()
    {
        $this->auth->freeAccount();
    }
    
    function applyUser()
    {
        $this->auth->applyUser();
    }
    
    public function fbconfig()
    {
        $this->load->view('fb/fbconfig');
    }
   
   
	 /*
     * Gplus & Fb terms condition check login
     * Load gplus library
     */  
    public function isLoginOrRegisterSession(){
        $status = $this->input->post('status');
        if($status) {
            $this->session->set_userdata('isCheckedRegister','1');
        }else {
            $this->session->unset_userdata('isCheckedRegister');
        }
	}
    
    public function isLoginOrRegisterCheck(){
		$this->auth->isLoginOrRegisterCheck();
	}
    
     public function setLoginOrRegisterCheck(){
        //$this->session_check->CheckUserLoginSession();
        $this->auth->setLoginOrRegisterCheck();
	}
    
    public function success_veryfication()
    {
        $this->home_template->load('home_template', 'verify_page');
    }
    
     public function testEmail(){
        /* Load Email Library */
        $this->load->library('email'); 
        $this->email->from('theteam@newmanevents.com', 'Newman'); 
        
        /* send email notification to admin */
        $this->email->to('shelendra.tiwari@gmail.com');
        $this->email->subject('This is a email');
        
        $this->email->message('This is a sample email send by shailendra');  
        $this->email->send();
        echo $this->email->print_debugger();
    }
    
    public function termsnconditions(){
        $this->home_template->load('home_template', 'termsnconditions');
    }
  
}

/* End of file home.php */
/* Location: ./system/application/modules/home/controllers/home.php */
