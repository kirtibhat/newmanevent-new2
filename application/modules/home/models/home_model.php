<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * @description: This model is used manage home controller data
 * @create date: 21-Nov-2013
 * @auther: Lokendra Meena
 * @email: lokendrameena@cdnsol.com
 * 
 */ 

class Home_model extends CI_model{


	private $tablenmuser= 'nmv_venue_user';
    
	function __construct(){
		parent::__construct();	
			
	}
	
	/*
	 * @description: This function is used to register user data
	 * @param: $insertData
	 * @type: array
	 * @return: $insertId
	 */ 
	
	
	public function registerinsert($insertData){	
		$this->db->insert($this->tablenmuser,$insertData); 
		$insertId = $this->db->insert_id();
		return $insertId; 
	}
	
	/*
	 * @description: This function is used to user id and password exist then login
	 * @param: $pastdata
	 * @return: boolean (true/false)
	 */ 
	
	public function checkuserexit($username,$password)
    { 
		$this->db->select('*');
		//$this->db->where('username',$username);
		$this->db->where('email',$username);
		$this->db->where('password',$password);
		$this->db->where('user_type_id','1');
		$query = $this->db->get($this->tablenmuser);
		if($query->row())
			return $query->row();
		else
			return false;	
	}
    
    //check user email already exist in table
    public function checkuseremailexit($email)
    { 
		$this->db->select('*');
		$this->db->where('email',$email);
		$query = $this->db->get($this->tablenmuser);
		if($query->row())
			return $query->row();
		else
			return false;	
	}
    
    /*
     * get FB user details
     * and check from DB
     */ 
    public function isFbUser($fbid,$fbfullname,$femail)
    {
        $returnArry = array();
        $this->db->select('*');
		$this->db->where('email',$femail);
		$query = $this->db->get($this->tablenmuser);
        $resRow = $query->num_rows();
        if(!$resRow) {
            $insData['email'] = $femail;
            $insData['username'] = $femail;
            $insData['fbid'] = $fbid;
            $insData['firstname'] = $fbfullname;
            $this->db->insert($this->tablenmuser,$insData);
            $insertId = $this->db->insert_id();
            $returnArry['status'] = '0';
            $returnArry['email'] = $femail;
            $returnArry['insertId'] = $insertId;
            $returnArry['fbid'] = $fbid;
            return $returnArry;
        }else {
            $returnArry['status'] = '1';
            $returnArry['email'] = $femail;
            return $returnArry;
        }

    }
    
}
?>
