<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//...............................This form load all frotned side form.............

	$user_id=frntUserLogin('sideevent'); 
	//to check login user type is valid
	 
	$eventId = (isset($event_id) && !empty($event_id))?$event_id:'';
	$formRegistratType = array(
		'name'	 => 'registrantTypeForm',
		'id'	 => 'registrantTypeForm',
		'method' => 'post',
		'class'  => 'form-vertical',
	); 
	
 ?>

<div class="container mt40lesseight container_main">
	
	<!-- to load header-----------  -->
    <?php $this->load->view('common/header'); ?>
    
   <div class="row-fluid mt10 ">
	<div class="span7 signupresponsive " id="frontendleft">
	  <div class="container-fluid signupbg">

		<div class="usercontainer">
			
			<div class="commonS_circle ml0 mr15 mt6" id="usershow">
				 <div class="smalluser"></div>
			</div>
			
			<div class="usertooltio">
				
			     <div class="tooltiparrow"></div>
			     <?php echo lang('registerForThisEventMsg'); ?>
			     <div class="clearfix"></div>
			     <a href="<?php echo base_url('frontend/index/'.encode($eventId)); ?>"><button type="button" class="submitbtn_cus mt10 mr0 bg_741372" >CANCEL</button></a>
				
			</div> 
		</div>
		   
		     <!--to load term condtion popup --> 
		  <?php $this->load->view('term_condition_popup'); ?>
		
		  <!--to load popup for registrant password --> 
		  <?php $this->load->view('registrant_password_popup'); ?>
		  
		<!-- start login add_login_form_field_popup  -->
		
		   <?php $this->load->view('login_popup'); 
				if($user_id > 0){ ?>
				<a  class="submitbtn_cus mt10 mr0 bg_741372 logout" ><?php echo lang('frntLogout'); ?></a>
			<?php }else{ ?>
	            <a class="submitbtn_cus mt10 mr0 bg_741372 add_login_form_field" data-toggle="modal" data-target="#myModal" ><?php echo lang('frntLogin'); ?></a>
			<?php } ?>
		<!--  end add_login_form_field_popup  -->
    
		<div class="clearfix"></div>
		<div class="accorianbg">
		  <div class="accordion" id="monogram-acc">
			  
			   <?php 
				// load Contact person form view
				$this->load->view('contact_person');
			
				 // open the form
				// echo form_open($this->uri->uri_string(),$formRegistratType);
				echo form_open(base_url('frontend/registrantPayments'),$formRegistratType); 
				
				 // load side_events view
				  $this->load->view('side_events');
				  
				 // load personal details form view
				 $this->load->view('personal_details');
				 
				 //load registration_type view
				  $this->load->view('registration_type');
			
				
				 // load side_events view
				//  $this->load->view('breakouts');
		
				 // load side_events view
				 $this->load->view('payment');
			  
				
				 echo form_close();
				 // close of the form
	            ?>
		    </div>	
		   </div> <!-- close accorianbg div --> 
		
   	 </div>	
   	</div>    	
   	 	
		 <div class="span5 addbgcontainer pull-right spnncerresponsive" id="frontendright">
		    <div class="addbg"> <div class="addcontent"><?php echo lang('advertisementMsg'); ?></div></div>
		    <div class="addbg"> <div class="addcontent"><?php echo lang('advertisementMsg'); ?></div></div>
	     </div>
	      <div class="clearfix"></div>
	     <!-- /sponcerbg -->
         
   </div> <!-- close row-fluid mt10 div --> 
     <div class="spacer40"></div>          
</div> <!-- /container -->


<script type="text/javascript">
	
 /** to hide loder **/
 $('.loginvalidate').hide();
 $('.loadershow').hide();
 
 /** code for check logout user **/
 
    var checkSession='<?php echo  $user_id; ?>';
    if(checkSession>0)
    {
		$('.add_login_form_field').hide(); 
	}
	//load country code and flag code
	$(".codeFlag").intlTelInput({defaultCountry:'au'});
</script>


	

