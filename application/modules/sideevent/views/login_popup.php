<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
//--------This form used for user login---------//	

$eventId = (isset($event_id) && !empty($event_id))?$event_id:'';
?>


<div class="modal fade frontendpopup dn" id="add_login_form_field_popup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close closebtnfront" data-dismiss="modal" aria-hidden="true">&times;</button>
        <div class="commonS_circle ml0">
                	<div class="lockicon_frontend"></div>
                </div>
        <h4 class="modal-title"><?php echo lang('frntLogin'); ?></h4>
        <div class="clearfix"></div>
      </div>
       <div class="modal-body">
             <?php echo lang('PleaseEnterEmailPassMsg'); ?>
       <div class="modelinner pl0 pr0">
       		
				<form name="loginsideform" id="loginsideform" method="post" action="" class="form-horizontal mt10"> 
                    <label><?php echo lang('frntEmail'); ?></label>
                        <div class="position_R">
                         <input type="text" name="login_email" id="login_email" value="" class="loginrequired" placeholder="<?php echo lang('frntEmail'); ?>*" >
				     
				        <label class="efformessage loginvalidate frontend_popup_error"><span class="errorarrow"></span><?php echo lang('frntRequiredMsg'); ?></label>
                      </div>
                    
                    <div class="spacer15"></div>
                    
                     <label><?php echo lang('frntPassword'); ?></label>
                           <div class="position_R">
                    	   <input type="password" name="login_password" id="login_password" value="" class="loginrequired" placeholder="<?php echo lang('frntPassword'); ?>*" >
						
						  <label class="efformessage loginvalidate frontend_popup_error"><span class="errorarrow"></span><?php echo lang('frntRequiredMsg');?></label>
                         </div>
						  
                    <div class="spacer15"></div>
                     <input type="hidden" name="event_id" id="event_id" value="<?php echo $eventId; ?>">
                     <button id="" class="submitbtn_cus mt10 mr0 bg_741372" type="submit"><?php echo lang('frntLogin'); ?></button>
                        
           </form>
        </div>
       </div>
      
    </div>
  </div>
</div>
<script>

	   
	$("#loginsideform").submit(function(event) {
		
	//to check validation for fields
	var validate=checkValidateFormField();
    if(validate==true)
    {  
		var fromData=$("#loginsideform").serialize();
		var url = baseUrl+'sideevent/userLogin';
		
		$.post(url,fromData, function(data) {
		  if(data){
			   var status = parseInt(data.status);
			   hide_popup("add_login_form_field_popup");
				  
				   if(status==1){
						  
						 $('.logout').show();
						 $('.add_login_form_field').hide(); 
						 refreshPge();
						
					}else{
						   
						   custom_popup(data.msg,false);	
					}
			}
		},"json");
	}
	return false;	
	
  });
   


</script>

<!-- close login popup code -->

 
