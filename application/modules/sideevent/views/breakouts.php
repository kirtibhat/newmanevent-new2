<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
//--------To show breakout content---------//	
$registeredId= (isset($registered_id) && !empty($registered_id))?$registered_id:''; 
$selectRegiId=''; 

?>
  <div class="accordion-group step2">
		<div class="accordion-heading">
			<div class="accordion-toggle breakoutform" data-toggle="collapse" data-parent="#monogram-acc" href="#Breakouts">
			   <?php echo lang('step5BreakoutsHeading'); ?>
			</div>
		</div>
		<div class="accordion-body collapse" id="Breakouts">
		 <div class="innerbody">
			   <?php echo lang('PleaseselectbreakoutMsg'); ?>
		
				<?php
				     
				    if(isset($breakoutDetails) && !empty($breakoutDetails))
				    {
						foreach($breakoutDetails as $breakout)
						{
							$date=date_create($breakout->date);
							//to get breakout stream on the basis of breakout Id
							$streams=$this->common_model->getDataFromTabel('breakout_stream','*',array('breakout_id'=>$breakout->id));
							
						
						?>
						<div class="mt10">
						<div id="breakoutContent">	
						<div class="registration_table">
						   <div class="tableheading"><?php echo ucfirst($breakout->breakout_name).' - '.date_format($date, 'jS F Y'); ?> </div>
                         
                        </div> 
                        <div class="frontendtable pb0">
						<div class="panel-body">
							<div id="flip-scroll">
								<table class=" table table-bordered table-striped table-condensed cf">
								<tbody>
								 <thead> 
									<tr class="strimbgtable">
									   <?php					
										foreach($streams  as $stream)
										{
										 ?>
											 <td><?php echo $stream->stream_name; ?> <br> <font size="-1"> <?php echo $stream->stream_venue; ?></font></td>
										 <?php					
										}
										
										?>					
									 </tr>
								</thead>
								<tr>
							 <?php
							     
								if(isset($streams) && !empty($streams))
								{
		
									foreach($streams  as $stream)
									{
										 $sessionDetails=getSessionDetails($stream->id);
										// $sessions=getDataFromTabel('breakout_session','*',array('stream_id'=>$stream->id));

									    ?>						
									    <td>
									    <?php	
											
											
										//to get breakout session on the basis of breakout Id
										//$sessions=getDataFromTabel('breakout_session','*',array('stream_id'=>$stream->id));
										if(!empty($sessionDetails))
										{
											foreach($sessionDetails as $session)
											{
											
												?>
											
												<?php
												 /* code to check select stream session */
												$checked='';
												if($selectRegiId!=0)
												{
													if(isset($frntBreakoutDetails) && !empty($frntBreakoutDetails))
													{
														foreach($frntBreakoutDetails as $frntbreakout)
														{
															if($frntbreakout->stream_session_id==$session->id)
															{

																$checked='checked';
															}
														}
													}
												}   
												 $readonly='';
												  
												// $streamLimit=checkbreakoutstreamlimit($session->breakout_session_id,$session->breakout_stream_id);
												   
												
												$breakoutSessionTime=date_format($date, 'jS F Y').', '.date('H:i', strtotime($session->start_time)).' to '.date('H:i', strtotime($session->end_time)); 

												 ?>
												
													<?php
													
													//if(!$streamLimit)
													//{
														$readonly='';  //'readonly';
													 ?>
														<!--  <input type="radio" name="breakout"  class="pull-left breakoutradio breakout_readonly bgzero" value=""  > -->
													  <?php
													// }else{
													 ?>
														<input type="radio" name="breakout<?php echo $session->id; ?>" id="breakout<?php echo $session->id; ?>" class="pull-left breakoutradio breakout<?php echo $session->id; ?>" value="<?php echo $session->id;?>" <?php echo $checked; ?>>
													 <?php 
													//}
													 ?>
													
													<input type="hidden" name="breakout_data<?php echo $session->id; ?>" id="breakout_data<?php echo $session->id; ?>" value="<?php echo $breakoutSessionTime;?>" >
													<div class="tableradio_cont">
														<span class="clr_lightblue"><?php echo $session->about_session; ?></span>
														<br>
														<div class="clearfix"></div>
														<span><?php echo $session->speakers; ?></span>
														
														<span><?php echo $session->organisation; ?></span>
														<div><?php echo $breakoutSessionTime; ?></div>
														
														
													</div>
												
												<?php 
													
												
											}
										} 
										 ?>
										
										 </td>
										<?php
										 
									}
										
											 
										
								}
										?>
									
								</tr>
										
								</tbody>
							   </table>
							 
							   </div>
							 </div>
							</div>
							</div>
						   </div>
							<?php
			
							}//end of outer foreach loop


	                }
	
                    ?> 
				  
				 <!-- <div id="breakoutContent"></div>  -->
		           <input type="hidden" name="stream_session_id" id="stream_session_id" value="">
		
			 
      </div>
     </div>
 </div>  			 

<script type="text/javascript">

var registered_id='<?php echo $registeredId; ?>';
/*?
 $(document).ready(function() {	
 
	  //get value of breakout form 
	  $.ajax({
		  type: "POST",
		  url: baseUrl+'frontend/createbreakoutform',
		  data: 'registered_id='+registered_id,
			success: function(data) {

			// $('#breakoutContent').html(data);
			 getSelectedBreakout();
			$('.breakoutradio').screwDefaultButtons({
				image: 'url("'+IMAGE+'foontendradio.png")',
				width: 17,
				height: 17,
			});
			 
			}
		 });
     
	});    
	*/
</script>
