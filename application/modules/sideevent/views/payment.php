<?php

    $registeredId= (isset($registered_id) && !empty($registered_id))?$registered_id:'';  
    $isEventExpired=(isset($is_event_expired) && !empty($is_event_expired))?$is_event_expired:''; 
    $docPath=(isset($termConditionDetails) && !empty($termConditionDetails))?$termConditionDetails[0]->document_path:'';
    $docFile=(isset($termConditionDetails) && !empty($termConditionDetails))?$termConditionDetails[0]->document_file:'';
  
     
     
    //it display expiry year more than 19 year from current year.
    $yearDiff=date('Y')+18;
 
?>
	  <div class="accordion-group step2">
			<div class="accordion-heading">
				<div class="accordion-toggle" data-toggle="collapse" data-parent="#monogram-acc" href="#Payment">
				  Step 4 - Payment
				</div>
			</div>
		<div class="accordion-body collapse paymentCollapse" id="Payment">
		<div class="innerbody">
			 <div class="tableheading position_R min-height44">
			 <div class="printpara"> <?php  echo lang('pleaseconfirmMsg');?></div>
			
			 <div class="printicon pull-right"></div>
			
			</div>
			 <div class="pymtregdetail">
			 
				<table>
			      
				<tr class="headingrow">
					<th><?php echo lang('registrationDetailsMsg'); ?></th>
					<th><?php echo lang('frntTotalMsg'); ?></th>
					<th></th>
				</tr>
				 <tr id="registered_user">	 
				   <td></td>
				   <td></td>
				   <td></td>
				  </tr>
				   
				<tr id="paymentRegis" >
					<td colspan="3"></td>
			    </tr>
			    
			    
			    
			    
			    <tr id="sideeventPayment" >
					<td colspan="3"></td>
			    </tr>
			    
			    
			    <tr id="breakoutrow" >
					<td colspan="3"></td>
			    </tr>
			    <tr id="subtotalrow" >
					<td colspan="3"></td>
			    </tr>
			    
			    <?php
			     if(isset($user_regis_amt) && !empty($user_regis_amt))
			     {
					 
			     ?>  
					<tr class="headingrow bg_lightblue">
					<th class="text-right"><?php echo lang('invoiceBalanceMsg'); ?></th>
					<th>$<span class="pay_amt"><?php echo $user_regis_amt['total_amt']; ?></span></th>
					<th></th>
					</tr>
					
					<tr class="headingrow bg_lightblue">
					<th class="text-right"><?php echo lang('gstIncludedMsg');  ?></th>
					<th>$<span class=""><?php echo $user_regis_amt['total_gst']; ?></span></th>
					<th></th>
					</tr>
					
					<tr class="headingrow bg_lightblue">
					<th class="text-right"><?php echo lang('discountForGroupBookingMsg'); ?></th>
					<th>$0.00</th>
					<th></th>
					</tr>
					
					<tr class="headingrow">
					<th class="text-right"><?php echo lang('balanceOwingMsg'); ?></th>
					<th>$<span class="pay_amt"><?php echo $user_regis_amt['total_amt']; ?></span></th>
					<th></th>
					</tr>
				
				 <?php
			    }
				
				?>
			 </table>
			 
			 </div><!------ /payment registration detail ------>
			 
			 <div class="pymtregdetail mt20">
				<div class="tableheading_2 pt2pb2">
						<input type="checkbox" name="term_condition" id="term_condition" class="term_condition">
						<label class="paymentcond"><?php echo lang('term&ConditionMsg'); ?> <br> 
						 <?php if(strlen($docFile)>'0'){ ?>
						  <a href="<?php echo base_url().$docPath.$docFile; ?>" target="_blank">
						      <?php  echo lang('clickHereToReadMsg'); ?>
						   </a>
						<?php } else{ ?>  
						     <a class='noTermCondition'>
						         <?php  echo lang('clickHereToReadMsg'); ?>
						      </a>
						<?php } ?>
					</label>    
				 </div>
				 
				 <div class="tableheading_2 pt2pb2 mt6">
							<input type="checkbox" name="ex1" id="">
							<label class="paymentcond"><?php echo lang('invoiceConfirmationMsg'); ?></label>    
				 </div> 
			 </div>
			 
			 <div class="finapayment sideSgestcont pl0">
				
				
				<div class="heading"><?php echo lang('paymentMsg'); ?></div>
			      
			      
					<div id="first_div">
					 
					  <!-- cash  payemnt -->
					<div class="tableheading_2">
						<input type="radio" class="select_payment" value="cash" name="select_payment" id="cash_payment" checked>
						<label><?php echo lang('cashPaymentMsg'); ?></label>
					</div>
					
					  <!-- creadit card payemnt -->
					 <div class="spacer15"></div>
					<div class="tableheading_2">
							<input type="radio" class="select_payment"  value="credit_card"  name="select_payment" id="credit_card" >
							<label><?php echo lang('creditCardMsg');?></label>
					</div>
						
					  <div class="formoentform_container pl25 credit_card_field">
					
							<div class="position_R">
									<input type="text" name="item_name" value="Ravindra" placeholder="Credit Card Name">
							 </div>
							<div class="position_R mb10">	
							
								<div class="pull-left frontend_select">
								<select class="selectpicker bla bla bli creditCardField" name="card_type" id="card_type">
									<option  class="selectitem" value="" ><?php echo lang('pleaseSelectCardTypeMsg'); ?>*</option>
									<option  class="selectitem" value="MasterCard" ><?php echo lang('masterCardMsg');?></option>
									<option class="selectitem" value="Visa" selected ><?php echo lang('visaMsg'); ?></option>
									
								</select>	
								</div>
							  
							   </div>   
							<div class="position_R">
								<input type="text" name="card_number" value="4840225861310547" id="card_number" placeholder="Credit Card Number*" class="creditCardField numValue">
							 
							 </div>
							 
							  <div class="position_R">
								 <input type="text" name="ccv" id="ccv" placeholder="CCV" value="157" class="">
							 </div>

						     <div class="position_R">
								<div class="pull-left maxW70 expireSpinner">
								      <input id="expire_month" name="expire_month" max="12" min="1" value="<?php echo date('m'); ?>" class="width100per guestspinner creditCardField" disabled>
								 </div>
								 
								 <div class="pull-left maxwidth100 ml25 expireSpinner">
									 <input id="expire_year" name="expire_year"  max="<?php echo $yearDiff; ?>" min="<?php echo date('Y'); ?>" value="<?php echo date('Y'); ?>" class="width100per guestspinner creditCardField" disabled >
									 <input type="hidden" name="expire_month_year" id="expire_month_year" value=""> 
								 </div>
								 
								<div class="expiredate"><?php echo lang('expiryDateMsg'); ?>*</div>
							
								 <div class="clearfix"></div> 
							   </div>

							 
						  </div>
				 
				 </div>	 
			     
					<!-- end of first_div -->
					
					<div class="spacer15"></div>
					 <!-- cheque  payemnt -->
					<div class="tableheading_2">
						<input type="radio" class="select_payment" value="cheque" name="select_payment" id="cheque_payment">
						<label><?php echo lang('chequePaymentMsg'); ?></label>
					</div>
					 <!--field for cheque payments -->
					 
					   <div id="cheque_fields">
						   
							<div class="position_R">    
								<input type="text" name="cheque_number" id="cheque_number" class="cheque_field numValue" placeholder="Cheque Number*">
							 </div>
							
							 <div class="position_R">
								<input type="text" name="bank_name" id="bank_name" class="cheque_field" placeholder="Bank Name*">
							 </div>
							 
						  </div>
					 <!--end field for cheque payments -->
					
					<div class="spacer15"></div>
					
						
					
                     <input type="hidden" value="" name="payment_by" id="payment_by">

					<div class="spacer15"></div>
				
				</div> 
				  
				  	
				<div class="mb20 ">
			
			   <?php 
				//to check registrant amount
				if(isset($user_regis_amt) && !empty($user_regis_amt))
				{   
					 $regiAmt= 100;
					 if($regiAmt==''){ $regiAmt='0';}
					 $payAmount = array(
				            'type'      =>'hidden',
							'name'      => 'pay_amount',
							'id'        => 'pay_amount',
							'value'     => $regiAmt,
						);	
					 $regisAmount = array(
						'type'      =>'hidden',
						'name'      => 'regis_pay_amount',
						'id'        => 'regis_pay_amount',
						'value'     => $regiAmt,
					);
					 echo form_input($regisAmount);
					 echo form_input($payAmount);	
					 $extraSave	= 'class="frontendsubmitbtn mr10 sideeventRegistrantPayment"';
					 if($isEventExpired)
					 {	
					   $extraSave	= 'class="frontendsubmitbtn mr10  eventDateExpired"';
				     }
					 echo form_button('button','Submit & Pay',$extraSave); 
	            }
	            
					if($registeredId>0)
					{
					   $extraSave	= 'class="frontendsubmitbtn mr10" id="update_btn" onclick="updateRegistrantDetails()"';
					   echo form_button('button','Update Registrant',$extraSave); 
					   
					   $extraSave	= 'class="frontendsubmitbtn mr10 add_btn" onClick="history.go(-1)"';
					   echo form_button('button','Back',$extraSave); 
					   
					}
					else
					{
						$extraSave	= 'class="frontendsubmitbtn mr10 add_btn" id="add_sideevent_regis"';
						if($isEventExpired)
						{	
						   $extraSave	= 'class="frontendsubmitbtn mr10 add_btn eventDateExpired"';
						}
						echo form_button('button','Add Registrant',$extraSave);  
					}
				 ?>
				
				 </div>
				  
			      <input type="hidden" id="payment_status" name="payment_status" value="no">
				  <div class="row-fluid loadershowdiv ">
						<div class="text-center loadershow dn">
							 <?php echo lang('frntLoadingMsg'); ?>
						</div>
						
			         </div>
			 </div>
			 
			</div>
		</div>
        
	</div> 
	  <!-- close accorianbg div --> 
	  
<script type="text/javascript">	  


    var IMAGE='<?php echo IMAGE; ?>';
    $(document).ready(function() {
	   $.ajax({
		  type: "POST",
		  url: baseUrl+'sideevent/createpaymentform',
			success: function(data) {
			
			 $('#registered_user').after(data);
		    
			}
	   });
	});
	
	$('input:radio').screwDefaultButtons({
		image: 'url("'+IMAGE+'foontendradio.png")',
		width: 17,
		height: 17,
	});
	
	$('input:checkbox').screwDefaultButtons({
		image: 'url("'+IMAGE+'frontendcheck.png")',
		width: 17,
		height: 17,
	});	 
	//to hide loader 
	$('.loadershow').hide();
	//to make spinner		
	guestspinner();
	 $('.selectpicker').selectpicker(); 
	 $('#cheque_fields').hide(); 
     $('.credit_card_field').hide(); 
    
</script>	
	 
