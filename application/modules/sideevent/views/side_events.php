<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
  //--------this form show all side even ---------//	
    $isDayWiseSelect='0';
    $eventId= (isset($event_id) && !empty($event_id))?$event_id:''; 
	$registrant_id= (isset($registrantId) && !empty($registrantId))?$registrantId:''; 	
    $registeredId= (isset($registered_id) && !empty($registered_id))?$registered_id:'';    
 
	$user_id=frntUserLogin('sideevent'); 
	$eventDietary='';

	
     
    
    // to get direactory in js for guest form
    if(isset($eventDietarys) && !empty($eventDietarys))
    {
		foreach($eventDietarys as $dietary)
	    {
		    $eventDietary=$eventDietary.'<option class="selectitem" value="'.$dietary->id.'">'.$dietary->dietary_name.'</option>';
	    }
	}   
?>
  <div class="accordion-group step2">
		<div class="accordion-heading">
			<div class="accordion-toggle sideevent" data-toggle="collapse" data-parent="#monogram-acc" href="#SideEvents" >
			Step 2 - Side Events

			</div>
		</div>
		
	<div class="accordion-body collapse" id="SideEvents">
	    <div class="innerbody">
		   
		    <div class="paragraph">	
		       <?php echo lang('PleaseSelectSideeventMsg'); ?>
			</div>
			 
			<div class="registration_table mt10">
				<div class="tableheading"><?php echo lang('sideEventsMsg'); ?></div>
				<div class="tableheading"><?php echo lang('priceIncGSTMsg'); ?></div> 
			</div>
			<?php
			
				if(isset($sideeventContent)){ echo $sideeventContent; } 				 
								 
				if(isset($sideeventDetails) && !empty($sideeventDetails))
				{
					
					foreach($sideeventDetails as $sideevent)
				    {
				      
				     $readonlySideevent='';
				     $date= date_create($sideevent->start_datetime); 
				     $sideEventData= ucfirst($sideevent->side_event_name).' - '.date_format($date, 'jS F Y').' '.date('H:s',strtotime($sideevent->start_datetime)).' to '.date('H:s',strtotime($sideevent->start_datetime)); 

				    ?>
				
					<div class="registration_table">
						<div class="regist_row">
								<div class="regist_mmm">
									<div class="tableheading_2 "><b><?php echo 'Date: '?></b> <?php echo dateFormate($sideevent->start_datetime,'d F Y'); ?> </div>
								</div>

							 <div class="regist_mmm">
								<div class="tableheading_2 pt0">

									<input id="sideeventradio<?php echo $sideevent->id; ?>" name="sideevent_radio" type="radio"  value="<?php echo $sideevent->id; ?>"  class="sideeventRadio sideeventcheck<?php echo $sideevent->id.' '.$readonlySideevent; ?>">
									<input type="hidden" value="<?php echo $sideevent->id; ?>">
									<input type="hidden" name="sideevent_data<?php echo $sideevent->id;?>" id="sideevent_data<?php echo $sideevent->id; ?>" value="<?php echo $sideEventData; ?>">
														  
									<label> <?php echo $sideevent->side_event_name;?></label>

									<div class="regist_smallfont"><?php echo $sideevent->side_event_name;?></div>

								</div>
								 <div class="tableheading_2 pt0">$<span class="sideeventAmt<?php echo $sideevent->id; ?>">0<?php //echo $total_price;?></span></div>
							 </div>
							 
							<?php
							 
								//if($sideevent->additional_guest)
								//{	
								?> 
								    <!-- to get registrant for side event -->
		                               <div class="clearfix"></div>	
						
									<div id="inner_wraaper<?php echo $sideevent->id;?>" class="inner_wraaper" style="display:none;">
                                         <div id="sideevent_registrant<?php echo $sideevent->id;?>" class="sideeventRegis"></div>
                                         
										
									</div> 
								 <?php
								//}
								 ?> 	
						</div>
					</div>
					<div class="clearfix"></div> 
					 
             
                <?php
				    
				    }
                }
                
                ?>
			 <input type="hidden" name="max_comp_guest" id="max_comp_guest<?php ?>" value="">
			 <input type="hidden" name="comp_guest_price" id="comp_guest_price<?php ?>" value="<?php  ?>">
					
			 <input type="hidden" name="event_price" id="event_price<?php ?>" value="<?php  ?>">

	         <div class="clearfix"></div> 
	         
	         <div class="registration_table">
					<div class="tableheading text-right"><?php echo lang('frntTotalMsg'); ?></div>
					<div class="tableheading">$<span id="days_total_price" >0</span> </div> 
			 </div>
             <input type="hidden" name="sideEventAmt" id="sideEventAmt" value='0'>
			 <input type="hidden" name="select_side_event" id="select_side_event" value="">
			 
			 <input type="hidden" name="daywise_id" id="daywise_id" value="<?php  ?>" >
		      <input type="hidden" name="regis_total_amt" id="regis_total_amt" value="" >
		      <input type="hidden" name="registrant_id" id="registrant_id" value="<?php ?>">
		      <input type="hidden" name="singleday_regis_id" id="singleday_regis_id" value="" >
		         <input type="hidden" name="regis_select_id" id="regis_select_id" value="">
		          <input type="hidden" name="select_sideeventRegisId" id="select_sideeventRegisId" value="">

                  <input type="hidden" name="select_daySideeventRegisId" id="select_daySideeventRegisId" value="">
                <input type="hidden" name="registered_id" id="registered_id" value="<?php echo $registeredId; ?>">
          
         </div> <!-- end of the innerbody div -->
       </div>        
	</div>
 
<script type="text/javascript">
      
/**
 * Function to get selected sideevent  details
*/

	 var emptyGuest=false;
	 var selectId='';
	 var eventDietary='<?php echo $eventDietary;?>';

	 $(window).on('load', function () {
		var regiid='<?php echo $registeredId; ?>';
		
		getSelectedSideevent(regiid);
			
	});

    var id='<?php echo $registrant_id; ?>';
    var dayid=0;
	var base_url='<?php echo base_url(); ?>'; 
	var IMAGE='<?php echo IMAGE; ?>'; 
	var prevId=0; 
	var addsideevent=false;
	var guestremove=false; 
	  
	/** function for select registration **/
	SelectRegistrant(id);
	 
   /** function for select registration daywise **/
   
	 dayRegistration(id,dayid);

</script>	

