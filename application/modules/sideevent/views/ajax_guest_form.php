<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
//--------To create guest form  by ajax process---------//	
   
    $guestCount=0;
    $sideeventRegistrantId = (isset($sideevent_registrantid) && !empty($sideevent_registrantid))?$sideevent_registrantid:'';
    
    if(isset($frntGuestDetails) && !empty($frntGuestDetails))
    {
		foreach($frntGuestDetails as $guest)
	    {
			 $guestId=$guest->id;
             ?>		
	
			
			 <div class="regist_mmm">
			   <div class="sideSgestcont guestrow guestrow<?php echo $guestId; ?>">
					<div class="paragraph"><?php  echo lang('guestMsg'); ?> <span id='countGuest'><?php echo $guestCount+1;?></span></div>
					<div class="formoentform_container">
						
						<div class="position_R">
							<input type="text" name="guestfirstname<?php echo $guestId;?>" id="guestfirstname<?php echo $guestId; ?>" value="<?php echo $guest->guest_firstname; ?>" placeholder="First Name*" class="guest" >
								 <span class="removeguest">
								 <a href="javascript:removeGuest('<?php echo $guestId;?>')" title="Remove"><span class="delete_icon"></span></a>
								<input type="hidden" name="guest_sideevent<?php echo $guestId; ?>" id="guest_sideevent<?php echo $guestId;?>" value="<?php echo $sideeventRegistrantId; ?>">
							 </span>
						 </div>
						 
						   <div class="position_R">
								<input type="text" name="guestsurname<?php echo $guestId;?>" id="guestsurname<?php echo $guestId;?>" value="<?php echo $guest->guest_surname; ?>" placeholder="Surname*" class="guest"  >
							</div>
							 
						   <div class="position_R">
								<div class="pull-left frontend_select">
							  <select  class="selectpicker bla bla bli guest"  name="guestdietary<?php echo $guestId; ?>"  id="guestdietary<?php echo $guestId;?>" >
								  <option class="selectitem" value="" ><?php  echo lang('dietaryRequirementsMsg'); ?>*</option>
								    <?php 
								        if($eventDietarys)
										{
											foreach($eventDietarys as $dietary)
											{
												 $selected='';
												 if($dietary->id==$guest->dietary_requirment_id)
												 {
													$selected='selected';
												 }
												 ?>
												 
												 <option class="selectitem" value="<?php echo $dietary->id;?>" <?php echo $selected; ?> > <?php echo $dietary->dietary_name; ?></option>
											     
											     <?php
											}
										}
								    
								    ?>
							   </select>
								</div>
							   <div class="clearfix"></div>
							</div>
							
							<div class="position_R">
								
								  <textarea name="guestdetails<?php echo $guestId; ?>" placeholder="Details" class="details_filed"><?php echo $guest->details; ?></textarea>

							</div>
					 </div>
				</div>
			</div>
			
                <?php
                $guestCount+=1;
	    }
	    ?>
	         <div class="position_R">
				 <input type="hidden" name="total_guest<?php echo $sideeventRegistrantId; ?>" id="total_guest<?php echo $sideeventRegistrantId; ?>" value="<?php echo $guestCount;?>">
			
			</div>
	    
	    <?php

    }
     ?>
