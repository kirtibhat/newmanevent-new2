<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
//--------This form used for registrant password---------//	

?>


<div class="modal fade frontendpopup dn" id="registrant_password_popup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close closebtnfront" data-dismiss="modal" aria-hidden="true">&times;</button>
        <div class="commonS_circle ml0">
                	<div class="lockicon_frontend"></div>
                </div>
        <h4 class="modal-title"><?php echo lang('registrationMsg'); ?></h4>
        <div class="clearfix"></div>
      </div>
       <div class="modal-body">
             <?php echo lang('enterPasswordMsg'); ?>
       <div class="modelinner pl0 pr0">
       		
				<form name="registrant_pass_form" id="registrant_pass_form" method="post" action="" class="form-horizontal mt10"> 
                    
                     <label><?php echo lang('frntPassword'); ?></label>
                           <div class="position_R">
                    	   <input type="password" name="registrant_password" id="registrant_password" value=""  placeholder="<?php echo lang('frntPassword'); ?>*"  >
						
						  <label class="efformessage regisValidate frontend_popup_error"><span class="errorarrow"></span><?php echo lang('frntRequiredMsg');?></label>
                         </div>
						  
                    <div class="spacer15"></div>
                       <input type="hidden" name="pass_registrant" id="pass_registrant" value=""> 
                     <input type="hidden" name="pass_registrant_id" id="pass_registrant_id" value="">
                     <button id="" class="submitbtn_cus mt10 mr0 bg_741372" type="submit"><?php echo lang('frntSubmit'); ?></button>
                        
           </form>
        </div>
       </div>
      
    </div>
  </div>
</div>

<!-- close login popup code -->

 <script type="text/javascript">
	 $('.regisValidate').hide();
	 </script>
