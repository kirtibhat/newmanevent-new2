<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
//--------this form show all registrant ---------//	
   $isDayWiseSelect='0';
   $registeredId= (isset($registered_id) && !empty($registered_id))?$registered_id:''; 
   $registrant_id= (isset($registrantId) && !empty($registrantId))?$registrantId:''; 
   $sideeventPrice= (isset($sideevent_price) && !empty($sideevent_price))?$sideevent_price:'0'; 
   $sideeventId= (isset($sideevent_id) && !empty($sideevent_id))?$sideevent_id:'0'; 
   $sideeventRegisId= (isset($sideevent_regis_id) && !empty($sideevent_regis_id))?$sideevent_regis_id:' '; 
   $maxGuest= (isset($max_guest) && !empty($max_guest))?$max_guest:'0'; 
   $guestPrice = (isset($guest_price) && !empty($guest_price))?$guest_price:'0'; 
 
?>
		 <div class="registration_table">
			 
		    <?php
	            
			 $daywiseid='';
			 $eventId='';
		
			if(isset($registrantDetails) && !empty($registrantDetails))
			{   
				
				$count=0;
				foreach($registrantDetails as $registrant)
				{
					
					 $eventId=$registrant->event_id; 
					 $required='';
					 $checked='';
					 $readonly='';
					 $registrantFee=0;
					
					 $feesArray=RegistrationFees($registrant->id);
					 if(!empty($feesArray))
					 {
						 $registrantFee=$feesArray['fees'];
					 }
					
					
					if($registrant->id==$registrant_id)
					{
						 $checked='bgminus17';	 
					}
					if(isset($regisLimitExceed) && !empty($regisLimitExceed))
					{
						if(in_array($registrant->id,$regisLimitExceed))
						{
						    $readonly='readonly';
						} 
					} 
					if(strlen($registrant->password))
					{
						$required='required';
					}
					$regiTotalPrice=$registrant->total_price;
					if($registrant->allow_earlybird>0 && strtotime($registrant->earlybird_date) >= strtotime(date('Y-m-d')))
					{
						 $regiTotalPrice=$registrant->total_price_early_bird; 
						 
					}
					 $regiTotalPrice=$registrantFee+$regiTotalPrice;
					 
					if($regiTotalPrice=='' || $registrant->complementry==1)
					{
						$regiTotalPrice='0';
					}
					 ?>
						<div class="regist_row sideevent_regis" >
						  <div class="tableheading_2">
						     <input type="radio" name="select_registrant" id="select_registration<?php echo $registrant->id; ?>" value="<?php echo $registrant->id;?>" class="select_registration<?php echo $registrant->id.' '.$checked; echo $readonly.' '.$required; ?> selectSideEventRegistrant">
						       <input type="hidden"  value="<?php echo $registrant->id; ?>" >
						       <input type="hidden" id="regis_amt<?php echo $registrant->id; ?>" value="<?php echo $regiTotalPrice; ?>" >
						       <input type="hidden" id="sideEventAmt<?php echo $sideeventRegisId; ?>" class="sideEventAmt<?php echo $sideeventRegisId; ?>" value="<?php echo $sideeventPrice; ?>">
						       <input type="hidden" id="regisName<?php echo $registrant->id; ?>" value="<?php  echo ucfirst($registrant->registrant_name); ?>" >
						      <label><?php echo ucfirst($registrant->registrant_name); ?></label>
						    <?php
						      if($registrant->group_booking_allowed)
			                    {
									 ?>
								   
									 <div class="regist_smallfont"><?php   echo "Group booking minimum is ".$registrant->min_group_booking_size." registrations."; ?></div>
									 <div class="regist_smallfont">
										  
									<?php
								   
								    if($registrant->percentage_discount_for_group_booking=='' || $registrant->percentage_discount_for_group_booking=='0')
									{
										 echo lang('noDiscountOnGroupMsg');
									}
									else
									{
										 ?>
										   <?php  echo lang('discountIsMsg').' '.$registrant->percentage_discount_for_group_booking.' '."%."; ?>
										  
										<?php 
									}  ?>
									 </div> <?php  
					            } 
					                
					             ?>  
					           <div class="regist_smallfont">
					            <?php echo $registrant->additional_details; ?> 
					            </div> 
					         </div>
					       	
					         <div class="tableheading_2"><?php echo '$'.$regiTotalPrice; ?></div>
                        </div> 
				    <?php 
			
					    if(!empty($registrant->option_for_single_day_registration) && isset($dayDetails) && !empty($dayDetails))
					    {
							
						    $checked=''; 
							if(isset($selectRegiDayWise) && !empty($selectRegiDayWise)>0)
							{
								$checked='bgminus17';
							}
						    ?>
                         <div class="regist_row sideevent_regis">
							<div class="regist_mmm">
							<div class="tableheading_2">
									<input type="radio" name="select_registrant" class="select_day<?php echo $registrant->id.' '.$checked; ?> selectRegistrant"  >
						            <input type="hidden"  value="<?php echo $registrant->id; ?>" >
									<label>  <?php $title_array = explode(' ', $registrant->registrant_name); $first_word = $title_array[0]; echo ucfirst($first_word); ?> Single Day Registration</label>
									 <div class="regist_smallfont"> Group booking minimum for single day registration is <?php echo $registrant->min_group_booking_size_for_single_day_registrant; ?> registrations.</div>
									<div class="regist_smallfont">
										<?php
										if($registrant->percentage_discount_for_single_day_registrant=='' || $registrant->percentage_discount_for_single_day_registrant=='0')
										{
											 echo "<br>".lang('noDiscountOnGroupMsg')."<br>";
											
										}
										else
										{
											 echo lang('discountIsMsg').' '.$registrant->percentage_discount_for_single_day_registrant.' '."%.";
										}
										?>
								     </div>  
										  
							</div>
							     <div class="tableheading_2"></div>
							</div>  
							<?php
							     $dayGuestPrice='0';
							     $daySideEventRegisId='';
							     $dayMaxGuest='';
								if(isset($dayDetails) && !empty($dayDetails))
								{
									if(isset($dateArray) && !empty($dateArray))
									{
										//$dateArray=datedaydifference($eventDetails[0]->starttime,$eventDetails[0]->endtime);
									 
										foreach($dayDetails as $day_detail)
										{
											
											if(!empty($dateArray) && array_key_exists($day_detail->day,$dateArray)) 
											{
			
												$daySideeventPrice='';
												$eventDayDate=$dateArray[$day_detail->day]; 
												$DMY= date('d', strtotime($eventDayDate['date'])).'th '. date('M Y',strtotime($eventDayDate['date']));
												$eventDayDate=$DMY; 
												$eventDay=$day_detail->day; 
												
												if(isset($dayPriceArray) && !empty($dayPriceArray)){
													if (array_key_exists($day_detail->id, $dayPriceArray)) {
														 
														 $daySideeventPrice= $dayPriceArray[$day_detail->id];
													}
												}
												//to check day registrant guest
												if(isset($dayRegistrantGuest) && !empty($dayRegistrantGuest)){
													if (array_key_exists($day_detail->id, $dayRegistrantGuest)) {
														 
														 $dayGuestPrice= $dayRegistrantGuest[$day_detail->id];
													}
												}
												
												//to check day side event registrant id
												if(isset($daySideEventRegistrantId) && !empty($daySideEventRegistrantId)){
													if (array_key_exists($day_detail->id, $daySideEventRegistrantId)) {
														 
														 $daySideEventRegisId= $daySideEventRegistrantId[$day_detail->id];
													}
												}
												
												//to check day side event registrant id
												if(isset($maxDayGuestArray) && !empty($maxDayGuestArray)){
													if (array_key_exists($day_detail->id, $maxDayGuestArray)) {
														 
														 $dayMaxGuest= $maxDayGuestArray[$day_detail->id];
													}
												}
												
												
												//$whereDay=array('registrant_id'=>$registrant->id,'day'=>$day);
												  
											   //  $dayDetails=getDataFromTabelWhereWhereIn('event_registrant_daywise','*',$whereDay,'id',$dayRegisIdArray);
												$readonly=''; 
												
												if(isset($selectRegiDayWise) && !empty($selectRegiDayWise)>0)
												{
													
													$checked='';
													if(in_array($day_detail->id,$selectRegiDayWise))
													{
													   $isDayWiseSelect='1';	
													   $checked='bgminus17';
													   if($daywiseid!='')
													   {
														   $daywiseid=$daywiseid.',';
													   }
													   $daywiseid=$daywiseid.$day_detail->id;
													}
													
												}
												
														$dayTotalAmt=$day_detail->total_price;
												
														if($day_detail->allow_earlybird>0 && strtotime($day_detail->date_early_bird) >= strtotime(date('Y-m-d')))
														{
															$dayTotalAmt=$day_detail->total_price_early_bird;
												
														}
														 //add registrqant fees
														  
														 $dayTotalAmt=$registrantFee+$dayTotalAmt;
														 
														
														if(isset($paid_day_regisId) && count($paid_day_regisId)>0)
														{
															foreach($paid_day_regisId as $key=>$value)
															{
																
																if($day_detail->id==$key && $paid_day_regisId[$key]==$day_detail->registrant_daywise_limit)
																{ 
																	 $readonly='readonly'; 
																} 
															}
														}
													
													 ?>
													
													 
														<div class="regist_mmm">
														
															<div class="tableheading_2 pt2pb2">
																 <input id="check<?php echo $day_detail->id; ?>" type="checkbox"  value="<?php echo $day_detail->id;?>" class="check_day<?php echo $registrant->id.' '.$checked;?> ml25 checked<?php echo $day_detail->id.' '.$readonly;?> sideEventDayRegis"  >
																 <input type="hidden"  value="<?php echo $day_detail->id; ?>">
																 <label class="pl25"><b><?php echo lang('regisDay'); echo $eventDay.' - ' ?></b> <?php echo $eventDayDate; ?> 
																  
																 <input type="hidden" id="day_amt<?php echo $day_detail->id; ?>" value="<?php echo $dayTotalAmt; ?>" >
																 <input type="hidden" class="sideEventDayAmt<?php echo $day_detail->id; ?>" value="<?php echo $daySideeventPrice; ?>">

															</div>
															<div class="tableheading_2 pt2pb2"><?php  echo '$'.$dayTotalAmt; ?></div>
														   
														</div> 
														
														 <!-- guest fields for day  -->
														 <div class="clearfix"></div>	
																 
															<div class="regist_mmm sideevent_regis guestFieldData dayGuestFiled<?php echo $day_detail->id;?>" style="display:none;">
																<div class="tableheading_2 pt2pb2 margin10">
																	<label class="disply_Inl clr_lightblue"><?php  echo lang('additionalGuestsMsg'); ?></label>
																	<div class="inputnubcontainer" onclick="sideEventAdditionalGuest('<?php echo $daySideEventRegisId;?>')" >

																	<input id="additional_guest<?php echo $daySideEventRegisId;?>" name="additional_guest<?php echo $daySideEventRegisId;?>" max="<?php echo $dayMaxGuest; ?>" min="0" value="0" class="mr15 guestspinner" readonly>
																	 
																	 </div>
																</div>
															  <div class="tableheading_2 pt0">$<span id="guest_prive_wrap<?php echo $daySideEventRegisId;?>"> <?php  echo $dayGuestPrice; ?></span></div>
																<input type="hidden" name="guest_price<?php echo $daySideEventRegisId; ?>" id="guest_price<?php echo $daySideEventRegisId; ?>" value="<?php echo $dayGuestPrice; ?>">
															   <div id="guest_fields<?php echo $daySideEventRegisId;?>" class="guestFieldData<?php echo $day_detail->id;;?>"></div>
																
															 </div>
															  <input type="hidden" name="daySideRegistId<?php echo $day_detail->id; ?>" id="daySideRegistId<?php echo $day_detail->id; ?>" value="<?php echo $daySideEventRegisId; ?>">

															  <input type="hidden" name="sideEventAmt<?php echo $daySideEventRegisId; ?>" id="sideEventAmt<?php echo $daySideEventRegisId; ?>" value="<?php echo $daySideeventPrice; ?>">
															
												
												  <?php
											}
										}// emd of inner loop	
									}
								} 	
								     ?>
					     </div> <?php
						}
						?>
				    <div class="clearfix"></div>	
						 
					<div class="regist_mmm sideevent_regis guestFieldData guestFiled<?php echo $registrant->id;?>" style="display:none;">
						<div class="tableheading_2 pt2pb2 margin10">
							
							<div class="spacer10"></div>
							<label class="disply_Inl clr_lightblue"><?php  echo lang('additionalGuestsMsg'); ?></label>
							<div class="inputnubcontainer" onclick="sideEventAdditionalGuest('<?php echo $sideeventRegisId;?>')" >
                            
							<input id="additional_guest<?php echo $sideeventRegisId;?>" name="additional_guest<?php echo $sideeventRegisId;?>" max="<?php echo $maxGuest; ?>" min="0" value="0" class="mr15 guestspinner" readonly>
							
							 </div>
						</div>
					     <div class="tableheading_2 pt0">$<span id="guest_prive_wrap<?php echo $sideeventRegisId;?>"> <?php  echo $guestPrice; ?></span></div>
                           <input type="hidden" name="guest_price<?php echo $sideeventRegisId; ?>" id="guest_price<?php echo $sideeventRegisId; ?>" value="<?php echo $guestPrice; ?>">
					       <div id="guest_fields<?php echo $sideeventRegisId;?>"></div>
					 </div>
					
					<input type="hidden" name="sideeventRegisId<?php echo $registrant->id; ?>" id="sideeventRegisId<?php echo $registrant->id; ?>" value="<?php echo $sideeventRegisId; ?>">	
						
				 <?php	 
			       
		        } // end of the outer loop
	        }
		   
	  ?>
            
			
		      
		  </div>
	
<script type="text/javascript">
   
   /* var id='<?php echo $registrant_id; ?>';
    var dayid=0;
	var base_url='<?php echo base_url(); ?>'; 
	var IMAGE='<?php echo IMAGE; ?>'; 
	var prevId=0; 
	var addsideevent=false;
	var guestremove=false; 
	  
	/** function for select registration **/
	// SelectRegistrant(id);
	 
   /** function for select registration daywise **/
   
	 //dayRegistration(id,dayid);
	


</script>

