<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
//--------This form used to show term and condition--------//	
  $termCondition=(isset($termConditionDetails) && !empty($termConditionDetails))?$termConditionDetails[0]->term_conditions:'';
  if(strlen($termCondition)=='0')
  {
	 $termCondition=lang('term_condition_default_msg');
  }  
?>


<div class="modal fade frontendpopup dn" id="termConditionPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close closebtnfront" data-dismiss="modal" aria-hidden="true">&times;</button>
		   <div class="commonS_circle ml0">
				<div class="lockicon_frontend"></div>
			</div>
            <h4 class="modal-title">Term & Conditions</h4>
            <div class="clearfix"></div>
      </div>
       <div class="modal-body">
            
       <div class="modelinner pl0 pr0">
       		
				<div class="position_R">
				     <?php echo $termCondition; ?>			     
			  </div>
 
        </div>
       </div>
      
    </div>
  </div>
</div>

<!-- close login popup code -->

 
