
   <?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * @description: This controller is used to manage login, registration
 * @create date: 21-Nov-2013
 * @auther: Rajendra Patidar
 * @email: rajendrapatidar@cdnsol.com
 * 
 */

class Sideevent extends MX_Controller{

	function __construct(){
		parent::__construct();
		$this->load->library(array('default_template','head','payments_pro'));
		$this->load->library(array('frnt_auth'));
		$this->load->library('factory',$this);
		
		$this->load->model(array('sideevent_model','common_model'));
		$this->form_validation->set_error_delimiters('<div class="server_msg_show">', '</div>');
		$this->load->language(array('frontend','common'));
		$this->load->helper('frontend');
		
		
	}

    /* 
     * @description: This function call by default and load template
     * @return: void
     *
     */
    function index()
    { 
		  
		  $this->factory->index();
	}

	/*
     * @description : This function is used for standard registration 
     *                to create personal details form, registrant type form.
     */
    function standardregister()
    {  
	 $this->factory->standardregister();
	}
	
	//........................................................................................................
      
	/*
     * @description : This function is used to create side event form
     * @param       : registrantId, regisDayId(registrant day wise Id)
     * @return      : ajax html sideevent data
     *
     */

	function createsideeventform()
	{
		$this->factory->createsideeventform();
	}

   //..........................................................................................................

	 /*
	 * @description: This function is used to create breakout form
	 * @return     : breakout html content
	 * @param      :
	 */
	 
    function createbreakoutform()
    {
	$this->factory->createbreakoutform();
    }
   
     //..........................................................................................................

     /*
     * @description: This function is used to save and update 
     * if user login then user details all updated.
     * @param: 
     * @return:void
     */

	function savestandardregister()
	{
	$this->factory->savestandardregister();
	}
	

	
    //..........................................................................................................

	/*
     * @description: This function is used to update all standard register details
     * @param: $registered_id for user, daywise_id(day wise selected registrant), registrant_id(registrant type id)       
     * @return : success message or failed message
     */

    function updatestandardregister()
	{
	$this->factory->updatestandardregister();
	}
    
    //..........................................................................................................

	/*
     * @description: This function is used to get registrant details
     * @return: registrant html content for payment page
     *@param:   registrantId
     */

	function registrantpayment()
	{
	$this->factory->registrantpayment();
	}
     
    //..........................................................................................................
 
    
	/*
     * @description: This function is used to get sidevent details
     * @return: sideevent html content for payment page
     *@param:   regis_sideeventId (registrant side event id)
     */
	function sideeventpayment()
	{
	$this->factory->sideeventpayment();
	}
	 
	//..........................................................................................................

	/*
     * @description: This function is used to show breakout for payment page
     * @return: html content of breakout
     * @param: breakout_stream_session_id
     */

	function breakoutpayment()
	{
	$this->factory->breakoutpayment();  
	}
	
   
	 //..........................................................................................................

	 /* @description  : This function is used to save conatct person detail
      * @return       :  @void
      *
      */
     
    function savecontactperson()
    {
    $this->factory->savecontactperson();    
    }

     //..........................................................................................................

	/*
	* @description: This function is used to  login for general user
    * @return:message
   */
    function userLogin()
    {
	 $this->factory->userLogin();   
    }
    //..........................................................................................................

    /*
    * @description: This function is used for create registrant payment page
    * @return     : Html content for payment form
    * $param      :
    */

	function createpaymentform()
	{
	$this->factory->createpaymentform();		
	}
    
    //..........................................................................................................

	 /*
	 * @description: This function is used to create edit side event form
	 * @return:      HTML content of sideevent form for login user
	 */
	function editsideeventform()
	{
	$this->factory->editsideeventform();	
	}
	/*
	* @description: This function is used to delete guest
	* @return: guest id
	*
	*/

	function removeguest()
	{
	$this->factory->removeguest();
	}//..........................................................................................................
	/*
	* @description: This function is used to show editable content of registrant
	* @return: registrant form conte
	* @parram: @void
	*/

	function editregistrant()
	{
	$this->factory->editregistrant();
	}
    //..........................................................................................................

	/*
	 * @description: This function is used to varificatrion of email
	 * @return     : void
	 *
	 */
	function emailverification()
	{
	$this->factory->emailverification();
	}


	//..........................................................................................................
 
	/*
     * @description: This function is used to re by Paypal
     * @return: void
     * @para:   paypal form details
     */
    function registrantPayments()
	{
	$this->factory->registrantPayments();  
	}
	//..........................................................................................................
	/*
     * @description: This function is used to check validate form fileds
     * @return:      true/false
     * @para  :  registrantId, regis_password
     */
	function checkFormFieldsValidation()
	{
	$this->factory->checkFormFieldsValidation();
	}
	
	//..........................................................................................................
 
	/*
     * @description: This function is used to check registration password
     * @return:      true/false
     * @para  :  registrantId, regis_password
     */
	function checkRegistrationPass()
	{
	$this->factory->checkRegistrationPass();
	}
	
	// ------------------------------------------------------------------------ 	
	/*
	 * @description: This function is used to get state list by master country id
	 * @return master state listing html
	 */
	
	public function masterstatelist()
	{
	$this->factory->masterstatelist();
	}
	
	// ------------------------------------------------------------------------   	
	/*
	 * @description: This function is used to get city list by master state id
	 * @return master city listing html
	 */
	 
	public function mastercitylist()
	{
	$this->factory->mastercitylist();
	} 	
	
	//..........................................................................................................
	/*
     * @description: This function is used to logout user form website
     * @return: void
     *
     */
     
    function logout()
    {
	$this->factory->logout();
	}
	
	//..............................................................................................................
    //This is testing email function for creating to test.
     
	
	function test()
	{
	$this->factory->test();
	} 
	
	//..........................................................................................................
	/*
     * @description: This function is used to get registrants
     * @param: sideevent_id
     * @return: void
     *
     */
	function getregistrants()
	{
	$this->factory->getregistrants();
	} 
	 
}


