<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 * This class is used to manage for home controller
 *
 * @package   validation
 * @author    Rajendra patidar
 * @email     rajendrapatidar@cdnsol.com
 * @since     Version 1.0
 * @filesource
 */
 
 
class Payments_pro{
	 
	private $sandbox = '';
	private $APIUsername = '';
	private $APIPassword = '';
	private $APISignature = '';
	private $APISubject = '';
	private $APIVersion = '';
	
	function __construct(){
		//create instance object
		$this->ci =& get_instance();
	    //$this->ci->load->library(array('form_validation'));
	    
	    // Load helpers
		$this->ci->load->helper('url');
		
		// Load PayPal library
	    $this->ci->config->load('paypal');
	  
		$config = array(
			'Sandbox' => $this->ci->config->item('Sandbox'), 			// Sandbox / testing mode option.
			'APIUsername' => $this->ci->config->item('APIUsername'), 	// PayPal API username of the API caller
			'APIPassword' => $this->ci->config->item('APIPassword'), 	// PayPal API password of the API caller
			'APISignature' => $this->ci->config->item('APISignature'), 	// PayPal API signature of the API caller
			'APISubject' => '', 									// PayPal API subject (email address of 3rd party user that has granted API permission for your app)
			'APIVersion' => $this->ci->config->item('APIVersion')		// API version you'd like to use for your call.  You can set a default version in the class and leave this blank if you want.
		);
		
		// Show Errors
		if($config['Sandbox'])
		{
			error_reporting(E_ALL);
			ini_set('display_errors', '1');
		}
		 //echo $this->ci->config->item('APIUsername'); die;
		$this->ci->load->library('paypal_pro', $config);	
	   
	}
	
	
	function do_direct_payment($userDataArray)
	{
				
		$DPFields = array(
							'paymentaction' => '', 						// How you want to obtain payment.  Authorization indidicates the payment is a basic auth subject to settlement with Auth & Capture.  Sale indicates that this is a final sale for which you are requesting payment.  Default is Sale.
							'ipaddress' => $_SERVER['REMOTE_ADDR'], 							// Required.  IP address of the payer's browser.
							'returnfmfdetails' => '1' 					// Flag to determine whether you want the results returned by FMF.  1 or 0.  Default is 0.
						);
						
		$CCDetails = array(
							'creditcardtype' => $userDataArray['card_type'],  // Required. Type of credit card.  Visa, MasterCard, Discover, Amex, Maestro, Solo.  If Maestro or Solo, the currency code must be GBP.  In addition, either start date or issue number must be specified.
							'acct' => $userDataArray['card_number'], 			  // Required.  Credit card number.  No spaces or punctuation.  
							'expdate' => $userDataArray['exp_date'],  		 // Required.  Credit card expiration date.  Format is MMYYYY
							'cvv2' => $userDataArray['ccv'], 			// Requirements determined by your PayPal account settings.  Security digits for credit card.
							'startdate' => '', 							    // Month and year that Maestro or Solo card was issued.  MMYYYY
							'issuenumber' => ''							    // Issue number of Maestro or Solo card.  Two numeric digits max.
						);
						
		$PayerInfo = array(
							'email' => $userDataArray['merchant_id'], 	// Email address of payer.
							'payerid' => '', 							// Unique PayPal customer ID for payer.
							'payerstatus' => '', 						// Status of payer.  Values are verified or unverified
							'business' => '' 							// Payer's business name.
						);
						
		$PayerName = array(
							'salutation' => 'Mr.', 						// Payer's salutation.  20 char max.
							'firstname' => '', 							// Payer's first name.  25 char max.
							'middlename' => '', 						// Payer's middle name.  25 char max.
							'lastname' => '', 							// Payer's last name.  25 char max.
							'suffix' => ''								// Payer's suffix.  12 char max.
						);
						
		$BillingAddress = array(
								'street' => '123 Test Ave.', 						// Required.  First street address.
								'street2' => '', 						// Second street address.
								'city' => 'Kansas City', 							// Required.  Name of City.
								'state' => 'MO', 							// Required. Name of State or Province.
								'countrycode' => 'US', 					// Required.  Country code.
								'zip' => '64111', 							// Required.  Postal code of payer.
								'phonenum' => '' 						// Phone Number of payer.  20 char max.
							);
							
		$ShippingAddress = array(
								'shiptoname' => '', 					// Required if shipping is included.  Person's name associated with this address.  32 char max.
								'shiptostreet' => '', 					// Required if shipping is included.  First street address.  100 char max.
								'shiptostreet2' => '', 					// Second street address.  100 char max.
								'shiptocity' => '', 					// Required if shipping is included.  Name of city.  40 char max.
								'shiptostate' => '', 					// Required if shipping is included.  Name of state or province.  40 char max.
								'shiptozip' => '', 						// Required if shipping is included.  Postal code of shipping address.  20 char max.
								'shiptocountry' => '', 					// Required if shipping is included.  Country code of shipping address.  2 char max.
								'shiptophonenum' => ''					// Phone number for shipping address.  20 char max.
								);
							
		$PaymentDetails = array(
								'amt' => $userDataArray['amt'], 							// Required.  Total amount of order, including shipping, handling, and tax.  
								'currencycode' => 'USD', 					// Required.  Three-letter currency code.  Default is USD.
								'itemamt' => '00.00', 						// Required if you include itemized cart details. (L_AMTn, etc.)  Subtotal of items not including S&H, or tax.
								'shippingamt' => '00.00', 					// Total shipping costs for the order.  If you specify shippingamt, you must also specify itemamt.
								'shipdiscamt' => '', 					// Shipping discount for the order, specified as a negative number.  
								'handlingamt' => '', 					// Total handling costs for the order.  If you specify handlingamt, you must also specify itemamt.
								'taxamt' => '', 						// Required if you specify itemized cart tax details. Sum of tax for all items on the order.  Total sales tax. 
								'desc' => 'Web Order', 							// Description of the order the customer is purchasing.  127 char max.
								'custom' => '', 						// Free-form field for your own use.  256 char max.
								'invnum' => '', 						// Your own invoice or tracking number
								'notifyurl' => ''						// URL for receiving Instant Payment Notifications.  This overrides what your profile is set to use.
							);	
				
		$OrderItems = array();
		$Item	 = array(
							'l_name' => '', 						// Item Name.  127 char max.
							'l_desc' => '', 						// Item description.  127 char max.
							'l_amt' => '', 							// Cost of individual item.
							'l_number' => '', 						// Item Number.  127 char max.
							'l_qty' => '', 							// Item quantity.  Must be any positive integer.  
							'l_taxamt' => '', 						// Item's sales tax amount.
							'l_ebayitemnumber' => '', 				// eBay auction number of item.
							'l_ebayitemauctiontxnid' => '', 		// eBay transaction ID of purchased item.
							'l_ebayitemorderid' => '' 				// eBay order ID for the item.
					);
		array_push($OrderItems, $Item);
		
		$Secure3D = array(
						  'authstatus3d' => '', 
						  'mpivendor3ds' => '', 
						  'cavv' => '', 
						  'eci3ds' => '', 
						  'xid' => ''
						  );
						  
		$PayPalRequestData = array(
								'DPFields' => $DPFields, 
								'CCDetails' => $CCDetails, 
								'PayerInfo' => $PayerInfo, 
								'PayerName' => $PayerName, 
								'BillingAddress' => $BillingAddress, 
								'ShippingAddress' => $ShippingAddress, 
								'PaymentDetails' => $PaymentDetails, 
								'OrderItems' => $OrderItems, 
								'Secure3D' => $Secure3D
							);
							
		$PayPalResult = $this->ci->paypal_pro->DoDirectPayment($PayPalRequestData);
		
		
		if(!isset($PayPalResult['ACK'])){
			 $msg = "Due to some technical issue transaction can not be completed.";
			 $msgArray = array('msg'=>$msg,'is_success'=>'false');
			 set_global_messages($msgArray);
			 echo json_encode(array('msg' =>'', 'status'=>'1'));
			 return true;
			
		}
	
		if(!$this->ci->paypal_pro->APICallSuccessful($PayPalResult['ACK']))
		{
			 $errors = $PayPalResult['ERRORS'];
			 $error=$errors['0'];
			 $msg=$error['L_LONGMESSAGE'];
			 
			 $msgArray = array('msg'=>$msg,'is_success'=>'false');
			 set_global_messages($msgArray);
			 echo json_encode(array('msg' =>'', 'status'=>'1'));
			 return true;
		}
		else
		{
			 // Successful call.  Load view or whatever you need to do here.
			 //function to save payment details
			 $this->ci->sideevent_model->savePaymentDetails($PayPalResult);	
			 return true;
			
		}
	}
	
	
}


/* End of file auth.php */
/* Location: ./system/application/modules/home/libraries/auth.php */
