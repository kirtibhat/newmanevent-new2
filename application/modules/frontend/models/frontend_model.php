<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Frontend_model extends CI_model{

    private $tablenmfrntregistrant='frnt_registrant';
    private $tablenmeventregistrants='event_registrants';
    private $tablenmuser='user';
    private $tablenmevent= 'event';
	private $tablenmfrntpaymentdetails='frnt_payment_details';
	private $tablenmfrntsideevent='frnt_side_event';
    private $tablenmsideevent='side_event';
    private $tablenmfrntbreakouts='frnt_breakouts';
    private $tablenmbreakouts='breakouts';
    private $tablenmbreakoutstreamsession='breakout_stream_session';
    private $tablenmbreakoutstream='breakout_stream';
    private $tablenmbreakoutsession='breakout_session';
    private $tablenmfrntformfieldvalue='frnt_form_field_value';
    private $tablenmcustomformfields='custom_form_fields';
     

	
  	//..........................................................................................................
     
	/*
    * @description: This function is used to create breakout payment form when user login 
    * @param      : registeredId     
    * @return     : Html content of the breakout payment form 
    */
    
    function createPaymentBreakoutForm($registeredId)
    {
		
	     $streamContent='';
	     //to get user slected breakout
	     $breakoutdetails=$this->common_model->getDataFromTabel('frnt_breakouts','*',array('registered_user_id'=>$registeredId));
		if($breakoutdetails)
		{
			foreach($breakoutdetails as $aboutbreakout)
			{
		         //to get breakout details from	 breakout_stream_session table
			     $breakoutAboutId=$aboutbreakout->stream_session_id;
			     $breakoutAbout=$this->common_model->getDataFromTabel('breakout_stream_session','*',array('id'=>$breakoutAboutId));
   
			    if($breakoutAbout)
			    {
				   foreach($breakoutAbout as $about)
				    {
						//to get breakout stream by stream Id	
					    $streamDetails=$this->common_model->getDataFromTabel('breakout_stream','*',array('id'=>$about->breakout_stream_id));
                        
                        //to get breakout seesion by session Id	 
					    $sessionDetails=$this->common_model->getDataFromTabel('breakout_session','*',array('id'=>$about->breakout_session_id));
                         
					   if(!empty($streamDetails) && !empty($sessionDetails))
						{
							// to get breakouts
							$breakoutDetails=$this->common_model->getDataFromTabel('breakouts','*',array('id'=>$streamDetails[0]->breakout_id),'','date','ASC');
							if($breakoutDetails)
							{
								 $data['breakout_date']= $date = date_create($breakoutDetails[0]->date);
								 $data['registered_id']=$registeredId;
                                 $data['streamDetails']=$streamDetails;
                                 $data['sessionDetails']=$sessionDetails;
                                 $data['breakout_aboutId']=$about->id; 
                                 //to load ajax breakout details for payment form
								 $streamContent=$streamContent.$this->load->view('ajax_breakout_payment',$data,true);
							}

						}
					} // end of inner foreach
					
				}

			} // end of the outer loop
			 
	    } 
	   
	       return $streamContent;
	}
	
	
	 
    /* function to check side event limit for registration
	* @#param: sideeventId
    * @return: enum value
    */
    
    function checksideeventlimit($sideevent_registrantId)
    {
		 $sideeventId='';
		 $sideeventRegistLimit=NULL;
		 $registrant_id='';
		 $registrant_limit=NULL;
		 
		 $user_id=frntUserLogin('frantendevent'); 
		 
		 //to get eventId
		 $eventId=eventId();
		
		 // to get sideeventId by side event registrant table
		 $sideEventRegistrant=$this->common_model->getDataFromTabel('side_event_registrant','side_event_id,registrant_limit,registrant_id',array('id'=>$sideevent_registrantId));
         if(!empty($sideEventRegistrant))
        {
			$sideeventId=$sideEventRegistrant[0]->side_event_id;
			$registrant_id=$sideEventRegistrant[0]->registrant_id;
			$registrant_limit=$sideEventRegistrant[0]->registrant_limit;
		}  	
	   
	     //get paid frnt registrant id
	  	 $frntRegistrantId=$this->paidRegistrantIdArray();
	  
	     //get sideevent limit
         $sideEventDetails=$this->common_model->getDataFromTabel('side_event','side_event_limit',array('id'=>$sideeventId));
        
        if(count($frntRegistrantId)>0)
        {  
			 $countSideevent=0;
			 $countSideeventRegistrant=0;
			foreach($frntRegistrantId as $frntRegistrant)
			{
			    //get frontend sideevent for registered_user
				$frntSideevent=$this->common_model->getDataFromTabel('frnt_side_event','id',array('registered_user_id'=>$frntRegistrant,'side_event_id'=>$sideeventId));   
                  
				 //to check selected registrant for this side event
				 $registrantDetails=$this->common_model->getDataFromTabel('frnt_registrant','registrant_id',array('id'=>$frntRegistrant));
				 if(!empty($registrantDetails) && !empty($frntSideevent))
				 {
					if($registrantDetails[0]->registrant_id==$registrant_id)
					{
						$countSideeventRegistrant+=count($frntSideevent);
					}
					
					// chech side event limit for event 
				    $countSideevent+=count($frntSideevent);
			        
				}
            } 
  
            if($countSideeventRegistrant!=0 && $countSideeventRegistrant==$registrant_limit)
            {
				return false; // if overall registrant limit not exists
			}
             //to check sideevent limit for this event
            if($sideEventDetails[0]->side_event_limit!=NULL && $countSideevent==$sideEventDetails[0]->side_event_limit)
            {
				return false; //if registrant limit not exists for this side event
			}
                 	 			
		} 
		  return true; //if limit exists
	      
	}    
	
	
	//........................................................................................................
    
	  
	/* @description:  create dynamic form fileds for personal details form 
	 * @#param     :  registeredId
     * @return     :  HTML content for fileds
     */ 
     
    function getStandardRegistrationDetails($registeredId)
    {
		
		 $filed_data='';
		 $eventId='';
         $registrantId='0';
         
         //to get loggedIn user Id
         $user_id=frntUserLogin('frantendevent');  
         
         //to get country list 
         $mastercountrydata= $this->common_model->getDataFromTabel('master_country','*');
         
         
         //to check registrant id post
         $post=$this->input->post();
		if($post && isset($post['registrantId']))
		{
			$registrantId=$this->input->post('registrantId');
		}
         
		 //to get event id 
		 $event_id=$this->common_model->getDataFromTabel('user','event_id',array('id'=>$user_id));
		 
		if($event_id)
		{
			$eventId=$event_id[0]->event_id;
		}
		 $filed_data='';
		
		 //to get custom form fileds for this form and eventId
		 $where=array('event_id'=>$eventId,'registrant_id'=>$registrantId);
		 $fields=$this->common_model->getDataFromTabelWhereWhereIn('custom_form_fields','*',$where,'field_status',array('1','2'));
		 
		if($fields)
		{ 
			$fieldName='';
			$placeholder='';
			foreach($fields as $field)
			{
				$fieldValue='';
				if($registeredId>0)
				{
					//to get form fields values
					$where=array('field_id'=>$field->id,'registered_user_id'=>$registeredId);
					$registeredDetails=$this->common_model->getDataFromTabel('frnt_form_field_value','*',$where);
					
					if($registeredDetails)
					{
						$fieldValue=$registeredDetails[0]->value;
					}
				}
                 // to create placeholder of field
				 $placeholder=ucfirst($field->field_name);
				 $fieldName=strtolower(str_replace(' ','_',$field->field_name));
				 $required='';
				 
				if($field->field_status=='2')
				{
					 $required='required_field';
					 $placeholder=ucfirst($field->field_name.'*');
				}
				
				//to add country falg for fax & mobile phone
				if($fieldName=='mobile_phone' || $fieldName=='fax')
				{
					 $required=$required.' flag_text_field codeFlag';
				}
				
				 //function for field validation
				//$required=$this->fieldValidation($fieldName);
				 
				 $default=$field->default_value;
				 $data['defaultArray']=json_decode($default);
			
				 // to send data to create personal details form for user
				 $data['field_id']=$field->id;
				 $data['field_name']=$fieldName;
				 $data['field_value']=$fieldValue;
				 $data['place_holder']=$placeholder;
				 $data['field_required']=$required;
				 $data['field_type']=$field->field_type;
				 $data['mastercountrydata']=$mastercountrydata;
				
				 
				 
				
				 //to add html content for personal details form
				 $filed_data=$filed_data.$this->load->view('ajax_personal_detail_fields',$data,true);
			}
				  // end of the first foreach 
				 return $filed_data;
		}
    }
   
    //........................................................................................................
    /* @description:  function for personal form contact personal details
	 * @#param     :  field name
     * @return      : class for validation
     */ 
    function fieldValidation($fieldName)
    {
		if($fieldName='first_name' || $fieldName='surname')
		{
			return 'alphaValue';
		}
		if($fieldName='fax' || $fieldName='mobile_phone')
		{
			return 'mobileValue';
		}
		if($fieldName='date_of_birth')
		{
			return 'readonly datetimepicker';
		}
		return 'alphaNumValue'; 
	}
    
    //........................................................................................................
    
    /* @description:  function to get sidevent for selected registrant 
	 * @#param     :  registrantId
     * @return      : ajax html sideevent data
     */ 
     
    function getSideeventDetails($registrantId)
    {
		$sideEventContent='';
		$eventId='';
		$regisDayId='';
	    $user_id=frntUserLogin('frantendevent');
		
		//to get eventId
		 $eventId=eventId();
		
        //get day wise id when user selected new registrant
        $post=$this->input->post();
		if($post && isset($post['regisDayId']))
		{
          $regisDayId=$this->input->post('regisDayId');
	    }
        
		$sideEventContent='No side event found.';
		$regisDaywiseId=explode(',',$regisDayId);
		$days=$regisDaywiseId[0];
		 
		//get sideevent by registrant 
		$whereIn=array('registrant_id'=>$registrantId,'registrant_daywise_id'=>NULL);
		$sideeventRegistrant=$this->common_model->getDataFromTabel('side_event_registrant','*',$whereIn);
     
		if($days>0)
		{
		  //to get sideevent by registrant	daywise
		  $whereIn=array('registrant_id'=>$registrantId);
		  $sideeventRegistrant=$this->common_model->getDataFromTabelWhereWhereIn('side_event_registrant','*', $whereIn,'registrant_daywise_id',$regisDaywiseId);
		}
		
		 $daysTotalPrice=0;
		if($sideeventRegistrant)
		{
			$count=0;
			$totalPrice='';
			$maxGuest='';
			$sideEventContent='';
			$sideeventTotalPrice=0;
			foreach($sideeventRegistrant as $sideeventRegis)
			{
				$count+=1;
				$readonly='';
			
				// check sideevent limit for registration
				$sideeventLimit=$this->checksideeventlimit($sideeventRegis->id);
				if(!$sideeventLimit)
				{
					$readonly='readonly';
				}
				//to get sideevent
				$sideeventDetails=$this->common_model->getDataFromTabel('side_event','*',array('id'=>$sideeventRegis->side_event_id));
				
				if($sideeventDetails)
				{ 
					
					 $totalPrice=$sideeventRegis->total_price; 
					
					if($sideeventDetails[0]->allow_earlybird>0 && strtotime($sideeventDetails[0]->earlybird_date)>= strtotime(date('Y-m-d')))
					{			
					     $totalPrice=$sideeventRegis->early_bird_price;
					}
					if($totalPrice=='' || $sideeventRegis->complementry==1)
					{
						 $totalPrice='0';
					}
					 
					 $sideeventTotalPrice=$sideeventTotalPrice+$totalPrice;
					 $isGuest=$sideeventRegis->additional_guest;
					 $compGuest=$sideeventRegis->complementory_guest;
					 
					 if($isGuest)
					 {
						 //set value for additional guest
						 $data['guest_price']=$sideeventRegis->total_price_for_additional_guest;
						 $data['maxGuest']=$sideeventRegis->maximum_number_of_additional_guest;
						 $data['is_additional_guest']=$isGuest;
					 }
					 
					 //check for complemetry guest
					 if($compGuest)
					 {
						 $data['max_comp_guest']=$sideeventRegis->maximum_number_of_complementory_guest;
						 $data['comp_guest_price']=$sideeventRegis->total_price_for_complementory_guest;
					 }
					  
					 //make array to send data on ajax_sideevent_view page
					 $data['sideevent_date']=dateFormate($sideeventDetails[0]->start_datetime,'d F Y');
					 $data['sideeventRegisId']=$sideeventRegis->id;
					 $data['sideeventName']=$sideeventDetails[0]->side_event_name;
					 $data['sideeventVenue']=$sideeventDetails[0]->venue;
					 $data['totalPrice']=$totalPrice;					 
					 $data['readonly']=$readonly;
                     
					//to get sideevent html content
				
					$sideEventContent=$sideEventContent.$this->load->view('ajax_sideevent_view',$data,true);
				}
			}
		} 
		         
					 return $sideEventContent;
		
	}
	
	//..........................................................................................................
    /* @description:   This function is used to get operand_number (organizer_number),event_number, registrant_number
	 * @#param     :   registrant_id
     * @return     :   number_array
    */ 
    function getDigitNumber($registrant_id)
    {
		//to get eventId
		$eventId=eventId();
		$this->db->select('te.event_number,te.user_id,tu.operand_number,ter.registrant_number');
		$this->db->from($this->tablenmeventregistrants.' as ter');
	    $this->db->from($this->tablenmuser.' as tu');
		$this->db->join($this->tablenmevent.' as te','te.user_id =tu.id','left');
		$this->db->where('te.id',$eventId);
		$this->db->where('ter.id',$registrant_id);
		$query = $this->db->get();
		$result= $query->row();
		return $result;

	}
	
   //..........................................................................................................
    /* @description:  function to save or update selected registrant according its condition
	 * @#param     :  registrantId, regisDayId(registrant day wise Id), condition
     * @return     :   registeredId (frontend registrant ID)
     */ 
     
    function saveSelectRegistrant($registrantId,$regisDayId,$registered_id='')
    {
		 
	     // to save day wise registrant in nm_event_registrant_daywise table
	     $userData='';
	     $daywiseEarlyBird='';
		 $user_id=frntUserLogin('frantendevent');
		 $daysid= explode(',',$regisDayId);
		 $days=$daysid[0];
		 
		 $eventId=eventId();
		 $regi['user_id']=$user_id;
		 $regi['registrant_id']=$registrantId;
		 $regi['event_id']=$eventId;  // add current event id
          
		if($days!='')
		{
				  
			   //check day wise early bird
			    $regisDayDetails=$this->common_model->getDataFromTabelWhereWhereIn('event_registrant_daywise','id,allow_earlybird,date_early_bird','','id',array($regisDayId));
				if($regisDayDetails)
				{
					
					foreach($regisDayDetails as $dayDetails)
					{
						if($dayDetails->allow_earlybird>0 && strtotime($dayDetails->date_early_bird) >= strtotime(date('Y-m-d')))
						{
							if(strlen($daywiseEarlyBird)>0)
							{
								$daywiseEarlyBird=$daywiseEarlyBird.','.$dayDetails->id;
							}
							else
							{
								$daywiseEarlyBird=$dayDetails->id;
							}
						}
					}
				}
			 $regi['registrant_day_wise_id']=$regisDayId;
			 $regi['is_day_wise']='1';
		}
		else
		{
			$regi['is_day_wise']='0';
			$regi['registrant_day_wise_id']='';
		}
        //to set commas daywise id
        $regi['earlybird_daywise_id']=$daywiseEarlyBird;
        
        //check for early registrant early_bird   
		$registrantDetails=$this->common_model->getDataFromTabel('event_registrants','allow_earlybird,earlybird_date,total_price,gst,total_price_early_bird,gst_early_bird,complementry',array('id'=>$registrantId));
			 
		if($registrantDetails)
		{
			$fees=0;
			$feesGst=0;
			
			$feesArray=RegistrationFees($registrantId);
			if(!empty($feesArray))
			{
				$fees=$feesArray['fees'];
				$feesGst=$feesArray['gst'];
				$regi['registrant_fees_price']=str_replace('-','',$fees);
				$regi['registrant_fees_gst']=str_replace('-','',$feesGst);
				$regi['registrant_fees_pay_by']=$feesArray['pay_by'];
			}
			if($registrantDetails[0]->complementry=='1')
			{
				$regi['registrant_total_price']='0';
				$regi['registrant_gst']='0';
			}
			else
			{
				$regi['registrant_total_price']=$registrantDetails[0]->total_price;
				$regi['registrant_gst']=$registrantDetails[0]->gst;
		
				if($registrantDetails[0]->allow_earlybird>0 && strtotime($registrantDetails[0]->earlybird_date) >= strtotime(date('Y-m-d')))
				{
					$regi['registrant_total_price']=$registrantDetails[0]->total_price_early_bird;
					$regi['registrant_gst']=$registrantDetails[0]->gst_early_bird;
					$regi['is_earlybird']='1';
				}
	    	}
			
		}
			
		// to save or update selected registrant in  nm_frnt_registrant table
		if(!empty($registered_id))
		{
			//update exist registrant details
			$whereId=array('id'=>$registered_id);
			$userData=$this->common_model->updateDataFromTabel('frnt_registrant',$regi,$whereId);
		}
		else
		{
			
		     
			 //to get 12 digint number as operand_number (organizer_number),event_number, registrant_number
			 $bookinNumber =  $this->getDigitNumber($registrantId);
			 $registrationNumber = getMasterNumber('registration',$eventId); //get event serial number
			 if(!empty($bookinNumber))
			 {	
				
				 //it create sixteen digit number as operand_number (organizer_number),event_number, registrant_number and registrationNumber
				 $regi['registration_booking_number']=$bookinNumber->operand_number.'-'.$bookinNumber->event_number.'-'.$bookinNumber->registrant_number.'-'.$registrationNumber;
			   
			     //to save registration number
			      $regi['registration_number'] =$registrationNumber;
			}
			 
			
			  $amt=$this->input->post('pay_amount');
			  if($amt=='0'){
				  $regi['payment_status']='1';
				  $regi['is_active']='1';
				  
			   }
			
			 
			//save new registrant data in frnt_registrant table
			 $userData=$this->common_model->addDataIntoTabel('frnt_registrant', $regi);
			  
		}
		 return $userData;
		
	}
	
	
	//..........................................................................................................
	 /*
     * @description : This function is used to check personal details form validate fileds
     * @return      : Void
     * @param       : registrantId
     */

	function checkValidateFormFileds($registrantId)
	{
       
		$user_id=frntUserLogin('frantendevent');
		
        //to get eventId
		$event_id=getEventId();
		
        //to get form details
	    $formDetails=$this->common_model->getDataFromTabel('form','id,name');
      
	   if($formDetails)
		{
			 $count=0;
			foreach($formDetails as $form)
			{
                //get form fields
				$formwhere=array('form_id'=>$form->id,'event_id'=>$event_id,'registrant_id'=>$registrantId);
				$fieldDetails=$this->common_model->getDataFromTabelWhereWhereIn('custom_form_fields','*',$formwhere ,'field_status',array('1','2'));
              
				if($fieldDetails)
				{
				   foreach($fieldDetails as $field)
					{
                     
						if($field->field_status=='2')
						{
							$count+=1;
							$fieldName=strtolower(str_replace(' ','_',$field->field_name));
							if($fieldName=='fax' || $fieldName=='mobile_phone')
							{
							  $this->form_validation->set_rules($fieldName, $field->field_name, 'trim|required|numeric');
							}
							else
							{
							  $this->form_validation->set_rules($fieldName, $field->field_name, 'trim|required');
							}
							//check country ,state,city
							if($fieldName=='address')
							{
							    $this->form_validation->set_rules('contact_country', 'country', 'trim|required');
                                $this->form_validation->set_rules('contact_state', 'state', 'trim|required');
                                //$this->form_validation->set_rules('contact_city', 'city', 'trim|required');

							}
							
						}
					}
					if($this->input->post('registrant_id')=='0' || $this->input->post('registrant_id')=='')
					{
					   $this->form_validation->set_rules('regis_select_id', 'registration type', 'trim|required');
					}
					 $post=$this->input->post();
					if(isset($post['payment_by']) && $post['payment_by']=='credit_card')
			        {
				        $this->form_validation->set_rules('card_type','Card type', 'trim|required');
				        $this->form_validation->set_rules('card_number','Card number', 'trim|required');
				        $this->form_validation->set_rules('expire_month_year','Expire month & year', 'trim|required');
				    }

				   if ($count>0 && !$this->form_validation->run())
					{
						 echo json_encode(array('msg' => validation_errors(), 'status' => '0',));
						 die;
					}
				}
			}
        }
        return true;
		
	}
	
	 //..........................................................................................................

    /*
     * @description: This function is used to save persoanl details form value 
     * @param:  regis_user_id, condition,registered_id    
     * @return : id
     */
     
	function savePersonalDetailsFormValue($regis_user_id,$condition='',$registered_id='')
	{   
         
		 $user_id=frntUserLogin('frantendevent');
		 $userData='';
	     $add='0';
		//to get eventId
		$event_id=getEventId();
		
		$registrantId=$this->input->post('registrant_id');
		
        // to get dynamic form for id,name
		$formDetails=$this->common_model->getDataFromTabel('form','id,name');

		if($formDetails)
		{
			foreach($formDetails as $form)
			{
                
                //get custom form fields value
				$formwhere=array('event_id'=>$event_id,'registrant_id'=>$registrantId,'form_id'=>$form->id);
				$fieldDetails=$this->common_model->getDataFromTabel('custom_form_fields','*',$formwhere);

				if($fieldDetails)
				{
					foreach($fieldDetails as $field)
					{
						$fieldName = str_replace(' ','_',$field->field_name);
						$fieldValue =$this->input->post($fieldName);
						$formdata['field_id'] = $field->id;
						$formdata['user_id'] =$user_id;
						$formdata['form_id'] = $form->id;
						
						//to convert in json formate and add country ,city, state
						if($fieldName=='address')
						{
							$address2=$this->input->post('address');
							$country=$this->input->post('contact_country');
							$state=$this->input->post('contact_state');
							$city=$this->input->post('contact_city');
							//to get name of country
							$countryName=$this->input->post('countryName');
							$stateName=$this->input->post('stateName');
							$cityName=$this->input->post('cityName');
							
							$addressArray = array('address1' =>$fieldValue, 'address2' =>$address2, 'country' =>$country, 'state' =>$state, 'city' =>$city);
                           
                            $addressNameArray=array('address1'=>$fieldValue,'address2' =>$address2,'cityName'=>$cityName,'stateName'=>$stateName,'countryName'=>$countryName);
                            
                            $regsultArray=array('addressId'=>$addressArray,'addressName'=>$addressNameArray);
                            
                            $address= json_encode($regsultArray); 
							$formdata['value'] = $address;
						}
						else
						{
							$formdata['value'] = $fieldValue;
						}
						
						if($condition=='save')
						{
						   //add personal form fields value
						   $formdata['registered_user_id']=$regis_user_id;
						   $userData= $this->common_model->addDataIntoTabel('frnt_form_field_value', $formdata);
						}
						else
						{

							$fieldDetails=$this->common_model->getDataFromTabel('frnt_form_field_value','id',array('field_id'=>$field->id,'registered_user_id'=>$registered_id));
                            if(empty($fieldDetails) && $add=='0')
                            {
							   $add='1';
							   $this->common_model->deleteRowFromTabel('frnt_form_field_value',array('registered_user_id'=>$registered_id));
				 
							} 
							if($add=='0')
							{
							   // update fields value
							  $whereId=array('field_id'=>$field->id,'registered_user_id'=>$registered_id);
							  $userData=$this->common_model->updateDataFromTabel('frnt_form_field_value',$formdata,$whereId);
						    }
						    else
						    {
								$formdata['registered_user_id'] = $registered_id;
								$this->common_model->addDataIntoTabel('frnt_form_field_value',$formdata);
							}
						}

					}
				}
			}//end outer loop
		}

		return $userData; 
	}
	
	//..........................................................................................................
    /*
    * @description: This function is used to save sideevent details 
    * @param:  regis_user_id, sideeventRegistrantId      
    * @return : sideeventId
    */
     
    function saveFrntSideevents($regis_user_id,$sideeventRegisId) 
    {
		
		$userdata='';
		$user_id=frntUserLogin('frantendevent'); 
		$regisSideevents= explode(',',$sideeventRegisId);
		$sideeventid=$regisSideevents[0];

		if($sideeventid!='')
		{
			foreach($regisSideevents as $sideeventRegis)
			{
				//to get side event by slected registrant. 
				$sideeventDetails=$this->common_model->getDataFromTabel('side_event_registrant','side_event_id',array('id'=>$sideeventRegis));
				if($sideeventDetails)
				{
				   //to check allow_earlybird
				   $sideeventObj=$this->common_model->getDataFromTabel('side_event','allow_earlybird,earlybird_date',array('id'=>$sideeventDetails[0]->side_event_id));
				   if($sideeventObj)
				    { 
						if($sideeventObj[0]->allow_earlybird>0 && strtotime($sideeventObj[0]->earlybird_date) >= strtotime(date('Y-m-d')))
						{
							 $sideeventData['is_earlybird']='1';
						}
					}
			
					$additionalGuest= $this->input->post('additional_guest'.$sideeventRegis);

					$sideeventData['user_id']=$user_id;
					$sideeventData['side_event_id']=$sideeventDetails[0]->side_event_id;
					$sideeventData['additional_guest']=$additionalGuest;
					$sideeventData['total_calculated_amount']=$this->input->post('eventTotalPrice'.$sideeventRegis);
					$sideeventData['registered_user_id']=$regis_user_id;
					$sideeventData['sideevent_registrant_id']=$sideeventRegis;
					
					// add data in frnt_side_event table
					$userdata = $this->common_model->addDataIntoTabel('frnt_side_event', $sideeventData);
	
					if($additionalGuest!=0)
					{
						$firstNames=$this->input->post('guest_first_name'.$sideeventRegis);
						$surnames=$this->input->post('guest_surname'.$sideeventRegis);
						$dietaries=$this->input->post('guest_dietary'.$sideeventRegis);
						$details=$this->input->post('guest_details'.$sideeventRegis);
						$guestdetails=$details[0];

						$sideeventGuest['frnt_side_event_id']=$userdata;
						foreach($firstNames as $key=>$value)
						{
							$surname=$surnames[$key];
							$dietary=$dietaries[$key];
							$sideeventGuest['guest_firstname']=$value;
							$sideeventGuest['guest_surname']=$surname;
							$sideeventGuest['dietary_requirment_id']=$dietary;
							if($guestdetails!='' && count($details)<=$key+1)
							{
								$sideeventGuest['details']=$details[$key];
							}
					
							// to add side event guest 
							$userdata = $this->common_model->addDataIntoTabel('frnt_side_event_guest', $sideeventGuest);
						}
					}
				}

			}
		}
		  return $userdata;
	}
     
     //..........................................................................................................

    /*
    * @description: This function is used to update sideevent details 
    * @param:  regis_user_id, sideeventRegistrantId      
    * @return : 
    */
     
    function updateSideevents($sideeventRegisId,$registered_id)
    {
		$userdata='';
		$user_id=frntUserLogin('frantendevent');
		$registrantSideevents= explode(',',$sideeventRegisId);
		$regis_sideeventid=$registrantSideevents[0];
     
	
		// get front side evnt details by registered user id
		$sideeventdetails=$this->common_model->getDataFromTabel('frnt_side_event','id,sideevent_registrant_id',array('registered_user_id'=>$registered_id));
        if($sideeventdetails)
        {
			foreach($sideeventdetails as $sideevent)
			{
				if(!in_array($sideevent->sideevent_registrant_id,$registrantSideevents))
				{
				
					//to remove guest
					$where=array('frnt_side_event_id'=>$sideevent->id);
					$userdata=$this->common_model->deleteRowFromTabel('frnt_side_event_guest',$where);
					
				    //to remove sideevent
					$whereId=array('sideevent_registrant_id'=>$sideevent->sideevent_registrant_id,'registered_user_id'=>$registered_id);
					$userdata=$this->common_model->deleteRowFromTabel('frnt_side_event',$whereId);
				}
			}
		} 
	
		if($regis_sideeventid!='')
		{
			 $frntSideEventId='';
			foreach($registrantSideevents as $regis_sideevent)
			{
				 
				$regisdetails=$this->common_model->getDataFromTabel('side_event_registrant','side_event_id',array('id'=>$regis_sideevent));
				if($regisdetails)
				{
					
					$additionalGuest= $this->input->post('additional_guest'.$regis_sideevent);
                    
					$sideeventData['user_id']=$user_id;
					$sideeventData['side_event_id']=$regisdetails[0]->side_event_id;
					$sideeventData['additional_guest']=$additionalGuest;
					$sideeventData['total_calculated_amount']=$this->input->post('eventTotalPrice'.$regis_sideevent);
					$sideeventData['registered_user_id']=$registered_id;
					$sideeventData['sideevent_registrant_id']=$regis_sideevent;
					//get front end sideevent
					$frntSideEventDetails=$this->common_model->getDataFromTabel('frnt_side_event','*', array('sideevent_registrant_id'=>$regis_sideevent,'registered_user_id'=>$registered_id));
				
					if($frntSideEventDetails)
					{
					   $frntSideEventId=$frntSideEventDetails[0]->id;
					  
					   // update front side event
					   $whereId=array('registered_user_id'=>$registered_id,'sideevent_registrant_id'=>$regis_sideevent);
					   $this->common_model->updateDataFromTabel('frnt_side_event', $sideeventData,$whereId);
					   // to get  selected guest by user to check  
					   $frntGuestDetails=$this->common_model->getDataFromTabel('frnt_side_event_guest','*',array('frnt_side_event_id'=>$frntSideEventDetails[0]->id));
					  
					   if($frntGuestDetails)
					    {
						   foreach($frntGuestDetails as $frntguest)
						    {
									
							   // to create dynamic id and get guest form fields values 
							   $guest['frnt_side_event_id']=$frntSideEventId;
							   $guest['guest_firstname']=$this->input->post('guestfirstname'.$frntguest->id);
							   $guest['guest_surname']=$this->input->post('guestsurname'.$frntguest->id);
							   $guest['dietary_requirment_id']=$this->input->post('guestdietary'.$frntguest->id);
							   $guest['details']=$this->input->post('guestdetails'.$frntguest->id);
							 
							   // update guest details
							    $whereId=array('id'=>$frntguest->id);
							    $userdata=$this->common_model->updateDataFromTabel('frnt_side_event_guest',$guest,$whereId);
						    }
					    }
					}
					else
					{ 
						
					    // to add new sideevent selected by user
						$frntSideEventId= $this->common_model->addDataIntoTabel('frnt_side_event',$sideeventData);
					    
					}
					 // to add new guest 
					 $post=$this->input->post();
					if($post && isset($post['guest_first_name'.$regis_sideevent]))
					{
						$firstNames=$this->input->post('guest_first_name'.$regis_sideevent);
						$surnames=$this->input->post('guest_surname'.$regis_sideevent);
						$dietaries=$this->input->post('guest_dietary'.$regis_sideevent);
						$details=$this->input->post('guest_details'.$regis_sideevent);
						$guestdetails=$details[0];

						$sideeventGuest['frnt_side_event_id']=$userdata;
						foreach($firstNames as $key=>$value)
						{
							$surname=$surnames[$key];
							$dietary=$dietaries[$key];
							$sideeventGuest['frnt_side_event_id']=$frntSideEventId;
							$sideeventGuest['guest_firstname']=$value;
							$sideeventGuest['guest_surname']=$surname;
							$sideeventGuest['dietary_requirment_id']=$dietary;
							if($guestdetails!='' && count($details)<=$key+1)
							{
								$sideeventGuest['details']=$details[$key];
							}
							// to save new guest details 
							$userdata = $this->common_model->addDataIntoTabel('frnt_side_event_guest', $sideeventGuest);
						}
					}
				}	

			}
		}
		   return true;
	}
	
	//..........................................................................................................

    /*
    * @description: This function is used to update breakouts details 
    * @param:  stream_session_id, registered_id(for which user)      
    * @return : 
    */
    function updateBreakouts($stream_session_id,$registered_id)
    {
		// to take multiple selected breakout id in the form of comma seprated value
		 $userdata='';
		 $user_id=frntUserLogin('frantendevent');
		 $breakoutArray= explode(',',$stream_session_id);
		 $breakouts=$breakoutArray[0];
		 $breakoutData['user_id']=$user_id;
		 $breakoutData['registered_user_id']=$registered_id;
		 
		 // to get user breakout
		 $breakoutDetails=$this->common_model->getDataFromTabel('frnt_breakouts','*',array('user_id'=>$user_id));
				  
		 if($breakoutDetails)
		{
			 foreach($breakoutDetails as $breakoutdetail)
			 {
				 if(!in_array($breakoutdetail->stream_session_id,$breakoutArray))
				 {
					 // to delete user breakouts
					 $whereDelteId=array('stream_session_id'=>$breakoutdetail->stream_session_id,'registered_user_id'=>$registered_id);
					 $userdata=$this->common_model->deleteRowFromTabel('frnt_breakouts',$whereDelteId);
					
				 }
			 }
		}
		if(count($breakouts)>0)
		{
			foreach($breakoutArray as $breakout)
			{
			   $breakoutData['stream_session_id']=$breakout;
			   $whereId=array('stream_session_id'=>$breakout,'registered_user_id'=>$registered_id);
			   
			   $breakoutdetails=$this->common_model->getDataFromTabel('frnt_breakouts','*', $whereId);
			   // if data exists then update otherwise add in else
			   if($breakoutdetails)
			   {
				  $userdata=$this->common_model->updateDataFromTabel('frnt_breakouts', $breakoutData, $whereId);
			   }
			   else
			   {
				    $userdata = $this->common_model->addDataIntoTabel('frnt_breakouts', $breakoutData);
			   }
			}
		}
		 return true;
	}
		
	/*
    * @description: This function is used to create payment form when user login 
    * @param      :        
    * @return     : Html content of the payment form 
    */
    
	function createPaymentForm()
	{
		
		 $totalamt='';
	     $paymentContent='';

	     $user_id=frntUserLogin('frantendevent');
	     $eventId=eventId();
	     $where=array('user_id'=>$user_id,'registrants_type'=>'0');
	     $regiDetails=$this->common_model->getDataFromTabel('frnt_registrant','*',$where);
        
	    if($regiDetails)
	    {
			$count=1;

		   foreach($regiDetails as $regi)
		    { 
			    $totalamt='0';
			    $fees=0;
			    $registrantDetails=$this->common_model->getDataFromTabel('event_registrants','*',array('id'=>$regi->registrant_id));
			    
			    $totalamt=$this->registrantTotalAmt($regi->id);
			    
				if($registrantDetails)
				{
					 $regiData['booking_id']=$regi->registration_booking_number;
					 $regiData['registeredId']=$regi->id;
					 $regiData['registrantDetails']=$registrantDetails;
					 $regiData['regis_count']=$count;
					 $regiData['payment_status']=$regi->payment_status;
					 
					 
					//to get payment option for event
					if(!empty($registrantDetails))
					{
						$fessArray=RegistrationFees($registrantDetails[0]->id);  
						if(!empty($fessArray))
						{
							$fees=$fessArray['fees']; 
						}
				    }
					 
					
					 $regiData['totalamt']=$totalamt;
					 
					 // to add registrant content
					 $registrantContent=$this->load->view('ajax_registrant_payment',$regiData,true); 
					
					 // to add sidevent content
					 $sideeventContent=$this->createPaymentSideeventForm($regi->id);
					
					 // to add breakout content
					 $breakoutContent=$this->createPaymentBreakoutForm($regi->id);
					 
					  // to show total amount (for all registrantion)
					 $totalamt=$this->getTotalAmount($regi->id);
 
				     //$totalamt=$totalamt+$fees;
				     
					 $totalData['total_amt']=$totalamt;
					 $totalData['registered_id']=$regi->id;
					 
					 //to get total caclute html content
					 $totalContent=$this->load->view('ajax_total_payment',$totalData,true);
					 
					 $paymentContent= $paymentContent.$registrantContent.$sideeventContent.$breakoutContent.$totalContent;
					 $count+=1;
				}  

			} 	    
          	   return $paymentContent;
	    }
		 
	}
	
	
	//..........................................................................................................

	/*
    * @description: This function is used to calculate registrant amount
    * @param      : registeredId     
    * @return     : Registrant Amount
    */
     
     function registrantTotalAmt($registeredId)
     {
		 
		$totalamt='0';
		$fees='0';
		 $where=array('id'=>$registeredId);
	     $regiDetails=$this->common_model->getDataFromTabel('frnt_registrant','registrant_id,registrant_day_wise_id',$where);
        
        //calculate registration fees
		if(!empty($regiDetails))
		{
			$fessArray=RegistrationFees($regiDetails[0]->registrant_id);  
			if(!empty($fessArray))
			{
				$fees=$fessArray['fees']; 
			}
		}
		 
         if($regiDetails)
         {
			 
			 $daysid= explode(',',$regiDetails[0]->registrant_day_wise_id);
			 $days=$daysid[0];
			
			if($days!='')
			{
				$totalamt=0;
				
				$daywiseDetails=$this->common_model->getDataFromTabelWhereWhereIn('event_registrant_daywise','*','','id',$daysid);
			   
				if(!empty($daywiseDetails))
				{
					foreach($daywiseDetails as $dayDetails)
					{
						if($dayDetails->allow_earlybird>0 && strtotime($dayDetails->date_early_bird) >= strtotime(date('Y-m-d')))
						{
							$totalamt+=$dayDetails->total_price_early_bird;
						}
						else
						{
							$totalamt+=$dayDetails->total_price;
						}
						$totalamt=$totalamt+$fees;
					}
					  
				}
					     
			}
			else
			{
				$registrantDetails=$this->common_model->getDataFromTabel('event_registrants','*',array('id'=>$regiDetails[0]->registrant_id));
				  
				if($registrantDetails)
				{
					 $totalamt=$registrantDetails[0]->total_price;
					
					if($registrantDetails[0]->allow_earlybird>0 && strtotime($registrantDetails[0]->earlybird_date) >= strtotime(date('Y-m-d')))
					{
					  $totalamt=$registrantDetails[0]->total_price_early_bird;
					}
					if($registrantDetails[0]->complementry=='1')
					{
						$totalamt=0;
					}
					$totalamt=$totalamt+$fees;
				}
			}
			
		}
		   
	 
		return $totalamt;
	 }
	
      
	//..........................................................................................................

	/*
    * @description: This function is used to calculate sideevent amount
    * @param      : registeredId     
    * @return     : TotalAmount (Registrant + sideevent)
    */
	
	function getTotalAmount($registeredId)
	{
		// to get registrant total amount
		 $totalamt=0;
		
		 $eventId=eventId();
		 
		 $where=array('id'=>$registeredId);
	     $regiDetails=$this->common_model->getDataFromTabel('frnt_registrant','registrant_id,registrant_day_wise_id',$where);
        if($regiDetails)
        {
			$registrant_id=$regiDetails[0]->registrant_id;
		    $totalamt=$this->frontend_model->registrantTotalAmt($registeredId);
		   
		    //get sideevent for registered user
			$frntSideEventDetails=$this->common_model->getDataFromTabel('frnt_side_event','*',array('registered_user_id'=>$registeredId));
         
			if($frntSideEventDetails)
			{
				foreach($frntSideEventDetails as $sideevent)
				{
					 $totalamt=$totalamt+$sideevent->total_calculated_amount;
				}
			}
			
			
			 return  $totalamt;
		}
	}

	
	
	//..........................................................................................................

	/*
    * @description: This function is used to create side event payment form when user login 
    * @param      : registeredId     
    * @return     : Html content of the side event payment form 
    */
     
    function createPaymentSideeventForm($registeredId)
    {   
		  
		 $sideeventContent=''; 
		 $totalamt=0;
		 $registrant_id='';
		 $sideeventAmt='';
		 //to get registrant total amount
		 $where=array('id'=>$registeredId);
	     $regiDetails=$this->common_model->getDataFromTabel('frnt_registrant','registrant_id,registrant_day_wise_id',$where);
        if($regiDetails)
        {
			$registrant_id=$regiDetails[0]->registrant_id;
		    $totalamt=$this->registrantTotalAmt($registeredId);
		}
		 
		 //get sideevent for registered user
		 $frntSideEventDetails=$this->common_model->getDataFromTabel('frnt_side_event','*',array('registered_user_id'=>$registeredId));
          
		if($frntSideEventDetails)
		{
			foreach($frntSideEventDetails as $sideevent)
			{
				 $eventTotalPrice=$sideevent->total_calculated_amount;
				 $sideeventId=$sideevent->side_event_id;
				 $guests=$sideevent->additional_guest;
				    
                 //to get sideevent
				 $sideeventDetails=$this->common_model->getDataFromTabel('side_event','*',array('id'=>$sideevent->side_event_id));
                
                  //to get sideeevent registrant wise
                 $where=array('id'=>$sideevent->sideevent_registrant_id);
				 $sideeventRegistrant=$this->common_model->getDataFromTabel('side_event_registrant','*',$where);
                
				if(!empty($sideeventDetails) && !empty($sideeventRegistrant))
				{
				     $guestsPrice=0;
				     $date=date_create($sideeventDetails[0]->start_datetime);
 
                     $guestsPrice='0';
				    if($guests>0)
					{
					    //calculation for complementru guest
					    if($sideeventRegistrant[0]->complementory_guest==1)
					    {
							$compGuest=$sideeventRegistrant[0]->maximum_number_of_complementory_guest;
							$compPrice=$sideeventRegistrant[0]->total_price_for_complementory_guest;
							$guestsPrice=$compGuest*$compPrice;
							if($guests<=$compGuest)
							{
								$guestsPrice=$guests*$compPrice;
							}
							if($guests>$compGuest)
							{
							   $aditGuest=$guests-$compGuest;
							   $guestsPrice=$guestsPrice+$sideeventRegistrant[0]->total_price_for_additional_guest*$aditGuest; 
						    }
						}
						else
						{
					       $guestsPrice=$sideeventRegistrant[0]->total_price_for_additional_guest*$guests;
					    }
					     
					     $totalamt=$totalamt+$guestsPrice;
					}

                      // check for early bird price
					 if($sideevent->total_calculated_amount>$guestsPrice){ 
                        $sideeventAmt=$sideevent->total_calculated_amount-$guestsPrice;
				     }
                  
					$totalamt=$totalamt+$sideeventAmt+$guestsPrice;
					
					if($sideeventDetails)
					{
					     $data['registeredId']=$registeredId;
					     $data['sideeventDetails']=$sideeventDetails;
					     $data['guests_price']=$guestsPrice;
					     $data['total_guests']=$guests;
					     $data['sideevent_amt']=$sideeventAmt;
					     $data['selectdate']=$date;   
					     $data['sideevent_registrantId']=$sideeventRegistrant[0]->id;
					}
				
					//to load sideevent html content on payment page
					 $sideeventContent=$sideeventContent.$this->load->view('ajax_sideevent_payment',$data,true);
				     
				}

			}   // end of the outer loop
               return $sideeventContent;
		}
	}
	
	
	
	//..........................................................................................................
	/* 
    * @description: This function is used to create edit sideevent form 
    * @param      :       
    * @return     : Html content of the edit sideevent form 
    */
	
    function editSideeventForm($registeredId)
    {
		$sideEventContent='';
		$registrantId='';
		$sideeventRegistrant='';
		$totalamt='0';
		$user_id=frntUserLogin('frantendevent');
		
		//to get eventId
		$eventId=eventId();
		
		$sideeventTotalPrice=0;
   
        $post=$this->input->post();
		
			
			$daysTotalPrice=0;
			$sideEventContent='No side event found.';
			//$registeredId=$this->input->post('selectregiid');
		
			//to get selected registrant by id
			$registeredUser=$this->common_model->getDataFromTabel('frnt_registrant','*',array('id'=>$registeredId));
            
			if($registeredUser)
			{
					
				$registrantId=$registeredUser[0]->registrant_id;
				$daysid= explode(',',$registeredUser[0]->registrant_day_wise_id); 
				$days=$daysid[0];

				if($days=='')
				{
				  //to get sideevent registrant
				  $sideeventRegistrant=$this->common_model->getDataFromTabel('side_event_registrant','*',array('registrant_id'=>$registrantId,'registrant_daywise_id'=>NULL));
				 }
				else
				{  
				    //to get daywise sideevent registrant
					$sideeventRegistrant=$this->common_model->getDataFromTabelWhereWhereIn('side_event_registrant','*',array('registrant_id'=>$registrantId),'registrant_daywise_id',$daysid);
				}
				
 				if($sideeventRegistrant)
				{

					$count=0;
					$totalPrice='';
					$maxGuest='';
					$sideEventContent='';
                   
					foreach($sideeventRegistrant as $sideevent)
					{
						$count+=1;
						$class='';
						$guest='0';
						$eventTotalPrice=0;

						$frontSideEventId=0;
						$display='style="display:none;"';
					
						//to get side event
						$sideeventDetails=$this->common_model->getDataFromTabel('side_event','*',array('id'=>$sideevent->side_event_id));
					
						 $totalPrice=$sideevent->total_price;
						if($sideeventDetails)
						{
							if($sideeventDetails[0]->allow_earlybird>0 && strtotime($sideeventDetails[0]->earlybird_date) >= strtotime(date('Y-m-d')))
							{
								 $totalPrice=$sideevent->early_bird_price;
							}
						}
						if(!empty($sideeventDetails))
						{
							 $class='';
							 $guestPrice=0;
							 $totalamt=0;
							 $guests=0;
			
							 //to get side event guest
							 $isGuest=$sideevent->additional_guest;
							 $compGuest=$sideevent->complementory_guest;
							
							 // to get user selected sideevent
					       	$frntSideeventDetails=$this->common_model->getDataFromTabel('frnt_side_event','*', array('sideevent_registrant_id'=>$sideevent->id,'registered_user_id'=>$registeredId));
                          
							if(!empty($frntSideeventDetails))
							{
								$class='bgminus17';
								$display='';
								$guests=$frntSideeventDetails[0]->additional_guest;
							    $totalamt=$totalamt+$frntSideeventDetails[0]->total_calculated_amount;
							    $frontSideEventId=$frntSideeventDetails[0]->id;
						
					    	}
							if($isGuest)
							{
								 $compGuest=0;
								 $compPrice=0;
								  
								 //calculate guest amount
								if($sideevent->complementory_guest==1)
								{
									$compGuest=$sideevent->maximum_number_of_complementory_guest;
									$compPrice=$sideevent->total_price_for_complementory_guest;
									$guestPrice=$compGuest*$compPrice;
									if($guests<=$compGuest)
									{
										$guestPrice=$guests*$compPrice;
									}
									if($guests>$compGuest)
									{
									   $aditGuest=$guests-$compGuest;
									   $guestPrice=$guestPrice+$sideevent->total_price_for_additional_guest*$aditGuest;
									}
								}
								else
								{
								   $guestPrice=$sideevent->total_price_for_additional_guest*$guests;
								} 
								    
									 //set value for additional guest
								     $data['addi_guest_price']=$sideevent->total_price_for_additional_guest;
									 $data['guest_price']=$guestPrice;
									 $data['max_guest']=$sideevent->maximum_number_of_additional_guest;
									 $data['is_additional_guest']=$isGuest;
							
									 $data['max_comp_guest']=$compGuest;
									 $data['comp_guest_price']=$compPrice;
							        
							}
							  
							  $eventTotalPrice=$guests*$guestPrice+$totalPrice;
							  //to check not selected sideevent
							  if(empty($frntSideeventDetails))
							  {
								 $eventTotalPrice='0';
							  }
						      
							 $guestForm=$this->createGuestForm($frontSideEventId,$sideevent->id);
							
							 $data['sideeventDetails']=$sideeventDetails;
							 $data['total_price']=$totalPrice;
							 $data['display_class']=$display;
							 $data['total_guest']=$guests;
							 $data['guest_form']=$guestForm;
							 $data['regi_count']=$count;
							 $data['add_class']=$class;
							 $data['event_total_price']=$eventTotalPrice;
							 $data['sidevent_registrant_id']=$sideevent->id;
							
							 //to load edit ajax sideeent page
							
							 $sideEventContent=$sideEventContent.$this->load->view('ajax_edit_sideevent',$data,true);
						}
					}// end of the outerloop
				}
            }
		
		   return $sideEventContent;
	}
	
	
	
	//..........................................................................................................

	/*
    * @description: This function is used to create guest form 
    * @param      :  frntsideeventid, sideeventid    
    * @return     : Html content of guest form
    */
	
	
	function createGuestForm($frntsideeventid,$sideevent_registrantid)
	{
		
		 $guestCount=0;
		 $guestContent='';
		 $eventDietary='';
		 $guestId='';
		 $user_id=frntUserLogin('frantendevent');
		
		 //to get eventId
		 $eventId=eventId();
		
         //to get dietary
		 $eventDietarys=$this->common_model->getDataFromTabel('event_dietary','*',array('event_id'=>$eventId));
		 
		 //to get side event guest
		 $frntGuestDetails=$this->common_model->getDataFromTabel('frnt_side_event_guest','*',array('frnt_side_event_id'=>$frntsideeventid));

         $data['eventDietarys']=$eventDietarys;
         $data['frntGuestDetails']=$frntGuestDetails;
         $data['sideevent_registrantid']=$sideevent_registrantid;
        //to load guest form
         return $this->load->view('ajax_guest_form',$data,true);
		
	}
	
	/*
    * @description: This function is used to caculate amount for all registrant 
    * @param      :      
    * @return     : Html content of guest form
    */
  
	function getUserAllRegistrantAmt()
	{
		 $user_id=frntUserLogin('frantendevent');
		 $totalCalculateAmt=0;
		 $totalGst=0;
		 //get user event
		 $eventId=eventId();
		
		 //get user  registrant
		$userRegistrants=$this->common_model->getDataFromTabelWhereWhereIn('frnt_registrant','id,registrant_id,registrant_day_wise_id',array('user_id'=>$user_id,'registrants_type'=>'0'),'payment_status',array('0,1'));
       
        if($userRegistrants)
        {
			
			foreach($userRegistrants as $registrant)
			{
				$totalAmt=0;
				$daywise_ids=explode(',',$registrant->registrant_day_wise_id);
				$isday=$daywise_ids[0];
				
				//check for daywise registrants 
				if(!empty($isday))
				{
					$registrantObj=$this->common_model->getDataFromTabelWhereWhereIn('event_registrant_daywise','*','','id',$daywise_ids);
					
					if(!empty($registrantObj))
					{
					    foreach($registrantObj as $obj)
					    {
							if($obj->allow_earlybird>0 && strtotime($obj->date_early_bird) >= strtotime(date('Y-m-d')))
							{
								 $totalGst=$totalGst+$obj->gst_early_bird;
								 $totalAmt=$totalAmt+$obj->total_price_early_bird;
								 
							}
							else
							{
								$totalAmt=$totalAmt+$obj->total_price;
								$totalGst=$totalGst+$obj->gst;
							}
							$feesArray=RegistrationFees($registrant->registrant_id);
							if(!empty($feesArray))
							{
								$totalAmt=$totalAmt+$feesArray['fees'];
								$totalGst=$totalGst+$feesArray['gst'];
							}   
						}
						  
					}
				}
				else
				{
					//check for simple registrant
					$registrants=$this->common_model->getDataFromTabel('event_registrants','*',array('id'=>$registrant->registrant_id));
		          
		            if($registrants)
		            {
						if($registrants[0]->allow_earlybird>0 && strtotime($registrants[0]->earlybird_date) >= strtotime(date('Y-m-d')))
						{
							 $totalGst=$registrants[0]->gst_early_bird;
							 $totalAmt=$registrants[0]->total_price_early_bird;
						}
						else
						{
							$totalAmt=$registrants[0]->total_price;
							$totalGst=$registrants[0]->gst;
						}
						   
						$feesArray=RegistrationFees($registrant->registrant_id);
						if(!empty($feesArray))
						{
						    $totalAmt=$totalAmt+$feesArray['fees'];
						    $totalGst=$totalGst+$feesArray['gst'];
					    }
				
						if($registrants[0]->complementry=='1')
						{
						  $totalAmt='0';	
						}
					}
				}
				$totalCalculateAmt=$totalCalculateAmt+$totalAmt;
				//calcualtion of registrant sideevents
				$userSideevents=$this->common_model->getDataFromTabel('frnt_side_event','*',array('registered_user_id'=>$registrant->id));		
				if(!empty($userSideevents))
				{
					foreach($userSideevents as $userSideevent)
					{ 
						 $totalCalculateAmt= $totalCalculateAmt+$userSideevent->total_calculated_amount; 
					}
				}
				
			}
				 
		}
		   if($totalGst==''){ $totalGst='0';}
	       $totalAmtArray=array('total_amt'=>$totalCalculateAmt,'total_gst'=>$totalGst);
	       //print_r($totalAmtArray); die;
	       return $totalAmtArray;
	   
	}
	
	function CheckSideEventRegistrant()
	{
		$regisrant=new newmanEvent();
	}
	 
	//..........................................................................................................

	/*@description  : This function is used to check personal contact form fields
    * @return        : Validation message
    * @param         :  
    */
    
    function checkContactFormFields()
    {
		
		 $eventId=$this->input->post('regis_event_id');
		 $user_id=frntUserLogin('frantendevent');
		  
		 //get event_limit from by event id
		 $event_details=$this->common_model->getDataFromTabel('event','number_of_rego',array('id'=>$eventId));
		  
		 //check event limit for registered user
         $eventLimit=$this->isEventLimitExists();
		if(!$eventLimit)
		{
			echo json_encode(array('msg' =>lang('registrationLimitMsg'), 'is_success'=>'false'));
			die;
			
		}
		 $this->form_validation->set_rules('contact_first_name', 'contact first name', 'trim|required');
		 $this->form_validation->set_rules('contact_surname', 'contact surname', 'trim|required');
		 $this->form_validation->set_rules('contactphone', 'contact phone', 'trim|required');
		 $this->form_validation->set_rules('contact_cmp_name', 'contact Company name', 'trim|required');
		 $this->form_validation->set_rules('contact_position', 'contact position', 'trim|required');
		 if(!$user_id)
		 {
			 $this->form_validation->set_rules('contact_email', 'contact email', 'trim|required');
			 $this->form_validation->set_rules('contact_password', 'contact Password', 'trim|required|min_length[5]|max_length[25]');
			 $this->form_validation->set_rules('contact_confirm_password', 'contact Confirm Password', 'trim|required');
		 }

		if (!$this->form_validation->run())
		 {
			 echo json_encode(array('msg' => validation_errors(), 'is_success'=>'false'));
			 die;
		 }

		if($this->input->post('contact_password')!=$this->input->post('contact_confirm_password')){

			 echo json_encode(array('msg' => lang('passwordNotMatchMsg'), 'is_success'=>'false'));
			 die;
		}
		 // to check email exist for this event
		 $email = $this->input->post('contact_email');
		 $is_exist =$this->common_model->getDataFromTabel('user','id',array('email'=>$email,'event_id'=>$eventId,'user_type_id'=>'2'));
		 if ($user_id=='' && !empty($is_exist)) {
			 echo json_encode(array('msg' => lang('existsEmailMsg'), 'is_success'=>'false'));
			 die;
		 }
	}
	
    //..........................................................................................................
	/* @description  : This function is used to convert paypal url response in array form
    * @return        : array for payapal data
    * @param         :  paypal response url
    */
	function savePaymentDetails($proArray)
	{
		
		$user_id=frntUserLogin('frantendevent');
		//get event id
		$eventId=eventId();
		
		if(count($proArray)>0 && $proArray['ACK']=='Success')
		{
	         
			$bookingId=randomnumber('8');
			$data['total_amount']=$proArray['AMT'];
			$data['received_amount']=$proArray['AMT'];
			$data['booking_id']=$bookingId;
			$data['payment_type']='credit_card';
			$data['transaction_id']=$proArray['TRANSACTIONID'];
			$data['payment_date']=date('Y-m-d H:s:i');
			$data['user_id']=$user_id;
		
		 
			//add payment details in table
			$paymentId=$this->common_model->addDataIntoTabel('frnt_payment_details', $data);
			
			if(!empty($paymentId))
			{
					
				
				//to update status of payment
				$paymentData['payment_status']='1';
				$paymentData['payment_id']=$paymentId;
		
				 //update frnt_registrant data
				 $frntregistrant=$this->common_model->updateDataFromTabel('frnt_registrant',$paymentData,array('payment_status'=>'0','user_id'=>$user_id));
				//send mail to user
				
				$mailResponse=$this->sendRegistrantDetailsMail($paymentId);		
				
				if($mailResponse)
				{
					 $transaction='Transaction Id:- '.$proArray['TRANSACTIONID'];
					 $noteMessage='Please note your booking id for future references.<bt> Your registrant details has been also sent to your email address.';

					 $amt='Amount:- $'.$proArray['AMT'];
					 $msg=lang('paymentTransactionMsg').'<br>'.$transaction.'<br>'.'BookingId:- '.$bookingId.'<br>'.$amt.'<br><br><small>'.$noteMessage.'</small>';
					 
					$msgArray = array('msg'=>$msg,'is_success'=>'true');
					set_global_messages($msgArray);
					echo json_encode(array('msg' =>'', 'status'=>'1'));
					return true; 
				}
				else
				{
					 $msg='Transaction failed.Please try again.';
			
					 $msgArray = array('msg'=>$msg,'is_success'=>'true');
					 set_global_messages($msgArray);
					 echo json_encode(array('msg' =>'', 'status'=>'1'));
					 return true; 
				}
			}
				
			
		}
		else
		{
			
		     //when transaction failed
			 $msg='Transaction failed.Please try again.';
			
			 $msgArray = array('msg'=>$msg,'is_success'=>'true');
			 set_global_messages($msgArray);
			 echo json_encode(array('msg' =>'', 'status'=>'1'));
			 return true; 
		 
		}
	}
	
	function updatePaymentDetails()
	{
		$paymentId='0';
		$user_id=frntUserLogin('frantendevent');
		//get event id
		$eventId=eventId();
	
		 //to get registrant details by event id
		 $registrantDetails=$this->common_model->getDataFromTabel('event_registrants','id',array('event_id'=>$eventId,'registrants_type'=>'0'));
		
		 //get last user inserted id
		 $paymentDetails=getDataFromTabel('frnt_payment_details','id',array('user_id'=>$user_id),'','id','DESC','1');
		 if(!empty($paymentDetails))
		 {
			 $paymentId=$paymentDetails[0]->id;
		 }
 
		 $paymentData['payment_status']='1';
		 $paymentData['payment_id']=$paymentId;
		
		 if($registrantDetails)
		 { 
			foreach($registrantDetails as $registrant)
			{
				 //update frnt_registrant data
				 $frntregistrant=$this->common_model->updateDataFromTabel('frnt_registrant',$paymentData,array('registrant_id'=>$registrant->id,'payment_status'=>'0','user_id'=>$user_id));
			 
			}
			 return true;
		 }
		
	  return false;
		
	}
	

	//..........................................................................................................
	/*@description  : This function is used to check event limit of registrant
    * @return       : true (if event limit exists) otherwise error message
    * @param        :  
    */
    
    function isEventLimitExists()
    {
		$user_id=frntUserLogin('frantendevent');
		//get event id
		$eventId=eventId();
		
		 // to check event limit
	  	 $event_details=$this->common_model->getDataFromTabel('event','number_of_rego',array('id'=>$eventId));
	  	 
	  	 //this function return registrant frnt id array
		 $registrantArray=$this->paidRegistrantIdArray();
         
		if(!empty($event_details) && count($registrantArray)>0)
		{
			if($event_details[0]->number_of_rego==count($registrantArray))
			{
				 return false;
			}
		}
		return true; 
		
	}
	
	//..........................................................................................................
	/*@description  : This function is used to get paid registrant id array
    * @return       : paid id array from frnt_registrant table
    * @param        :  
    */
    
    function paidRegistrantIdArray()
    {
		
		
		$user_id=frntUserLogin('frantendevent');
		$eventId=eventId();
		
		$registeredArrayId=array();
		 
        $this->db->select('tfr.id');
        $this->db->from($this->tablenmuser.' as tu');
        $this->db->join($this->tablenmfrntregistrant.' as tfr','tfr.user_id=tu.id');
		$this->db->where('tu.event_id',$eventId);
		$this->db->where('tfr.payment_status','1');
		
		$query = $this->db->get();
	
	    $registrantDetails=$query->result();   
      
		 
	    //get registrant details by event id
	   	// $registrantDetails=$this->common_model->getDataFromTabel('event_registrants','id',array('event_id'=>$eventId));
        
        if($registrantDetails)
        {
			$regisIds='';
			foreach($registrantDetails as $registrant)
			{    
				if(strlen($regisIds)>0)
				{
					$regisIds=$regisIds.',';
				}
				  $regisIds=$regisIds.$registrant->id;
			}
			 
			if(strlen($regisIds)>0)
			{ 
				//get user registrant id
				$registrantDetails=$this->common_model->getDataFromTabel('frnt_registrant','id',array('payment_status'=>'1'));
				
				if($registrantDetails)
				{
					foreach($registrantDetails as $regis)
					{
						$registeredArrayId[]=$regis->id;
					}
				} 
	             return  $registeredArrayId;
			}
		}
	}
	
	//..........................................................................................................
	/*@description  : This function is used to save cheque and cash payment
    * @return       : @void
    * @param        : @void
    */
	function saveChequeCashPayment()
	{
		
		$userData='';
		$user_id=frntUserLogin('frantendevent');
		//get event id
		$eventId=eventId();
		$post=$this->input->post();
		
		if($post && isset($post['payment_by']) && isset($post['pay_amount']))
		{
			$payment_by=$this->input->post('payment_by');	
			$payment_by=$this->input->post('pay_amount'); 
			if($post['payment_by']=='cheque')
			{
				if(isset($post['bank_name']) && isset($post['cheque_number']))
				{
					 $data['transaction_id']=$post['cheque_number'];
					 $data['bank_name']=$post['bank_name'];
				}
			}    
			
			$booking_id=randomnumber('8');  
			$data['total_amount']=$post['pay_amount'];
			$data['received_amount']=$post['pay_amount'];
			$data['booking_id']=$booking_id;
			$data['payment_type']=$post['payment_by'];
			$data['payment_date']=date('Y-m-d H:s:i');
			
			
			$data['user_id']=$user_id;
		
			//add payment details in table
			$paymentId=$this->common_model->addDataIntoTabel('frnt_payment_details', $data);
			
			if(!empty($paymentId))
			{
			   
				//to update status of payment
				$paymentData['payment_status']='1';
				$paymentData['payment_id']=$paymentId;
		
				 //update frnt_registrant data
				$userData=$this->common_model->updateDataFromTabel('frnt_registrant',$paymentData,array('payment_status'=>'0','user_id'=>$user_id));
				
				
				 //send mail to user
				$mailResponse=$this->sendRegistrantDetailsMail($paymentId);
					
				if($mailResponse)
				{
					$noteMessage='Please note your booking id for future references.<bt> Your registrant details has been also sent to your email address.';
					
					$msg=lang('registrationSuccessMsg');
					$msgArray = array('msg'=>$msg.'<br>'.'BookingId:- '.$booking_id.'<br>'.'Amount:- $'.$post['pay_amount'].'<br><br><small>'.$noteMessage.'</small>','is_success'=>'true');
					set_global_messages($msgArray);
					echo json_encode(array('msg' =>'', 'status' => '1')); 
					die;
				}
			
				
			}
			else
			{   
				 //when transaction failed
				$msgArray = array('msg'=>lang('faildTryAgainMsg'),'is_success'=>'false');
				set_global_messages($msgArray);
				echo json_encode(array('msg' =>'', 'status' => '0')); 
				die;
				 
			}
		
		}
		  
	}
	
	//..........................................................................................................
	/*@description  : This function is used to send mail for regsitran details
    * @return       : @void
    * @param        : @void
    */
	
	function sendRegistrantDetailsMail($paymentId)
	{
		
		$userEmail='';
		$userId=frntUserLogin('frantendevent');
		//get event id
		$eventId=eventId();
	  
		$frntRegisDetails=$this->common_model->getDataFromTabel('frnt_registrant','id,registrant_id,is_day_wise,registrant_day_wise_id',array('payment_id'=>$paymentId));

			
		 //to get user payment details
	    $paymentDetails= $this->getEventUserPaymentDetails($paymentId);
		
		if(!empty($frntRegisDetails))
		{
			foreach($frntRegisDetails as $frntRegis)
			{
				unset($regisDetails);
                $regisDetails = array();
				$regis_details=$this->common_model->getDataFromTabel('event_registrants','registrant_name',array('id'=>$frntRegis->registrant_id));
				if(!empty($regis_details))
				{
					
					if($frntRegis->is_day_wise=='1')
					{
						 $dayRegistrant='';
						 $daysid= explode(',',$frntRegis->registrant_day_wise_id);
						 $dayRegisDetails=$this->common_model->getDataFromTabelWhereWhereIn('event_registrant_daywise','event_date_day','','id',$daysid);
                        
                        if(!empty($dayRegisDetails))
                         {
							foreach($dayRegisDetails as $dayRegis)
							{
							    $dayRegistrant=$dayRegistrant.dateFormate($dayRegis->event_date_day,'d F Y').', ';
							}
				 
						 }
						 
						$regisDetails['registrant_name']=$regis_details[0]->registrant_name.' '.'single day registration for '.$dayRegistrant; 
						 
					}
					else
					{
						$regisDetails['registrant_name']=$regis_details[0]->registrant_name;
				 
					}
			    }
			   
			    //get selected sideevent by front registered id
			    $sideeventDetails=$this->getSelectedSideevent($frntRegis->id);
			    $sideeventContent='';
			    if(!empty($sideeventDetails))
			    {
					foreach($sideeventDetails as $sideevent)
					{
						$sideeventContent=$sideeventContent.$sideevent->side_event_name.' '.dateFormate($sideevent->start_datetime,'d F Y').' '.'from '.date('H:s',strtotime($sideevent->start_datetime)).' '.'to '.date('H:s',strtotime($sideevent->end_datetime)).'<br>'; 
					}
				}
                 $regisDetails['sideevent']=$sideeventContent; 
			    $breakouts=$this->getSelectedBreakout($frntRegis->id);
			    $breakoutContent='';
			    if(!empty($breakouts))
			    {
					foreach($breakouts as $breakout)
					{
						 $breakoutContent=$breakoutContent.$breakout->breakout_name.' '.dateFormate($breakout->date,'d F Y').' '.$breakout->stream_name.' '.'from '.date('H:s',strtotime($breakout->start_time)).' '.'to '.date('H:s',strtotime($breakout->end_time)).'<br>';
					}
				}
				$regisDetails['breakout']=$breakoutContent;
				$dataDetails[]=$regisDetails;
				
				//insert payment details in array
				 $data['payemtntDetails']=$paymentDetails; 
			     $data['dataDetails']=$dataDetails;
			    
				$this->sendRegistranMail($frntRegis->id,$data);
				
			}
			 return true; 
				
		}
		 
	} 
	
	function sendRegistranMail($registeredId,$data)
	{
		
		$to='';
		//to get email and first name for contact person
		 $contactDetails=getRegistrantFields($registeredId);
	
		 if(!empty($contactDetails))
		 {
			$data['firstname']=(!empty($contactDetails[0]))?$contactDetails[0]:'User';
			$to=$contactDetails[1];
		 }
	
		//to send mail 
		$from=$this->config->item('admin_email');
		$subject='User registrant details';
		$this->load->library('email');
		$this->email->from($from);
		$this->email->to($to);
		$this->email->subject($subject);
	   
		$this->email->message($this->load->view('email_view/registrant_details_template',$data,true));
		$this->email->send(); 
		return true;
		
	}
	
	//..........................................................................................................
	/*@description  : This function is used get selected sideevent details
    * @return       : sideevent details
    * @param        : registeredId
    */
	 function getSelectedSideevent($registeredId)
	 {
		 	 
		$this->db->select('tse.side_event_name,tse.start_datetime,tse.end_datetime');
		$this->db->from($this->tablenmfrntsideevent.' as tfse');
	    $this->db->join($this->tablenmsideevent.' as tse','tse.id =tfse.side_event_id','right');
		$this->db->where('tfse.registered_user_id',$registeredId);
		$query = $this->db->get();
		$result= $query->result();
		//echo $this->db->last_query();
		return $result;
		
		
	 }
	 

	 
	//..........................................................................................................
	/*@description  : This function is used get event payment details
    * @return       : paymentId
    * @param        : event and user name
    */
	 function getEventUserPaymentDetails($paymentId)
	 {
		$eventId=eventId();
		
		$this->db->select('te.event_title,tfpd.*');
		$this->db->from($this->tablenmevent.' as te');
		$this->db->from($this->tablenmfrntpaymentdetails.' as tfpd');
	    //$this->db->join($this->tablenmuser.' as tu','te.id =tu.event_id','left');
		$this->db->where('te.id',$eventId);
		$this->db->where('tfpd.id',$paymentId);
		$query = $this->db->get();
		$result= $query->row();
		//echo $this->db->last_query();
	
		return $result;
		
	 }
	 
	 //..........................................................................................................
	/*@description  : This function is used to get breakout
    * @return       : breakout name
    * @param        : registeredId
    */
    
    function getSelectedBreakout($registeredId)
    {
		 
		$this->db->select('tb.breakout_name,tb.date,tbsts.id,tbsts.breakout_stream_id,tbsts.breakout_session_id,tbst.stream_name,tbst.breakout_id,tbst.stream_venue,tbs.start_time,tbs.end_time');
		$this->db->from($this->tablenmfrntbreakouts.' as tfb');
	    $this->db->join($this->tablenmbreakoutstreamsession.' as tbsts','tbsts.id =tfb.stream_session_id','right');
		$this->db->join($this->tablenmbreakoutstream.' as tbst','tbst.id =tbsts.breakout_stream_id','right');
        $this->db->join($this->tablenmbreakoutsession.' as tbs','tbs.id =tbsts.breakout_session_id','right');
        $this->db->join($this->tablenmbreakouts.' as tb','tb.id =tbst.breakout_id','right');

		$this->db->where('tfb.registered_user_id',$registeredId);
		$query = $this->db->get();
		$result= $query->result();
		//$this->db->last_query();
		return $result;
		
	}

	//..........................................................................................................
	/*@description  : This function is used to check event expired or not
    * @return       : true/false
    * @param        : @void
    */
	function isEventExpired()
	{
		$eventId=eventId();
		$eventDetails=$this->common_model->getDataFromTabel('event','last_reg_date',array('id'=>$eventId));
		if(!empty($eventDetails))
		{
			$currentDate=strtotime(date('Y-m-d'));
			$lastRegDate=strtotime($eventDetails[0]->last_reg_date);
			if($currentDate>$lastRegDate)
			{
				 return true;
			}
		}	
		return false; 	
	}
	 	
	// ------------------------------------------------------------------------ 
	
	/*
	 * @descript: This function is used to get registrant id for excceded limit
	 * @return  : registrant id  array event wise
	 * @param   : @void 
	 */ 
     
       
	
	function getRregistrantLimitExceededArray()
	{
		
		$registrantIdArray=array();
		$zeroRegistrantsIdArray=array();
		$frntZeroLimitRegistrant='';
		$zeroRegistrantsLimit='';
		$registrantsLimit='0';
		$eventLimit='0';
		//createArray to take registrant id for limit overed.
		$regisLimitArray=array();
		
		$eventId=eventId();		
		//to get event limit
	    $eventDetails=$this->common_model->getDataFromTabel('event','number_of_rego',array('id'=>$eventId));
		if(!empty($eventDetails))
		{
			$eventLimit=$eventDetails[0]->number_of_rego;
		}
		//to get registrant by registrant id
		$registrantDetails=$this->common_model->getDataFromTabel('event_registrants','id,registrants_limit',array('event_id'=>$eventId,'registrants_type'=>'0'));
				
		if(!empty($registrantDetails))
		{
			foreach($registrantDetails as $registrant)
			{
				$registrantIdArray[]=$registrant->id;
				$zeroLimitRegistrants[$registrant->id]=$registrant->registrants_limit;
				if($registrant->registrants_limit!='0'){
				   $registrantsLimit=$registrantsLimit+$registrant->registrants_limit;
			    }
			    else
			    {
					$zeroRegistrantsIdArray[]=$registrant->id;
				}
			}
			$zeroRegistrantsLimit=$eventLimit-$registrantsLimit;
		    if(!empty($zeroRegistrantsIdArray))
		    {
				//to get all selected registrants whic is consider zero limit
				$frntZeroLimitRegistrant=$this->common_model->getDataFromTabelWhereWhereIn('frnt_registrant','registrant_id',array('payment_status'=>'1'),'registrant_id',$zeroRegistrantsIdArray);
			}
			//to get  paid frontend registrants
			$frntRegistrants=$this->common_model->getDataFromTabelWhereWhereIn('frnt_registrant','registrant_id',array('payment_status'=>'1'),'registrant_id',$registrantIdArray);
	        
	        if(!empty($frntRegistrants))
	        {    
				$count='0';
				foreach($registrantDetails as $registrant)
				{
					if(!empty($zeroRegistrantsIdArray) && in_array($registrant->id,$zeroRegistrantsIdArray))
					{
						if(count($frntZeroLimitRegistrant)==$zeroRegistrantsLimit)
						{
							$regisLimitArray[]=$registrant->id;
						}
					}
					else
					{
						foreach($frntRegistrants as $front)
						{
							$count+=1;
						}
						//to checl limit
						if($count!='0' && $registrant->registrants_limit!=NULL)
						{
							//registrant limit check here 
							if($registrant->registrants_limit==$count)
							{
								$regisLimitArray[]=$frntRegistrants[0]->registrant_id;
							}
						}
					}
				}//end of enner loop
			}
		}
		   return $regisLimitArray;
	} 
	
	 // ------------------------------------------------------------------------ 
	
	/*
	 * @descript: This function is used to calculte zero limit registrants
	 * @return  : array of registrant for excced limit
	 * @param   : zeroLimitRegistrants array for registrants id
	 */ 
	
	function getCalculatedLimitOFZeroRegistrant($zeroLimitRegistrants)
	{
		if(!empty($zeroLimitRegistrants))
	    {
			foreach($zeroLimitRegistrants as $zeroLimit)
			{
				   
			}
		}
	}
	
	 // ------------------------------------------------------------------------ 
	
	/*
	 * @descript: This function is given day registrant id as key and value as number count
	 * @return  : day_registrant_array
	 * @param   : registrantId
	 */ 

	function getDaywiseRegistrantPaidId()
	{
		 //create day id array
		 $dayIdArray=array();
		 $eventId=eventId();	
		 $frntRegistrants=$this->common_model->getDataFromTabel('frnt_registrant','registrant_day_wise_id',array('is_day_wise'=>'1','payment_status'=>'1','event_id'=>$eventId,'registrants_type'=>'0'));
	    if(!empty($frntRegistrants)) 
	    {
			foreach($frntRegistrants as $frnRegis)
			{
				$daysid= explode(',',$frnRegis->registrant_day_wise_id);
			 
				$days=$daysid[0];
				if($days!='')
				{
					
					foreach($daysid as $day)
					{
						$dayIdArray[]=$day;
					}
				}
			}
			 
			 
		}
		return 	array_count_values($dayIdArray);
	}
	
	/*
	 * @descript: This email function
	 * @return  : true
	 * @param   : @from, to
	 */ 
	
	function sendMail($from,$to,$subject='',$emailData='')
	{
	
		$this->load->library('email');
		$this->email->from($from);
		$this->email->to($to);
		$this->email->subject($subject);
	
		$this->email->message($this->load->view('email_view/register_email_template',$emailData,true));
		$this->email->send(); 

		return true;
	}
	
}


?>
