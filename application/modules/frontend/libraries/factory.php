<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * @description: This controller is used to manage login, registration
 * @create date: 21-Nov-2013
 * @auther: Rajendra Patidar
 * @email: rajendrapatidar@cdnsol.com
 * 
 */

class Factory{
	
	private $ci = NULL;
	
	function __construct($ci){
		//create instance object
		$this->ci = $ci;
	}
//..............................................................................................................
    
    /* 
     * @access : public
     * @description: This function call by default and load template
     * @return: void
     *
     */
	public function index()
		{ 
		//to get eventId
		$eventId = decode($this->ci->uri->segment(3));
		//set event id in session for global use
		if(!empty($eventId))
			{
			$this->ci->session->set_userdata('eventId',$eventId);
			}
		//to send event id by array
		$data['event_id']=$eventId;
		//to show popup baground color
		$data['popupHeaderBg'] ='bg_00aec5';  //set popup bg color
	    // to get customize form details for hreader section
		$where=array('event_id'=>$eventId);
		$data['customize_form']=$this->ci->common_model->getDataFromTabel('customize_forms','*',$where);
		$data['eventDetails']=$this->ci->common_model->getDataFromTabel('event','*',array('id'=>$eventId));
		$this->ci->default_template->load('default_template','common/index',$data,true);
	  }
//..............................................................................................................

	/*
	 * @access : public
     * @description : This function is used for standard registration 
     *                to create personal details form, registrant type form.
     * @param       : eventId.                
     * @return      : Dynamic fields for personal details form, Data for registrant type form
     *                by ajax process.
     */
    function standardregister()
    {  
	    
	    
		$post=$this->ci->input->post();
		
		if($post && isset($post['registeredId']))
		{
			 $registeredId = $this->ci->input->post('registeredId');
			 // to create custom form fileds by registeredId  
			 echo $this->ci->frontend_model->getStandardRegistrationDetails($registeredId);	 
		}	
	   else
		{
			 $user_id=frntUserLogin('frantendevent'); 
			 $eventId = decode($this->ci->uri->segment(3));
			 //to get eventId when user first time registered
			 if(!empty($eventId))
			{
			$this->ci->session->set_userdata('eventId',$eventId);
			}
			//to send event id by array
			$data['event_id']=$eventId;
		     
			if($user_id)
			{
				//to get eventId
		        $eventId =eventId();
		      
			     // get user details for logged in user
				 $data['userDetails']=$this->ci->common_model->getDataFromTabel('user','*',array('id'=>$user_id));
				 
				 
				 if(!empty($data['userDetails'])){
					$data['userDetails'] = $data['userDetails'][0];
				}
				 // to get registrant type for this event
				 $whereEventId=array('event_id'=>$eventId,'registrants_type'=>'0'); 
				 $data['registrantDetails']=$this->ci->common_model->getDataFromTabel('event_registrants','*',$whereEventId,'','registrants_order','ASC');
			 
				 $data['regisLimitExceed']=$this->ci->frontend_model->getRregistrantLimitExceededArray();
				
				 //to get event directory
				 $eventDietarys=$this->ci->common_model->getDataFromTabel('event_dietary','*',array('event_id'=>$eventId));
				 $data['eventDietarys']=$eventDietarys;
				 
				 //get paid day id for event
				 $data['paid_day_regisId']=$this->ci->frontend_model->getDaywiseRegistrantPaidId();
			    
				 //calculate user amount for all registrant
				  $data['user_regis_amt']=$this->ci->frontend_model->getUserAllRegistrantAmt();
				 
				 $where=array('event_id'=>$eventId);
		         $data['termConditionDetails']=$this->ci->common_model->getDataFromTabel('event_term_conditions','*',$where);
		    
				 //get breakout	 
		         $data['breakoutDetails']=$this->ci->common_model->getDataFromTabel('breakouts','*',array('event_id'=>$eventId));
			}
			 //to check valid event id
			if($eventId=='')
			{
				 $msgArray = array('msg'=>lang('emailExistsMsg'),'is_success'=>'false');
			     set_global_messages($msgArray);	
				 redirect(base_url('frontend/index/'));
			}
			 // to get event details
			 $whereid=array('id'=>$eventId);
			 $eventDetails=$this->ci->common_model->getDataFromTabel('event','*',$whereid);
			if(!empty($eventDetails))
			{
				 $isArroved=$eventDetails[0]->is_approved;
				if($isArroved=='0')
				{
					 $msgArray = array('msg'=>lang('eventNotPublishMsg'),'is_success'=>'false');
			         set_global_messages($msgArray);	
				     redirect(base_url('frontend/index/'.encode($eventId)));
				}
			}
			 $data['eventDetails']=$eventDetails;
			 
		     // to get customize form details for hreader section
			 $where=array('event_id'=>$eventId);
			 $data['customize_form']=$this->ci->common_model->getDataFromTabel('customize_forms','*',$where);
			 
			 
		     //to send event id by array
			 $data['event_id']=$eventId;
			 	
			 $data['is_event_expired']=$this->ci->frontend_model->isEventExpired();  
			 //to show popup baground color
			 $data['popupHeaderBg'] ='bg_00aec5';  //set popup bg color
			 // to load template with data
			 
			 $this->ci->default_template->load('default_template','registrant_details',$data,TRUE); 
		}

	}
	
//........................................................................................................
      
	/*
	 * @access : public
     * @description : This function is used to create side event form
     * @param       : registrantId, regisDayId(registrant day wise Id)
     * @return      : ajax html sideevent data
     *
     */

	function createsideeventform()
	{
		//to check registrantId is post
		$post=$this->ci->input->post();
		if($post && isset($post['registrantId']))
		{
			$registrantId=$this->ci->input->post('registrantId');	
			//to get sideevent details 
			echo $this->ci->frontend_model->getSideeventDetails($registrantId);
		}		
	}

//..........................................................................................................

	 /*
	 * @access : public
	 * @description: This function is used to create breakout form
	 * @return     : breakout html content
	 * @param      :
	 */
	 
    function createbreakoutform()
    {
	    $breakoutContent='';
	    $day='1';
	    $time='Time';

	    $user_id=frntUserLogin('frantendevent');
	  
		//get login user eventId
		 $eventId=eventId();
		
		$selectRegiId='0';

         //to check data is post
        $post=$this->ci->input->post();
		if($post && isset($post['registered_id']))
		{
		     $selectRegiId=$this->ci->input->post('registered_id');
		     //get selcted breakout for registered user by user id 
	         $frntBreakoutDetails=$this->ci->common_model->getDataFromTabel('frnt_breakouts','stream_session_id',array('registered_user_id'=>$selectRegiId));
             $data['frntBreakoutDetails']=$frntBreakoutDetails;
		}
		
	    //get breakout	 
		$breakoutDetails=$this->ci->common_model->getDataFromTabel('breakouts','*',array('event_id'=>$eventId));
		
		if($breakoutDetails)
		{
			 $prevDate='';
			foreach($breakoutDetails as $breakout)
			{
				 $sessionContent='';
				 //to get breakout stream on the basis of breakout Id
				 $streams=$this->ci->common_model->getDataFromTabel('breakout_stream','*',array('breakout_id'=>$breakout->id));
				
				 //to get breakout session on the basis of breakout Id
				 $sessions=$this->ci->common_model->getDataFromTabel('breakout_session','*',array('breakout_id'=>$breakout->id));
				 $data['streams']=$streams;
				 $data['sessions']=$sessions;
				 $data['prevDate']=$prevDate;
				 $data['breakout_name']=$breakout->breakout_name;
				 $data['breakoutDate']=$breakout->date;
				 $data['select_regisId']=$selectRegiId;
				
				 //to load breakout html content
				 $breakoutContent=$breakoutContent.$this->ci->load->view('ajax_breakout_view',$data,true);  	 
			}
				
		}
	      echo $breakoutContent;
    }   
//..........................................................................................................

     /*
     * @access : public 
     * @description: This function is used to save and update 
     * if user login then user details all updated.
     * @param: 
     * @return:void
     */

	function savestandardregister()
	{
	    $amt=0;
	    $userdata=0;
	    $regi_user_id='';
	  
        // to get user id if user login and take user event id 
	    $user_id=frntUserLogin('frantendevent');
		// to get event_id of login user
	    $event_id=eventId();	 
	    
	     //to check from fields validation
	     $this->ci->checkFormFieldsValidation();
	   
		// to check post data
		 $post=$this->ci->input->post();
		if($post && isset($post['registrant_id']) && $post['registrant_id']!='')
		{
			  
			 $registrantId=$this->ci->input->post('registrant_id');
			 $regisDayId=$this->ci->input->post('daywise_id');
			  
			
			 //save  registrant in  table
			 $regi_user_id=$this->ci->frontend_model->saveSelectRegistrant($registrantId,$regisDayId);
			 
				
			 // to save personal details form fields value
			 $userdata=$this->ci->frontend_model->savePersonalDetailsFormValue($regi_user_id,'save');
			
			/* to save selected side event and guest */
			if(isset($post['select_side_event']) && $post['select_side_event']!='')
			{
			     $sideeventRegisId=$this->ci->input->post('select_side_event');
			     $userdata=$this->ci->frontend_model->saveFrntSideevents($regi_user_id,$sideeventRegisId);  
			}
			  
			/* code to save breakout */
			 $breakoutArray= explode(',',$this->ci->input->post('stream_session_id'));
			 $breakouts=$breakoutArray[0];
			 $breakoutData['user_id']=$user_id;
			 $breakoutData['registered_user_id']=$regi_user_id;
			 
			if(count($breakouts)>0)
			{
				foreach($breakoutArray as $breakout)
				{
				  $breakoutData['stream_session_id']=$breakout;
				  
				  // to add slected breakout 
				  $userdata = $this->ci->common_model->addDataIntoTabel('frnt_breakouts', $breakoutData);
				}
			}
		}
		 
			if($post && isset($post['pay_amount']))
			{
				  $amt=$this->ci->input->post('pay_amount');
			}
			
			if($post && isset($post['payment_by']) && $post['payment_by']!='')
			{
			 
				$payment_by=$this->ci->input->post('payment_by');
				if($payment_by=='cheque' || $payment_by=='cash')
				{
				    
					$userdata=$this->ci->frontend_model->saveChequeCashPayment(); 
					
				}
				  
				if($payment_by=='credit_card' && $amt>0)
				{
				
					$this->registrantPayments();
					return true;
				}
			}
				 
			$msg=lang('registrationSuccessMsg');
			$msgArray = array('msg'=>$msg,'is_success'=>'true');
			set_global_messages($msgArray);	
			echo json_encode(array('msg' =>'', 'status' =>'1'));
			return true;
		
	}
//..........................................................................................................

	/*
	 * @access : public
     * @description: This function is used to update all standard register details
     * @param: $registered_id for user, daywise_id(day wise selected registrant), registrant_id(registrant type id)       
     * @return : success message or failed message
     */

    function updatestandardregister()
	{
		
	    $userdata=0;
	    $regi_user_id='';
	    $registered_id='';
	    
	    $user_id=frntUserLogin('frantendevent');
	   
		// to check post data
		 $post=$this->ci->input->post();
		if($post && isset($post['daywise_id']) && isset($post['registrant_id']))
		{
			 $registrantId=$this->ci->input->post('registrant_id');
			 $regisDayId=$this->ci->input->post('daywise_id');
			 $registered_id=$this->ci->input->post('registered_id');
			 
			 //to check validation of fields
	         $this->ci->frontend_model->checkValidateFormFileds($registrantId);
            
          
			//update day registrant
			$this->ci->frontend_model->saveSelectRegistrant($registrantId,$regisDayId,$registered_id);
		} 
		    
		    // to update personal details form fields value
		    $this->ci->frontend_model->savePersonalDetailsFormValue($regi_user_id,'',$registered_id);
			
			// update registrant type
			if($post && isset($post['select_side_event']))
			{
				 //to save selected side event and guest 
				  $sideeventRegisId=$this->input->post('select_side_event');
			      
			      $this->ci->frontend_model->updateSideevents($sideeventRegisId,$registered_id);  
			        
			}
		    
			// update breakput
			if($post && isset($post['stream_session_id']))
			{
				 //to save selected side event and guest 
				 $stream_session_id=$this->ci->input->post('stream_session_id');
				
				 $userdata=$this->ci->frontend_model->updateBreakouts($stream_session_id,$registered_id);  
			}
			 
			    $msg=lang('updateMsg');
		
				$msgArray = array('msg'=>$msg,'is_success'=>'true');
			    set_global_messages($msgArray);	
			    echo json_encode(array('msg' => '', 'status' => '1'));
			    die;             
	}    
//..........................................................................................................

	/*
	 * @access : public
     * @description: This function is used to get registrant details
     * @return: registrant html content for payment page
     * @param:   registrantId
     */

	 function registrantpayment()
	 {
		
		//to check post data
		
		$post=$this->ci->input->post();
		if($post && isset($post['registrantId']))
		{
			 $registrantContent='';
			 $user_id=frntUserLogin('frantendevent');
			 
			
			 // function to get user selected registrant from nm_frnt_registrants table
			$regiDetails=$this->ci->common_model->getDataFromTabel('frnt_registrant','id,payment_id',array('user_id'=>$user_id));
			$count=1;
			if(!empty($regiDetails))
			{
				$count=count($regiDetails)+1;
			 
			}  
			 //
			 $total_amt=$this->ci->input->post('total_amt');
			 $registrantId=$this->ci->input->post('registrantId');
			 
			 
			 // function to get event registrant from nm_event_registrants table
			 $registrantDetails=$this->ci->common_model->getDataFromTabel('event_registrants','id,registrant_name',array('id'=>$registrantId));

			 $data['registeredId']= $this->ci->input->post('registered_id');
			 $data['regis_count']=$count;
			 $data['totalamt']=$total_amt;
			 $data['registrantDetails']=$registrantDetails;
			 //load seleced registrant on payment page
			 $this->ci->load->view('ajax_registrant_payment',$data);
		}
	 }     
//..........................................................................................................
 
    
	/*
	 * @access : public
     * @description: This function is used to get sidevent details
     * @return: sideevent html content for payment page
     *@param:   regis_sideeventId (registrant side event id)
     */
	 function sideeventpayment()
	 {
		// to check sidevent Id is post 
		$post=$this->ci->input->post();
		if($post && isset($post['select_sideevent']))
		{
			
			 $sideeventContent='';
			 $totalPrice='';
			 $sideeventId='';
			
			 $guestsPrice='0';
			 $guests='0';
			 $guests=$this->ci->input->post('guests');
			 $eventTotalPrice=$this->ci->input->post('event_price');
			 $registrant_id=$this->ci->input->post('registrantId');
			 $regis_sideeventId=$this->ci->input->post('select_sideevent');
			 $guests=$this->ci->input->post('guests');
			 
			 $eventPrice=$eventTotalPrice;
			
				 
			 
			 // get side event according to registrant type
             $sideevent_id=$this->ci->common_model->getDataFromTabel('side_event_registrant','side_event_id',array('id'=>$regis_sideeventId));
             if($sideevent_id)
            {
				  $sideeventId=$sideevent_id[0]->side_event_id;
			}
			if($sideeventId>0)
			{
			       // to get sidevent by registrant and sidevent id
				   $sideeventDetails=$this->ci->common_model->getDataFromTabel('side_event','*',array('id'=>$sideeventId));
				
				   //get sideevent by sideevent id for paticular registrant 
				   $where=array('id'=>$regis_sideeventId);
				   
				   $sideeventRegistrant=$this->ci->common_model->getDataFromTabel('side_event_registrant','*',$where);
			     
				   if($sideeventDetails)
				   {
				   	   if($sideeventRegistrant)
					    {
			                // to show data formate in the form DAY 1 - 1st February 2014
						    $date=date_create($sideeventDetails[0]->start_datetime);
						    $totalPrice=$sideeventRegistrant[0]->total_price;
						    //check for complementry guest
						    if($sideeventRegistrant[0]->complementry==1)
						    {
								$totalPrice='0';
							}
						     
							if($sideeventDetails[0]->allow_earlybird>0 && strtotime($sideeventDetails[0]->earlybird_date) >= strtotime(date('Y-m-d')))
							{
								 $totalPrice=$sideeventRegistrant[0]->early_bird_price;
							}
							
							if($guests>0)
							{
								 $guestsPrice=$sideeventRegistrant[0]->total_price_for_additional_guest*$guests;
							} 
							
							     
								 $data['sideevent_registrantId']=$sideeventRegistrant[0]->id;
								 $data['sideeventDetails']=$sideeventDetails;
								 $data['total_guests']=$guests;
								 $data['guests_price']=$guestsPrice;
								 $data['selectdate']=$date;
								 $data['total_price']=$totalPrice;
								 // to get side event html content for payment page
								 $sideeventContent=$sideeventContent.$this->ci->load->view('ajax_sideevent_payment',$data);	
						}
				}

				 echo $sideeventContent;
			}
		}
	 }	 
//..........................................................................................................

	/*
	 * @access : public
     * @description: This function is used to show breakout for payment page
     * @return: html content of breakout
     * @param: breakout_stream_session_id
     */
	function breakoutpayment()
	{
        // to check breakoutAboutId is post 
		 $post=$this->ci->input->post();
		if($post && isset($post['breakoutAboutId']))
		{
		
		     $streamContent='';
		     $breakoutAboutId=$this->ci->input->post('breakoutAboutId');
		     
		     // get data from breakout_stream_session table
		     $breakoutAbout=$this->ci->common_model->getDataFromTabel('breakout_stream_session','*',array('id'=>$breakoutAboutId));
		
		    if($breakoutAbout)
		    {
			   foreach($breakoutAbout as $about)
				{  
					//get breakout stream by stream id
					$streamDetails=$this->ci->common_model->getDataFromTabel('breakout_stream','*',array('id'=>$about->breakout_stream_id));
					
					//get breakout session by session id
					$sessionDetails=$this->ci->common_model->getDataFromTabel('breakout_session','*',array('id'=>$about->breakout_session_id));

					if($streamDetails)
					{
						if($sessionDetails)
						{
							//get breakouts by id
							$breakoutDetails=$this->ci->common_model->getDataFromTabel('breakouts','*',array('id'=>$streamDetails[0]->breakout_id));

							if($breakoutDetails)
							{
		
								 $date = date_create($breakoutDetails[0]->date);
								
								$data['breakout_aboutId']=$breakoutAboutId;  
								$data['breakout_date']=$date; 
								$data['streamDetails']=$streamDetails; 
								$data['sessionDetails']=$sessionDetails; 
								//load breakout on payment page
								echo $streamContent=$streamContent.$this->ci->load->view('ajax_breakout_payment',$data);
							}
						}
					}
				} //end of foreach loop			  
			}
	    }
	}   
//..........................................................................................................

	 /* 
	  * @access : public
	  * @description  : This function is used to save conatct person detail
      * @return       :  @void
      *
      */     
    function savecontactperson()
    {
         //to check post of the form data 
         $post=$this->ci->input->post(); 
		if($post && isset($post['contact_first_name']))
		{
			 $eventId=$this->ci->input->post('regis_event_id');
			 $user_id=frntUserLogin('frantendevent');
			  
			 //check contact form fields validation 
			 $this->ci->frontend_model->checkContactFormFields();  
			 
			 // to get from data in array
			 $userDetail['username'] =$this->ci->input->post('contact_email');
			 $userDetail['user_title'] =$this->ci->input->post('contact_user_title');
			 $userDetail['firstname'] = $this->ci->input->post('contact_first_name');
			 $userDetail['lastname'] = $this->ci->input->post('contact_surname');
			 $userDetail['email'] =$this->ci->input->post('contact_email');
			 $userDetail['phone'] =$this->ci->input->post('contactphone');
			 $userDetail['mobile'] =$this->ci->input->post('contact_mobile');
			 $userDetail['company_name'] =$this->ci->input->post('contact_cmp_name');
			 $userDetail['position'] =$this->ci->input->post('contact_position');
			  
			if(strlen($this->ci->input->post('contact_password'))>0)
			{
			     $userDetail['password'] = md5($this->ci->input->post('contact_password'));
			}
			  
			// to check user login bu id then update form details
			
			if($user_id)
			{
				$whereUserId=array('id'=>$user_id);
				$this->ci->common_model->updateDataFromTabel('user', $userDetail, $whereUserId);
				
				 echo json_encode(array('msg' => lang('updateMsg'), 'is_success'=>'1'));
				 die();
			}
			else
			{
				 $userDetail['event_id']=$eventId;
				 $userDetail['user_type_id'] ='2';
				
				//to add data in user table and get user id
				$user_id=$this->ci->common_model->addDataIntoTabel('user', $userDetail);
				
				//send email to registered user for email verification
				$from=$this->ci->config->item('admin_email');
				$to=$this->ci->input->post('contact_email');
				$subject='Email varification';
				
				$siteurl=base_url().'frontend/emailverification/'.encode($user_id);
				
				$emailData = array(
				'name'=>$this->ci->input->post('contact_first_name'),
				'email_id'=>$to,
				'password'=>$this->ci->input->post('contact_password'),
				'activation_url'=>$siteurl,
				); 
				
				//function to send mail
				$this->ci->frontend_model->sendMail($from, $to, $subject,$emailData);
				
				$msg=lang('addSuccessMsg');
				$msgArray = array('msg'=>$msg,'is_success'=>'true');
			    set_global_messages($msgArray);
				echo json_encode(array('msg' => '', 'is_success'=>'true'));
				die();
			}
				 echo json_encode(array('msg' => lang('faildTryAgainMsg'), 'is_success'=>'false'));
				 die();
	    }
    }
//..........................................................................................................

	/*
	* @access : public 
	* @description: This function is used to  login for general user
    * @return:message
    */
    function userLogin()
    {
	  $usertype = $this->ci->config->item('registrant_user_type');
		
		$validuser = FALSE;
		 $this->ci->frnt_auth->login($usertype);
    }    
//..........................................................................................................

    /*
    * @access : public 
    * @description: This function is used for create registrant payment page
    * @return     : Html content for payment form
    * $param      :
    */
	function createpaymentform()
	{
	  //to get payment details	
	   echo $this->ci->frontend_model->createPaymentForm();		
	}    
//..........................................................................................................

	 /*
	 * @access : public 
	 * @description: This function is used to create edit side event form
	 * @return:      HTML content of sideevent form for login user
	 */
	function editsideeventform()
	{
		
		//to get edit sideevent form content
		//echo $this->ci->frontend_model->editSideeventForm();
	
	}
//..............................................................................................................

	/*
	* @access : public 
	* @description: This function is used to delete guest
	* @return: guest id
	*
	*/
	function removeguest()
	{
         $post=$this->ci->input->post();
	    if($post && isset($post['guestId']))
	    {
			
			 $frntSideEventId='';
			
			 $whereId=array('id'=>$this->ci->input->post('guestId'));
			 
			 $guests=$this->common_model->getDataFromTabel('frnt_side_event_guest','*',$whereId);
			 if($guests)
			{
				 $frntSideEventId=$guests[0]->frnt_side_event_id;
				
			}

			 $totalguestGuest=$this->ci->common_model->getDataFromTabel('frnt_side_event_guest','*',array('frnt_side_event_id'=>$frntSideEventId));
			 // to count remaining guest
			
			 $remainingGuest=count($totalguestGuest)-1;
			   
	     	 $status=$this->common_model->deleteRow('frnt_side_event_guest',$whereId);
             
            if($status)
			{
				 $remainGuest['total_calculated_amount']=$this->ci->input->post('remainamt');
				 $remainGuest['additional_guest']=$remainingGuest;
				 
				 //to insert remaining total guest in table
				 $whereId=array('id'=>$frntSideEventId);
				 $this->ci->common_model->updateDataFromTabel('frnt_side_event',$remainGuest,$whereId);
			}
			 echo $status;
			 die;
	    }
	}    
//..........................................................................................................

	/*
	* @access : public 
	* @description: This function is used to show editable content of registrant
	* @return: registrant form conte
	* @parram: @void
	*/
	function editregistrant()
	{
		
		$regiId=0;
		$daywiseId=array();
		$registeredId = decode($this->ci->uri->segment(3));
		$user_id=frntUserLogin('frantendevent');
         
		//to get login user eventId
		$eventId=eventId();
		//to redirect
		if($user_id=='')
        {
			redirect(base_url('frontend/index/'));
		}
		//to get user slected registrant by registrant Id
		$registeredDetails=$this->ci->common_model->getDataFromTabel('frnt_registrant','registrant_id,registrant_day_wise_id',array('id'=>$registeredId));
		if($registeredDetails)
		{
				$regiId=$registeredDetails[0]->registrant_id;
				$daysid= explode(',',$registeredDetails[0]->registrant_day_wise_id);
				$days=$daysid[0];
				   if($days!='')
				    {
						foreach($daysid as $day)
						{
							$daywiseId[]=$day;
						}
				    }
		}
      
		$where=array('id'=>$eventId);
		$data['selectRegiDayWise']=$daywiseId;
		$data['registered_id']=$registeredId;
		
		
		//to get event details by event Id
		$data['eventDetails']=$this->ci->common_model->getDataFromTabel('event','*',$where);
		
		//to get registrant details by event Id
		$data['registrantDetails']=$this->ci->common_model->getDataFromTabel('event_registrants','*',array('event_id'=>$eventId,'registrants_type'=>'0'),'','registrants_order','ASC');
	   
		 //to get user details by user Id
		 $data['userDetails']=$this->ci->common_model->getDataFromTabel('user','*',array('id'=>$user_id));
		  if(!empty($data['userDetails'])){
			$data['userDetails'] = $data['userDetails'][0];
		  }
		  
		 //send event Id 
		$data['event_id']=$eventId;
		
	    //send registrant Id on registrant type page
		$data['registrantId']=$regiId;
		
		 //to show popup baground color
	     $data['popupHeaderBg'] ='bg_00aec5';  //set popup bg color
			 
		 //to get event directory
		 $eventDietarys=$this->ci->common_model->getDataFromTabel('event_dietary','*',array('event_id'=>$eventId));
		 $data['eventDietarys']=$eventDietarys;
	   
		 // to get customize form details for hreader section
		 $where=array('event_id'=>$eventId);
		 $data['customize_form']=$this->ci->common_model->getDataFromTabel('customize_forms','*',$where);
		
		 //to check registrant limit 
	     $data['regisLimitExceed']=$this->ci->frontend_model->getRregistrantLimitExceededArray();
				
		//get paid day id for event
		$data['paid_day_regisId']=$this->ci->frontend_model->getDaywiseRegistrantPaidId();
			    
		//to get edit sideevent form content
		$data['sideeventContent']=$this->ci->frontend_model->editSideeventForm($registeredId);
		 
		//get breakout	 
	    $data['breakoutDetails']=$this->ci->common_model->getDataFromTabel('breakouts','*',array('event_id'=>$eventId));
		$this->ci->default_template->load('default_template','registrant_details',$data,TRUE);
	}
//..........................................................................................................
	
	/*
	 * @access : public
	 * @description: This function is used to varificatrion of email
	 * @return     : void
	 *
	 */
	function emailverification()
	{
		 $userId = decode($this->ci->uri->segment(3));
		
		 //get user details on the basis of user id
		 $whereArray=array('id'=>$userId);
		 $userdetails = $this->ci->common_model->getDataFromTabel('user','event_id,email_verify',$whereArray);
  
		if($userdetails)
		{
			//to check account already verified
			if($userdetails[0]->email_verify==1)
			{
				$msg=lang('alreadyemailVerifyMsg');
				$msgArray = array('msg'=>$msg,'is_success'=>'true');
				set_global_messages($msgArray);
				redirect(base_url().'frontend/index/'.encode($userdetails[0]->event_id));
			}
			
			//to update status for verified email user
			$data['email_verify']='1'; 
			$this->ci->common_model->updateDataFromTabel('user',$data,array('id'=>$userId));
			
			//to set message 
			$msg=lang('emailVerifyMsg');
			$msgArray = array('msg'=>$msg,'is_success'=>'true');
			set_global_messages($msgArray);
			redirect(base_url().'frontend/index/'.encode($userdetails[0]->event_id));
		}
		   redirect(base_url('frontend/index/'));
	}


//..........................................................................................................

	/*
	 * @access : public
     * @description: This function is used to re by Paypal
     * @return: void
     * @para:   paypal form details
     */
    function registrantPayments()
	{ 
		$user_id=frntUserLogin('frantendevent');
         //to get login user eventId
		$eventId=eventId();
        $userDataArray=array(); 
        $post=$this->ci->input->post();
		if(isset($post['card_type']) && isset($post['card_number']) && isset($post['ccv']))
		{     
			$userDataArray['amt']=$this->ci->input->post('pay_amount');
			$userDataArray['card_type']=$this->ci->input->post('card_type');
			$userDataArray['card_number']=$this->ci->input->post('card_number');
		    $userDataArray['exp_date']=$this->ci->input->post('expire_month_year');
			$userDataArray['ccv']=$this->ci->input->post('ccv');
		}
		$pamentOption = $this->ci->common_model->getDataFromTabel('payment_gateway','merchant_id',array('event_id'=>$eventId));
		
		if(!empty($pamentOption))
		{
			 $userDataArray['merchant_id']=$pamentOption[0]->merchant_id;
			 //to payment by paypal from payment pro library
						
			$this->ci->payments_pro->do_direct_payment($userDataArray);
	         return true;
			 
		}
		else
		{
		     $msg=lang('merchantIdMsg').'Please try again.';
			 $msgArray = array('msg'=>$msg,'is_success'=>'false');
			 set_global_messages($msgArray);
			 echo json_encode(array('msg' =>'', 'status'=>'1'));
			 return true;
		}
	}
	//..........................................................................................................
	
	/*
	 * @access : public
     * @description: This function is used to check validate form fileds
     * @return:      true/false
     * @para  :  registrantId, regis_password
     */
	function checkFormFieldsValidation()
	{
		//to check event limit for registrant 
		$eventLimit=$this->ci->frontend_model->isEventLimitExists();
		
		if(!$eventLimit)
		{
			 $msg=lang('registrationLimitMsg');	
			 echo json_encode(array('msg' => $msg, 'status' => '0'));
			 die;
		} 
		$post=$this->ci->input->post();
		if(isset($post['registrant_id']) && $post['registrant_id']>0)
		{
			$registrantId=$this->ci->input->post('registrant_id');
			  
			 //function to validation of personal form fileds
			 $this->ci->frontend_model->checkValidateFormFileds($registrantId);
		} 
		  return true;
	}
	//..........................................................................................................
	
	/*
	 * @access : public
     * @description: This function is used to check registration password
     * @return:      true/false
     * @para  :  registrantId, regis_password
     */
	function checkRegistrationPass()
	{
		$post=$this->ci->input->post();
		
		if(isset($post) && isset($post['pass_registrant_id']) && isset($post['registrant_password']))
		{ 
			
			 $registrant_id=$this->ci->input->post('pass_registrant_id');
			 $password=$this->ci->input->post('registrant_password');
			 
			 //to get registrant for id and password
			 $regisDetails = $this->ci->common_model->getDataFromTabel('event_registrants','id',array('id'=>$registrant_id,'password'=>$password));
			
			if(!empty($regisDetails))
			{
				//if pass exists
				echo json_encode(array('msg' => lang('permissionGrantedMsg'), 'status' => '1'));
				die;
			}
			  echo json_encode(array('msg' =>lang('enterValidPassMsg'), 'status' => '0'));
			  die;
		}
	}	
// ------------------------------------------------------------------------ 	

	/*
	 * @access : public
	 * @description: This function is used to get state list by master country id
	 * @return master state listing html
	 */
	
	public function masterstatelist(){
		$star='';
		$countryId 	= $this->ci->input->post('countryId');
		$required 	= $this->ci->input->post('required');
		if($required=='required_field')
		{
			$star='*';
		}
		$where	= array('country_id'=>$countryId);
		$stateData 	= $this->ci->common_model->getDataFromTabel('master_state','*',$where,'','state_name','ASC');
		$stateHtml  = '<select name="contact_state" id="contact_state" class="selectpicker  bla bla bli '.$required.'" > 
				        <option value="">Select State'.$star.'</option>';
		
		if(!empty($stateData)){
			foreach($stateData as $statelistdata){
				$stateId 	= $statelistdata->state_id;
				$stateName  = $statelistdata->state_name;
				$stateHtml  .= '<option value="'.$stateId.'">'.$stateName.'</option>';
			}
			
		}
		echo $stateHtml.'</select>'; 
	}
	
// ------------------------------------------------------------------------   	
	
	/*
	 * @access : public
	 * @description: This function is used to get city list by master state id
	 * @return master city listing html
	 */
	 
	public function mastercitylist(){
		//$star='';
		$stateId 	= $this->ci->input->post('stateId');
		//$required 	= $this->input->post('required');
		
		$where	= array('state_id'=>$stateId);
		$cityData 	= $this->ci->common_model->getDataFromTabel('master_city','*',$where,'','city_name','ASC');
		$cityHtml  = '<select name="contact_city" id="contact_city" class="selectpicker  bla bla bli"><option value="">Select City</option>';
		
		if(!empty($cityData)){
			foreach($cityData as $citylistdata){
				$cityId 	= $citylistdata->city_id;
				$cityName  = $citylistdata->city_name;
				$cityHtml  .= '<option value="'.$cityId.'">'.$cityName.'</option>';
			}
		}
		echo $cityHtml.'</select>'; 
	} 
//..........................................................................................................
	
	/*
	 * @access : public
     * @description: This function is used to logout user form website
     * @return: void
     *
     */
    function logout()
    {
		$this->ci->session->unset_userdata('userid');
		$this->ci->session->unset_userdata('username');
		$this->ci->session->unset_userdata('firstname');
		$this->ci->session->unset_userdata('sponsorid');
		//$this->session->sess_destroy();
	    return true;
	}
//..............................................................................................................
    
    /*
	 * @access : public
     * @description: This is testing email function for creating to test.
     * @return: void
     *
     */
	function test()
	{
		$this->ci->load->library('email');
		$this->ci->email->from('myadmin@cdnosl.com');
		$this->ci->email->to('lokendrameena@cdnsol.com');
		$this->ci->email->subject('subject');
		$message = 'test';
		$this->ci->email->message($message);
		$this->ci->email->send(); 
		echo $this->ci->email->print_debugger(); 
	}  
//..............................................................................................................	 
}


