
<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 * This class is used to manage for home controller
 *
 * @package   validation
 * @author    Rejendra Patidar
 * @email     rajendrapatidar@cdnsol.com
 * @since     Version 1.0
 * @filesource
 */
 
 
class Payments{
	 
	private $sandbox = '';
	private $APIUsername = '';
	private $APIPassword = '';
	private $APISignature = '';
	private $APISubject = '';
	private $APIVersion = '';
	
	function __construct(){
		//create instance object
		$this->ci =& get_instance();
	    //$this->ci->load->library(array('form_validation'));
	    
	    // Load helpers
		$this->ci->load->helper('url');
		
		// Load PayPal library
	    $this->ci->config->load('paypal');
	   	
	}
	
	//--------------------------------------------------------------------------------
	
	/* 
	 * @access: public
	 * @descrition: This function is used  for user login
	 * @return void
	 */ 
	
	
	public function do_Adaptive_Payment($userDataArray){
		
			
		// Load PayPal library
		    $eventId=getEventId();
			$config = array(
				'Sandbox' => $this->ci->config->item('Sandbox'), 			// Sandbox / testing mode option.
				'APIUsername' => $this->ci->config->item('APIUsername'), 	// PayPal API username of the API caller
				'APIPassword' => $this->ci->config->item('APIPassword'), 	// PayPal API password of the API caller
				'APISignature' => $this->ci->config->item('APISignature'), 	// PayPal API signature of the API caller
				'APISubject' => '', 									// PayPal API subject (email address of 3rd party user that has granted API permission for your app)
				'APIVersion' => $this->ci->config->item('APIVersion'), 		// API version you'd like to use for your call.  You can set a default version in the class and leave this blank if you want.
				'DeviceID' => $this->ci->config->item('DeviceID'), 
				'ApplicationID' => $this->ci->config->item('ApplicationID'), 
				'DeveloperEmailAccount' => $this->ci->config->item('DeveloperEmailAccount')
			);
			
			$this->ci->load->library('paypal/paypal_adaptive', $config);		
		
			
			$trackingIdGet = encode(time());
			$invoiceIDGet  = rand();
			
			// Prepare request arrays
			$PayRequestFields = array(
									'ActionType' => 'PAY', 								// Required.  Whether the request pays the receiver or whether the request is set up to create a payment request, but not fulfill the payment until the ExecutePayment is called.  Values are:  PAY, CREATE, PAY_PRIMARY
									'CancelURL' => site_url('frontend/standardregister/'.encode($eventId)), 									// Required.  The URL to which the sender's browser is redirected if the sender cancels the approval for the payment after logging in to paypal.com.  1024 char max.
									'CurrencyCode' => 'EUR', 								// Required.  3 character currency code.
									'FeesPayer' => 'SENDER', 									// The payer of the fees.  Values are:  SENDER, PRIMARYRECEIVER, EACHRECEIVER, SECONDARYONLY
									'IPNNotificationURL' => 'http://115.113.182.141/toadsquare_branch/dev/en/membershipcart/test', 						// The URL to which you want all IPN messages for this payment to be sent.  1024 char max.
									'Memo' => '', 										// A note associated with the payment (text, not HTML).  1000 char max
									'Pin' => '', 										// The sener's personal id number, which was specified when the sender signed up for the preapproval
									'PreapprovalKey' => '', 							// The key associated with a preapproval for this payment.  The preapproval is required if this is a preapproved payment.  
									'ReturnURL' => site_url('frontend/Payment_details/'.$trackingIdGet), 									// Required.  The URL to which the sener's browser is redirected after approvaing a payment on paypal.com.  1024 char max.
									'ReverseAllParallelPaymentsOnError' => '', 			// Whether to reverse paralel payments if an error occurs with a payment.  Values are:  TRUE, FALSE
									'SenderEmail' => '', 								// Sender's email address.  127 char max.
									'TrackingID' => $trackingIdGet,
																		// Unique ID that you specify to track the payment.  127 char max.
									);
									
			$ClientDetailsFields = array(
									'CustomerID' => '', 								// Your ID for the sender  127 char max.
									'CustomerType' => '', 								// Your ID of the type of customer.  127 char max.
									'GeoLocation' => '', 								// Sender's geographic location
									'Model' => '', 										// A sub-identification of the application.  127 char max.
									'PartnerName' => ''									// Your organization's name or ID
									);
									
			//$FundingTypes = array("ECHECK", "CREDITCARD", "BALANCE");
			$FundingTypes = array();
			//buyer_1348040918_per@cdnsol.com
			$Receivers = array();
			$Receiver = array(
							'Amount' => $userDataArray['amt'], 											// Required.  Amount to be paid to the receiver.
							'Email' => $userDataArray['merchant_id'],	 												// Receiver's email address. 127 char max.
							'InvoiceID' => $invoiceIDGet, 											// The invoice number for the payment.  127 char max.
							'PaymentType' => 'SERVICE', 										// Transaction type.  Values are:  GOODS, SERVICE, PERSONAL, CASHADVANCE, DIGITALGOODS
							'PaymentSubType' =>'' , 									// The transaction subtype for the payment.
							'Phone' => array('CountryCode' => '', 'PhoneNumber' => '', 'Extension' => ''), // Receiver's phone number.   Numbers only.
			 'Primary' => False												
	// Whether this receiver is the primary receiver.  Values are boolean:  TRUE, FALSE
							);
			array_push($Receivers,$Receiver);
			
			
			
			
		//	print_r($Receivers);die;
			
			$SenderIdentifierFields = array(
											'UseCredentials' => 'TRUE'						// If TRUE, use credentials to identify the sender.  Default is false.
											);
											
			$AccountIdentifierFields = array(
											'Email' => 'test@test.com', 								// Sender's email address.  127 char max.
											'Phone' => array('CountryCode' => '', 'PhoneNumber' => '', 'Extension' => '')								// Sender's phone number.  Numbers only.
											);
											
			$PayPalRequestData = array(
								'PayRequestFields' => $PayRequestFields, 
								'ClientDetailsFields' => $ClientDetailsFields, 
								'FundingTypes' => $FundingTypes, 
								'Receivers' => $Receivers, 
								'SenderIdentifierFields' => $SenderIdentifierFields, 
								'AccountIdentifierFields' => $AccountIdentifierFields
								);	
								
			$PayPalResult = $this->ci->paypal_adaptive->Pay($PayPalRequestData);
		
			if(!$this->ci->paypal_adaptive->APICallSuccessful($PayPalResult['Ack']))
			{
		
				 $errors =$PayPalResult['Errors'];
				 $dataArray=$errors[0];
				 $msg=$dataArray['Message'];
				  
				 $msgArray = array('msg'=>$msg,'is_success'=>'false');
			     set_global_messages($msgArray);
			     redirect(base_url().'frontend/standardregister/'.encode($eventId));
			
			}
			else
			{
						   
				// Successful call.  Load view or whatever you need to do here.
				header('Location: '.$PayPalResult['RedirectURL']);
				exit();
			}
	
		
	}
	
		

}

/* End of file auth.php */
/* Location: ./system/application/modules/home/libraries/auth.php */
