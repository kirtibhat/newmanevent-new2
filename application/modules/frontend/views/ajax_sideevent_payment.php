<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
//--------To show select sideevent on payment page  by ajax process---------//	

    $sideeventRegistrantId = (isset($sideevent_registrantId) && !empty($sideevent_registrantId))?$sideevent_registrantId:'';
    $guests = (isset($total_guests) && !empty($total_guests))?$total_guests:'0';
    $guestsPrice = (isset($guests_price) && !empty($guests_price))?$guests_price:'0'; 
    $date = (isset($selectdate) && !empty($selectdate))?$selectdate:''; 
    $totalPrice = (isset($total_price) && !empty($total_price))?$total_price:'0'; 
    
    // to show all ready selected sideevent on payment page  by ajax process when user login
    $registeredId = (isset($registeredId) && !empty($registeredId))?$registeredId:''; 
    $sideeventAmt = (isset($sideevent_amt) && !empty($sideevent_amt))?$sideevent_amt:'0'; 
   
   
    if(isset($sideeventDetails) && !empty($sideeventDetails)) 
    {
		if(!empty($registeredId))
		{
			
	    ?>	
			 <tr class="editsideevent<?php echo $registeredId.$sideeventRegistrantId;?> editsideevent<?php echo $registeredId; ?>">
			        <td> 
				   <?php echo ucfirst($sideeventDetails[0]->side_event_name).' - '.date_format($date, 'jS F Y'); 
					
					if($sideeventDetails[0]->show_time_on_form)
					{
					  echo  ', '.date('H:s',strtotime($sideeventDetails[0]->start_datetime)).' to '.date('H:s',strtotime($sideeventDetails[0]->end_datetime)); 
					 
					}
					?>
					</td> 
					
					<td>$<?php echo $sideeventAmt; ?></td>
					<td></td>
			 </tr>
 
		     <tr class="editsideevent<?php echo $registeredId.$sideeventRegistrantId;?> editsideevent<?php echo $registeredId; ?>">
		    
					<td><span id="sideeventguest<?php echo $registeredId; ?>"><?php echo $guests.' '.lang('additionalGuestsMsg'); ?></span></td>
					<td>$<span id="sideeventprice<?php echo $registeredId; ?>"><?php if($guestsPrice!=''){ echo $guestsPrice; } else{ echo "0"; }?></span></td>
					<td>&nbsp;</td>
			 </tr>
				   <tr class="editsideevent<?php echo $registeredId.$sideeventRegistrantId;?> editsideevent<?php echo $registeredId; ?>">
				   <td><div class="spacer15 "></div></td>
				   <td></td>
				   <td></td>
			 </tr>
			
         <?php	
           
		}
		else
	    {
			
         ?>	
		 <tr class="sidepayment<?php echo $sideeventRegistrantId; ?> sidepayment">
			   <td> 
				   <?php echo ucfirst($sideeventDetails[0]->side_event_name).' - '.date_format($date, 'jS F Y'); 
					
					if($sideeventDetails[0]->show_time_on_form)
					{
					  echo  ', '.date('H:s',strtotime($sideeventDetails[0]->start_datetime)).' to '.date('H:s',strtotime($sideeventDetails[0]->end_datetime)); 
					}
					?>
			    </td>
			    <td>$<?php echo $totalPrice; ?></td>
			    <td>&nbsp;</td>
		 </tr>
		
		 <tr class="sidepayment<?php echo $sideeventRegistrantId; ?>">
			<td>
				<span id="sideeventguest" class="sideeventguest">  <?php  echo $guests; ?> 
				   <?php echo lang('additionalGuestsMsg'); ?> 
				</span> 
			</td>
			<td>
				 $<span id="sideeventprice" class="sideeventprice"><?php echo $guestsPrice; if($guestsPrice==''){ echo '0'; } ?></span>
			</td>
		      <td>&nbsp;</td>
		 </tr>
		 
		
          <?php
          
    	}
    
    }
   
	?>
