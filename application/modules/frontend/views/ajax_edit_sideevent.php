<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
//--------To show calculate amount on payment page  by ajax process---------//	

    $totalPrice = (isset($total_price) && !empty($total_price))?$total_price:'0';
    $display = (isset($display_class) && !empty($display_class))?$display_class:'';
    $guest = (isset($total_guest) && !empty($total_guest))?$total_guest:'0';
    $maxGuest = (isset($max_guest) && !empty($max_guest))?$max_guest:'0';
    $guestForm = (isset($guest_form) && !empty($guest_form))?$guest_form:'';
    $count = (isset($regi_count) && !empty($regi_count))?$regi_count:'';
    $class = (isset($add_class) && !empty($add_class))?$add_class:'';
    $eventTotalPrice = (isset($event_total_price) && !empty($event_total_price))?$event_total_price:'';
    $sideventRegistrantId = (isset($sidevent_registrant_id) && !empty($sidevent_registrant_id))?$sidevent_registrant_id:'';
    
    $isAdditionalGuest = (isset($is_additional_guest) && !empty($is_additional_guest))?$is_additional_guest:'';
	$guestPrice = (isset($guest_price) && !empty($guest_price))?$guest_price:'0';
	$addiGuestPrice=(isset($addi_guest_price) && !empty($addi_guest_price))?$addi_guest_price:'0';
	
	$maxCompGuest = (isset($max_comp_guest) && !empty($max_comp_guest))?$max_comp_guest:'0';
	$compGuestPrice = (isset($comp_guest_price) && !empty($comp_guest_price))?$comp_guest_price:'0';
	
   
    if(isset($sideeventDetails) && !empty($sideeventDetails))
    {
	
		
     ?>
      
     <div class="registration_table">
		
		<div class="regist_row">
				
			<div class="regist_mmm">
				<div class="tableheading_2"><?php echo lang('dayMsg');  echo $count; ?></div>
			</div>

			 <div class="regist_mmm">
			         <div class="tableheading_2 pt0">

				     <input id="sideeventcheck<?php echo $sideventRegistrantId; ?>" type="checkbox"  value="<?php echo $sideventRegistrantId; ?>" onclick="selectSideEvent('<?php echo $sideventRegistrantId; ?>')" class="sideevent_check sideeventcheck<?php echo $sideventRegistrantId.' '.$class; ?>">
				      <label><?php echo $sideeventDetails[0]->side_event_name; ?></label>

				     <div class="regist_smallfont"><?php echo $sideeventDetails[0]->venue; ?></div>

				     </div>
				     <div class="tableheading_2 pt0">$<?php echo $totalPrice; ?></div>
			</div>
			<?php
			
			  if($isAdditionalGuest)
			  {
				?>
				<div id="inner_wraaper<?php echo $sideventRegistrantId; ?>" <?php echo $display;?> >

					 <div class="regist_mmm">
						<div class="tableheading_2 pt2pb2 margin10">
							
							 <label class="disply_Inl clr_lightblue"><?php echo lang('additionalGuestsMsg'); ?></label>
							 <div class="inputnubcontainer" onclick="additionalGuest('<?php echo $sideventRegistrantId; ?>')">
								 <input id="additional_guest<?php echo $sideventRegistrantId; ?>" name="additional_guest<?php echo $sideventRegistrantId;?>" max="<?php echo $maxGuest; ?>" min="<?php if($guest!=''){ echo $guest; } else{ echo '0';} ?>" value="<?php if($guest!=''){ echo $guest;} else{ echo '0'; } ?>" class="mr15 guestspinner"  readonly >
							 </div>
							 
						</div>
						      <div class="tableheading_2 pt0">$<span id="comp_guest<?php echo $sideventRegistrantId;?>"><?php if($compGuestPrice){ echo $compGuestPrice; } else{ echo $addiGuestPrice; }?></span></div>
							
					</div>
						   <?php echo $guestForm; ?>
						   
						  <div id="guest_fields<?php echo $sideventRegistrantId; ?>"></div>
							 
				    </div>
			<?php } ?>
			      
			       <input type="hidden" name="max_comp_guest" id="max_comp_guest<?php echo $sideventRegistrantId;?>" value="<?php echo $maxCompGuest; ?>">
                   <input type="hidden" name="comp_guest_price" id="comp_guest_price<?php echo $sideventRegistrantId;?>" value="<?php echo $compGuestPrice; ?>">
                   <input type="hidden" name="guest_price" id="guest_price<?php echo $sideventRegistrantId;?>" value="<?php echo $addiGuestPrice; ?>">
             
				   <input type="hidden" name="event_price" id="event_price<?php echo $sideventRegistrantId;?>" value="<?php echo $totalPrice; ?>">
				   <input type="hidden" name="eventTotalPrice<?php echo $sideventRegistrantId; ?>" id="eventTotalPrice<?php echo $sideventRegistrantId; ?>" value="<?php echo $eventTotalPrice; ?>" class="event_total_price" >
		</div>
	 </div>
	 <?php
	
    }
	
