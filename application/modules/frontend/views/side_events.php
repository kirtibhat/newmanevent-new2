<?php

	$user_id=frntUserLogin('frantendevent');
	$eventDietary='';

	$eventId= (isset($event_id) && !empty($event_id))?$event_id:''; 
	$registrant_id= (isset($registrantId) && !empty($registrantId))?$registrantId:''; 
    $registeredId= (isset($registered_id) && !empty($registered_id))?$registered_id:'';  
    
    // to get direactory in js for guest form
    if(isset($eventDietarys) && !empty($eventDietarys))
    {
		foreach($eventDietarys as $dietary)
	    {
		  $eventDietary=$eventDietary.'<option class="selectitem" value="'.$dietary->id.'">'.$dietary->dietary_name.'</option>';
	    }
	}   

?>
 
  <div class="accordion-group step2">
		<div class="accordion-heading">
			<div class="accordion-toggle sideevent" data-toggle="collapse" data-parent="#monogram-acc" href="#SideEvents" >
			<?php echo lang('step4SideEventsHeading'); ?>

			</div>
		</div>
		
	<div class="accordion-body collapse" id="SideEvents">
	    <div class="innerbody">
		   
		    <div class="paragraph">	
		       <?php echo lang('PleaseSelectSideeventMsg'); ?>
			</div>
			
			<div class="registration_table mt10">
				<div class="tableheading"><?php echo lang('sideEventsMsg'); ?></div>
				<div class="tableheading"><?php echo lang('priceIncGSTMsg'); ?></div> 
			</div>
			  
	         <div id="sideeventContent"><?php if(isset($sideeventContent)){ echo $sideeventContent; } ?></div>	       
	         <div class="clearfix"></div> 
	         
	         <div class="registration_table">
					<div class="tableheading text-right"><?php echo lang('frntTotalMsg'); ?></div>
					<div class="tableheading">$<span id="days_total_price" >0</span> </div> 
			 </div>
             <input type="hidden" name="total_amount" id="total_amount" value='0'>
			 <input type="hidden" name="select_side_event" id="select_side_event" value="">
          
         </div> <!-- end of the innerbody div -->
       </div>        
	</div>
 
<script type="text/javascript">
      
 
/**
 * Function to get selected sideevent  details
*/

 var emptyGuest=false;
 var selectId='';
 var eventDietary='<?php echo $eventDietary;?>';

 $(window).on('load', function () {
	var regiid='<?php echo $registeredId; ?>';
	
	getSelectedSideevent(regiid);
		
});


 
</script>	

