<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
//--------To creat side content by ajax process---------//	
?>
<?php

    $sideeventDate = (isset($sideevent_date) && !empty($sideevent_date))?$sideevent_date:'';
    $sideevent_regiId = (isset($sideeventRegisId) && !empty($sideeventRegisId))?$sideeventRegisId:'';
    $sideevent_name = (isset($sideeventName) && !empty($sideeventName))?$sideeventName:'';
	$sideevent_venue = (isset($sideeventVenue) && !empty($sideeventVenue))?$sideeventVenue:'';
   
	$total_price = (isset($totalPrice) && !empty($totalPrice))?$totalPrice:'0';
	$max_guest = (isset($maxGuest) && !empty($maxGuest))?$maxGuest:'0';
	$guestPrice = (isset($guest_price) && !empty($guest_price))?$guest_price:'0';
	
	$maxCompGuest = (isset($max_comp_guest) && !empty($max_comp_guest))?$max_comp_guest:'0';
	$compGuestPrice = (isset($comp_guest_price) && !empty($comp_guest_price))?$comp_guest_price:'0';
	
	$isAdditionalGuest = (isset($is_additional_guest) && !empty($is_additional_guest))?$is_additional_guest:'';
	$readonlySideevent = (isset($readonly) && !empty($readonly))?$readonly:'';
     
?>

<div class="registration_table">
	<div class="regist_row">
			<div class="regist_mmm">
				<div class="tableheading_2 "><b><?php echo 'Date: '?></b> <?php echo $sideeventDate; ?> </div>
		    </div>

		 <div class="regist_mmm">
			<div class="tableheading_2 pt0">

				<input id="sideeventcheck<?php echo $sideevent_regiId; ?>" type="checkbox"  value="<?php echo $sideevent_regiId; ?>" onclick="selectSideEvent('<?php echo $sideevent_regiId; ?>')" class="sideevent_check sideeventcheck<?php echo $sideevent_regiId.' '.$readonlySideevent; ?>">
				<label> <?php echo $sideevent_name;?></label>

				<div class="regist_smallfont"><?php echo $sideevent_venue;?></div>

			</div>
			 <div class="tableheading_2 pt0">$<?php echo $total_price;?></div>
		 </div>
		<?php
		
	    if($isAdditionalGuest)
	    {
		?> 
			<div id="inner_wraaper<?php echo $sideevent_regiId;?>" style="display:none;">

				<div class="regist_mmm">
					<div class="tableheading_2 pt2pb2 margin10">
						<label class="disply_Inl clr_lightblue"><?php  echo lang('additionalGuestsMsg'); ?></label>
						<div class="inputnubcontainer" onclick="additionalGuest('<?php echo $sideevent_regiId;?>')" >

						<input id="additional_guest<?php echo $sideevent_regiId;?>" name="additional_guest<?php echo $sideevent_regiId;?>" max="<?php echo $max_guest; ?>" min="0" value="0" class="mr15 guestspinner" readonly>
                         
						 </div>
					</div>
					 <div class="tableheading_2 pt0">$<span id="comp_guest<?php echo $sideevent_regiId;?>"><?php if($compGuestPrice){ echo $compGuestPrice; } else{ echo $guestPrice; }?></span></div>
				 </div>
					<div id="guest_fields<?php echo $sideevent_regiId;?>"></div>
			</div>
         <?php
		}
         ?> 
			
            
             
             <input type="hidden" name="max_comp_guest" id="max_comp_guest<?php echo $sideevent_regiId;?>" value="<?php echo $maxCompGuest; ?>">
             <input type="hidden" name="comp_guest_price" id="comp_guest_price<?php echo $sideevent_regiId;?>" value="<?php echo $compGuestPrice; ?>">
             <input type="hidden" name="guest_price" id="guest_price<?php echo $sideevent_regiId;?>" value="<?php echo $guestPrice; ?>">
             			
             <input type="hidden" name="event_price" id="event_price<?php echo $sideevent_regiId;?>" value="<?php echo $total_price; ?>">

		   <input type="hidden" name="eventTotalPrice<?php echo $sideevent_regiId;?>" id="eventTotalPrice<?php echo $sideevent_regiId;?>" value="" class="event_total_price">
	</div>
</div>


