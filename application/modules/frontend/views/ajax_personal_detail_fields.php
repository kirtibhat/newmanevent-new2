<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
//--------To creat personal details form fields by ajax process---------//	
?>
<?php

    $fieldId = (isset($field_id) && !empty($field_id))?$field_id:'';
    $fieldName = (isset($field_name) && !empty($field_name))?$field_name:'';
    $fieldValue = (isset($field_value) && !empty($field_value))?$field_value:'';
	$placeholder = (isset($place_holder) && !empty($place_holder))?$place_holder:'';
	$required = (isset($field_required) && !empty($field_required))?$field_required:'';
	$fieldType = (isset($field_type) && !empty($field_type))?$field_type:'';
	$countryName='';
	$stateName='';
	$cityName='';
	
	

	if(isset($defaultArray) && count($defaultArray)>0)
	{
	   $fieldData='';
	   foreach($defaultArray as $key=>$value)
	   {
		  
		    if($fieldType=='selectbox')
		    {
				if($fieldData=='')
				{
				      
					   $fieldData='<option value="">Please Select '.$placeholder.'</option>';
				 
				}

				if($fieldValue!='')
				{
					
					 $fieldData=$fieldData.'<option value="'.strtolower($value).'" selected="selected">'.ucfirst($value).'</option>';
  
				}
				else
				{
					$fieldData=$fieldData.'<option value="'.strtolower($value).'">'.ucfirst($value).'</option>';
				}
		    }

	   }
	   
		 /* check for dropdown */
		if($fieldType=='selectbox')
		{
			?> 
			   <div class="position_R">
				   <div class="pull-left  frontend_select">
						<select name="<?php echo $fieldName; ?>" id="<?php echo $fieldName; ?>" class="selectpicker bla bla bli <?php echo $required; ?>" > 
							<?php echo $fieldData; ?>
						</select>
			        </div>
			              <div class="clearfix"></div>
			        </div>
		   
		    <?php
		}
	} /* end of default if */
	else if($fieldType=='textarea')
	{
        ?>
	       <div class="position_R">
	            <textarea name="<?php echo $fieldName; ?>" id="<?php echo $fieldName; ?>" placeholder="<?php echo $placeholder; ?>" class="<?php echo $required; ?>">
	              <?php echo $fieldValue; ?>
	             </textarea>
	         </div>
	    <?php     
	}  //to add address fields like country city, state ,Postcode 
	else if($fieldName=='address')
	{
		$star='';
		$cpcountry='';
	    $cpstate='';
		if($required=='required_field')
		{
			 $placeholder='Address';
			 $star='*';
		}
		
         $addressArray=json_decode($fieldValue,true);
      
		 $address1=''; 
		 $address2='';
		 $country='';
		 $state='';
		 $city=''; 
		 
		if(!empty($addressArray) && array_key_exists('addressId',$addressArray)) {
			
			 if(array_key_exists('addressName',$addressArray)) {	
				 
			 $addressFields = $addressArray['addressId'];
			 $addressName = $addressArray['addressName'];
				 
				if(!empty($addressFields))
				{
					$address1=$addressFields['address1'];
					$address2=$addressFields['address2'];
					$country=$addressFields['country'];
					$state=$addressFields['state'];
					$city=$addressFields['city'];
					
					$countryName=$addressName['countryName'];
					$stateName=$addressName['stateName'];
					$cityName=$addressName['cityName'];
					
				}
			}
		}
		
		?>
		 <div class="position_R">
		  <input type="<?php echo $fieldType; ?>" name="<?php echo $fieldName; ?>" id="<?php echo $fieldName; ?>" placeholder="<?php echo $placeholder.'1'.$star; ?>" value="<?php echo $address1; ?>" class="<?php echo $required; ?>" >
        </div>
        
        <div class="position_R">
			 <input type="text" name="address_second" id="address_second" placeholder="<?php echo $placeholder.'2'; ?>" value="<?php echo $address2; ?>" class="" >
		</div>
		
		 <div class="position_R">
		   <div class="pull-left  frontend_select">
			      <?php
			        // country list data
					$masterCountryDataList[''] = 'Select Country'.$star;	
					if(isset($mastercountrydata) && !empty($mastercountrydata)){
						foreach($mastercountrydata as $key => $value){
							$masterCountryDataList[$value->country_id] = $value->country_name;
						}
					}
			        	$other ='class=" bla bla bli '.$required.' id="""';
						 //echo form_dropdown('contactCountry',$masterCountryDataList,$cpcountry,$other);
			         
			        ?>
			        <select name="contact_country" id="contact_country" class="selectpicker bla bla bli <?php echo $required; ?>" > 
				                <option value="">Select Country<?php echo $star ?></option>
					     <?php
					        	if(isset($mastercountrydata) && !empty($mastercountrydata)){
									foreach($mastercountrydata as $key => $value){
										$selected='';
										if($country==$value->country_id)
										{
											$selected='selected';
										}
									?>
									 <option value="<?php echo $value->country_id; ?>" <?php echo $selected; ?>><?php echo $value->country_name; ?></option>
									 	
									<?php	
									}
							    }
					       
					     ?>
				    </select>
				    
			
			</div>
				  <div class="clearfix"></div>
		</div>
	    
	    <div class="position_R">
		   <div class="pull-left  frontend_select">
			
				 <input type="hidden" id="state_required" value='<?php echo $required; ?>'>
				<div class="contact_state">
					 <?php
					    $masterSateDataList='';
					    //to get state list
					    if(!empty($country)){
                          $masterSateDataList=$this->common_model->getDataFromTabel('master_state','*',array('country_id'=>$country));
                         }
                        
					 ?>
				    <select name="contact_state" id="contact_state" class="selectpicker  bla bla bli <?php echo $required; ?>" > 
				      <option value="">Select State <?php echo $star; ?></option>
				     <?php
				     
				       
					if(!empty($state)  && !empty($masterSateDataList)){
						foreach($masterSateDataList as $key => $value){
							$selected='';		
							if($state==$value->state_id)
							{
								$selected='selected';
							}
						?>	
							 <option value="<?php echo $value->state_id ?>" <?php echo $selected; ?>><?php echo $value->state_name ?></option>
						<?php
						}
					}
					
					 ?>
				    </select>
				 </div>
			</div>
				  <div class="clearfix"></div>
		 </div>
		 
		 <div class="position_R">
		   <div class="pull-left  frontend_select">
			     <?php
			     
					 $where=array('state_id'=>$state);
					 $masterCityDataList= getDataFromTabel('master_city','*',$where);
                  
			     ?>
			    <div class="contact_city">
					<select name="contact_city" id="contact_city" class="selectpicker bla bla bli"> 
						 <option value="">Select City</option>
						 <?php
                          if(!empty($city)  && !empty($masterCityDataList)){
							foreach($masterCityDataList as $key => $value){
								$selected='';		
								if($city==$value->city_id)
								{
									$selected='selected';
								}
						
						?>	
							 <option value="<?php echo $value->city_id ?>" <?php echo $selected; ?>><?php echo $value->city_name ?></option>
						<?php
						}
					}
					?>
					</select>
				</div>	
			</div>
				  <div class="clearfix"></div>
	     </div>		        
		
		
		<div class="position_R">
			 <input type="text" name="post_code" id="post_code" placeholder="<?php echo 'Postcode'; ?>" value="" class="" >
		</div>
		
		
        <?php
	}
	else
	{
	   if($fieldName=='email')
	   {
		    ?>
		       <div class="position_R">
				   <input type="email" name="<?php echo $fieldName; ?>" id="<?php echo $fieldName; ?>" placeholder="<?php echo $placeholder.'" value="'.$fieldValue; ?>"  onkeyup="email_avila_chk()" class="email <?php echo $required;?>">
		       </div>
	          <!--  -->
	       <?php
	   }
	   else
	   {
		 
            ?>
		         <div class="position_R">
					 <input type="<?php echo $fieldType; ?>" name="<?php echo $fieldName; ?>" id="<?php echo $fieldName; ?>" placeholder="<?php echo $placeholder; ?>" value="<?php echo $fieldValue; ?>" class="<?php echo $required; ?>" >
				 </div>
	        <?php
	   }
	}
	
?>

<script>
	var countryName='<?php echo $countryName; ?>';
	var stateName='<?php echo $stateName; ?>';
	var cityName='<?php echo $cityName; ?>';
	if(countryName!='')
	{
		$('#countryName').val(countryName);
		$('#stateName').val(stateName);
		$('#cityName').val(cityName);
	}

</script>
