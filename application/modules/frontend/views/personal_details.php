<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
//--------personal details form---------//	
	

	$registeredId=0; 
	$eventId = (isset($event_id) && !empty($event_id))?$event_id:'';
	$registeredId = (isset($selectRegi) && !empty($selectRegi))?$selectRegi:'';	
	$user_id=frntUserLogin('frantendevent');
	
	?>
	                            
	<div class="accordion-group step2">
		    <div class="accordion-heading">
			<div class="accordion-toggle regi_form" data-toggle="collapse" data-parent="#monogram-acc" href="#PersonalDetails"  >
				<?php  echo lang('step3PersonalDetailsHeading');?>
			</div>
		    </div>
		    
	    <div class="accordion-body collapse" id="PersonalDetails">
			<div class="innerbody">
			    <div class="paragraph"><?php echo lang('PleaseNoteThatFirstMsg'); ?></div>
			     <div class="mendotryfield"><?php echo lang('allFieldsWithMsg'); ?></div>

				 <div class="formheading"><?php echo lang('personalDetailsMsg');  ?></div>
								
				  <div class="formoentform_container">
				  
				   <div id="form_fileds"></div>
				     
				</div>
				<!-- close formoentform_container div -->
				<input type="hidden" name="countryName" id="countryName" value="">
				<input type="hidden" name="stateName" id="stateName" value="">
                 <input type="hidden" name="cityName" id="cityName" value="">


		      
	    </div>
	</div>  
  <!-- close accordion-group div --> 
  
<script>
	
	//set master state list data by state selection
    $(document).on('change','#contact_country',function() {


		var getCountryName = $(this).find("option:selected").text();

		var countryId = $(this).val();
		var required=$('#state_required').val();
		
		sendData = {'countryId':countryId,'required':required};
		postUrl = 'frontend/masterstatelist';
	
		doFrontendAjaxRequest(postUrl,sendData,'contact_state');
		

		//$('#state').html('<option valu="">Select State</option>');
		//$('#city').html('<option valu="">Select City</option>');
		
		$('.selectpicker').selectpicker(); 
		$('#countryName').val(getCountryName);
		

	});
   
	//set master city list data by state selection
	$(document).on('change','#contact_state',function() {
	//$('#contact_state').change(function(){
	    
	    var getStateName = $(this).find("option:selected").text();
 
		var stateId = $(this).val();
		var required=$('#state_required').val();
		sendData = {'stateId':stateId,'required':required};
		postUrl = 'frontend/mastercitylist';
		doFrontendAjaxRequest(postUrl,sendData,'contact_city');
		$('#stateName').val(getStateName);
	});	
	 
    $(document).on('change','#contact_city',function() {
	//$('#contact_city').change(function(){
		
	    var getCityName = $(this).find("option:selected").text();
      
		var stateId = $(this).val();
		sendData = {'stateId':stateId};
		postUrl = 'frontend/mastercitylist';
		doFrontendAjaxRequest(postUrl,sendData,'contactCity');
		$('#cityName').val(getCityName);
	});	
	
     
	
</script>

