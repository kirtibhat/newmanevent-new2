<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
//--------To creat breakout content by ajax process---------//	

 $breakout_date = (isset($breakoutDate) && !empty($breakoutDate))?$breakoutDate:'';
 $selectRegiId = (isset($select_regisId) && !empty($select_regisId))?$select_regisId:'';
 $breakourName = (isset($breakout_name) && !empty($breakout_name))?$breakout_name:'';
   
	if(isset($streams) && !empty($streams))
	{
		
		 $streamContent='';
		 $time='';
		 $tableClass='martopminus20';
		 $date = date_create($breakout_date);
        ?>
		 <div class="registration_table">
       <?php			  
		  if($prevDate!=$date)
		 {
			 $tableClass='';
			 $time='Time';
			 $prevDate=$date;
             ?>			
		    <div class="tableheading"><?php echo ucfirst($breakourName).' - '.date_format($date, 'jS F Y'); ?> </div>
             <?php		  
		}
		 
        ?>
		</div>
              
<div class="frontendtable pb0">
	<div class="panel-body">
		<div id="flip-scroll">
			<table class="'.$tableClass.' table table-bordered table-striped table-condensed cf">
			<tbody>
				 <thead>
				    <tr class="strimbgtable">
					 <td><?php echo $time; ?></td>
					
					   <?php					
						foreach($streams  as $stream)
						{
						 ?>
							 <td><?php echo $stream->stream_name; ?> <br> <font size="-1"> <?php echo $stream->stream_venue; ?></font></td>
						 <?php					
						}
						
						?>					
					 </tr>
				  </thead>
				  
                    <?php
				    if(isset($sessions) && !empty($sessions))
				    {

					    foreach($sessions as $session)
					    {

					     ?>						
						 <tr>
							<td> <?php echo date('H:i', strtotime($session->start_time)).'<br>'.date('H:i',strtotime($session->end_time)); ?></td>
						 <?php		
						 				
							foreach($streams  as $stream)
							{
                                 //get breakout stream seeesion details
								 $sessionArray=array('breakout_session_id'=>$session->id,'breakout_stream_id'=>$stream->id);
								 $aboutSessions=getDataFromTabel('breakout_stream_session','*',$sessionArray);
                                 
								if($aboutSessions)
								{
									foreach($aboutSessions as $about)
									{
                                          
										 /* code to check select stream session */
										$checked='';
										if($selectRegiId!=0)
										{
											if(isset($frntBreakoutDetails) && !empty($frntBreakoutDetails))
											{
												foreach($frntBreakoutDetails as $frntbreakout)
												{
													if($frntbreakout->stream_session_id==$about->id)
													{

														$checked='checked';
													}
												}
											}
										}   
										 $readonly='';
										  
										 $streamLimit=checkbreakoutstreamlimit($about->breakout_session_id,$about->breakout_stream_id);
										   
										
										$breakoutData=$stream->stream_name.' - '.date_format($date, 'jS F Y').', '.date('H:i', strtotime($session->start_time)).' to '.date('H:i', strtotime($session->end_time)); 

                                         ?>
										<td>
											<?php
											
											if(!$streamLimit)
										    {
											    $readonly='readonly';
											 ?>
											      <input type="radio" name="breakout"  class="pull-left breakoutradio breakout_readonly bgzero" value=""  >
											  <?php
										     }else{
											 ?>
											    <input type="radio" name="breakout<?php echo $session->id; ?>" id="breakout<?php echo $about->id; ?>" class="pull-left breakoutradio breakout<?php echo $about->id; ?>" value="<?php echo $about->id;?>" <?php echo $checked; ?>>
											 <?php 
										    }
											 ?>
											
											<input type="hidden" name="breakout_data<?php echo $about->id; ?>" id="breakout_data<?php echo $about->id; ?>" value="<?php echo $breakoutData;?>" >
											<div class="tableradio_cont">
												<span class="clr_lightblue"><?php echo $about->about_session; ?></span>
												<br>
												<div class="clearfix"></div>
												<span><?php echo $about->speakers; ?></span>
												<div class="clearfix"></div>
												<span><?php echo $about->organisation; ?></span>
												
											</div>
										</td>

                                        <?php 
									}
								}

							}
                             
					    }
				    }
                        ?>
                         </tr>
				  

			        </tbody>
			        </table>
		        <div>
		     </div>

	    </div>
    </div>

<div>
<?php
	
	}

?>
