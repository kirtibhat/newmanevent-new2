<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
//--------To calculate amount on payment page  by ajax process---------//	

    $registeredId = (isset($registered_id) && !empty($registered_id))?$registered_id:'';
    $totalamt = (isset($total_amt) && !empty($total_amt))?$total_amt:'';
    if($totalamt==''){ $totalamt='0'; }
   
    ?>
    
    <tr class="editbreakout<?php echo $registeredId; ?>">
	   <td><div class="spacer15"></div></td>
	   <td></td>
	   <td></td>
	</tr>
	
	<tr class="headingrow editsubtotal<?php echo $registeredId; ?>">
		<th class="text-right"><?php echo lang('subTotalMsg'); ?></th>
		<th>$<span id="payment_total_amt<?php echo $registeredId; ?>"><?php echo $totalamt; ?></span></th>
		<th></th>
	</tr>
