<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
//--------this form show all registrant ---------//	
   $isDayWiseSelect='0';
   $registeredId= (isset($registered_id) && !empty($registered_id))?$registered_id:''; 
   $registrant_id= (isset($registrantId) && !empty($registrantId))?$registrantId:''; 
 
?>
 <div class="accordion-group step2">
	 
	<div class="accordion-heading">
		<div class="accordion-toggle registrant_form" data-toggle="collapse" data-parent="#monogram-acc" href="#RegistrationType" >
		  <?php echo lang('step2RegistrationTypeHeading'); ?>
		 
		</div>
	</div>
	<div class="accordion-body collapse regiscollapse" id="RegistrationType">
	   <div class="innerbody">
		 
			<div class="paragraph"><?php echo lang('selectTypeOfRegisMsg'); ?></div>
				<div class="registration_table mt10">
					<div class="tableheading"><?php echo lang('typeOfRegistrationMsg'); ?></div>
					<div class="tableheading"><?php echo lang('priceIncGSTMsg'); ?></div> 
				</div>
				
		 <div class="registration_table">
			 
		    <?php
	            
			 $daywiseid='';
			 $eventId='';
			if(isset($registrantDetails) && !empty($registrantDetails))
			{   
				
				$count=0;
				foreach($registrantDetails as $registrant)
				{
					 $eventId=$registrant->event_id; 
					 $required='';
					 $checked='';
					 $readonly='';
					 $registrantFee=0;
					
					 $feesArray=RegistrationFees($registrant->id);
					 if(!empty($feesArray))
					 {
						 $registrantFee=$feesArray['fees'];
					 }
					
					
					if($registrant->id==$registrant_id)
					{
						 $checked='bgminus17';
						 
					}
			
					if(isset($regisLimitExceed) && !empty($regisLimitExceed))
					{
						if(in_array($registrant->id,$regisLimitExceed))
						{
						    $readonly='readonly';
						} 
					} 
					if(strlen($registrant->password))
					{
						$required='required';
					}
					$regiTotalPrice=$registrant->total_price;
					if($registrant->allow_earlybird>0 && strtotime($registrant->earlybird_date) >= strtotime(date('Y-m-d')))
					{
						 $regiTotalPrice=$registrant->total_price_early_bird; 
						 
					}
					 $regiTotalPrice=$registrantFee+$regiTotalPrice;
					 
					if($regiTotalPrice=='' || $registrant->complementry==1)
					{
						$regiTotalPrice='0';
					}
					 ?>
						<div class="regist_row">
						  <div class="tableheading_2">
						     <input type="radio" name="select_registrant<?php echo $registrant->id; ?>" id="select_registration<?php echo $registrant->id; ?>" onclick="SelectRegistrant('<?php echo $registrant->id;?>')" value="<?php echo $registrant->id;?>" class="select_registration<?php echo $registrant->id.' '.$checked; echo $readonly.' '.$required; ?>"  >
						       <input type="hidden" id="regis_amt<?php echo $registrant->id; ?>" value="<?php echo $regiTotalPrice; ?>" >
						       <label><?php echo ucfirst($registrant->registrant_name); ?></label>
						    <?php
						      if($registrant->group_booking_allowed)
			                    {
									 ?>
								   
									 <div class="regist_smallfont"><?php   echo "Group booking minimum is ".$registrant->min_group_booking_size." registrations."; ?></div>
									 <div class="regist_smallfont">
										  
									<?php
								   
								    if($registrant->percentage_discount_for_group_booking=='' || $registrant->percentage_discount_for_group_booking=='0')
									{
										 echo lang('noDiscountOnGroupMsg');
									}
									else
									{
										 ?>
										   <?php  echo lang('discountIsMsg').' '.$registrant->percentage_discount_for_group_booking.' '."%."; ?>
										  
										<?php 
									}  ?>
									 </div> <?php  
					            } 
					                
					             ?>  
					           <div class="regist_smallfont">
					            <?php echo $registrant->additional_details; ?> 
					            </div> 
					         </div>
					       	
					         <div class="tableheading_2"><?php echo '$'.$regiTotalPrice; ?></div>
                        </div> 
				    <?php 
			
					    if($registrant->option_for_single_day_registration)
					    {
						    $checked=''; 
							if(isset($selectRegiDayWise) && !empty($selectRegiDayWise)>0)
							{
								$checked='bgminus17';
							}
						    ?>
                         <div class="regist_row">
							<div class="regist_mmm">
							<div class="tableheading_2">
									<input type="radio" class="select_day<?php echo $registrant->id.' '.$checked.' '.$readonly; ?>"  onclick="SingleDay('<?php echo $registrant->id; ?>')"  >
						  
									<label>  <?php $title_array = explode(' ', $registrant->registrant_name); $first_word = $title_array[0]; echo ucfirst($first_word); ?> Single Day Registration</label>
									 <div class="regist_smallfont"> Group booking minimum for single day registration is <?php echo $registrant->min_group_booking_size_for_single_day_registrant; ?> registrations.</div>
									<div class="regist_smallfont">
										<?php
										if($registrant->percentage_discount_for_single_day_registrant=='' || $registrant->percentage_discount_for_single_day_registrant=='0')
										{
											 echo "<br>".lang('noDiscountOnGroupMsg')."<br>";
											
										}
										else
										{
											 echo lang('discountIsMsg').' '.$registrant->percentage_discount_for_single_day_registrant.' '."%.";
										}
										?>
								     </div>  
										  
							</div>
							     <div class="tableheading_2"></div>
							</div>  
							<?php
							   
								 if(isset($eventDetails) && !empty($eventDetails))
								{
								    $dateArray=datedaydifference($eventDetails[0]->starttime,$eventDetails[0]->endtime);
								 
								    foreach($dateArray as $value)
								    {
										 $readonly=''; 
										 $count+=1;
										 $day=$value['day'];
										 $date=$value['date'];
										 $DMY= date('d', strtotime($date)).'th '. date('M Y',strtotime($date));
										 
										 $whereDay=array('registrant_id'=>$registrant->id,'day'=>$day);
										  
								        $dayDetails=getDataFromTabel('event_registrant_daywise','*',$whereDay);
	                                   
										if(!empty($dayDetails))
										{
									        if(isset($selectRegiDayWise) && !empty($selectRegiDayWise)>0)
									        {
												
												$checked='';
												if(in_array($dayDetails[0]->id,$selectRegiDayWise))
												{
												   $isDayWiseSelect='1';	
												   $checked='bgminus17';
												   if($daywiseid!='')
												   {
													   $daywiseid=$daywiseid.',';
												   }
												   $daywiseid=$daywiseid.$dayDetails[0]->id;
												}
												
											}
											
													$dayTotalAmt=$dayDetails[0]->total_price;
											
													if($dayDetails[0]->allow_earlybird>0 && strtotime($dayDetails[0]->date_early_bird) >= strtotime(date('Y-m-d')))
													{
														$dayTotalAmt=$dayDetails[0]->total_price_early_bird;
											
													}
													 //add registrqant fees
													  
													 $dayTotalAmt=$registrantFee+$dayTotalAmt;
													 
													
													if(isset($paid_day_regisId) && count($paid_day_regisId)>0)
													{
														foreach($paid_day_regisId as $key=>$value)
														{
															
															if($dayDetails[0]->id==$key && $paid_day_regisId[$key]==$dayDetails[0]->registrant_daywise_limit)
															{ 
																 $readonly='readonly';
																 
															} 
														}
													}
												
												 ?>
													<div class="regist_mmm">
													
														<div class="tableheading_2 pt2pb2">
															 <input id="check<?php echo $dayDetails[0]->id; ?>" type="checkbox"  value="<?php echo $dayDetails[0]->id;?>" class="check_day<?php echo $registrant->id.' '.$checked;?> ml25 checked<?php echo $dayDetails[0]->id.' '.$readonly;?>" onclick="dayRegistration('<?php echo $registrant->id; ?>','<?php echo $dayDetails[0]->id; ?>')" >
															 <label class="pl25"><b><?php echo lang('regisDay'); echo $day.' - ' ?></b> <?php echo $DMY; ?> 
															  
															 <input type="hidden" id="day_amt<?php echo $dayDetails[0]->id; ?>" value="<?php echo $dayTotalAmt; ?>" >

														</div>
														<div class="tableheading_2 pt2pb2"><?php  echo '$'.$dayTotalAmt; ?></div>
													   
													</div> 
												
												  <?php
										}	 	
								    } //end ot the inner loop
								   
							    } ?>
					     </div> <?php
						}	 
			             ?>				
			                  <div class="clearfix"></div>	
		           <?php
		        } // end of the outer loop
	        }
		   
	  ?>
				<div class="registration_table">
					<div class="tableheading text-right"><?php echo lang('frntTotalMsg'); ?></div>
					<div class="tableheading">$<span id="regis_amount">0</span> </div> 	
				</div>
				
		        <input type="hidden" name="regis_total_amt" id="regis_total_amt" value="" >
		        <input type="hidden" name="registrant_id" id="registrant_id" value="<?php echo $registrant_id; ?>">
		         <input type="hidden" name="regis_select_id" id="regis_select_id" value="">
                <input type="hidden" name="daywise_id" id="daywise_id" value="<?php echo $daywiseid; ?>" >
                 <input type="hidden" name="singleday_regis_id" id="singleday_regis_id" value="<?php if($isDayWiseSelect){ echo $registeredId; }?>">
                <input type="hidden" name="registered_id" id="registered_id" value="<?php echo $registeredId; ?>">
		        <input type="hidden" name="regis_event_id" id="regis_event_id" value="<?php echo $eventId; ?>">
		      
			 </div> <!-- end of the table div -->
			 
		  </div>
		</div>	
   </div>

<script type="text/javascript">
   
    var id='<?php echo $registrant_id; ?>';
    var dayid=0;
	var base_url='<?php echo base_url(); ?>'; 
	var IMAGE='<?php echo IMAGE; ?>'; 
	var prevId=0; 
	var addsideevent=false;
	var guestremove=false; 
	  
	/** function for select registration **/
	 SelectRegistrant(id);
	 
   /** function for select registration daywise **/
   
	 dayRegistration(id,dayid);
	


</script>

