<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
//--------To show breakout content---------//	
$registeredId= (isset($registered_id) && !empty($registered_id))?$registered_id:'0'; 
$user_id=isLoginGeneralUser();
$frntBreakoutDetails=getDataFromTabel('frnt_breakouts','*',array('user_id'=>$user_id));

$selectBrk='';
//$selectRegiId=''; 
?>
  <div class="accordion-group step2">
		<div class="accordion-heading">
			<div class="accordion-toggle breakoutform" data-toggle="collapse" data-parent="#monogram-acc" href="#Breakouts">
			   <?php echo lang('step5BreakoutsHeading'); ?>
			</div>
		</div>
		<div class="accordion-body collapse" id="Breakouts">
		 <div class="innerbody">
			   <?php echo lang('PleaseselectbreakoutMsg'); ?>
		
				<?php
				   
				    if(isset($breakoutDetails) && !empty($breakoutDetails))
				    {
						foreach($breakoutDetails as $breakout)
						{
							$sessionIntervals=getStreamSessionInterval($breakout->id);
							$date=date_create($breakout->date);
							//to get breakout stream on the basis of breakout Id
							$streams=$this->common_model->getDataFromTabel('breakout_stream','*',array('breakout_id'=>$breakout->id));
						?>
						<div class="mt10">
						<div id="breakoutContent">	
						<div class="registration_table">
						   <div class="tableheading"><?php echo ucfirst($breakout->breakout_name).' - '.date_format($date, 'jS F Y'); ?> </div>
                         
                        </div> 
                        <div class="frontendtable pb0">
						<div class="panel-body">
							<div id="flip-scroll">
								<table class=" table table-bordered table-striped table-condensed cf table_breakout">
								<tbody>
								 <thead> 
									<tr class="strimbgtable">
										<td>Time</td>
									   <?php					
										foreach($streams  as $stream)
										{
										 ?>
											 <td><?php echo $stream->stream_name; ?> <br> <font size="-1"> <?php echo $stream->stream_venue; ?></font></td>
										 <?php					
										}
										
										?>					
									 </tr>
								</thead>
							<tr>	
							  <td>
							 <?php
						 
								//to check time interval
								$intervalPosition='';
								$timeIntervalArray=''; 
								if(isset($streams) && !empty($streams))
								{ 
									foreach($streams  as $stream)
									{
										$sessionDetails=getSessionDetails($stream->id);
										if(!empty($sessionDetails))
										{
											 $sessionId='';
											 
											foreach($sessionDetails as $session)
											{
												
												$sessionCount=0;
												 $sessionId=$session->id;
												if(isset($sessionIntervals) && !empty($sessionIntervals))
												{    
													$count=0;
													$sessionCount;	
													foreach($sessionIntervals as $sessionInterval) 
													{
														 $startDate='';
														 $endDate='';
														 $count+=1;
														 
														 if(isset($sessionInterval['start_date'])){
															   $startDate=$sessionInterval['start_date'];
															   $endDate=$sessionInterval['end_date'];
														    }
															if(strtotime($endDate)<strtotime($session->end_time))
															{
															 	
															  $intervalPosition[$sessionId]=$count;
															}
															if(strtotime($startDate)<strtotime($session->start_time))
															{
																$sessionCount+=1;
																//echo $startDate.':'.$session->start_time.'<br>';
																$timeIntervalArray[$sessionId]=$sessionCount;
															}
													}  
												}
											}
										}
									}
								}
								
								if(isset($sessionIntervals) && !empty($sessionIntervals))
								{    
									$count=0;
									foreach($sessionIntervals as $sessionInterval) 
									{
										$count+=1;
										 $startDate='';
										 $endDate='';
										 if(isset($sessionInterval['start_date'])){
											   $startDate=$sessionInterval['start_date'];
											   $endDate=$sessionInterval['end_date'];
										   }
										  ?>
										  <div class="brea_main_block minheight89">	
										    <?php echo $startDate.' - '.$endDate; ?>
										   </div>	
										<?php
									}
								}
								?>
								
							  </td>
						    <?php
						              
									if(isset($streams) && !empty($streams))
									{
			
										foreach($streams  as $stream)
										{
											 $sessionDetails=getSessionDetails($stream->id);
											?>	
											<td>
											<?php	
									
											//to get breakout session on the basis of breakout Id
											if(!empty($sessionDetails))
											{
												$count=0;
												foreach($sessionDetails as $session)
												{
													// code to check select stream session 
													$checked='';
													if($registeredId!=0)
													{
														if(isset($frntBreakoutDetails) && !empty($frntBreakoutDetails))
														{
															foreach($frntBreakoutDetails as $frntbreakout)
															{
																if($frntbreakout->stream_session_id==$session->id)
																{
							                                        
																	$checked='bgminus17';
																	if($selectBrk!='')
																	{
																		$selectBrk=$selectBrk.',';
																	}
																	 $selectBrk=$selectBrk.$session->id;
																}
															}
														}
													}   
													 $readonly='';
													  
													// $streamLimit=checkbreakoutstreamlimit($session->breakout_session_id,$session->breakout_stream_id);
													   
													$breakoutSessionTime=date_format($date, 'jS F Y').', '.date('H:i', strtotime($session->start_time)).' to '.date('H:i', strtotime($session->end_time)); 
													$breakout_data=$stream->stream_name.' - '.$breakoutSessionTime;
													 ?>
														<?php
														
														//if(!$streamLimit)
														//{
															$readonly='';  //'readonly';
														 ?>
															<!--  <input type="radio" name="breakout"  class="pull-left breakoutradio breakout_readonly bgzero" value=""  > -->
														  <?php
														// }else{
															
														 //to add class
														 $startInterval='';
														 $interval='';
														 $pedding='';
														if(!empty($intervalPosition) && !empty($timeIntervalArray))
														{
															   if (array_key_exists($session->id, $intervalPosition)) {
															       $interval=$intervalPosition[$session->id];
															          if($interval!='0' && $interval!='1'){
																		 $mar='57';  
																		$pedd= $mar*$interval; 
																	
																		$pedding='style="padding-bottom:'.$pedd.'px;"';
															        } 
															    } 
															    if (array_key_exists($session->id, $timeIntervalArray)){
															        
															        $startInterval=$timeIntervalArray[$session->id];
															        $startInterval=$startInterval-$count;
																		for($i=1; $i<=$startInterval;)
																		{
																			?>	
																			 <div class="brea_main_block " ></div>
																			<?php
																			$i++;
																		}
															    }
														}
														 ?>
											              <div class="brea_main_block " <?php echo $pedding; ?>>	
															<input type="radio" name="breakout<?php echo $session->id; ?>" id="breakout<?php echo $session->id; ?>" class="pull-left breakoutradio breakout<?php echo $session->id; ?> <?php echo $checked; ?>" value="<?php echo $session->id;?>"   >
														 <?php 
														  
														//}
														   $totalWord='100';
														   $word= strlen($session->speakers.$session->organisation);
														   $totalWord=$totalWord-$word;
														  if(strlen($session->about_session)>$totalWord){
														    //$string=str_replace(' ', '',$session->about_session);
														     $aboutSession= substr($session->about_session, 0, $totalWord);
													      }
													      else
													      {
															  $aboutSession=$session->about_session;
														  }
														 ?>
														<input type="hidden" name="breakout_data<?php echo $session->id; ?>" id="breakout_data<?php echo $session->id; ?>" value="<?php echo $breakout_data;?>" >
														<div class="tableradio_cont">
															
															<span class="clr_lightblue"><?php echo $aboutSession; ?></span>
															<br>
															<div class="clearfix"></div>
															<span><?php echo $session->speakers; ?></span>
															<span><?php echo $session->organisation; ?></span>
															<div><?php //echo $breakoutSessionTime; ?></div>
														</div>
													   </div>
													<?php 
														$count+=1;
												}
											} 
											 ?>
											
											 </td>
											 
											<?php
											 
										}
									}
									?>
									  </tr>
							
								
								</tbody>
							   </table>
							 
							   </div>
							 </div>
							</div>
							</div>
						   </div>
							<?php
			
						}//end of outer foreach loop


	                }
	
                    ?> 
				  
				 <!-- <div id="breakoutContent"></div>  -->
		           <input type="hidden" name="stream_session_id" id="stream_session_id" value="<?php echo $selectBrk; ?>">
		
			 
      </div>
     </div>
 </div>  			 

<script type="text/javascript">

var registered_id='<?php echo $registeredId; ?>';

/*?
 $(document).ready(function() {	
 
	  //get value of breakout form 
	  $.ajax({
		  type: "POST",
		  url: baseUrl+'frontend/createbreakoutform',
		  data: 'registered_id='+registered_id,
			success: function(data) {

			// $('#breakoutContent').html(data);
			 getSelectedBreakout();
			$('.breakoutradio').screwDefaultButtons({
				image: 'url("'+IMAGE+'foontendradio.png")',
				width: 17,
				height: 17,
			});
			 
			}
		 });
     
	});    
	*/
</script>
