
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
  /********--------this template used to send registrant details to user. ----************/

   // to get event_id of login user
	$eventId=getEventId();	 
    $eventTitle = (isset($payemtntDetails) && !empty($payemtntDetails->event_title))?$payemtntDetails->event_title:'';
 	$firstName = (isset($firstname) && !empty($firstname))?$firstname:'User';
 	$transactionId = (isset($payemtntDetails) && !empty($payemtntDetails->transaction_id))?$payemtntDetails->transaction_id:'';
 	$bookingId= (isset($payemtntDetails) && !empty($payemtntDetails->booking_id))?$payemtntDetails->booking_id:''; 
    $paymentType= (isset($payemtntDetails) && !empty($payemtntDetails->payment_type))?$payemtntDetails->payment_type:''; 
    $totalAmount= (isset($payemtntDetails) && !empty($payemtntDetails->total_amount))?$payemtntDetails->total_amount:'0'; 
    $bankName= (isset($payemtntDetails) && !empty($payemtntDetails->bank_name))?$payemtntDetails->bank_name:''; 
  
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
	
	<title>Event Details</title>
	
	<meta content="text/html; charset=iso-8859-1" http-equiv="Content-Type" />
	<style type="text/css">
	.list a {color: #cc0000; text-transform: uppercase; font-family: Verdana; font-size: 11px; text-decoration: none;}

	</style>
	
	
</head>
<body marginheight="0" topmargin="0" marginwidth="0" bgcolor="#c5c5c5" leftmargin="0">

<table cellspacing="0" border="0" style="background-image: url(<?php echo base_url('templates/system/images/email_images'); ?>/bg.gif); background-color: #c5c5c5;" cellpadding="0" width="100%">
	
	<tr>
		
		<td valign="top">

			<table cellspacing="0" border="0" align="center" style="background: #fff; border-right: 1px solid #ccc; border-left: 1px solid #ccc;" cellpadding="0" width="600">
				<tr>
					<td valign="top">
						<!-- header -->
						<table cellspacing="0" border="0" height="157" cellpadding="0" width="600">
							<tr>
								
								<td class="header-text" height="25" valign="top" style="color: #999; font-family: Verdana; font-size: 10px; text-transform: uppercase; padding: 0 20px;" width="540" colspan="2">
									<img  alt="Image" src="<?php echo base_url('templates/system/images/'); ?>/logobig.png" style="border: 0; display: block; padding: 10px 0 10px 0;" editable="true" label="Image" />
								</td>
								
							</tr>
							<tr>
								<td class="main-title" height="13" valign="top" style="padding: 0 20px; color:#F16531; font-size: 25px; font-family: Georgia; font-style: italic;" width="600" colspan="2">
									<singleline label="Title">Registration for <?php echo ucfirst($eventTitle);?></singleline>
								</td>
								<td class="header-bar" valign="top" style="color: ##009EC2; font-family: Verdana; font-size: 10px; text-transform: uppercase; padding: 0 20px; height: 15px; text-align: right;" width="200">
									<currentdayname /> <currentday /> <currentmonthname /> <currentyear />
								</td>
								<tr>
									<td height="20" valign="top" width="600" colspan="2">
										<img src="<?php echo base_url('templates/system/images/email_images'); ?>/breaker.jpg" height="20" alt="" style="border: 0;" width="600" />
									</td>
								</tr>
						
						<!-- / header -->
					
				</table></td>
				<tr>
					<td>
						<!-- content -->
						<repeater>
						<table cellspacing="0" border="0" cellpadding="0" height="200" width="600">
							<tr>
								<td class="article-title" height="45" valign="top" style="padding: 20px 20px 10px; color:#5C6F7B; font-family: Georgia; font-size: 16px; font-weight: bold;" width="600">
									<singleline label="Title">
									Hello <?php echo ucfirst($firstName); ?>, 
									<br>	
									<br>	
										Congratulations! You have successfully registered for <?php echo $eventTitle; ?>. Event details is as follows below:</singleline>
								
								</td>
							</tr>
							<td class="article-title" height="45" valign="top" style="padding: 20px 20px 10px; color:#5C6F7B; font-family: Georgia; font-size: 16px; font-weight: bold;" width="600">
									<singleline label="Title">
									 Payment Details: <br> <br>	
									    <?php if($paymentType=='credit_card'){ ?>
									    TransactionId :  <?php echo $transactionId.'<br>';
									   }
									    ?>
									   	
									     BookingId :  <?php echo $bookingId;?>
								        <br>
								        Payment By :  <?php echo $paymentType;?>
								        <?php if($bankName!=''){?>
								         <br>
								         Bank Name :  <?php echo $bankName;?>
								         <?php }  ?>
								       
								         <br>
								         Amount :  $<?php echo $totalAmount;?>
								</td>
							</tr>
							<?php 
							   
							    if(isset($dataDetails) && !empty($dataDetails))
							    {
									$count='0';
									foreach($dataDetails as $data)
									{
										
							          $count+=1;
							     ?>
									<tr>
										<td class="article-title" height="45" valign="top" style="padding: 20px 20px 10px; color:#5C6F7B; font-family: Georgia; font-size: 16px; font-weight: bold;" width="600">
											<singleline label="Title">
											Registrant : 
											<?php  echo $data['registrant_name'];?>
										</td>
									</tr>
									<tr>
										<td class="article-title" height="45" valign="top" style="padding: 20px 20px 10px; color:#5C6F7B; font-family: Georgia; font-size: 16px; font-weight: bold;" width="600">
											<singleline label="Title">
											SideEvent : <br>	
												<?php  echo $data['sideevent'];?>
										</td>
									</tr>
									<tr>
										<td class="article-title" height="45" valign="top" style="padding: 20px 20px 10px; color:#5C6F7B; font-family: Georgia; font-size: 16px; font-weight: bold;" width="600">
											<singleline label="Title">
											Breakout : 	<br>
												<?php  echo $data['breakout'];?>
										</td>
									</tr>
									  <?php
								    }
								}
									?>
							<tr>
								<td class="content-copy" valign="top" style="padding: 0 20px 10px; color: #000; font-size: 14px; font-family: Georgia; line-height: 20px;">
									  <br>
									  <multiline label="Description">Please click here to update registrant details : <a href="<?php echo base_url().'frontend/index'.encode($eventId); ?>" style="color:#352C65"><?php echo base_url().'frontend/index'.encode($eventId); ?></a></multiline>
								</td>
							</tr>		
									
							<tr>
								<td class="article-title" height="25" valign="middle" style="padding: 20px 20px 10px; color:#5C6F7B; font-family: Georgia; font-size: 16px; font-weight: bold;" width="600">
									Team Newmanevents
								</td>
							</tr>
							
						</table>
						</repeater>
						<!--  / content -->
					</td>
				</tr>
				<tr>
					<td valign="top" width="600">
						<!-- footer -->
						<table cellspacing="0" border="0" height="202" cellpadding="0" width="600">
							<tr>
								<td height="20" valign="top" width="600" colspan="2">
									<img src="<?php echo base_url('templates/system/images/email_images'); ?>/breaker.jpg" height="20" alt="" style="border: 0;" width="600" />
								</td>
							</tr>
							<tr>
								<td class="copyright" height="100" align="center" valign="top" style="padding: 0 20px; color: #009EC2; font-family: Verdana; font-size: 10px; text-transform: uppercase; line-height: 20px;" width="600" colspan="2">
									<multiline label="Description">www.newmanevents.com <br />
									Company - 123 Some Street, City, ST 99999. Ph +1 4 1477 89 745</multiline>
								</td>
							</tr>
						</table>
						<!-- / end footer -->
					</td>
				</tr></table></td>

</table>
</body>
</html>
