<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
//--------Contact person details form---------//	
?>
<?php
      $eventId = (isset($event_id) && !empty($event_id))?$event_id:'';
      $userLogin=frntUserLogin('frantendevent');
  
      //set contact form data
      $firstName = (!empty($userDetails->firstname))?$userDetails->firstname:'';
      $surname = (!empty($userDetails->lastname))?$userDetails->lastname:'';
      $usertitle = (!empty($userDetails->user_title))?$userDetails->user_title:'';
      $email=(!empty($userDetails->email))?$userDetails->email:'';
      $phone=(!empty($userDetails->phone))?$userDetails->phone:'';
      $mobile=(!empty($userDetails->mobile))?$userDetails->mobile:'';
      $company=(!empty($userDetails->company_name))?$userDetails->company_name:'';
      $position=(!empty($userDetails->position))?$userDetails->position:'';
      $titlevalue='';
      /* to show required field */
      $pass=lang('frntPassword').'*';
      $confirmPass=lang('regisConfirmPassMsg').'*';
      $class='error_message';
      $readonly='';
      $formCheckClass='';
     
     
      if($userLogin)
      {
		   $pass=lang('frntPassword');
           $confirmPass=lang('regisConfirmPassMsg');
           $class='';
           $readonly='readonly';
           $titlevalue=$usertitle;
           $formCheckClass='check_form';
	  }
	  else
	  {
		   $usertitle=lang('regisUserTitle');  
	  }
      
       $formContactRegistration = array(
		'name'	 => 'registrantfronntendForm',
		'id'	 => 'registrantfronntendForm',
		'method' => 'post',
		'class'  => 'form-vertical',
	); 
	   $titleArray=array(
	            $titlevalue =>$usertitle,
			   'Mr'=>lang('regisUserTitleMr'),
			   'Mrs'=>lang('regisUserTitleMrs'),
			   'Ms'=>lang('regisUserTitleMs'),
			   'Miss'=>lang('regisUserTitleMiss'),
			   
			   );
      
      $contactFirstName = array(
		'name'	=> 'contact_first_name',
		'value'	=> $firstName,
		'id'	=> 'contact_first_name',
		'class'	=> 'error_message alphaValue',
		'placeholder'=>lang('regisFirstName').'*',
	);
	
	 $contactSurnameName = array(
		'name'	=> 'contact_surname',
		'value'	=> $surname,
		'id'	=> 'contact_surname',
		'class'	=> 'error_message alphaValue',
		'placeholder'=>lang('regisSurname').'*',
	);
	
	 $contactEmail = array(
		'name'	=> 'contact_email',
		'value'	=> $email,
		'id'	=> 'contact_email',
		'class'	=> 'error_message email',
		'onkeyup'=> 'email_avila_chk()',	
		 $readonly=>$readonly,
		'placeholder'=>lang('frntEmail').'*',
	);
	 $contactPassword = array(
		'name'	=> 'contact_password',
		'type' =>'password',
		'value'	=> '',
		'id'	=> 'contact_password',
		'class'	=> $class,
		'maxlength'=>'25',
		'placeholder'=>$pass,
	);
	 $contactConfirmPass = array(
		'name'	=> 'contact_confirm_password',
		'type' =>'password',
		'value'	=> '',
		'id'	=> 'contact_confirm_password',
		'class'	=> $class,
		'maxlength'=>'25',
		'placeholder'=>$confirmPass,
	);
	 $contactPhone = array(
		'name'	=> 'contactphone',
		'value'	=> $phone,
		'id'	=> 'contactphone',
		'class'	=> 'error_message mobile_value mobileValue',
		'placeholder'=>'Phone*',
	);
	 $contactMobile = array(
		'name'	=> 'contact_mobile',
		'value'	=> $mobile,
		'id'	=> 'contact_mobile',
		'class'	=> 'error_message mobile_value mobileValue codeFlag flag_text_field',
		'placeholder'=>lang('regisMobile').'*',
	);
	 $contactCompanyName = array(
		'name'	=> 'contact_cmp_name',
		'value'	=> $company,
		'id'	=> 'contact_cmp_name',
		'class'	=> 'error_message alphaNumValue',
		'placeholder'=>lang('regisCompanyName').'*',
	);
	 $contactPosition = array(
		'name'	=> 'contact_position',
		'value'	=> $position,
		'id'	=> 'contact_position',
		'class'	=> 'error_message alphaNumValue',
		'placeholder'=>lang('regisPosition').'*',
	);
	
 ?>

     <div class="accordion-group">
			<div class="accordion-heading">
				<div class="accordion-toggle contact_person <?php echo $formCheckClass; ?>" data-toggle="collapse" data-parent="#monogram-acc" href="#ContactPersonDetails">
				<?php echo lang('regisContactPersonDetailsHeading'); 
				   if($userLogin){ 
				 ?>
		              <div class="complete_check contact_check"></div>
		            <?php } ?>  
				</div>
			</div>
			
	  <?php echo form_open($this->uri->uri_string(),$formContactRegistration); ?>
		<div class="accordion-body collapse contactForm " id="ContactPersonDetails">
			<div class="innerbody">
					<div class="paragraph">
						  <?php echo lang('regisPleaseNoteThatMsg'); ?>
						</div>
					<div class="mendotryfield">
					   <?php echo lang('regisAllFiledsMsg'); ?>
					 </div>
			
			<div class="formoentform_container">
		  
				<div class="position_R">
					<div class="pull-left frontend_select">
					<?php
						  $other='class="selectpicker bla bla bli" id="contact_user_title" ';
						  echo form_dropdown('contact_user_title',$titleArray, $titlevalue, $other);
					?>
					</div>
				<div class="clearfix"></div>
				</div>
		   
			<div class="position_R ">
			<!--	<input type="text" name="contact_first_name" id="contact_first_name" placeholder="First Name*" class="error_message" value="<?php //if(count($userDetails)){ echo $userDetails->firstname; }?>" > -->
				  <?php echo form_input($contactFirstName); ?>
			</div>
			
			 <div class="position_R ">
				   <?php echo form_input($contactSurnameName); ?>
				
			</div>
			
			 <div class="position_R">
				   <?php echo form_input($contactEmail); ?>
				 <label class="efformessage contact_email"><span class="errorarrow"></span>Please enter valid email</label>
			 </div>
			<!-- Add password field -->
			 <div class="position_R">
				 
				   <?php echo form_input($contactPassword); ?>
			  
			 </div>
			
			 <div class="position_R">
				 <?php echo form_input($contactConfirmPass); ?>
				 <label class="efformessage confirm_password_msg"><span class="errorarrow"></span>Password not Match</label>
			 </div>
			
			
			<div class="position_R">
				  <?php echo form_input($contactPhone); ?>
			</div>
			
			<div class="position_R">
				  <?php echo form_input($contactMobile); ?>		  
			</div>
			
			<div class="position_R">
				   <?php echo form_input($contactCompanyName); ?>			   
			 </div>
			<div class="position_R">
				   <?php echo form_input($contactPosition); ?>			  
			 </div>
		   
			<div class="clearfix"></div> 
			<div class="spacer15"></div>
			 <div class="mb20">
				 <?php 
				        $userhidden = array(
				            'type'      =>'hidden',
							'name'      => 'user_id',
							'id'        => 'user_id',
							'value'     => $userLogin,
						);
						  $regishidden = array(
				            'type'      =>'hidden',
							'name'      => 'regis_event_id',
							'id'        => 'regis_event_id',
							'value'     => $eventId,
						);
						echo form_input($userhidden); 
						echo form_input($regishidden);
						$extraSave 	= 'class="frontendsubmitbtn mr0" id="submit_btn"';
						echo form_submit('submit',lang('frntSubmit'),$extraSave);
						
					?>
			
			<div class="clearfix"></div> 
			 </div>
		 

			 <div class="row-fluid loadershowdiv ">
				<div class="text-center loadershow dn">
					 <?php echo lang('frntLoadingMsg'); ?>
				</div>
				<div class="text-center mesasgeshow dn"></div>
			</div>
		   
			</div>
		</div>
	</div>
	      
	<?php echo form_close(); ?>		
</div>



	<!-- close contact person group -->
                      
				      
<script type="text/javascript">
	
/**
 code for submit form data 
*/

 $('.loadershow').hide();
 $('.contact_email').hide();
 $('.confirm_password_msg').hide();	
 	
$(document).ready(function(){
	
 $("#registrantfronntendForm").submit(function( event ) {
       
       var check_email= email_avila_chk();
       if(check_email==false)
       {
		   return false;    
	   }
		var fromData=$("#registrantfronntendForm").serialize();
		var url = baseUrl+'frontend/savecontactperson';
		$('.loadershow').show();    
		$.post(url,fromData, function(data) {
			  if(data){
                 
				if(data.is_success=='true'){	
						  refreshPge();	
						  return false; 
					}
					$('.loadershow').hide();
					if(data.is_success=='1')
					{
						custom_popup(data.msg,true);
					}
					else{
						custom_popup(data.msg,false);		
					}
				}
			},"json");
		return false;	 
	});

//to drop down list
//$('.selectpicker').selectpicker(); 
  
	});	


</script>
