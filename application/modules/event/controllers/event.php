<?php
/**
 * By this class you can manage event setup section
 * 
 * @author  Lokendra Meena
 * @email   loknedrameena@cdnsol.com
 * @Hint    In this class all public method name shoule be normal case 
 * and private metho name shoule be camel case 
 * 
 */

class Event extends MX_Controller{
  /**
   * Constructor - Sets Event Class require helper and library other.
   * The constructor can be passed an array of config values
   */
    public $accountType =   1;
    public $fileSuffix  =   '';
    
    function __construct(){
    parent::__construct();
    $this->load->library('factory',$this);
    $this->load->library('event_libraries_factoryfree',$this);
    $this->load->library(array('head','template','process_upload'));
    $this->load->model(array('event_model'));
    $this->load->language(array('event'));
    $this->form_validation->set_error_delimiters('<div class="server_msg_show">', '</div>');
    $this->session_check->checkSession(); 
    }
    
    /*
    * @description: This function is used to save add event data
    * @return     : @void
    */ 

    public function addevent(){
    $this->factory->addevent(); 
    } 

    /*
    * @description: This function is used to to open popup of event category 
    * @return  : @void
    */ 
    public function eventcategorypopup(){
    $this->factory->eventcategorypopup();
    }

    /*
    * @description: This function is used to save add new event category
    * @return  : @void
    */ 
    public function addupdateeventcategory(){
    $this->factory->addupdateeventcategory(); 
    }

    /*
    * @description: This function is used to delete event category
    * @return  : @void
    */ 
    public function deleteeventcategory(){
    $this->factory->deleteeventcategory();  
    }
  
    /*
    * @description: This function is used to open popup of custom contact type
    * @return  : @void
    */ 
    public function eventcustomcontactpopup(){
    $this->factory->eventcustomcontactpopup();  
    }

    /*
    * @description: This function is used to get custom contact list
    * @return  : @void
    */ 
    public function geteventcustomcontact(){
    $this->factory->geteventcustomcontact();  
    }

    /*
    * @description: This function is used to open popup for email list
    * @return  : @void
    */ 
    public function eventcustomemailpopup(){
    $this->factory->eventcustomemailpopup();  
    }

    /*
    * @description: This function is used to save Custom email
    * @return  : @void
    */ 
    public function addupdateeventemail(){
    $this->factory->addupdateeventemail();  
    }

    /*
    * @description: This function is used to add attachment using Custom email
    * @return  : @void
    */ 
    public function addupdateeventemailattach(){
    $this->factory->addupdateeventemailattach();  
    }

    /*
    * @description: This function is used to delete the Custom email
    * @return  : @void
    */ 
    public function deleteeventemail(){
    $this->factory->deleteeventemail(); 
    }

    /*
    * @description: This function is used to delete email attachment
    * @return  : @void
    */ 
    public function deleteemailachment(){
    $this->factory->deleteemailachment(); 
    }

    /*
    * @description: This function is used to show event details forms like gerneral event, contact person
    * @param1: $eventId
    * @return: void
    *  
    */

    public function eventdetails($eventId="0"){	
    ($this->config->item('accountType')==1) ? $this->event_libraries_factoryfree->eventdetails($eventId) : $this->factory->eventdetails($eventId);
    }

    /*
    * @description: This function is used to upload sponsors logo
    * @param : upload path
    * @return void
    * 
    */ 

    public function uploadsponsorslogo(){
    $this->factory->uploadsponsorslogo(); 
    }

    /*
    * @description: This function is used to delete uploaded sponsors logo
    * @param:  deleteId
    * @return void
    * 
    */ 

    public function deletesponsorslogo(){
    $this->factory->deletesponsorslogo(); 
    } 

    /*
    * @description: This function is used to upload  sponsors logo
    * @param : upload path
    * @return void
    * 
    */ 

    public function uploadtermattachmentterm(){
        ($this->config->item('accountType')==1) ? $this->event_libraries_factoryfree->uploadtermattachmentterm() : $this->factory->uploadtermattachmentterm();
     
    } 

    /*
    * @description: This function is used to upload  sponsors logo
    * @param : upload path
    * @return void
    * 
    */ 

    public function uploadtermattachmentrefund(){
    $this->factory->uploadtermattachmentrefund(); 
    } 

    /* (Still this method are not using)
    * @description: This function is used to delete term and codition document
    * @return void
    */ 

    public function deletetermattachmentterm(){
    $this->factory->deletetermattachmentterm(); 
    } 

    /* (Still this method are not using)
    * @description: This function is used to delete term and codition document
    * @return void
    */ 

    public function deletetermattachmentrefund(){
    $this->factory->deletetermattachmentrefund(); 
    }

    /*
    * @description: This function is used to  delete event
    * @param : eventId
    * 
    */

    public function deleteevent($eventId){
    $this->factory->deleteevent($eventId);  
    }

    /*
    * @description: This function is used to registrant details  
    * @param: eventId
    * @return: void
    * 
    */ 

    public function setupregistrant(){  
    $this->factory->setupregistrant();  
    }

    /*
    * @description: This function is used to add and edit perosnal details form custom field
    * @return void
    * 
    */ 

    public function add_edit_custom_field(){
    $this->factory->addeditcustomfield(); 
    }

    /*
    * @description: This function is used to add type of registration 
    * @return void
    * 
    */ 

    public function addediteventregistration(){
    $this->factory->addediteventregistration(); 
    }

    /*
    * @description: This function is used to duplicate type of registration 
    * @return void
    * 
    */ 

    public function duplicateeventregistration(){
    $this->factory->duplicateeventregistration(); 
    }

    /*
    * @description: This function is used to delete type of registration 
    * @return void
    * 
    */   

    public function deleteeventregistration(){
    $this->factory->deleteeventregistration();
    } 

    /*
    * @description: This function is used to edit and update custome field in personal form
    * @return void
    * 
    */ 

    public function addeditcustomefieldsave(){
    $this->factory->addeditcustomefieldsave();
    } 
  
    /*
    * @description: This function is used to delete custome field in personal form
    * @return void
    * 
    */   

    function deleteeditcustomefield(){
    $this->factory->deleteeditcustomefield();
    }

    /*
    * @description: This function is used to add dietary 
    * @return void
    * 
    */ 

    public function addeventdietary(){
    $this->factory->addeventdietary();
    }

    /*
    * @description: This function is used to update registrant order 
    * @param:  registrantsid
    * @return void
    * 
    */ 

    public function regitrantorderupdate(){
    $this->factory->regitrantorderupdate();
    }

    /*
    * @description: This function is used to delete dietary 
    * @param:  deleteId
    * @return void
    * 
    */ 


    public function deletedietarytype(){
    $this->factory->deletedietarytype();
    } 

    /*
    * @description: This function is used to setup side events 
    * @param: eventId
    * @return: void
    * 
    */ 

    public function setupsideevents(){
    $this->factory->setupsideevents();
    }

    /*
    * @description: This function is used to open add & edit side event popup view
    * @return void
    * 
    */ 

    public function addeditduplicatesideeventview(){
    $this->factory->addeditduplicatesideeventview();
    }

    /*
    * @description: This function is used to save add & edit side event popup data
    * @return void
    * 
    */ 

    public function addeditduplicatesideeventsave(){
    $this->factory->addeditduplicatesideeventsave();
    }

    /*
    * @description: This function is used to delete side event 
    * @return void
    * 
    */   

    public function deletesideevent(){

    $this->factory->deletesideevent();
    }

    /*
    * @description: This function is used to setup extras
    * @param: eventId
    * @return: void
    * 
    */ 

    public function setupextras(){
    $this->factory->setupextras();
    }
    
    /*
    * @description: This function is used to load popup section for extras
    * @return: void
    *  
    */
    public function extrapopup(){
    $this->factory->extrapopup();
    }

    /*
    * @description: This function is used to add type of registration 
    * @return void
    * 
    */ 

    public function addeditextrasave(){
    $this->factory->addeditextrasave(); 
    }

    /*
    * @description: This function is used to delete extra item 
    * @return void
    * 
    */
    public function deleteextraitem(){    
    $this->factory->deleteextraitem();
    }

    /*
    * @description: This function is used to add event extra item custome field
    * @return void
    * 
    */
    public function eventextrascustomfield(){    
    $this->factory->eventextrascustomfield();
    }

    /*
    * @description: This function is used to edit and update custome field in personal form
    * @return void
    * 
    */   
    public function addeditextrascustomfieldsave(){
    $this->factory->addeditextrascustomfieldsave();
    }

    /*
    * @description: This function is used to delete custome field in personal form
    * @return void
    * 
    */   


    function deleteextrascustomfield(){
    $this->factory->deleteextrascustomfield();
    }

    /*
    * @description: This function is used for setup breakout  
    * @param: eventId
    * @return: void
    * 
    */ 

    public function setupbreakouts(){
    $this->factory->setupbreakouts();
    }

    /*
    * @description: This function is used to open  add & edit side event popup view
    * @return void
    * 
    */ 

    public function addeditbreakoutview(){
    $this->factory->addeditbreakoutview();
    }

    /*
    * @description: This function is used to open  duplicate breakout view
    * @return void
    * 
    */ 

    public function duplicatebreakoutview(){
    $this->factory->duplicatebreakoutview();
    }

    /*
    * @description: This function is used to add duplicate type of breakout 
    * @return void
    * 
    */ 

    public function duplicatebreakoutsave(){
    $this->factory->duplicatebreakoutsave();
    }

    /*
    * @description: This function is used to add type of breakout 
    * @return void
    * 
    */ 

    public function addeditbreakoutsave(){
    $this->factory->addeditbreakoutsave();
    }

    /*
    * @description: This function is used to delete breakout 
    * @return void
    * 
    */
    public function deletebreakout(){
    $this->factory->deletebreakout();
    }

    /*
    * @description: This function is used to delete breakout session 
    * @return void
    * 
    */
    public function deletebreakoutsession(){
    $this->factory->deletebreakoutsession();
    } 
  
  /*
   * @description: This function is used for setup sponsors  
   * @param: eventId
   * @return: void
   * 
   */ 
  
  public function setupsponsors(){
    $this->factory->setupsponsors();
  }
  
  // ------------------------------------------------------------------------ 
  
   /*
  * @description: This function is used to add exhibitor floor plan
  * @return void
  * 
  */ 
  
  public function addexhibitorfloorplan(){
    $this->factory->addexhibitorfloorplan();
  }
  
  
    // ------------------------------------------------------------------------ 

    /*
    * @description: This function is used to update exhibitor floor plan
    * @return void
    * 
    */ 


    public function updateexhibitorfloorplan(){
    $this->factory->updateexhibitorfloorplan();
    } 

    // ------------------------------------------------------------------------ 

    /*
    * @description: This function is used to delete floor plan document
    * @return void
    */ 

    public function deleteexhibitorfloorplan(){
    $this->factory->deleteexhibitorfloorplan();
    } 

    // ------------------------------------------------------------------------ 

    /*
    * @description: This function is used to open add & edit sponsor popup view
    * @return void
    * 
    */ 

    public function addeditsponsorsview(){
    $this->factory->addeditsponsorsview();
    }

    // ------------------------------------------------------------------------ 

    /*
    * @description: This function is used to add sponsor  
    * @return void
    * 
    */ 

    public function addeditsponsorssave(){
    $this->factory->addeditsponsorssave();
    }

    // ------------------------------------------------------------------------ 

    /*
    * @description: This function is used to open add & edit sponsor additional purchase
    * items popup view
    * @return void
    * 
    */ 

    public function addeditpurchaseitemview(){
    $this->factory->addeditpurchaseitemview();
    } 

    // ------------------------------------------------------------------------ 

    /*
    * @description: This function is used to add sponsor additional purchase
    * items popup view
    * @return void
    * 
    */ 

    public function addeditpurchaseitemsave(){
    $this->factory->addeditpurchaseitemsave();
    }

    // ------------------------------------------------------------------------ 

    /*
    * @description: This function is used to delete purchase item 
    * @return void
    * 
    */   


    public function deletepurchaseitem(){
    $this->factory->deletepurchaseitem();
    }

    // ------------------------------------------------------------------------ 

    /*
    * @description: This function is used to delete sponsors 
    * @return void
    * 
    */   


    public function deletesponsors(){
    $this->factory->deletesponsors();
    }

    // ------------------------------------------------------------------------   

    /*
    * @description: This function is used for setup exhibitors  
    * @param: eventId
    * @return: void
    * 
    */ 

    public function setupexhibitors(){
    $this->factory->setupexhibitors();
    }

    // ------------------------------------------------------------------------ 

    /*
    * @description: This function is used to open add & edit exhibitor popup view
    * @return void
    * 
    */ 

    public function addeditexhibitorsview(){
    $this->factory->addeditexhibitorsview();
    }

    // ------------------------------------------------------------------------ 

    /*
    * @description: This function is used to add exhibitor save
    * @return void
    * 
    */ 

    public function addeditexhibitorsave(){
    $this->factory->addeditexhibitorsave();
    }

    // ------------------------------------------------------------------------ 

    /*
    * @description: This function is used to delete exhibitor 
    * @return void
    * 
    */   

    public function deleteexhibitor(){
    $this->factory->deleteexhibitor();
    }

    // ------------------------------------------------------------------------ 

    /*
    * @description: This function is used to open add & edit exhibitor additional items
    * items popup view
    * @return void
    * 
    */ 

    public function addeditexhibitoritemview(){
    $this->factory->addeditexhibitoritemview();
    } 

    // ------------------------------------------------------------------------ 

    /*
    * @description: This function is used to add exhibitor additional item
    * items popup view
    * @return void
    * 
    */ 

    public function addeditexhibitoritemsave(){
    $this->factory->addeditexhibitoritemsave();
    }

    // ------------------------------------------------------------------------ 

    /*
    * @description: This function is used to delete exhibitor item 
    * @return void
    * 
    */   

    public function deleteexhibitoritem(){
    $this->factory->deleteexhibitoritem();
    }

    // ------------------------------------------------------------------------ 

    /*
    * @description: This function is used for manage payment option  
    * @param: eventId
    * @return: void
    * 
    */ 

    public function setuppayment(){
    $this->factory->setuppayment();
    }

    // ------------------------------------------------------------------------ 

    /*
    * @description: This function is used to open add edit payment option popup view
    * @return void
    * 
    */ 

    public function addeditpaymentoptionview(){
    $this->factory->addeditpaymentoptionview();
    }

    // ------------------------------------------------------------------------ 

    /*
    * @description: This function is used to open add payment option save
    * @return void
    * 
    */ 

    public function addeditpaymentoptionsave(){
    $this->factory->addeditpaymentoptionsave();
    }

    // ------------------------------------------------------------------------ 

    /*
    * @description: This function is used for customize forms
    * @param: eventId
    * @return: void
    * 
    */ 

    public function customizeforms($themeId=''){

    ($this->config->item('accountType')==1) ? $this->event_libraries_factoryfree->customizeforms($themeId) : $this->factory->customizeforms();
    }

    public function customizeformpreview($themeId=''){
    $this->event_libraries_factoryfree->customizeformpreview($themeId);
    }

    // ------------------------------------------------------------------------ 

    /*
    * @description: This function is used to add event header
    * @return void
    * 
    */ 

    public function customizeformsmedia(){
    $this->factory->customizeformsmedia();
    }



    // ------------------------------------------------------------------------ 

    /*
    * @description: This function is used to delete event logo
    * @return void
    */ 

    public function deletecustomizeformslogo(){
    $this->factory->deletecustomizeformslogo();
    } 

    // ------------------------------------------------------------------------ 

    /*
    * @description: This function is used to delete event header
    * @return void
    */ 

    public function deletecustomizeformsheader(){
    $this->factory->deletecustomizeformsheader();
    } 

    // ------------------------------------------------------------------------ 

    /*
    * @description: This function is used for customize forms
    * @param: eventId
    * @return: void
    * 
    */ 

    public function confirmdetails(){
    ($this->config->item('accountType')==1) ? $this->event_libraries_factoryfree->confirmdetails() : $this->factory->confirmdetails();
    }

    // ------------------------------------------------------------------------ 

    /*
    * @description: This function is used to set custome error without any field on sever side
    * @return false
    */

    public function custom_error_set($str){
    $this->form_validation->set_message('custom_error_set', '%s');
    return FALSE;
    }

    // ------------------------------------------------------------------------   

    /*
    * @description: This function is used to get state list by master country id
    * @return master state listing html
    */

    public function masterstatelist(){
    $this->factory->masterstatelist();
    }


    // ------------------------------------------------------------------------   

    /*
    * @description: This function is used to get city list by master state id
    * @return master city listing html
    */

    public function mastercitylist(){
    $this->factory->mastercitylist(); 
    } 


    // ------------------------------------------------------------------------   

    /*
    * @description: This function is used to get state list by country id
    * @return state listing html
    */

    public function statelist(){
    $this->factory->statelist(); 
    }  

    // ------------------------------------------------------------------------   

    /*
    * @description: This function is used to get city list by state id
    * @return city listing html
    */

    public function citylist(){
    $this->factory->citylist(); 
    }  

    // ------------------------------------------------------------------------   

    /*
    * @description: This function is used to get company name list by state id and city id
    * @return company name listing html
    */

    public function companynamelist(){
    $this->factory->companynamelist(); 
    }   

    // ------------------------------------------------------------------------ 

    /*
    * @access:    public
    * @description: This function is used to publish and unpublish 
    * @return void
    * 
    */ 


    public function confirmstatus(){
    ($this->config->item('accountType')==1) ? $this->event_libraries_factoryfree->confirmstatus() : $this->factory->confirmstatus();
    } 

    // ------------------------------------------------------------------------ 

    /*
    * public 
    * @description: This function is used to publish and unpublish 
    * @return void
    * 
    */ 

    public function confirmmsg(){
    $this->factory->confirmmsg(); 
    }

    /********************************** Corporate Section *********************************/

    /*
    * @description: This function is used to show corporate details forms like floor plan,default package,additional purchase plan...
    * @param1: $eventId
    * @return: void
    *  
    */

    public function corporatedetails(){
    $this->factory->corporatedetails(); 
    }

    /*
    * @description: This function is used to show popup for corporate Section
    * @return: void
    *  
    */
    public function corporatepopup(){
    $this->factory->corporatepopup();
    }

    /*
    * @description: This function is used to save Corporate Floor Plan
    * @return: void
    *  
    */

    public function addcorporatefloorplan(){
    $this->factory->addcorporatefloorplan();
    }

    /*
    * @description:  This function is used to update corporate floor plan
    * @return: void
    *  
    */

    public function updatecorporatefloorplan(){
    $this->factory->updatecorporatefloorplan();
    }


    /*
    * @description:  This function is used to delete corporate floor plan
    * @return: void
    *  
    */

    public function deletecorporatefloorplan(){
    $this->factory->deletecorporatefloorplan();
    }


    /*
    * @description: This function is used to add/update corporate Space Type
    * @return: void
    *  
    */

    public function addupdatecorporatespacetype(){
    $this->factory->addupdatecorporatespacetype();
    }  

    /*
    * @description: This function is used to delete corporate space type
    * @return: void
    *  
    */
    public function deletecorporatespacetype(){
    $this->factory->deletecorporatespacetype();
    }


    /*
    * @description: This function is used to delete default package benefit
    * @return: void
    *  
    */
    public function deletedefaultpackagebenefit(){
    $this->factory->deletedefaultpackagebenefit();
    }

    /*
    * @description: This function is used to delete default Purchase item
    * @return: void
    *  
    */
    public function deletedefaultpurchaseitem(){
    $this->factory->deletedefaultpurchaseitem();
    }

    /*
    * @description: This function is used to delete corporate package
    * @return: void
    *  
    */
    public function deletecorporatepackage(){
    $this->factory->deletecorporatepackage();
    }


    /*
    * @description: This function is used to get Allocate Reserved Booths
    * @return: void
    *  
    */
    public function getfloorplantype(){
    $this->factory->getfloorplantype();
    }


    /*
    * @description: This function is used to view program details
    * @return: void
    *  
    */
    public function programdetails(){
    $this->factory->programdetails();
    }


    /*
    * @description: This function is used to load popup section for program
    * @return: void
    *  
    */
    public function programpopup(){
    $this->factory->programpopup();
    }


    /*
    * @description: This function is used to delete perticular program 
    * @return: void
    *  
    */
    public function deleteprogrampresenter(){
    $this->factory->deleteprogrampresenter();
    }

    public function getstreamshtml(){
    $this->factory->getstreamshtml();
    }

    // ------------------------------------------------------------------------ 

    /*
    * @description: This function is used to delete payment option
    * @return void
    * 
    */ 

    public function deletepaymentoption(){
    $this->factory->deletepaymentoption();
    }

    /*
    * @description: This function is used to to open popup of event category 
    * @return  : @void
    */ 
    public function eventcustomsocialmediapopup(){
    $this->factory->eventcustomsocialmediapopup();
    }

    /*
    * @description: This function is used to to open popup of event category 
    * @return  : @void
    */ 
    public function saveCustomeSocialMediaType(){
    $this->factory->insertupdateeventcustomsocialmedia();
    }

    /*
    * @description: This function is used to delete payment option
    * @return void
    * 
    */ 

    public function deleteCustomeSocialMediaType(){
    $this->factory->deleteCustomeSocialMediaType();
    }


    /************** Setup for free account ***************/

    /*
    * @description: This function is used to show event details forms like gerneral event, contact person
    * @param1: $eventId
    * @return: void
    *  
    */

    public function invitations(){	
    $this->event_libraries_factoryfree->invitations();
    }

    /*
    * @description: This function is used to add type of registration 
    * @return void
    * 
    */ 

    public function addediteventInvitation(){
    $this->event_libraries_factoryfree->addediteventInvitation(); 
    }


    public function add_edit_custom_field_free(){
    $this->event_libraries_factoryfree->addeditcustomfield(); 
    }
    /*
    * @description: This function is used to edit and update custome field in personal form
    * @return void
    * 
    */ 

    public function addeditcustomefieldsavefree(){
    $this->event_libraries_factoryfree->addeditcustomefieldsave();
    } 

    /*
    * @description: This function is used to delete type of registration 
    * @return void
    * 
    */   

    public function deleteeventinvitation(){
    $this->event_libraries_factoryfree->deleteeventinvitation();
    } 

    /*
    * @description: This function is used to duplicate type of registration 
    * @return void
    * 
    */ 

    public function duplicateeventinvitation(){
    $this->event_libraries_factoryfree->duplicateeventinvitation(); 
    }

    /*
    * @description: This function is used to  delete event
    * @param : eventId
    * 
    */
  
    public function deletesingleevent(){
    $this->event_libraries_factoryfree->deleteevent();  
    }

    //save selected theme data
    public function addediteventtheme(){
    $this->event_libraries_factoryfree->addediteventtheme();  
    }

    public function add_edit_custom_theme_popup(){
    $this->event_libraries_factoryfree->add_edit_custom_theme_popup();  
    }

    public function use_custom_theme_apply(){
    $this->event_libraries_factoryfree->use_custom_theme_apply();  
    }

    public function addeventdietaryfree(){
    $this->event_libraries_factoryfree->addeventdietaryfree();
    }

    public function saveCanvasObject(){
    $this->event_libraries_factoryfree->saveCanvasObject();
    }

    public function customizeformsmediafree(){
    $this->event_libraries_factoryfree->addediteventtheme();  
    }

    public function publishEventAction(){
    $this->event_libraries_factoryfree->publishEventAction();  
    }
    
    public function cancelEventAction(){
    $this->event_libraries_factoryfree->cancelEventAction();  
    }
  
  
}

/* End of file event.php */
/* Location: ./system/application/modules/event/controllers/event.php */
?>
