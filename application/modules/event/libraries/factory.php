<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 *                                                  
 * This class is used to manage for event setup controller manage
 *
 * @package   event manage
 * @author    Lokendra Meena
 * @email     lokendrameena@cdnsol.com
 * @since     Version 1.0
 * @filesource
 */

class Factory{
   
  //set limit of show records 
  private $limit = '10';  
  private $ci = NULL;  
  
  function __construct($ci){
    //create instance object
    $this->ci = $ci;
  }
   // ------------------------------------------------------------------------ 
  
  /* @access:    public
   * @description: This function is used to save add event data
   * @return     : @void
   */ 
   
  public function addevent(){
    
    //call add event validate
    $this->_addEventValidate();
    
    if ($this->ci->form_validation->run($this->ci))
    {
      //call function for save data
      $eventId = $this->_createEventSave();
    
      //This member function is call to insert all required value for this event
      $this->eventrequiredatainsert($eventId);
      $redirectUrl = 'event/eventdetails/'.encode($eventId);
      
      $msg = lang('event_msg_event_create_successfully');
      $msgArray=array('msg'=>$msg,'is_success'=>'true','url'=>$redirectUrl);
      //set_global_messages($msgArray);
      echo json_encode($msgArray);
    }
    else
    {
      $errors = $this->ci->form_validation->error_array();
      echo json_message('msg',$errors,'is_success','false');
    }
        
  } 
  
  
  
  //----------------------------------- Event Setup Section ----------------------------------------------
  
  // ------------------------------------------------------------------------ 

    /*
    * @access:    public
    * @description: This function is used to show event details forms like gerneral event, contact person
    * @param1:    $eventId
    * @return:    void
    *  
    */
     
  public function eventdetails($eventId="0"){
    if(!empty($eventId)){
      $this->ci->session->set_userdata('eventId',$eventId);
    }elseif(currentEventId()){
      //current open eventId
      $eventId = currentEventId();
    }
    
    //decode entered eventid
    $eventId = decode($eventId); 
    
    //check is data post 
    if($this->ci->input->post()){
      
      // Store Collapse Value in Session to open the next form
      $this->ci->session->set_userdata('openCollapseValue',$this->ci->input->post('collapseValue'));
      $actionName = $this->ci->input->post('formActionName');
      switch($actionName){
        //update general event setup form data
        case "generalEventSetup":
          $this->updategeneraleventsetup();
        break;
        
        //insert & update general contact person form data
        case "contactPerson":
          $this->insertupdatecontactperson();
        break;
        
        //insert & update custom contact person form data
        case "eventCustomContactSave":
          $this->insertupdatecustomcontactperson();
        break;
        
        //insert & update general contact person form data
        case "eventSocialMedia":
          $this->insertupdateeventSocialMedia();
        break;
        
        //insert & update custom contact person form data
        case "eventCustomSocialMediaSave":
          $this->insertupdateeventcustomsocialmedia();
        break;
        
        //insert & update general bank details form data
        case "eventLostPassword":
          $this->insertupdatelostpassworddetails();
        break;
        
        //insert & update general bank details form data
        case "formEventEmail":
          $this->addupdateeventemail();
        break;
        
        //insert & update general invoice details form data
        case "eventInvoiceDetails":
          $this->insertupdateinvoicedetails();
        break;
        
          //insert & update general invoice details form data
        case "formEventGroupBooking":
          $this->insertupdategroupbooking();
        break;
          //insert & update event category details form data
        case "eventCategory":
          $this->addupdateeventcategory();
        break;
        
        //insert & update general bank details form data
        case "eventBankDetails":
          $this->insertupdatebankdetails();
        break;
        
        //insert & update general term & condition form data
        case "eventTermsCondition":
          $this->insertupdatetermscondition();
        break;
        
        default:
        // load event form view
        $this->eventshowview($eventId);
      }
    }else{
      // load event details form view
      $this->eventshowview($eventId);
    }
  }

  
  /* @access:    public
   * @description: This function is used to get event custom contact list
   * @return     : @void
   */ 
  public function geteventcustomcontact(){ 
     $contactPersionId =  $this->ci->uri->segment(3);
     if(!empty($contactPersionId)){
     $this->ci->load->view('custom_contact_list',array('contactPersionId'=>$contactPersionId));  
     }
  }
  
  // ------------------------------------------------------------------------ 
  
  /* @access:    public
   * @description: This function is used to open popup of custom contact type
   * @return     : @void
   */ 
  public function eventcustomcontactpopup(){ 
     // check id exit or not
    
     $eventContactType  = trim($this->ci->input->post('contactType'));  
     $eventContactDetails = trim($this->ci->input->post('contactDetails')); 
     $data = array('contact_type'=>$eventContactType,'details'=>$eventContactDetails);
     /*if(!empty($eventCustomContactId) && $eventCustomContactId!=FALSE){
      // get details of this custom contact
      $result = $this->ci->event_model->geteventcustomcontact(array('custom_contact_id '=>$eventCustomContactId));  
      if(!empty($result)){
        $data['contact_type'] = $result->contact_type;
        $data['details']    = $result->details;
      }
     }*/
     $this->ci->load->view('eventdetails/event_custom_contact_type',$data);
  }
  
  
  /* @access:    public
   * @description: This function is used to open popup of custom emails
   * @return     : @void
   */ 
  public function eventcustomemailpopup(){ 
     
     $data['emailDetails'] = "";
     //current open eventId
     $data['eventId']    = currentEventId();   
     $eventCustomEmailId = $this->ci->input->post('emailId'); 
     if(!empty($eventCustomEmailId) && $eventCustomEmailId!=FALSE){
      // get details of this custom contact
      $result = $this->ci->event_model->geteventcustomemaildetails(array('email_id'=>decode($eventCustomEmailId)));  
      if(!empty($result)){
        $data['emailDetails'] = $result;
        //$data['SYSTEMIMAGE']  = base_url('themes');
      }
     } 
    $this->ci->load->view('event_email_popup',$data);
  }
  
  
  /* @access:    public
   * @description: This function is used to  open popup of event category popup
   * @return     : @void
   */ 
  public function eventcategorypopup(){
    
    $eventCategoryId = $this->ci->input->post('categoryId');
    $data = array('category'=>"",'is_corporate'=>"1",'catId'=>"");
    if(!empty($eventCategoryId)){
      $eventCategorydata = $this->ci->event_model->geteventCategories(0,$eventCategoryId);
      if(!empty($eventCategorydata[0])){
        $data['category']   = $eventCategorydata[0]->category;
        $data['is_corporate'] = $eventCategorydata[0]->is_corporate;
        $data['catId']    = $eventCategorydata[0]->category_id;
      }
    }
    $this->ci->load->view('eventdetails/event_category_popup',$data);
  }
  
  
  // Upload attachment 
  public function addupdateeventemailattach(){  
    
    $dirUploadPath         = './media/event_email_attachment/';
    $dirSavePath         = '/media/event_email_attachment/';
    $uploadRecord          = $this->ci->process_upload->upload_file($dirUploadPath);
    
    $updateData['document_file'] = $uploadRecord['filename'];
    $updateData['document_path'] = $dirSavePath;
    $updateData['email_id']    = $this->ci->input->post('tableInsertedId');
    
    // insert event email attachment
    $attachmentId = $this->ci->event_model->addeventemailattachment($updateData);
    $fileId     = remove_extension($uploadRecord['filename']);
    echo json_encode(array('id'=>$attachmentId,'fileId'=>$fileId));
    
  } 
    
  /*
   * @access: public
   * @description: This function is used to delete email attachment
   * @return void
   * 
  */ 
  public function deleteemailachment(){
    $deleteId = $this->ci->input->post('deleteId');
    if(!empty($deleteId)){
    $result=$this->ci->common_model->deleteRow('event_email_attachments',array('attachment_id'=>$deleteId));
    echo json_message('msg',"Email attachment deleted successfully!",'is_success','true');  
    }else{
    echo json_message('msg',"There was an error, Please try again!",'is_success','false');  
    }
  }
  
  
  
  /* @access:    public
   * @description: This function is used to delete email
   * @return     : @void
  */ 
  public function deleteeventemail(){
    $eventEmailId = $this->ci->input->post('deleteId');
    $result=$this->ci->common_model->deleteRow('event_emails',array('email_id'=>decode($eventEmailId)));
    echo json_message('msg',"Custom Email deleted successfully!",'is_success','true');  
  }
  
  
  // ---------------------------------- Program Section -----------------------------------------------
  
  
  
  /*
  * @description: This function is used to show popup for Program section
  * @return void
  * 
    */ 
   
    public function programpopup(){
     
      $actionName       = $this->ci->input->post('popupType');
      $eventId        = $this->ci->input->post('eventId');
           
      switch($actionName){
      
      case "programPresenter":
        $this->_programpresenterpopup($eventId);
      break;
      case "addBenefitPopup":
        $this->_addbenefitpopup();
        break;    
      case "addDefaultPurchasePopup" : 
        $this->_addbenefitdefaultpurchasepopup();
        break;
        case "addCustomFieldPopup":
        $this->_addcustomfieldpopup();
        break;
         case "addCorporatePackagePopup":
        $this->_addcorporatepackagepopup();
        break;
          
      default : 
        echo false;
      break;
    
    }
  }
  
  
  /*
  * @description: This function is used to load presenter popup
  * @return void
  * 
    */ 
    private function _programpresenterpopup($eventId="0"){
    
      // get posted value
      $data['event_id']  = $eventId;  
      if($this->ci->input->post('presenterId')!='0'){
        $data['id'] = $this->ci->input->post('presenterId');  
        // get program presenter details
        $presenterDetails    = $this->ci->common_model->getDataFromTabel('program_presenter','id,title,firstname,lastname',$data); 
        $data['presenterDetails'] = (!empty($presenterDetails)) ? $presenterDetails[0] : "";
        $data['isPresenterCopy']  = $this->ci->input->post('isPresenterCopy');  
      }else{
        $data['id']         = "";
        $data['presenterDetails'] = "";
        $data['isPresenterCopy']  = "";
      }
      
      $this->ci->load->view('program/popup_add_presenter',$data);
  }
  
  /*
    * @access:    public
    * @description: This function is used to show program details forms like add presenter,Program block
    * @param1:    $eventId
    * @return:    void
    *  
    */
  public function programdetails(){
    
    // current open eventId
    $eventId = currentEventId();
  
    //decode entered eventid
    $eventId = decode($eventId); 
    
    //check is data post 
    if($this->ci->input->post()){
      
      // Store Collapse Value in Session to open the next form
      //$this->ci->session->set_userdata('openCollapseValue',$this->ci->input->post('collapseValue'));
      $actionName = $this->ci->input->post('formActionName');
      switch($actionName){
        //add/Update program presenter data
        case "programAddPresenter":
        $this->addupdateprogrampresenterdata($eventId);
        break;
        case "programPresenterDetailSave":
        $this->updateprogrampresenterDetails($eventId);
        break;
        case "programBlockSave":
        //call insert and update data function
        $this->insertupdatebreakouttypes();
        break;
        default:
        // load event form view
        $this->programdetailsview($eventId);
      }
    }else{
      // load event details form view
      $this->programdetailsview($eventId);
    }
  }
  
  /*
    * @access:    public
    * @description: This function is used to delete program presenter
    * @param1:    void
    * @return:    boolean
    *  
    */
  public function deleteprogrampresenter(){
    $programId = $this->ci->input->post('deleteId');
    $result=$this->ci->common_model->deleteRow('program_presenter',array('id'=>$programId));
    if($result){
      echo json_message('msg',lang('program_presenter_deleted_succ'),'is_success','true'); 
    }else{
      echo json_message('msg',lang('comm_error'),'is_success','false'); 
    }  
  }
  
  /*
  * @access: private 
    * @description: This function is used to load event details view
    * @param1: $eventId
    * @return: void
    *  
    */
  
  private function programdetailsview($eventId="0"){
      
    //chheck if eventId is zero 
    if($eventId==0){
      redirect(base_url('dashboard'));
    }
    $userId = isLoginUser();
    $data['userId'] = $userId;
    $data['eventId'] =$eventId;
    $data['eventdetails'] = $this->ci->event_model->geteventdetails($eventId,$userId);
    $data['popupHeaderBg'] ='bg_0073ae'; // popup header bg class
    $data['popupButtonBg'] ='custombtn_save'; // popup button bg class 
    
    //if empty then redirect
    if(empty($data['eventdetails'])){
      redirect(base_url('dashboard'));
    }
    
    $where = array('event_id'=>$eventId);
    
    // get program presenter details
    $data['presenterDetails']    = $this->ci->common_model->getDataFromTabel('program_presenter','*',$where); 
    
    $data['eventbreakouts']      = $this->ci->common_model->getDataFromTabel('breakouts','*',$where,'','id','ASC');
    
    // get all registrant category with its limit
    $data['registrantCategory']  =  $this->ci->event_model->getRegistrantCategory($eventId); 
    
    // get all Registrant type 
    $data['registrantType']    =  $this->ci->common_model->getDataFromTabel('event_registrants','id,registrant_name',$where); 
        
    $this->ci->template->load('template','program/program_details',$data,TRUE);
    
  }
  
  
  
  /*
  * @access: private 
    * @description: This function is used to add or update presenter
    * @param1: $eventId
    * @return: void
    *  
    */
  
  private function addupdateprogrampresenterdata($eventId="0"){
      
    $this->_addUpdatePresenterValidate();
    if($this->ci->form_validation->run($this->ci)){
      $this->_savepresenterdata($eventId);
    }else{
      $errors = $this->ci->form_validation->error_array();
      echo json_message('msg',$errors,'is_success','false');
    }
      
  }
  
  /*
  * @access: private 
    * @description: This function is used to validate presenter data
    * @param1: $eventId
    * @return: void
    *  
    */
  
  private function _addUpdatePresenterValidate($eventId="0"){
    
    // set ruls for form filed
    $this->ci->form_validation->set_rules('presenter_title', 'Presenter Title', 'trim|required');
    $this->ci->form_validation->set_rules('presenter_fname', 'First Name', 'trim|required');
    $this->ci->form_validation->set_rules('presenter_lname', 'Last Name', 'trim|required');
        
  }
  
  /*
  * @access: private 
    * @description: This function is used to save presenter data
    * @param1: $eventId
    * @return: void
    *  
    */
  
  private function _savepresenterdata($eventId="0"){
    
    $insertData['event_id']   = $eventId;
    $insertData['title']    = trim($this->ci->input->post('presenter_title'));
    $insertData['firstname']    = trim($this->ci->input->post('presenter_fname'));
    $insertData['lastname']   = trim($this->ci->input->post('presenter_lname'));
    $insertData['lastname']   = trim($this->ci->input->post('presenter_lname'));
    
    $isPresenterCopy      = trim($this->ci->input->post('is_presenter_copy'));
    $presenterId        = trim($this->ci->input->post('presenter_id'));
    
    if(!empty($presenterId) && $isPresenterCopy=='0'){ // update presenter
        $this->ci->common_model->updateDataFromTabel('program_presenter', $insertData,array('event_id'=>$eventId,'id'=>$presenterId));
        $msg = lang('program_presenter_updated_succ');
        echo json_encode(array('msg'=>$msg,'is_success'=>'true','url'=>'event/programdetails'));
    }else{ // add new presenter
      $presenterId = $this->ci->common_model->addDataIntoTabel('program_presenter', $insertData);
      if($presenterId){
        $msg =  ($isPresenterCopy=='1') ? lang('program_presenter_copy_succ') : lang('program_presenter_add_succ');
        echo json_encode(array('msg'=>$msg,'is_success'=>'true','url'=>'event/programdetails'));
      }else{
        $errors = lang('comm_error');
        echo json_message('msg',$errors,'is_success','false');
      }
    }
    
    
  }
  
  
  /*
  * @access: private 
    * @description: This function is used to update presenter data
    * @param1: $eventId
    * @return: void
    *  
    */
  
  private function updateprogrampresenterDetails($eventId="0"){
    $this->_updateprogrampresenterDetailsValidate();
    if($this->ci->form_validation->run($this->ci)){
      $this->_savepresenterdeatils($eventId);
    }else{
      $errors = $this->ci->form_validation->error_array();
      echo json_message('msg',$errors,'is_success','false');
    }
  }
  
  /*
  * @access: private 
    * @description: This function is used to validate presenter data
    * @param1: $eventId
    * @return: void
    *  
    */
  
  private function _updateprogrampresenterDetailsValidate($eventId="0"){
    // set ruls for form filed
    
    $presenterId = $this->ci->input->post('presenterId');
    
    $presenterEmail = 'presenterEmail'.$presenterId;
    
    /*$this->ci->form_validation->set_rules('presenterOrganisation'.$presenterId, 'organisation', 'trim|required');
    $this->ci->form_validation->set_rules('presenterEmail'.$presenterId, 'email ', 'trim|required');
    $this->ci->form_validation->set_rules('presenterEmailConfirm'.$presenterId, 'confirm email', 'trim|required|email|matches['.$presenterEmail.']');
    $this->ci->form_validation->set_rules('presenterPhone'.$presenterId, 'phone', 'trim|required');
    $this->ci->form_validation->set_rules('presenterTopic'.$presenterId, 'topic', 'trim|required');*/
    $this->ci->form_validation->set_rules('presenterRegRequired'.$presenterId, 'registration required', 'trim|required');
    
    $presenterRequired = $this->ci->input->post('presenterRegRequired'.$presenterId);
    if($presenterRequired=='1'){
      $this->ci->form_validation->set_rules('registrantType'.$presenterId, 'registration type', 'trim|required');
    }
    
  }
  
  
  /*
  * @access: private 
    * @description: This function is used to save presenter data
    * @param1: $eventId
    * @return: void
    *  
    */
  private function _savepresenterdeatils($eventId="0"){
    
    $presenterId                            = $this->ci->input->post('presenterId');
    $isRegistrationReg                      = $this->ci->input->post('presenterRegRequired'.$presenterId);
    $updateData['organisation']             = $this->ci->input->post('presenterOrganisation'.$presenterId);
    $updateData['email']                    = $this->ci->input->post('presenterEmail'.$presenterId);
    $updateData['mobile']                   = $this->ci->input->post('presenterPhone'.$presenterId);//$this->ci->input->post('input_hidden_mobile'.$presenterId);
    //$updateData['phone']                    = $this->ci->input->post('input_hidden_landline'.$presenterId);
    $updateData['topic']                    = $this->ci->input->post('presenterTopic'.$presenterId);
    $updateData['presentation_description'] = $this->ci->input->post('presenterDescription'.$presenterId);
    $updateData['is_registration_required'] = $isRegistrationReg;
    $updateData['registration_type']        = ($isRegistrationReg=='1') ? $this->ci->input->post('registrantType'.$presenterId) : "";
    $updateData['fbid']                     = $this->ci->input->post('contactFb');
    $updateData['twitterid']                = $this->ci->input->post('contactTwitter');
    $updateData['linkedin']                 = $this->ci->input->post('contactLinkedin');
    $updateData['gplus']                    = $this->ci->input->post('contactGplus');
    $updateData['pintrest']                 = $this->ci->input->post('contactPintrest');
    
    $where      = array('id'=>$presenterId,'event_id'=>$eventId);
    $presenterId  = $this->ci->common_model->updateDataFromTabel('program_presenter', $updateData,$where);
    if($presenterId){
      $msg = lang('program_presenter_updated_succ');
      echo json_encode(array('msg'=>$msg,'is_success'=>'true','url'=>'event/programdetails'));
    }else{
      $errors = lang('comm_error');
      echo json_message('msg',$errors,'is_success','false');
    }
  }
  
  
  public function getstreamshtml(){
    
    $data['sessionVal']     = $this->ci->input->post('getValueSession');
    $data['limit']      = $this->ci->input->post('streamLimit');
    $data['programBlockId']   = $this->ci->input->post('programBlockId');
    
    $this->ci->load->view('program/program_stream_html',$data);
        
  }
  
  //----------------------------------- Corporate Section ----------------------------------------------
  
  
    /*
   * @description: This function is used to show details of corporate section
   * @return void
   * 
    */ 
    
  public function corporatedetails(){
    
    $eventId = currentEventId();
  
    if(!empty($eventId)){
      //decode entered eventid
      $eventId = decode($eventId); 
      
      
      //check is data post 
      if($this->ci->input->post()){
        
        // Store Collapse Value in Session to open the next form
        $this->ci->session->set_userdata('openCollapseValue',$this->ci->input->post('collapseValue'));
        
        $actionName = $this->ci->input->post('formActionName');
        switch($actionName){
          
          case "corporateFloorPlan":
            $this->addcorporatefloorplan($eventId);
          break;
          case "corporateSpaceType":
            $this->addupdatecorporatespacetype($eventId);
          break;
          case "deafultPackageBenefit":
            $this->savedeafultPackageBenefit($eventId);
          break;
          case "deafultAdditionalPurchaseItem":
            $this->savedeafultAdditionalPurchaseItem($eventId);
          break;
          case "corporateDefaultBenefit":
            $this->insertupdatecorporatedefaultbenefit($eventId);
          break;
          case "corporateDefaultPackageBenefit":
            $this->insertupdatedefaultPackageBenefit($eventId);
          break;
          case "corporateDefaultPurchaseItem":
              $this->insertupdatedefaultpurchaseitem($eventId);
          break;    
          case "masterPersonalDetails":
            $this->addeditcustomefieldsave();
          break; 
          case "formCorporatePackage":  
            $this->addeditcorporatepackage($eventId);
          break;
          case "corporatePackageDetailsSave": 
            $this->addeditcorporatepackageDetails($eventId);
          break;  
          case "corporateApplicationDetails": 
            $this->corporateApplicationDetails($eventId);
          break;
          
          default:
          // load event form view
          $this->corporateshowview($eventId);
        }
      }else{
        // load event details form view
        $this->corporateshowview($eventId);
      }
    }else{
      // redirect to dashboard
      redirect(base_url('dashboard'));
    }
    
  }
  
  
     
  // ------------------------------------------------------------------------ 
  
  /* @access:    public
   * @description: This function is used to add new event category
   * @return     : @void
   */
  public function deleteeventcategory(){
    
    $eventCategoryId = $this->ci->input->post('deleteId');
    $result=$this->ci->common_model->deleteRow('event_categories',array('category_id'=>$eventCategoryId));
    echo json_message('msg',lang('event_category_deleted_succ'),'is_success','true');  
  } 
  
  
  // ------------------------------------------------------------------------ 
   
  /* (Still this method are not using)
   * @access: public
   * @description: This function is used to delete term and codition document
   * @return void
   */ 
   
  public function deletetermattachmentterm(){
    
    $termId            = $this->ci->input->post('deleteId');
    $updateData['document_file'] = '';
    $updateData['document_path'] = '';
    $where               = array('id'=>$termId);
    
    //check record is exist 
    $getResult = $this->ci->common_model->getDataFromTabel('event_term_conditions','document_file,document_path',$where);
    if(!empty($getResult)){
      //delte image from directory
      findFileNDelete($getResult[0]->document_path, $getResult[0]->document_file);
    }
    
    $this->ci->common_model->updateDataFromTabel('event_term_conditions',$updateData,$where);
    echo json_message('msg','Term condition document deleted.');
  }
  
  /*
   * @access:  public
   * @description: This function is used to add and edit perosnal details form custom field
   * @return void
   * 
   */ 
  
  public function addeditcustomfield(){
    $data['eventId']      = $this->ci->input->post('eventId');
    $data['registrantidval']  = $this->ci->input->post('registrantid');
    $customfieldid        = $this->ci->input->post('customfieldid');
    $data['formId']         = $this->ci->input->post('formId');
    $data['formAction']       = $this->ci->input->post('formAction');
    $data['parentFormId']       = $this->ci->input->post('parentFormId');
    $data['customfieldidval'] = $customfieldid;
    $where            = array('id'=>$customfieldid);
    //get data form custome form field table
    $data['eventregistrants'] = $this->ci->common_model->getDataFromTabel('custom_form_fields','*',$where,'','id','ASC','1');
    if(!empty($data['eventregistrants'])){
      $data['eventregistrants'] = $data['eventregistrants'][0];
    }

    $this->ci->load->view('registration/add_edit_personal_form_field_popup',$data); 
  }
  
  
  // ------------------------------------------------------------------------ 
  
  /*
   * @access:  public
   * @description: This function is used to add type of registration 
   * @return void
   * 
   */ 
   
  public function addediteventregistration(){
    
    //set validation
    $this->ci->form_validation->set_rules('registrationName', 'registration type', 'trim|required');
    $this->ci->form_validation->set_rules('registrantType', 'Category', 'trim|required');
    
    if($this->ci->form_validation->run($this->ci)){
      $eventId                = $this->ci->input->post('eventId');
      $registrationName             =   $this->ci->input->post('registrationName');
      $registrationCategory           =   $this->ci->input->post('registrantType');
      $registrationTypeId           =   $this->ci->input->post('registrationTypeId');
      $registrantType             =   $this->ci->input->post('registrantType');
      $insertData['registrant_name']      =   $registrationName;
      $insertData['category_id']        =   $registrationCategory;
      $insertData['event_id']         =   $eventId;
      $insertData['registrants_type']     =   $registrantType;
      $insertData['registrant_number']      =   getMasterNumber('registrant',$eventId);
      
      if($registrationTypeId > 0){
        //udpate registrant title
        $where = array('id'=>$registrationTypeId);
        $this->ci->common_model->updateDataFromTabel('event_registrants',$insertData,$where);
        $msg =  lang('event_msg_type_of_registration_edit_successfully');
      }else{
        //registrant type add
        $registrantId = $this->ci->common_model->addDataIntoTabel('event_registrants', $insertData);
        $msg = lang('event_msg_type_of_registration_add_successfully');
        //This function is used to add form fields 
        $this->addformfields($eventId,'1',$registrantId);
      }
      
      $returnArray=array('msg'=>$msg,'is_success'=>'true');
      //set_global_messages($returnArray);
      echo json_encode($returnArray);
    }else{
      $errors = $this->ci->form_validation->error_array();
      echo json_message('msg',$errors,'is_success','false');
    }
  }
  
  
  
  // ------------------------------------------------------------------------ 
  
  /*
   * @access:    public
   * @description: This function is used to duplicate type of registration 
   * @return void
   * 
   */ 
   
  public function duplicateeventregistration(){
    
    if($this->ci->input->post()){
      $registrationName   = $this->ci->input->post('registrationName');
      $registrationTypeId = $this->ci->input->post('registrationTypeId');
      $eventId      = $this->ci->input->post('eventId');
      
      //get data form registrants
      $where = array('id'=>$registrationTypeId);
      $getRegistrantData = $this->ci->common_model->getDataFromTabel('event_registrants','*',$where);
        
      //registrant daywise data
      $daywiseWhere         = array('registrant_id'=>$registrationTypeId);
      $getRegistrantDaywiseData   = $this->ci->common_model->getDataFromTabel('event_registrant_daywise','*',$daywiseWhere);
      
      if(!empty($getRegistrantData)){
        $getRegistrantData = $getRegistrantData[0];
        foreach($getRegistrantData as $key => $value){
          if($key=="registrant_name"){
            $insertData[$key] =  $registrationName; 
          }else{
            $insertData[$key] =  $value; 
          }
        }
        
        //unset the key of regstrant
        unset($insertData['id']);
        unset($insertData['registrants_limit']);
        unset($insertData['registrants_order']);
        unset($insertData['registrant_number']);
        
        //add registrant number 
        $insertData['registrant_number']      =   getMasterNumber('registrant',$eventId);
        
        //registrant type add
        $registrantId = $this->ci->common_model->addDataIntoTabel('event_registrants', $insertData);
        
        if(!empty($getRegistrantDaywiseData)){
          foreach($getRegistrantDaywiseData as $getRegistrantDaywiseDataArr){
            foreach($getRegistrantDaywiseDataArr as $key => $value){
              $insertDaywiseData[$key] =  $value;
            }
            $insertDaywiseData['registrant_id'] = $registrantId;
            unset($insertDaywiseData['id']);
            unset($insertDaywiseData['registrant_daywise_limit']);
            //add data in regitrant daywise table
            $this->ci->common_model->addDataIntoTabel('event_registrant_daywise', $insertDaywiseData);
            unset($insertDaywiseData);
          }
        }
      }
      
      $msg = lang('event_msg_type_registration_duplicate_created');
      //This function is used to add form fields 
      $this->addformfields($eventId,'1',$registrantId);
      
      $returnArray=array('msg'=>$msg,'is_success'=>'true');
      //set_global_messages($returnArray);
      echo json_encode($returnArray);
    }else{
      redirect(base_url('dashboard'));
    }
  }
  
  
  
  
  
  // ------------------------------------------------------------------------ 
  
  /*
   * @access:    public
   * @description: This function is used to delete type of registration 
   * @return void
   * 
   */   
  
  
  public function deleteeventregistration(){
    
    $deleteid   = $this->ci->input->post('deleteId');
    //delete registant record from other table
    $where    = array('registrant_id'=>$deleteid);
    $this->ci->common_model->deleteRowFromTabel('custom_form_fields',$where);
    
    //delete registrant from table
    $where = array('id'=>$deleteid);
    $this->ci->common_model->deleteRowFromTabel('event_registrants',$where);
    $msg = lang('event_msg_type_registration_deleted');
    $returnArray=array('msg'=>$msg,'is_success'=>'true');
    //set_global_messages($returnArray);
    echo json_encode($returnArray);
  }
  
  // ------------------------------------------------------------------------  
   
  /*
   * @access: public
   * @description: This function is used to upload  sponsors logo
   * @param : upload path
   * @return void
   * 
   */ 
   
  public function uploadtermattachmentterm(){
    
    $dirUploadPath         = './media/event_attachment/';
    $dirSavePath         = '/media/event_attachment/';
    $uploadData            = $this->ci->process_upload->upload_file($dirUploadPath);
    $eventId           = $_REQUEST['event_id'];
    $updateData['document_file'] = $uploadData['filename'];
    $updateData['document_path'] = $dirSavePath;
    $fileId            = remove_extension($uploadData['filename']);
    // get document id by event id
    $where = array('event_id'=>$eventId);
    $getResult = $this->ci->common_model->getDataFromTabel('event_term_conditions','id',$where);
    // update document id by id
    $termId = $getResult[0]->id;
    $upWhere = array('id'=>$termId);
    $this->ci->common_model->updateDataFromTabel('event_term_conditions',$updateData,$upWhere);
    echo json_encode(array('id'=>$termId,'fileId'=>$fileId));
  } 
  
  // ------------------------------------------------------------------------ 
  
  /*
   * @access:    public
   * @description: This function is used to delete custome field in personal form
   * @return void
   * 
   */   
  
  public function deleteeditcustomefield(){
    
    $deleteid = $this->ci->input->post('deleteId');
    //delete registant record from other table
    $where = array('id'=>$deleteid);
    $this->ci->common_model->deleteRowFromTabel('custom_form_fields',$where);
    $msg = lang('event_msg_custom_field_deleted');
    $returnArray=array('msg'=>$msg,'is_success'=>'true');
    set_global_messages($returnArray);
    echo json_encode($returnArray);
  }
  
  
  // ------------------------------------------------------------------------ 
  
  /*
   * @access:    public
   * @description: This function is used to add dietary 
   * @return void
   * 
   */ 
   
  public function addeventdietary(){
    
    $this->ci->form_validation->set_rules('dietaryName', 'dietary name', 'trim|required');
    
    if($this->ci->input->post()){
    
      if ($this->ci->form_validation->run($this->ci))
      { 
        $eventId           = $this->ci->input->post('eventId');
        $dietaryName         = $this->ci->input->post('dietaryName');
        $dataArray['dietary_name']   = $dietaryName;
        $dataArray['event_id']     = $eventId;
        $dataArray['is_undeletable'] = '0';
        
        //get deitary requirement data and order of dietary
        $whereCondi = array('event_id'=>$eventId);
        $dietaryOrder = 0;
        $eventdietary = $this->ci->common_model->getDataFromTabel('event_dietary','id,dietary_order',$whereCondi,'','dietary_order','DESC','1');
        if(!empty($eventdietary)){
          $eventdietary = $eventdietary[0];
          $dietaryOrder = $eventdietary->dietary_order;
          $dietaryId = $eventdietary->id;
          
          //update deitary order for making it last order
          $updateData['dietary_order'] = $dietaryOrder+1;
          $whereUp = array('id'=>$dietaryId);
          $this->ci->common_model->updateDataFromTabel('event_dietary',$updateData,$whereUp);
        }
        $dataArray['dietary_order'] = $dietaryOrder;
        
        //call member function for add dietary
        $insertId = $this->adddietarydata($dataArray);
        $msg = lang('event_msg_dietary_add_successfully');
        $returnArray=array('msg'=>$msg,'id'=>$insertId,'is_success'=>'true'); 
        echo json_encode($returnArray);
      }else{
        $errors = $this->ci->form_validation->error_array();
        echo json_message('msg',$errors,'is_success','false');
        
      }
    }else{
      redirect(base_url('dashboard'));
    }
  }
    
  
   /*
     * @access: public
     * @description: This function is used to  delete event
     * @param : eventId
     * 
     */
    
    public function deleteevent($eventId){
    
    //decode entered eventId
    $eventId = decode($eventId);
    
    //delete data form contact person
    $whereCondi = array('event_id'=>$eventId);
    $this->ci->common_model->deleteRowFromTabel('event_contact_person',$whereCondi);
    
    //delete data form event invoice 
    $this->ci->common_model->deleteRowFromTabel('event_invoice',$whereCondi);
    
    //delete data form event bank details
    $this->ci->common_model->deleteRowFromTabel('event_bank_detail',$whereCondi);
    
    //delete data form event term condition and its attachment
    $getResult = $this->ci->common_model->getDataFromTabel('event_term_conditions','document_file,document_path',$whereCondi);
    if(!empty($getResult)){
      //delte image from directory
      findFileNDelete($getResult[0]->document_path, $getResult[0]->document_file);
    }
    $this->ci->common_model->deleteRowFromTabel('event_term_conditions',$whereCondi);
    
    //delete data form event table and sponsor logo
    $where = array('id'=>$eventId);
    $getResult = $this->ci->common_model->getDataFromTabel('event_sponsors_logos','logo_file_path,logo_file_name',$whereCondi);
    if(!empty($getResult)){
      //delte image from directory
      findFileNDelete($getResult[0]->logo_file_path, $getResult[0]->logo_file_name);
    }
    $this->ci->common_model->deleteRowFromTabel('event',$where);
    $msg = lang('event_msg_event_deleted_successfully');
    $msgArray = array('msg'=>$msg,'is_success'=>'true');
    set_global_messages($msgArray);
    redirect('dashboard');
  }
  
  
  
  
  
  // ------------------------------------------------------------------------ 
  
    /*
  * @description: This function is used to show details of corporate section
  * @return void
  * 
    */  
  private function corporateshowview($eventId=0){
    
    //chheck if eventId is zero 
    if($eventId==0){
      redirect(base_url('dashboard'));
    }
    
    $userId = isLoginUser();
    $data['userId'] = $userId;
    $data['eventId'] =$eventId;
    
    $where  = array('event_id'=>$eventId);
    
    //get event details 
    $data['eventdetails']           = $this->ci->event_model->geteventdetails($eventId,$userId);
    
    //get data from corporate floor plan booth table
    $data['corporateFloorPlan']       = $this->ci->common_model->getDataFromTabel('corporate_floor_plan','*',$where,'','id','ASC');
    
    // get space tye record
    $data['spaceTypeRecord']        = $this->ci->event_model->getCorporateSpaceType($where);
    
    // get data from default package benefit
    $data['corporatedefaultPacBenefit']   = $this->ci->common_model->getDataFromTabel('corporate_package_benefit','*',array('event_id'=>$eventId,'corporate_package_id'=>0),'','package_benefit_id','ASC');
    
    // get data from default package benefit
    $data['corporatedefaultpurchaseitem'] = $this->ci->common_model->getDataFromTabel('corporate_purchase_item','*',array('event_id'=>$eventId,'corporate_package_id'=>0),'','purchase_item_id','ASC');
    
    //get master field for each event id
    $whereMsterField = array('event_id'=>$eventId,'form_id'=>'2','registrant_id'=>'0');
    $data['masterformfielddata']      = getDataFromTabel('custom_form_fields','*',$whereMsterField);
    
    // get corporate category 
    $data['eventCorporateCategories']     = $this->ci->common_model->getDataFromTabel('event_categories','*',array('event_id'=>$eventId,'is_corporate'=>'1'));
    
    // get corporate package details
    // get data from default package benefit
    $data['corporatepackagedetails']    = $this->ci->common_model->getDataFromTabel('corporate_package','*',$where,'','id','ASC');
        
    $this->ci->template->load('template','corporate/corporate_details',$data,TRUE);
    
  }
  
  
  
  // ------------------------ Registration Section  ------------------------------------------------ 
  
  /*
   * @access: public
   * @description: This function is used to registrant details  
   * @param: eventId
   * @return: void
   * 
   */ 
  
  public function setupregistrant(){  
    //current open eventId
    $eventId = currentEventId();
    
    //decode entered eventid
    $eventId = decode($eventId); 
    
    //check is data post 
    if($this->ci->input->post()){
      $actionName = $this->ci->input->post('formActionName');
      switch($actionName){  
        
        case "masterPersonalDetails":
          $this->insertupdatemasterpersonaldetails();
        break;  
        case "registrantCustomFieldSave":
          $this->addeditcustomefieldsave(); 
        break; 
        case "bookerCustomFieldSave":
          $this->addeditcustomefieldsave(); 
        break; 
        case "registrantsDetails":
          $this->insertupdateregistranttypes();
        break;
        
        default:
        // load registrant form view
        $this->registrantview($eventId);
      }
    }else{
      // load registrant form view
      $this->registrantview($eventId);
    }
  }
  
  
  // ------------------------------------------------------------------------ 
  
  /*
   * @access:    public
   * @description: This function is used to delete dietary 
   * @param:  deleteId
   * @return void
   * 
   */ 
  
  public function deletedietarytype(){
    $deleteid = $this->ci->input->post('deleteId');
    $where = array('id'=>$deleteid);
    //check record is exist 
    $this->ci->common_model->deleteRowFromTabel('event_dietary',$where);
    echo json_message('msg','Dietary type deleted.');
  }
    
    
  // ------------------------------------------------------------------------ 
    
    /*
    * @access:  private
    * @default: load event details
    * @description: This function is used to load registraint show details
    * @return: void
    *  
    */
  
  private function registrantview($eventId="0"){
    
    //chheck if eventId is zero 
    if($eventId==0){
      redirect(base_url('dashboard'));
    }
    $userId          = isLoginUser();
    $data['userId']      = $userId;
    $data['eventId']     = $eventId;
    $data['popupHeaderBg'] = 'bg_0073ae'; // popup header bg class 
    $data['popupButtonBg'] = 'custombtn_save'; // popup button bg class 
    
    //get event details 
    $data['eventdetails'] = $this->ci->event_model->geteventdetails($eventId,$userId);
    
    //if empty then redirect
    if(empty($data['eventdetails'])){
      redirect(base_url('dashboard'));
    }
    
    $where = array('event_id'=>$eventId);
    $regisWhere = array('event_id'=>$eventId, 'is_corporate_package'=>'0');
    $data['eventregistrants'] = $this->ci->common_model->getDataFromTabel('event_registrants','*',$regisWhere,'','registrants_order','ASC');
    $regisWhereCorp = array('event_id'=>$eventId, 'is_corporate_package'=>'1');
    $data['eventregistrantsCorp'] = $this->ci->common_model->getDataFromTabel('event_registrants','*',$regisWhereCorp,'','registrants_order','ASC');
    
    //get master field for each event id
    $whereCategoryField = array('is_corporate'=>'0');
    $data['registrantCategorydata'] = getDataFromTabelWhereIn('event_categories','*','event_id',array($eventId),$whereCategoryField);
    
    //get master field for each event id
    $whereMsterField = array('event_id'=>$eventId,'registrant_id'=>'0','form_id'=>'1');
    $data['masterformfielddata'] = getDataFromTabel('custom_form_fields','*',$whereMsterField);
    
    //get booker field for each event id
    $whereMsterField = array('event_id'=>$eventId,'registrant_id'=>'0','form_id'=>'3');
    $data['bookerformfielddata'] = getDataFromTabel('custom_form_fields','*',$whereMsterField);
    
    //get dietary data  
    $data['eventdietary'] = $this->ci->common_model->getDataFromTabel('event_dietary','*',$where,'','dietary_order','ASC');
    $this->ci->template->load('template','registration/setup_registrant',$data,TRUE);
  }
  

 // Private functions ------------------------------------------------------------------------ 
 
  /*
   * @access:    private
   * @description: This function is used to validate add event 
   * @return void
   * 
   */ 
  
  private function _addEventValidate(){
    
    // set ruls for form filed
    $this->ci->form_validation->set_rules('eventName', 'event name', 'trim|required');
    $this->ci->form_validation->set_rules('eventStartDate', 'event start date', 'trim|required');
    $this->ci->form_validation->set_rules('eventEndDate', 'event end date', 'trim|required');
    
    //check event selected date
    if($this->ci->input->post('eventStartDate') && $this->ci->input->post('eventEndDate')){
      $eventStartDate = strtotime($this->ci->input->post('eventStartDate'));
      $eventEndDate   = strtotime($this->ci->input->post('eventEndDate'));
      $currentDate    = strtotime(date('y-m-d'));
      if($currentDate  > $eventStartDate || $currentDate  > $eventEndDate){
        $this->ci->form_validation->set_rules('eventStartDateVali', 'Event start & end date should be greater than equal from today date.', 'callback_custom_error_set');
      }
      if($eventStartDate > $eventEndDate){
        $this->ci->form_validation->set_rules('eventStartDateVali', 'Event start date should not be greater than event end date.', 'callback_custom_error_set');
      }
    }
  }
    
 
  // ------------------------------------------------------------------------ 
  
  /*
   * @access:    private
   * @description: This function is used to save create event 
   * @return void
   * 
   */ 
  
  private function _createEventSave(){
	$insertData['user_id']                  =  isLoginUser();
    
    $event_title                            =  $this->ci->input->post('eventName');
    $insertData['event_title']              =  $event_title;
    $insertData['starttime']                =  dateFormate($this->ci->input->post('eventStartDate'),'Y-m-d H:i:s');
    $insertData['endtime']                  =  dateFormate($this->ci->input->post('eventEndDate'),'Y-m-d H:i:s');
    $insertData['event_number']             =  getMasterNumber('event'); //get event serial number
    $insertData['master_registrant_id']     =  'AA01'; //set default registrant number start with this number
    $insertData['master_registration_id']   =  'AA01'; //set default registration number start  with this number
    
    $event_title_new                        =  replaceSpaceWithCharacter($event_title, "_");
    $randomnumber                           =  randomnumber(4);
    
    $eventUrl                               =  $event_title_new.'_'.$randomnumber;
    $eventUrl                               =  strtolower($eventUrl);
    $insertData['event_url']                =  $eventUrl; //set unic url for event
    
    $eventId =  $this->ci->common_model->addDataIntoTabel('event', $insertData);
    return $eventId;
  }



    // ------------------------------------------------------------------------ 
  
  /*
   * @access:    private
   * @description: This function is used to event required data while create event
   * @param: $eventId
   * @return void
   */ 
  
  private function eventrequiredatainsert($eventId){
    
    $dietaryArray = $this->ci->config->item('default_deitary');
    
    //add two dietary requirment
    $order = 1;
    for($i=0;$i<count($dietaryArray);$i++){
      $dataArray['dietary_name']  = $dietaryArray[$i];
      $dataArray['event_id']    = $eventId; 
      $isUnDeleteable = ($dietaryArray[$i]=='None' || $dietaryArray[$i]=='Other')?1:0;
      $dataArray['is_undeletable'] = $isUnDeleteable;
      $dataArray['dietary_order'] = $order;
      
      //add only by one value in data base
      $this->adddietarydata($dataArray);
      $order++;
    }
    
    //add master field for each event
    $this->addmasterformfields($eventId,'1');
    $this->addmasterformfields($eventId,'2');
    $this->addmasterformfields($eventId,'3');
    $this->addmasterformfields($eventId,'4');
    $this->addmasterformfields($eventId,'5');
    //add default categories for each event
    $this->adddefaultcategories($eventId);
    
    // add default corporate categories for each event
    $this->adddefaultcorporatecategories($eventId);
    
    // add default package benefit for each event
    $this->adddefaultpackagebenefit($eventId);
    
    // add default additional purchase item
    $this->adddefaultadditionalpurchaseitem($eventId);
    
    
    
  }

  
  
  // ------------------------------------------------------------------------ 
  
  /*
   * @access:    private
   * @description: This function is used to add master field for each event wise to user custome form fields
   * @return void;
   * @param1 = eventId
   * @param2 = formId
   */
   
  private function addmasterformfields($eventId,$formId){
      
    $where = array('form_id'=>$formId);
    $formfieldmaster = $this->ci->common_model->getDataFromTabel('custom_form_field_master','*',$where);
    if(!empty($formfieldmaster)){
      foreach($formfieldmaster as $fieldmaster){
        $arrayVal = $this->ci->input->post();
        $filedId = 'field_'.$fieldmaster->id;
        $insertData['field_name'] = $fieldmaster->field_name;
        $insertData['field_type'] = $fieldmaster->field_type;
        $insertData['default_value'] = $fieldmaster->default_value;
        $insertData['form_id'] = $fieldmaster->form_id;
        $insertData['is_editable'] = $fieldmaster->is_editable;
        $insertData['event_id'] = $eventId;
        $insertData['field_status'] = $fieldmaster->field_status;
        $insertData['field_create_by'] = '1';
        $insertId = $this->ci->common_model->addDataIntoTabel('custom_form_fields', $insertData);
      }
    }
  }
  
  
  /*
   * @access:    private
   * @description: This function is used to add default cateogory
   * @return void;
   * @param1 = eventId
  */
  private function adddefaultcategories($eventId){
    
    $where = array('event_id'=>0,'is_corporate'=>'0');
    $defaultCategories = $this->ci->common_model->getDataFromTabel('event_categories','*',$where);
    if(!empty($defaultCategories)){
      foreach($defaultCategories as $value){
        
        $insertData['category']   = $value->category;     
        $insertData['event_id']   = $eventId;     
        $insertData['is_corporate'] = '0';    
        
        $insertId = $this->ci->common_model->addDataIntoTabel('event_categories', $insertData);
      }//end loop
    } 
      
  }
  
  /*
   * @access:    private
   * @description: This function is used to add default corporate cateogory
   * @return void;
   * @param1 = eventId
  */
  private function adddefaultcorporatecategories($eventId){
    
    $where = array('event_id'=>0,'is_corporate'=>'1');
    $defaultCategories = $this->ci->common_model->getDataFromTabel('event_categories','*',$where);
    if(!empty($defaultCategories)){
      foreach($defaultCategories as $value){
        
        $insertData['category']   = $value->category;     
        $insertData['event_id']   = $eventId;     
        $insertData['is_corporate'] = '1';    
        
        $insertId = $this->ci->common_model->addDataIntoTabel('event_categories', $insertData);
      }//end loop
    } 
      
  }
  
  /*
   * @access:    private
   * @description: This function is used to add default package benefit
   * @return void;
   * @param1 = eventId
  */
  private function adddefaultpackagebenefit($eventId){
    
    $where = array('event_id'=>0,'corporate_package_id'=>'0');
    $defaultCategories = $this->ci->common_model->getDataFromTabel('corporate_package_benefit','*',$where);
    if(!empty($defaultCategories)){
      foreach($defaultCategories as $value){
        
        $insertData['benefit_name']       = $value->benefit_name;     
        $insertData['event_id']         = $eventId;     
        $insertData['corporate_package_id']   = '0';    
        
        $insertId = $this->ci->common_model->addDataIntoTabel('corporate_package_benefit', $insertData);
      }//end loop
    } 
  }
  
  /*
   * @access:    private
   * @description: This function is used to add default purchase item
   * @return void;
   * @param1 = eventId
  */
  
  private function adddefaultadditionalpurchaseitem($eventId){
    
    $where = array('event_id'=>0,'corporate_package_id'=>'0');
    $defaultCategories = $this->ci->common_model->getDataFromTabel('corporate_purchase_item','*',$where);
    if(!empty($defaultCategories)){
      foreach($defaultCategories as $value){
        
        $insertData['name']           = $value->name;     
        $insertData['item_limit']         = $value->item_limit;     
        $insertData['total_price']        = $value->total_price;    
        $insertData['gst_include_price']    = $value->gst_include_price;    
        $insertData['event_id']         = $eventId;     
        $insertData['corporate_package_id']   = '0';    
        
        $insertId = $this->ci->common_model->addDataIntoTabel('corporate_purchase_item', $insertData);
        
      }//end loop
    } 
  }
  
    
  // ------------------------------------------------------------------------ 
  
  /*
   * @access:    private
   * @description: This function is used to add type of dietary requirement 
   * @return void
   * 
   */ 
  
  private function adddietarydata($dataArray){
    
    $insertData['dietary_name']   = $dataArray['dietary_name'];
    $insertData['event_id']     = $dataArray['event_id'];
    $insertData['is_undeletable'] = $dataArray['is_undeletable'];
    $insertData['dietary_order']  = $dataArray['dietary_order'];
    $insertId = $this->ci->common_model->addDataIntoTabel('event_dietary', $insertData);
    return $insertId;
  } 
  
  // ------------------------------------------------------------------------ 
  
  /*
  * @access: private 
    * @description: This function is used to load event details view
    * @param1: $eventId
    * @return: void
    *  
    */
  
  private function eventshowview($eventId="0"){
      
    //chheck if eventId is zero 
    if($eventId==0){
      redirect(base_url('dashboard'));
    }
    $userId = isLoginUser();
    $data['userId'] = $userId;
    $data['eventId'] =$eventId;
    $data['eventdetails'] = $this->ci->event_model->geteventdetails($eventId,$userId);
    $data['popupHeaderBg'] ='bg_0073ae'; // popup header bg class
    $data['popupButtonBg'] ='custombtn_save'; // popup button bg class 
    
    //if empty then redirect
    if(empty($data['eventdetails'])){
      redirect(base_url('dashboard'));
    }
    
    $where = array('event_id'=>$eventId);
    $whereUserData = array('id'=>$userId);
    //$data['eventsponsorslogos']   =   $this->ci->common_model->getDataFromTabel('event_sponsors_logos','*',$where);
    $data['userdata']           =   $this->ci->common_model->getDataFromTabel('user','*',$whereUserData);
    $data['mastercountrydata']  =   $this->ci->common_model->getDataFromTabel('master_country','*');
    $data['countrydata']        =   $this->ci->common_model->getDataFromTabel('country','*');
    $data['statedata']          =   $this->ci->common_model->getDataFromTabel('state','*');
    $data['getEventCategories'] =   $this->ci->common_model->getDataFromTabel('event_categories','*',$where);
    $data['eventSocialMediadetails'] =   $this->ci->common_model->getDataFromTabel('event_social_media','*',$where);
    
    $data['accountType'] = $this->ci->accountType;
    $data['fileSuffix'] = $this->ci->fileSuffix;
    
    $this->ci->template->load('template','eventdetails/event_details',$data,TRUE);
  }
  
  
  // ------------------------------------------------------------------------ 
  
  /*
   * @access:      private
   * @description: This function is used to check validate & save general event setup details 
   * @return void
   * 
   */ 
  
  private function updategeneraleventsetup(){
    
    //this function is used to validate general event setup   
    $this->_generalEventSetupValidate();  
    
    //get ajax request
    $is_ajax_request = $this->ci->input->is_ajax_request();
    //get eventId
    $eventId = $this->ci->input->post('eventId');
    
    if ($this->ci->form_validation->run($this->ci))
    {
      //call for save general event setup
      $this->_generalEventSetupSave($eventId);
      
      $msg = lang('event_msg_general_event_setup_saved');
      if($is_ajax_request){
        //echo json_message('msg',$msg);
        echo json_encode(array('msg'=>$msg,'is_success'=>'true','url'=>'event/eventdetails'));
      }else{
        $msgArray = array('msg'=>$msg,'is_success'=>'true');
        set_global_messages($msgArray);
        redirect('event/eventdetails/'.encode($eventId));
      }
    }
    else
    {
      $errors = $this->ci->form_validation->error_array();
      if($is_ajax_request){
        echo json_message('msg',$errors,'is_success','false');
      }else{
        // load event form view
        $this->eventshowview($eventId);
      }
    }
  }
  
  
  // ------------------------------------------------------------------------ 
  
  /*
   * @access:      private
   * @description: This function is used to validate general event setup 
   * @return void
   * 
   */ 
  
  /*private function _generalEventSetupValidate(){ 
    
    // set ruls for form filed
    $this->ci->form_validation->set_rules('eventName', 'event name', 'trim|required');
    $this->ci->form_validation->set_rules('eventSubtitle', 'event subtitle', 'trim|required');
    $this->ci->form_validation->set_rules('eventReferenceNumber', 'event reference number', 'trim');
    $this->ci->form_validation->set_rules('eventStartDate', 'event start date', 'trim|required');
    $this->ci->form_validation->set_rules('eventEndDate', 'event end date', 'trim|required');
    //$this->ci->form_validation->set_rules('earlybirdCutOffDate', 'early bird cut off date', 'trim');
    $this->ci->form_validation->set_rules('eventDescription', 'event description', 'trim');
    $this->ci->form_validation->set_rules('eventMaxRegistrants', 'event max registrants', 'trim|required|numeric');
    $this->ci->form_validation->set_rules('registerLastDate', 'register last date', 'trim|required');
    $this->ci->form_validation->set_rules('registerStartDate', 'registeration start date', 'trim|required');
    //$this->ci->form_validation->set_rules('earlybirdCloseDate', 'early bird close date', 'trim|required');
    $this->ci->form_validation->set_rules('eventLocation', 'event location', 'trim');
    $this->ci->form_validation->set_rules('eventVenue', 'event venue', 'trim|required');
    $this->ci->form_validation->set_rules('sittingStyle', 'sitting style', 'trim');
    $this->ci->form_validation->set_rules('timeZone', 'time zone', 'trim|required');
    //$this->ci->form_validation->set_rules('eventDestination', 'destination', 'trim|required');
    
    //check event selected date
    if($this->ci->input->post('eventStartDate') && $this->ci->input->post('eventEndDate')){
      
      $eventStartDate      = strtotime($this->ci->input->post('eventStartDate'));
      $eventEndDate        = strtotime($this->ci->input->post('eventEndDate'));
      $registerLastDate    = strtotime($this->ci->input->post('registerLastDate'));
      $earlybirdCutOffDate = strtotime($this->ci->input->post('earlybirdCutOffDate'));
      $registerStartDate    = strtotime($this->ci->input->post('registerStartDate'));
      $earlybirdCloseDate = strtotime($this->ci->input->post('earlybirdCloseDate'));
      $currentDate      = strtotime(date('y-m-d'));
      
      if($currentDate  > $eventEndDate){
        $this->ci->form_validation->set_rules('eventStartDateVali', 'Event end date should be greater than equal from today date.', 'callback_custom_error_set');
      }
      
      if($eventStartDate > $eventEndDate){
        $this->ci->form_validation->set_rules('eventStartDateVali', 'Event start date should not be greater than event end date.', 'callback_custom_error_set');
      }
      
      if($registerLastDate > $eventStartDate){
        $this->ci->form_validation->set_rules('registerLastDate', 'Registration cut off date should not be greater than event start date.', 'callback_custom_error_set');
      }
      
      if($registerStartDate > $eventStartDate){
        $this->ci->form_validation->set_rules('registerLastDate', 'Registration start date should not be greater than form start date.', 'callback_custom_error_set');
      }
      
      if($earlybirdCloseDate > $eventStartDate){
        $this->ci->form_validation->set_rules('registerLastDate', 'Early bird close date should not be greater than form start date.', 'callback_custom_error_set');
      }
      
      //event register Earlybird Cut Off validation
        if($eventStartDate < $earlybirdCutOffDate){
        $this->ci->form_validation->set_rules('earlybirdCutOffDate', 'Earlybird Cut off  date should not be greater than form start date.', 'callback_custom_error_set');
      } 
       
    } 
  }*/
  
  private function _generalEventSetupValidate(){
    
    // set ruls for form filed
    $this->ci->form_validation->set_rules('eventName', 'event name', 'trim|required');
    //$this->ci->form_validation->set_rules('eventSubtitle', 'event subtitle', 'trim|required');
    $this->ci->form_validation->set_rules('eventReferenceNumber', 'event reference number', 'trim');
    $this->ci->form_validation->set_rules('eventStartDate', 'event start date', 'trim|required');
    $this->ci->form_validation->set_rules('eventEndDate', 'event end date', 'trim|required');
    //$this->ci->form_validation->set_rules('earlybirdCutOffDate', 'early bird registeration start off date', 'trim');
    $this->ci->form_validation->set_rules('eventDescription', 'event description', 'trim');
    $this->ci->form_validation->set_rules('eventMaxRegistrants', 'event max registrants', 'trim|required|numeric');
    $this->ci->form_validation->set_rules('registerLastDate', 'register last date', 'trim|required');
    $this->ci->form_validation->set_rules('registerStartDate', 'registeration start date', 'trim|required');
    //$this->ci->form_validation->set_rules('earlybirdCloseDate', 'early bird cut off date', 'trim|required');
    $this->ci->form_validation->set_rules('eventLocation', 'event location', 'trim');
    $this->ci->form_validation->set_rules('eventVenue', 'event venue', 'trim|required');
    $this->ci->form_validation->set_rules('sittingStyle', 'sitting style', 'trim');
    $this->ci->form_validation->set_rules('timeZone', 'time zone', 'trim|required');
    //$this->ci->form_validation->set_rules('eventDestination', 'destination', 'trim|required');
    
    //check event selected date
    if($this->ci->input->post('eventStartDate') && $this->ci->input->post('eventEndDate')){
      
      $eventStartDate      = strtotime($this->ci->input->post('eventStartDate'));
      $eventEndDate        = strtotime($this->ci->input->post('eventEndDate'));
      $registerLastDate    = strtotime($this->ci->input->post('registerLastDate'));
      $earlybirdCutOffDate = strtotime($this->ci->input->post('earlybirdCutOffDate'));
      $registerStartDate    = strtotime($this->ci->input->post('registerStartDate'));
      $earlybirdCloseDate = strtotime($this->ci->input->post('earlybirdCloseDate'));
      $currentDate      = strtotime(date('y-m-d'));
      
      if($currentDate  > $eventEndDate){
        $this->ci->form_validation->set_rules('eventStartDateVali', 'Event end date should be greater than equal from today date.', 'callback_custom_error_set');
      }
      
      if($eventStartDate > $eventEndDate){
        $this->ci->form_validation->set_rules('eventStartDateVali', 'Event start date should not be greater than event end date.', 'callback_custom_error_set');
      }
      
      if($registerLastDate > $eventStartDate){
        $this->ci->form_validation->set_rules('registerLastDate', 'Registration cut off date should not be greater than event start date.', 'callback_custom_error_set');
      }
      
      if($registerStartDate > $eventStartDate){
        $this->ci->form_validation->set_rules('registerLastDate', 'Registration start date should not be greater than form start date.', 'callback_custom_error_set');
      }
      
      if($registerStartDate > $registerLastDate){
        $this->ci->form_validation->set_rules('registerStartDate', 'Registration start date should not be greater than form registeration last date.', 'callback_custom_error_set');
      }
      
      if($registerStartDate > $registerLastDate){
        $this->ci->form_validation->set_rules('registerStartDate', 'Registration start date should not be greater than form registeration last date.', 'callback_custom_error_set');
      }
      
      if($earlybirdCutOffDate > $earlybirdCloseDate){
        $this->ci->form_validation->set_rules('earlybirdCutOffDate', 'Early bird cut off date should not be greater than early bird start date.', 'callback_custom_error_set');
      }
      
      //event register Earlybird Cut Off validation
      if($eventStartDate < $earlybirdCutOffDate){
        $this->ci->form_validation->set_rules('earlybirdCutOffDate', 'Earlybird registeration start date should not be greater than form registration start date.', 'callback_custom_error_set');
      }
      
      if($eventStartDate < $earlybirdCloseDate){
        $this->ci->form_validation->set_rules('earlybirdCutOffDate', 'Earlybird registeration cut off date should not be greater than form registration start date.', 'callback_custom_error_set');
      } 
       
    } 
  }
   
  
  // ------------------------------------------------------------------------ 
  
  /*
   * @access: private
   * @description: This function is used to save general event setup 
   * @return void
   * 
   */ 
  
  private function _generalEventSetupSave($eventId){
    
    // prepare array for update data
    $updateData['event_title']          = $this->ci->input->post('eventName');
    $updateData['subtitle']          = $this->ci->input->post('eventSubtitle');
    $updateData['short_form_title']     = $this->ci->input->post('eventShortFormTitle');
    $updateData['event_reference_number']   = $this->ci->input->post('eventReferenceNumber');
    $updateData['starttime']        = dateFormate($this->ci->input->post('eventStartDate'),"Y-m-d H:i");
    $updateData['endtime']            = dateFormate($this->ci->input->post('eventEndDate'),"Y-m-d H:i");
    $updateData['description']        = $this->ci->input->post('eventDescription');
    $updateData['number_of_rego']     = $this->ci->input->post('eventMaxRegistrants');
    $updateData['last_reg_date']      = dateFormate($this->ci->input->post('registerLastDate'),"Y-m-d H:i");
    $updateData['early_bird_reg_date']    = dateFormate($this->ci->input->post('earlybirdCutOffDate'),"Y-m-d H:i");
    $updateData['reg_startdate']      = dateFormate($this->ci->input->post('registerStartDate'),"Y-m-d H:i");
    $updateData['reg_closedate']      = dateFormate($this->ci->input->post('earlybirdCloseDate'),"Y-m-d H:i");
    $updateData['event_location']       = $this->ci->input->post('eventLocation');
    $updateData['event_venue']        = $this->ci->input->post('eventVenue');
    $updateData['sitting_style']      = $this->ci->input->post('sittingStyle');
    $updateData['time_zone']          = $this->ci->input->post('timeZone');
    $updateData['destination']        = $this->ci->input->post('eventDestination');
    
    //prepare condition for update data by id
    $where = array('id'=>$eventId);
    //call model for update data
    $this->ci->common_model->updateDataFromTabel('event',$updateData,$where); 
  }
  
  
  // ------------------------------------------------------------------------ 
  
  /*
   * @access: private
   * @description: This function is used to validate & insert and update contact person details 
   * @return void
   * 
   */ 
  
  private function insertupdatecontactperson(){
    
    //call function for validate
    $this->_contactPersonValidate();
    
    //get ajax request
    $is_ajax_request = $this->ci->input->is_ajax_request();
    //get eventId
    $eventId = $this->ci->input->post('eventId');
    
    if($this->ci->form_validation->run($this->ci))
    {
        //call function for save 
        $msg = $this->_contactPersonSave($eventId);
      
        //check ajax post request 
        if($is_ajax_request){
          echo json_message('msg',$msg);
        }else{
          $msgArray = array('msg'=>$msg,'is_success'=>'true');
          set_global_messages($msgArray);
          redirect('event/eventdetails/'.encode($eventId));
        }
    }else{
      
      $errors = $this->ci->form_validation->error_array();
      if($is_ajax_request){
        echo json_message('msg',$errors,'is_success','false');
      }else{
        // load event form view
        $this->eventshowview($eventId);
      }
    } 
  }
  
  
  
  
  
  // ------------------------------------------------------------------------   
    
  /*
   * @access: private
   * @description: This function is used to validate contact personal details
   * @return void
   * 
   */ 
  
  private function _contactPersonValidate(){
  
    // set ruls for form filed
    //$this->form_validation->set_rules('title', 'title', 'trim|required');
    $this->ci->form_validation->set_rules('firstName', 'first name', 'trim|required');
    $this->ci->form_validation->set_rules('lastName', 'last name', 'trim|required');
    $this->ci->form_validation->set_rules('contactOrganisation', 'organisation', 'trim');
    $this->ci->form_validation->set_rules('contactPosition', 'position', 'trim');
    $this->ci->form_validation->set_rules('contactAddress1', 'contact address1', 'trim|required');
    $this->ci->form_validation->set_rules('contactCity', 'contact city', 'trim|required');
    $this->ci->form_validation->set_rules('contactState', 'contact state', 'trim|required');
    $this->ci->form_validation->set_rules('contactPostcode', 'contact postcode', 'trim|required');
    $this->ci->form_validation->set_rules('contactCountry', 'contact country', 'trim|required');
    $this->ci->form_validation->set_rules('contactPhone', 'contact phone', 'trim|required');
    $this->ci->form_validation->set_rules('contactFax', 'contact fax', 'trim');
    //$this->ci->form_validation->set_rules('contactMobile', 'mobile', 'trim');
    $this->ci->form_validation->set_rules('contactEmail', 'email', 'trim|required|email');
    $this->ci->form_validation->set_rules('confirmEmail', 'confirm email', 'trim|required|email|matches[contactEmail]');
    $this->ci->form_validation->set_rules('contactWeb', 'web url', 'trim');
  }
  
  /*
   * @access: private
   * @description: This function is used to save contact personal details
   * @return void
   * 
   */ 
  
  private function _contactPersonSave($eventId){
    // difine data in array 
    $getData['event_contact_person'] = $this->ci->input->post('eventContactPerson');
    $getData['title']          = $this->ci->input->post('title');
    $getData['first_name']       = $this->ci->input->post('firstName');
    $getData['last_name']        = $this->ci->input->post('lastName');
    $getData['event_id']       = $this->ci->input->post('eventId');
    $getData['organisation']     = $this->ci->input->post('contactOrganisation');
    $getData['position']         = $this->ci->input->post('contactPosition');
    $getData['address']        = $this->ci->input->post('contactAddress1');
    $getData['address_second']     = $this->ci->input->post('contactAddress2');
    $getData['city']         = $this->ci->input->post('contactCity');
    $getData['state']          = $this->ci->input->post('contactState');
    $getData['postcode']       = $this->ci->input->post('contactPostcode');
    $getData['country']        = $this->ci->input->post('contactCountry');
    
    //Contact save
    
    $getData['phone1_mobile']      = $this->ci->input->post('input_hidden_mobile1');
    $getData['phone1_landline']    = $this->ci->input->post('input_hidden_landline1');
    
    $getData['phone2_mobile']      = $this->ci->input->post('input_hidden_mobile2');
    $getData['phone2_landline']    = $this->ci->input->post('input_hidden_landline2');
    
    $getData['fax']          = $this->ci->input->post('contactFax').'-'.$this->ci->input->post('contactFax1');
    $getData['mobile']         = $this->ci->input->post('contactMobile');
    $getData['email']          = $this->ci->input->post('contactEmail');
    $getData['web']          = $this->ci->input->post('contactWeb');
    $getData['fbid']          = $this->ci->input->post('contactFb');
    $getData['twitterid']          = $this->ci->input->post('contactTwitter');
    $getData['linkedin']          = $this->ci->input->post('contactLinkedin');
    $getData['gplus']          = $this->ci->input->post('contactGplus');
    $getData['pintrest']          = $this->ci->input->post('contactPintrest');
    
    // Custom contact type
    $customContactType         = $this->ci->input->post('contactType'); // post as array 
    $customContactDetails        = $this->ci->input->post('contactDetails'); // post as array 
    
    // check contact person details by event id
    $where = array('event_id'=>$eventId);
    //$countResult = $this->ci->common_model->countResult('event_contact_person',$where);
      $result = $this->ci->common_model->getDataFromTabel('event_contact_person','id',$where);
    
    //update here
    if($result){
      
      // get event contact persion id
      $customContactId = $result[0]->id;
      
      $this->ci->common_model->updateDataFromTabel('event_contact_person',$getData,$where);
      $msg = lang('event_msg_contact_person_details_saved');
    }else{
      //insert here
      $customContactId = $this->ci->common_model->addDataIntoTabel('event_contact_person', $getData);
      $msg = lang('event_msg_contact_person_details_saved');
    }
    
     // delete records if exits based on contact persion id
    $contactPersonId=$this->ci->input->post('contact_person_id'); // array posted
    if(!empty($contactPersonId)){
      $this->ci->event_model->deleteeventcustomcontact($contactPersonId);
    } 
        
    //store custom contacts
    if(!empty($customContactType)){
    
      
      foreach($customContactType as $key=>$value){
        $customContactData = array();
        
        $customContactData['contact_person_id'] = $customContactId;
        $customContactData['contact_type']    = $value;
        $customContactData['details']         = (isset($customContactDetails[$key])) ? $customContactDetails[$key] : "";
        
        $bulkCustomData[] = $customContactData;
        
      }//end loop
      
      $this->ci->event_model->addeventcustomcontact($bulkCustomData);
    } 
        
    return $msg;
  }
  
  // ------------------------------------------------------------------------ 
  
  /*
   * @access: private
   * @description: This function is used to validate & insert and update contact person details 
   * @return void
   * 
   */ 
   private function insertupdatecustomcontactperson(){
    
    //call function for validate
    $this->_contactCustomValidate();
    
    if($this->ci->form_validation->run($this->ci))
    {
      // call a function to save contact details
      $this->_savecustomcontact();
      
    }else{
      $errors = $this->ci->form_validation->error_array();
      echo json_message('msg',$errors,'is_success','false');
    } 
      
   }
  
    /*
   * @access: private
   * @description: This function is used to validate custom contact personal
   * @return void
   * 
   */ 
  
  private function _contactCustomValidate(){
    
    $this->ci->form_validation->set_rules('ctype', 'Contact Type', 'trim|required');
    //$this->ci->form_validation->set_rules('ctype_details', 'Details', 'trim|required');
  }
  
  /*
   * @access: private
   * @description: This function is used to save Custom contact details 
   * @return void
   * 
   */ 
   
   private function _savecustomcontact(){
    
    //get current eventId
    $eventId = currentEventId();
    
    // get Contact Persion id
    $result=$this->ci->common_model->getDataFromTabel('event_contact_person','id',array('event_id'=>decode($eventId)));
    
    if(!empty($result)){
       
       $data['contact_person_id'] = (isset($result[0]->id)) ? $result[0]->id : "0";
       $data['contact_type']    = trim($this->ci->input->post('ctype'));
       $data['details']         = trim($this->ci->input->post('ctype_details'));
      
       // insert record in db
      $customContactId = $this->ci->common_model->addDataIntoTabel('event_custom_contact', $data);
      if($customContactId){
        $msg = lang('event_msg_custom_contact_saved');
        echo json_message('msg',$msg,'is_success','true');
      }else{
        $errors = lang('comm_error');
        echo json_message('msg',$errors,'is_success','false');
      } 
      
    }else{
      $errors = lang('event_custom_contact_add_error');
      echo json_message('msg',$errors,'is_success','false');
    }
  }
  
  
    // ------------------------------------------------------------------------ 
  
  /*
   * @access:      private
   * @description: This function is used to vlaidate, insert and update lost password details 
   * @return void
   * 
   */ 
  
  private function insertupdatelostpassworddetails(){
    
    //call function for validate
    $this->_lostPasswordValidate();
    
    //get ajax request
    $is_ajax_request = $this->ci->input->is_ajax_request();
    //get eventId
    $eventId = $this->ci->input->post('eventId');
    
    if ($this->ci->form_validation->run($this->ci))
    {
      //call function for save
      $msg = $this->_lostPasswordSave($eventId);
        
      // check ajax post request
      if($is_ajax_request){
        echo json_message('msg',$msg);
      }else{
        $msgArray = array('msg'=>$msg,'is_success'=>'true');
        set_global_messages($msgArray);
        redirect('event/eventdetails/'.encode($eventId));
      } 
    
    }else{
      $errors = $this->ci->form_validation->error_array();
      if($is_ajax_request){
        echo json_message('msg',$errors,'is_success','false');
      }else{
        // load event form view
        $this->eventshowview($eventId);
      }
    } 
  }
  
  // ------------------------------------------------------------------------ 
  
  /*
   * @access: private
   * @description: This function is used to validate lost password details 
   * @return void
   * 
   */ 
  
  private function _lostPasswordValidate(){    
    // set ruls for form filed
    $this->ci->form_validation->set_rules('lostContactName', 'contact name', 'trim|required');
    $this->ci->form_validation->set_rules('lostContactPhone', 'contact phone', 'trim|required');
    $this->ci->form_validation->set_rules('lostContactEmail', 'contact email', 'trim|required|email');
    $this->ci->form_validation->set_rules('lostConfirmEmail', 'confirm email', 'trim|required|email|matches[lostContactEmail]');
  }
  
    // ------------------------------------------------------------------------ 
  
  /*
   * @access: private
   * @description: This function is used to save lost password details 
   * @return void
   * 
   */ 
  
  private function _lostPasswordSave($eventId){
    
    
    $mobile = $this->ci->input->post('input_hidden_mobile3');
    $landline = $this->ci->input->post('input_hidden_landline3');
    
    if($this->ci->input->post('input_hidden_mobile3')==""){
      $mobile = $this->ci->input->post('lostContactPhone');
    }elseif($this->ci->input->post('input_hidden_landline3')==""){
      $landline = $this->ci->input->post('lostContactPhone');
    }
    
    $getData['contact_name']  = $this->ci->input->post('lostContactName');
    $getData['contact_phone']   = $landline;
    $getData['contact_mobile']  = $mobile;
    $getData['contact_email']   = $this->ci->input->post('lostContactEmail');
    
    // update contact personal date in event table
    $where = array('id'=>$eventId);
    $this->ci->common_model->updateDataFromTabel('event',$getData,$where);
    $msg = lang('event_msg_event_lost_password_saved');
    
    return $msg;
  }
  
  /* @access:    public
  * @description: This function is used to save email agaist this event
  * @return     : @void
  */ 
  public function addupdateeventemail(){
    
    $insert = TRUE;
    
    $this->_eventEmailValidate();
    
    if($this->ci->form_validation->run($this->ci))
    {
      $jsonReturn = $this->_eventEmailSave();
      echo $jsonReturn;
    }else{
      $errors = $this->ci->form_validation->error_array();
      echo json_message('msg',$errors,'is_success','false','url','event/eventdetails/');
    }
  } 
  
  
    // ------------------------------------------------------------------------   
    
  /*
   * @access: private
   * @description: This function is used to validate Custom Email details
   * @return void
   * 
   */ 
  
  private function _eventEmailValidate(){
  
    // set ruls for form filed
    //$this->form_validation->set_rules('title', 'title', 'trim|required');
    $this->ci->form_validation->set_rules('ce_name', 'Name', 'trim|required');
    $this->ci->form_validation->set_rules('ce_subject', 'Subject', 'trim|required');
    $this->ci->form_validation->set_rules('ce_from', 'From Email', 'trim');
    $this->ci->form_validation->set_rules('ce_from_name', 'From Name', 'trim');
    $this->ci->form_validation->set_rules('ce_content', 'Content', 'trim|required');
    
  }
  
  /* @access:    private
   * @description: This function is used to save email agaist this event
   * @return     : boolean
  */ 
  private function _eventEmailSave(){
    
    $insert   = TRUE;
    $is_success = 'false';
    $msg    = "";
    $eventId  = currentEventId();
    $emailId = 0;
    if(!empty($eventId)){
      // get posted values
      $emailId = decode($this->ci->input->post('emailId'));
      
      if(!empty($emailId)){
            // check this email id exist if exit then update the email log
            $where = array('email_id'=>$emailId);
            $countResult = $this->ci->common_model->countResult('event_emails',$where);
            if($countResult>0){
              $insert  = FALSE;
            } // end if
      } // end if
          
      $emailTitle   = trim($this->ci->input->post('ce_name'));    
      $subject    = trim($this->ci->input->post('ce_subject'));   
      $fromEmail    = trim($this->ci->input->post('ce_from'));    
      $fromEmailName  = trim($this->ci->input->post('ce_from_name'));   
      $contents       = $this->ci->input->post('ce_content');   
      $category       = serialize($this->ci->input->post('ce_category'));   
    
                  
      $data = array('event_id'=>decode($eventId),'email_title'=>$emailTitle,'subject'=>$subject,'from_email'=>$fromEmail,'from_name'=>$fromEmailName,'contents'=>$contents,'category_id'=>$category);
      if($insert){ // insert new record in db
        $emailId = $this->ci->event_model->addeventcustomemail($data);
        if($emailId){
          $is_success = 'true';$msg = lang('event_msg_event_email_saved');
        }
      }else{
        if($this->ci->event_model->updateeventcustomemail($data,array('email_id'=>$emailId))){
          $is_success = 'true';$msg = lang('event_msg_event_email_saved');
        }
      }
    }else{
      $msg = lang('comm_error');
    }
    //return json_message('msg',$msg,'is_success',$is_success,'is_insert',$insert,'eventtitle',$emailTitle,'eventid',$emailId);
    return json_encode(array('msg'=>$msg,'is_success'=>$is_success,'url'=>'event/eventdetails/','is_insert'=>$insert,'tableInsertedId'=>$emailId));
  } 
  
  // ------------------------------------------------------------------------   
    
  /*
   * @access: private
   * @description: This function is used to validate & insert and update invoice details 
   * @return void
   * 
   */ 
  
  private function insertupdateinvoicedetails(){
    
    //call function for validate
    $this->_invoiceDetailsValidate();
    
    //get ajax request
    $is_ajax_request = $this->ci->input->is_ajax_request();
    //get eventId
    $eventId     = $this->ci->input->post('eventId');
    
    if ($this->ci->form_validation->run($this->ci))
    {
      //call function for save
      $msg = $this->_invoiceDetailsSave($eventId);
        
      //check ajax post requrest
      if($is_ajax_request){
        echo json_message('msg',$msg);
      }else{
        $msgArray = array('msg'=>$msg,'is_success'=>'true');
        set_global_messages($msgArray);
        redirect('event/eventdetails/'.encode($eventId));
      } 
      
    }else{
      $errors = $this->ci->form_validation->error_array();
      if($is_ajax_request){
        echo json_message('msg',$errors,'is_success','false');
      }else{
        // load event form view
        $this->eventshowview($eventId);
      }
    }   
  }
  
  // ------------------------------------------------------------------------ 
  
  /*
   * @access:    public
   * @description: This function is used to validate invoice details
   * @return void
   * 
   */ 
  
  private function _invoiceDetailsValidate(){
    
    // set ruls for form filed
    $this->ci->form_validation->set_rules('eventInvoiceHeader', 'invoice header', 'trim|required');
    $this->ci->form_validation->set_rules('eventInvoiceName', 'invoice name', 'trim|required');
    //invoice abn
    if($this->ci->input->post('isABN')=='1'){
      $this->ci->form_validation->set_rules('eventInvoiceABN', 'invoice abn', 'trim|required');
      
      //gst rate
      if($this->ci->input->post('isCollectGst')=='1'){
        $this->ci->form_validation->set_rules('eventGSTRate', 'gst rate', 'trim|required');
      }
    }
    
    $this->ci->form_validation->set_rules('eventInvoiceAddress1', 'invoice address1', 'trim|required');
    $this->ci->form_validation->set_rules('eventInvoiceAddress2', 'invoice address1', 'trim');
    $this->ci->form_validation->set_rules('eventInvoiceCity', 'invoice city', 'trim|required');
    $this->ci->form_validation->set_rules('eventInvoiceState', 'invoice state', 'trim|required');
    $this->ci->form_validation->set_rules('eventInvoicePostcode', 'invoice postcode', 'trim|required');
    $this->ci->form_validation->set_rules('eventInvoiceCountry', 'invoice country', 'trim|required');
    $this->ci->form_validation->set_rules('eventInvoiceEmailSentto', 'email invoice copy to', 'trim|email');
    
    if($this->ci->input->post('eventInvoiceEmailSentto')){
      $this->ci->form_validation->set_rules('eventInvoiceEmailSenttoConfirm', 'confirm email invoice copy to', 'trim|required|email|matches[eventInvoiceEmailSentto]');
    }
    
    $this->ci->form_validation->set_rules('eventInvoiceEmailSentfrom', 'invoice email sent from', 'trim|required|email');
    $this->ci->form_validation->set_rules('eventInvoiceEmailSentfromConfirm', 'confirm invoice email sent from', 'trim|required|email|matches[eventInvoiceEmailSentfrom]');
    $this->ci->form_validation->set_rules('eventInvoiceInstruction', 'instruction', 'trim');
  }
  
  // ------------------------------------------------------------------------ 
  
  /*
   * @access:  private
   * @description: This function is used to save invoice details
   * @return void
   * 
   */ 
  
  private function _invoiceDetailsSave($eventId){
    
    //prepare array for save data
    $getData['event_id']    = $this->ci->input->post('eventId');
    $getData['event_header']  = $this->ci->input->post('eventInvoiceHeader');
    $getData['invoice_name']  = $this->ci->input->post('eventInvoiceName');
    $getData['address']     = $this->ci->input->post('eventInvoiceAddress1');
    $getData['address_second']  = $this->ci->input->post('eventInvoiceAddress2');
    $getData['city']      = $this->ci->input->post('eventInvoiceCity');
    $getData['state']     = $this->ci->input->post('eventInvoiceState');
    $getData['postcode']    = $this->ci->input->post('eventInvoicePostcode');
    $getData['country']     = $this->ci->input->post('eventInvoiceCountry');
    $getData['sent_to_email']   = $this->ci->input->post('eventInvoiceEmailSentto');
    $getData['sent_from_email'] = $this->ci->input->post('eventInvoiceEmailSentfrom');
    $getData['invoice_intro'] = $this->ci->input->post('eventInvoiceInstruction');
    $getData['above_address'] = $this->ci->input->post('aboveAddressApply');
    
    //invoice abn
    if($this->ci->input->post('isABN')=='1'){
      $getData['is_abn']  = $this->ci->input->post('isABN');
      $getData['abn']   = $this->ci->input->post('eventInvoiceABN');
      
      //gst rate
      if($this->ci->input->post('isCollectGst')=='1'){
        $getData['is_collect_gst'] = $this->ci->input->post('isCollectGst');
        $getData['gst_rate']     = $this->ci->input->post('eventGSTRate');
      }else{
        $getData['is_collect_gst'] = $this->ci->input->post('isCollectGst');
        $getData['gst_rate']     = '0';
      }
    }else{
      $getData['is_abn']      = $this->ci->input->post('isABN');
      $getData['abn']       = '0';
      $getData['is_collect_gst']  = $this->ci->input->post('isCollectGst');
      $getData['gst_rate']    = '0';
    }
    
    // check contact person details by event id
    $where = array('event_id'=>$eventId);
    $countResult = $this->ci->common_model->countResult('event_invoice',$where);
    //update here
    if($countResult > 0){
      $this->ci->common_model->updateDataFromTabel('event_invoice',$getData,$where);
      $msg = lang('event_msg_event_invoice_details_saved');
    }else{
      //insert here
      $this->ci->common_model->addDataIntoTabel('event_invoice', $getData);
      $msg = lang('event_msg_event_invoice_details_saved');
    }
    
    return $msg;
  }
  
  
  // ------------------------------------------------------------------------ 
  
  /*
   * @access:  private
   * @description: This function is used to save group booking details
   * @return void
   * 
   */ 
  
  private function insertupdategroupbooking(){
    
    //call function for validate
    $validate = TRUE;
    if($this->ci->input->post('group_booking')=='1'){
      $this->_groupingbookingValidate();
      $validate=$this->ci->form_validation->run($this->ci);
    }
    //get ajax request
    $is_ajax_request = $this->ci->input->is_ajax_request();
    //get eventId
    $eventId     = $this->ci->input->post('eventId');
        
    if($validate)
    {
      $group_booking   = ($this->ci->input->post('group_booking')=='1') ? $this->ci->input->post('group_booking') : "0";
      $min_gsize     = $this->ci->input->post('min_gsize');
      $discount      = $this->ci->input->post('discount');
      
      $updateData = array('group_booking_allowed'=>$group_booking,'min_group_booking_size'=>$min_gsize,'percentage_discount_for_group_booking'=>$discount);
      
      //prepare condition for update data by id
      $where = array('id'=>$eventId);
      //call model for update data
      $msg = $this->ci->common_model->updateDataFromTabel('event',$updateData,$where);  
      
      //check ajax post requrest
      if($is_ajax_request){
        echo json_message('msg',$msg);
      }else{
        $msgArray = array('msg'=>$msg,'is_success'=>'true');
        set_global_messages($msgArray);
        redirect('event/eventdetails/'.encode($eventId));
      } 
      
    }else{
      $errors = $this->ci->form_validation->error_array();
      echo json_message('msg',$errors,'is_success','false');
    }   
    
  }
  
  
  /*
   * @access:    public
   * @description: This function is used to validate Group booking
   * @return void
   * 
   */ 
  
  private function _groupingbookingValidate(){
    
    // set ruls for form filed
    if($this->ci->input->post('group_booking')=='1'){
      $this->ci->form_validation->set_rules('min_gsize', 'Minimum Group Size', 'trim|required|numeric');
      $this->ci->form_validation->set_rules('discount', 'Discount', 'trim|required|numeric');
    } 
  }
  
  
  // ------------------------------------------------------------------------ 
  
  /* @access:    public
   * @description: This function is used to add new event category
   * @return     : @void
   */ 
  public function addupdateeventcategory(){
    
    //current open eventId
    $eventId = currentEventId();
    
    if(!empty($eventId)){
      //decode entered eventid
      $eventId = decode($eventId); 
      
      // set ruls for form filed
      $this->ci->form_validation->set_rules('cname', 'Name', 'trim|required');
      $this->ci->form_validation->set_rules('c_package', 'Corporate Package', 'trim|required');
      
      if($this->ci->form_validation->run($this->ci)){
        
        // get posted value
        $catId     = trim($this->ci->input->post('catId'));
        $cname     = trim($this->ci->input->post('cname'));
        $isCorporate = trim($this->ci->input->post('c_package'));
        $data = array('event_id'=>$eventId,'category'=>$cname,'is_corporate'=>$isCorporate);
        
        // check this category exit if exit then update the category
        $where = array('category_id'=>$catId);
        $countResult = $this->ci->common_model->countResult('event_categories',$where);
        $jsonResponse = array('is_success'=>'true','url'=>'event/eventdetails');
        if($countResult>0){
          $result = $this->ci->event_model->updateeventcategory($data,$where);
          if($result){
            $jsonResponse['msg'] = lang("event_category_updated_succ");
            echo json_encode($jsonResponse);  
          }else{
            $jsonResponse['msg'] = lang('comm_error');$jsonResponse['is_success'] = 'false';
            echo json_encode($jsonResponse);  
          }
        }else{
          $result = $this->ci->event_model->addeventcategory($data);
          if($result){
            $jsonResponse['msg'] = lang("event_category_add_succ");
            echo json_encode($jsonResponse);  
          }else{
            $jsonResponse['msg'] = lang('comm_error');$jsonResponse['is_success'] = 'false';
            echo json_encode($jsonResponse);  
          }
        } 
              
      }
    } // end if
    
  }
  
  // ------------------------------------------------------------------------ 
  
  /*
   * @access: private
   * @description: This function is used to vlaidate, insert and update bank details 
   * @return void
   * 
   */ 
  
  private function insertupdatebankdetails(){
    
    //call function for validate
    $this->_bankDetailsValidate();
    
    //get ajax request
    $is_ajax_request = $this->ci->input->is_ajax_request();
    //get eventId
    $eventId     = $this->ci->input->post('eventId');
    
    if ($this->ci->form_validation->run($this->ci))
    {
      //call function for save
      $msg = $this->_bankDetailsSave($eventId);
        
      // check ajax post request
      if($is_ajax_request){
        echo json_message('msg',$msg);
      }else{
        $msgArray = array('msg'=>$msg,'is_success'=>'true');
        set_global_messages($msgArray);
        redirect('event/eventdetails/'.encode($eventId));
      } 
    
    }else{
      $errors = $this->ci->form_validation->error_array();
      if($is_ajax_request){
        echo json_message('msg',$errors,'is_success','false');
      }else{
        // load event form view
        $this->eventshowview($eventId);
      }
    } 
  }
  
  
  // ------------------------------------------------------------------------ 
  
  /*
   * @access: private
   * @description: This function is used to validate bank details 
   * @return void
   * 
   */ 
  
  private function _bankDetailsValidate(){
    
    // set ruls for form filed
    $this->ci->form_validation->set_rules('bankAccountName', 'account name', 'trim|required');
    $this->ci->form_validation->set_rules('eventBank', 'bank', 'trim|required');
    $this->ci->form_validation->set_rules('bankLocation', 'location', 'trim|required');
    //$this->ci->form_validation->set_rules('eventBSB', 'BSB', 'trim|required');
    $this->ci->form_validation->set_rules('eventAccountNumber', 'account number', 'trim|required');
  }
  
  // ------------------------------------------------------------------------ 
  
  /*
   * @access: private
   * @description: This function is used to save bank details 
   * @return void
   * 
   */ 
  
  private function _bankDetailsSave($eventId){
    
    $getData['event_id']    = $this->ci->input->post('eventId');
    $getData['account_name']  = $this->ci->input->post('bankAccountName');
    $getData['bank']      = $this->ci->input->post('eventBank');
    $getData['bank_location']   = $this->ci->input->post('bankLocation');
    $getData['bsb']       = $this->ci->input->post('eventBSB');
    $getData['account_number']  = $this->ci->input->post('eventAccountNumber');
    
    // check contact person details by event id
    $where     = array('event_id'=>$eventId);
    $countResult = $this->ci->common_model->countResult('event_bank_detail',$where);
    //update here
    if($countResult > 0){
      $this->ci->common_model->updateDataFromTabel('event_bank_detail',$getData,$where);
      $msg = lang('event_msg_event_bank_details_saved');
    }else{
      //insert here
      $this->ci->common_model->addDataIntoTabel('event_bank_detail', $getData);
      $msg = lang('event_msg_event_bank_details_saved');
    }
    return $msg;
  }
  
  
  // ------------------------------------------------------------------------ 
  
  /*
   * @access: private
   * @description: This function is used to insert and update term condition 
   * @return void
   * 
   */ 
  
  private function insertupdatetermscondition(){
    
    //call function for validate
    $this->_termsConditionValidate();
    
    //get ajax request
    $is_ajax_request = $this->ci->input->is_ajax_request();
    //get eventId
    $eventId     = $this->ci->input->post('eventId');
    
    if ($this->ci->form_validation->run($this->ci))
    {
      //call function for save
      $termArray   = $this->_termsConditionSave($eventId);
      $msg     = $termArray['msg'];
      $termid    = $termArray['termid'];
      $url     = $termArray['url'];
      
      // check ajax post request
      if($is_ajax_request){
        echo json_encode(array('msg'=>$msg,'is_success'=>'true','termid'=>$termid,'url'=>$url));
      }else{
        $msgArray = array('msg'=>$msg,'is_success'=>'true');
        set_global_messages($msgArray);
        redirect('event/eventdetails/'.encode($eventId));
      } 
      
    }else{
      $errors = $this->ci->form_validation->error_array();
      if($is_ajax_request){
        echo json_message('msg',$errors,'is_success','false');
      }else{
        // load event form view
        $this->eventshowview($eventId);
      }
    } 
  } 
  
  // ------------------------------------------------------------------------ 
  
  /*
   * @access: private
   * @description: This function is used to validate term condition 
   * @return void
   * 
   */ 
  
  private function _termsConditionValidate(){
  
    //set validation term
    if($this->ci->input->post('termSelection')=='0'){
      $this->ci->form_validation->set_rules('eventnTermsCondition', 'term condition', 'trim|required');
    }else{
      $this->ci->form_validation->set_rules('eventnTermsCondition', 'term condition', 'trim');
    }
  }
  
  // ------------------------------------------------------------------------ 
  
  /*
   * @access: private
   * @description: This function is used to save term condition 
   * @param: $eventId (int)
   * @return: $termArray
   * 
   */ 
  
  private function _termsConditionSave($eventId){
    
    //prepare data
    $getData['event_id']     = $this->ci->input->post('eventId');
    $getData['term_conditions']  = $this->ci->input->post('eventnTermsCondition');
    
    // check contact person details by event id
    $where = array('event_id'=>$eventId);
    $getResult = $this->ci->common_model->getDataFromTabel('event_term_conditions','id',$where);
    $countResult = count($getResult);
    //update here
    if(!empty($getResult) && $countResult > 0){
      $this->ci->common_model->updateDataFromTabel('event_term_conditions',$getData,$where);
      $msg = lang('event_msg_event_term_condition_saved');
      $termid = $getResult[0]->id;
    }else{
      //insert here
      $insertId = $this->ci->common_model->addDataIntoTabel('event_term_conditions', $getData);
      $msg = lang('event_msg_event_term_condition_saved');
      $termid = $insertId;
    }
    $termArray = array('msg'=>$msg,'termid'=>$termid,'url'=>'event/eventdetails/');
    return    $termArray;
  }
  
  /*
  * @description: This function is used to save corporate floor plan
  * @return void
  * 
    */ 
  private function addcorporatefloorplan(){
    
    
    // check validation
    $this->_addcorporatefloorplanValidate();
    if($this->ci->form_validation->run($this->ci)){
      // check floor plan count
      $totalSpaceAvailable = $this->ci->input->post('refr_no');
      $eventId           = $this->ci->input->post('eventId');
      // get used space   
        $usedSpace = $this->ci->event_model->getCorporateUsedSpace(array('event_id'=>$eventId)); 
        $usedSpace = (!empty($usedSpace)) ? $usedSpace->used_space : 0;
        if($usedSpace>$totalSpaceAvailable){
        $msg = lang('corporate_floor_paln_space_type_space_error');
        $returnArray = array('msg'=>$msg,'is_success'=>'false','url'=>'event/corporatedetails');
        echo json_encode($returnArray);
      }else{
        $this->_savecorporatefloorplan();
      } 
    }   
    else{
      
      $errors = $this->ci->form_validation->error_array();
      echo json_message('msg',$errors,'is_success','false');
      
    }   
  }
  
    /*
   * @access:    private
   * @description: This function is used to validate add event 
   * @return void
   * 
   */ 
  
  private function _addcorporatefloorplanValidate(){
    
    // set ruls for form filed
    $this->ci->form_validation->set_rules('exhibitionSpace', 'Exhibition Space', 'trim|required');
    $this->ci->form_validation->set_rules('refr_no', 'Total Spaces Available', 'trim|required|greater_than[1]');
  
  }
  
  /*
   * @description: This function is used to save corporate floor plan
   * @return void
   * 
    */
     
  private function _savecorporatefloorplan(){
    
      // get posted value
      $eventId           = $this->ci->input->post('eventId');
      $totalSpaceAvailable     = $this->ci->input->post('refr_no');
      $exhibitionSpace         = $this->ci->input->post('exhibitionSpace');
      $corporateFloorPlanId    = $this->ci->input->post('corporateFloorPlanId');
      
      $updateData['total_space_available']    = $totalSpaceAvailable;
      $updateData['event_id']             = $eventId;
      $updateData['is_exhibition_space']        = $exhibitionSpace;
      
      
      if($corporateFloorPlanId > 0){
        
        // update data id by id
        $upWhere = array('id'=>$corporateFloorPlanId);
        $this->ci->common_model->updateDataFromTabel('corporate_floor_plan',$updateData,$upWhere);
        $msg = lang('event_msg_exhibitor_floor_plan_updated');
      }
      else{
        // insert data id by id
        $floorPlanId = $this->ci->common_model->addDataIntoTabel('corporate_floor_plan', $updateData);
        $msg = lang('event_msg_exhibitor_floor_plan_added');
      } 
        
        
      $returnArray = array('msg'=>$msg,'is_success'=>'true','url'=>'event/corporatedetails');
      echo json_encode($returnArray);
  
  }
  
  /*
    * @description: This function is used to add/Update Corporate Space type
    * @return void
    * 
    */ 
  
  public function addupdatecorporatespacetype(){
    
    $this->_addCorporateSpaceValidate();
    if($this->ci->form_validation->run($this->ci)){
      
      // Store Collapse Value in Session to open the next form
      $this->ci->session->set_userdata('openCollapseValue',$this->ci->input->post('collapseValue'));
      
      $floorPlanTypeId   = decode($this->ci->input->post('floorPlanTypeId')); 
      // check this id exist if exit then update records
      $spaceTypeRecord  = $this->ci->event_model->getCorporateSpaceTypeDetails(array('floor_plan_type_id'=>$floorPlanTypeId)); 
      
      $insertUpdateData['space_type']       = trim($this->ci->input->post('space_name'));
      $insertUpdateData['total_space_available']  = trim($this->ci->input->post('space_ava'));
      $insertUpdateData['event_id']         = $this->ci->input->post('eventId'); 
      $space                    = $this->ci->input->post('space'); // post as array
       
      if(!empty($space)){
        
        $insertUpdateData['available_booth']   = serialize($space); 
        
        if(!empty($spaceTypeRecord)){
        
          if($this->ci->event_model->updateCorporateSpaceType($insertUpdateData,$floorPlanTypeId)){
            echo json_encode(array('msg'=>lang('corporate_floor_paln_space_type_updated'),'is_success'=>'true','url'=>'event/corporatedetails/'));
          }else{
            $errors = lang('comm_error');
            echo json_message('msg',$errors,'is_success','false');
          }
              
        }else{
            
            $spaceTypeId = $this->ci->event_model->insertCorporateSpaceType($insertUpdateData);
            if(!empty($spaceTypeId)){
              echo json_encode(array('msg'=>lang('corporate_floor_paln_space_type_save'),'is_success'=>'true','url'=>'event/corporatedetails/'));
            }else{
              $errors = lang('comm_error');
              echo json_message('msg',$errors,'is_success','false');
            } 
         }  
      
      }else{
          $errors = lang('space_booth_required');
          echo json_message('msg',$errors,'is_success','false');
      }    
    }else{
      $errors = $this->ci->form_validation->error_array();
      echo json_message('msg',$errors,'is_success','false');
    }
    
  }
  
  
  /*
   * @access:    private
   * @description: This function is used to validate add event 
   * @return void
   * 
   */ 
  
  private function _addCorporateSpaceValidate(){
    
    // set ruls for form filed
    $this->ci->form_validation->set_rules('space_name', 'Space Name', 'trim|required');
    $this->ci->form_validation->set_rules('space_ava', 'Space available', 'trim|required');
    $this->ci->form_validation->set_rules('space[]', 'Space booth no.', 'required');
  
  }
  
  function savedeafultPackageBenefit($eventId){
    $errors = lang('corporate_package_benefit_save_succ');
    echo json_encode(array('msg'=>$errors,'is_success'=>'true','url'=>'event/corporatedetails'));
  }
  
  function savedeafultAdditionalPurchaseItem($eventId){
    $errors = lang('corporate_package_benefit_save_succ');
    echo json_encode(array('msg'=>$errors,'is_success'=>'true','url'=>'event/corporatedetails'));
  }
    
    /*
   * @description: This function is used to save corporate default benefit
   * @return void
   * 
    */ 
    
  public function insertupdatecorporatedefaultbenefit($eventId){
    
    
    $this->_addCorporateDefaultPackageValidate();
    if($this->ci->form_validation->run($this->ci)){
      
      $benefits       = $this->ci->input->post('benefits'); // post as array
      $packageBenefitId   = $this->ci->input->post('packageBenefitId'); // post as array
      $packageId      = $this->ci->input->post('packageId'); // post as array
        $packageId      = ($packageId!="") ? $packageId : 0;
      if($packageBenefitId!=""){
        
        $updateData     = array('benefit_name'=>$benefits[0],'corporate_package_id'=>$packageId);
        $packageBenefitId = decode($packageBenefitId);
        $result =  $this->ci->event_model->updatecorporatedefaultbenefit($updateData,$packageBenefitId);
        
      }else{
      
        foreach($benefits as $value){
            if($value!=""){ 
            $insertData[] = array('benefit_name'=>$value,'event_id'=>$eventId,'corporate_package_id'=>$packageId);    
             }
         } //end loop
          $result = $this->ci->event_model->insertcorporatedefaultbenefit($insertData);
      } // end else
      
      if($result){
          $errors = ($packageId==0) ?  lang('corporate_benefit_save_succ') : lang('corporate_package_benefit_save_succ') ;
          echo json_encode(array('msg'=>$errors,'is_success'=>'true','url'=>'event/corporatedetails'));
      }else{
        $errors = lang('comm_error');
        echo json_message('msg',$errors,'is_success','false');
      }
      
    }else{
        $errors = $this->ci->form_validation->error_array();
        echo json_message('msg',$errors,'is_success','false');
     }  
  }
  
  /*
  * @description: This function is used to save corporate default package benefit on form submit
  * @return void
  * 
  */ 
  public function insertupdatedefaultPackageBenefit($eventId){
    $packageId      = 0;
    // Custom contact type
    $defaultBenefits  = $this->ci->input->post('benefits');
     //store custom contacts
    if(!empty($defaultBenefits)){
      foreach($defaultBenefits as $key=>$value){
        $insertData[] = array('benefit_name'=>$value,'event_id'=>$eventId,'corporate_package_id'=>$packageId);        
      }//end loop      
      $result = $this->ci->event_model->insertcorporatedefaultbenefit($insertData);
    }
    $msg = lang('corporate_benefit_save_succ');
    echo json_message('msg',$msg);
  }
  
  /*
   * @access:    private
   * @description: This function is used to validate default package benefit
   * @return void
   * 
   */ 
  
  private function _addCorporateDefaultPackageValidate(){
    
    // set ruls for form filed
    $this->ci->form_validation->set_rules('benefits[]', 'Benefits', 'required');
  
  }
  
  
   /*
   * @description: This function is used to save corporate default purchase item
   * @return void
   * 
    */ 
  public function insertupdatedefaultpurchaseitem($eventId){
    
    $this->_addCorporateDefaultPurchaseValidate();
    
    if($this->ci->form_validation->run($this->ci)){
        
        // get all posted value
        $purchaseitemid               = $this->ci->input->post('purchaseitemid');
        $packageId                  = $this->ci->input->post('packageId');
        $packageId                  = ($packageId!="") ? $packageId : 0;
        
        $insertupdatedata['event_id']       = $eventId;
        $insertupdatedata['corporate_package_id'] = $packageId;
        $insertupdatedata['name']           = trim($this->ci->input->post('purchase_name'));
        $insertupdatedata['item_limit']       = trim($this->ci->input->post('purchase_limit'));
        $insertupdatedata['total_price']      = trim($this->ci->input->post('purchase_price'));
        $insertupdatedata['gst_include_price']    = trim($this->ci->input->post('purchase_price_gst'));
        
        if($purchaseitemid!=""){ // updated record
          $result = $this->ci->event_model->updatecorporatedefaultpackageItem($insertupdatedata,decode($purchaseitemid));
        }else{ // insert record
          $result = $this->ci->event_model->insertcorporatedefaultpackageItem($insertupdatedata);
        } 
        
        if($result){
           $errors = lang('corporate_purchased_item_save_succ');
           echo json_encode(array('msg'=>$errors,'is_success'=>'true','url'=>'event/corporatedetails'));
        }else{
          $errors = lang('comm_error');
          echo json_message('msg',$errors,'is_success','false');
        }
        
        
    }else{
        $errors = $this->ci->form_validation->error_array();
        echo json_message('msg',$errors,'is_success','false');
    }
  }
  
  
    
  /*
   * @access:    private
   * @description: This function is used to validate default purchase item 
   * @return void
   * 
   */ 
  
  private function _addCorporateDefaultPurchaseValidate(){
    
    // set ruls for form filed
    $this->ci->form_validation->set_rules('purchase_name', 'Name', 'trim|required');
    $this->ci->form_validation->set_rules('purchase_limit', 'Limit', 'trim|required');
    $this->ci->form_validation->set_rules('purchase_price', 'Total Price', 'trim|required');
    //$this->ci->form_validation->set_rules('purchase_price_gst', 'GST Include', 'trim|required');
  
  }
  
  
  
  /*
   * @description: This function is used to save corporate package
   * @return void
   * 
    */ 
    
  public function addeditcorporatepackage(){
    
    $this->_addCorporatePackageValidate();
    if($this->ci->form_validation->run($this->ci)){
      
      $data['corporate_package_name'] = $this->ci->input->post('corporate_package_name');
      $data['category_id']      = $this->ci->input->post('corporate_package_id');
      $data['event_id']         = $this->ci->input->post('eventId');
      $packageId            = $this->ci->input->post('packageId');
      $isCopyAction           = $this->ci->input->post('isCopyAction');
       
       // check action request for copy or add or edit 
        if($isCopyAction=='1'){
          
            $newpackageId = $this->ci->event_model->copycorporatecategory($data,$packageId);    
            //$result = $this->ci->event_model->updatecorporatepackage($data,array('id'=>$packageId));
            if($newpackageId){
              
              //copy benefit or purchase item
              $this->ci->event_model->copybenefitpurchaseitem($newpackageId,$packageId);
              
              $errors = lang('corporate_package_save_succ');
              echo json_encode(array('msg'=>$errors,'is_success'=>'true','url'=>$this->ci->uri->uri_string()));
            }else{
              $errors = lang('comm_error');
              echo json_message('msg',$errors,'is_success','false');
            }       
        }else{
        
          
          
          if(!empty($packageId)){
            $result = $this->ci->event_model->updatecorporatepackage($data,array('id'=>$packageId));
            if($result){
              $this->addRegistrantAsCorporate($data['event_id'],$data['category_id'],$packageId);
              
              $errors = lang('corporate_package_save_succ');
              echo json_encode(array('msg'=>$errors,'is_success'=>'true','url'=>$this->ci->uri->uri_string()));
            }else{
              $errors = lang('comm_error');
              echo json_message('msg',$errors,'is_success','false');
            }
          }else{
            $result = $this->ci->event_model->addcorporatepackage($data);
            if($result){
              $errors = lang('corporate_package_save_succ');
              echo json_encode(array('msg'=>$errors,'is_success'=>'true','url'=>$this->ci->uri->uri_string()));
            }else{
              $errors = lang('comm_error');
              echo json_message('msg',$errors,'is_success','false');
            }
          } 
          
      }
    
    }else{
          $errors = $this->ci->form_validation->error_array();
          echo json_message('msg',$errors,'is_success','false');
    } 
  }
  
  
  /*
   * @access:    private
   * @description: This function is used to validate Corporate package
   * @return void
   * 
   */ 
  
  private function _addCorporatePackageValidate(){
    
    // set ruls for form filed
    $this->ci->form_validation->set_rules('corporate_package_name', 'Name', 'trim|required');
    $this->ci->form_validation->set_rules('corporate_package_id', 'Category', 'trim|required');
  
  }
  
   /*
   * @description: This function is used to save corporate application 
   * @return void
   * 
    */ 
  
  public function corporateApplicationDetails($eventId){
    
     $filedId = $this->ci->input->post('fieldid');
     if(is_array($filedId)){
       foreach($filedId as $value){
      $update[]     = array('id'=>$value,'field_status'=>$this->ci->input->post('field_'.$value)); 
     }
     
     $result = $this->ci->event_model->updatecorporateApplicationDetails($update,array('event_id'=>$eventId));
     if($result){
      echo json_encode(array('msg'=>lang('corporate_app_detail_updated_succ'),'is_success'=>'true','url'=>'event/corporatedetails'));
     }else{
      echo json_message('msg',lang('comm_error'),'is_success','false');
     }
     
     }else{
    echo json_message('msg',lang('comm_error'),'is_success','false');  
     }
  }
  
  
  /*
   * @description: This function is used to save corporate package Details form
   * @return void
   * 
    */ 
  
  public function addeditcorporatepackageDetails(){
    
    
    //get package id
    $packageId = decode($this->ci->input->post('package_id'));
    if(!empty($packageId)){ 
      //validate corporate package
      $this->_addCorporatePackageDetailValidate($packageId);
      if($this->ci->form_validation->run($this->ci)){
       $this->_addeditcorporatepackageDetails($packageId);
      }else{
      $errors = $this->ci->form_validation->error_array();
      echo json_message('msg',$errors,'is_success','false'); 
      }
    }else{
      $errors = lang('comm_error');
      echo json_message('msg',$errors,'is_success','false'); 
    }
    
  }
  
  /*
   * @access :   public
   * @description: This function is used to delete corporate package
   * @return void
   */
  public function deletecorporatepackage(){
     $deleteId = $this->ci->input->post('deleteId');
       $result=$this->ci->event_model->deletecorporatepackage(array('id'=>$deleteId));
       if($result){
       echo json_message('msg',lang('corporate_package_deleted_succ'),'is_success','true');
     }else{
       echo json_message('msg',lang('comm_error'),'is_success','false');
     }
    
  }
  
  //----------------- Registrant Type Section -------------------------------
  
  // ------------------------------------------------------------------------ 
  
  /*
   * @access:    private
   * @description: This function is used to insert and update master personal details form
   * all field save one by one by loop
   * @return void
   * 
   */ 
  
  private function insertupdatemasterpersonaldetails(){
    
    //get ajax request
    $is_ajax_request = $this->ci->input->is_ajax_request();
    //get eventId
    $eventId     = $this->ci->input->post('eventId');
    
    if ($this->ci->input->post())
    {
      //update personal details form
      $arrayVal     =  $this->ci->input->post('fieldid');
      
      //update field value
      $arrayFieldVal  =  $this->ci->input->post();
      
      //update field status one by one
      foreach($arrayVal as $fieldid){
        
        $filedRowId = 'field_'.$fieldid;
        $field_status = $arrayFieldVal[$filedRowId];
        $updateData['field_status'] = $field_status;
        $where = array('id'=>$fieldid);
        $this->ci->common_model->updateDataFromTabel('custom_form_fields',$updateData,$where);
      }
      
      $msg = lang('event_msg_personal_details_form_saved');
      $arrayReturn = array('msg'=>$msg,'is_success'=>'true');
      echo json_encode($arrayReturn);
    }
    else
    {
      $errors =$this->ci->form_validation->error_array();
      if($is_ajax_request){
        echo json_message('msg',$errors,'is_success','false');
      }else{
        // load registrant form view
        $this->registrantview($eventId);
      }
    } 
  }
  
  // ------------------------------------------------------------------------ 
  
  /*
   * @access:    public
   * @description: This function is used to edit and update custome field in personal form
   * @return void
   * 
   */ 
   
  public function addeditcustomefieldsave(){

    //set validation
    $this->ci->form_validation->set_rules('fieldName', 'field name', 'trim|required');
  
    if ($this->ci->form_validation->run($this->ci)){
      $eventId      = $this->ci->input->post('eventId');
      $fieldName      = $this->ci->input->post('fieldName');
      $fieldRegistantId   = ($this->ci->input->post('fieldRegistantId')!="") ? $this->ci->input->post('fieldRegistantId') : $this->ci->input->post('registrantId');
      $customFieldId    = $this->ci->input->post('customFieldId');
      $fieldType      = $this->ci->input->post('fieldType');
      $defaultValue   = NULL;
      if($this->ci->input->post('defaultValue') && ($fieldType=="selectbox" || $fieldType=="radio")){
        $defaultValue = json_encode($this->ci->input->post('defaultValue'),JSON_FORCE_OBJECT);
      }
      //this value commmon for add and edit case
      $insertData['field_name']     = strtolower($fieldName);
      $insertData['field_type']     = $fieldType;
      $insertData['default_value']  = $defaultValue;
      $insertData['form_id']      = ($this->ci->input->post('form_id')!="") ? $this->ci->input->post('form_id') : "1";
      $insertData['event_id']     = $eventId;
      $insertData['registrant_id']  = $fieldRegistantId;
      
      
      // udpate custome field data
      if($customFieldId > 0){
        $where = array('id'=>$customFieldId);
        $this->ci->common_model->updateDataFromTabel('custom_form_fields',$insertData,$where);
        $msg = lang('event_msg_custom_field_updated');
        $returnId = $customFieldId;
        $saveType = 'update';
      }else{
        //this value set only create field time
        $insertData['field_status'] = '1';
        $insertData['is_editable'] = '1';
        $insertData['field_create_by'] = '0';
        // data custome field data
        $insertId = $this->ci->common_model->addDataIntoTabel('custom_form_fields', $insertData);
        $msg = lang('event_msg_custom_field_add');
        $returnId = $insertId;
        $saveType = 'insert';
      }
      
      
      $fieldsmastervalue = fieldsmastervalue();      
      $returnArray=array('msg'=>$msg,'is_success'=>'true','url'=>$this->ci->uri->uri_string(),'id'=>$returnId,'fieldsmastervalue'=>$fieldsmastervalue,'saveType'=>$saveType); 
      //set_global_messages($returnArray);
      echo json_encode($returnArray);
    }else{
      $errors = $this->ci->form_validation->error_array();
      echo json_message('msg',$errors,'is_success','false');
    }
  } 
  
    // ------------------------------------------------------------------------ 
  
  /*
   * @access:    private
   * @description: This function is used to insert and update registrant details forms data 
   * @return void
   * 
   */ 
  
  private function insertupdateregistranttypes(){
    
    if($this->ci->input->post()){
      //get form registrant id
      $registrantId = $this->ci->input->post('registrantId');
      
      //get eventId
      $eventId = $this->ci->input->post('eventId');
      
      //call function for validate
      $this->_registrantValidate($registrantId,$eventId);
      
      //call function for registrant validate
      $this->_registrantDaywiseValidate($registrantId);
      
      //get ajax request
      $is_ajax_request = $this->ci->input->is_ajax_request();
      
      if ($this->ci->form_validation->run($this->ci))
      {
        //call function for registrant data save
        $this->_registrantSave($registrantId,$eventId);
        
        //call function daywise for save 
        $this->_registrantDaywiseSave($registrantId);
        
        //call personal details form save
        $this->_insertupdatepersonaldetails();
        
        //set message save
        $msg = lang('event_msg_registration_form_details_saved');
        
        // check ajax request
        if($is_ajax_request){            
            //echo json_message('msg',$msg);          
            $returnArray=array('msg'=>$msg,'is_success'=>'true','url'=>$this->ci->uri->uri_string()); 
            //set_global_messages($returnArray);
            echo json_encode($returnArray);
        }else{
            $msgArray = array('msg'=>$msg,'is_success'=>'true');
            set_global_messages($msgArray);
            redirect('event/eventdetails/'.encode($eventId));
        }
          
      }else{
        $errors = $this->ci->form_validation->error_array();
        if($is_ajax_request){
          echo json_message('msg',$errors,'is_success','false');
        }else{
          // load event form view
          $this->eventshowview($eventId);
        }
      } 
    }else{
        $msg = lang('event_msg_invalid_data_request');
        $msgArray = array('msg'=>$msg,'is_success'=>'false');
        set_global_messages($msgArray);
        redirect(base_url('dashboard'));
      }
  }
  
  
  
    
  // ------------------------------------------------------------------------ 
  
  /*
   * @access:    private
   * @description: This function is used to add master field to user custome form fields
   * @return void;
   * @param1 = eventId
   * @param2 = formId
   * @param3 = registrantId
   */
   
  private function addformfields($eventId,$formId,$registrantId){
      
      $where = array('event_id'=>$eventId,'registrant_id'=>'0');
      $formfieldmaster = $this->ci->common_model->getDataFromTabel('custom_form_fields','*',$where);
      if(!empty($formfieldmaster)){
        foreach($formfieldmaster as $fieldmaster){
          $arrayVal = $this->ci->input->post();
          $filedId = 'field_'.$fieldmaster->id;
          $insertData['field_name'] = $fieldmaster->field_name;
          $insertData['field_type'] = $fieldmaster->field_type;
          $insertData['default_value'] = $fieldmaster->default_value;
          $insertData['form_id'] = $fieldmaster->form_id;
          $insertData['is_editable'] = $fieldmaster->is_editable;
          $insertData['event_id'] = $eventId;
          $insertData['registrant_id'] = $registrantId;
          $insertData['field_status'] = $fieldmaster->field_status;
          $insertData['field_create_by'] = $fieldmaster->field_create_by;
          $insertId = $this->ci->common_model->addDataIntoTabel('custom_form_fields', $insertData);
        }
      }
  }
  
  // ------------------------------------------------------------------------ 
  
  /*
   * @access:    private
   * @description: This function is used to insert and update personal details form
   * all field save one by one by loop
   * @return void
   * 
   */ 
  
  private function _insertupdatepersonaldetails(){
  
    if ($this->ci->input->post())
    {
      //update personal details form
      $arrayVal     =   $this->ci->input->post('fieldid');
      
      //update field value
      $arrayFieldVal  =   $this->ci->input->post();
      
      //update field status one by one
      foreach($arrayVal as $fieldid){
        
        $filedRowId = 'field_'.$fieldid;
        $field_status = $arrayFieldVal[$filedRowId];
        $updateData['field_status'] = $field_status;
        $where = array('id'=>$fieldid);
        $this->ci->common_model->updateDataFromTabel('custom_form_fields',$updateData,$where);
      }
    }
      
  }
  
  
    
  // ------------------------------------------------------------------------ 
  
  /*
   * @access:    private
   * @description: This function is used to validate registrant  
   * @param1: $registrantId (int)
   * @return: void
   */  
  
  private function _registrantValidate($registrantId,$eventId){
    
    //set all form validation
    //$this->form_validation->set_rules('additionalDetails'.$registrantId, 'additional details', 'trim|required');
    $this->ci->form_validation->set_rules('registrantCategory'.$registrantId, 'Category', 'trim|required');
    $this->ci->form_validation->set_rules('limit'.$registrantId, 'limit', 'trim|required|numeric');
    
    if(!$this->ci->input->post('comaplimentary'.$registrantId)){
      $this->ci->form_validation->set_rules('totalPrice'.$registrantId, 'Total price', 'trim|required|numeric');
      //$this->ci->form_validation->set_rules('GSTInclude'.$registrantId, 'GST include', 'trim|required|numeric');
    }
    
    //echo '<pre>';print_r($this->ci->input->post());die;
    
    //if check then set access password then insert password
    if($this->ci->input->post('passwordAccess'.$registrantId)=='1'){
      $this->ci->form_validation->set_rules('password'.$registrantId, 'password', 'trim|required|matches[confirmpassword'.$registrantId.']');
      $this->ci->form_validation->set_rules('confirmpassword'.$registrantId, 'confirm password', 'trim|required');
    } 
    
    //check register limit status get limit array
    $limitArrayData        = $this->_getRegistrantsLimit($eventId,$registrantId); // call get limit function
    $totalEventRegisterLimit   = $limitArrayData['totalEventRegisterLimit']; // total limit
    $totalRegistrantsLimitIndb   = $limitArrayData['totalRegistrantsLimitIndb']; // get total limit in db for all registrant
    $registrantIdFieldLimit    = $limitArrayData['registrantIdFieldLimit']; // get limit save for this registrant
    $enteredLimit          = $this->ci->input->post('limit'.$registrantId);// user entered limit in in the field
    
    //Check limit availability and set custome error set
    if($enteredLimit  <= $totalEventRegisterLimit){
      if($totalRegistrantsLimitIndb != "0"){
        $getTotalLimitAvailable = $totalEventRegisterLimit   - $totalRegistrantsLimitIndb;
        $curretnRigisLimitdb    = $totalRegistrantsLimitIndb - $registrantIdFieldLimit;
        $avalimitForThisRegis   = $registrantIdFieldLimit    + $getTotalLimitAvailable;
        if($avalimitForThisRegis < $enteredLimit){
          $this->ci->form_validation->set_rules('limittotal', 'The regisrant entered limit is exceed form total limit.', 'callback_custom_error_set');
        }
      }
    }else{
      $this->ci->form_validation->set_rules('limittotal', 'The regisrant limit entered is exceed form total limit.', 'callback_custom_error_set');
    }
    
    /*---------check early bird field validation-------------------*/
      $where = array('id'=>$eventId);
      $getEventDataForEarlyData = $this->ci->common_model->getDataFromTabel('event','starttime',$where);
      
      if($this->ci->input->post('earlyBirdDate'.$registrantId)){
        if(!empty($getEventDataForEarlyData)){
          $getEventDataForEarlyData = $getEventDataForEarlyData[0];
          $earlyBirdDataDataSrttotime   = strtotime($getEventDataForEarlyData->starttime);  
          $earlyBirdDataSrttotime   = strtotime($this->ci->input->post('earlyBirdDate'.$registrantId)); 
          if($earlyBirdDataDataSrttotime < $earlyBirdDataSrttotime){
            $this->ci->form_validation->set_rules('earlybirddateselector', 'Early bird date should be less than from event start date.', 'callback_custom_error_set');
          }
        }
      }
    /*---------check early bird field validation-------------------*/
    
    if($this->ci->input->post('allowEarlybird'.$registrantId)){
      //$this->ci->form_validation->set_rules('earlyBirdDate'.$registrantId, 'early bird date', 'trim|required');
      //$this->ci->form_validation->set_rules('earlyBirdUnitPrice'.$registrantId, 'early bird unit price', 'trim|required|numeric');
      $this->ci->form_validation->set_rules('earlyBirdLimit'.$registrantId, 'early bird limit', 'trim|required|numeric');
      $this->ci->form_validation->set_rules('earlyBirdPrice'.$registrantId, 'early bird total price', 'trim|required|numeric');
      
      $earlyBirdLimit          = $this->ci->input->post('earlyBirdLimit'.$registrantId);
      if($earlyBirdLimit > $enteredLimit){
        $this->ci->form_validation->set_rules('earlyBirdLimitCheck', 'The early bird limit should be less than from total limit.', 'callback_custom_error_set');        
      }
    }
    
    $this->ci->form_validation->set_rules('singleDayRegistration'.$registrantId, 'single day registration', 'trim|required');
    //$this->ci->form_validation->set_rules('registrationDisplayed'.$registrantId, 'email', 'trim|required');
    
    //set form validation if checked  registrantGroupbookings
    if($this->ci->input->post('registrantGroupbookings'.$registrantId)){
      //$this->ci->form_validation->set_rules('registrantGroupBookingsSize'.$registrantId, 'group bookings size', 'trim|required|numeric');
      //$this->ci->form_validation->set_rules('registrantGroupBookingsPercent'.$registrantId, 'group bookings percent', 'trim|required|numeric');
    }
    
    //set form validation if checked  group_booking_allowed_for_singleday_registrant
    if($this->ci->input->post('singleDayGroupbookings'.$registrantId)){
      $this->ci->form_validation->set_rules('singleDayGroupbookings'.$registrantId, 'single day group bookings', 'trim|required|numeric');
      $this->ci->form_validation->set_rules('singleDayGroupBookingsPercent'.$registrantId, 'single day group bookings percent', 'trim|required|numeric');
    }
  } 
  
  
  // ------------------------------------------------------------------------ 
  
  /*
   * @access:    private
   * @description: This function is used to validate registrant  
   * @param1: $registrantId (int)
   * @return: void
   */  
  
  private function _registrantSave($registrantId,$eventId){
    
    //value asign in the variable
    $singleDayRegistration  = $this->ci->input->post('singleDayRegistration'.$registrantId);
    
    $updateData['additional_details']            = $this->ci->input->post('additionalDetails'.$registrantId);
    $updateData['registrants_limit']             = $this->ci->input->post('limit'.$registrantId);
    $updateData['registrants_limit_early_bird']  = $this->ci->input->post('earlyBirdLimit'.$registrantId);
    $updateData['unit_price']                    = $this->ci->input->post('unitPrice'.$registrantId);    
    $updateData['option_for_single_day_registration']  = $singleDayRegistration;
    $updateData['allow_earlybird']               = ($this->ci->input->post('allowEarlybird'.$registrantId))?$this->ci->input->post('allowEarlybird'.$registrantId):"0";    
    $updateData['category_id']                   = ($this->ci->input->post('registrantCategory'.$registrantId))?$this->ci->input->post('registrantCategory'.$registrantId):"0";
    
    
    $updateData['complementry']                   = ($this->ci->input->post('comaplimentary'.$registrantId))?$this->ci->input->post('comaplimentary'.$registrantId):"0";
    if($this->ci->input->post('comaplimentary'.$registrantId)){
      //set null if not checked
      $updateData['total_price']          = NULL;
      $updateData['gst']                  = NULL;
      $updateData['total_price_inc_gst']  = NULL;
    }else{
      $updateData['total_price']           = $this->ci->input->post('totalPrice'.$registrantId);
      $updateData['gst']                   = $this->ci->input->post('GSTInclude'.$registrantId);
      $updateData['total_price_inc_gst']   = $this->ci->input->post('totalPriceIncGST'.$registrantId);
    }
    
    
    
    
    
    //if check then set access password then insert password
    if($this->ci->input->post('passwordAccess'.$registrantId)=='1'){
      $updateData['password']  =  $this->ci->input->post('password'.$registrantId);
    }else{
      $updateData['password']  = NULL;
    } 
    
    //check early bird and update data
    if($this->ci->input->post('allowEarlybird'.$registrantId)){
      $updateData['earlybird_date']                   = dateFormate($this->ci->input->post('earlyBirdDate'.$registrantId),'Y-m-d');
      $updateData['unit_price_early_bird']            = $this->ci->input->post('earlyBirdUnitPrice'.$registrantId);
      $updateData['total_price_early_bird']           = $this->ci->input->post('earlyBirdPrice'.$registrantId);
      $updateData['gst_early_bird']                   = $this->ci->input->post('earlyGSTInclude'.$registrantId);
      $updateData['total_price_early_bird_inc_gst']   = $this->ci->input->post('earlyBirdPriceIncGST'.$registrantId);
    }else{
      //set null if not checked
      $updateData['earlybird_date']                 = NULL;
      $updateData['unit_price_early_bird']          = NULL;
      $updateData['total_price_early_bird']         = NULL;
      $updateData['gst_early_bird']                 = NULL;
      $updateData['total_price_early_bird_inc_gst'] = NULL;
    }
    
    //get data if checked registrantGroupbookings
    if($this->ci->input->post('registrantGroupbookings'.$registrantId)){
      $updateData['group_booking_allowed']                = $this->ci->input->post('registrantGroupbookings'.$registrantId);
      $updateData['min_group_booking_size']                 = $this->ci->input->post('registrantGroupBookingsSize'.$registrantId);
      $updateData['percentage_discount_for_group_booking']  = $this->ci->input->post('registrantGroupBookingsPercent'.$registrantId);
    }else{
      $updateData['group_booking_allowed']                = 0;
      $updateData['min_group_booking_size']                 = '';
      $updateData['percentage_discount_for_group_booking']  = '';
    }
    
    //get data if checked singleDayGroupbookings
    if($this->ci->input->post('singleDayGroupbookings'.$registrantId)){
      $updateData['group_booking_allowed_for_singleday_registrant']      = $this->ci->input->post('singleDayGroupbookings'.$registrantId);
      $updateData['min_group_booking_size_for_single_day_registrant']    = $this->ci->input->post('registrantSingleDayGroupBookingsSize'.$registrantId);
      $updateData['percentage_discount_for_single_day_registrant']       = $this->ci->input->post('singleDayGroupBookingsPercent'.$registrantId);
    }else{
      $updateData['group_booking_allowed_for_singleday_registrant']      = 0;
      $updateData['min_group_booking_size_for_single_day_registrant']    = '';
      $updateData['percentage_discount_for_single_day_registrant']       = '';
    }
    
    $whereRegis = array('id'=>$registrantId); 
    $this->ci->common_model->updateDataFromTabel('event_registrants',$updateData,$whereRegis);
  }
  
  // ------------------------------------------------------------------------ 
  
  /*
   * @access:   private
   * @description: This function is used to validate registrant daywise 
   * @param1: $registrantId (int)
   * @return: void
   */  
  
  private function _registrantDaywiseValidate($registrantId){
    
    //get entered limit
    $enteredLimit          = $this->ci->input->post('limit'.$registrantId);// user entered limit in in the field
    
    if($this->ci->input->post('regis_daywiseid_'.$registrantId)){
      $getTotalSignleDayLimit = 0;
      $RegisDayWaiseidArray   = $this->ci->input->post('regis_daywiseid_'.$registrantId);
      $countDay=0;
      foreach($RegisDayWaiseidArray as $key=>$value){
        $dayOfRegis = $key; // key will be registraint day
        $regisDayId = $registrantId.'_'.$dayOfRegis;
        
        // check if registrantDay is checked then set validation
        if($this->ci->input->post('registrantDay'.$regisDayId)){
          if($this->ci->input->post('registrantDay'.$regisDayId)){
            $this->ci->form_validation->set_rules('registrantDayLimit'.$regisDayId, $dayOfRegis.' day limit', 'trim|required|numeric');
            //$this->ci->form_validation->set_rules('registrantDayUnitPrice'.$regisDayId, $dayOfRegis.' day unit price', 'trim|required|numeric');
          //  $this->ci->form_validation->set_rules('registrantGSTInclude'.$regisDayId, $dayOfRegis.' day GST include', 'trim|required|numeric');
            //$getTotalSignleDayLimit =  $getTotalSignleDayLimit + $this->ci->input->post('registrantDayLimit'.$regisDayId);
          }
        }
        
        // check if registrantEarlybirdDay is checked then set validation
        if($this->ci->input->post('registrantEarlybirdDay'.$regisDayId) ){
          if($this->ci->input->post('registrantEarlybirdDay'.$regisDayId)){
            //$this->ci->form_validation->set_rules('registrantEarlybirdDate'.$regisDayId, $dayOfRegis.' day early bird Date', 'trim|required');
            $this->ci->form_validation->set_rules('registrantEarlybirdDayLimit'.$regisDayId, $dayOfRegis.' day early bird Limit', 'trim|required');
            $this->ci->form_validation->set_rules('registrantEarlybirdDayUnitPrice'.$regisDayId, $dayOfRegis.' day early bird Day Unit Price', 'trim|required|numeric');
          //  $this->ci->form_validation->set_rules('registrantEarlybirdGSTInclude'.$regisDayId, $dayOfRegis.' day early bird GST include', 'trim|required|numeric');
          }
        } 
        
        
        /*---------check early bird date field validation-------------------*/
          if($this->ci->input->post('registrantEarlybirdDate'.$regisDayId)){
            $dateRegisidArray        = $this->ci->input->post('date_regisid_'.$registrantId);
            $singleDayRregiDayDate       = strtotime($dateRegisidArray[$countDay]); 
            $earlyBirdDataSrttotime    = strtotime($this->ci->input->post('registrantEarlybirdDate'.$regisDayId));  
            if($singleDayRregiDayDate < $earlyBirdDataSrttotime){
              $this->ci->form_validation->set_rules('earlybirddateselector'.$regisDayId, 'Single Day '.$dayOfRegis.' Early bird date should be less than from selected daywise date.', 'callback_custom_error_set');
            }
          }
        /*---------check early bird  date field validation-------------------*/
        
        
        /*---------check  daywise limit with entered registrant limit-------------------*/
          if($this->ci->input->post('registrantDay'.$regisDayId)){
            $getSignleDayLimit =  $this->ci->input->post('registrantDayLimit'.$regisDayId); 
            if($enteredLimit  < $getSignleDayLimit){
              $this->ci->form_validation->set_rules('totalsignledaylimit'.$dayOfRegis, 'The day '.$dayOfRegis.' limit is exceed form entered registrant limit '.$enteredLimit.'.', 'callback_custom_error_set');
            }
          }
        /*---------check  daywise limit with entered registrant limit-------------------*/
      
        $countDay++;  
      }
      
      //Check signle day total limit with entered limit for this registrant
      /*if($enteredLimit  < $getTotalSignleDayLimit){
        $this->ci->form_validation->set_rules('totalsignledaylimit', 'The single day regisrant total limit sum is exceed form '.$enteredLimit.' available.', 'callback_custom_error_set');
      }*/
      
    }
  } 
  
  // ------------------------------------------------------------------------ 
  
  /*
   * @access:   private
   * @description: This function is used to save registrant daywise 
   * @param1: $registrantId (int)
   * @return: void
   */  
  
  private function _registrantDaywiseSave($registrantId){
    
    //value asign in the variable
    $singleDayRegistration  = $this->ci->input->post('singleDayRegistration'.$registrantId);
    
    //if use is not checked single day registraint then all entered days date will be delete
    if($singleDayRegistration==0){
      //delete data from database
      $whereDelete = array('registrant_id'=>$registrantId);
      $this->ci->common_model->deleteRowFromTabel('event_registrant_daywise',$whereDelete);
    }else{
    
      //registrant daywise data update and insert
      if($this->ci->input->post('regis_daywiseid_'.$registrantId)){
        $daysRegisidArray = $this->ci->input->post('days_regisid_'.$registrantId);
        $dateRegisidArray = $this->ci->input->post('date_regisid_'.$registrantId);
        $RegisDayWaiseidArray = $this->ci->input->post('regis_daywiseid_'.$registrantId);
        
        $dayCount=0;
        foreach($RegisDayWaiseidArray as $key=>$value){
          
          $dayOfRegis = $key; // key will be registraint day
          $regisDayId = $registrantId.'_'.$dayOfRegis;
          $regisDayWiseId = $value;
          
          // if user is checked registrantday or registrant early bird day then record will add or update
          if($this->ci->input->post('registrantDay'.$regisDayId) || $this->ci->input->post('registrantEarlybirdDay'.$regisDayId) ){
            
            if($this->ci->input->post('registrantDay'.$regisDayId)){
              $getData['registrant_daywise_limit']     = $this->ci->input->post('registrantDayLimit'.$regisDayId);
              //$getData['total_price']              = $this->ci->input->post('registrantDayTotalPrice'.$regisDayId);
                $getData['complimentary']                   = ($this->ci->input->post('registrantComplimentary'.$regisDayId))?$this->ci->input->post('registrantComplimentary'.$regisDayId):"0";
                if($this->ci->input->post('registrantComplimentary'.$regisDayId)){
                  //set null if not checked
                  $getData['unit_price']          = NULL;
                  $getData['total_price']          = NULL;
                  $getData['gst']                  = NULL;
                  $getData['total_price_inc_gst']  = NULL;
                }else{
                      $getData['unit_price']               = $this->ci->input->post('registrantDayUnitPrice'.$regisDayId);
                      $getData['gst']                    = $this->ci->input->post('registrantGSTInclude'.$regisDayId);
                      $getData['total_price_inc_gst']                    = $this->ci->input->post('registrantDayPriceIncGST'.$regisDayId);
                      $getData['total_price']              = ($getData['unit_price']+$getData['gst']);
                }
            }else{
              $getData['registrant_daywise_limit']   = NULL;
              $getData['unit_price']           = NULL;
              $getData['total_price']            = NULL;
              $getData['gst']                  = NULL;
              $getData['total_price_inc_gst']                  = NULL;
            }
            
            // check if registrantEarlybirdDay is checked then get  data
            if($this->ci->input->post('registrantEarlybirdDay'.$regisDayId)){
              $getData['allow_day_group_booking']     = $this->ci->input->post('registrantDayGroupBooking'.$regisDayId);;
              $getData['allow_earlybird']         = $this->ci->input->post('registrantEarlybirdDay'.$regisDayId);
              $getData['date_early_bird']       = dateFormate($this->ci->input->post('registrantEarlybirdDate'.$regisDayId),'Y-m-d');
              $getData['unit_price_early_bird']     = $this->ci->input->post('registrantEarlybirdDayUnitPrice'.$regisDayId);
              $getData['total_price_early_bird']    = $this->ci->input->post('registrantEarlybirdDayPrice'.$regisDayId);
              $getData['gst_early_bird']          = $this->ci->input->post('registrantEarlybirdGSTInclude'.$regisDayId);
              $getData['early_bird_limit']          = $this->ci->input->post('registrantEarlybirdDayLimit'.$regisDayId);
            }else{
              $getData['allow_day_group_booking']     = NULL;
              $getData['allow_earlybird']         = NULL;
              $getData['date_early_bird']         = NULL;
              $getData['unit_price_early_bird']     = NULL;
              $getData['total_price_early_bird']    = NULL;
              $getData['gst_early_bird']          = NULL;
              $getData['early_bird_limit']          = NULL;
            }
            
            //echo $dayCount;echo '<pre>';print_r($dateRegisidArray); 
            $getData['registrant_id']     = $registrantId;
            $getData['event_date_day']    = $dateRegisidArray[$dayCount];
            $getData['day']               = $dayOfRegis;
            
            $whereRegisDay = array('registrant_id'=>$registrantId,'day'=>$dayOfRegis);  
            $registrantDaywise = $this->ci->common_model->getDataFromTabel('event_registrant_daywise','id',$whereRegisDay,'','id','ASC');
            
            if($registrantDaywise){
              //update registrant form data into database
              $this->ci->common_model->updateDataFromTabel('event_registrant_daywise',$getData,$whereRegisDay);
            }else{
              //add registrant form data into database
              $this->ci->common_model->addDataIntoTabel('event_registrant_daywise', $getData);
            }
            
          }else{
            // if not checked anything then record will be deleted by day and registraint id wise
            //delete data from database
            $whereDeleteCondi = array('registrant_id'=>$registrantId,'day' =>$dayOfRegis);
            $this->ci->common_model->deleteRowFromTabel('event_registrant_daywise',$whereDeleteCondi);
          }
          
          $dayCount++;
          
        }
        
      }
    }//die;
  }
  
  // ------------------------------------------------------------------------ 
  
  /*
  * @access:  private
    * @description: This function is used get registrant limit by passsing parameter
    * @param1: $eventId
    * @param2: $registrantId
    * @return: array
    *  
    */
  
  private function _getRegistrantsLimit($eventId,$registrantId){
    
    //get event details data
    $where = array('id'=>$eventId);
    $regisWhere = array('event_id'=>$eventId);
    $regisIdWhere = array('id'=>$registrantId);
    $getEventData = $this->ci->common_model->getDataFromTabel('event','number_of_rego',$where);
    $getRegisLimitSum = $this->ci->common_model->getSum('event_registrants','registrants_limit',$regisWhere);
    $getRegistrantsIdData = $this->ci->common_model->getDataFromTabel('event_registrants','registrants_limit',$regisIdWhere);
    
    $totalEventRegisterLimit = 0;
    //get number of registrant
    if(!empty($getEventData )){
      $getEventData = $getEventData[0];
      $totalEventRegisterLimit = $getEventData->number_of_rego;
    }
    $totalRegistrantsLimitIndb = 0;
    //get registered number sum
    if(!empty($getRegisLimitSum )){
      $getRegisLimitSum = $getRegisLimitSum[0];
      $totalRegistrantsLimitIndb = $getRegisLimitSum->registrants_limit;
    }
    
    $registrantIdFieldLimit = 0;
    //get registered number sum
    if(!empty($getRegistrantsIdData )){
      $getRegistrantsIdData = $getRegistrantsIdData[0];
      if(!empty($getRegistrantsIdData->registrants_limit)){
        $registrantIdFieldLimit = $getRegistrantsIdData->registrants_limit;
      }
    }
    
    $returnArray = array('totalEventRegisterLimit'=>$totalEventRegisterLimit,'totalRegistrantsLimitIndb'=>$totalRegistrantsLimitIndb
    ,'registrantIdFieldLimit'=>$registrantIdFieldLimit);
    return $returnArray;
  } 
   
   
    /*
   * @description: This function is used to validate corporate package Details form
   * @return void
   * 
    */ 
    
  public function _addCorporatePackageDetailValidate($packageId){
    
    $this->ci->form_validation->set_rules('corporate_package_id_'.$packageId, 'Corporate Category', 'trim|required');
    
    $this->ci->form_validation->set_rules('package_subject_'.$packageId, 'Subject to Approval', 'trim|required');
    /*$this->ci->form_validation->set_rules('package_pass_req_'.$packageId, 'Password Required', 'trim|required');
    
    if($this->ci->input->post('package_pass_req_'.$packageId)=='1'){ //if password req 1 than apply valiation
      $this->ci->form_validation->set_rules('password_'.$packageId, 'Password', 'trim|required|matches[con_password_'.$packageId.']');
      $this->ci->form_validation->set_rules('con_password_'.$packageId, 'Confirm Password', 'trim|required');
    }*/
    
    $this->ci->form_validation->set_rules('package_available_'.$packageId.'[]', 'Packages Available', 'trim|required');
    $this->ci->form_validation->set_rules('package_totalprice_'.$packageId, 'Total Price', 'trim|required');
    //$this->ci->form_validation->set_rules('package_gst_'.$packageId, 'GST Included', 'trim|required');
    $this->ci->form_validation->set_rules('reg_include_'.$packageId, 'Registration Included', 'trim|required');
    
    if($this->ci->input->post('is_exhibition_space_'.$packageId)=='1'){
      $this->ci->form_validation->set_rules('exhibition_type_'.$packageId, 'Exhibition Space Type', 'trim|required');
      $this->ci->form_validation->set_rules('booth_no_'.$packageId, 'Booths per Package', 'trim|required|numeric');
      $this->ci->form_validation->set_rules('allocate_reserved_booths_'.$packageId.'[]', 'Allocate Reserved Booths', 'trim|required');
    } 
     
  } 
  
  /*
   * @description: This function is used to save corporate package Details form
   * @return void
   * 
    */ 
  
  function _addeditcorporatepackageDetails($packageId){
    
    $data['category_id']              = $this->ci->input->post('corporate_package_id_'.$packageId);
    $data['description']              = $this->ci->input->post('package_description_'.$packageId);
    $data['limit_package_available']  = $this->ci->input->post('package_available_'.$packageId);
    $data['total_price']              = $this->ci->input->post('package_totalprice_'.$packageId);
    $data['gst']                      = $this->ci->input->post('package_gst_'.$packageId);
    $data['total_price_inc_gst']      = $this->ci->input->post('package_totalprice_gst_inc_'.$packageId);
    $data['event_id']                 = $this->ci->input->post('eventId');
    /*$data['request_password']       = $this->ci->input->post('package_pass_req_'.$packageId);*/
    $data['subject_to_approval']      = $this->ci->input->post('package_subject_'.$packageId);
    $data['password']                 = $this->ci->input->post('password_'.$packageId);
    $data['registration_included']    = $this->ci->input->post('reg_include_'.$packageId);
            
    /*Set Corporate Package Entry in Registrants Table*/
    if($this->ci->input->post('reg_include_'.$packageId)==1){
      $this->addRegistrantAsCorporate($data['event_id'],$data['category_id'],$packageId);
    }         
            
            
    $available_benefit         = $this->ci->input->post('available_benefit_'.$packageId);
    if(!empty($available_benefit)){
      $data['package_benefit_ids'] = serialize($available_benefit);
    } 
    $purchase_Item_ids          = $this->ci->input->post('purchase_items_'.$packageId);
    if(!empty($purchase_Item_ids)){
      $data['purchase_Item_ids']    = serialize($purchase_Item_ids);
    }      
    
    $updateCorporateResult = $this->ci->event_model->updatecorporatepackage($data,array('id'=>$packageId));   
    
    $corporateExhibitionSpace['floor_plan_type_id']     = $this->ci->input->post('exhibition_type_'.$packageId); // floor plan type id means space id
    $corporateExhibitionSpace['corporate_package_id']     = $packageId;
    $corporateExhibitionSpace['event_id']           = $this->ci->input->post('eventId');
    $allocateReservedBooths                 = $this->ci->input->post('allocate_reserved_booths_'.$packageId); // post as array 
    
    if(!empty($allocateReservedBooths)){
      
      foreach($allocateReservedBooths as $value){
        $corporateExhibitionSpace['booth_position'] =  $value;
        $corporatePackageBooths[] = $corporateExhibitionSpace;
      }//end loop
      
      // check if corporate_package_id is exist or not 
      $availableBooths = $this->ci->event_model->getCorporatePackageBooths(array('corporate_package_id'=>$packageId));
      $deleteResult  = TRUE;
      if($availableBooths!=0){ // exist
        // delete available booths and insert new one
        $deleteResult = $this->ci->event_model->deleteCorporatePackageBooths(array('corporate_package_id'=>$packageId));  
      } 
      // insert into corporate Package Booths table
      if($deleteResult){
        $this->ci->event_model->addCorporatePackageBooths($corporatePackageBooths); 
      }
    }//end if
    
    if($updateCorporateResult){
      $errors = lang('corporate_package_update_succ');
      //echo json_message('msg',$errors,'is_success','true'); 
      echo json_encode(array('msg'=>$errors,'is_success'=>'true','url'=>'event/corporatedetails'));
    }else{
      $errors = lang('comm_error');
      echo json_message('msg',$errors,'is_success','false'); 
    }
    
  }
  
  
  /*Set Corporate Package Entry in Registrants Table*/
  public function addRegistrantAsCorporate($event_id,$category_id,$packageId){
    $where = array('event_id'=>$event_id,'id'=>$packageId);  
    $corporatepackageData = $this->ci->common_model->getDataFromTabel('corporate_package','corporate_package_name',$where,'','id','ASC');
    $corporate_package_name = $corporatepackageData[0]->corporate_package_name;
    
    $whereCheckRegistrant = array('event_id'=>$event_id,'is_corporate_package'=>'1');
    $checkRegistrantData = $this->ci->common_model->getDataFromTabel('event_registrants','registrant_name',$whereCheckRegistrant,'','id','ASC');
    //print_r($checkRegistrantData);die;   
    
    if(!empty($checkRegistrantData)){
      $registrant_name = $checkRegistrantData[0]->registrant_name;
      $whereRegistrant = array('event_id'=>$event_id,'registrant_name'=>$registrant_name,'is_corporate_package'=>'1');
      $data['registrant_name'] = $corporate_package_name;
      $data['category_id'] = $category_id;
      $this->ci->common_model->updateDataFromTabel('event_registrants',$data,$whereRegistrant);
    } else {       
      $data['registrant_name'] = $corporate_package_name;
      $data['event_id'] = $event_id;   
      $data['category_id'] = $category_id;    
      $data['is_corporate_package'] = '1';   
      $this->ci->common_model->addDataIntoTabel('event_registrants', $data);
    }
  }
  
  
  /*
  * @description: This function is used to show popup for corporate section
  * @return void
  * 
    */ 
   
    public function corporatepopup(){
     
      $actionName       = $this->ci->input->post('popupType');
      $data['eventId']    = $this->ci->input->post('eventId');
      $data['removeDivID']  = $this->ci->input->post('removeDivID');
    
     
      switch($actionName){
      
      case "spaceTypePopup":
        $this->_floorplantype();
      break;
      case "addBenefitPopup":
        $this->_addbenefitpopup();
        break;    
      case "addDefaultPurchasePopup" : 
        $this->_addbenefitdefaultpurchasepopup();
        break;
        case "addCustomFieldPopup":
        $this->_addcustomfieldpopup();
        break;
         case "addCorporatePackagePopup":
        $this->_addcorporatepackagepopup();
        break;
          
      default : 
        echo false;
      break;
    
    }
  }
  
  
  /*
  * @description: This function is used to load floor plan/Space type
  * @return void
  * 
    */ 
  private function _floorplantype(){
    
      $data['eventId']    = $this->ci->input->post('eventId');
      $data['removeDivID']  = $this->ci->input->post('removeDivID');
    
      $floorPlanTypeId    = decode($this->ci->input->post('floorPlanTypeId'));
      
      // get corporate space type if exsit 
      $data['spaceTypeRecord']    = $this->ci->event_model->getCorporateSpaceTypeDetails(array('floor_plan_type_id'=>$floorPlanTypeId)); 
      
      //get total available booths for this event
      $totalAvailableBooths =  $this->ci->event_model->getTotalAvailableBooths(array('event_id'=>$data['eventId']));
      $totalNoAvailableBooths = array();
      
      if(!empty($totalAvailableBooths)){
        $availableBooths = array(); 
        foreach($totalAvailableBooths as $value){
         $availableBooths = unserialize($value->available_booth); // multi array
         
         foreach($availableBooths as $val){
           $totalNoAvailableBooths[] = $val;
         } 
         
        } // end foreach
        
      }//end if
      
      $data['totalAvailableBooths'] = $totalNoAvailableBooths;
      $data['totalSpaceAvalible']   = $this->ci->input->post('refr_no');
      
      $this->ci->load->view('corporate/corporate_space_type',$data);
      
   }
   
   public function getfloorplantype(){
     $booths = "";
     $floorPlanTypeId     = $this->ci->input->post('floorPlanTypeId'); 
     $packageId         = $this->ci->input->post('productId'); 
     $reservedBoothsArray = array();
     //get package reserved booths
     $reservedBooths = $this->ci->common_model->getDataFromTabel('corporate_package_booths','booth_position',array('corporate_package_id'=>$packageId));
     if(!empty($reservedBooths)){
      foreach($reservedBooths as $value){
        $reservedBoothsArray[] = $value->booth_position;
      } // end loop
     }//end if
     
       // get corporate space type if exsit 
       $spaceTypeRecord    = $this->ci->event_model->getCorporateSpaceTypeDetails(array('floor_plan_type_id'=>$floorPlanTypeId)); 
       if($spaceTypeRecord){
         $availableBooth    = $spaceTypeRecord->available_booth;
         $availableBoothArray = (!empty($availableBooth)) ? unserialize($availableBooth) : "";
         if(is_array($availableBoothArray)){
         $booths ="";
         
         // get Already reserved booths for other packages for the same event id
        $reservedBoothForOtherPac = getReservedBoothForOtherPackage($packageId);  
          
        foreach($availableBoothArray as $key=>$value){
          $booth    = (strlen($value)==1) ? "0".$value : $value;
          $checked  = (in_array($value,$reservedBoothsArray)) ? 'checked="checked"' : "";
          $disabled   = (in_array($booth,$reservedBoothForOtherPac)) ? 'disabled="disabled" checked="checked" ' : "";
          
          $booths .=' <span class="checkbox_wrapper">';
          $booths .= '<input id="tick_'.$packageId.'_'.$key.'" '.$checked.' '.$disabled.'  class="sub_check reserved_booth" type="checkbox" data-productid="'.$packageId.'" value="'.$booth.'" name="allocate_reserved_booths_'.$packageId.'[]" >';
          $booths .= '<label for="tick_'.$packageId.'_'.$key.'">'.$booth.'</label>';
          $booths .= '</span>';
           
         }
         }
         
       }
     echo $booths;
   }  
   
  
  /*
  * @description: This function is used to load add benefit popup
  * @return void
  * 
    */ 
  private function _addbenefitpopup(){
    
      $data['eventId']    = $this->ci->input->post('eventId');
      $data['packageId']    = $this->ci->input->post('packageId');
      $data['removeDivID']  = $this->ci->input->post('removeDivID');
      $data['openPopupId']  = $this->ci->input->post('openPopupId');
    
      $package_benefit_id  = decode($this->ci->input->post('package_benefit_id'));
      $where = array('package_benefit_id'=>$package_benefit_id);
      $data['default_package_benefit'] = $this->ci->common_model->getDataFromTabel('corporate_package_benefit','*',$where);
      
      $this->ci->load->view('corporate/popup_default_package_benefit',$data);
    
  } 
  
  
  /*
  * @description: This function is used to load add benefit default purcahse popup
  * @return void
  * 
    */ 
  private function _addbenefitdefaultpurchasepopup(){
    
      $data['eventId']    = $this->ci->input->post('eventId');
      $data['eventdetails'] = $this->ci->event_model->geteventdetails($data['eventId']);
      $data['packageId']    = $this->ci->input->post('packageId');
      $data['openPopupId']  = $this->ci->input->post('openPopupId');
      
      $purchase_item_id  = decode($this->ci->input->post('purchase_item_id'));
      $where = array('purchase_item_id'=>$purchase_item_id);
      $data['default_purchase_item']  = $this->ci->common_model->getDataFromTabel('corporate_purchase_item','*',$where);
      
      $this->ci->load->view('corporate/popup_default_additional_purchase_item',$data);  
    
  } 
  
  
  /*
  * @description: This function is used to load custom field popup
  * @return void
  * 
    */ 
  private function _addcustomfieldpopup(){
    
    $data['eventId']    = $this->ci->input->post('eventId');
    $data['removeDivID']  = $this->ci->input->post('removeDivID');
    
    $customfieldid  = decode($this->ci->input->post('customfieldid'));
    $where = array('id'=>$customfieldid);
    $data['customFieldRecord']  = $this->ci->common_model->getDataFromTabel('custom_form_fields','*',$where);   
    $this->ci->load->view('corporate/popup_corporate_application_custom_field',$data);
  }
  
  /*
  * @description: This function is used to load corporate package popup
  * @return void
  * 
    */ 
  private function _addcorporatepackagepopup(){
  
    $data['eventId']    = $this->ci->input->post('eventId');
    $data['removeDivID']  = $this->ci->input->post('removeDivID');
    $packageId        = $this->ci->input->post('packageId');
    $data['isCopy']     = $this->ci->input->post('isCopy');
    
  
    $where = array('id'=>$packageId);
    $packageDetails = $this->ci->common_model->getDataFromTabel('corporate_package','id,category_id,corporate_package_name',$where);    
    $data['packageDetails'] = (!empty($packageDetails)) ? $packageDetails[0] : FALSe;
    $this->ci->load->view('corporate/popup_corporate_package',$data);
  
  }
  

  
  // ------------------------------------------------------------------------ 
  
   /*
    * @access : public
  * @description: This function is used to update corporate floor plan
  * @return void
  * 
  */ 

  
  public function updatecorporatefloorplan(){
    
    $dirUploadPath = './media/floor_plan/';
    $dirSavePath = '/media/floor_plan/';
    $uploadData = $this->ci->process_upload->upload_file($dirUploadPath);
    $eventId = $_REQUEST['event_id'];
    $updateData['floor_plan']      = $uploadData['filename'];
    $updateData['floor_plan_path'] = $dirSavePath;
    $updateData['event_id']      = $eventId;
    $fileId = remove_extension($uploadData['filename']);
    $upWhere = array('event_id'=>$eventId);
    $this->ci->common_model->updateDataFromTabel('corporate_floor_plan',$updateData,$upWhere);
    echo json_encode(array('id'=>$floorPlanId,'fileId'=>$fileId));
  } 
  
  // ------------------------------------------------------------------------ 
  
  /*
   * @access :   public
   * @description: This function is used to delete floor plan document
   * @return void
   */ 
   
  public function deletecorporatefloorplan(){
    
    
    $floorPlanId          = $this->ci->input->post('deleteId');
    $updateData['floor_plan']       = '';
    $updateData['floor_plan_path']  = '';
    $where              = array('id'=>$floorPlanId);
    
    //check record is exist 
    $getResult = $this->ci->common_model->getDataFromTabel('corporate_floor_plan','floor_plan,floor_plan_path',$where);
    if(!empty($getResult)){
      //delte image from directory
      findFileNDelete($getResult[0]->floor_plan_path, $getResult[0]->floor_plan);
    }
    
    $this->ci->common_model->updateDataFromTabel('corporate_floor_plan',$updateData,$where);
    echo json_message('msg','Floor plan document deleted.');
  } 
  
  
  
  /*
  * @description: This function is used to delete default package benefit
    * @return: void
    *  
    */
   public function deletecorporatespacetype(){
     $deleteId = $this->ci->input->post('deleteId');
     if(!empty($deleteId)){
       $this->ci->event_model->deletecorporatespacetype(array('floor_plan_type_id'=>decode($deleteId)));
         echo json_message('msg',lang('default_corporate_spacetype_deleted_succ'),'is_success','true');
     }else{
       echo json_message('msg',lang('comm_error'),'is_success','false');
     }
   }
  
  
  /*
  * @description: This function is used to delete default package benefit
    * @return: void
    *  
    */
   public function deletedefaultpackagebenefit(){
     $deleteId = $this->ci->input->post('deleteId');
     if(!empty($deleteId)){
       $this->ci->event_model->deletedefaultpackagebenefit(array('package_benefit_id '=>decode($deleteId)));
         echo json_message('msg',lang('default_package_deleted_succ'),'is_success','true');
     }else{
       echo json_message('msg',lang('comm_error'),'is_success','false');
     }
   }
  
    
  /*
  * @description: This function is used to delete default package benefit
    * @return: void
    *  
    */
   public function deletedefaultpurchaseitem(){
     $deleteId = $this->ci->input->post('deleteId');
     if(!empty($deleteId)){
       $this->ci->event_model->deletedefaultpurchaseitem(array('purchase_item_id'=>decode($deleteId)));
         echo json_message('msg',lang('default_purchase_deleted_succ'),'is_success','true');
     }else{
       echo json_message('msg',lang('comm_error'),'is_success','false');
     }
   }
  
  
  /////////////////////////////////////////////////////////////////////////////////////////////////////////
  
  // ------------------------------------------------------------------------ 
  
  /*
   * @access: public
   * @description: This function is used to setup side events 
   * @param: eventId
   * @return: void
   * 
   */ 
  
  public function setupsideevents(){
    
    //current open eventId
    $eventId = currentEventId();
    
    //decode entered eventid
    $eventId = decode($eventId); 
    
    //check is data post 
    if($this->ci->input->post()){
      
      //call insert and update data function side event
      //$this->insertupdatesideevents();
      $actionName = $this->ci->input->post('formActionName');
      switch($actionName){
        //add/Update side event data
        case "sideeventDetails":
        $this->insertupdatesideevents($eventId);
        break;
        //add/Update side event registrants data
        case "sideeventRegistrantDetails":
        $this->insertupdatesideeventRegistrants($eventId);
        break;
        default:
        // load event form view
        $this->sideeventssetupview($eventId);
      }
    }else{
      // load registrant form view
      $this->sideeventssetupview($eventId);
    }
  }
  
   /////////////////////////////////////////////////////////////////////////////////////////////////////////
  
  
  
  
  // ------------------------------------------------------------------------ 
  
  /*
  * @access: public
    * @description: This function is used to load side event view 
    * @param: $eventId
    * @return: void
    *  
    */
  
  private function sideeventssetupview($eventId="0"){
    
    //chheck if eventId is zero 
    if($eventId==0){
      redirect(base_url('dashboard'));
    }
    
    $userId           = isLoginUser();
    $data['userId']       = $userId;
    $data['eventId']      = $eventId;
    $data['popupHeaderBg']    = 'sideevent_bg_header';//set popup header background color
    $data['popupButtonBg']    = 'sideevent_type_bg_button'; // popup button bg class 
    $data['eventdetails']     = $this->ci->event_model->geteventdetails($eventId,$userId);
    $data['eventregistrants']   = $this->ci->event_model->getRegistrantList($eventId,'1'); // get registrant list
    
    // get all registrant category with its limit
    $data['registrantCategory']  =  $this->ci->event_model->getRegistrantCategory($eventId); 
    
    //if empty then redirect
    if(empty($data['eventdetails'])){
      redirect(base_url('dashboard'));
    }
    
    $where = array('event_id'=>$eventId);
    $registrantWhere = array('event_id'=>$eventId,'registrants_limit !=' => '');
    $data['sideeventdata'] = $this->ci->common_model->getDataFromTabel('side_event','*',$where,'','id','ASC');
    $this->ci->template->load('template','sideevents/side_events_setup',$data,TRUE);
  }
  
  
  
  // ------------------------------------------------------------------------ 
  
  /*
   * @access: public
   * @description: This function is used to open add & edit side event popup view
   * @return void
   * 
   */ 
   
  public function addeditduplicatesideeventview(){
    
    $data['eventId']   = $this->ci->input->post('eventId');
    $sideeventid     = $this->ci->input->post('sideeventid');
    $formAction      = $this->ci->input->post('formAction');
    $data['sideeventid'] = $sideeventid;
    $where = array('id'=>$sideeventid);
    //get data form custome form field table
    $data['sideeventdata'] = $this->ci->common_model->getDataFromTabel('side_event','side_event_name',$where,'','id','ASC','1');
    if(!empty($data['sideeventdata'])){
      $data['sideeventdata'] = $data['sideeventdata'][0];
    }
    //set popup header
    switch($formAction){
      case "add":
        $headerAction = lang('event_side_popup_add_title');
      break;
      case "edit":
        $headerAction = lang('event_side_popup_edit_title');
      break;  
      case "duplicate":
        $headerAction = lang('event_side_popup_duplicate_title');
      break;    
    }
    $data['headerAction']    = $headerAction; 
    $data['formActionValue'] = $formAction; 

    $this->ci->load->view('sideevents/add_edit_sideevent_popup',$data);
  }
  
  // ------------------------------------------------------------------------ 
  
  /*
   * @access: public
   * @description: This function is used to save add & edit side event popup data
   * @return void
   * 
   */ 
   
  public function addeditduplicatesideeventsave(){
    
    $formAction = $this->ci->input->post('formAction');
    //set validation
    $this->ci->form_validation->set_rules('sideEventName', 'side event name', 'trim|required');
    
    if($this->ci->form_validation->run($this->ci)){
      
      switch($formAction){
      
        case "add":
        case "edit":
          $msg = $this->_addeditsideeventsave(); //add edit side event
        break;
        
        case "duplicate":
          $msg = $this->_duplicatesideeventsave(); //duplicate side event
        break;
      }
      
      $returnArray=array('msg'=>$msg,'is_success'=>'true','url'=>'event/setupsideevents'); 
      //set_global_messages($returnArray);
      echo json_encode($returnArray);
      
      //$returnArray=array('msg'=>$msg,'is_success'=>'true','url'=>$this->ci->uri->uri_string()); 
      //echo json_encode($returnArray);
    }else{
      $errors = $this->ci->form_validation->error_array();
      echo json_message('msg',$errors,'is_success','false');
    }
  }
  
  // ------------------------------------------------------------------------ 
  
  /*
   * @access: private
   * @description: This function is used to add and edit side event 
   * @return message (string)
   * 
   */   
  
  private function _addeditsideeventsave(){
    
    $eventId             = $this->ci->input->post('eventId');
    $sideEventName           = $this->ci->input->post('sideEventName');
    $sideEventId           = $this->ci->input->post('sideEventId');
    $insertData['side_event_name'] = $sideEventName;
    $insertData['event_id']      = $eventId;
    if($sideEventId > 0){
      $where = array('id'=>$sideEventId);
      $this->ci->common_model->updateDataFromTabel('side_event',$insertData,$where);
      $msg = lang('event_msg_side_event_updated_successfully');
    }else{
      $insertId = $this->ci->common_model->addDataIntoTabel('side_event', $insertData);
      $msg = lang('event_msg_side_event_add_successfully');
    }
    return $msg; 
  }
  
  // ------------------------------------------------------------------------ 
  
  /*
   * @access: private
   * @description: This function is used to duplicate side event 
   * @return void
   * 
   */   
  
  private function _duplicatesideeventsave(){

    $eventId            = $this->ci->input->post('eventId');
    $sideEventName            = $this->ci->input->post('sideEventName');
    $sideEventId            = $this->ci->input->post('sideEventId');
    
    $where    = array('id'=>$sideEventId);
    $whereCondi = array('side_event_id'=>$sideEventId);
    //get data form side event table
    $sideeventdata      = $this->ci->common_model->getDataFromTabel('side_event','*',$where,'','id','ASC','1');
    $sideeventregisdata   = $this->ci->common_model->getDataFromTabel('sideevent_registrant','*',$whereCondi,'','id','ASC');
    
    //insert data insert side event table
    if(!empty($sideeventdata)){
      $sideeventdata = $sideeventdata[0];
      $insertData['side_event_limit']       = '0';
      $insertData['event_id']               = $eventId;
      $insertData['side_event_name']        = $sideEventName;
      $insertData['venue']                  = $sideeventdata->venue;
      $insertData['start_datetime']         = $sideeventdata->start_datetime;
      $insertData['end_datetime']           = $sideeventdata->end_datetime;
      $insertData['additional_details']     = $sideeventdata->additional_details;
      $insertData['common_price']           = $sideeventdata->common_price;     
      $insertData['event_restrict_access']  = $sideeventdata->event_restrict_access;
      
      $insertData['early_bird_limit']       = 0;
      $insertData['early_bird_total_price'] = $sideeventdata->early_bird_total_price;
      $insertData['early_bird_gst_included'] = $sideeventdata->early_bird_gst_included;
      $insertData['min_group_booking_size']  = $sideeventdata->min_group_booking_size;
      $insertData['percentage_discount_for_group_booking'] = $sideeventdata->percentage_discount_for_group_booking;
      $insertData['allow_earlybird']         = $sideeventdata->allow_earlybird;
      $insertData['allow_group_booking']     = $sideeventdata->allow_group_booking; 
      $insertId = $this->ci->common_model->addDataIntoTabel('side_event', $insertData);
    }
    
    //insert data insert side event registrant table
    if(!empty($sideeventregisdata)){
      foreach($sideeventregisdata as $sideeventregis){
        $insertDataRegis['side_event_id']                      = $insertId;
        $insertDataRegis['registrant_id']                      = $sideeventregis->registrant_id;
        $insertDataRegis['reg_limit']                          = $sideeventregis->reg_limit;
        $insertDataRegis['complementary']                      = $sideeventregis->complementary;
        $insertDataRegis['total_price_comp']                   = $sideeventregis->total_price_comp;
        $insertDataRegis['gst_included_total_price_comp']      = $sideeventregis->gst_included_total_price_comp;
        $insertDataRegis['total_early_bird_price_comp']        = $sideeventregis->total_early_bird_price_comp;
        $insertDataRegis['gst_included_early_bird_comp']       = $sideeventregis->gst_included_early_bird_comp;
        $insertDataRegis['additional_guests']                  = $sideeventregis->additional_guests;
        $insertDataRegis['guests_limit']                       = $sideeventregis->guests_limit;
        $insertDataRegis['total_price_ad_guest']               = $sideeventregis->total_price_ad_guest;
        $insertDataRegis['gst_included_total_price_ad_guest']  = $sideeventregis->gst_included_total_price_ad_guest;
        $insertDataRegis['total_early_bird_price_ad_guest']    = $sideeventregis->total_early_bird_price_ad_guest;
        $insertDataRegis['gst_included_total_price_early_bird_ad_guest']  = $sideeventregis->gst_included_total_price_early_bird_ad_guest;
        $insertDataRegis['complementary_guests']               = $sideeventregis->complementary_guests;
        $insertDataRegis['complementary_guests_limit']         = $sideeventregis->complementary_guests_limit;
        
        $this->ci->common_model->addDataIntoTabel('sideevent_registrant', $insertDataRegis);
        unset($insertDataRegis);
      }      
    }
    
    //set duplicate message
    $msg = lang('event_msg_side_event_duplicate_successfully');
    
    return $msg; 
    
  }
  
  // ------------------------------------------------------------------------
  /*
   * @access: public
   * @description: This function is used to delete side event 
   * @return void
   * 
   */ 
    public function deletesideevent(){
      $deleteid = $this->ci->input->post('deleteId');
      //delete registant record from other side event registrant table
      $whereRegis = array('side_event_id'=>$deleteid);
      $this->ci->common_model->deleteRowFromTabel('sideevent_registrant',$whereRegis);

      //delete registant record from other side event table
      $where = array('id'=>$deleteid);
      $this->ci->common_model->deleteRowFromTabel('side_event',$where);

      $msg = lang('event_msg_side_event_deleted');
      $returnArray=array('msg'=>$msg,'is_success'=>'true');
      echo json_encode($returnArray);
    }
  
  // ------------------------------------------------------------------------ 
  
  /*
   * @access: private
   * @description: This function is used to insert and update side event forms data 
   * @return void
   * 
   */ 
  
  private function insertupdatesideevents(){
    
    if($this->ci->input->post()){
      
      //get form registrant id
      $sideEventId = $this->ci->input->post('sideEventId');
    
      //get eventId
      $eventId   = $this->ci->input->post('eventId');
      
      //call function for sideevent validate
      $this->_sideeventValidate($sideEventId,$eventId);
      
      
      //get ajax request
      $is_ajax_request = $this->ci->input->is_ajax_request();
      
      if ($this->ci->form_validation->run($this->ci))
      {
        //call function for save side event 
        $this->_sideeventSave($sideEventId,$eventId);
      
          
        // check ajax request
        $msg = lang('event_msg_side_events_setup_saved');
        
        if($is_ajax_request){
          //echo json_message('msg',$msg);
          //$msgArray = array('msg'=>$msg,'is_success'=>'true');
          //set_global_messages($msgArray);
          $returnArray=array('msg'=>$msg,'is_success'=>'true','url'=>$this->ci->uri->uri_string()); 
          echo json_encode($returnArray);
        }else{
          $msgArray = array('msg'=>$msg,'is_success'=>'true');
          set_global_messages($msgArray);
          redirect('event/eventdetails/'.encode($eventId));
        }
          
      }else{
        $errors = $this->ci->form_validation->error_array();
        if($is_ajax_request){
          echo json_message('msg',$errors,'is_success','false');
        }else{
          // load event form view
          $this->sideeventssetupview($eventId);
        }
      }
      
    }else{
        $msg    = lang('event_msg_invalid_data_request');
        $msgArray = array('msg'=>$msg,'is_success'=>'false');
        set_global_messages($msgArray);
        redirect(base_url('dashboard'));
      }
      
  }
  
  
  // ------------------------------------------------------------------------ 
  
  /*
   * @access: private
   * @description: This function is used to insert and update side event registrant types forms data 
   * @return void
   * 
   */ 
  
  private function insertupdatesideeventRegistrants(){
    
    if($this->ci->input->post()){
      
      //get form registrant id
      $sideEventId = $this->ci->input->post('sideEventId');
    
      //get eventId
      $eventId   = $this->ci->input->post('eventId');
      
      
      //call function for validate registrant
      $this->_sideeventRegistrantValidate($sideEventId);
      
      //get ajax request
      $is_ajax_request = $this->ci->input->is_ajax_request();
      
      if ($this->ci->form_validation->run($this->ci))
      {
      
        //call function for save side event registrant
        $this->_sideeventRegistrantSave($sideEventId);
          
        // check ajax request
        $msg = lang('event_msg_side_events_registrant_saved');
        
        if($is_ajax_request){
          //echo json_message('msg',$msg);
          //$msgArray = array('msg'=>$msg,'is_success'=>'true');
          //set_global_messages($msgArray);
          $returnArray=array('msg'=>$msg,'is_success'=>'true','url'=>$this->ci->uri->uri_string()); 
          echo json_encode($returnArray);
        }else{
          $msgArray = array('msg'=>$msg,'is_success'=>'true');
          set_global_messages($msgArray);
          redirect('event/eventdetails/'.encode($eventId));
        }
          
      }else{
        $errors = $this->ci->form_validation->error_array();
        if($is_ajax_request){
          echo json_message('msg',$errors,'is_success','false');
        }else{
          // load event form view
          $this->sideeventssetupview($eventId);
        }
      }
      
    }else{
        $msg    = lang('event_msg_invalid_data_request');
        $msgArray = array('msg'=>$msg,'is_success'=>'false');
        set_global_messages($msgArray);
        redirect(base_url('dashboard'));
      }
      
  }
  
  
  
  
  // ------------------------------------------------------------------------ 
  
  /*
   * @access: private
   * @description: This function is used to validate side event  
   * @param1: $sideEventId (int)
   * @param2: $eventId (int)
   * @return: void
   * 
   */
   
  private function _sideeventValidate($sideEventId,$eventId){
    //set all form validation
    $this->ci->form_validation->set_rules('sideEventLimit'.$sideEventId, 'side event limit', 'trim|required|numeric');
    $this->ci->form_validation->set_rules('sideEventVenue'.$sideEventId, 'venue', 'trim|required');
    $this->ci->form_validation->set_rules('sideEventStartDate'.$sideEventId, 'side event start date', 'trim|required');
    $this->ci->form_validation->set_rules('sideEventEndDate'.$sideEventId, 'side event end date', 'trim|required');
    
    $where = array('id'=>$eventId);
    $whereCondi = array('event_id'=>$eventId);
    $whereCondiSide = array('id'=>$sideEventId);
    //get data from event table for check main limit
    $getEventDataForLimitChk = $this->ci->common_model->getDataFromTabel('event','number_of_rego,starttime,endtime',$where);
    $getEventDataForLimitChk = $getEventDataForLimitChk[0];
    
    //get all side event total limit sum by this event id
    $getSideEventSumLimitChk = $this->ci->common_model->getSum('side_event','side_event_limit',$whereCondi);
    
    //get side event data for limit check by current side event id
    $getSideEventDataLimit = $this->ci->common_model->getSum('side_event','side_event_limit',$whereCondiSide);
    
    //check entered side limit with total limit
    if($this->ci->input->post('sideEventLimit'.$sideEventId)){
      if(!empty($getEventDataForLimitChk)){
        $totalNumberOfRegis   = $getEventDataForLimitChk->number_of_rego; 
        //enter side event 
        $sideEventLimit     = $this->ci->input->post('sideEventLimit'.$sideEventId);  
      
        //here we are check side event limit with main event limit
        if($totalNumberOfRegis < $sideEventLimit){
          $this->ci->form_validation->set_rules('sideeventlimitchk', 'Side event limit should be less than from total limit.', 'callback_custom_error_set');
        }
        
        //Here we check side event enter limit from total sue of all saved side event limit
        if(!empty($getSideEventSumLimitChk)){
          $getSideEventSumLimitChk = $getSideEventSumLimitChk[0];
          $sideEventLimitDataSaved = '0';
          if(!empty($getSideEventDataLimit)){
            $getSideEventDataLimit     = $getSideEventDataLimit[0];
            $sideEventLimitDataSaved   = $getSideEventDataLimit->side_event_limit; 
          }
          
          //get all side event limit sum
          $sideEventLimitSum      = $getSideEventSumLimitChk->side_event_limit; 
          //minute side event saved limit from sum of side event 
          $sideEventLimitSum      = $sideEventLimitSum - $sideEventLimitDataSaved;
          
          //check side event total sum + entered limit with main event limit
          if($totalNumberOfRegis > $sideEventLimitSum){
            $avaibaleSideEventLimit = $totalNumberOfRegis - $sideEventLimitSum;
            //check side event availabe limit with side event entered limit
            if($avaibaleSideEventLimit < $sideEventLimit){
              $this->ci->form_validation->set_rules('sideeventlimitchk', 'All Side events limit sum should be less than from total limit.', 'callback_custom_error_set');
            } 
          
          }
          
        }
      }
    }
    
    //set side event selected date check should be between start and end date
    if(!empty($getEventDataForLimitChk)){
      $eventStartTime         = strtotime($getEventDataForLimitChk->starttime); 
      $eventEndTime         = strtotime($getEventDataForLimitChk->endtime); 
      $sideEventStartDateValue  = strtotime($this->ci->input->post('sideEventStartDate'.$sideEventId)); 
      $sideEventEndDateValue    = strtotime($this->ci->input->post('sideEventEndDate'.$sideEventId)); 
    
      if($eventEndTime < $sideEventStartDateValue || $eventStartTime > $sideEventStartDateValue){
        $this->ci->form_validation->set_rules('sideeventdatecheck', 'Side event selected start date should be between main event start & end date.', 'callback_custom_error_set');
      }elseif($eventEndTime < $sideEventEndDateValue || $eventStartTime > $sideEventEndDateValue){
        $this->ci->form_validation->set_rules('sideeventdatecheck', 'Side event selected end date should be between main event start & end date.', 'callback_custom_error_set');
      }
    }
    
    //this section check is event allow early bird then validate
    if($this->ci->input->post('sideEventAllowEarlybird'.$sideEventId)){
      $this->ci->form_validation->set_rules('earlyBirdLimit'.$sideEventId, 'early bird limit', 'trim|required');
      $this->ci->form_validation->set_rules('earlyBirdTotalPrice'.$sideEventId, 'early bird total price', 'trim|required');
      
      $sideEventLimit     = $this->ci->input->post('sideEventLimit'.$sideEventId);
      $earlyBirdLimit     = $this->ci->input->post('earlyBirdLimit'.$sideEventId);
      if($earlyBirdLimit > $sideEventLimit){
        $this->ci->form_validation->set_rules('earlyBirdLimitchk', 'early bird limit should be less than from side event limit.', 'callback_custom_error_set');
      }      
    } 
    
    
  }
  
  // ------------------------------------------------------------------------ 
  
  /*
   * @access:    private
   * @description: This function is used to validate side event registrant 
   * @param1:    $sideEventId (int)
   * @return:    void
   * 
   */
   
  private function _sideeventRegistrantValidate($sideEventId){
    //if($this->ci->input->post('registrantcheckbox'.$sideEventId)){
      //echo "<pre>";print_r($this->ci->input->post('registrantcheckbox'.$sideEventId));die;
      //$SideEventRegistaintIdMultiArray = $this->ci->input->post('registrantcheckbox'.$sideEventId);
      
      $reg_limitSum = 0;
      //foreach($SideEventRegistaintIdMultiArray as $registrantId=>$value){
      
          $registrantId = $this->ci->input->post('registrantId');
      
          $appendId = $sideEventId.'_'.$registrantId;
          $this->ci->form_validation->set_rules('reg_limit'.$appendId, 'registrant limit', 'trim|required|numeric');
          
          $reg_limitSum += $this->ci->input->post('reg_limit'.$appendId);
          
          if(!$this->ci->input->post('complementary'.$appendId)){
            $this->ci->form_validation->set_rules('total_price_comp'.$appendId, 'complementary total price', 'trim|required|numeric');
            $this->ci->form_validation->set_rules('gst_included_total_price_comp'.$appendId, 'complementary total price gst included', 'trim|required|numeric');
            if($this->ci->input->post('allowEarlyBirdComp'.$appendId)){
              $this->ci->form_validation->set_rules('total_early_bird_price_comp'.$appendId, 'complementary total early bird price', 'trim|required|numeric');
              $this->ci->form_validation->set_rules('gst_included_early_bird_comp'.$appendId, 'complementary total early bird price gst included', 'trim|required|numeric');          
            }
          }
          
          if($this->ci->input->post('additional_guests'.$appendId)){
            $this->ci->form_validation->set_rules('guests_limit'.$appendId, 'Guest Limit', 'trim|required|numeric');
            $this->ci->form_validation->set_rules('total_price_ad_guest'.$appendId, 'additional guests total price', 'trim|required|numeric');
            $this->ci->form_validation->set_rules('gst_included_total_price_ad_guest'.$appendId, 'additional guests total price gst included', 'trim|required|numeric');
            
            if($this->ci->input->post('allowEarlyBirdAdGuest'.$appendId)){
              $this->ci->form_validation->set_rules('total_early_bird_price_ad_guest'.$appendId, 'additional guests total early bird price', 'trim|required|numeric');
              $this->ci->form_validation->set_rules('gst_included_total_price_early_bird_ad_guest'.$appendId, 'additional guests early bird price gst included', 'trim|required|numeric');          
            }
          }
            
          if($this->ci->input->post('complementary_guests'.$appendId)){
            $this->ci->form_validation->set_rules('complementary_guests_limit'.$appendId, 'complimentary guest limit', 'trim|required|numeric');
          } 
      //}
      
      /*$sideEventLimit     = $this->ci->input->post('sideEventLimit'.$sideEventId);
      if($reg_limitSum>$sideEventLimit){
        $this->ci->form_validation->set_rules('regLimitchk', 'all registrant limit should be less than from total limit.', 'callback_custom_error_set');
      }*/
      
    //} 
  }
  
  // ------------------------------------------------------------------------ 
  
  /*
   * @access:    private
   * @description: This function is used to save side event  
   * @param1:    $sideEventId (int)
   * @param2:    $eventId (int)
   * @return:    void
   * 
   */
   
  private function _sideeventSave($sideEventId,$eventId){

    $updateData['side_event_limit']    = $this->ci->input->post('sideEventLimit'.$sideEventId);
    $updateData['venue']           = $this->ci->input->post('sideEventVenue'.$sideEventId);
    $updateData['start_datetime']      = dateFormate($this->ci->input->post('sideEventStartDate'.$sideEventId),'Y-m-d H:i');
    $updateData['end_datetime']        = dateFormate($this->ci->input->post('sideEventEndDate'.$sideEventId),'Y-m-d H:i');
    $updateData['additional_details']    = $this->ci->input->post('sideEventAddDetail'.$sideEventId);
    
    $updateData['common_price'] = ($this->ci->input->post('sideEventCommonPrice'.$sideEventId))?$this->ci->input->post('sideEventCommonPrice'.$sideEventId):'0';
    if($this->ci->input->post('sideEventCommonPrice'.$sideEventId)==1){
      $updateData['common_total_price'] = $this->ci->input->post('common_total_price'.$sideEventId);
      $updateData['common_gst_included'] = $this->ci->input->post('common_gst_included'.$sideEventId);
    } else {
      $updateData['common_total_price']   = NULL;
      $updateData['common_gst_included']  = NULL;
    }
    
    
    $updateData['event_restrict_access'] = ($this->ci->input->post('restrictEventAccess'.$sideEventId)) ? $this->ci->input->post('restrictEventAccess'.$sideEventId):'0';
    
    if($this->ci->input->post('restrictEventAccess'.$sideEventId)==1){
      if($this->ci->input->post('registrant_cat_'.$sideEventId)==1){
        $sideeventRegistrantArray = array();
        $SideEventRegistaintIdMultiArray = $this->ci->input->post('registrantcheckbox'.$sideEventId);
        if(!empty($SideEventRegistaintIdMultiArray)){
          foreach($SideEventRegistaintIdMultiArray as $registrantId=>$value){ 
             $sideeventRegistrantArray[] = $registrantId;
          }
        } else {
            $sideeventRegistrantArray = NULL;
        }
        $updateData['sideevent_registrants'] = json_encode($sideeventRegistrantArray);
      } else {
        $updateData['sideevent_registrants'] = NULL;
      }
    } else {
      $updateData['sideevent_registrants'] = NULL;
    }
    
    $sideEventAllowEarlybird = ($this->ci->input->post('sideEventAllowEarlybird'.$sideEventId)) ? $this->ci->input->post('sideEventAllowEarlybird'.$sideEventId):'0';
   if($sideEventAllowEarlybird==1){
     $updateData['early_bird_limit']    = $this->ci->input->post('earlyBirdLimit'.$sideEventId);
     $updateData['early_bird_total_price']    = $this->ci->input->post('earlyBirdTotalPrice'.$sideEventId);
     $updateData['early_bird_gst_included']    = $this->ci->input->post('earlyGSTInclude'.$sideEventId);  
   }
   
       $sideEventGroupbookings = ($this->ci->input->post('sideEventGroupbookings'.$sideEventId))?$this->ci->input->post('sideEventGroupbookings'.$sideEventId):'0';
     if($sideEventGroupbookings==1){
      $updateData['min_group_booking_size']    = $this->ci->input->post('sideEventGroupBookingsSize'.$sideEventId);
      $updateData['percentage_discount_for_group_booking']    = $this->ci->input->post('sideEventGroupBookingsPercent'.$sideEventId);
    }
    

    $updateData['allow_earlybird']     = $sideEventAllowEarlybird;
    $updateData['allow_group_booking']     = $sideEventGroupbookings; 
    
    if($this->ci->input->post('restrictEventAccess'.$sideEventId)==0){
      //delete registant record from other side event registrant table
      $whereRegis = array('side_event_id'=>$sideEventId);
      $this->ci->common_model->deleteRowFromTabel('sideevent_registrant',$whereRegis);
    }
    
    $whereRegis = array('id'=>$sideEventId);  
    $this->ci->common_model->updateDataFromTabel('side_event',$updateData,$whereRegis);

    
    //print_r($SideEventRegistaintIdMultiArray);die;
    if(!empty($SideEventRegistaintIdMultiArray)){
      foreach($SideEventRegistaintIdMultiArray as $registrantId=>$value){ 
          $sideeventRegistrantArray[] = $registrantId;
      }
    } else {  
      $whereRegis = array('side_event_id'=>$sideEventId);
      $this->ci->common_model->deleteRowFromTabel('sideevent_registrant',$whereRegis);
    }



  }
  
  // ------------------------------------------------------------------------ 
  
  /*
   * @access:    private
   * @description: This function is used to save side event registrant 
   * @param1:    $sideEventId (int)
   * @return:    void
   * 
   */
   
  private function _sideeventRegistrantSave($sideEventId){
    //print_r($this->ci->input->post());die;
    /*if($this->ci->input->post('registrantcheckbox'.$sideEventId)){
      $sideeventRegistrantArray = array();
      $SideEventRegistaintIdMultiArray = $this->ci->input->post('registrantcheckbox'.$sideEventId);
      foreach($SideEventRegistaintIdMultiArray as $registrantId=>$value){ 
          $sideeventRegistrantArray[] = $registrantId;*/       
          
          $sideEventId = $this->ci->input->post('sideEventId');
          $registrantId = $this->ci->input->post('registrantId');
          
          $appendId = $sideEventId.'_'.$registrantId;          
          $sideEventRegis['registrant_id'] = $registrantId;
          $sideEventRegis['side_event_id'] = $sideEventId;
          $SideEventRegistaintId = $this->ci->input->post('SideEventRegistaintId'.$appendId);
          $sideEventRegis['reg_limit'] = $this->ci->input->post('reg_limit'.$appendId);
          if($this->ci->input->post('complementary'.$appendId)){                  
            $sideEventRegis['complementary'] = '1';
            $sideEventRegis['total_price_comp'] = NULL;
            $sideEventRegis['gst_included_total_price_comp'] = NULL;
            $sideEventRegis['total_price_inc_gst'] = NULL;
            $sideEventRegis['complimentary_allow_earlybird'] = 0;
            $sideEventRegis['complimentary_earlybird_limit'] = NULL;
            $sideEventRegis['total_early_bird_price_comp'] = NULL;
            $sideEventRegis['gst_included_early_bird_comp'] = NULL;
            $sideEventRegis['total_early_bird_price_gst_inc_comp'] = NULL;                 
            $sideEventRegis['complimentary_allow_group_booking'] = 0;                 
          } else {  
            $sideEventRegis['complementary'] = '0';
            $sideEventRegis['total_price_comp'] = $this->ci->input->post('total_price_comp'.$appendId);
            $sideEventRegis['gst_included_total_price_comp'] = $this->ci->input->post('gst_included_total_price_comp'.$appendId);
            $sideEventRegis['total_price_inc_gst'] = $this->ci->input->post('total_price_inc_gst_comp'.$appendId);
            
            $sideEventRegis['complimentary_allow_group_booking'] = $this->ci->input->post('allowGroupBooking'.$appendId);            
            
            if($this->ci->input->post('allowEarlyBirdComp'.$appendId)){
              $sideEventRegis['complimentary_allow_earlybird'] = $this->ci->input->post('allowEarlyBirdComp'.$appendId);
              $sideEventRegis['complimentary_earlybird_limit'] = $this->ci->input->post('earlyBird_limit'.$appendId);
              $sideEventRegis['total_early_bird_price_comp'] = $this->ci->input->post('total_early_bird_price_comp'.$appendId);
              $sideEventRegis['gst_included_early_bird_comp'] = $this->ci->input->post('gst_included_early_bird_comp'.$appendId); 
              $sideEventRegis['total_early_bird_price_gst_inc_comp'] = $this->ci->input->post('total_early_bird_price_gst_inc_comp'.$appendId);
            } else {
              $sideEventRegis['complimentary_allow_earlybird'] = 0;
              $sideEventRegis['complimentary_earlybird_limit'] = NULL;
              $sideEventRegis['total_early_bird_price_comp'] = NULL;
              $sideEventRegis['gst_included_early_bird_comp'] = NULL;
              $sideEventRegis['total_early_bird_price_gst_inc_comp'] = NULL;
            }                            
          }
          
          if($this->ci->input->post('additional_guests'.$appendId)){
            $sideEventRegis['additional_guests'] = '1';
            $sideEventRegis['guests_limit'] = $this->ci->input->post('guests_limit'.$appendId);
            $sideEventRegis['total_price_ad_guest'] = $this->ci->input->post('total_price_ad_guest'.$appendId);
            $sideEventRegis['gst_included_total_price_ad_guest'] = $this->ci->input->post('gst_included_total_price_ad_guest'.$appendId);
            $sideEventRegis['total_price_gst_inc_ad_guest'] = $this->ci->input->post('total_price_gst_inc_ad_guest'.$appendId);
            
            
            if($this->ci->input->post('allowEarlyBirdAdGuest'.$appendId)){
              $sideEventRegis['allow_earlybird_ad_guest'] = $this->ci->input->post('allowEarlyBirdAdGuest'.$appendId);
              $sideEventRegis['earlybird_limit_ad_guest'] = $this->ci->input->post('earlyBirdAdGuest_limit'.$appendId);
                            
              $sideEventRegis['total_early_bird_price_ad_guest'] = $this->ci->input->post('total_early_bird_price_ad_guest'.$appendId);
              $sideEventRegis['gst_included_total_price_early_bird_ad_guest'] = $this->ci->input->post('gst_included_total_price_early_bird_ad_guest'.$appendId); 
              $sideEventRegis['total_early_bird_price_gst_inc_ad_guest'] = $this->ci->input->post('total_early_bird_price_gst_inc_ad_guest'.$appendId); 
           } else {
              $sideEventRegis['allow_earlybird_ad_guest'] = 0;
              $sideEventRegis['earlybird_limit_ad_guest'] = NULL;            
              $sideEventRegis['total_early_bird_price_ad_guest'] = NULL;
              $sideEventRegis['gst_included_total_price_early_bird_ad_guest'] = NULL;
              $sideEventRegis['total_early_bird_price_gst_inc_ad_guest'] = NULL;
           }
            
            
          } else {
            $sideEventRegis['additional_guests'] = '0';
            $sideEventRegis['guests_limit'] = NULL;
            $sideEventRegis['total_price_ad_guest'] = NULL;
            $sideEventRegis['gst_included_total_price_ad_guest'] = NULL;
            $sideEventRegis['total_price_gst_inc_ad_guest'] = NULL;
            
            $sideEventRegis['allow_earlybird_ad_guest'] = 0;
            $sideEventRegis['earlybird_limit_ad_guest'] = NULL;            
            $sideEventRegis['total_early_bird_price_ad_guest'] = NULL;
            $sideEventRegis['gst_included_total_price_early_bird_ad_guest'] = NULL;
            $sideEventRegis['total_early_bird_price_gst_inc_ad_guest'] = NULL;
          }
          
            
          if($this->ci->input->post('complementary_guests'.$appendId)){
            $sideEventRegis['complementary_guests'] = '1';
            $sideEventRegis['complementary_guests_limit'] = $this->ci->input->post('complementary_guests_limit'.$appendId);
          }  else {
            $sideEventRegis['complementary_guests'] = '0';
            $sideEventRegis['complementary_guests_limit'] = NULL;
          }   
          if($SideEventRegistaintId > 0){
              $whereSideRegis = array('id'=>$SideEventRegistaintId);  
              $this->ci->common_model->updateDataFromTabel('sideevent_registrant',$sideEventRegis,$whereSideRegis);
         }else{
              $insertId = $this->ci->common_model->addDataIntoTabel('sideevent_registrant', $sideEventRegis);              
         }       
  
          //check wheater default pricing in side event is set 1 if default pricing is saved
          if($registrantId==0){
            $whereSideEvent = array('id'=>$sideEventId); 
            $updateSideEvent['common_price'] = 1;
            $this->ci->common_model->updateDataFromTabel('side_event',$updateSideEvent,$whereSideEvent); 
          }
         
      /*}


      //This will get all registrant for side event id saved in db
      $where = array('side_event_id' => $sideEventId);
      $sideeventRegistrantData = getDataFromTabel('sideevent_registrant', 'registrant_id', $where, '', 'id', 'ASC');
      if(!empty($sideeventRegistrantData)){
        $sideeventRegistrantArrayDB = array();
        foreach($sideeventRegistrantData as $key=>$sideeventRegistrant){
          $sideeventRegistrantArrayDB[] = $sideeventRegistrant->registrant_id; 
        }
      }

      //This will get all registrant that is in db but unchecked by user and than delete from table
      $delArray = array_diff($sideeventRegistrantArrayDB, $sideeventRegistrantArray);
      foreach($delArray as $key=>$regId){
        $delWhere = array('side_event_id' => $sideEventId, 'registrant_id' => $regId);                    
        $this->ci->common_model->deleteRowFromTabel('sideevent_registrant',$delWhere);
      }

    } 
    else{
      //This will get all registrant that is in db and than delete from table
      $where = array('side_event_id' => $sideEventId);                    
      $sideeventRegistrantData = getDataFromTabel('sideevent_registrant', '*', $where, '', 'id', 'ASC');
      if(!empty($sideeventRegistrantData)){
        $this->ci->common_model->deleteRowFromTabel('sideevent_registrant', $where, '', 'id', 'ASC');
      }
    } */    
  }
  
  // ------------------------------------------------------------------------ 
  
  /*
   * @access: public
   * @description: This function is used for setup breakout  
   * @param: eventId
   * @return: void
   * 
   */ 
  
  public function setupbreakouts(){
    //current open eventId
    $eventId = currentEventId();
  
    //decode entered eventid
    $eventId = decode($eventId); 
    
    //check is data post 
    if($this->ci->input->post()){
          
        //call insert and update data function
        $this->insertupdatebreakouttypes();
    }else{
      
      // load registrant form view
      $this->breakoutstepsviews($eventId);
    }
  }
  
  // ------------------------------------------------------------------------ 
  
  /*
  * @access: private 
    * @default load event breakout details
    * @description: This function is used to load breakout steps details
    * @return: void
    *  
    */
  
  private function breakoutstepsviews($eventId="0"){
    
    //chheck if eventId is zero 
    if($eventId==0){
      redirect(base_url('dashboard'));
    }
    $userId          = isLoginUser();
    $data['userId']      = $userId;
    $data['popupHeaderBg'] = 'breakout_bg_header';//set popup header background color
    $data['popupButtonBg'] = 'breakout_type_bg_button'; // popup button bg class 
    $data['eventId']     = $eventId;
    
    $where            = array('event_id'=>$eventId);
    $registrantWhere      = array('event_id'=>$eventId,'registrants_limit !=' => '');
    $data['eventbreakouts']   = $this->ci->common_model->getDataFromTabel('breakouts','*',$where,'','id','ASC');
    $data['eventregistrants'] = $this->ci->event_model->getRegistrantList($eventId); // get registrant list
    $this->ci->template->load('template','breakout_steps',$data,TRUE);
  }
  
  // ------------------------------------------------------------------------ 
  
  /*
   * @access:    public
   * @description: This function is used to open  add & edit side event popup view
   * @return void
   * 
   */ 
   
  public function addeditbreakoutview(){
    $userId = isLoginUser();  
    $data['userId'] = $userId;
    $data['eventId']     = $this->ci->input->post('eventId');
    $data['eventdetails']         = $this->ci->event_model->geteventdetails($data['eventId'],$userId); 
    $breakoutIdValue     = $this->ci->input->post('breakoutId');
    $data['breakoutIdValue'] = $breakoutIdValue;
    $where           = array('id'=>$breakoutIdValue);
    //get data form custome form field table
    $data['breakoutdata'] = $this->ci->common_model->getDataFromTabel('breakouts','breakout_name,block_type,date,start_time,end_time',$where,'','id','ASC','1');
    if(!empty($data['breakoutdata'])){
      $data['breakoutdata'] = $data['breakoutdata'][0];
    }

    $this->ci->load->view('program/add_edit_breakout_popup',$data);
  }
  
  // ------------------------------------------------------------------------ 
  
  /*
   * @access:    public
   * @description: This function is used to open  duplicate breakout view
   * @return void
   * 
   */ 
   
  public function duplicatebreakoutview(){
    $eventId         = $this->ci->input->post('eventId');
    $data['eventId']     = $eventId;
    $breakoutIdValue     = $this->ci->input->post('breakoutId');
    $data['breakoutIdValue'] = $breakoutIdValue;
    $where = array('id'=>$breakoutIdValue,'event_id'=>$eventId);
    //get data form custome form field table
    $data['breakoutdata'] = $this->ci->common_model->getDataFromTabel('breakouts','breakout_name,block_type,date,start_time,end_time',$where,'','id','ASC','1');
    if(!empty($data['breakoutdata'])){
      $data['breakoutdata'] = $data['breakoutdata'][0];
    }

    $this->ci->load->view('program/duplicate_breakout_popup',$data);
  }
  
  // ------------------------------------------------------------------------ 
  
  /*
   * @access:    public
   * @description: This function is used to add duplicate type of breakout 
   * @return void
   * 
   */ 
   
  public function duplicatebreakoutsave(){
    
    //set validation
    $this->ci->form_validation->set_rules('breakoutName', 'block name', 'trim|required');
    
    //get event id and breakout id
    $eventId     = $this->ci->input->post('eventId');
    $dupliBreakoutId = $this->ci->input->post('breakoutId');
    
    if($this->ci->form_validation->run($this->ci)){
      
      /*----breakout table data start----*/ 
       $insertedBreakoutData = $this->_breakoutDuplicate($eventId,$dupliBreakoutId);
       $insertedBreakoutId  = $insertedBreakoutData['breakoutId'];
       $breakoutBlockType   = $insertedBreakoutData['blockType'];
       $dupliBreakoutData   = $insertedBreakoutData['dupliBreakoutData'];
       
       if($insertedBreakoutId>0){ 
        
        if($breakoutBlockType=='1'){ // 1 for plenary
          
          /*----breakout stream data start----*/  
          $sessionArray = $this->_breakoutSessionDuplicateForPlenary($dupliBreakoutId,$insertedBreakoutId,$dupliBreakoutData);
          /*----breakout stream data end----*/  
        
          
        }else{ // non plenary
        /*----breakout table data end----*/ 
        
        /*----breakout stream data start----*/  
          $streamArray = $this->_breakoutStreamDuplicate($dupliBreakoutId,$insertedBreakoutId);
        /*----breakout stream data end----*/  
        
        /*----breakout session data start----*/ 
          $sessionArray = $this->_breakoutSessionDuplicate($dupliBreakoutId,$insertedBreakoutId,$streamArray);
        /*----breakout session data end----*/
          
        /*----breakout stream session data start----*/  
        //  $this->_breakoutStreamSessionDuplicate($dupliBreakoutId,$insertedBreakoutId,$streamArray,$sessionArray);
        /*----breakout stream session data end----*/
         }
        $msg    = lang('event_msg_breakout_duplicate_created_successfully');
        $returnArray= array('msg'=>$msg,'is_success'=>'true','url'=>'event/programdetails'); 
      }else{
        $msg    = lang('comm_error');
        $returnArray= array('msg'=>$msg,'is_success'=>'false','url'=>'event/programdetails'); 
      } 
        //set_global_messages($returnArray);
        echo json_encode($returnArray);
    }else{
      $errors = $this->ci->form_validation->error_array();
      echo json_message('msg',$errors,'is_success','false');
    }
  }
  
  // ------------------------------------------------------------------------ 
  
  /*
   * @access:    private
   * @description: This function is used to add duplicate breakout data 
   * $param: $eventId
   * $param: $dupliBreakoutId
   * @return void
   * 
   */ 
  
  
  private function _breakoutDuplicate($eventId,$dupliBreakoutId){
    //get breakout data
    $insertedBreakoutId = 0;$blockType=0;
    $where       = array('id'=>$dupliBreakoutId,'event_id'=>$eventId);
    $getBreakoutdata = $this->ci->common_model->getDataFromTabel('breakouts','*',$where,'','id','ASC','1');
    
    if(!empty($getBreakoutdata)){
      $getBreakoutdata = $getBreakoutdata[0];
      $blockType     = $getBreakoutdata->block_type;
    
      $breakoutName   = $this->ci->input->post('breakoutName');
      unset($getBreakoutdata->id);
      unset($getBreakoutdata->breakout_name);
      foreach($getBreakoutdata as $key1=>$val1){
        $insertBreakout[$key1]  = $val1;
      }
      $insertBreakout['breakout_name'] = $breakoutName;
      
      //insert prepare data in breakout table
      $insertedBreakoutId = $this->ci->common_model->addDataIntoTabel('breakouts', $insertBreakout);
    }
    
    return array('breakoutId'=>$insertedBreakoutId,'blockType'=>$blockType,'dupliBreakoutData'=>$getBreakoutdata);
  } 
  
  // ------------------------------------------------------------------------ 
  
  /*
   * @access:    private
   * @description: This function is used to add duplicate breakout stream data 
   * @param: $dupliBreakoutId;
   * @param: $insertedBreakoutId;
   * @return void
   * 
   */ 
  
  
  private function _breakoutStreamDuplicate($dupliBreakoutId,$insertedBreakoutId){
    
    $returnData = '';
    $where = array('breakout_id'=>$dupliBreakoutId);
    $getBreakoutStreamdata = $this->ci->common_model->getDataFromTabel('breakout_stream','*',$where,'','id','ASC');
    
    if(!empty($getBreakoutStreamdata)){
      foreach($getBreakoutStreamdata as $getBreakoutStream){
        $getBreakoutStream = (array) $getBreakoutStream;
        unset($getBreakoutStream['id']);//remove unuse colom
        $getBreakoutStream['breakout_id'] = $insertedBreakoutId;
        $getBreakoutStream['block_limit'] = '0';
        
        //insert data one by one
        $streamArray[]['streamid'] =$this->ci->common_model->addDataIntoTabel('breakout_stream', $getBreakoutStream);
      }
      $returnData = $streamArray;
    }
    
    return $returnData;
  }
  
  
  // ------------------------------------------------------------------------ 
  
  /*
   * @access:    private
   * @description: This function is used to add duplicate breakout session data 
   * @param: $dupliBreakoutId;
   * @param: $insertedBreakoutId;
   * @return void
   * 
   */ 
  
  
  private function _breakoutSessionDuplicate($dupliBreakoutId,$insertedBreakoutId,$streamArray){
    
    $returnData = '';
    //get breakout stream data
    $where = array('breakout_id'=>$dupliBreakoutId);
    $getBreakoutStreamdata = $this->ci->common_model->getDataFromTabel('breakout_stream','*',$where,'','id','ASC');
    
    if(!empty($getBreakoutStreamdata)){
      
      $countRow=0;
      foreach($getBreakoutStreamdata  as $getBreakoutStream){
        $streamId = $getBreakoutStream->id;
        //get breakout session data by sream id
        $whereStream = array('stream_id'=>$streamId);
        $getBreakoutSessiondata = $this->ci->common_model->getDataFromTabel('breakout_session','*',$whereStream,'','id','ASC');
        
        if(!empty($getBreakoutSessiondata)){
          $countRow1=0;
          foreach($getBreakoutSessiondata as $getBreakoutSession){
            $getBreakoutSession = (array) $getBreakoutSession;
            unset($getBreakoutSession['id']);//remove unuse colom
            $getBreakoutSession['breakout_id'] = $insertedBreakoutId;
            $getBreakoutSession['stream_id'] = $streamArray[$countRow]['streamid'];
            
            //insert data one by one
            $sessionArray[$countRow][$countRow1]['sessionid'] = $this->ci->common_model->addDataIntoTabel('breakout_session', $getBreakoutSession);
            $sessionArray[$countRow][$countRow1]['streamid'] = $streamArray[$countRow]['streamid'];
          
            $countRow1++;//row increment
          }
          
        }
        $countRow++; //row increment
      }
      
      $returnData = $sessionArray;
    } 
    
    return $returnData;
  }
  
  // ------------------------------------------------------------------------ 
  
  /*
   * @access:    private
   * @description: This function is used to add duplicate breakout session data 
   * @param: $dupliBreakoutId;
   * @param: $insertedBreakoutId;
   * @return void
   * 
   */ 
  
  private function _breakoutSessionDuplicateForPlenary($dupliBreakoutId,$insertedBreakoutId,$dupliBreakoutData){
    $returnData   = '';
    $sessionArray = array();
    if(!empty($dupliBreakoutData)){
      
      //$breakoutData = $dupliBreakoutData[0];
      $numberOfSession  = $dupliBreakoutData->number_of_session;
      for($i=1;$i<=$numberOfSession;$i++){
        
        //get breakout session data by session no 
        $whereSession = array('session'=>$i,'breakout_id'=>$dupliBreakoutId);
        $getBreakoutSessiondata = $this->ci->common_model->getDataFromTabel('breakout_session','*',$whereSession,'','id','ASC');
        if(!empty($getBreakoutSessiondata)){
          foreach($getBreakoutSessiondata as $getBreakoutSession){
            $getBreakoutSession = (array) $getBreakoutSession;
            unset($getBreakoutSession['id']);//remove unuse colom
            $getBreakoutSession['breakout_id']    = $insertedBreakoutId;
            $getBreakoutSession['stream_id']      = 0;
            $getBreakoutSession['start_time']     = '';
            $getBreakoutSession['end_time']       = '';
            $getBreakoutSession['session_limit']    = 0;
            
            //insert data one by one
            $sessionArray[] = $this->ci->common_model->addDataIntoTabel('breakout_session', $getBreakoutSession);
          }
        }
      }
    }
    return $sessionArray;
  }
  
  // ------------------------------------------------------------------------ 
  
  /*
   * @access:    private
   * @description: This function is used to add duplicate breakout stream session data 
   * @param: $dupliBreakoutId;
   * @param: $insertedBreakoutId;
   * @param: $streamArray;
   * @param: $sessionArray;
   * @return void
   * 
   */ 
  
  
  private function _breakoutStreamSessionDuplicate($dupliBreakoutId,$insertedBreakoutId,$streamArray,$sessionArray){
    
    //get breakout stream data
    $where = array('breakout_id'=>$dupliBreakoutId);
    $getBreakoutStreamdata = $this->ci->common_model->getDataFromTabel('breakout_stream','*',$where,'','id','ASC');
    
    if(!empty($getBreakoutStreamdata)){
      
      $countRow=0;
      foreach($getBreakoutStreamdata  as $getBreakoutStream){
        $streamId = $getBreakoutStream->id;
        //get breakout session data by sream id
        $whereStream = array('breakout_stream_id'=>$streamId);
        $getBreakoutSessiondata = $this->ci->common_model->getDataFromTabel('breakout_stream_session','*',$whereStream,'','id','ASC');
        
        if(!empty($getBreakoutSessiondata)){
          $countRow1=0;
          foreach($getBreakoutSessiondata as $getBreakoutSession){
            $getBreakoutSession = (array) $getBreakoutSession;
            unset($getBreakoutSession['id']);//remove unset colom
            $getBreakoutSession['breakout_stream_id']    = $sessionArray[$countRow][$countRow1]['streamid'];
            $getBreakoutSession['breakout_session_id']   = $sessionArray[$countRow][$countRow1]['sessionid'];
            
            //insert data one by one
            $this->ci->common_model->addDataIntoTabel('breakout_stream_session', $getBreakoutSession);
            $countRow1++;//row increment
          }
          
        }
        $countRow++; //row increment
      }
      
    } 
    
  }
  
  // ------------------------------------------------------------------------ 
  
  /*
   * @access : public
   * @description: This function is used to add type of breakout 
   * @return void
   * 
   */ 
   
  public function addeditbreakoutsave(){
    
    //set validation
    $this->ci->form_validation->set_rules('breakoutName', 'block name', 'trim|required');
    $this->ci->form_validation->set_rules('blockType', 'block Type', 'trim|required');
    $this->ci->form_validation->set_rules('blockStartTime', 'block start time', 'trim|required');
    $this->ci->form_validation->set_rules('blockEndTime',   'block end time', 'trim|required');
    
    //get event id and breakout id
    $eventId = $this->ci->input->post('eventId');
    $breakoutId = $this->ci->input->post('breakoutId');
    
    //get event data for checking limit, date validation
    $where = array('id'=>$eventId);
    $getEventDataForLimitChk = $this->ci->common_model->getDataFromTabel('event','starttime,endtime',$where);
    if(!empty($getEventDataForLimitChk)){
      $getEventDataForLimitChk = $getEventDataForLimitChk[0];
    }
          
    //call breakout date validate function
    $this->_breakoutsDateValidate($getEventDataForLimitChk);
    
    if($this->ci->form_validation->run($this->ci)){
      $breakoutName = $this->ci->input->post('breakoutName');
      $breakoutType = $this->ci->input->post('blockType');
      
      $insertData['breakout_name'] = $breakoutName;
      $insertData['block_type']    = $breakoutType;
      
      $insertData['event_id']    = $eventId;
      $insertData['date']      = dateFormate($this->ci->input->post('breakoutDate'),'Y-m-d');
      $insertData['start_time']  = timeFormate($this->ci->input->post('blockStartTime'));
      $insertData['end_time']    = timeFormate($this->ci->input->post('blockEndTime'));
    
      if($breakoutId > 0){
        $where = array('id'=>$breakoutId);
        $this->ci->common_model->updateDataFromTabel('breakouts',$insertData,$where);
        $msg = lang('event_msg_breakout_updated_successfully');
      }else{
        $insertId = $this->ci->common_model->addDataIntoTabel('breakouts', $insertData);
        $msg = lang('event_msg_breakout_add_successfully');
      }
      $returnArray=array('msg'=>$msg,'is_success'=>'true','url'=>'event/programdetails'); 
      //set_global_messages($returnArray);
      echo json_encode($returnArray);
    }else{
      $errors = $this->ci->form_validation->error_array();
      echo json_message('msg',$errors,'is_success','false');
    }
  }
  
  // ------------------------------------------------------------------------ 
  
  /*
   * @access : public
   * @description: This function is used to delete breakout 
   * @return void
   * 
   */   
  
  
  public function deletebreakout(){
    
    $deleteid = $this->ci->input->post('deleteId');
    //delete registant record from other side event registrant table
      
    //get data from breakout table dldete from stream session table
    $where = array('breakout_id' => $deleteid);
    $breakoutstreamdata = $this->ci->common_model->getDataFromTabel('breakout_stream','id',$where,'','id','ASC');
  
    if(!empty($breakoutstreamdata)){
      foreach($breakoutstreamdata as $streamdata){
        $breakoutStreamId = $streamdata->id;
        $whereBreakoutStream= array('breakout_stream_id'=>$breakoutStreamId);
        $this->ci->common_model->deleteRowFromTabel('breakout_stream_session',$whereBreakoutStream);
        
        //remove data from breakouts_session_registrants
        $where=array('stream_id'=>$breakoutStreamId);
        $this->ci->common_model->deleteRowFromTabel('breakouts_session_registrants',$where);
      }
    }
    
    //delete data from other table
    $whereBreakout = array('breakout_id'=>$deleteid);
    //$this->ci->common_model->deleteRowFromTabel('breakouts_registrant',$whereBreakout);
    //$this->ci->common_model->deleteRowFromTabel('breakout_block',$whereBreakout);
    $this->ci->common_model->deleteRowFromTabel('breakout_stream',$whereBreakout);
    $this->ci->common_model->deleteRowFromTabel('breakout_session',$whereBreakout);
    
    //delete registant record from other side event table
    $where = array('id'=>$deleteid);
    $this->ci->common_model->deleteRowFromTabel('breakouts',$where);
    
    $msg = lang('event_msg_breakout_deleted');
    $returnArray=array('msg'=>$msg,'is_success'=>'true');
    //set_global_messages($returnArray);
    echo json_encode($returnArray);
  }
  
  // ------------------------------------------------------------------------ 
  
  /*
   * @access : public
   * @description: This function is used to delete breakout 
   * @return void
   * 
   */   
  
  
  public function deletebreakoutsession(){
    
    $deleteid = $this->ci->input->post('deleteId');
    //delete registant record from other side event registrant table
      
    //get data from breakout table dldete from stream session table
    $where = array('id' => $deleteid);
    $breakoutsessiondata = $this->ci->common_model->getDataFromTabel('breakout_session','*',$where,'','id','ASC');

  
    if(!empty($breakoutsessiondata)){
        $sessiondata = $breakoutsessiondata[0];
        $breakoutSessionId = $sessiondata->id;
        $breakoutId = $sessiondata->breakout_id;
        $whereBreakoutSession = array('session_id'=>$breakoutSessionId);        
        $this->ci->common_model->deleteRowFromTabel('breakouts_session_registrants',$whereBreakoutSession);
        
        $whereBreakout = array('id'=>$breakoutId);
        $breakoutdata = $this->ci->common_model->getDataFromTabel('breakouts','*',$whereBreakout,'','id','ASC');
        $breakoutdata = $breakoutdata[0];
      
        $breakoutNumberSession = $breakoutdata->number_of_session;
        $data = array('number_of_session'=>$breakoutNumberSession-1);
        $this->ci->common_model->updateDataFromTabel('breakouts',$data,$whereBreakout);
    }
    
    //delete data from other table
    $whereBreakout = array('id'=>$deleteid);
    $this->ci->common_model->deleteRowFromTabel('breakout_session',$whereBreakout);
  
    
    $msg = lang('program_block_session_deleted_succ');
    $returnArray=array('msg'=>$msg,'is_success'=>'true');
    //set_global_messages($returnArray);
    echo json_encode($returnArray);
  }
  
  //------------------------------------------------------------------------------------
  
  /*
   * @access : private
   * @description: This function is used to insert and update breakout form data 
   * @return void
   * 
   */ 
  
  private function insertupdatebreakouttypes(){
    
    /* breakout call as program block */
    //get ajax request
      $is_ajax_request = $this->ci->input->is_ajax_request();   
      
    if($this->ci->input->post()){
    
      $this->ci->form_validation->set_rules('eventId', 'block name', 'trim|required');
    
      //get form breakout id
      $breakoutId = $this->ci->input->post('breakoutId');
      
      //get eventId
      $eventId  = $this->ci->input->post('eventId');
      
      //get event data for checking limit, date validation
      $where = array('id'=>$eventId);
      $getEventDataForLimitChk = $this->ci->common_model->getDataFromTabel('event','number_of_rego',$where);
      if(!empty($getEventDataForLimitChk)){
        $getEventDataForLimitChk = $getEventDataForLimitChk[0];
      }
      
      // Check block type is plenary or not
      $blockType = $this->ci->input->post('blockTypeHidden'.$breakoutId);
      
      if($blockType=='1') // 1 belongs to plenary
      {
        //call breakout Session validate function
        $this->_breakoutSessionValidate($breakoutId,$getEventDataForLimitChk); 
        
        ///check validate
        if($this->ci->form_validation->run($this->ci))
        {
        
          //This function call for save breakout data 
          $breakoutNumberOfSession  =  $this->ci->input->post('noofsession'.$breakoutId);
          $updateData         =  $this->_getSessionBreakoutData($breakoutId,$breakoutNumberOfSession);
          
          $this->_breakoutsDataSave($breakoutId,$updateData);
          
          //This function call for save breakout Session data 
          $breakoutNumberOfSession = $this->_breakoutsSessionDataSave($breakoutId);       
          
          // check ajax request
          $msg = lang('event_msg_setup_breakout_saved');
          
          if($is_ajax_request){
            //echo json_message('msg',$msg);
            //$msgArray = array('msg'=>$msg,'is_success'=>'true');
            //set_global_messages($msgArray);
            $returnArray=array('msg'=>$msg,'is_success'=>'true','url'=>$this->ci->uri->uri_string()); 
            echo json_encode($returnArray);  
          }else{
            $msgArray = array('msg'=>$msg,'is_success'=>'true');
            set_global_messages($msgArray);
            redirect('event/setupbreakouts/'.encode($eventId));
          }
        }else{
            $errors = $this->ci->form_validation->error_array();
            
            if($is_ajax_request){
              echo json_message('msg',$errors,'is_success','false');
            }else{
              
              // load event form view
              $this->breakoutstepsviews($eventId);
            }
        } 
        
      }else{      
            //call breakout stream validate function
            $this->_breakoutStreamValidate($breakoutId,$getEventDataForLimitChk); 
          
            ///check validate
            if($this->ci->form_validation->run($this->ci))
            {
               
              //This function call for save breakout data 
              $breakoutNumberOfSession      = $this->ci->input->post('breakoutNumberOfSession'.$breakoutId);
              $updateData             = $this->_getStreamBreakoutData($breakoutId);
              
                          
              $this->_breakoutsDataSave($breakoutId,$updateData);
                
              
              $streamSessionIdArray = $this->_breakoutsStreamSave($breakoutId);
              
                      
              //This function call for save breakout session stream details data 
              //$this->_breakoutsSessionStreamSave($breakoutId,$streamSessionIdArray);
              
              // check ajax request
              $msg = lang('event_msg_setup_breakout_saved');
              
              if($is_ajax_request){
                //echo json_message('msg',$msg);
                //$msgArray = array('msg'=>$msg,'is_success'=>'true');
                //set_global_messages($msgArray);
                
                $returnArray=array('msg'=>$msg,'is_success'=>'true','url'=>$this->ci->uri->uri_string()); 
                echo json_encode($returnArray);
              }else{
                $msgArray = array('msg'=>$msg,'is_success'=>'true');
                set_global_messages($msgArray);
                redirect('event/setupbreakouts/'.encode($eventId));
              }
          
            }else{
              $errors = $this->ci->form_validation->error_array();
              
              if($is_ajax_request){
                echo json_message('msg',$errors,'is_success','false');
              }else{
                
                // load event form view
                $this->breakoutstepsviews($eventId);
              }
            }
          }
      
      }else{
             
            $msg = lang('event_msg_invalid_data_request');
            $msgArray = array('msg'=>$msg,'is_success'=>'false');
            set_global_messages($msgArray);
            redirect(base_url('dashboard'));
      }
       
  }
  
  //--------------------------------------------------------------------------  
  
  /*
   * @description: This function is used to validate breatkout date
   * @param: getEventDataForLimitChk (array)
   * @param: breakoutId (int)
   * @return void
   */  
  
  
  private function _breakoutsDateValidate($getEventDataForLimitChk){
    //set fixed field validation
    $this->ci->form_validation->set_rules('breakoutDate', 'breakout date', 'trim|required');
          
    //breakout selected date check should be between start and end date
    if(!empty($getEventDataForLimitChk) && $this->ci->input->post('breakoutDate')){
      $eventStartTime     = strtotime($getEventDataForLimitChk->starttime); 
      $eventEndTime     = strtotime($getEventDataForLimitChk->endtime); 
      $breakoutDate   = strtotime($this->ci->input->post('breakoutDate'));  
    
      if($eventEndTime <= $breakoutDate && $eventStartTime >= $breakoutDate){
        $this->ci->form_validation->set_rules('breakoutdatecheck', lang('event_msg_breakout_select_between_date'), 'callback_custom_error_set');
      }
    }
  }
  

  
  //------------------------------------------------------------------------------------
  
  /*
   * @description: This function is used to validate breatkout stream section
   * @param: breakoutId
   * @return void
   */  
  
  private function _breakoutStreamValidate($breakoutId,$getEventDataForLimitChk){
    $sessionLimitArray  = array();
        
    //set daynamic field validation is breakout stream
    if($this->ci->input->post('breakoutStreamId'.$breakoutId)){
      
      // get Session limit
      $whereCondi     = array('id'=>$breakoutId);
      $getBreakoutData  = $this->ci->common_model->getDataFromTabel('breakouts','start_time,end_time',$whereCondi);
      if(!empty($getBreakoutData)){
        $getBreakoutData = $getBreakoutData[0];
      }
          
      
      $getPostIdArray  =  $this->ci->input->post('breakoutStreamId'.$breakoutId);
      foreach($getPostIdArray as $key => $value){
        
        $limit = array();
        $breakoutStreamRow = $key;
        $breakoutStreamRowId = $breakoutId.'_'.$breakoutStreamRow;
        
        //set validation
        $this->ci->form_validation->set_rules('breakoutStreamName'.$breakoutStreamRowId, 'breakout stream name '.$breakoutStreamRow, 'trim|required');
        $this->ci->form_validation->set_rules('breakoutStreamVenue'.$breakoutStreamRowId, 'breakout stream venue '.$breakoutStreamRow, 'trim|required');
        $this->ci->form_validation->set_rules('breakoutLimit'.$breakoutStreamRowId, 'breakout limit '.$breakoutStreamRow, 'trim|required|numeric');
        
        if($this->ci->input->post('restrictStreamAccess'.$breakoutStreamRowId)=='1'){
          $this->ci->form_validation->set_rules('registrantcheckbox'.$breakoutStreamRowId, 'session restrict stream access '.$key.' registrant type ', 'required');
          $registrantIDs = $this->ci->input->post('registrantcheckbox'.$breakoutStreamRowId);
          $streamLimits  = $this->ci->input->post('breakoutLimit'.$breakoutStreamRowId);
          $registrantLimitValidate ='';
          if(is_array($registrantIDs)){
            foreach($registrantIDs as $regkey=> $val){
              $registrantLimitValidate = 'registrantlimit'.$breakoutStreamRowId.'_'.$regkey;
              $limit[] = $this->ci->input->post($registrantLimitValidate);
            }
            if($registrantLimitValidate!=""){ 
               $this->ci->form_validation->set_rules($registrantLimitValidate, 'Stream '.$key.' registrant limit', 'required');
            }
          }
          
          if(array_sum($limit)>$streamLimits){
            $this->ci->form_validation->set_rules('streamLimit', 'Stream '.$key.' stream limit can not be exceed from registrant limit', 'callback_custom_error_set');
          }
        }
        
        //check entered stream limit with total limit
        if($this->ci->input->post('breakoutLimit'.$breakoutStreamRowId)){
          $streamLimit[] = $this->ci->input->post('breakoutLimit'.$breakoutStreamRowId);  
        }
        
        $getSessionPostIdArray  =   $this->ci->input->post('breakoutSessionId'.$breakoutStreamRowId);
          if(!empty($getSessionPostIdArray)){
          
           foreach($getSessionPostIdArray as $sessionId=>$sessionValue){  
          
          
          $this->ci->form_validation->set_rules('sessionStartTime'.$breakoutStreamRowId.'_'.$sessionId,'Stream '.$key.', Session '.$sessionId.' start time','required');
          $this->ci->form_validation->set_rules('sessionFinishTime'.$breakoutStreamRowId.'_'.$sessionId,'Stream '.$key.', Session '.$sessionId.' end time','required');
          
          // Check session time       
          $sessionStartTime  = strtotime($this->ci->input->post('sessionStartTime'.$breakoutStreamRowId.'_'.$sessionId));
          $sessionEndTime    = strtotime($this->ci->input->post('sessionStartTime'.$breakoutStreamRowId.'_'.$sessionId));
              
            // start time should be less than end time
            if($sessionStartTime>$sessionEndTime){  
              $this->ci->form_validation->set_rules('sessionTimechk'.$breakoutStreamRowId.'_'.$sessionId, 'Stream '.$key.', Session '.$sessionId.' start time should be less than end time.', 'callback_custom_error_set');
            }
          
            //check enter  time with selected block time 
            if(!empty($getBreakoutData)){
              $blockStartTimeVal = strtotime($getBreakoutData->start_time);
              $blockEndTimeVal   = strtotime($getBreakoutData->end_time);
              if($blockStartTimeVal > $sessionStartTime || $blockEndTimeVal < $sessionStartTime || $blockStartTimeVal > $sessionEndTime || $blockEndTimeVal < $sessionEndTime){
                $this->ci->form_validation->set_rules('sessionstreamchk'.$breakoutStreamRowId, 'Stream '.$key.', Session '.$sessionId.' start and end time should be between selected block time.', 'callback_custom_error_set');
              }
            }
          }//end loop 
         }//end if  
        
      } //end foreach
        
      if(!empty($getEventDataForLimitChk)){
        $totalNumberOfRegis       = $getEventDataForLimitChk->number_of_rego; 
        $totalStreamLimit     = array_sum($streamLimit);
      
        if($totalNumberOfRegis < $totalStreamLimit){
          $this->ci->form_validation->set_rules('sessionlimitchk'.$breakoutStreamRowId, 'Stream Limit '.$breakoutStreamRow.' limit should be less than from total limit.', 'callback_custom_error_set');
        }
      } 
    
    }
    
  }
  
  
  
  //------------------------------------------------------------------------------------
  
  /*
   * @access: private
   * @description: This function is used to validate breatkout session section
   * @param: breakoutId (int)
   * @param: getEventDataForLimitChk (array)
   * @return void
  */  
  private function _breakoutSessionValidate($breakoutId,$getEventDataForLimitChk){
    
    $whereCondi     = array('id'=>$breakoutId);
    $getBreakoutData  = $this->ci->common_model->getDataFromTabel('breakouts','start_time,end_time',$whereCondi);
    $sessionLimitArray  = array();
    if(!empty($getBreakoutData)){
      $getBreakoutData = $getBreakoutData[0];
    }
      
    //set daynamic field validation is breakout session
    if($this->ci->input->post('breakoutSessionId'.$breakoutId)){
      $getPostIdArray  =  $this->ci->input->post('breakoutSessionId'.$breakoutId);
       
       $count='1';
      foreach($getPostIdArray as $key => $value){
        $limit = array();
        $breakoutSessionRow   = $key;
        $breakoutSessionRowId = $breakoutId.'_'.$breakoutSessionRow;
        
        //set validation
        $this->ci->form_validation->set_rules('sessionName'.$breakoutSessionRowId, 'session start time '.$key, 'trim|required');
        $this->ci->form_validation->set_rules('sessionStartTime'.$breakoutSessionRowId, 'session start time '.$key, 'trim|required');
        $this->ci->form_validation->set_rules('sessionFinishTime'.$breakoutSessionRowId, 'session end time '.$key, 'trim|required');
        $this->ci->form_validation->set_rules('breakoutSessionVenue'.$breakoutSessionRowId, 'session location '.$key, 'trim|required');
        $this->ci->form_validation->set_rules('breakoutSessionLimit'.$breakoutSessionRowId, 'session  limit  '.$key, 'trim|required|numeric');
        
        $sessionLimitArray[] = $this->ci->input->post('breakoutSessionLimit'.$breakoutSessionRowId);
        
        if($this->ci->input->post('restrictSessionAccess'.$breakoutSessionRowId)=='1'){
          $this->ci->form_validation->set_rules('registrantcheckbox'.$breakoutSessionRowId, 'session restrict stream access '.$key.' registrant type ', 'required');
          $registrantIDs = $this->ci->input->post('registrantcheckbox'.$breakoutSessionRowId);
          $sessionLimit  = $this->ci->input->post('breakoutSessionLimit'.$breakoutSessionRowId);
          $registrantLimitValidate ='';
          if(is_array($registrantIDs)){
            foreach($registrantIDs as $regkey=> $val){
              $registrantLimitValidate = 'registrantlimit'.$breakoutSessionRowId.'_'.$regkey;
              $limit[] = $this->ci->input->post($registrantLimitValidate);
            }
            if($registrantLimitValidate!=""){ 
               $this->ci->form_validation->set_rules($registrantLimitValidate, 'Session '.$breakoutSessionRow.' registrant limit', 'required');
            }
          }
          
          if(array_sum($limit)>$sessionLimit){
            $this->ci->form_validation->set_rules('sessionLimit', 'Session '.$breakoutSessionRow.' session limit can not be exceed from registrant limit', 'callback_custom_error_set');
          }
        }
        
        
        // Check session time       
        $sessionStartTime  = strtotime($this->ci->input->post('sessionStartTime'.$breakoutSessionRowId));
        $sessionEndTime    = strtotime($this->ci->input->post('sessionFinishTime'.$breakoutSessionRowId));
          
          // start time should be less than end time
          if($sessionStartTime>$sessionEndTime){  
            $this->ci->form_validation->set_rules('sessionTimechk'.$breakoutSessionRowId, 'Session '.$key.' start time should be less than end time.', 'callback_custom_error_set');
          }
        
          //check enter  time with selected block time 
          if(!empty($getBreakoutData)){
            $blockStartTimeVal = strtotime($getBreakoutData->start_time);
            $blockEndTimeVal   = strtotime($getBreakoutData->end_time);
            if($blockStartTimeVal > $sessionStartTime || $blockEndTimeVal < $sessionStartTime || $blockStartTimeVal > $sessionEndTime || $blockEndTimeVal < $sessionEndTime){
              $this->ci->form_validation->set_rules('sessionstreamchk'.$breakoutSessionRowId, 'Session '.$key.' start and end time should be between selected block time.', 'callback_custom_error_set');
            }
          }
        
          $sessionTimes[] = array('sessionStartTime'=>$sessionStartTime,'sessionFinishTime'=>$sessionEndTime,'rowId'=>$breakoutSessionRow,'breakoutSessionRowId'=>$breakoutSessionRowId);
          
          $count++;
      }
      
      
      //check entered session limit with total limit
      if(!empty($getEventDataForLimitChk)){
        $totalNumberOfRegis       = $getEventDataForLimitChk->number_of_rego; 
        $totalSessionLimit      = array_sum($sessionLimitArray);   // get total limit of session
        if($totalNumberOfRegis < $totalSessionLimit){
          $this->ci->form_validation->set_rules('sessionlimitchk'.$breakoutSessionRowId, 'Session limit should be less than from total limit.', 'callback_custom_error_set');
        }
      } // end if
      
      
      if(!empty($sessionTimes)){
        foreach($sessionTimes as $key=>$value){
          foreach($sessionTimes as $keyPoint=>$time){
            if($key!=$keyPoint){
            $x = $value['sessionStartTime'];    
            $y = $value['sessionFinishTime'];    
            if((($time['sessionStartTime']<$x && $time['sessionFinishTime']<$y && ($time['sessionStartTime']<$y && $time['sessionFinishTime']<$x)) || ($time['sessionStartTime']>$x && $time['sessionFinishTime']>$y && ($time['sessionStartTime']>$y && $time['sessionFinishTime']>$x)))){
            }else{
              $this->ci->form_validation->set_rules('sessionTimechk'.$breakoutSessionRowId, 'Session '.$value['rowId'].' Session time can not be overlap.', 'callback_custom_error_set');
            }
            } //end if  
          } // end foreach
        }
      } // end if 
      
    }
    
    
  }
  
  
  
  //------------------------------------------------------------------------------------
  
  private function  _getStreamBreakoutData($breakoutId){
  
    $updateData['number_of_blocks']   = $this->ci->input->post('blockBreakoutNumbHidden'.$breakoutId);
    $updateData['number_of_streams']  = $this->ci->input->post('breakoutNumberOfStreams'.$breakoutId);
    $updateData['number_of_session']  = $this->ci->input->post('breakoutNumberOfSession'.$breakoutId);
    $updateData['allow_stream_hopping'] = $this->ci->input->post('allowStreamHopping'.$breakoutId);
    return $updateData;
  } 
  
  /*
   * @access: private
   * @description: This function is used to save breakout data 
   * @param: breakoutId
   * @return void
   */  
  
  private function _breakoutsDataSave($breakoutId,$updateData){
    
    $whereRegis = array('id'=>$breakoutId); 
    $status = $this->ci->common_model->updateDataFromTabel('breakouts',$updateData,$whereRegis);
    return $status;
  }
  
    //------------------------------------------------------------------------------------
  
  /*
   * @access: private
   * @description: This function is used to prepaired data to update
   * @param: breakoutId
   * @return array
   */  
  
  private function _getSessionBreakoutData($breakoutId,$breakoutNumberOfSession){
    $updateData['number_of_session']  = $breakoutNumberOfSession;
    $updateData['number_of_streams']  = 0; // if block type belongs to plenary so it does not contains any streams
    return $updateData;
  }
  

  //----------------------------------------------------------------------------------
  
  /**
   * @access: private
   * @description: This function is used to save breakout stream data 
   * @param: breakoutId
   * @return void
   * 
  **/  
  
  private function _breakoutsStreamSave($breakoutId){
    
    //define blank varibale
      $sessionArray=array();
    $returnArray = array();
    $streamArray=array();
    $insertId='';
  
    //if breakout stream has field then insert and update record
    
    if($this->ci->input->post('breakoutStreamId'.$breakoutId)){
      //print_r($this->ci->input->post());die;
      
      $getPostIdArray  =  $this->ci->input->post('breakoutStreamId'.$breakoutId);
      
      foreach($getPostIdArray as $key => $value){
        
        //define value in command variable
        $breakoutPostId   = $value;
        $breakoutRowKey     = $key;
        $breakoutRowKeyId   = $breakoutId.'_'.$breakoutRowKey;
        $dd=$this->ci->input->post('breakoutStreamVenue'.$breakoutRowKeyId);
        
        //set value for insert and update 
        $streamData['stream_name']        = $this->ci->input->post('breakoutStreamName'.$breakoutRowKeyId);
        $streamData['stream_venue']       = $this->ci->input->post('breakoutStreamVenue'.$breakoutRowKeyId);
        
        $streamData['number_of_session']       = $this->ci->input->post('breakoutNumberOfSession'.$breakoutRowKeyId);
        
        
        $streamData['block_limit']        = $this->ci->input->post('breakoutLimit'.$breakoutRowKeyId);
        $streamData['restrict_stream_access']   = $this->ci->input->post('restrictStreamAccess'.$breakoutRowKeyId);
        $streamData['stream']             = $breakoutRowKey;
        $streamData['breakout_id']        = $breakoutId;
        
        //if greater then zero then update data otherwise add data
        if($breakoutPostId > 0){
          $streamCondi = array('id'=>$breakoutPostId);  
          $this->ci->common_model->updateDataFromTabel('breakout_stream',$streamData,$streamCondi);
          //echo $this->ci->db->last_query(); die;
          //set stream id in array
          $insertId=$breakoutPostId;
          $streamSessionIdArrayStream[$breakoutRowKey] = $breakoutPostId;
        }else{
          $insertId = $this->ci->common_model->addDataIntoTabel('breakout_stream', $streamData);
          //set stream id in array
          $streamSessionIdArrayStream[$breakoutRowKey] = $insertId;
        }
        
        //create array set in return array variable
        $returnArray['streamId'] = $streamSessionIdArrayStream;
        
        //to add stream id to remove
        $streamArray[] = $insertId;
        
        //to save session of streamsessionArray
        $sessionArray[$insertId]=$this->_breakoutsSessionSave($breakoutId,$key,$insertId); 
        
        // Store registrant Ids with respect to stream
        $this->_streamRegistrantSave($breakoutId,$key,$insertId); 
        
      }
      
      // delete record except to inserted ids (From Stream Table)
      $whereDeleteCondi = array('breakout_id'=>$breakoutId);
      $this->ci->common_model->deletelWhereWhereIn('breakout_stream',$whereDeleteCondi,'id','',$streamArray);
      
      
    }else{
      //delete data from database of not checked 
      $whereDeleteCondi = array('breakout_id'=>$breakoutId);
      $this->ci->common_model->deleteRowFromTabel('breakout_stream',$whereDeleteCondi);
      
      //delete data from database of not checked 
      $whereDeleteCondi = array('breakout_id'=>$breakoutId);
      $this->ci->common_model->deleteRowFromTabel('breakout_session',$whereDeleteCondi);
      
    }
     
     //to marge array in sassion array 
     $returnArray['sessionId']=$sessionArray;
      
     return $returnArray;
  }
  
  //----------------------------------------------------------------------------------
  
  /**
   * @access: private
   * @description: This function is used to save breakout session data 
   * @param: breakoutId
   * @return void
   * 
  **/  
  private function _breakoutsSessionDataSave($breakoutId){
    $sessionIdArray   = array();
    if($this->ci->input->post('breakoutSessionId'.$breakoutId)){
      
      $sessionIdArray = array();
      $getPostIdArray  =  $this->ci->input->post('breakoutSessionId'.$breakoutId);
      
    
      foreach($getPostIdArray as $key => $value){
        
        //define value in command variable
        $breakoutPostId   = $value;
        $breakoutRowKey     = $key;
        $breakoutRowKeyId   = $breakoutId.'_'.$breakoutRowKey;
        
        //set value for insert and update 
        $sessionData['session_name']      = $this->ci->input->post('sessionName'.$breakoutRowKeyId);
        $sessionData['start_time']        = $this->ci->input->post('sessionStartTime'.$breakoutRowKeyId);
        $sessionData['end_time']          = $this->ci->input->post('sessionFinishTime'.$breakoutRowKeyId);
        $sessionData['session_location']      = $this->ci->input->post('breakoutSessionVenue'.$breakoutRowKeyId);
        $sessionData['session_limit']       = $this->ci->input->post('breakoutSessionLimit'.$breakoutRowKeyId);
        $sessionData['session_restrict_access'] = $this->ci->input->post('restrictSessionAccess'.$breakoutRowKeyId);
        
        $sessionData['session']             = $breakoutRowKey;
        $sessionData['breakout_id']         = $breakoutId;
        
        
        //print_r($this->ci->input->post());
      
        $sessionData['number_of_presenters']    = $this->ci->input->post('noOfPresenter'.$breakoutRowKeyId);
        $sessionData['presenters_description']  = $this->ci->input->post('presenter_description'.$breakoutRowKeyId);
        
        // add presenter ids in json format
        
        $presenterIds = $this->ci->input->post('presenters_select_'.$breakoutRowKeyId);
        $presenters = '';
        if(is_array($presenterIds) && !empty($presenterIds)){
          $presenters = json_encode($presenterIds);
        }
        
        $sessionData['presenters_ids']  = $presenters;
        
        
        
        //print_r($sessionData);die;
        //if greater then zero then update data otherwise add data
        if($breakoutPostId > 0){
          $sessionCondi = array('id'=>$breakoutPostId); 
          $this->ci->common_model->updateDataFromTabel('breakout_session',$sessionData,$sessionCondi);
          //set stream id in array
          $insertId=$breakoutPostId;
          $sessionIdArray[$breakoutRowKey] = $breakoutPostId;
        }else{
          
          $insertId = $this->ci->common_model->addDataIntoTabel('breakout_session', $sessionData);
          //set stream id in array
          $sessionIdArray[$breakoutRowKey] = $insertId;
                    
        }
        
        // Store registrant Ids with respect to session
        $this->_sessionRegistrantSave($breakoutId,$key,$insertId); 
        
      }//end loop
      
      
    }//end if 
    
      return $sessionIdArray; 
  }
  
  /**
   * @access: private
   * @description: This function is used to save stream registrant data
   * @param: breakoutId
   * @return void
   * 
  **/  
  
  private function _sessionRegistrantSave($breakoutId,$sessionKey,$sessionId){
     
     $registrantInsertRecord = array();
     $registrantkey = $breakoutId.'_'.$sessionKey;
     
     // delete record if exist then insert new records againts session id
     $where = array('session_id'=>$sessionId);
     $this->ci->common_model->deleteRowFromTabel('breakouts_session_registrants',$where);
     
    if($this->ci->input->post('restrictSessionAccess'.$registrantkey)=='1'){
       $registrants = $this->ci->input->post('registrantcheckbox'.$registrantkey);
       if(!empty($registrants)){
        foreach($registrants as $key=>$value){  
      
          $registrantData['stream_id']      =  0;
          $registrantData['session_id']     =  $sessionId;
          $registrantData['registrant_id']    =  $key;
          $registrantData['session_limit']  =  $this->ci->input->post('registrantlimit'.$registrantkey.'_'.$key);
          
          $registrantInsertRecord[] = $registrantData;
        
        } //end loop
       }
       if(!empty($registrantInsertRecord)){
        $this->ci->common_model->addMultipleDataIntoTabel('breakouts_session_registrants', $registrantInsertRecord);
       }
      } 
  }
  
  
  /**
   * @access: private
   * @description: This function is used to save stream registrant data
   * @param: breakoutId
   * @return void
   * 
  **/  
  private function _streamRegistrantSave($breakoutId,$streamKey,$streamId){
     
     $registrantInsertRecord = array();
     $registrantkey = $breakoutId.'_'.$streamKey;
     
     //remove data from breakouts_session_registrants
    $where=array('stream_id'=>$streamId);
    $this->ci->common_model->deleteRowFromTabel('breakouts_stream_registrants',$where);
    
    if($this->ci->input->post('restrictStreamAccess'.$registrantkey)=='1'){
      $registrants = $this->ci->input->post('registrantcheckbox'.$registrantkey);
       
       if(!empty($registrants)){
        
        foreach($registrants as $key=>$value){  
      
          $registrantData['stream_id']      =  $streamId;
          $registrantData['registrant_id']    =  $key;
          $registrantData['registrant_limit'] =  $this->ci->input->post('registrantlimit'.$registrantkey.'_'.$key);
          
          $registrantInsertRecord[] = $registrantData;
        
        } //end loop
        
       }
       if(!empty($registrantInsertRecord)){
        $this->ci->common_model->addMultipleDataIntoTabel('breakouts_stream_registrants', $registrantInsertRecord);
      }
      } 
  }
  
  //----------------------------------------------------------------------------------
  
  /**
   * @access : private
   * @description: This function is used to save breakout session data 
   * @param: breakoutId
   * @return void
   * 
  **/  
  
  private function _breakoutsSessionSave($breakoutId,$streamKey,$streamId){
    
    //define blank varibale
    $returnArray= '';
    $sessionId='';
    $removeArray=array();
    
    
    /*echo "BreakoutID: ".$breakoutId;
    echo "streamKey: ".$streamKey;
    echo "streamId: ".$streamId;
        
    print_r($this->ci->input->post());die;*/         
    //if breakout session has field then insert and update record
    if($this->ci->input->post('breakoutSessionId'.$breakoutId.'_')){
      $count=0;
      $getPostIdArray = $this->ci->input->post('breakoutSessionId'.$breakoutId.'_');
      //print_r($getPostIdArray);die;       
      foreach($getPostIdArray as $key => $value)
      {
         
        $count++;
         
        //define value in command variable
        $breakoutPostId   = $value;
        $breakoutRowKey     = $count;
        //$breakoutRowKeyId   = $breakoutId.'_'.$streamKey.'_'.$breakoutRowKey;
        $breakoutRowKeyId   = $breakoutId.'_'.$breakoutRowKey;
        
        $sessionblockselect = $this->ci->input->post('sessionblockselect'.$breakoutRowKeyId);
        //set value for insert and update 
      
        $sessionData['end_time']            = $this->ci->input->post('sessionFinishTime'.$breakoutRowKeyId);
        $sessionData['start_time']          = $this->ci->input->post('sessionStartTime'.$breakoutRowKeyId);
        $sessionData['stream_id']         = $streamId;
        $sessionData['session']             = $count;
        $sessionData['breakout_id']         = $breakoutId;
        
        $sessionData['session_name']          = $this->ci->input->post('sessionName'.$breakoutRowKeyId);
        $sessionData['number_of_presenters']    = $this->ci->input->post('noOfPresenter'.$breakoutRowKeyId);
        $sessionData['presenters_description']  = $this->ci->input->post('presenter_description'.$breakoutRowKeyId);
        
        // add presenter ids in json format
        
        $presenterIds = $this->ci->input->post('presenters_select_'.$breakoutRowKeyId);
        $presenters = '';
        if(is_array($presenterIds) && !empty($presenterIds)){
          $presenters = json_encode($presenterIds);
        }
        
        $sessionData['presenters_ids']  = $presenters;
        
        //if greater then zero then update data otherwise add data
        if($breakoutPostId > 0){
          $removeArray[]=$breakoutPostId;
          $sessionId=$breakoutPostId;
          $streamSessionIdArraySession[$breakoutRowKey] =$breakoutPostId;
          
          $streamCondi = array('id'=>$breakoutPostId);  
          $this->ci->common_model->updateDataFromTabel('breakout_session',$sessionData,$streamCondi);
          //echo $this->ci->db->last_query(); die;
          //set session id in array
          $streamSessionIdArraySession[$breakoutRowKey] = $breakoutPostId;
        }else{
             
            $sessionId= $this->ci->common_model->addDataIntoTabel('breakout_session', $sessionData);
            $removeArray[]=$sessionId;
            //set session id in array
            $streamSessionIdArraySession[$breakoutRowKey] = $sessionId;
          
        }
        
        
      } 
      
       //remove session 
       /*
       if(!empty($removeArray)){
         $where=array('stream_id'=>$streamId);
         //print_r($removeArray);
         $this->ci->common_model->deletelWhereWhereIn('breakout_session',$where,'id','',$removeArray);
         
         } 
         */
        //Create array set in returnArray 
          $returnArray = $streamSessionIdArraySession;
      
    }else{
      
      //delete data from database of not checked
      $whereDeleteCondi = array('breakout_id'=>$breakoutId,'stream_id'=>$streamId);
      $this->ci->common_model->deleteRowFromTabel('breakout_session',$whereDeleteCondi);
        
    }
        
       return $returnArray;
  }
  
  //----------------------------------------------------------------------------------
  
  /**
   * @access : private
   * @description: This function is used to remove session stream details and session 
   * @param: breakoutId (int)
   * @param: breakoutNumberOfSession (int)
   * @return void
   * 
  **/  
  
  private function _breakoutsRemoveSessionStream($breakoutId,$breakoutNumberOfSession){
    
    //This section will delete session and session stream details records if use change steam session value
  
    $whereDeleteCondi = array('breakout_id'=>$breakoutId,'session > '=>$breakoutNumberOfSession);
    $breakoutSessionData = $this->ci->common_model->getDataFromTabel('breakout_session','id',$whereDeleteCondi,'','id','ASC');
    if(!empty($breakoutSessionData)){
      foreach($breakoutSessionData as $sessionData ){
          $deleteSessionId= $sessionData->id;
          $whereDeleteSessStream = array('breakout_session_id'=>$deleteSessionId);
          //delete record from breakout stream session table
          $this->ci->common_model->deleteRowFromTabel('breakout_stream_session',$whereDeleteSessStream);
      }
    }
    
    return true;
  }
  
  //----------------------------------------------------------------------------------
  
  /**
   * @access : private
   * @description: This function is used to save breakout session stream details data 
   * @param: breakoutId (int)
   * @param: streamSessionIdArray (array)
   * @return void
   * 
  **/  
  
   private function _breakoutsSessionStreamSave($breakoutId,$streamSessionIdArray)
   {
      
    $streamCount='1';
    //if breakout stream session details has field then insert and update record
    $removeArray=array();
    $streamId='';
    if($this->ci->input->post('breakoutStreamId'.$breakoutId))
    {
      $getPostFirstIdArray  =$this->ci->input->post('breakoutStreamId'.$breakoutId);
      foreach($getPostFirstIdArray as $key => $streamId)
      { 
             
        //define stream key and value
        //$firstKey = $key;
        $postArrayKeyId =  $breakoutId.'_'.$key;

        if($this->ci->input->post('breakoutSessionId'.$breakoutId.'_'.$key))
        {
          $getPostIdArray  =  $this->ci->input->post('breakoutSessionId'.$breakoutId.'_'.$key);
              
          $count='1';
          foreach($getPostIdArray as $key => $value)
          {
            
            //$getStreamSessionPostIdArray  = $this->ci->input->post('breakoutStreamSessionId'.$breakoutId);
            
            //define value in command variable
            $breakoutRowKeyId      = $key;
            $breakoutPostId      =  $value; // $getStreamSessionPostIdArray[$breakoutRowKeySession];
             
            
            //set value for insert and update 
            $streamSessionData['about_session']   = $this->ci->input->post('breakoutAboutSession'.$breakoutRowKeyId);
            $streamSessionData['speakers']        = $this->ci->input->post('breakoutSpeakers'.$breakoutRowKeyId);
            $streamSessionData['organisation']      = $this->ci->input->post('breakoutOrganisation'.$breakoutRowKeyId);
          
            if(!empty($streamSessionIdArray)){
              if(isset($streamSessionIdArray['streamId']) && !empty($streamSessionIdArray['streamId'])){
                $streamId= $streamSessionIdArray['streamId'][$streamCount];
                $streamSessionData['breakout_stream_id']  = $streamId;
              }
            }
            
            if(!empty($streamSessionIdArray)){
              if(isset($streamSessionIdArray['sessionId']) && !empty($streamSessionIdArray['sessionId'])){
                $sessionData= $streamSessionIdArray['sessionId'][$streamId]; 
                
                  $streamSessionData['breakout_session_id']=$sessionData[$count];
                  $removeArray[]=$sessionData[$count];
               
              }
            }
                        
            //if greater then zero then update data otherwise add data
            if($breakoutPostId > 0){
              
              $removeArray[]=$breakoutPostId;
              $sessionStreamCondi = array('breakout_session_id'=>$breakoutPostId,'breakout_stream_id'=>$streamId);  
              $this->ci->common_model->updateDataFromTabel('breakout_stream_session',$streamSessionData,$sessionStreamCondi);
            
            }else{
                 $insertId=$this->ci->common_model->addDataIntoTabel('breakout_stream_session', $streamSessionData);
                   //echo $insertId; die;
            } 
              $count++;
            
          }
            
        }
          $streamCount++;
          
         //This function call for save breakout session data 
             $where=array('breakout_stream_id'=>$streamId);
             if(!empty($removeArray)){
                $this->ci->common_model->deletelWhereWhereIn('breakout_stream_session',$where,'breakout_session_id','',$removeArray);
                }else{
           //remove all stream details    
           $this->ci->common_model->deleteRowFromTabel('breakout_stream_session',$where); 
        }
         
      }
                
    }
      
    return true;
  }
  
  // ------------------------------------------------------------------------ 
  
  /*
   * @access : public
   * @description: This function is used for setup sponsors  
   * @param: eventId
   * @return: void
   * 
   */ 
  
  public function setupsponsors(){
    
    //current open eventId
    $eventId = currentEventId();
    
    //decode entered eventid
    $eventId = decode($eventId); 
    
    //check is data post 
    if($this->ci->input->post()){
      
        //call insert and update data function
        $this->insertupdatesponsors();
    }else{
      
      // load sponsor setup form view
      $this->sponsorsstepsviews($eventId);
    }
  }
  
  // ------------------------------------------------------------------------ 
  
  /*
  * @access: private 
    * @description: This function is used to load sponsor setup view load
    * @param1: $eventId
    * @return: void
    *  
    */
  
  public function sponsorsstepsviews($eventId="0"){
    
    //chheck if eventId is zero 
    if($eventId==0){
      redirect(base_url('dashboard'));
    }
    $userId          = isLoginUser();
    $data['userId']      = $userId;
    $data['popupHeaderBg'] = 'sponsor_bg_header';//set popup header background color
    $data['popupButtonBg'] = 'sponsor_type_bg_button'; // popup button bg class 
    $data['eventId']     = $eventId;
    
    $where       = array('event_id'=>$eventId);
    $registrantWhere = array('event_id'=>$eventId,'registrants_limit !=' => '');
    //get data from exhibitor booth table
    $data['exhibitorbooths']  = $this->ci->common_model->getDataFromTabel('sponsor_exhibitor_booths','*',$where,'','id','ASC');
    //get sponsor data
    $data['eventsponsor']     = $this->ci->common_model->getDataFromTabel('sponsor','*',$where,'','id','ASC');
    //get exhbitor floor plan data
    $data['exhibitorFloorPlan'] = $this->ci->common_model->getDataFromTabel('exhibitor_floor_plan','*',$where,'','id','ASC');
    //get side event data
    $data['eventsideevent']   = $this->ci->common_model->getDataFromTabel('side_event','*',$where,'','id','ASC');
    //get event registrant data
    $data['eventregistrants']   = $this->ci->common_model->getDataFromTabel('event_registrants','*',$registrantWhere,'','id','ASC');
    $this->ci->template->load('template','sponsors_steps',$data,TRUE);
  }
  
  // ------------------------------------------------------------------------ 
  
   /*
    * @access : public
  * @description: This function is used to add exhibitor floor plan
  * @return void
  * 
  */ 
  
  public function addexhibitorfloorplan(){
    
    $eventId           = $this->ci->input->post('eventId');
    $numberOfBooths        = $this->ci->input->post('numberOfBooths');
    $exhibitorFloorPlanId = $this->ci->input->post('exhibitorFloorPlanIdField');
    $updateData['number_of_booth'] = $numberOfBooths;
    $updateData['event_id'] = $eventId;
    
    if($exhibitorFloorPlanId > 0){
      // update data id by id
      $upWhere = array('id'=>$exhibitorFloorPlanId);
      $this->ci->common_model->updateDataFromTabel('exhibitor_floor_plan',$updateData,$upWhere);
      $msg = lang('event_msg_exhibitor_floor_plan_updated');
    }
    else{
      // insert data id by id
      $exhibitorFloorPlanId = $floorPlanId = $this->ci->common_model->addDataIntoTabel('exhibitor_floor_plan', $updateData);
      $msg = lang('event_msg_exhibitor_floor_plan_added');
    } 
    //$returnArray=array('msg'=>$msg,'is_success'=>'true','exhibitorfloorplanid'=>$exhibitorFloorPlanId);
    $returnArray = array('msg'=>$msg,'is_success'=>'true');
    set_global_messages($returnArray);
    echo json_encode($returnArray);
  }
  
  // ------------------------------------------------------------------------ 
  
   /*
    * @access : public
  * @description: This function is used to update exhibitor floor plan
  * @return void
  * 
  */ 

  
  public function updateexhibitorfloorplan(){
    
    $dirUploadPath = './media/floor_plan/';
    $dirSavePath = '/media/floor_plan/';
    $uploadData = $this->ci->process_upload->upload_file($dirUploadPath);
    $eventId = $_REQUEST['event_id'];
    $updateData['floor_plan'] = $uploadData['filename'];
    $updateData['floor_plan_path'] = $dirSavePath;
    $updateData['event_id'] = $eventId;
    $fileId = remove_extension($uploadData['filename']);
    $upWhere = array('event_id'=>$eventId);
    $this->ci->common_model->updateDataFromTabel('exhibitor_floor_plan',$updateData,$upWhere);
    echo json_encode(array('id'=>$floorPlanId,'fileId'=>$fileId));
  } 
  
  // ------------------------------------------------------------------------ 
  
  /*
   * @access :   public
   * @description: This function is used to delete floor plan document
   * @return void
   */ 
   
  public function deleteexhibitorfloorplan(){
    
    $floorPlanId          = $this->ci->input->post('deleteId');
    $updateData['floor_plan']     = '';
    $updateData['floor_plan_path']  = '';
    $where              = array('id'=>$floorPlanId);
    
    //check record is exist 
    $getResult = $this->ci->common_model->getDataFromTabel('exhibitor_floor_plan','floor_plan,floor_plan_path',$where);
    if(!empty($getResult)){
      //delte image from directory
      findFileNDelete($getResult[0]->floor_plan_path, $getResult[0]->floor_plan);
    }
    
    $this->ci->common_model->updateDataFromTabel('exhibitor_floor_plan',$updateData,$where);
    echo json_message('msg','Floor plan document deleted.');
  } 
  
  // ------------------------------------------------------------------------ 
  
  /*
   * @access :   public
   * @description: This function is used to open add & edit sponsor popup view
   * @return void
   * 
   */ 
   
  public function addeditsponsorsview(){
    
    $data['eventId'] = $this->ci->input->post('eventId');
    $sponsorsIdValue = $this->ci->input->post('sponsorsId');
    $data['sponsorsIdValue'] = $sponsorsIdValue;
    $where = array('id'=>$sponsorsIdValue);
    //get data form custome form field table
    $data['sponsorsdata'] = $this->ci->common_model->getDataFromTabel('sponsor','sponsor_name',$where,'','id','ASC','1');
    if(!empty($data['sponsorsdata'])){
      $data['sponsorsdata'] = $data['sponsorsdata'][0];
    }
  
    $this->ci->load->view('add_edit_sponsor_popup',$data);
  }
  
  // ------------------------------------------------------------------------ 
  
  /*
   * @access: public
   * @description: This function is used to add sponsor  
   * @return void
   * 
   */ 
   
  public function addeditsponsorssave(){
    
    //set validation
    $this->ci->form_validation->set_rules('sponsorName', 'sponsor name', 'trim|required');
    
    if($this->ci->form_validation->run($this->ci)){
      $eventId = $this->ci->input->post('eventId');
      $sponsorName = $this->ci->input->post('sponsorName');
      $sponsorsIdValue = $this->ci->input->post('sponsorsId');
      $insertData['sponsor_name'] = $sponsorName;
      $insertData['event_id'] = $eventId;
    
      if($sponsorsIdValue > 0){
        $where = array('id'=>$sponsorsIdValue);
        $this->ci->common_model->updateDataFromTabel('sponsor',$insertData,$where);
        $msg = lang('event_msg_sponsor_title_updated_successfully');
      }else{
        $insertId = $this->ci->common_model->addDataIntoTabel('sponsor', $insertData);
        $msg = lang('event_msg_sponsor_add_successfully');
      }
      $returnArray=array('msg'=>$msg,'is_success'=>'true'); 
      set_global_messages($returnArray);
      echo json_encode($returnArray);
    }else{
      $errors = $this->ci->form_validation->error_array();
      echo json_message('msg',$errors,'is_success','false');
    }
  }
  
  // ------------------------------------------------------------------------ 
  
  /*
   * @access: public
   * @description: This function is used to open add & edit sponsor additional purchase
   * items popup view
   * @return void
   * 
   */ 
   
  public function addeditpurchaseitemview(){
    
    $data['sponsorsIdValue'] = $this->ci->input->post('sponsorsId');
    $itemIdValue       = $this->ci->input->post('itemId');
    $data['itemIdValue']    = $itemIdValue;
    $where = array('id'=>$itemIdValue);
    //get data form custome form field table
    $data['additionalitemsdata'] = $this->ci->common_model->getDataFromTabel('sponsor_benefits','item_title',$where,'','id','ASC','1');
    if(!empty($data['additionalitemsdata'])){
      $data['additionalitemsdata'] = $data['additionalitemsdata'][0];
    }
  
    $this->ci->load->view('sponsor_purchase_item_popup',$data);
  }
  
  // ------------------------------------------------------------------------ 
  
  /*
   * @access: public
   * @description: This function is used to add sponsor additional purchase
   * items popup view
   * @return void
   * 
   */ 
   
  public function addeditpurchaseitemsave(){
    
    //set validation
    $this->ci->form_validation->set_rules('itemName', 'item name', 'trim|required');
    
    if($this->ci->form_validation->run($this->ci)){
      $itemIdValue = $this->ci->input->post('itemId');
      $itemName = $this->ci->input->post('itemName');
      $sponsorsId = $this->ci->input->post('sponsorsId');
      $insertData['item_title'] = $itemName;
      $insertData['sponsor_id'] = $sponsorsId;
      
      if($itemIdValue > 0){
        $where = array('id'=>$itemIdValue);
        $this->ci->common_model->updateDataFromTabel('sponsor_benefits',$insertData,$where);
        $msg = lang('event_msg_sponsor_additional_purchase_item_updated');
      }else{
        $insertId = $this->ci->common_model->addDataIntoTabel('sponsor_benefits', $insertData);
        $msg = lang('event_msg_sponsor_additional_purchase_item_add');
      }
      $returnArray=array('msg'=>$msg,'is_success'=>'true'); 
      set_global_messages($returnArray);
      echo json_encode($returnArray);
    }else{
      $errors = $this->ci->form_validation->error_array();
      echo json_message('msg',$errors,'is_success','false');
    }
  }
  
  // ------------------------------------------------------------------------ 
  
  /*
   * @access : public
   * @description: This function is used to delete purchase item 
   * @return void
   * 
   */   
  
  
  public function deletepurchaseitem(){
  
    $deleteid = $this->ci->input->post('deleteId');
    
    //delete registant record from other sponsor additional items
    $where = array('id'=>$deleteid);
    $this->ci->common_model->deleteRowFromTabel('sponsor_benefits',$where);
    
    $msg     = lang('event_msg_sponsor_additional_purchase_item_deleted');
    $returnArray =array('msg'=>$msg,'is_success'=>'true');
    set_global_messages($returnArray);
    echo json_encode($returnArray);
  }
  
  
  // ------------------------------------------------------------------------ 
  
  /*
   * @access: public
   * @description: This function is used to delete sponsors 
   * @return void
   * 
   */   
  
  
  public function deletesponsors(){
    
    $deleteid = $this->ci->input->post('deleteId');
    
    //delete registant record from other side event table
    $where = array('id'=>$deleteid);
    $whereCondti = array('sponsor_id'=>$deleteid);
    $this->ci->common_model->deleteRowFromTabel('sponsor_benefits',$whereCondti);
    $this->ci->common_model->deleteRowFromTabel('sponsor_exhibitor_booths',$whereCondti);
    $this->ci->common_model->deleteRowFromTabel('sponsor_registrant',$whereCondti);
    $this->ci->common_model->deleteRowFromTabel('sponsor_side_event',$whereCondti);
    $this->ci->common_model->deleteRowFromTabel('sponsor',$where);
    
    $msg = lang('event_msg_sponsor_deleted');
    $returnArray=array('msg'=>$msg,'is_success'=>'true');
    set_global_messages($returnArray);
    echo json_encode($returnArray);
  }
  
  // ------------------------------------------------------------------------ 
  
  /*
   * @access : private
   * @description: This function is used to insert and update sponsors forms data 
   * @return void
   * 
   */ 
  
  function insertupdatesponsors(){
    
    if($this->ci->input->post()){
      
      //print_r($this->ci->input->post());die();
      //get eventId
      $eventId = $this->ci->input->post('eventId');
      
      //get form sponsor id
      $sponsorId = $this->ci->input->post('sponsorId');
      
      //sponsor validate call
      $this->_sponsorRegistranValidate($sponsorId);
      
      //sponsor validate call
      $this->_sponsorDataValidate($sponsorId,$eventId);
    
      //get ajax request
      $is_ajax_request = $this->ci->input->is_ajax_request();
      
      ///check validate
      if ($this->ci->form_validation->run($this->ci))
      {
        //call for registrant data save
        $this->_sponsorRegistranSave($sponsorId);
        
        //call for sponsor data save
        $this->_sponsorDataSave($sponsorId,$eventId);
        
        //call for sponsor booth data save
        $this->_boothNumberSave($sponsorId,$eventId,'sponsor');
        
        //call for sponsor side event save
        $this->_sponsorSideeventSave($sponsorId);
        
        //call for sponsor additional purchase item save
        $this->_sponsorPurchaseItemSave($sponsorId);
        
        // check ajax request
        $msg = lang('event_msg_sponsor_details_saved');
        
        if($is_ajax_request){
          echo json_message('msg',$msg);
          $msgArray = array('msg'=>$msg,'is_success'=>'true');
          //set_global_messages($msgArray);
        }else{
          $msgArray = array('msg'=>$msg,'is_success'=>'true');
          set_global_messages($msgArray);
          redirect('event/setupsponsors/'.encode($eventId));
        }
        
    
      }else{
        $errors = $this->ci->form_validation->error_array();
        if($is_ajax_request){
          echo json_message('msg',$errors,'is_success','false');
        }else{
          // load event form view
          $this->breakoutstepsviews($eventId);
        }
      }
    }else{
        $msg = lang('event_msg_invalid_data_request');
        $msgArray = array('msg'=>$msg,'is_success'=>'false');
        set_global_messages($msgArray);
        redirect(base_url('dashboard'));
      }
  }
  
  //------------------------------------------------------------------------------------
  
  /*
   * @access : private
   * @description: This function is used to validate registrant section
   * @param: sponsorId
   * @return void
   */  
   
  
  private function _sponsorRegistranValidate($sponsorId){
    if($this->ci->input->post('sponsorRegistrantId'.$sponsorId)){
      //get side event registration id from multi dimentional array
      $sponsorRegistaintIdMultiArray = $this->ci->input->post('sponsorRegistrantId'.$sponsorId);
      
      foreach($sponsorRegistaintIdMultiArray as $multikey=>$sponsorRegistaintIdArray){
        
        $getRegistantId = $multikey;
        foreach($sponsorRegistaintIdArray as $key=>$value){
          
          if($key ==0){
            $keyset =  $getRegistantId;
            $getSingleDayRegistId= 0;
          }else{
            $keyset =  $getRegistantId.'_'.$key;
            $getSingleDayRegistId= $key;
          }
          
          $registrantId = $keyset; // registaint id
          $regisSponsorId = $sponsorId.'_'.$registrantId;
          if($this->ci->input->post('registrantSelectionLimit'.$regisSponsorId)){
            $this->ci->form_validation->set_rules('registrantLimit'.$regisSponsorId, 'registrant limit', 'trim|required|numeric');
          
            
            //check entered registrant limit from db 
            if($getSingleDayRegistId==0){
              $where = array('id'=>$getRegistantId);
              $sideEventRegist = getDataFromTabel('event_registrants','registrants_limit as regslimit, registrant_name as regisname',$where);
              //set variable for registrant limit validate
              if(!empty($sideEventRegist)){
                $sideEventRegist = $sideEventRegist[0];
                $getRegistrantLimit = $sideEventRegist->regslimit;
                $getRegistrantName = $sideEventRegist->regisname;
                $validateMsg = 'registrant limit should be less than registant limit.';
              }
            }else{
              
              //get registrant name when validate with day wise
              $where = array('id'=>$getRegistantId);
              $sideEventRegist = getDataFromTabel('event_registrants','registrant_name as regisname',$where);
              if(!empty($sideEventRegist)){
                $sideEventRegist = $sideEventRegist[0];
                $registrantName = $sideEventRegist->regisname;
              }
              
              //get data from daywise registrant table
              $Daywisewhere = array('id'=>$getSingleDayRegistId);
              $sideEventRegistDaywise = getDataFromTabel('event_registrant_daywise','registrant_daywise_limit as regslimit, day',$Daywisewhere);
            
              //set variable for daywise registrant limit validate
              if(!empty($sideEventRegistDaywise)){
                $sideEventRegistDaywise = $sideEventRegistDaywise[0];
                $getRegistrantLimit = $sideEventRegistDaywise->regslimit;
                $getRegistrantDay = $sideEventRegistDaywise->day;
                $getRegistrantName  = $registrantName.' '.$getRegistrantDay;
                $validateMsg = 'registrant limit should be less than daywise registant limit.';
              }
            }
            
            //check condition for sponsor event registrant and single day limit
            $enteredRegistrantLimit = $this->ci->input->post('registrantLimit'.$regisSponsorId);
            if($getRegistrantLimit < $enteredRegistrantLimit && !empty($getRegistrantLimit)){
              $this->ci->form_validation->set_rules('registrantlimitchk'.$registrantId, $getRegistrantName.' '.$validateMsg, 'callback_custom_error_set');
            }
          
          }
          
        }
      } 
    }
  }
  
  //----------------------------------------------------------------------------------
  
  /**
   * @access : private
   * @description: This function is used to validate sponsor data 
   * @param: sponsorId
   * @return void
   * 
  **/  
  
  private function _sponsorDataValidate($sponsorId,$eventId){
    
    //set fixed field validation
    $this->ci->form_validation->set_rules('additionalDetails'.$sponsorId, 'additional details', 'trim|required');
    $this->ci->form_validation->set_rules('limitOfPackage'.$sponsorId, 'limit package', 'trim|required|numeric');
    $this->ci->form_validation->set_rules('totalPrice'.$sponsorId, 'tatal price', 'trim|required|numeric');
    $this->ci->form_validation->set_rules('GSTIncluded'.$sponsorId, 'GST Include', 'trim|require|numericd');
    $this->ci->form_validation->set_rules('additionalBoothPrice'.$sponsorId, 'GST Include', 'trim|required|numeric');
    $this->ci->form_validation->set_rules('additionalBoothGSTIncluded'.$sponsorId, 'GST Include', 'trim|required|numeric');
      
      
    //if complimentary checked
    if($this->ci->input->post('complimentaryRegistration'.$sponsorId)){
      $this->ci->form_validation->set_rules('complimentaryRegistrationsNum'.$sponsorId, 'complimentary number', 'trim|required|numeric');
    }
    
    //if additional guest is checked
    if($this->ci->input->post('additionalGuests'.$sponsorId)){
      $this->ci->form_validation->set_rules('additionalLimitPerPerson'.$sponsorId, 'additional limit per person', 'trim|required|numeric');
      $this->ci->form_validation->set_rules('additionalTotalPrice'.$sponsorId, 'additional total price', 'trim|required|numeric');
      $this->ci->form_validation->set_rules('additionalGSTIncluded'.$sponsorId, 'additional GST', 'trim|required|numeric');
    }
    
    $where = array('id'=>$eventId);
    $getEventDataForLimitChk = $this->ci->common_model->getDataFromTabel('event','number_of_rego',$where);
    $getEventDataForLimitChk = $getEventDataForLimitChk[0];
    
    //check entered sponsor limit with total limit
    if($this->ci->input->post('limitOfPackage'.$sponsorId)){
      if(!empty($getEventDataForLimitChk)){
        $totalNumberOfRegis   = $getEventDataForLimitChk->number_of_rego; 
        $sideEventLimit     = $this->ci->input->post('limitOfPackage'.$sponsorId);  
      
        if($totalNumberOfRegis < $sideEventLimit){
          $this->ci->form_validation->set_rules('exhibitorlimitchk', 'Limit of package should be less than from event total limit.', 'callback_custom_error_set');
        }
      }
    } 
  }
    
  //------------------------------------------------------------------------------------
  
  /*
   * @access : private
   * @description: This function is used to save sponsor selected registrant data one by one 
   * @param: sponsorId
   * @return void
   */  
  
  private function _sponsorRegistranSave($sponsorId){
    //check registraint selection one by one
    if($this->ci->input->post('sponsorRegistrantId'.$sponsorId)){
      //get side event registration array
      //get side event registration id from multi dimentional array
      $sponsorRegistaintIdMultiArray = $this->ci->input->post('sponsorRegistrantId'.$sponsorId);
      
      foreach($sponsorRegistaintIdMultiArray as $multikey=>$sponsorRegistaintIdArray){
        
        $getRegistantId = $multikey;
        foreach($sponsorRegistaintIdArray as $key=>$value){
          
          if($key ==0){
            $keyset =  $getRegistantId;
            $regisDaywiseId = NULL;
          }else{
            $keyset =  $getRegistantId.'_'.$key;
            $regisDaywiseId = $key;
          }
        
          $registrantId = $keyset; // registaint id
          $sponsorRegistaintId = $value; // registaint id
          $regisSponsorId = $sponsorId.'_'.$registrantId;
            
          //checke selected registraint   
          if($this->ci->input->post('registrantSelectionLimit'.$regisSponsorId)){
            
            $sponsorRegis['registrant_limit'] =  $this->ci->input->post('registrantLimit'.$regisSponsorId);
            
            //set value in array
            $sponsorRegis['sponsor_id']      =  $sponsorId;
            $sponsorRegis['registrant_id']     =  $registrantId;
            $sponsorRegis['registrant_daywise_id']  =  $regisDaywiseId;
            // if side event registraint id is greater then zero then update record
            if($sponsorRegistaintId > 0){
              $whereSideRegis = array('id'=>$sponsorRegistaintId);  
              $this->ci->common_model->updateDataFromTabel('sponsor_registrant',$sponsorRegis,$whereSideRegis);
            }else{
              $insertId = $this->ci->common_model->addDataIntoTabel('sponsor_registrant', $sponsorRegis);
            }
            
          }else{
            //delete data from database
            $whereDeleteCondi = array('sponsor_id'=>$sponsorId, 'registrant_id'=>$registrantId,'registrant_daywise_id'=>$regisDaywiseId);
            $this->ci->common_model->deleteRowFromTabel('sponsor_registrant',$whereDeleteCondi);
          }
        
        }
        
      } 
      
    }
  }
  
  //----------------------------------------------------------------------------------
  
  /**
   * @access : private
   * @description: This function is used to save sponsor data 
   * @param: sponsorId
   * @param: eventId
   * @return void
   * 
  **/  
  
  private function _sponsorDataSave($sponsorId,$eventId){
    
    $updateData['additional_detail'] = $this->ci->input->post('additionalDetails'.$sponsorId);
    $updateData['limit_package'] = $this->ci->input->post('limitOfPackage'.$sponsorId);
    $updateData['total_price'] = $this->ci->input->post('totalPrice'.$sponsorId);
    $updateData['gst'] = $this->ci->input->post('GSTIncluded'.$sponsorId);
    $updateData['additional_booth_price'] = $this->ci->input->post('additionalBoothPrice'.$sponsorId);
    $updateData['additional_booth_gst'] = $this->ci->input->post('additionalBoothGSTIncluded'.$sponsorId);
    //if complimentary checked
    if($this->ci->input->post('complimentaryRegistration'.$sponsorId)){
      $updateData['complementary_registration'] = $this->ci->input->post('complimentaryRegistration'.$sponsorId);
      $updateData['number_complimentary_registrations'] = $this->ci->input->post('complimentaryRegistrationsNum'.$sponsorId);
    }else{
      $updateData['complementary_registration'] = NULL;
      $updateData['number_complimentary_registrations'] = NULL;
    }
    
    //if additional guest is checked
    if($this->ci->input->post('additionalGuests'.$sponsorId)){
      $updateData['is_additional_guests']   = $this->ci->input->post('additionalGuests'.$sponsorId);
      $updateData['limit_per_person']     = $this->ci->input->post('additionalLimitPerPerson'.$sponsorId);
      $updateData['additi_guest_total_price'] = $this->ci->input->post('additionalTotalPrice'.$sponsorId);
      $updateData['additi_guest_gst']     = $this->ci->input->post('additionalGSTIncluded'.$sponsorId);
    }else{
      $updateData['is_additional_guests']   = $this->ci->input->post('additionalGuests'.$sponsorId);
      $updateData['limit_per_person']     = NULL;
      $updateData['additi_guest_total_price'] = NULL;
      $updateData['additi_guest_gst']     = NULL;
    }
    $updateData['can_attend_breakout'] = $this->ci->input->post('attendBreakouts'.$sponsorId);
    $updateData['event_id'] = $eventId;
    $whereSpons = array('id'=>$sponsorId);  
    $this->ci->common_model->updateDataFromTabel('sponsor',$updateData,$whereSpons);
  }
  
  //----------------------------------------------------------------------------------
  
  /**
   * @access : private
   * @description: This function is used to save booth number 
   * @param: sponsorId (int)
   * @param: eventId   (int)
   * @param: boothType  (string)  
   * @return void
   * 
  **/  
  
  private function _boothNumberSave($insertId,$eventId,$boothType){
    
    //check booth selection
    if($this->ci->input->post('boolNumberAvailableHidden'.$insertId)){
    
      //get booth selection array
      $boothNumberArray = $this->ci->input->post('boolNumberAvailableHidden'.$insertId);
      
      foreach($boothNumberArray as $key=>$value){
        
        $boothPosition = $key;
        $boothNumberId = $value;
        $boolNumberAvailableField = $insertId.'_'.$boothPosition;
        
        //checke selected registraint   
        if($this->ci->input->post('boolNumberAvailable'.$boolNumberAvailableField) > 0){
          
          $boothData['event_id']      = $eventId; 
          $boothData['booth_position']  = $boothPosition; 
          if($boothType == 'sponsor')
            $boothData['sponsor_id']  = $insertId;
          else  
            $boothData['exhibitor_id']  = $insertId;
          
          //if booth number id is zero then insert record
          if($boothNumberId==0){  
            //insert data into database
            $this->ci->common_model->addDataIntoTabel('sponsor_exhibitor_booths', $boothData);
          }
          
        }else{
          //delete data from database by condition
          if($boothType == 'sponsor')
            $whereDeleteCondi = array('sponsor_id'=>$insertId, 'booth_position'=>$boothPosition,'event_id'=>$eventId);
          else
            $whereDeleteCondi = array('exhibitor_id'=>$insertId, 'booth_position'=>$boothPosition,'event_id'=>$eventId);
          $this->ci->common_model->deleteRowFromTabel('sponsor_exhibitor_booths',$whereDeleteCondi);
        }
      
      }
    }
  }
  
  //----------------------------------------------------------------------------------
  
  /**
   * @access : private
   * @description: This function is used to save sponsor data 
   * @param: sponsorId
   * @return void
   * 
  **/  
  
  
  private function _sponsorSideeventSave($sponsorId){
    
    if($this->ci->input->post('complimentarySideeventHidden'.$sponsorId)){
      $getPostIdArray  =  $this->ci->input->post('complimentarySideeventHidden'.$sponsorId);
      foreach($getPostIdArray as $key => $value){
        
        //define value in command variable
        $sponsorSideeventId = $value;
        $sideeventId    = $key;
        $sponsorRowKeyId    = $sponsorId.'_'.$sideeventId;
    
        //if value is greater than add into data base
        if($this->ci->input->post('complimentarySideevent'.$sponsorRowKeyId) > 0){
          //set value for insert and update 
          $sideeventData['sponsor_id']     = $sponsorId;
          $sideeventData['side_event_id']    = $sideeventId;
          $sideeventData['side_event_limit']   = $this->ci->input->post('complimentarySideevent'.$sponsorRowKeyId);
          //if greater then zero then update data otherwise add data
          if($sponsorSideeventId > 0){
            $sideeventCondi = array('id'=>$sponsorSideeventId); 
            $this->ci->common_model->updateDataFromTabel('sponsor_side_event',$sideeventData,$sideeventCondi);
          }else{
            $this->ci->common_model->addDataIntoTabel('sponsor_side_event', $sideeventData);
          } 
          
        }else{
          //if number zero then delete
          $whereDeleteCondi = array('side_event_id'=>$sideeventId,'sponsor_id'=>$sponsorId);
          $this->ci->common_model->deleteRowFromTabel('sponsor_side_event',$whereDeleteCondi);
        }
      }
    }
      
  }
  
  //----------------------------------------------------------------------------------
  
  /**
   * @access : private
   * @description: This function is used to save sponsor purchase item data 
   * @param: sponsorId
   * @return void
   * 
  **/  
  
  
  private function _sponsorPurchaseItemSave($sponsorId){
    
    if($this->ci->input->post('additionalItemHidden'.$sponsorId)){
      $getPostIdArray  =  $this->ci->input->post('additionalItemHidden'.$sponsorId);
      foreach($getPostIdArray as $key => $value){
        
        //define value in command variable
        $itemId     = $value;
        $itemRowKeyId   = $sponsorId.'_'.$itemId;
    
        //if value is greater than add into data base
        if($this->ci->input->post('additionalItemField'.$itemRowKeyId)){
          
          //set value for update 
          $updateData['item_status']     = '1';
          $whereCondti = array('id'=>$itemId,'sponsor_id'=>$sponsorId); 
          $this->ci->common_model->updateDataFromTabel('sponsor_benefits',$updateData,$whereCondti);
          
        }else{
          //set value for update 
          $updateData['item_status']     = '0';
          $whereCondti = array('id'=>$itemId,'sponsor_id'=>$sponsorId);
          $this->ci->common_model->updateDataFromTabel('sponsor_benefits',$updateData,$whereCondti);
        }
      }
    }
      
  }
  
  // ------------------------------------------------------------------------   
    
  /*
   * @access : public
   * @description: This function is used for setup exhibitors  
   * @param: eventId
   * @return: void
   * 
   */ 
  
  public function setupexhibitors(){
    
    //current open eventId
    $eventId = currentEventId();
    
    //decode entered eventid
    $eventId = decode($eventId); 
    
    //check is data post 
    if($this->ci->input->post()){
      
        //call insert and update data function
        $this->insertupdateexhibitor();
    }else{
      
      // load registrant form view
      $this->exhibitorsstepsviews($eventId);
    }
  }
  
  // ------------------------------------------------------------------------ 
  
  /*
  * @access : private 
    * @description: This function is used to load exhibitors steps view
  * @param: $eventId
    * @return: void
    *  
    */
  
  private function exhibitorsstepsviews($eventId="0"){
    
    //chheck if eventId is zero 
    if($eventId==0){
      redirect(base_url('dashboard'));
    }
    $userId = isLoginUser();
    $data['userId'] = $userId;
    $data['eventId'] =$eventId;
    $data['popupHeaderBg'] ='exhibitor_bg_header'; // popup header bg class
    $data['popupButtonBg'] ='exhibitor_type_bg_button'; // popup button bg class 
    
    $where = array('event_id'=>$eventId);
    //get exhibitor data
    $data['exhibitordata']      = $this->ci->common_model->getDataFromTabel('exhibitor','*',$where,'','id','ASC');
    //get side event data
    $data['eventsideevent']     = $this->ci->common_model->getDataFromTabel('side_event','*',$where,'','id','ASC');
    //get exhbitor floor plan data
    $data['exhibitorFloorPlan']   = $this->ci->common_model->getDataFromTabel('exhibitor_floor_plan','*',$where,'','id','ASC');
    //get exhbitor floor plan booth used data 
    $data['exhibitorbooths']    = $this->ci->common_model->getDataFromTabel('sponsor_exhibitor_booths','*',$where,'','id','ASC');
    
    $this->ci->template->load('template','exhibitors_steps',$data,TRUE);
  }
  
  // ------------------------------------------------------------------------ 
  
  /* 
   * @access : public
   * @description: This function is used to open add & edit exhibitor popup view
   * @return void
   * 
   */ 
   
  public function addeditexhibitorsview(){
    
    $data['eventId'] = $this->ci->input->post('eventId');
    $exhibitorIdValue = $this->ci->input->post('exhibitorId');
    $data['exhibitorIdValue'] = $exhibitorIdValue;
    $where = array('id'=>$exhibitorIdValue);
    //get data form custome form field table
    $data['exhibitordata'] = $this->ci->common_model->getDataFromTabel('exhibitor','exhibitor_name',$where,'','id','ASC','1');
    if(!empty($data['exhibitordata'])){
      $data['exhibitordata'] = $data['exhibitordata'][0];
    }
  
    $this->ci->load->view('add_edit_exhibitor_popup',$data);
  }
  
  // ------------------------------------------------------------------------ 
  
  /*
   * @access :   public
   * @description: This function is used to add exhibitor save
   * @return void
   * 
   */ 
   
  public function addeditexhibitorsave(){
    
    //set validation
    $this->ci->form_validation->set_rules('exhibitorName', 'exhibitor name', 'trim|required');
    
    if($this->ci->form_validation->run($this->ci)){
      $eventId      = $this->ci->input->post('eventId');
      $exhibitorName    = $this->ci->input->post('exhibitorName');
      $exhibitorIdValue   = $this->ci->input->post('exhibitorId');
      $insertData['exhibitor_name'] = $exhibitorName;
      $insertData['event_id']     = $eventId;
    
      if($exhibitorIdValue > 0){
        $where = array('id'=>$exhibitorIdValue);
        $this->ci->common_model->updateDataFromTabel('exhibitor',$insertData,$where);
        $msg = lang('event_msg_exhibitor_title_updated');
      }else{
        $insertId = $this->ci->common_model->addDataIntoTabel('exhibitor', $insertData);
        $msg = lang('event_msg_exhibitor_add');
      }
      $returnArray=array('msg'=>$msg,'is_success'=>'true'); 
      set_global_messages($returnArray);
      echo json_encode($returnArray);
    }else{
      $errors = $this->ci->form_validation->error_array();
      echo json_message('msg',$errors,'is_success','false');
    }
  }
  
  // ------------------------------------------------------------------------ 
  
  /*
   * @access :   public
   * @description: This function is used to delete exhibitor 
   * @return void
   * 
   */   
  
  public function deleteexhibitor(){
    $deleteid = $this->ci->input->post('deleteId');
    
    //delete registant record from other side event table
    $where = array('id'=>$deleteid);
    $this->ci->common_model->deleteRowFromTabel('exhibitor',$where);
    
    $msg = lang('event_msg_exhibitor_deleted');
    $returnArray=array('msg'=>$msg,'is_success'=>'true');
    set_global_messages($returnArray);
    echo json_encode($returnArray);
  }
  
  // ------------------------------------------------------------------------ 
  
  /*
   * @access :   public
   * @description: This function is used to open add & edit exhibitor additional items
   * items popup view
   * @return void
   * 
   */ 
   
  public function addeditexhibitoritemview(){
    
    $data['exhibitorIdValue']   = $this->ci->input->post('exhibitorId');
    $itemIdValue        = $this->ci->input->post('itemId');
    $data['itemIdValue']    = $itemIdValue;
    $where = array('id'=>$itemIdValue);
    //get data form custome form field table
    $data['additionalitemsdata'] = $this->ci->common_model->getDataFromTabel('exhibitor_item','item',$where,'','id','ASC','1');
    if(!empty($data['additionalitemsdata'])){
      $data['additionalitemsdata'] = $data['additionalitemsdata'][0];
    }
  
    $this->ci->load->view('add_edit_exhibitor_item_popup',$data);
  }
  
  // ------------------------------------------------------------------------ 
  
  /*
   * @access: public
   * @description: This function is used to add exhibitor additional item
   * items popup view
   * @return void
   * 
   */ 
   
  public function addeditexhibitoritemsave(){
    
    //set validation
    $this->ci->form_validation->set_rules('itemName', 'item name', 'trim|required');
    
    if($this->ci->form_validation->run($this->ci)){
      $itemIdValue    = $this->ci->input->post('itemId');
      $itemName       = $this->ci->input->post('itemName');
      $exhibitorId    = $this->ci->input->post('exhibitorId');
      $insertData['item'] = $itemName;
      $insertData['exhibitor_id'] = $exhibitorId;
      
      if($itemIdValue > 0){
        $where = array('id'=>$itemIdValue);
        $this->ci->common_model->updateDataFromTabel('exhibitor_item',$insertData,$where);
        $msg = lang('event_msg_exhibitor_additional_item_updated');
      }else{
        $insertId = $this->ci->common_model->addDataIntoTabel('exhibitor_item', $insertData);
        $msg = lang('event_msg_exhibitor_additional_item_add');
      }
      $returnArray=array('msg'=>$msg,'is_success'=>'true'); 
      set_global_messages($returnArray);
      echo json_encode($returnArray);
    }else{
      $errors = $this->ci->form_validation->error_array();
      echo json_message('msg',$errors,'is_success','false');
    }
  }
  
  // ------------------------------------------------------------------------ 
  
  /*
   * @access: public
   * @description: This function is used to delete exhibitor item 
   * @return void
   * 
   */   
  
  public function deleteexhibitoritem(){
  
    $deleteid = $this->ci->input->post('deleteId');
    
    //delete registant record from other exhibitor items
    $where = array('id'=>$deleteid);
    $this->ci->common_model->deleteRowFromTabel('exhibitor_item',$where);
    
    $msg = lang('event_msg_exhibitor_item_deleted');
    $returnArray=array('msg'=>$msg,'is_success'=>'true');
    set_global_messages($returnArray);
    echo json_encode($returnArray);
  }
  
  // ------------------------------------------------------------------------  
  
  /*
   * @access: private
   * @description: This function is used to insert and update exhibitor forms data 
   * @return void
   * 
   */ 
  
  private function insertupdateexhibitor(){
    
    if($this->ci->input->post()){
      
      //get eventId
      $eventId = $this->ci->input->post('eventId');
      
      //print_r($this->ci->input->post());die();
      
      //get form sponsor id
      $exhibitorId = $this->ci->input->post('exhibitorId');
      
      //call for exhibitor validate
      $this->_exhibitorValidate($exhibitorId,$eventId);
      
      //get ajax request
      $is_ajax_request = $this->ci->input->is_ajax_request();
      
      $exhibitorFloorPlanid = $this->ci->input->post('exhibitorFloorPlanid');
      
      ///check validate
      if ($this->ci->form_validation->run($this->ci))
      {
        
        //call for exhibitor save
        $this->_exhibitorSave($exhibitorId,$eventId);
        
        //call for sponsor booth data save
        $this->_boothNumberSave($exhibitorId,$eventId,'exhibitor');
        
        //call for exhibitor side event save
        $this->_exhibitorSideeventSave($exhibitorId);
        
        //call for exhibitor item save
        $this->_sponsorItemSave($exhibitorId);
        
        //set success message
        $msg = lang('event_msg_exhibitor_details_saved');
        // check ajax  post request
        if($is_ajax_request){
          echo json_message('msg',$msg);
          $msgArray = array('msg'=>$msg,'is_success'=>'true');
          //set_global_messages($msgArray);
        }else{
          $msgArray = array('msg'=>$msg,'is_success'=>'true');
          set_global_messages($msgArray);
          redirect('event/setupsponsors/'.encode($eventId));
        }
        
    
      }else{
        $errors = $this->ci->form_validation->error_array();
        if($is_ajax_request){
          echo json_message('msg',$errors,'is_success','false');
        }else{
          // load event form view
          $this->breakoutstepsviews($eventId);
        }
      }
    }else{
        $msg = lang('event_msg_exhibitor_invalid_data_request');
        $msgArray = array('msg'=>$msg,'is_success'=>'false');
        set_global_messages($msgArray);
        redirect(base_url('dashboard'));
      }
  } 
  
  // ------------------------------------------------------------------------ 
  
  /*
   * @access: private
   * @description: This function is used to validate exhibitor
   * @param: $exhibitorId
   * @return: void
   * 
   */
  
  private function _exhibitorValidate($exhibitorId,$eventId){
    
    //set fixed field validation
    $this->ci->form_validation->set_rules('limitOfPackage'.$exhibitorId, 'limit of package', 'trim|required|numeric');
    $this->ci->form_validation->set_rules('totalPrice'.$exhibitorId, 'total price', 'trim|required|numeric');
    $this->ci->form_validation->set_rules('GSTIncluded'.$exhibitorId, 'GST included', 'trim|required|numeric');
    $this->ci->form_validation->set_rules('additionalDetails'.$exhibitorId, 'additional details', 'trim|required');
    $this->ci->form_validation->set_rules('attendBreakouts'.$exhibitorId, 'attend breakouts', 'trim|required');
    
    $where = array('id'=>$eventId);
    $getEventDataForLimitChk = $this->ci->common_model->getDataFromTabel('event','number_of_rego',$where);
    $getEventDataForLimitChk = $getEventDataForLimitChk[0];
    
    //check entered exhibitor limit with total limit
    if($this->ci->input->post('limitOfPackage'.$exhibitorId)){
      if(!empty($getEventDataForLimitChk)){
        $totalNumberOfRegis   = $getEventDataForLimitChk->number_of_rego; 
        $sideEventLimit     = $this->ci->input->post('limitOfPackage'.$exhibitorId);  
      
        if($totalNumberOfRegis < $sideEventLimit){
          $this->ci->form_validation->set_rules('exhibitorlimitchk', 'Limit of package should be less than from event total limit.', 'callback_custom_error_set');
        }
      }
    }
    
  }
  
  // ------------------------------------------------------------------------ 
  
  /*
   * @acces: private
   * @description: This function is used to save exhibitor
   * @param1: $exhibitorId
   * @param2: $eventId
   * @return: void
   * 
   */
  
  private function _exhibitorSave($exhibitorId,$eventId){
    
    //update data initilize in array
    $updateData['additional_details'] = $this->ci->input->post('additionalDetails'.$exhibitorId);
    $updateData['limit_of_package'] = $this->ci->input->post('limitOfPackage'.$exhibitorId);
    $updateData['total_price'] = $this->ci->input->post('totalPrice'.$exhibitorId);
    $updateData['gst'] = $this->ci->input->post('GSTIncluded'.$exhibitorId);
    $updateData['attend_breakouts'] = $this->ci->input->post('attendBreakouts'.$exhibitorId);
    $updateData['event_id'] = $eventId;
    //condition array
    $whereSpons = array('id'=>$exhibitorId);  
    //udpate data in the data-base
    $this->ci->common_model->updateDataFromTabel('exhibitor',$updateData,$whereSpons);
  }
  
  //----------------------------------------------------------------------------------
  
  /**
   * @access: private
   * @description: This function is used to save exhibitor side event data 
   * @param: sponsorId
   * @return void
   * 
  **/  
  
  private function _exhibitorSideeventSave($exhibitorId){
    
    if($this->ci->input->post('complimentarySideeventHidden'.$exhibitorId)){
      $getPostIdArray  =  $this->ci->input->post('complimentarySideeventHidden'.$exhibitorId);
      foreach($getPostIdArray as $key => $value){
        
        //define value in command variable
        $exhibitorSideeventId = $value;
        $sideeventId    = $key;
        $sponsorRowKeyId    = $exhibitorId.'_'.$sideeventId;
    
        //if value is greater than add into data base
        if($this->ci->input->post('complimentarySideevent'.$sponsorRowKeyId) > 0){
          //set value for insert and update 
          $sideeventData['exhibitor_id']     = $exhibitorId;
          $sideeventData['side_event_id']    = $sideeventId;
          $sideeventData['side_event_limit']   = $this->ci->input->post('complimentarySideevent'.$sponsorRowKeyId);
          //if greater then zero then update data otherwise add data
          if($exhibitorSideeventId > 0){
            $sideeventCondi = array('id'=>$exhibitorSideeventId); 
            $this->ci->common_model->updateDataFromTabel('exhibitor_side_event',$sideeventData,$sideeventCondi);
          }else{
            $this->ci->common_model->addDataIntoTabel('exhibitor_side_event', $sideeventData);
          } 
          
        }else{
          //if number zero then delete
          $whereDeleteCondi = array('side_event_id'=>$sideeventId,'exhibitor_id'=>$exhibitorId);
          $this->ci->common_model->deleteRowFromTabel('exhibitor_side_event',$whereDeleteCondi);
        }
      }
    }
      
  }
  
  //----------------------------------------------------------------------------------
  
  /**
   * @access: private
   * @description: This function is used to save exhibitor item data 
   * @param: exhibitorId
   * @return void
   * 
  **/  
  
  
  private function _sponsorItemSave($exhibitorId){
    
    if($this->ci->input->post('exhibitorItemIdHidden'.$exhibitorId)){
      $getPostIdArray  =  $this->ci->input->post('exhibitorItemIdHidden'.$exhibitorId);
      foreach($getPostIdArray as $key => $value){
      
        //define value in comman variable
        $itemId     = $value;
        $itemRowKeyId   = $exhibitorId.'_'.$itemId;
    
        //set value for update 
        $updateData['item']    = $this->ci->input->post('itemDescription'.$itemRowKeyId);
        $whereCondti = array('id'=>$itemId,'exhibitor_id'=>$exhibitorId); 
        $this->ci->common_model->updateDataFromTabel('exhibitor_item',$updateData,$whereCondti);
      }
    }
  }
  
  // ------------------------------------------------------------------------ 
  
  /*
   * @access: public
   * @description: This function is used for manage payment option  
   * @param: eventId
   * @return: void
   * 
   */ 
  
  public function setuppayment(){
    
    //current open eventId
    $eventId = currentEventId();
    
    //decode entered eventid
    $eventId = decode($eventId); 
    
    //check is data post 
    if($this->ci->input->post()){
      
      //call insert and update data function
      $actionName = $this->ci->input->post('formActionName');
      switch($actionName){
        //save registration fees details data
        case "registrationFees":
          $this->insertupdateregisfees();
        break;
        
        //save bank fees details data
        case "bankFees":
          $this->insertupdatebankfees();
        break;
        
        //save credit card fees details data
        case "creditCardFees":
          $this->insertupdatecreditcardfees();
        break;
        
        //save payment gateway details data
        case "paymentGateway":
          $this->insertupdatepaymentgateway();
        break;
        
        //save payment option setup
        case "paymentOptionSetup":
          $this->insertupdatepaymentoptionsetup();
        break;

        //save cheque fees details data
        case "chequeFees":
          $this->insertupdatechequefees();
        break;

        //save refund fees details data
        case "refundFees":
          $this->insertupdaterefundfees();
        break;
        
        default:
        // load event form view
        $this->paymentoptionviews($eventId);
      }
        
    }else{
      
      // load registrant form view
      $this->paymentoptionviews($eventId);
    }
  }
  
  // ------------------------------------------------------------------------ 
  
  /*
  * @access: private
    * @default load event exhibitors payment option details
    * @description: This function is used to load exhibitors payment option details
    * @return: void
    *  
    */
  
  private function paymentoptionviews($eventId="0"){
    
    //chheck if eventId is zero 
    if($eventId==0){
      redirect(base_url('dashboard'));
    }
    $userId = isLoginUser();
    $data['userId'] = $userId;
    $data['eventId'] =$eventId;
    $data['popupHeaderBg'] ='bg_e31b23'; // popup header bg class
    
    $where = array('event_id'=>$eventId);
    //get event details 
    
    $data['eventdetails']         = $this->ci->event_model->geteventdetails($eventId,$userId);
    $data['registfeedetails']     = $this->ci->common_model->getDataFromTabel('payment_registration_fee','*',$where,'','id','ASC');
    $data['bankfeedetails']       = $this->ci->common_model->getDataFromTabel('payment_bank_fee','*',$where,'','id','ASC');
    $data['creditcarddetails']    = $this->ci->common_model->getDataFromTabel('payment_cc_fee','*',$where,'','id','ASC');
    $data['eventregistrants']     = $this->ci->common_model->getDataFromTabel('event_registrants','*',$where,'','id','ASC');
    $data['paymentoptionsmaster'] = $this->ci->common_model->getDataFromTabel('payment_options_master','*','','','id','ASC');
    $data['paymentoptionsdata']   = $this->ci->common_model->getDataFromTabel('payment_options','*',$where,'','id','ASC');
    $data['chequefeedetails']     = $this->ci->common_model->getDataFromTabel('payment_cheque_fee','*',$where,'','id','ASC');
    $data['refundfeedetails']     = $this->ci->common_model->getDataFromTabel('payment_refund_fee','*',$where,'','id','ASC');
    
    // get all registrant category with its limit
    $data['registrantCategory']  =  $this->ci->event_model->getRegistrantCategory($eventId);
    
    $data['paymentgatewaydata'] = $this->ci->common_model->getDataFromTabel('payment_gateway','*',$where,'','id','ASC');
    $this->ci->template->load('template','paymentoptions/payment_setup',$data,TRUE);
  }
  
  
  // ------------------------------------------------------------------------ 
  
  /*
   * @access: private
   * @description: This function is used to save registration fees details 
   * @return void
   * 
   */ 
  
  private function insertupdateregisfees(){  
    $isSave = 0;
    if ($this->ci->input->post('registrationFee')==1){
      //print_r($this->ci->input->post());
      
      // validate for post data
      $this->ci->form_validation->set_rules('regisTotalPrice', 'registrant total price', 'trim|required|numeric'); 
      
      if($this->ci->form_validation->run($this->ci))
      {
        $isSave = 1;
      } else {     
        $isSave = 0;
      }    
    } else if ($this->ci->input->post('registrationFee')==0) {
      $isSave = 1;
    }
    //echo $isSave;die;
        
    //get ajax request
    $is_ajax_request = $this->ci->input->is_ajax_request();
    //get eventId
    $eventId = $this->ci->input->post('eventId');
    
    if($isSave==1)
    {      
      $paymentRegisId = $this->ci->input->post('paymentRegisId');
      
      // get post data
      $getData['registeration_fee']     = $this->ci->input->post('registrationFee');
      if ($this->ci->input->post('registrationFee')==0) {
        $getData['fee_to_be_paid_by']     = 0;
        $getData['gst']                   = NULL;
        $getData['total_price']           = NULL;
      } else if ($this->ci->input->post('registrationFee')== 1) {
        $getData['fee_to_be_paid_by']     = $this->ci->input->post('whoWillPayFee');
        $getData['gst']           = $this->ci->input->post('regisGSTIncluded');
        $getData['total_price']       = $this->ci->input->post('regisTotalPrice');
      }
      
      
      $getData['event_id']        = $eventId;
      
      if($paymentRegisId > 0){
        $where = array('id'=>$paymentRegisId);
        $this->ci->common_model->updateDataFromTabel('payment_registration_fee',$getData,$where);
      }else{
        $this->ci->common_model->addDataIntoTabel('payment_registration_fee', $getData);
      } 
      
      $msg = lang('event_msg_registration_fees_saved');
      if($is_ajax_request){
        echo json_message('msg',$msg);
        $msgArray = array('msg'=>$msg,'is_success'=>'true');
        //set_global_messages($msgArray);
      }else{
        $msgArray = array('msg'=>$msg,'is_success'=>'true');
        set_global_messages($msgArray);
        redirect('event/setuppayment/'.encode($eventId));
      }
    }
    else
    {
      $errors = $this->ci->form_validation->error_array();
      if($is_ajax_request){
        echo json_message('msg',$errors,'is_success','false');
      }else{
        // load payment option views
        $this->paymentoptionviews($eventId);
      }
    }
  }
  
  // ------------------------------------------------------------------------   
  
  /*
   * @access: private
   * @description: This function is used to save payment gateway 
   * @return void
   * 
   */ 
  
  private function insertupdatepaymentgateway(){
    
    // set ruls for form filed
    $this->ci->form_validation->set_rules('paymentGetwayType', 'payment gateway type', 'trim|required');
    $this->ci->form_validation->set_rules('merchantIdField', 'merchant id', 'trim|required');
    
    //get ajax request
    $is_ajax_request = $this->ci->input->is_ajax_request();
    //get eventId
    $eventId = $this->ci->input->post('eventId');
    
    if ($this->ci->form_validation->run($this->ci))
    {
      $getwayId = $this->ci->input->post('getwayId');
      // get post data
      $getData['merchant_id']       = $this->ci->input->post('merchantIdField');
      $getData['payment_gateway_type']  = $this->ci->input->post('paymentGetwayType');
      $getData['event_id'] = $eventId;
      
      if($getwayId > 0){
        $where = array('id'=>$getwayId);
        $this->ci->common_model->updateDataFromTabel('payment_gateway',$getData,$where);
      }else{
        $this->ci->common_model->addDataIntoTabel('payment_gateway', $getData);
      } 
      
      $msg = lang('event_msg_payment_gateway_detail_saved');
      if($is_ajax_request){
        echo json_message('msg',$msg);
        $msgArray = array('msg'=>$msg,'is_success'=>'true');
        //set_global_messages($msgArray);
      }else{
        $msgArray = array('msg'=>$msg,'is_success'=>'true');
        set_global_messages($msgArray);
        redirect('event/setuppayment/'.encode($eventId));
      }
    }
    else
    {
      $errors = $this->ci->form_validation->error_array();
      if($is_ajax_request){
        echo json_message('msg',$errors,'is_success','false');
      }else{
        // load payment option views
        $this->paymentoptionviews($eventId);
      }
    }
  }
  
  
  // ------------------------------------------------------------------------ 
  
  /*
   * @access: private
   * @description: This function is used to save bank fees details 
   * @return void
   * 
   */ 
  
  private function insertupdatebankfees(){  
    $isSave = 0;
    if ($this->ci->input->post('registrationFee')==1){
      //print_r($this->ci->input->post());
    
      // validate for post data
      $this->ci->form_validation->set_rules('regisTotalPrice', 'transection total price', 'trim|required|numeric');
 
      
      if($this->ci->form_validation->run($this->ci))
      {
        $isSave = 1;
      } else {     
        $isSave = 0;
      }    
    } else if ($this->ci->input->post('registrationFee')==0) {
      $isSave = 1;
    }
    //echo $isSave;die;
        
    //get ajax request
    $is_ajax_request = $this->ci->input->is_ajax_request();
    //get eventId
    $eventId = $this->ci->input->post('eventId');
  
        
    if($isSave==1)
    {
      
      $paymentBankId = $this->ci->input->post('paymentBankId');
      
      // get post data
      $getData['event_id']        = $eventId;
      $getData['registeration_fee']     = $this->ci->input->post('registrationFee');
      if ($this->ci->input->post('registrationFee')==0) {
        $getData['fee_to_be_paid_by']     = 0;
        $getData['gst']                   = NULL;
        $getData['total_price']           = NULL;
      } else if ($this->ci->input->post('registrationFee')== 1) {
        $getData['fee_to_be_paid_by']     = $this->ci->input->post('whoWillPayFee');
        $getData['gst']           = $this->ci->input->post('regisGSTIncluded');
        $getData['total_price']       = $this->ci->input->post('regisTotalPrice');
      }
      
      
      if($paymentBankId > 0){
        $where = array('id'=>$paymentBankId);
        $this->ci->common_model->updateDataFromTabel('payment_bank_fee',$getData,$where);
      }else{
        $this->ci->common_model->addDataIntoTabel('payment_bank_fee', $getData);
      } 
      
      $msg = lang('event_msg_bank_fees_saved');
      if($is_ajax_request){
        echo json_message('msg',$msg);
        $msgArray = array('msg'=>$msg,'is_success'=>'true');
        //set_global_messages($msgArray);
      }else{
        $msgArray = array('msg'=>$msg,'is_success'=>'true');
        set_global_messages($msgArray);
        redirect('event/setuppayment/'.encode($eventId));
      }
    }
    else
    {
      $errors = $this->ci->form_validation->error_array();
      if($is_ajax_request){
        echo json_message('msg',$errors,'is_success','false');
      }else{
        // load payment option views
        $this->paymentoptionviews($eventId);
      }
    }
  }

  // ------------------------------------------------------------------------ 
  
  /*
   * @access: private
   * @description: This function is used to save cheque fees details 
   * @return void
   * 
   */ 
  
  private function insertupdatechequefees(){ 
    $isSave = 0;
    if ($this->ci->input->post('paymentChequeFee')==1){
      //print_r($this->ci->input->post());
      
      // validate for post data
      $this->ci->form_validation->set_rules('regisTotalPrice', 'check processing total price', 'trim|required|numeric'); 
      
      if($this->ci->form_validation->run($this->ci))
      {
        $isSave = 1;
      } else {     
        $isSave = 0;
      }    
    } else if ($this->ci->input->post('paymentChequeFee')==0) {
      $isSave = 1;
    }
    //echo $isSave;die
    
    
    
    //get ajax request
    $is_ajax_request = $this->ci->input->is_ajax_request();
    //get eventId
    $eventId = $this->ci->input->post('eventId');
    
    if($isSave==1)
    {
      
      $paymentChequeId = $this->ci->input->post('paymentChequeId');         
      
      // get post data
      $getData['event_id']        = $eventId;
      $getData['cheque_processing_fee']     = $this->ci->input->post('paymentChequeFee');
      if ($this->ci->input->post('paymentChequeFee')==0) {
        $getData['fee_to_be_paid_by'] = 0;
        $getData['total_price']       = NULL;
        $getData['gst']               = NULL;
      } else if ($this->ci->input->post('paymentChequeFee')== 1) {
        $getData['fee_to_be_paid_by']     = $this->ci->input->post('whoWillPayFee');
        $getData['total_price']       = $this->ci->input->post('regisTotalPrice');
        $getData['gst']           = $this->ci->input->post('regisGSTIncluded');
      }
            
      if($paymentChequeId > 0){
        $where = array('id'=>$paymentChequeId);
        $this->ci->common_model->updateDataFromTabel('payment_cheque_fee',$getData,$where);
      }else{
        $this->ci->common_model->addDataIntoTabel('payment_cheque_fee', $getData);
      } 
      
      $msg = lang('event_msg_cheque_fees_saved');
      if($is_ajax_request){
        echo json_message('msg',$msg);
        $msgArray = array('msg'=>$msg,'is_success'=>'true');
        //set_global_messages($msgArray);
      }else{
        $msgArray = array('msg'=>$msg,'is_success'=>'true');
        set_global_messages($msgArray);
        redirect('event/setuppayment/'.encode($eventId));
      }
    } else  {
      $errors = $this->ci->form_validation->error_array();
      if($is_ajax_request){
        echo json_message('msg',$errors,'is_success','false');
      }else{
        // load payment option views
        $this->paymentoptionviews($eventId);
      }
    }
  }

  // ------------------------------------------------------------------------ 
  
  /*
   * @access: private
   * @description: This function is used to save refund fees details 
   * @return void
   * 
   */ 
  
  private function insertupdaterefundfees(){
    $isSave = 0;
    if ($this->ci->input->post('refundFee')==1){
      //print_r($this->ci->input->post());
      
      // validate for post data
      $this->ci->form_validation->set_rules('regisTotalPrice', 'refund processing total price', 'trim|required|numeric'); 
      
      if($this->ci->form_validation->run($this->ci))
      {
        $isSave = 1;
      } else {     
        $isSave = 0;
      }    
    } else if ($this->ci->input->post('refundFee')==0) {
      $isSave = 1;
    }
    //echo $isSave;die;
    
    //get ajax request
    $is_ajax_request = $this->ci->input->is_ajax_request();
    //get eventId
    $eventId = $this->ci->input->post('eventId');
    
    if($isSave==1)
    {      
      $paymentRefundId = $this->ci->input->post('paymentRefundId');
      
      // get post data
      $getData['event_id']        = $eventId;
      $getData['refund_fee']     = $this->ci->input->post('refundFee');
      if ($this->ci->input->post('refundFee')==0) {
        $getData['fee_to_be_paid_by']     = 0;
        $getData['gst']                   = NULL;
        $getData['total_price']           = NULL;
      } else if ($this->ci->input->post('refundFee')== 1) {
        $getData['fee_to_be_paid_by']     = $this->ci->input->post('whoWillPayFee');
        $getData['gst']           = $this->ci->input->post('regisGSTIncluded');
        $getData['total_price']       = $this->ci->input->post('regisTotalPrice');
      }
      
      
      if($paymentRefundId > 0){
        $where = array('id'=>$paymentRefundId);
        $this->ci->common_model->updateDataFromTabel('payment_refund_fee',$getData,$where);
      }else{
        $this->ci->common_model->addDataIntoTabel('payment_refund_fee', $getData);
      } 
      
      $msg = lang('event_msg_refund_fees_saved');
      if($is_ajax_request){
        //echo json_message('msg',$msg);
        $returnArray = array('msg'=>$msg,'is_success'=>'true','url'=>$this->ci->uri->uri_string());
        //set_global_messages($msgArray);
        echo json_encode($returnArray);
      }else{
        $msgArray = array('msg'=>$msg,'is_success'=>'true');
        set_global_messages($msgArray);
        redirect('event/setuppayment/'.encode($eventId));
      }
    }
    else
    {
      $errors = $this->ci->form_validation->error_array();
      if($is_ajax_request){
        echo json_message('msg',$errors,'is_success','false');
      }else{
        // load payment option views
        $this->paymentoptionviews($eventId);
      }
    }
  }
  
  // ------------------------------------------------------------------------   
  
  /*
   * @access: private
   * @description: This function is used to save credit card fees details 
   * @return void
   * 
   */ 
  
  private function insertupdatecreditcardfees(){
    
    // set ruls for form filed
    $this->ci->form_validation->set_rules('whoWillPayFee', 'who Will Pay Fee credit card', 'trim|required');
    
    //get ajax request
    $is_ajax_request = $this->ci->input->is_ajax_request();
    //get eventId
    $eventId = $this->ci->input->post('eventId');
    
    if ($this->ci->form_validation->run($this->ci))
    {
      
      $creditCardId = $this->ci->input->post('creditCardId');
      
      // get post data
      $getData['who_will_pay_fee'] = $this->ci->input->post('whoWillPayFee');
      //check if registraint is selected
      if($this->ci->input->post('acceptedCreditCards')){
        $typeOfRegistrant = json_encode($this->ci->input->post('acceptedCreditCards'),JSON_FORCE_OBJECT);
      }else{
        $typeOfRegistrant = NULL;
      }
      $getData['accepted_credit_cards'] = $typeOfRegistrant;      
      $getData['event_id'] = $eventId;
      
      if($creditCardId > 0){
        $where = array('id'=>$creditCardId);
        $this->ci->common_model->updateDataFromTabel('payment_cc_fee',$getData,$where);
      }else{
        $this->ci->common_model->addDataIntoTabel('payment_cc_fee', $getData);
      } 
      
      $msg = lang('event_msg_credit_card_fees_details_saved');
      if($is_ajax_request){
        echo json_message('msg',$msg);
        $msgArray = array('msg'=>$msg,'is_success'=>'true');
        //set_global_messages($msgArray);
      }else{
        $msgArray = array('msg'=>$msg,'is_success'=>'true');
        set_global_messages($msgArray);
        redirect('event/setuppayment/'.encode($eventId));
      }
    }
    else
    {
      $errors = $this->ci->form_validation->error_array();
      if($is_ajax_request){
        echo json_message('msg',$errors,'is_success','false');
      }else{
        // load payment option views
        $this->paymentoptionviews($eventId);
      }
    }
  } 
  
  // ------------------------------------------------------------------------ 
  
  /*
   * @access: private
   * @description: This function is used to save Setup Payment Options details 
   * @return void
   * 
   */ 
  private function insertupdatepaymentoptionsetup(){
      //echo '<pre>';print_r($this->ci->input->post);die;
      $eventId            =   $this->ci->input->post('eventId');
      $selectedPaymentOptionArray   =   array();
      $selectedPaymentOption      =   $this->ci->input->post('selectedPaymentOption');
      if(!empty( $selectedPaymentOption)){  
        $selectedPaymentOptionArray = explode(",",$selectedPaymentOption);
      }

      
      //set default validation
      $this->ci->form_validation->set_rules('eventId', 'eventid', 'trim|required');
      
    if(!empty($selectedPaymentOptionArray)){
      foreach($selectedPaymentOptionArray as $optionValue){
        
        $rowKeyId = $optionValue;
        $rowFieldName = strtolower($this->ci->input->post('paymentModeNameHidden'.$rowKeyId));
        
        //set validation for paymentInstruction credit card not allow payment instruction
        if($rowKeyId!='1'){
          $this->ci->form_validation->set_rules('paymentInstruction'.$rowKeyId, $rowFieldName.' payment instruction ', 'trim|required');
        }
      }
    }
    else
    {   
      $this->ci->form_validation->set_rules('paymentOpotionSelection', 'Plese enter atleast one payment option.', 'callback_custom_error_set');
    }
    
    //get ajax request
    $is_ajax_request = $this->ci->input->is_ajax_request();
    
    if ($this->ci->form_validation->run($this->ci))
    {
      
      //set daynamic field value payment option
      if($selectedPaymentOptionArray){
        $IdHiddenArray  =   $this->ci->input->post('paymentModeIdField');
        
        foreach($selectedPaymentOptionArray as $getOptionValue){
        
          $paymentModeKey = $getOptionValue-1;
          $paymentOptionValue = $IdHiddenArray[$paymentModeKey];
          $rowKeyId = $getOptionValue;
          //set value for paymentInstruction
          if($this->ci->input->post('paymentInstruction'.$rowKeyId)){
            //set value for update 
            $getData['payment_instruction']     = $this->ci->input->post('paymentInstruction'.$rowKeyId);
          }
          
          //check if registraint is selected
          if($this->ci->input->post('regisSlection'.$rowKeyId)){
            $typeOfRegistrant = json_encode($this->ci->input->post('regisSlection'.$rowKeyId),JSON_FORCE_OBJECT);
          }else{
            $typeOfRegistrant = NULL;
          }
          
          $getData['registrant_id']     = $typeOfRegistrant;
          $getData['payment_mode_master_id']    = $this->ci->input->post('paymentModeIdHidden'.$rowKeyId);
          $getData['payment_mode']    = $this->ci->input->post('paymentModeNameHidden'.$rowKeyId);
          
          $getData['event_id']  = $eventId;
          
          if($paymentOptionValue > 0){
            $where = array('id'=>$paymentOptionValue);
            $this->ci->common_model->updateDataFromTabel('payment_options',$getData,$where);
          }else{
            $this->ci->common_model->addDataIntoTabel('payment_options', $getData);
          }
          
        }
      }
      
      $msg = lang('event_msg_payment_options_saved');
      if($is_ajax_request){
        echo json_message('msg',$msg);
        $msgArray = array('msg'=>$msg,'is_success'=>'true');
        //set_global_messages($msgArray);
      }else{
        $msgArray = array('msg'=>$msg,'is_success'=>'true');
        set_global_messages($msgArray);
        redirect('event/setuppayment/'.encode($eventId));
      }
    }
    else
    {
      $errors = $this->ci->form_validation->error_array();
      if($is_ajax_request){
        echo json_message('msg',$errors,'is_success','false');
      }else{
        // load payment option views
        $this->paymentoptionviews($eventId);
      }
    }
  } 
  
  // ------------------------------------------------------------------------ 
  
  /*
   * @access:  public
   * @description: This function is used to open add payment option popup view
   * @return void
   * 
   */ 
   
  public function addeditpaymentoptionview(){
    $data['eventId'] = $this->ci->input->post('eventId');
    $paymentModeId     = $this->ci->input->post('paymentModeId');
    $formAction      = $this->ci->input->post('formAction');
    $data['paymentModeId'] = $paymentModeId;
    $where = array('id'=>$paymentModeId);
    //get data form custome form field table
    $data['paymentOptiondata'] = $this->ci->common_model->getDataFromTabel('payment_options','payment_mode',$where,'','id','ASC','1');
    if(!empty($data['paymentOptiondata'])){
      $data['paymentOptiondata'] = $data['paymentOptiondata'][0];
    }
    //set popup header
    switch($formAction){
      case "add":
        $headerAction = lang('event_payment_add_payment_option');
      break;
      case "edit":
        $headerAction = lang('event_payment_edit_payment_option');
      break;  
    }
    $data['headerAction']    = $headerAction; 
    $data['formActionValue'] = $formAction; 

    $this->ci->load->view('paymentoptions/add_alternative_payment_option',$data);
  }
  
  // ------------------------------------------------------------------------ 
  
   /*
    * @access:  public
  * @description: This function is used to open add edit payment option save
  * @return void
  * 
  */ 
    
  public function addeditpaymentoptionsave(){
    
    //set validation
    $this->ci->form_validation->set_rules('paymentOptionModeName', 'payment option mode name', 'trim|required');      
    if($this->ci->form_validation->run($this->ci)){
      $eventId = $this->ci->input->post('eventId');
      $paymentModeId           = $this->ci->input->post('paymentModeId');
      $paymentOptionModeName = $this->ci->input->post('paymentOptionModeName');
      $insertData['payment_mode'] = $paymentOptionModeName;
      $insertData['event_id'] = $eventId;
      $insertData['payment_mode_master_id'] = '0';
      if($paymentModeId > 0){
        $where = array('id'=>$paymentModeId);
        $this->ci->common_model->updateDataFromTabel('payment_options',$insertData,$where);
        $msg = lang('event_msg_alternative_payment_option_updated_successfully');
      }else{
        $insertId = $this->ci->common_model->addDataIntoTabel('payment_options', $insertData);
        $msg = lang('event_msg_alternative_payment_option_add_successfully');
      }
      $returnArray=array('msg'=>$msg,'is_success'=>'true'); 
      //set_global_messages($returnArray);
      echo json_encode($returnArray); 
    }else{    
      $errors = $this->ci->form_validation->error_array();
      echo json_message('msg',$errors,'is_success','false');
    }
  }


  // ------------------------------------------------------------------------ 
  /*
  * @access:  public
  * @description: This function is used to delete payment option
  * @return void
  * 
  */
  function deletepaymentoption(){
    $paymentModeId = $this->ci->input->post('deleteId');
    $where = array('id'=>$paymentModeId);
    
    $this->ci->common_model->deleteRowFromTabel('payment_options',$where);
    $msg = lang('event_msg_payment_option_delete_successfully');
    echo json_message('msg', $msg);
  }
  
  // ------------------------------------------------------------------------ 
  
  /*
   * @access: public
   * @description: This function is used for customize forms
   * @param: eventId
   * @return: void
   * 
   */ 
  
  public function customizeforms(){
    //current open eventId
    $eventId = currentEventId();
    
    //decode entered eventid
    $eventId = decode($eventId); 
    
    //check is data post 
    if($this->ci->input->post()){
      
        //call insert and update data function
        $this->insertupdatecolortheme();
    }else{
      
      // load registrant form view
      $this->customizeformsviews($eventId);
    }
  }
  
  // ------------------------------------------------------------------------ 
  
  /*
  *
  * @access: public
    * @description: This function is used to load customize forms details
    * @return: void
    *  
    */
  
  public function customizeformsviews($eventId="0"){
    
    //chheck if eventId is zero 
    if($eventId==0){
      redirect(base_url('dashboard'));
    }
    
    $userId = isLoginUser();
    $data['userId'] = $userId;
    $data['eventId'] =$eventId;
    $data['popupHeaderBg'] ='bg_807f83'; // popup header bg class
    
    $where = array('id'=>$eventId);
    $whereEvent = array('event_id'=>$eventId);
    $data['eventsponsorslogos'] = $this->ci->common_model->getDataFromTabel('event_sponsors_logos','*',$whereEvent);
    $data['eventdata'] = $this->ci->common_model->getDataFromTabel('event','event_title',$where,'','id','ASC');
    $data['customizeformsdata'] = $this->ci->common_model->getDataFromTabel('customize_forms', '*', $whereEvent, '', 'id', 'ASC');
    $data['previousthemes'] = $this->ci->event_model->getpreviouscolourthemes($eventId, $userId);
    $this->ci->template->load('template','customizeforms/customize_forms',$data,TRUE);
  }
  
  
  // ------------------------------------------------------------------------ 
  
   public function themeMediaFree() {
       
        $saveStatus = false;
        $eventId = $this->ci->input->post('eventId'); 
        $logo_position = $this->ci->input->post('logo_position');
        $old_logo_position = $this->ci->input->post('old_logo_position');
        if(!empty($logo_position)){
			$updateData['logo_position'] = $this->ci->input->post('logo_position');
			if($logo_position != $old_logo_position){
				$saveStatus = true;
			}
		}
        
        
        $logo_gallery_image = $this->ci->input->post('logo_gallery_image');
        $gallery_action = $this->ci->input->post('gallery_action');
        if(!empty($gallery_action) && !empty($logo_gallery_image)) {            
            $updateData['logo_image'] = $logo_gallery_image;
            $updateData['logo_image_path'] = '/media/gallery/';
            $dirSavePath = '/media/gallery/';
            $fileName = $logo_gallery_image;
            $saveStatus = true;
        }
        
        $header_gallery_image = $this->ci->input->post('header_gallery_image');
        //$gallery_action = $this->ci->input->post('gallery_action');
        if(!empty($gallery_action) && !empty($header_gallery_image)) {            
            $eventId = $this->ci->input->post('eventId'); 
            $updateData['header_image'] = $header_gallery_image;
            $updateData['header_image_path'] = '/media/gallery/';
            $dirSavePath = '/media/gallery/';
            $fileName = $header_gallery_image;
            $saveStatus = true;
        }
      
        if(!empty($_FILES['headerImage']['tmp_name']) || !empty($_FILES['logoImage']['tmp_name'])) {
            $actionPost = $this->ci->input->post('action_post');  
            $eventId = $this->ci->input->post('eventId');  

            if($actionPost=='logo'){
              $dirUploadPath = './media/event_logo/';
              $dirSavePath = '/media/event_logo/';
            }else{
              $dirUploadPath = './media/event_header/';
              $dirSavePath = '/media/event_header/';
            } 
              
            $config['upload_path'] = $dirUploadPath;
            $config['allowed_types'] = 'gif|jpg|png';
            //$config['max_size']	= '100';
            //$config['max_width']  = '1024';
            //$config['max_height']  = '768';
            $this->ci->load->library('upload', $config);
            if ($actionPost=='header' && ! $this->ci->upload->do_upload('headerImage'))
            {
                echo $error = array('error' => $this->ci->upload->display_errors());
            }else if($actionPost=='logo' && ! $this->ci->upload->do_upload('logoImage')){
				echo $error = array('error' => $this->ci->upload->display_errors());
				}
            else{
                $uploadData = $this->ci->upload->data();   
                $fileName = $uploadData['file_name'];        
            }
            //print_r($uploadData);
            if($actionPost=='logo'){
                $updateData['logo_image'] = $fileName;
                $updateData['logo_image_path'] = $dirSavePath;
				$updateData['logo_position'] = $this->ci->input->post('logo_position'); 
            }else{
                $updateData['header_image'] = $fileName;
                $updateData['header_image_path'] = $dirSavePath;
            }
            $saveStatus = true;
        }
            
         if($saveStatus) {   
            // Common save & update code for theme header/ logo / sponsor images //
            $fileId = remove_extension($fileName);
            //get event id
            $updateData['event_id'] = $eventId;
            // get document id by event id
            $where = array('event_id'=>$eventId);
            $getResult = $this->ci->common_model->getDataFromTabel('customize_forms','id',$where);

            if(!empty($getResult)){
            // update data id by id
            $headerId = $getResult[0]->id;
            $upWhere = array('id'=>$headerId);
            $this->ci->common_model->updateDataFromTabel('customize_forms',$updateData,$upWhere);
            $saveStatus = true;
            }else{
            // insert data id by id
            $headerId = $this->ci->common_model->addDataIntoTabel('customize_forms', $updateData);
            $saveStatus = true;
            } 
            echo json_encode(array('id'=>$headerId,'fileId'=>$fileId,'filename'=>$fileName,'imagepath'=>$dirSavePath));      
        }else {
            echo 'null';
        }      
       
  }
  
  
  
  /*
  * @access: public
  * @description: This function is used to add event header
  * @return void
  * 
  */ 

  
  public function customizeformsmedia(){
    
    $actionPost = $_REQUEST['action_post'];
    
    if($actionPost=='logo'){
      $dirUploadPath = './media/event_logo/';
      $dirSavePath = '/media/event_logo/';
      $uploadData = $this->ci->process_upload->upload_file($dirUploadPath);
      $eventId = $_REQUEST['event_id'];
      $updateData['logo_image'] = $uploadData['filename'];
      $updateData['logo_image_path'] = $dirSavePath;
    }else{
      $dirUploadPath = './media/event_header/';
      $dirSavePath = '/media/event_header/';
      $uploadData = $this->ci->process_upload->upload_file($dirUploadPath);
      $eventId = $_REQUEST['event_id'];
      $updateData['header_image'] = $uploadData['filename'];
      $updateData['header_image_path'] = $dirSavePath;
    } 
    
    $updateData['event_id'] = $eventId;
    $fileId = remove_extension($uploadData['filename']);
    // get document id by event id
    $where = array('event_id'=>$eventId);
    $getResult = $this->ci->common_model->getDataFromTabel('customize_forms','id',$where);
    
    if(!empty($getResult)){
      // update data id by id
      $headerId = $getResult[0]->id;
      $upWhere = array('id'=>$headerId);
      $this->ci->common_model->updateDataFromTabel('customize_forms',$updateData,$upWhere);
    }
    else{
      // insert data id by id
      $headerId = $this->ci->common_model->addDataIntoTabel('customize_forms', $updateData);
    } 
    echo json_encode(array('id'=>$headerId,'fileId'=>$fileId));
  }
  
  // ------------------------------------------------------------------------ 
  
  /*
   * @access: public
   * @description: This function is used to delete event logo
   * @return void
   */ 
   
  public function deletecustomizeformslogo(){    
    $floorPlanId = $this->ci->input->post('deleteId');
    $updateData['logo_image'] = '';
    $updateData['logo_image_path'] = '';
    $where = array('id'=>$floorPlanId);
    
    //check record is exist 
    $getResult = $this->ci->common_model->getDataFromTabel('customize_forms','logo_image,logo_image_path',$where);
    if(!empty($getResult)){
      //delte image from directory
      findFileNDelete($getResult[0]->logo_image_path, $getResult[0]->logo_image);
    }
    $this->ci->common_model->updateDataFromTabel('customize_forms',$updateData,$where);
    echo json_message('msg','Event logo deleted.');
  } 
  
  // ------------------------------------------------------------------------ 
  
  /*
   * @access: public
   * @description: This function is used to delete event header
   * @return void
   */ 
   
  public function deletecustomizeformsheader(){
    
    $floorPlanId = $this->ci->input->post('deleteId');
    $updateData['header_image'] = '';
    $updateData['header_image_path'] = '';
    $where = array('id'=>$floorPlanId);
    
    //check record is exist 
    $getResult = $this->ci->common_model->getDataFromTabel('customize_forms','header_image,header_image_path',$where);
    if(!empty($getResult)){
      //delte image from directory
      findFileNDelete($getResult[0]->header_image_path, $getResult[0]->header_image);
    }
    $this->ci->common_model->updateDataFromTabel('customize_forms',$updateData,$where);
    echo json_message('msg','Event header deleted.');
  } 
  
  // ------------------------------------------------------------------------ 
  
  /*
   * @access: private
   * @description: This function is used to insert update color theme
   * @return void
   */ 
   
  private function insertupdatecolortheme() {
    //set validation for insertupdatecolortheme
    $this->ci->form_validation->set_rules('colour_scheme', 'colour scheme', 'trim|required');
    $this->ci->form_validation->set_rules('background_colour', 'background colour', 'trim|required');
    $this->ci->form_validation->set_rules('main_colour', 'main colour', 'trim|required');
    $this->ci->form_validation->set_rules('highlight_colour', 'highlight colour', 'trim|required');
    $this->ci->form_validation->set_rules('font_package', 'font package', 'trim|required');
    $this->ci->form_validation->set_rules('link_colour', 'link colour', 'trim|required');

    //get ajax request
    $is_ajax_request = $this->ci->input->is_ajax_request();
    //get eventId
    $eventId = $this->ci->input->post('eventId');
    
    if ($this->ci->form_validation->run($this->ci))
    {      
      $getData['colour_scheme'] = $this->ci->input->post('colour_scheme');
      $getData['background_colour'] = $this->ci->input->post('background_colour');
      $getData['main_colour'] = $this->ci->input->post('main_colour');
      $getData['highlight_colour'] = $this->ci->input->post('highlight_colour');
      $getData['font_package'] = $this->ci->input->post('font_package');
      $getData['link_color'] = $this->ci->input->post('link_colour');

      $getData['event_id'] = $eventId;

      // get document id by event id
      $where = array('event_id'=>$eventId);
      $getResult = $this->ci->common_model->getDataFromTabel('customize_forms','id',$where);
      
      if(!empty($getResult)){
        // update data id by id
        $headerId = $getResult[0]->id;
        $upWhere = array('id'=>$headerId);
        $this->ci->common_model->updateDataFromTabel('customize_forms',$getData,$upWhere);
      }
      else{
        // insert data id by id
        $headerId = $this->ci->common_model->addDataIntoTabel('customize_forms', $getData);
      } 
      
      $msg = lang('event_msg_colour_theme_saved');
      if($is_ajax_request){
        //echo json_message('msg',$msg);
        $returnArray=array('msg'=>$msg,'is_success'=>'true','url'=>$this->ci->uri->uri_string()); 
        echo json_encode($returnArray);
        //set_global_messages($msgArray);
      }else{
        $msgArray = array('msg'=>$msg,'is_success'=>'true');
        set_global_messages($msgArray);
        redirect('event/customizeforms/'.encode($eventId));
      }
    }
    else
    {
      $errors = $this->ci->form_validation->error_array();
      if($is_ajax_request){
        echo json_message('msg',$errors,'is_success','false');
      }else{
        // load customize form views
        $this->customizeformsviews($eventId);
      }
    }
  } 
  
  // ------------------------------------------------------------------------ 
  
  /*
   * @access: public
   * @description: This function is used for customize forms
   * @param: eventId
   * @return: void
   * 
   */ 
  
  public function confirmdetails()
  {
    //current open eventId
    $eventId = currentEventId();
    
    //decode entered eventid
    $eventId = decode($eventId); 
    
    // load registrant form view
    $this->confirmdetailsviews($eventId);
    
  }
  
  // ------------------------------------------------------------------------ 
  
  /*
  * @access:  private
    * @description: This function is used to load customize forms details
    * @return: void
    *  
    */
  
  private function confirmdetailsviews($eventId="0"){
    
    //chheck if eventId is zero 
    if($eventId==0){
      redirect(base_url('dashboard'));
    }
    
    $userId = isLoginUser();
    $data['userId'] = $userId;
    $data['eventId'] =$eventId;
    $data['popupHeaderBg'] ='bg_807f83'; // popup header bg class
    
    $where = array('id'=>$eventId);
    $whereEvent = array('event_id'=>$eventId);
    $regisWhere = array('event_id'=>$eventId);
    $data['eventdata'] = $this->ci->common_model->getDataFromTabel('event','*',$where,'','id','ASC');
    $data['eventregistrants'] = $this->ci->common_model->getDataFromTabel('event_registrants','*',$regisWhere,'','registrants_order','ASC');
    $this->ci->template->load('template','confirmdetails/confirm_details',$data,TRUE);
  }
  
  // ------------------------------------------------------------------------   
  
  /*
   * @access: public
   * @description: This function is used to get state list by master country id
   * @return master state listing html
   */
  
  public function masterstatelist(){
    $countryId  = $this->ci->input->post('countryId');
    $where  = array('country_id'=>$countryId);
    $stateData  = $this->ci->common_model->getDataFromTabel('master_state','*',$where,'','state_name','ASC');
    $stateHtml  = '<option value="">Select State</option>';
    
    if(!empty($stateData)){
      foreach($stateData as $statelistdata){
        $stateId  = $statelistdata->state_id;
        $stateName  = $statelistdata->state_name;
        $stateHtml  .= '<option value="'.$stateId.'">'.$stateName.'</option>';
      }
    }
    echo $stateHtml; 
  }
  
  // ------------------------------------------------------------------------   
  
  /*
   * @access: public
   * @description: This function is used to get city list by master state id
   * @return master city listing html
   */
   
  public function mastercitylist(){
    $stateId  = $this->ci->input->post('stateId');
    $where  = array('state_id'=>$stateId);
    $cityData   = $this->ci->common_model->getDataFromTabel('master_city','*',$where,'','city_name','ASC');
    $cityHtml  = '<option value="">Select City</option>';
    
    if(!empty($cityData)){
      foreach($cityData as $citylistdata){
        $cityId   = $citylistdata->city_id;
        $cityName  = $citylistdata->city_name;
        $cityHtml  .= '<option value="'.$cityId.'">'.$cityName.'</option>';
      }
    }
    echo $cityHtml; 
  } 
  
  // ------------------------------------------------------------------------   
  
  /*
   * @access: public
   * @description: This function is used to get state list by country id
   * @return state listing html
   */
  
  public function statelist(){
    $countryId  = $this->ci->input->post('countryId');
    $where    = array('country_id'=>$countryId);
    $stateData  = $this->ci->common_model->getDataFromTabel('state','*',$where,'','state_name','ASC');
    $stateHtml  = '<option value="">Select State</option>';
    
    if(!empty($stateData)){
      foreach($stateData as $statelistdata){
        $stateId  = $statelistdata->id;
        $stateName  = $statelistdata->state_name;
        $stateHtml  .= '<option value="'.$stateId.'">'.$stateName.'</option>';
      }
    }
    echo $stateHtml; 
  }
  
  // ------------------------------------------------------------------------   
  
  /*
   * @access: public
   * @description: This function is used to get city list by state id
   * @return city listing html
   */
   
  public function citylist(){
    $stateId  = $this->ci->input->post('stateId');
    $where  = array('state_id'=>$stateId);
    $cityData   = $this->ci->common_model->getDataFromTabel('city','*',$where,'','city_name','ASC');
    $cityHtml  = '<option value="">Select City</option>';
    
    if(!empty($cityData)){
      foreach($cityData as $citylistdata){
        $cityId   = $citylistdata->id;
        $cityName  = $citylistdata->city_name;
        $cityHtml  .= '<option value="'.$cityId.'">'.$cityName.'</option>';
      }
    }
    echo $cityHtml; 
  } 
  
  // ------------------------------------------------------------------------   
  
  /*
   * @access: public
   * @description: This function is used to get company name list by state id and city id
   * @return company name listing html
   */
   
  public function companynamelist(){
    $stateId  = $this->ci->input->post('stateId');
    $cityId   = $this->ci->input->post('cityId');
    $companyNameData  = $this->ci->event_model->getcompanylist($stateId,$cityId);
    $companyNameHtml  = '<option value="">Select Company Name</option>';
    
    if(!empty($companyNameData)){
      foreach($companyNameData as $companyNamelistdata){
        $companyNameId    = $companyNamelistdata->id;
        $companyNameName    = $companyNamelistdata->company_name.', '.$companyNamelistdata->city_name;
        $companyNameHtml   .= '<option value="'.$companyNameId.'">'.$companyNameName.'</option>';
      }
    }
    echo $companyNameHtml; 
  } 
  
  // ------------------------------------------------------------------------ 
  
   /*
    * @access:  public
  * @description: This function is used to publish and unpublish 
  * @return void
  * 
  */ 
  
  
  public function confirmstatus(){
    $statusValue = $this->ci->input->post('statusValue');
    $eventId   = $this->ci->input->post('eventId');
    $updateData = array('is_approved'=>$statusValue); 
    $upWhere = array('id'=>$eventId);
    $this->ci->common_model->updateDataFromTabel('event',$updateData,$upWhere);
    if($statusValue=='1'){
      $msg = lang('event_conf_status_pub_msg');
    }else{
      $msg = lang('event_conf_status_unpub_msg');
    } 
    $msgArray = array('msg'=>$msg,'is_success'=>'true');
    //set_global_messages($msgArray);
    echo json_message('msg',$msg);
  } 
  
  // ------------------------------------------------------------------------ 
  
   /*
  * @description: This function is used to publish and unpublish 
  * @return void
  * 
  */ 
  
  public function confirmmsg(){
    $fieldMsgArray = array();
    $eventId = $this->ci->input->post('eventId');
    $userId = isLoginUser();
    $regisWhere = array('event_id'=>$eventId);
    $eventdetails = $this->ci->event_model->geteventdetails($eventId,$userId);
    if(!empty($eventdetails)){
      if(empty($eventdetails->number_of_rego)){
        $fieldMsgArray['general_event_setup'] = 'General Event Setup';
      }
      if(empty($eventdetails->first_name)){
        $fieldMsgArray['event_contact_person'] = 'Event Contact Person';
      }
      if(empty($eventdetails->event_header)){
        $fieldMsgArray['event_invoice_details'] = 'Event Invoice Details';
      }
      if(empty($eventdetails->account_name)){
        $fieldMsgArray['reconciled_funds'] = 'Reconciled Funds';
      }
      if(empty($eventdetails->term_conditions) && empty($eventdetails->document_file)){
        $fieldMsgArray['terms_and_condition'] = 'Terms and Conditions';
      }
    }
    
    //registrant message listing
    $registrantName = '';
    $eventregistrants = $this->ci->common_model->getDataFromTabel('event_registrants','*',$regisWhere,'','registrants_order','ASC');
    if(!empty($eventregistrants)){
      foreach($eventregistrants as $registrants){
        if(empty($registrants->total_price)){
          $registrantName[] = $registrants->registrant_name.' registration';
        }
      }
      if(!empty($registrantName)){
        $registrantName = implode(', ',$registrantName);
        $fieldMsgArray['regis_name'] = $registrantName;
      }
    }
    
    //side event message listing
    $sideeventName = '';
    $where = array('event_id'=>$eventId);
    $sideeventdata = $this->ci->common_model->getDataFromTabel('side_event','*',$where,'','id','ASC');
    if(!empty($sideeventdata)){
      foreach($sideeventdata as $sideevent){
        if(empty($sideevent->side_event_limit)){
          $sideeventName[] = $sideevent->side_event_name.' side event';
        }
      }
      if(!empty($sideeventName)){
        $sideeventName = implode(', ',$sideeventName);
        $fieldMsgArray['side_event_name'] = $sideeventName;
      }
    }
    
    //breakout message listing
    $breakoutsName = '';
    $where = array('event_id'=>$eventId);
    $breakoutdata = $this->ci->common_model->getDataFromTabel('breakouts','*',$where,'','id','ASC');
    if(!empty($breakoutdata)){
      foreach($breakoutdata as $breakout){
        if(empty($breakout->number_of_streams) && empty($breakout->number_of_session)){
          $breakoutsName[] = $breakout->breakout_name.' breakout';
        }
      }
      if(!empty($breakoutsName)){
        $breakoutsName = implode(', ',$breakoutsName);
        $fieldMsgArray['breakout_error'] = $breakoutsName;
      }
    }
    
    if(!empty($fieldMsgArray)){
      $data['error_msg'] = $fieldMsgArray;    
      $this->ci->load->view('confirmdetails/confirm_details_publish_message',$data);
    }else{
      echo 'done';
    }
  }
  
  
  
  // ------------------------------------------------------------------------  
   
  /*
   * @access: public
   * @description: This function is used to upload  sponsors logo
   * @param : upload path
   * @return void
   * 
   */ 
   
  public function uploadtermattachmentrefund(){
    $dirUploadPath            = './media/event_attachment/';
    $dirSavePath            = '/media/event_attachment/';
    $uploadData             = $this->ci->process_upload->upload_file($dirUploadPath);
    $eventId              = $_REQUEST['event_id'];
    $updateData['refund_document_file'] = $uploadData['filename'];
    $updateData['refund_document_path'] = $dirSavePath;
    $fileId               = remove_extension($uploadData['filename']);
    // get document id by event id
    $where = array('event_id'=>$eventId);
    $getResult = $this->ci->common_model->getDataFromTabel('event_term_conditions','id',$where);
    // update document id by id
    $termId = $getResult[0]->id;
    $upWhere = array('id'=>$termId);
    $this->ci->common_model->updateDataFromTabel('event_term_conditions',$updateData,$upWhere);
    echo json_encode(array('id'=>$termId,'fileId'=>$fileId));
  }
  
  
  // ------------------------------------------------------------------------ 
   
  /*
   * (Still this method are not using)
   * @access: public
   * @description: This function is used to delete term and codition document
   * @return void
   */ 
   
  public function deletetermattachmentrefund(){
    
    $termId               = $this->ci->input->post('deleteId');
    $updateData['refund_document_file'] = '';
    $updateData['refund_document_path'] = '';
    $where                = array('id'=>$termId);
    
    //check record is exist 
    $getResult = $this->ci->common_model->getDataFromTabel('event_term_conditions','refund_document_file,refund_document_path',$where);
    if(!empty($getResult)){
      //delte image from directory
      findFileNDelete($getResult[0]->refund_document_path, $getResult[0]->refund_document_file);
    }
    
    $this->ci->common_model->updateDataFromTabel('event_term_conditions',$updateData,$where);
    echo json_message('msg','Refund policy document deleted.');
  }
  
  // ------------------------------------------------------------------------  
    
  // ------------------------------------------------------------------------ 
  
   /*
    * @access:   public
  * @description: This function is used to update registrant order 
  * @param:  registrantsid
  * @return void
  * 
  */ 
  
  public function regitrantorderupdate(){
    if($this->ci->input->post('registrantsid')){
      $registrantsidarray = $this->ci->input->post('registrantsid');
      foreach($registrantsidarray as $key => $value){
        $updateData = array('registrants_order'=>$key); 
        $upWhere = array('id'=>$value);
        $this->ci->common_model->updateDataFromTabel('event_registrants',$updateData,$upWhere);
      }
      $msg    = lang('event_msg_registrant_order_changed');
      $msgArray   = array('msg'=>$msg,'is_success'=>'true');
      set_global_messages($msgArray);
      echo json_message('msg',$msg);
    }
  }
  
  /*
   * @access: public
   * @description: This function is used to upload sponsors logo
   * @param : upload path
   * @return void
   * 
   */ 
   
  public function uploadsponsorslogo(){
      
    $eventId        = $_REQUEST['event_id'];  
    $dirUploadPath      = './media/events_sponsors_logo/'.base64_encode($eventId).'/';
    $dirSavePath      = '/media/events_sponsors_logo/'.base64_encode($eventId).'/';
    $uploadData       = $this->ci->process_upload->upload_file($dirUploadPath);
    $date['event_id']     = $eventId;
    $date['logo_file_name'] = $uploadData['filename'];
    $date['logo_file_path'] = $dirSavePath;
    $fileId         = remove_extension($uploadData['filename']);
    $insertId         = $this->ci->common_model->addDataIntoTabel('event_sponsors_logos', $date);
    echo json_encode(array('id'=>$insertId,'fileId'=>$fileId));
  }
  
    
  // ------------------------------------------------------------------------ 
  
  /*
   * @access: public
   * @description: This function is used to delete uploaded sponsors logo
   * @param:  deleteId
   * @return void
   * 
   */ 
   
  
  public function deletesponsorslogo(){
    $deleteid = $this->ci->input->post('deleteId');
    $where    = array('logo_id'=>$deleteid);
    
    //check record is exist 
    $getResult = $this->ci->common_model->getDataFromTabel('event_sponsors_logos','logo_file_path,logo_file_name',$where);
    if(!empty($getResult)){
      //delte image from directory
      findFileNDelete($getResult[0]->logo_file_path, $getResult[0]->logo_file_name);
    }
    
    $this->ci->common_model->deleteRowFromTabel('event_sponsors_logos',$where);
    echo json_message('msg','Sponsors logo deleted.');
  }
  
  
  
 // ------------------------------------------------------------------------
  /*
   * @access: public
   * @description: This function is used to setup extras 
   * @param: eventId
   * @return: void
   * 
   */   
  public function setupextras(){    
    //current open eventId
    $eventId = currentEventId();

    //decode entered eventid
    $eventId = decode($eventId); 

    //check is data post 
    if($this->ci->input->post()){      
        //call insert and update data function side event
        $this->insertupdateextras();
    }else{
      // load registrant form view
      $this->extrassetupview($eventId);
    }
  }
  
  // ------------------------------------------------------------------------ 
  
  /*
   * @access: private
   * @description: This function is used to insert and update registrant types forms data 
   * @return void
   * 
   */ 
  
  private function insertupdateextras(){
    if($this->ci->input->post()){
      
      //get form registrant id
      $extraId = $this->ci->input->post('extraId');
    
      //get eventId
      $eventId   = $this->ci->input->post('eventId');
      
      //call function for sideevent validate
      $this->_eventExtraValidate($extraId,$eventId);
      
      //call function for validate registrant
      $this->_eventExtraRegistrantValidate($extraId);
      
      //get ajax request
      $is_ajax_request = $this->ci->input->is_ajax_request();
      
      if ($this->ci->form_validation->run($this->ci))
      {
        //call function for save side event 
        $this->_eventExtraSave($extraId,$eventId);
      
        //call function for save side event registrant
        $this->_eventExtraRegistrantSave($extraId);
          
        // check ajax request
        $msg = lang('event_msg_extra_setup_saved');
        
        if($is_ajax_request){
          //echo json_message('msg',$msg);
          $msgArray = array('msg'=>$msg,'is_success'=>'true','url'=>'event/setupextras');
          echo json_encode($msgArray);
        }else{
          $msgArray = array('msg'=>$msg,'is_success'=>'true');
          redirect('event/setupextras/'.encode($eventId));
        }          
      }else{
        $errors = $this->ci->form_validation->error_array();
        if($is_ajax_request){
          echo json_message('msg',$errors,'is_success','false');
        }else{
          // load event form view
          $this->extrassetupview($eventId);
        }
      }      
    }else{
      $msg    = lang('event_msg_invalid_data_request');
      $msgArray = array('msg'=>$msg,'is_success'=>'false');
      redirect(base_url('dashboard'));
    }
  }
 
  
  // ------------------------------------------------------------------------ 
  
  /*
   * @access: private
   * @description: This function is used to validate event extra item  
   * @param1: $extraid (int)
   * @param2: $eventId (int)
   * @return: void
   * 
   */
   
  private function _eventExtraValidate($extraId,$eventId){        
    $eventExtraLimit = $this->ci->input->post('eventExtraLimit'.$extraId);
    if($eventExtraLimit){
      //set all form validation
      $this->ci->form_validation->set_rules('eventExtraLimit'.$extraId, 'extra item limit', 'trim|required|numeric'); 
    } 
    
    $eventExtraCommonPrice = $this->ci->input->post('eventExtraCommonPrice'.$extraId);
    if($eventExtraCommonPrice){
      $this->ci->form_validation->set_rules('common_total_price'.$extraId, 'common total price', 'trim|required|numeric'); 
      $this->ci->form_validation->set_rules('common_gst_included'.$extraId, 'common price gst included', 'trim|required|numeric'); 
    }   
  }

  
  // ------------------------------------------------------------------------ 
  
  /*
   * @access:    private
   * @description: This function is used to validate event extra item registrant 
   * @param1:    $extraid (int)
   * @return:    void
   * 
   */
   
  private function _eventExtraRegistrantValidate($extraId){
    if($this->ci->input->post('registrantcheckbox'.$extraId)){      
      $eventExtraMultiArray = $this->ci->input->post('registrantcheckbox'.$extraId);
      
      $reg_limitSum = 0;
      foreach($eventExtraMultiArray as $registrantId=>$value){
          $appendId = $extraId.'_'.$registrantId;
          $this->ci->form_validation->set_rules('reg_limit'.$appendId, 'registrant limit', 'trim|required|numeric');
          
          $reg_limitSum += $this->ci->input->post('reg_limit'.$appendId);
          
          if(!$this->ci->input->post('complementary'.$appendId)){
            $this->ci->form_validation->set_rules('total_price_comp'.$appendId, 'complementary total price', 'trim|required|numeric');
            $this->ci->form_validation->set_rules('gst_included_total_price_comp'.$appendId, 'complementary total price gst included', 'trim|required|numeric');                      
          }          
           
      }
      
      $eventExtraLimit     = $this->ci->input->post('eventExtraLimit'.$extraId);
      if($reg_limitSum>$eventExtraLimit){
        $this->ci->form_validation->set_rules('regLimitchk', 'all registrant limit should be less than from total limit.', 'callback_custom_error_set');
      }
      
    } 
  }
  
  
  
  // ------------------------------------------------------------------------
  /*
   * @access:    private
   * @description: This function is used to save event extra item
   * @param1:    $sideEventId (int)
   * @param2:    $eventId (int)
   * @return:    void
   * 
   */
   
  private function _eventExtraSave($extraId,$eventId){                   
    $updateData['extra_limit']    = $this->ci->input->post('eventExtraLimit'.$extraId);
    $updateData['additional_details']    = $this->ci->input->post('sideEventAddDetail'.$extraId);    
    $updateData['restrict_purchase_access'] = ($this->ci->input->post('restrictPurchaseAccess'.$extraId))?$this->ci->input->post('restrictPurchaseAccess'.$extraId):'0';
    
    $updateData['common_price'] = ($this->ci->input->post('eventExtraCommonPrice'.$extraId))?$this->ci->input->post('eventExtraCommonPrice'.$extraId):'0';
    if($this->ci->input->post('eventExtraCommonPrice'.$extraId)==1){
      $updateData['common_total_price'] = $this->ci->input->post('common_total_price'.$extraId);
      $updateData['common_gst_included'] = $this->ci->input->post('common_gst_included'.$extraId);
    } else {
      $updateData['common_total_price']   = NULL;
      $updateData['common_gst_included']  = NULL;
    }
    
    
    $whereRegis = array('id'=>$extraId);  
    $this->ci->common_model->updateDataFromTabel('event_extras',$updateData,$whereRegis);
    
    if($this->ci->input->post('restrictPurchaseAccess'.$extraId)==0){
      //delete registant record from other side event registrant table
      $whereRegis = array('extra_id'=>$extraId);
      $this->ci->common_model->deleteRowFromTabel('event_extra_registrant',$whereRegis);
    }
    
    $filedId = $this->ci->input->post('fieldid');
    if(is_array($filedId)){
      foreach($filedId as $value){
        $update[]     = array('id'=>$value,'field_status'=>$this->ci->input->post('field_'.$value)); 
      }
      $result = $this->ci->event_model->updateeventExtraCustomFileds($update,array('extra_id'=>$extraId));  
    }
  }
  
  // ------------------------------------------------------------------------ 
  
  /*
   * @access:    private
   * @description: This function is used to save event extra item registrant 
   * @param1:    $extraid (int)
   * @return:    void
   * 
   */
   
  private function _eventExtraRegistrantSave($extraId){
    if($this->ci->input->post('registrantcheckbox'.$extraId)){
      $eventExtraRegistrantArray = array();
      $eventExtraMultiArray = $this->ci->input->post('registrantcheckbox'.$extraId);
      foreach($eventExtraMultiArray as $registrantId=>$value){  
          $eventExtraRegistrantArray[] = $registrantId;      
          $appendId = $extraId.'_'.$registrantId;          
          $eventExtraRegis['registrant_id'] = $registrantId;
          $eventExtraRegis['extra_id'] = $extraId;
          $eventExtraRegistaintId = $this->ci->input->post('eventExtraRegistaintId'.$appendId);
          $eventExtraRegis['reg_limit'] = $this->ci->input->post('reg_limit'.$appendId);
          if($this->ci->input->post('complementary'.$appendId)){
            $eventExtraRegis['complementary'] = '1';
            $eventExtraRegis['total_price'] = NULL;
            $eventExtraRegis['gst_included'] = NULL;                 
          } else { 
            $eventExtraRegis['complementary'] = '0';
            $eventExtraRegis['total_price'] = $this->ci->input->post('total_price_comp'.$appendId);
            $eventExtraRegis['gst_included'] = $this->ci->input->post('gst_included_total_price_comp'.$appendId);   
          }
          
          if($eventExtraRegistaintId > 0){
              $whereEventExtraRegis = array('id'=>$eventExtraRegistaintId);  
              $this->ci->common_model->updateDataFromTabel('event_extra_registrant',$eventExtraRegis,$whereEventExtraRegis);
         }else{
              $insertId = $this->ci->common_model->addDataIntoTabel('event_extra_registrant', $eventExtraRegis);
         }       
      }

      //This will get all registrant for extra id saved in db
      $where = array('extra_id' => $extraId);                    
      $eventextraRegistrantData = getDataFromTabel('event_extra_registrant', 'registrant_id', $where, '', 'id', 'ASC');
      if(!empty($eventextraRegistrantData)){
        $eventExtraRegistrantArrayDB = array();
        foreach($eventextraRegistrantData as $key=>$eventextraRegistrant){
          $eventExtraRegistrantArrayDB[] = $eventextraRegistrant->registrant_id; 
        }
      }
      
      //This will get all registrant that is in db but unchecked by user and than delete from table
      $delArray = array_diff($eventExtraRegistrantArrayDB, $eventExtraRegistrantArray);
      foreach($delArray as $key=>$regId){
        $delWhere = array('extra_id' => $extraId, 'registrant_id' => $regId);                    
        $this->ci->common_model->deleteRowFromTabel('event_extra_registrant',$delWhere);
      }
    } 
    else{
      //This will get all registrant that is in db and than delete from table
      $where = array('extra_id' => $extraId);                    
      $eventextraRegistrantData = getDataFromTabel('event_extra_registrant', '*', $where);
      if(!empty($eventextraRegistrantData)){
        $this->ci->common_model->deleteRowFromTabel('event_extra_registrant',$where);
      }
    } 

  }



// ------------------------------------------------------------------------ 
  
  /*
  * @access: public
    * @description: This function is used to load event extra view 
    * @param: $eventId
    * @return: void
    *  
    */
  
  private function extrassetupview($eventId="0"){    
    //chheck if eventId is zero 
    if($eventId==0){
      redirect(base_url('dashboard'));
    }
    
    $userId           = isLoginUser();
    $data['userId']       = $userId;
    $data['eventId']      = $eventId;
    $data['popupHeaderBg']    = 'sideevent_bg_header';//set popup header background color
    $data['popupButtonBg']    = 'sideevent_type_bg_button'; // popup button bg class 
    $data['eventdetails']     = $this->ci->event_model->geteventdetails($eventId,$userId);
    $data['eventregistrants']   = $this->ci->event_model->getRegistrantList($eventId,'1'); // get registrant list
    
    // get all registrant category with its limit
    $data['registrantCategory']  =  $this->ci->event_model->getRegistrantCategory($eventId); 
    
    //if empty then redirect
    if(empty($data['eventdetails'])){
      redirect(base_url('dashboard'));
    }
    
    $where = array('event_id'=>$eventId);
    $registrantWhere = array('event_id'=>$eventId,'registrants_limit !=' => '');
    $data['event_extra_data'] = $this->ci->common_model->getDataFromTabel('event_extras','*',$where,'','id','ASC');
    $this->ci->template->load('template','extras/extras_setup',$data,TRUE);
  }
  
  // ------------------------------------------------------------------------ 
  
  /*
   * @access: public
   * @description: This function is used to open add & edit extra popup view
   * @return void
   * 
   */ 
   
  public function extrapopup(){    
    $data['eventId']   = $this->ci->input->post('eventId');
    $extraid     = $this->ci->input->post('extraid');
    $formAction      = $this->ci->input->post('formAction');
    $data['extraid'] = $extraid;
    $where = array('id'=>$extraid);
    
    //get data form custome form field table
    $data['event_extra_data'] = $this->ci->common_model->getDataFromTabel('event_extras','extra_name',$where,'','id','ASC','1');
    if(!empty($data['event_extra_data'])){
      $data['event_extra_data'] = $data['event_extra_data'][0];
    }
    
    //set popup header
    switch($formAction){
      case "add":
        $headerAction = lang('event_extras_popup_add_title');
      break;
      case "edit":
        $headerAction = lang('event_extras_popup_edit_title');
      break;  
      case "duplicate":
        $headerAction = lang('event_extras_popup_duplicate_title');
      break;    
    }
    $data['headerAction']    = $headerAction; 
    $data['formActionValue'] = $formAction; 

    $this->ci->load->view('extras/add_edit_extra_popup',$data);
  }
  
  // ------------------------------------------------------------------------ 
  
  /*
   * @access:  public
   * @description: This function is used to add type of registration 
   * @return void
   * 
   */ 
   
  public function addeditextrasave(){        
    $formAction = $this->ci->input->post('formAction');
    //set validation
    $this->ci->form_validation->set_rules('extraName', 'extra item name', 'trim|required'); 
    
    if($this->ci->form_validation->run($this->ci)){      
      switch($formAction){      
        case "add":
        case "edit":
          $msg = $this->_addedit_extra_item_save(); //add edit side event
        break;
        
        case "duplicate":
          $msg = $this->_duplicate_extra_item_save(); //duplicate side event
        break;
      }      
      $returnArray=array('msg'=>$msg,'is_success'=>'true','url'=>'event/setupextras');
      echo json_encode($returnArray);
    }else{
      $errors = $this->ci->form_validation->error_array();
      echo json_message('msg',$errors,'is_success','false');
    }
  }
  
  // ------------------------------------------------------------------------
  /*
   * @access: public
   * @description: This function is used to delete extra item
   * @return void
   * 
   */ 
    public function deleteextraitem(){
      $deleteid = $this->ci->input->post('deleteId');
      //delete registant record from other side event registrant table
      $whereRegis = array('extra_id'=>$deleteid);
      $this->ci->common_model->deleteRowFromTabel('event_extra_registrant',$whereRegis);

      //delete registant record from other side event table
      $where = array('id'=>$deleteid);
      $this->ci->common_model->deleteRowFromTabel('event_extras',$where);

      $msg = lang('event_msg_extra_delete_successfully');
      $returnArray=array('msg'=>$msg,'is_success'=>'true');
      echo json_encode($returnArray);
    }
  
  
  
    // ------------------------------------------------------------------------ 
  
  /*
   * @access: private
   * @description: This function is used to add and edit extra item 
   * @return message (string)
   * 
   */   
  
  private function _addedit_extra_item_save(){
    $eventId                = $this->ci->input->post('eventId');
    $extraId                = $this->ci->input->post('extraId');
    $extraName             =   $this->ci->input->post('extraName');
    $insertData['event_id']      =   $eventId;
    $insertData['extra_name']      =   $extraName;

    if($extraId > 0){
      //udpate extra item name
      $where = array('id'=>$extraId);
      $this->ci->common_model->updateDataFromTabel('event_extras',$insertData,$where);
      $msg =  lang('event_msg_extra_edit_successfully');
    }else{
      //extra item add
      $extraId = $this->ci->common_model->addDataIntoTabel('event_extras', $insertData);
      $msg = lang('event_msg_extra_add_successfully');
    }
    return $msg; 
  }
  
  // ------------------------------------------------------------------------ 
  
  /*
   * @access: private
   * @description: This function is used to duplicate extra item
   * @return void
   * 
   */   
  
  private function _duplicate_extra_item_save(){
    $eventId            = $this->ci->input->post('eventId');
    $extraId        = $this->ci->input->post('extraId');
    $extraName      = $this->ci->input->post('extraName');
    
    $where    = array('id'=>$extraId);
    $whereCondi = array('extra_id'=>$extraId);
    //get data form side event table
    $eventExtraData      = $this->ci->common_model->getDataFromTabel('event_extras','*',$where,'','id','ASC','1');
    $eventExtraRegisdata   = $this->ci->common_model->getDataFromTabel('event_extra_registrant','*',$whereCondi,'','id','ASC');
    //echo 'Extra Data<pre>';print_r($eventExtraData);
    //echo 'Extra Regis Data<pre>';print_r($eventExtraRegisdata);die;
    
    
    //insert data into event_extras table
    if(!empty($eventExtraData)){      
      $eventExtraData = $eventExtraData[0];      
      $insertData['extra_name']           = $extraName;
      $insertData['event_id']             = $eventId;
      $insertData['additional_details']   = $eventExtraData->additional_details;
      $insertData['extra_limit']   = '0';
      $insertData['common_price']         = $eventExtraData->common_price;
      $insertData['restrict_purchase_access'] = $eventExtraData->restrict_purchase_access;      
      $insertId = $this->ci->common_model->addDataIntoTabel('event_extras', $insertData);
      $restrict_purchase_access = $eventExtraData->restrict_purchase_access;
    }
    
    //insert data into event_extra_registrant table
    if(!empty($eventExtraRegisdata)){
      if($restrict_purchase_access!=0){
        foreach($eventExtraRegisdata as $eventExtraRegis){        
          $insertDataRegis['registrant_id']       = $eventExtraRegis->registrant_id;          
          $insertDataRegis['extra_id']            = $insertId;
          $insertDataRegis['reg_limit']           = 0;
          $insertDataRegis['complementary']       = $eventExtraRegis->complementary;
          $insertDataRegis['total_price']         = $eventExtraRegis->total_price;
          $insertDataRegis['gst_included']        = $eventExtraRegis->gst_included;
          $this->ci->common_model->addDataIntoTabel('event_extra_registrant', $insertDataRegis);
          unset($insertDataRegis);
        } 
      }     
    }
    
    //set duplicate message
    $msg = lang('event_msg_extra_duplicate_successfully');    
    return $msg; 
  }  
  
  /*
   * @access:  public
   * @description: This function is used to add and edit custom field for event extra item
   * @return void
   * 
   */ 
  
  public function eventextrascustomfield(){
    $data['extraId']      = $this->ci->input->post('extraId');
    $customfieldid        = $this->ci->input->post('customfieldid');
    $data['formId']         = $this->ci->input->post('formId');
    $data['formAction']       = $this->ci->input->post('formAction');
    $data['customfieldidval'] = $customfieldid;
    $where = array('id'=>$customfieldid);
    //get data form custome form field table
    $data['extrafields'] = $this->ci->common_model->getDataFromTabel('event_extras_custom_fields','*',$where,'','id','ASC','1');
    if(!empty($data['extrafields'])){
      $data['extrafields'] = $data['extrafields'][0];
    }

    $this->ci->load->view('extras/custom_field_popup',$data);  
    
  }
  
  // ------------------------------------------------------------------------ 
  
  /*
   * @access:    public
   * @description: This function is used to edit and update custom field for event extra item
   * @return void
   * 
   */ 
   
  public function addeditextrascustomfieldsave(){

    //set validation
    $this->ci->form_validation->set_rules('fieldName', 'field name', 'trim|required');
  
    if ($this->ci->form_validation->run($this->ci)){
      $extraId     = $this->ci->input->post('extraId');
      $fieldName   = $this->ci->input->post('fieldName');      
      $customFieldId    = $this->ci->input->post('customFieldId');
      $fieldType      = $this->ci->input->post('fieldType');
      $defaultValue   = NULL;
      if($this->ci->input->post('defaultValue') && ($fieldType=="selectbox" || $fieldType=="radio")){
        $defaultValue = json_encode($this->ci->input->post('defaultValue'),JSON_FORCE_OBJECT);
      }
      //this value commmon for add and edit case
      $insertData['field_name']     = strtolower($fieldName);
      $insertData['field_type']     = $fieldType;
      $insertData['default_value']  = $defaultValue;
      $insertData['extra_id']     = $extraId;      
      
      // udpate custom field data
      if($customFieldId > 0){
        $where = array('id'=>$customFieldId);
        $this->ci->common_model->updateDataFromTabel('event_extras_custom_fields',$insertData,$where);
        $msg = lang('event_msg_extra_custom_field_updated');
        $returnId = $customFieldId;
        $saveType = 'update';
      }else{
        //this value set only create field time
        $insertData['field_status'] = '1';
        $insertData['is_editable'] = '1';
        $insertData['field_create_by'] = '0';
        // data custom field data
        $insertId = $this->ci->common_model->addDataIntoTabel('event_extras_custom_fields', $insertData);
        $msg = lang('event_msg_extra_custom_field_add');
        $returnId = $insertId;
        $saveType = 'insert';
      }
       
      $fieldsmastervalue = fieldsmastervalue();      
      $returnArray=array('msg'=>$msg,'is_success'=>'true','url'=>$this->ci->uri->uri_string(),'id'=>$returnId,'fieldsmastervalue'=>$fieldsmastervalue,'saveType'=>$saveType); 
      //set_global_messages($returnArray);
      echo json_encode($returnArray);
    }else{
      $errors = $this->ci->form_validation->error_array();
      echo json_message('msg',$errors,'is_success','false');
    }
  } 
  
  // ------------------------------------------------------------------------ 
  
  /*
  * @access:    public
  * @description: This function is used to delete custom field for event extra item
  * @return void
  * 
  */
  public function deleteextrascustomfield(){
    $deleteid = $this->ci->input->post('deleteId');
    if(!empty($deleteid)){
      //delete registant record from other table
      $where = array('id'=>$deleteid);
      $this->ci->common_model->deleteRowFromTabel('event_extras_custom_fields',$where);
      $msg = lang('event_msg_extra_custom_field_deleted');
      $returnArray=array('msg'=>$msg,'is_success'=>'true');
      //set_global_messages($returnArray);
      //echo json_encode($returnArray);
      echo json_message('msg',lang('event_msg_extra_custom_field_deleted'),'is_success','true');
    }else{
     echo json_message('msg',lang('comm_error'),'is_success','false');
    }

  }
  
  
  
  // ------------------------------------------------------------------------   
    
  /*
   * @access: private
   * @description: This function is used to validate socail media details
   * @return void
   * 
   */ 
  
  private function _eventSocialMediaValidate(){  
    // set rules for form fields 
    return true;   
  }
  
  
  // ------------------------------------------------------------------------  
  /*
   * @access: private
   * @description: This function is used to validate & insert and update socail media details 
   * @return void
   * 
   */ 
  
  private function insertupdateeventSocialMedia(){
    
    //call function for validate
    $this->_eventSocialMediaValidate();
    
    //get ajax request
    $is_ajax_request = $this->ci->input->is_ajax_request();
    //get eventId
    $eventId = $this->ci->input->post('eventId');
    
    /*if($this->ci->form_validation->run($this->ci))
    {*/
        //call function for save 
        $msg = $this->_eventSocialMediaSave($eventId);
      
        //check ajax post request 
        if($is_ajax_request){
          echo json_message('msg',$msg);
        }else{
          $msgArray = array('msg'=>$msg,'is_success'=>'true');
          set_global_messages($msgArray);
          redirect('event/eventdetails/'.encode($eventId));
        }
    /*}else{
      
      $errors = $this->ci->form_validation->error_array();
      if($is_ajax_request){
        echo json_message('msg',$errors,'is_success','false');
      }else{
        // load event form view
        $this->eventshowview($eventId);
      }
    } */
  }
  
  // ------------------------------------------------------------------------ 
  /*
   * @access: private
   * @description: This function is used to save socail media details
   * @return void
   * 
   */ 
  
  private function _eventSocialMediaSave($eventId){
    // difine data in array 
    $getData['event_id']       = $this->ci->input->post('eventId');
    $getData['website']          = $this->ci->input->post('eventWebsite');
    $getData['facebook']          = $this->ci->input->post('eventFacebook');
    $getData['instagram']          = $this->ci->input->post('eventInstagram');
    $getData['twitter']          = $this->ci->input->post('eventTwitter');
    $getData['youtube']          = $this->ci->input->post('eventYoutube');
    $getData['pintrest']          = $this->ci->input->post('eventPintrest');
    

    // check contact person details by event id
    $where = array('event_id'=>$eventId);
    $result = $this->ci->common_model->getDataFromTabel('event_social_media','id',$where);
    
    /*
    $arrayVal     =  $this->ci->input->post('fieldid');
    //update field value
    $arrayFieldVal  =  $this->ci->input->post('mediatypevalue');
     
    //update field status one by one
    if(!empty($arrayVal)){
      foreach($arrayVal as $key=>$value){
        $field_status = $arrayFieldVal[$key];
        $updateData['media_value'] = $field_status;
        $where = array('id'=>$value);
        $this->ci->common_model->updateDataFromTabel('event_social_media_custom',$updateData,$where);
      }
    }*/
    
    //update here
    if($result){      
      // get event contact persion id
      $customContactId = $result[0]->id;      
      $this->ci->common_model->updateDataFromTabel('event_social_media',$getData,$where);
      $msg = lang('event_msg_contact_person_details_saved');
    }else{
      //insert here
      $customContactId = $this->ci->common_model->addDataIntoTabel('event_social_media', $getData);
      $msg = lang('event_msg_contact_person_details_saved');
    }
        
    return $msg;
  }
  
  // ------------------------------------------------------------------------ 

  /* @access:    public
  * @description: This function is used to open popup of custom contact type
  * @return     : @void
  */ 
  public function eventcustomsocialmediapopup(){ 
    $data['eventId']      = $this->ci->input->post('eventId');
    $customfieldid        = $this->ci->input->post('customfieldid');
    $data['customfieldidval'] = $customfieldid;

    $where            = array('id'=>$customfieldid);
    //get data form custome form field table
    $data['eventcustomSocialMedia'] = $this->ci->common_model->getDataFromTabel('event_social_media_custom','*',$where,'','id','ASC','1');
    if(!empty($data['eventcustomSocialMedia'])){
      $data['eventcustomSocialMedia'] = $data['eventcustomSocialMedia'][0];
    }
    $this->ci->load->view('eventdetails/event_custom_social_media',$data);
  }

  
   /*
   * @access: private
   * @description: This function is used to validate custom contact personal
   * @return void
   * 
   */ 
  
  private function _customSocialMediaValidate(){
    
    $this->ci->form_validation->set_rules('mediatypename', 'Media Type Name', 'trim|required');
    $this->ci->form_validation->set_rules('mediatypevalue', 'Media Type Value', 'trim|required');
  }
  
  // ------------------------------------------------------------------------ 
  
  /*
   * @access: private
   * @description: This function is used to validate & insert and update contact person details 
   * @return void
   * 
   */ 
   public function insertupdateeventcustomsocialmedia(){    
    //call function for validate
    $this->_customSocialMediaValidate();    
    if($this->ci->form_validation->run($this->ci)) {
      // call a function to save contact details
      $this->_saveCustomSocialMedia();      
    }else{
      $errors = $this->ci->form_validation->error_array();
      echo json_message('msg',$errors,'is_success','false');
    } 
   }
   
   /*
   * @access: private
   * @description: This function is used to save Custom contact details 
   * @return void
   * 
   */ 
   
   private function _saveCustomSocialMedia(){    
      $customFieldId          = $this->ci->input->post('customFieldId');
      $data['event_id']       = $this->ci->input->post('eventId');
      $data['media_name']     = trim($this->ci->input->post('mediatypename'));
      $data['media_value']    = trim($this->ci->input->post('mediatypevalue'));
      
      // udpate custome field data
      if($customFieldId > 0){
        $where = array('id'=>$customFieldId);
        $this->ci->common_model->updateDataFromTabel('event_social_media_custom',$data,$where);
        $msg = lang('event_msg_custom_social_media_updated');
        $returnId = $customFieldId;
        $saveType = 'update';
      }else{
        // data custome field data
        $insertId = $this->ci->common_model->addDataIntoTabel('event_social_media_custom', $data);
        $msg = lang('event_msg_custom_social_media_added');
        $returnId = $insertId;
        $saveType = 'insert';
      }
      
      $returnArray=array('msg'=>$msg,'is_success'=>'true','url'=>$this->ci->uri->uri_string(),'id'=>$returnId,'saveType'=>$saveType); 
      //set_global_messages($returnArray);
      echo json_encode($returnArray);
  }
  
  /*
   * @access:    public
   * @description: This function is used to delete custome field in personal form
   * @return void
   * 
   */   
  
  public function deleteCustomeSocialMediaType(){
    
    $deleteid = $this->ci->input->post('deleteId');
    //delete registant record from other table
    $where = array('id'=>$deleteid);
    $this->ci->common_model->deleteRowFromTabel('event_social_media_custom',$where);
    $msg = lang('event_msg_custom_social_media_deleted');
    $returnArray=array('msg'=>$msg,'is_success'=>'true');
    set_global_messages($returnArray);
    echo json_encode($returnArray);
  }
   
  
  
  
}


/* End of file factory.php */
/* Location: ./system/application/modules/event/libraries/factory.php */
