<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 *                                                  
 * This class is used to manage for event setup controller manage
 *
 * @package   event manage
 * @author    Amit Soni
 * @email     amitsoni@cdnsol.com
 * @since     Version 1.0
 * @filesource
 */

class Event_Libraries_Factoryfree{
   
  //set limit of show records 
  private $limit = '10';  
  private $ci = NULL;  
  
  function __construct($ci){
    //create instance object
    $this->ci = $ci;

  }
  
   /*
    * @access:    public
    * @description: This function is used to show event details forms like gerneral event, contact person
    * @param1:    $eventId
    * @return:    void
    *  
    */
     
  public function eventdetails($eventId="0"){
      //decode entered eventid 
    $getEventId = decode($eventId);  
    if(!empty($eventId) && is_numeric($getEventId)){
      $this->ci->session->set_userdata('eventId',$eventId);
    }elseif(currentEventId()){
      //current open eventId
      $eventId = currentEventId();
    }
    
    //decode entered eventid
    $eventId = decode($eventId); 
    
    //check is data post 
    if($this->ci->input->post()){
      
      // Store Collapse Value in Session to open the next form
      $this->ci->session->set_userdata('openCollapseValue',$this->ci->input->post('collapseValue'));
      $actionName = $this->ci->input->post('formActionName');
      //echo '<pre>'; print_r($this->ci->input->post()); exit;
      #echo '<pre>'; print_r($_FILES); exit; 

      switch($actionName){
        //update general event setup form data
        case "generalEventSetup":
          $this->updategeneraleventsetup();
        break;
        
        case "generalEventVenueSetup":
          $this->updateGeneralEventVenueSetup();
        break;
        
        //insert & update general contact person form data
        case "contactPerson":
          $this->insertupdatecontactperson();
        break;              
        
        //insert & update general contact person form data
        case "eventSocialMedia":
          $this->insertupdateeventSocialMedia();
        break;
        
        //insert & update custom contact person form data
        case "eventCustomSocialMediaSave":
          $this->insertupdateeventcustomsocialmedia();
        break;
        
        //insert & update general term & condition form data
        case "eventTermsCondition":
          $this->insertupdatetermscondition();
        break;
        
        case "eventVenueContactPerson":
            $this->eventVenueContactPersonSave();
        break;
        
        default:
        // load event form view
        $this->eventshowview($eventId);
      }
    }else{
      // load event details form view
      $this->eventshowview($eventId);
    }
  }
  
   /*
    * @access: private 
    * @description: This function is used to load event details view
    * @param1: $eventId
    * @return: void
    *  
    */
  
  private function eventshowview($eventId="0"){
      
    //chheck if eventId is zero 
    if($eventId==0){
      redirect(base_url('dashboard'));
    }
    $userId = isLoginUser();
    $data['userId'] = $userId;
    $data['eventId'] =$eventId;
    $data['eventdetails'] = $this->ci->event_model->geteventdetails($eventId,$userId);
   
    //if empty then redirect
    if(empty($data['eventdetails'])){
      redirect(base_url('dashboard'));
    }
    
    $where = array('event_id'=>$eventId);
    $whereUserData = array('id'=>$userId);
    //$data['eventsponsorslogos']   =   $this->ci->common_model->getDataFromTabel('event_sponsors_logos','*',$where);
    $data['userdata']           =   $this->ci->common_model->getDataFromTabel('user','*',$whereUserData);
    $data['mastercountrydata']  =   $this->ci->common_model->getDataFromTabel('master_country','*');
    $data['countrydata']        =   $this->ci->common_model->getDataFromTabel('country','*');
    $data['statedata']          =   $this->ci->common_model->getDataFromTabel('state','*');
    $data['getEventCategories'] =   $this->ci->common_model->getDataFromTabel('event_categories','*',$where);
    $data['eventSocialMediadetails'] =   $this->ci->common_model->getDataFromTabel('event_social_media','*',$where);
    
    $data['accountType'] = $this->ci->config->item('accountType');
	$data['fileSuffix'] = $this->ci->config->item('fileSuffix');
    
    $this->ci->template->load('template','eventdetails/event_details'.$this->ci->config->item('fileSuffix'),$data,TRUE);
  }
  
  
  /*
   * @access: private
   * @description: This function is used to check validate & save general event venue details 
   * @return void
   * 
   */ 
  
  private function updateGeneralEventVenueSetup(){
    
    //this function is used to validate general event setup   
    //$this->_generalEventVenueValidate();  
    //get ajax request
    $is_ajax_request = $this->ci->input->is_ajax_request();
    //get eventId
    $eventId = $this->ci->input->post('eventId');
    $action = $this->ci->input->post('next_save');
    if($action == 'next_save'){
			$valCheck = true;
			if(!empty($valCheck)){
			$this->_generalEventVenueSave($eventId);
			$msg = lang('event_msg_general_event_setup_saved');
			if($is_ajax_request){
				echo json_encode(array('msg'=>$msg,'is_success'=>'true','url'=>'event/eventdetails'));
			  }else{
				echo json_encode(array('msg'=>$msg,'is_success'=>'true','url'=>'event/eventdetails'));
			}
		}
	}	
	//$this->ci->form_validation->run($this->ci)
    if ($eventId)
    {
      //call for save general event setup
      $this->_generalEventVenueSave($eventId);
      
      $msg = lang('event_msg_general_event_setup_saved');
      if($is_ajax_request){
        //echo json_message('msg',$msg);
        echo json_encode(array('msg'=>$msg,'is_success'=>'true','url'=>'event/eventdetails'));
      }else{
        $msgArray = array('msg'=>$msg,'is_success'=>'true');
        set_global_messages($msgArray);
        redirect('event/eventdetails/'.encode($eventId));
      }
    }
    else
    {
      $errors = $this->ci->form_validation->error_array();
      if($is_ajax_request){
        echo json_message('msg',$errors,'is_success','false');
      }else{
        // load event form view
        $this->eventshowview($eventId);
      }
    }
  }
  
  /*
   * validate venue details
   * @param eventVenue
   */   
  private function _generalEventVenueValidate(){    
    // set ruls for form filed
    $this->ci->form_validation->set_rules('eventLocation', 'event location', 'trim');
    $this->ci->form_validation->set_rules('eventVenue', 'event venue', 'trim|required');
    /*
	$this->ci->form_validation->set_rules('eventVenueAddress1', 'event address', 'trim|required');
	$this->ci->form_validation->set_rules('eventVenueAddress2', 'event address', 'trim|required');
	$this->ci->form_validation->set_rules('eventVenueCity', 'event city', 'trim|required');
	$this->ci->form_validation->set_rules('eventVenueState', 'event state', 'trim|required');
	$this->ci->form_validation->set_rules('eventVenueZip', 'event zip', 'trim|required');
    */
  }
  
  /*
   * @access: private
   * @description: This function is used to save general event setup 
   * @return void
   * 
   */ 
  
  private function _generalEventVenueSave($eventId){
      
    $updateData['event_location']       = $this->ci->input->post('eventLocation');
    $updateData['event_venue']        = $this->ci->input->post('eventVenue');
    $updateData['destination']        = $this->ci->input->post('eventDestination');
    $updateData['eventVenueAddress1']      = $this->ci->input->post('eventVenueAddress1');
    $updateData['eventVenueAddress2']      = $this->ci->input->post('eventVenueAddress2');
    $updateData['eventVenueCity']          = $this->ci->input->post('eventVenueCity');
    $updateData['eventVenueState']         = $this->ci->input->post('eventVenueState');
    $updateData['eventVenueZip']           = $this->ci->input->post('eventVenueZip');
    $updateData['show_map']           = $this->ci->input->post('show_map');
    $updateData['event_lat']           = $this->ci->input->post('event_lat');
    $updateData['event_long']           = $this->ci->input->post('event_long');
    //prepare condition for update data by id
    $where = array('id'=>$eventId);
    //call model for update data
    $this->ci->common_model->updateDataFromTabel('event',$updateData,$where); 
  }
  
  
  /*
   * @access:      private
   * @description: This function is used to check validate & save general event setup details 
   * @return void
   * 
   */ 
  
  private function updategeneraleventsetup(){
    
    //this function is used to validate general event setup   
    $this->_generalEventSetupValidate();  
    $action = $this->ci->input->post('next_save');
    
    //get ajax request
    $is_ajax_request = $this->ci->input->is_ajax_request();
    //get eventId
    $eventId = $this->ci->input->post('eventId');
	if($action == 'next_save'){
			$valCheck = true;
			if(!empty($valCheck)){
					 $this->_generalEventSetupSave($eventId);
					$msg = lang('event_msg_general_event_setup_saved');
					if($is_ajax_request){
					//echo json_message('msg',$msg);
					echo json_encode(array('msg'=>$msg,'is_success'=>'true','url'=>'event/eventdetails'));
					}else{
					$msgArray = array('msg'=>$msg,'is_success'=>'true');
					set_global_messages($msgArray);
					redirect('event/eventdetails/'.encode($eventId));
				  }
			}
	}	
    if ($this->ci->form_validation->run($this->ci))
    {
      //call for save general event setup
      $this->_generalEventSetupSave($eventId);
      
      $msg = lang('event_msg_general_event_setup_saved');
      if($is_ajax_request){
        //echo json_message('msg',$msg);
        echo json_encode(array('msg'=>$msg,'is_success'=>'true','url'=>'event/eventdetails'));
      }else{
        $msgArray = array('msg'=>$msg,'is_success'=>'true');
        set_global_messages($msgArray);
        redirect('event/eventdetails/'.encode($eventId));
      }
    }
    else
    {
      $errors = $this->ci->form_validation->error_array();
      if($is_ajax_request){
        echo json_message('msg',$errors,'is_success','false');
      }else{
        // load event form view
        $this->eventshowview($eventId);
      }
    }
  }
  
  
  
  
  /*
   * validate
   * general event setup details
   */ 
  
  private function _generalEventSetupValidate(){
    
    // set ruls for form filed
    //$this->ci->form_validation->set_rules('eventName', 'event name', 'trim|required');
    //$this->ci->form_validation->set_rules('eventReferenceNumber', 'event reference number', 'trim');
    $this->ci->form_validation->set_rules('eventStartDate', 'event start date', 'trim|required');
    $this->ci->form_validation->set_rules('eventEndDate', 'event end date', 'trim|required');
    //$this->ci->form_validation->set_rules('eventDescription', 'event description', 'trim');
    //$this->ci->form_validation->set_rules('eventMaxRegistrants', 'event max registrants', 'trim|required|numeric');
    //$this->ci->form_validation->set_rules('registerLastDate', 'register last date', 'trim|required');
    $this->ci->form_validation->set_rules('registerStartDate', 'Invitation start date', 'trim|required');
    //$this->ci->form_validation->set_rules('eventLocation', 'event location', 'trim');
    //$this->ci->form_validation->set_rules('eventVenue', 'event venue', 'trim|required');
    //$this->ci->form_validation->set_rules('sittingStyle', 'sitting style', 'trim');
    //$this->ci->form_validation->set_rules('timeZone', 'time zone', 'trim|required');
    //check event selected date
    $this->ci->form_validation->set_rules('eventUrl', 'Event url', 'trim|required');
    
    $event_url      = $this->ci->input->post('eventUrl');
    $eventUrlOld    = $this->ci->input->post('eventUrlOld');
    
    if(trim($event_url) != trim($eventUrlOld)) {
        $where = array('event_url'=> $event_url );
        $getEventDetails = countRowsTable('event',$where);
        
        if($getEventDetails > 0) {
            $this->ci->form_validation->set_rules('eventUrlOld', 'Event URL does not available.', 'callback_custom_error_set');
        }
    }
    
    
    if($this->ci->input->post('eventStartDate') && $this->ci->input->post('eventEndDate')){
      
      $eventStartDate      = strtotime($this->ci->input->post('eventStartDate'));
      $eventEndDate        = strtotime($this->ci->input->post('eventEndDate'));
      $registerLastDate    = strtotime($this->ci->input->post('registerLastDate'));
     
      $registerStartDate    = strtotime($this->ci->input->post('registerStartDate'));
 
      $currentDate      = strtotime(date('y-m-d'));
      
      if($currentDate  > $eventEndDate){
        $this->ci->form_validation->set_rules('eventStartDateVali', 'Event end date should be greater than equal from today date.', 'callback_custom_error_set');
      }
      
      if($eventStartDate > $eventEndDate){
        $this->ci->form_validation->set_rules('eventStartDateVali', 'Event start date should not be greater than event end date.', 'callback_custom_error_set');
      }
    if(!empty($registerLastDate) && ($registerLastDate > 0) ) {
      if($registerLastDate > $eventStartDate){
        $this->ci->form_validation->set_rules('registerLastDate', 'Invitation cut off date should not be greater than event start date.', 'callback_custom_error_set');
      }
      
       if($registerStartDate > $registerLastDate){
        $this->ci->form_validation->set_rules('registerStartDate', 'Invitation start date should not be greater than form registration last date.', 'callback_custom_error_set');
      }
      
      if($registerStartDate > $registerLastDate){
        $this->ci->form_validation->set_rules('registerStartDate', 'Invitation start date should not be greater than form registration last date.', 'callback_custom_error_set');
      }
    }
      
      if($registerStartDate > $eventStartDate){
        $this->ci->form_validation->set_rules('registerLastDate', 'Invitation start date should not be greater than form start date.', 'callback_custom_error_set');
      }
      
     
       
    } 
  }
  
  /*
   * @access: private
   * @description: This function is used to save general event setup 
   * @return void
   * 
   */ 
  
  private function _generalEventSetupSave($eventId){
   
    // prepare array for update data
    $isRegisterLastDateYes              = $this->ci->input->post('isRegisterLastDateYes');
    
    $updateData['event_title']          = $this->ci->input->post('eventName');
    $updateData['subtitle']          	= $this->ci->input->post('eventSubtitle');
    $updateData['short_form_title']     = $this->ci->input->post('eventShortFormTitle');
    $updateData['event_reference_number']   = $this->ci->input->post('eventReferenceNumber');
    $updateData['starttime']        	= dateFormate($this->ci->input->post('eventStartDate'),"Y-m-d H:i:s");
    $updateData['endtime']            	= dateFormate($this->ci->input->post('eventEndDate'),"Y-m-d H:i:s");
    $updateData['description']        	= $this->ci->input->post('eventDescription');
    $updateData['number_of_rego']       = $this->ci->input->post('eventMaxRegistrants');
    if($isRegisterLastDateYes){
        $updateData['last_reg_date']    = dateFormate($this->ci->input->post('registerLastDate'),"Y-m-d H:i:s");  
    }else{
        $updateData['last_reg_date']    = '';  
    }
      
    
    $updateData['reg_startdate']        = dateFormate($this->ci->input->post('registerStartDate'),"Y-m-d H:i:s");
    $updateData['sitting_style']        = $this->ci->input->post('sittingStyle');
    $updateData['time_zone']            = $this->ci->input->post('timeZone');
    $updateData['event_is_public']      = $this->ci->input->post('eventIsPublic');
    $updateData['event_url']            = $this->ci->input->post('eventUrl');
    
    
    
    //prepare condition for update data by id
    $where = array('id'=>$eventId);
    //call model for update data
    $this->ci->common_model->updateDataFromTabel('event',$updateData,$where); 
  }
  
   /*
   * @access: private
   * @description: This function is used to validate & insert and update contact person details 
   * @return void
   * 
   */ 
  
  private function insertupdatecontactperson(){
    
    //call function for validate
    //$this->_contactPersonValidate();
    //get ajax request
    $is_ajax_request = $this->ci->input->is_ajax_request();
    //get eventId
    $eventId = $this->ci->input->post('eventId');
    $action = $this->ci->input->post('next_save');
    
     
    if($action == 'next_save'){
			$valCheck = true;
			if(!empty($valCheck)){
			#echo '<pre>'; print_r($this->ci->input->post()); exit;
			$msg = $this->_contactPersonSave($eventId);
			//check ajax post request 
			if($is_ajax_request){
			  echo json_message('msg',$msg);
			}else{
			  $msgArray = array('msg'=>$msg,'is_success'=>'true');
			  set_global_messages($msgArray);
			  redirect('event/eventdetails/'.encode($eventId));
			}
		}
	}	
    //$this->ci->form_validation->run($this->ci)
    if(isset($eventId))
    {
        //call function for save 
        $msg = $this->_contactPersonSave($eventId);
        //check ajax post request 
        if($is_ajax_request){
          echo json_message('msg',$msg);
        }else{
          $msgArray = array('msg'=>$msg,'is_success'=>'true');
          set_global_messages($msgArray);
          redirect('event/eventdetails/'.encode($eventId));
        }
    }else{
      
      $errors = $this->ci->form_validation->error_array();
      if($is_ajax_request){
        echo json_message('msg',$errors,'is_success','false');
      }else{
        // load event form view
        $this->eventshowview($eventId);
      }
    } 
  }
  
   /*
   * @access: private
   * @description: This function is used to validate contact personal details
   * @return void
   * 
   */ 
  
  private function _contactPersonValidate(){
  
    /*
    $this->ci->form_validation->set_rules('firstName', 'first name', 'trim|required');
    $this->ci->form_validation->set_rules('lastName', 'last name', 'trim|required');
    $this->ci->form_validation->set_rules('contactOrganisation', 'organisation', 'trim');
    $this->ci->form_validation->set_rules('contactPhone', 'contact phone', 'trim|required');
    $this->ci->form_validation->set_rules('contactFax', 'contact fax', 'trim');
    $this->ci->form_validation->set_rules('contactMobile', 'mobile', 'trim');
    $this->ci->form_validation->set_rules('contactEmail', 'email', 'trim|required|email');
    $this->ci->form_validation->set_rules('confirmEmail', 'confirm email', 'trim|required|email|matches[contactEmail]');
    $this->ci->form_validation->set_rules('contactWeb', 'web url', 'trim');
    * 
    */
    return true;
  }
  
   /*
   * @access: private
   * @description: This function is used to save contact personal details
   * @return void
   * 
   */ 
  
  private function _contactPersonSave($eventId){

    $getData['title']               = $this->ci->input->post('title');
    $getData['first_name']          = $this->ci->input->post('firstName');
    $getData['last_name']           = $this->ci->input->post('lastName');
    $getData['event_id']            = $this->ci->input->post('eventId');
    $getData['organisation']        = $this->ci->input->post('contactOrganisation');
    //$getData['mobile']            = $this->ci->input->post('contactPhone');
    $getData['phone1_mobile']       = $this->ci->input->post('input_hidden_mobile1');
    $getData['phone1_landline']     = $this->ci->input->post('input_hidden_landline1');
    $getData['email']               = $this->ci->input->post('contactEmail');
   
    $customContactType              = $this->ci->input->post('contactType'); // post as array 
    $customContactDetails           = $this->ci->input->post('contactDetails'); // post as array 
    // check contact person details by event id
    $where = array('event_id'=>$eventId);
    //$countResult = $this->ci->common_model->countResult('event_contact_person',$where);
    $result = $this->ci->common_model->getDataFromTabel('event_contact_person','id',$where);
    
    //update here
    if($result){      
      // get event contact persion id
      $customContactId = $result[0]->id;
      $this->ci->common_model->updateDataFromTabel('event_contact_person',$getData,$where);
      $msg = lang('event_msg_contact_person_details_saved');
    }else{
      //insert here
      $customContactId = $this->ci->common_model->addDataIntoTabel('event_contact_person', $getData);
      $msg = lang('event_msg_contact_person_details_saved');
    }
    
     // delete records if exits based on contact persion id
    $contactPersonId=$this->ci->input->post('contact_person_id'); // array posted
    if(!empty($contactPersonId)){
      $this->ci->event_model->deleteeventcustomcontact($contactPersonId);
    } 
        
    //store custom contacts
    if(!empty($customContactType)){
    
      
      foreach($customContactType as $key=>$value){
        $customContactData = array();
        
        $customContactData['contact_person_id'] = $customContactId;
        $customContactData['contact_type']    = $value;
        $customContactData['details']         = (isset($customContactDetails[$key])) ? $customContactDetails[$key] : "";
        
        $bulkCustomData[] = $customContactData;
        
      }//end loop
      
      $this->ci->event_model->addeventcustomcontact($bulkCustomData);
    } 
        
    return $msg;
  }
  
  
   /*
   * @access: private
   * @description: This function is used to validate & insert and update socail media details 
   * @return void
   * 
   */ 
  
  private function insertupdateeventSocialMedia(){
    
    //call function for validate
    $this->_eventSocialMediaValidate();    
    //get ajax request
    $is_ajax_request = $this->ci->input->is_ajax_request();
    $eventId = $this->ci->input->post('eventId');
    //get eventId
    $action = $this->ci->input->post('next_save'); 
    if($action == 'next_save'){
		$msg = $this->_eventSocialMediaSave($eventId);
	} else {
		$msg = $this->_eventSocialMediaSave($eventId);
	}
    
    
      
    //check ajax post request 
    if($is_ajax_request){
      echo json_message('msg',$msg);
    }else{
      $msgArray = array('msg'=>$msg,'is_success'=>'true');
      set_global_messages($msgArray);
      redirect('event/eventdetails/'.encode($eventId));
    }
  }
  
    /*
   * @access: private
   * @description: This function is used to validate socail media details
   * @return void
   * 
   */ 
  
  private function _eventSocialMediaValidate(){  
    // set rules for form fields 
    return true;   
  }
  
   /*
   * @access: private
   * @description: This function is used to save socail media details
   * @return void
   * 
   */ 
  
  private function _eventSocialMediaSave($eventId){
    // difine data in array 
    $getData['event_id']       = $this->ci->input->post('eventId');
    $getData['website']        = $this->ci->input->post('eventWebsite');
    $getData['facebook']       = $this->ci->input->post('eventFacebook');
    $getData['instagram']      = $this->ci->input->post('eventInstagram');
    $getData['twitter']        = $this->ci->input->post('eventTwitter');
    $getData['youtube']        = $this->ci->input->post('eventYoutube');
    $getData['pintrest']       = $this->ci->input->post('eventPintrest');
    $getData['gplus']          = $this->ci->input->post('eventGplus');
    $getData['linkedIn']       = $this->ci->input->post('eventLinkedin');

    // check contact person details by event id
    $where = array('event_id'=>$eventId);
    $result = $this->ci->common_model->getDataFromTabel('event_social_media','id',$where);
    
    //update here
    if($result){      
      // get event contact persion id
      $customContactId = $result[0]->id;      
      $this->ci->common_model->updateDataFromTabel('event_social_media',$getData,$where);
      $msg = lang('event_msg_contact_person_details_saved');
    }else{
      //insert here
      $customContactId = $this->ci->common_model->addDataIntoTabel('event_social_media', $getData);
      $msg = lang('event_msg_contact_person_details_saved');
    }
        
    return $msg;
  }
  
  /*
   * @access: private
   * @description: This function is used to insert and update term condition 
   * @return void
   * 
   */ 
  
  private function insertupdatetermscondition(){
    $this->_termsConditionValidate();
    $is_ajax_request    = $this->ci->input->is_ajax_request();
    $eventId            = $this->ci->input->post('eventId');
    $action = $this->ci->input->post('next_save');
    //echo '<pre>';print_r($_FILES); exit;
    #echo '<pre>';  print_r($this->ci->input->post()); 
    //exit;

    if($action == 'next_save'){
      $termArray        = $this->_termsConditionSave($eventId);
      $msg              = $termArray['msg'];
      $termid           = $termArray['termid'];
      $url              = $termArray['url'];
      if($is_ajax_request){
        echo json_encode(array('msg'=>$msg,'is_success'=>'true','termid'=>$termid,'url'=>$url));
      }else{
        $msgArray = array('msg'=>$msg,'is_success'=>'true');
        set_global_messages($msgArray);
        redirect('event/eventdetails/'.encode($eventId));
      } 
	}
    if ($this->ci->form_validation->run($this->ci)){
      $termArray        = $this->_termsConditionSave($eventId);
      $msg              = $termArray['msg'];
      $termid           = $termArray['termid'];
      $url              = $termArray['url'];
      if($is_ajax_request){
        echo json_encode(array('msg'=>$msg,'is_success'=>'true','termid'=>$termid,'url'=>$url));
      }else{
        $msgArray = array('msg'=>$msg,'is_success'=>'true');
        set_global_messages($msgArray);
        redirect('event/eventdetails/'.encode($eventId));
      } 
    }else{
      $errors = $this->ci->form_validation->error_array();
      if($is_ajax_request){
        echo json_message('msg',$errors,'is_success','false');
      }else{
        $this->eventshowview($eventId);
      }
    } 
  } 
  
  
   /*
   * @access: private
   * @description: This function is used to validate term condition 
   * @return void
   * 
   */ 
  
  private function _termsConditionValidate(){  
    //set validation term
    if($this->ci->input->post('termSelection')=='0'){
      $this->ci->form_validation->set_rules('eventnTermsCondition', 'term condition', 'trim|required');
    }else{
      $this->ci->form_validation->set_rules('eventnTermsCondition', 'term condition', 'trim');
    }
  }
  
  
   /*
   * @access: private
   * @description: This function is used to save term condition 
   * @param: $eventId (int)
   * @return: $termArray
   * 
   */ 
  
  private function _termsConditionSave($eventId){
	//echo '1'; exit; 
    //prepare data
    $getData['event_id']     = $this->ci->input->post('eventId');
    $getData['term_conditions']  = $this->ci->input->post('eventnTermsCondition');
    
    // check contact person details by event id
    $where = array('event_id'=>$eventId);
    $getResult = $this->ci->common_model->getDataFromTabel('event_term_conditions','id',$where);
    $countResult = count($getResult);
    //update here
    if(!empty($getResult) && $countResult > 0){
      $this->ci->common_model->updateDataFromTabel('event_term_conditions',$getData,$where);
      $msg = lang('event_msg_event_term_condition_saved');
      $termid = $getResult[0]->id;
    }else{
      //insert here
      $insertId = $this->ci->common_model->addDataIntoTabel('event_term_conditions', $getData);
      $msg = lang('event_msg_event_term_condition_saved');
      $termid = $insertId;
    }
    $termArray = array('msg'=>$msg,'termid'=>$termid,'url'=>'event/eventdetails/');
    return    $termArray;
  }
  
  
  /*
    * @access:    public
    * @description: This function is used to show event details forms like gerneral event, contact person
    * @param1:    $eventId
    * @return:    void
    *  
    */
     
  public function invitations(){
    //current open eventId
    $eventId = currentEventId();
    
    //decode entered eventid
    $eventId = decode($eventId); 
    
    //check is data post 
    if($this->ci->input->post()){
      
      // Store Collapse Value in Session to open the next form
      $this->ci->session->set_userdata('openCollapseValue',$this->ci->input->post('collapseValue'));
      $actionName = $this->ci->input->post('formActionName');
      
      #echo '<pre>'; print_r($this->ci->input->post()); exit; 

        switch($actionName){  
        case "bookerDetails":
          $this->insertupdatemasterpersonaldetails();
        break;
        case "bookerCustomFieldSave":
          $this->addeditcustomefieldsave(); 
        break;
        case "masterPersonalDetails":
          $this->insertupdatemasterpersonaldetails();
        break;  
        case "registrantCustomFieldSave":
          $this->addeditcustomefieldsave(); 
        break; 
        case "invitationsDetails":
          $this->insertupdateinvitationtypes();
        break;
        case "dietaryrequirements":
          $this->addeventdietaryfree();
        break;
        
        default:
        // load event form view
        $this->invitationshowview($eventId);
      }
    }else{
      #echo '<pre>'; print_r($eventId); exit; 
      // load event details form view
      $this->invitationshowview($eventId);

    }
  }
  
  
  /*
   * @access:    public
   * @description: This function is used to add dietary 
   * @return void
   * 
   */ 
   
  public function addeventdietaryfree(){
    $this->ci->form_validation->set_rules('dietaryName', 'dietary name', 'trim|required');
    
    if($this->ci->input->post()){
    
      if ($this->ci->form_validation->run($this->ci))
      { 
		  
        $eventId           = $this->ci->input->post('eventId');
        $dietaryName         = $this->ci->input->post('dietaryName');
        $dataArray['dietary_name']   = $dietaryName;
        $dataArray['event_id']     = $eventId;
        $dataArray['is_undeletable'] = '0';
        
        //get deitary requirement data and order of dietary
        $whereCondi = array('event_id'=>$eventId);
        $dietaryOrder = 0;
        $eventdietary = $this->ci->common_model->getDataFromTabel('event_dietary','id,dietary_order',$whereCondi,'','dietary_order','DESC','1');
        if(!empty($eventdietary)){
          $eventdietary = $eventdietary[0];
          $dietaryOrder = $eventdietary->dietary_order;
          $dietaryId = $eventdietary->id;
          
          //update deitary order for making it last order
          $updateData['dietary_order'] = $dietaryOrder+1;
          $whereUp = array('id'=>$dietaryId);
          $this->ci->common_model->updateDataFromTabel('event_dietary',$updateData,$whereUp);
        }
        $dataArray['dietary_order'] = $dietaryOrder;
        
        //call member function for add dietary
        $insertId = $this->adddietarydata($dataArray);
        $msg = lang('event_msg_dietary_add_successfully');
        $returnArray=array('msg'=>$msg,'id'=>$insertId,'is_success'=>'true'); 
        echo json_encode($returnArray);
      }else{
        $errors = $this->ci->form_validation->error_array();
        echo json_message('msg',$errors,'is_success','false');
        
      }
    }else{
      redirect(base_url('dashboard'));
    }
  }
  
  private function adddietarydata($dataArray){
    
    $insertData['dietary_name']   = $dataArray['dietary_name'];
    $insertData['event_id']     = $dataArray['event_id'];
    $insertData['is_undeletable'] = $dataArray['is_undeletable'];
    $insertData['dietary_order']  = $dataArray['dietary_order'];
    $insertId = $this->ci->common_model->addDataIntoTabel('event_dietary', $insertData);
    return $insertId;
  } 
  
  
  /*
    * @access: private 
    * @description: This function is used to load event details view
    * @param1: $eventId
    * @return: void
    *  
    */
  
  private function invitationshowview($eventId="0"){
    

    //chheck if eventId is zero 
    if($eventId==0){
      redirect(base_url('dashboard'));
    }
    $userId          = isLoginUser();
    $data['userId']      = $userId;
    $data['eventId']     = $eventId;
    
    //get event details 
    $data['eventdetails'] = $this->ci->event_model->geteventdetails($eventId,$userId);
    #echo "<pre>"; 
    #print_r($data); exit; 
    //if empty then redirect
    if(empty($data['eventdetails'])){
      redirect(base_url('dashboard'));
    }
    
    $where = array('event_id'=>$eventId);
    $data['eventinvitations'] = $this->ci->common_model->getDataFromTabel('event_invitations','*',$where,'','invitations_order','ASC');
    
    
    //get master field for each event id
    $whereMsterField = array('event_id'=>$eventId,'registrant_id'=>'0','form_id'=>'4');
    $data['masterformfielddata'] = getDataFromTabel('custom_form_fields','*',$whereMsterField);
   
    $whereMsterField = array('event_id'=>$eventId,'registrant_id'=>'0','form_id'=>'5');
    $data['bookerformfielddata'] = getDataFromTabel('custom_form_fields','*',$whereMsterField); 
    #echo $this->ci->db->last_query(); exit;
    //get dietary data  
    $data['eventdietary'] = $this->ci->common_model->getDataFromTabel('event_dietary','*',$where,'','dietary_order','ASC');

    $this->ci->template->load('template','invitations/invitation_setup'.$this->ci->config->item('fileSuffix'),$data,TRUE);
  }
  
  /*
   * @access:    private
   * @description: This function is used to insert and update master personal details form
   * all field save one by one by loop
   * @return void
   * 
   */ 
  
  private function insertupdatemasterpersonaldetails(){
    
    //get ajax request
    $is_ajax_request = $this->ci->input->is_ajax_request();
    //get eventId
    $eventId     = $this->ci->input->post('eventId');
    $action = $this->ci->input->post('next_save'); 
    if($action == 'next_save'){
        $valCheck = true;
        if(!empty($valCheck)){
        $arrayVal     =  $this->ci->input->post('fieldid');
        $arrayFieldVal  =  $this->ci->input->post();
        //echo '<pre>'; print_r($arrayFieldVal); exit;
        foreach($arrayVal as $fieldid){
          $filedRowId = 'field_'.$fieldid;
          $field_status = $arrayFieldVal[$filedRowId];
          $updateData['field_status'] = $field_status;
          $where = array('id'=>$fieldid);
          $this->ci->common_model->updateDataFromTabel('custom_form_fields',$updateData,$where);
        }
        $msg = lang('event_msg_personal_details_form_saved');
        $arrayReturn = array('msg'=>$msg,'is_success'=>'true');
        echo json_encode($arrayReturn);
        }
    }
    
    if ($this->ci->input->post())
    {
      //update personal details form
      $arrayVal     =  $this->ci->input->post('fieldid');
      
      //update field value
      $arrayFieldVal  =  $this->ci->input->post();
      
      //update field status one by one
      foreach($arrayVal as $fieldid){
        
        $filedRowId = 'field_'.$fieldid;
        $field_status = $arrayFieldVal[$filedRowId];
        $updateData['field_status'] = $field_status;
        $where = array('id'=>$fieldid);
        $this->ci->common_model->updateDataFromTabel('custom_form_fields',$updateData,$where);
      }
      
      $msg = lang('event_msg_personal_details_form_saved');
      $arrayReturn = array('msg'=>$msg,'is_success'=>'true');
      echo json_encode($arrayReturn);
    }
    else
    {
      $errors =$this->ci->form_validation->error_array();
      if($is_ajax_request){
        echo json_message('msg',$errors,'is_success','false');
      }else{
        // load registrant form view
        $this->registrantview($eventId);
      }
    } 
  }
  
   /*
   * @access:  public
   * @description: This function is used to add type of registration 
   * @return void
   * 
   */ 
   
  public function addediteventInvitation(){
    //set validation
    $this->ci->form_validation->set_rules('invitationName', 'invitation type', 'trim|required');
    
    if($this->ci->form_validation->run($this->ci)){
      $eventId                          =   $this->ci->input->post('eventId');
      $invitationName                   =   $this->ci->input->post('invitationName');
      $invitationTypeId                 =   $this->ci->input->post('invitationTypeId');
      $insertData['invitation_name']    =   $invitationName;
      $insertData['event_id']           =   $eventId;

      if($invitationTypeId > 0){
        $dataId =  $invitationTypeId; 
        //udpate registrant title
        $where = array('id'=>$invitationTypeId);
        $this->ci->common_model->updateDataFromTabel('event_invitations',$insertData,$where);
        $msg =  lang('event_msg_type_of_invitation_edit_successfully');
      }else{
        //registrant type add
        $invitationId = $this->ci->common_model->addDataIntoTabel('event_invitations', $insertData);
        $msg = lang('event_msg_type_of_invitation_add_successfully');
        //This function is used to add form fields 
        $this->addformfields($eventId,'4',$invitationId);
        $dataId =  $invitationId; 
      }
      
      $returnArray=array('msg'=>$msg,'is_success'=>'true','invitationtypeid'=>$dataId,'pageaction'=>'invitation');
      //set_global_messages($returnArray);
      echo json_encode($returnArray);
    }else{
      $errors = $this->ci->form_validation->error_array();
      echo json_message('msg',$errors,'is_success','false');
    }
  }
  
  /*
   * @access:    private
   * @description: This function is used to add master field to user custome form fields
   * @return void;
   * @param1 = eventId
   * @param2 = formId
   * @param3 = registrantId
   */
   
  private function addformfields($eventId,$formId,$registrantId){
      
      $where = array('event_id'=>$eventId,'registrant_id'=>'0');
      $formfieldmaster = $this->ci->common_model->getDataFromTabel('custom_form_fields','*',$where);
      if(!empty($formfieldmaster)){
        foreach($formfieldmaster as $fieldmaster){
          $arrayVal = $this->ci->input->post();
          $filedId = 'field_'.$fieldmaster->id;
          $insertData['field_name'] = $fieldmaster->field_name;
          $insertData['field_type'] = $fieldmaster->field_type;
          $insertData['default_value'] = $fieldmaster->default_value;
          $insertData['form_id'] = $fieldmaster->form_id;
          $insertData['is_editable'] = $fieldmaster->is_editable;
          $insertData['event_id'] = $eventId;
          $insertData['registrant_id'] = $registrantId;
          $insertData['field_status'] = $fieldmaster->field_status;
          $insertData['field_create_by'] = $fieldmaster->field_create_by;
          $insertId = $this->ci->common_model->addDataIntoTabel('custom_form_fields', $insertData);
        }
      }
  }
  
  /*
   * @access:    private
   * @description: This function is used to insert and update registrant details forms data 
   * @return void
   * 
   */ 
  
  private function insertupdateinvitationtypes(){
    
    if($this->ci->input->post()){
      //get form registrant id
      $invitationId = $this->ci->input->post('invitationId');
      
      //get eventId
      $eventId = $this->ci->input->post('eventId');
      
      //call function for validate
      $this->_invitationValidate($invitationId,$eventId);
      
      //call function for registrant validate
      //$this->_registrantDaywiseValidate($registrantId);
      
      //get ajax request
      $is_ajax_request = $this->ci->input->is_ajax_request();

      $action = $this->ci->input->post('next_save'); 
      if($action == 'next_save'){
          $this->_invitationSave($invitationId,$eventId);
          $this->_insertupdatepersonaldetails();
          $msg = lang('event_msg_invitation_form_details_saved');
          if($is_ajax_request){            
              $returnArray=array('msg'=>$msg,'is_success'=>'true','url'=>$this->ci->uri->uri_string(),'invitationtypedetailsid'=>$invitationId); 
              echo json_encode($returnArray);
          }else{
              $msgArray = array('msg'=>$msg,'is_success'=>'true');
              set_global_messages($msgArray);
              redirect('event/eventdetails/'.encode($eventId));
          }
      }
      if ($this->ci->form_validation->run($this->ci))
      {
        //call function for registrant data save
        $this->_invitationSave($invitationId,$eventId);
        //call personal details form save
        $this->_insertupdatepersonaldetails();
        //set message save
        $msg = lang('event_msg_invitation_form_details_saved');
        // check ajax request
        if($is_ajax_request){            
            //echo json_message('msg',$msg);          
            $returnArray=array('msg'=>$msg,'is_success'=>'true','url'=>$this->ci->uri->uri_string(),'invitationtypedetailsid'=>$invitationId); 
            //set_global_messages($returnArray);
            echo json_encode($returnArray);
        }else{
            $msgArray = array('msg'=>$msg,'is_success'=>'true');
            set_global_messages($msgArray);
            redirect('event/eventdetails/'.encode($eventId));
        }
          
      }else{
        $errors = $this->ci->form_validation->error_array();
        if($is_ajax_request){
          echo json_message('msg',$errors,'is_success','false');
        }else{
          // load event form view
          $this->eventshowview($eventId);
        }
      } 
    }else{
        $msg = lang('event_msg_invalid_data_request');
        $msgArray = array('msg'=>$msg,'is_success'=>'false');
        set_global_messages($msgArray);
        redirect(base_url('dashboard'));
      }
  }
  
  /*
   * @access:    private
   * @description: This function is used to validate registrant  
   * @param1: $registrantId (int)
   * @return: void
   */  
  
  private function _invitationValidate($invitationId,$eventId){
    $this->ci->form_validation->set_rules('limit'.$invitationId, 'limit', 'trim|required|numeric');
     //if check then set access password then insert password
    if($this->ci->input->post('passwordAccess'.$invitationId)=='1'){
      $this->ci->form_validation->set_rules('password'.$invitationId, 'password', 'trim|required|matches[confirmpassword'.$invitationId.']');
      $this->ci->form_validation->set_rules('confirmpassword'.$invitationId, 'confirm password', 'trim|required');
    } 
    $this->ci->form_validation->set_rules('limit_invitation'.$invitationId, 'Limit per Invitation', 'trim|required');
    $this->ci->form_validation->set_rules('remaining_invitations'.$invitationId, 'Show Invitations Remaining', 'trim|required');
    $this->ci->form_validation->set_rules('allow_waiting_list'.$invitationId, 'Allow Waiting List', 'trim|required');
    $limitArrayData        = $this->_getInvitationsLimit($eventId,$invitationId);
    $totalEventRegisterLimit   = $limitArrayData['totalEventRegisterLimit']; // total limit
    $totalRegistrantsLimitIndb   = $limitArrayData['totalRegistrantsLimitIndb']; // get total limit in db for all registrant
    $registrantIdFieldLimit    = $limitArrayData['registrantIdFieldLimit']; // get limit save for this registrant
    $enteredLimit          = $this->ci->input->post('limit'.$invitationId);// user entered limit in in the field
    
    //Check limit availability and set custome error set
    if($enteredLimit  <= $totalEventRegisterLimit){
      if($totalRegistrantsLimitIndb != "0"){
        $getTotalLimitAvailable = $totalEventRegisterLimit   - $totalRegistrantsLimitIndb;
        $curretnRigisLimitdb    = $totalRegistrantsLimitIndb - $registrantIdFieldLimit;
        $avalimitForThisRegis   = $registrantIdFieldLimit    + $getTotalLimitAvailable;
        if($avalimitForThisRegis < $enteredLimit){
          $this->ci->form_validation->set_rules('limittotal', lang('event_limit_max_error_message'), 'callback_custom_error_set');
        }
      }
    }else{
      $this->ci->form_validation->set_rules('limittotal', lang('event_limit_max_error_message'), 'callback_custom_error_set');
    }
    
    
    
  } 
  
  
  private function _getInvitationsLimit($eventId,$registrantId){
    
    //get event details data
    $where = array('id'=>$eventId);
    $regisWhere = array('event_id'=>$eventId);
    $regisIdWhere = array('id'=>$registrantId);
    $getEventData = $this->ci->common_model->getDataFromTabel('event','number_of_rego',$where);
    $getRegisLimitSum = $this->ci->common_model->getSum('event_invitations','invitation_limit',$regisWhere);
    $getRegistrantsIdData = $this->ci->common_model->getDataFromTabel('event_invitations','invitation_limit',$regisIdWhere);
    
    $totalEventRegisterLimit = 0;
    //get number of registrant
    if(!empty($getEventData )){
      $getEventData = $getEventData[0];
      $totalEventRegisterLimit = $getEventData->number_of_rego;
    }
    $totalRegistrantsLimitIndb = 0;
    //get registered number sum
    if(!empty($getRegisLimitSum )){
      $getRegisLimitSum = $getRegisLimitSum[0];
      $totalRegistrantsLimitIndb = $getRegisLimitSum->invitation_limit;
    }
    
    $registrantIdFieldLimit = 0;
    //get registered number sum
    if(!empty($getRegistrantsIdData )){
      $getRegistrantsIdData = $getRegistrantsIdData[0];
      if(!empty($getRegistrantsIdData->invitation_limit)){
        $registrantIdFieldLimit = $getRegistrantsIdData->invitation_limit;
      }
    }
    
    $returnArray = array('totalEventRegisterLimit'=>$totalEventRegisterLimit,'totalRegistrantsLimitIndb'=>$totalRegistrantsLimitIndb
    ,'registrantIdFieldLimit'=>$registrantIdFieldLimit);
    return $returnArray;
  } 
  
  
  
  /*
   * @access:    private
   * @description: This function is used to validate registrant  
   * @param1: $registrantId (int)
   * @return: void
   */  
  
  private function _invitationSave($registrantId,$eventId){
    
   
    //$updateData['details']            = $this->ci->input->post('additionalDetails'.$registrantId);
    $updateData['invitation_limit']             = $this->ci->input->post('limit'.$registrantId);
    $updateData['limit_per_invitation']             = $this->ci->input->post('limit_invitation'.$registrantId);
    $updateData['show_remaining_invitations']             = $this->ci->input->post('remaining_invitations'.$registrantId);
    $updateData['allow_waiting_list']             = $this->ci->input->post('allow_waiting_list'.$registrantId);

    
    //if check then set access password then insert password
    if($this->ci->input->post('passwordAccess'.$registrantId)=='1'){
      $updateData['password']  =  $this->ci->input->post('password'.$registrantId);
    }else{
      $updateData['password']  = NULL;
    } 

    $whereRegis = array('id'=>$registrantId); 
    $this->ci->common_model->updateDataFromTabel('event_invitations',$updateData,$whereRegis);
  }
  
   /*
   * @access:    private
   * @description: This function is used to insert and update personal details form
   * all field save one by one by loop
   * @return void
   * 
   */ 
  
  private function _insertupdatepersonaldetails(){
  
    if ($this->ci->input->post())
    {
      //update personal details form
      $arrayVal     =   $this->ci->input->post('fieldid');
      
      //update field value
      $arrayFieldVal  =   $this->ci->input->post();
      
      //update field status one by one
      foreach($arrayVal as $fieldid){
        
        $filedRowId = 'field_'.$fieldid;
        $field_status = $arrayFieldVal[$filedRowId];
        $updateData['field_status'] = $field_status;
        $where = array('id'=>$fieldid);
        $this->ci->common_model->updateDataFromTabel('custom_form_fields',$updateData,$where);
      }
    }
      
  }
  
    /*
   * @access:  public
   * @description: This function is used to add and edit perosnal details form custom field
   * @return void
   * 
   */ 
  
  public function addeditcustomfield(){
   // echo '<pre>';    print_r($_POST);die;
    $data['eventId']      = $this->ci->input->post('eventId');
    $data['registrantidval']  = $this->ci->input->post('registrantid');
    $customfieldid        = $this->ci->input->post('customfieldid');
    $data['formId']         = $this->ci->input->post('formId');
    $data['formAction']       = $this->ci->input->post('formAction');
    $data['parentFormId']       = $this->ci->input->post('parentFormId');
    $data['customfieldidval'] = $customfieldid;
    $where            = array('id'=>$customfieldid);
    //get data form custome form field table
    $data['eventregistrants'] = $this->ci->common_model->getDataFromTabel('custom_form_fields','*',$where,'','id','ASC','1');
    if(!empty($data['eventregistrants'])){
      $data['eventregistrants'] = $data['eventregistrants'][0];
    }

    $this->ci->load->view('invitations/add_edit_personal_form_field_popup',$data); 
  }
  
  /*
   * @access:    public
   * @description: This function is used to edit and update custome field in personal form
   * @return void
   * 
   */ 
   
  public function addeditcustomefieldsave(){

    //set validation
    $this->ci->form_validation->set_rules('fieldName', 'field name', 'trim|required');
  
    if ($this->ci->form_validation->run($this->ci)){
      $eventId      = $this->ci->input->post('eventId');
      $fieldName      = $this->ci->input->post('fieldName');
      $fieldRegistantId   = ($this->ci->input->post('fieldRegistantId')!="") ? $this->ci->input->post('fieldRegistantId') : $this->ci->input->post('registrantId');
      $customFieldId    = $this->ci->input->post('customFieldId');
      $fieldType      = $this->ci->input->post('fieldType');
      $defaultValue   = NULL;
      if($this->ci->input->post('defaultValue') && ($fieldType=="selectbox" || $fieldType=="radio")){
        $defaultValue = json_encode($this->ci->input->post('defaultValue'),JSON_FORCE_OBJECT);
      }
      //this value commmon for add and edit case
      $insertData['field_name']     = strtolower($fieldName);
      $insertData['field_type']     = $fieldType;
      $insertData['default_value']  = $defaultValue;
      $insertData['form_id']      = ($this->ci->input->post('form_id')!="") ? $this->ci->input->post('form_id') : "1";
      $insertData['event_id']     = $eventId;
      $insertData['registrant_id']  = $fieldRegistantId;
      
      
      // udpate custome field data
      if($customFieldId > 0){
        $where = array('id'=>$customFieldId);
        $this->ci->common_model->updateDataFromTabel('custom_form_fields',$insertData,$where);
        $msg = lang('event_msg_custom_field_updated');
        $returnId = $customFieldId;
        $saveType = 'update';
      }else{
        //this value set only create field time
        $insertData['field_status'] = '1';
        $insertData['is_editable'] = '1';
        $insertData['field_create_by'] = '0';
        // data custome field data
        $insertId = $this->ci->common_model->addDataIntoTabel('custom_form_fields', $insertData);
        $msg = lang('event_msg_custom_field_add');
        $returnId = $insertId;
        $saveType = 'insert';
      }
      
      
      $fieldsmastervalue = fieldsmastervalue();      
      $returnArray=array('msg'=>$msg,'is_success'=>'true','url'=>$this->ci->uri->uri_string(),'id'=>$returnId,'fieldsmastervalue'=>$fieldsmastervalue,'saveType'=>$saveType); 
      //set_global_messages($returnArray);
      
     //get master field for each event id
     //$whereMsterField = array('event_id'=>$eventId,'registrant_id'=>'0','form_id'=>'4');
     //$data['masterformfielddata'] = getDataFromTabel('custom_form_fields','*',$whereMsterField);
      
      
      echo json_encode($returnArray);
    }else{
      $errors = $this->ci->form_validation->error_array();
      echo json_message('msg',$errors,'is_success','false');
    }
  }
  
  /*
   * @access: public
   * @description: This function is used for customize forms
   * @param: eventId
   * @return: void
   * 
   */ 
  
  public function customizeforms($themeId){
    //current open eventId
    $eventId = currentEventId();
    
    //decode entered eventid
    $eventId = decode($eventId); 
    
    //check is data post 
    if($this->ci->input->post()){
      #echo '<pre>'; print_r($this->ci->input->post()); exit; 
      //call insert and update data function
        $this->insertupdatecolortheme();
    }else{      
      // load registrant form view
      $this->customizeformsviews($themeId);
    }
  }
  
    /*
  *
  * @access: public
    * @description: This function is used to load customize forms details
    * @return: void
    *  
    */
  
  public function customizeformsviews($themeId=''){
      
    $themetype = $this->ci->uri->segment(4); 
    //current open eventId
    $eventId = currentEventId();
    
    //decode entered eventid
    $eventId = decode($eventId); 
    //chheck if eventId is zero 
    if($eventId==0){
      redirect(base_url('dashboard'));
    }
    
    $userId = isLoginUser();
    $data['userId'] = $userId;
    $data['eventId'] =$eventId;
    $data['popupHeaderBg'] ='bg_807f83'; // popup header bg class
    
    /*
     * get event details
     * @param eventid, userid
     */  
    $data['eventdetails'] = $this->ci->event_model->geteventdetails($eventId,$userId);
    /*
     * get event default theme details
     */  
    $data['eventthemesdetails'] = $this->ci->event_model->geteventthemesdetails();
    
    /*
     * get event used theme details
     */  
    $data['eventusedthemesdetails'] = $this->ci->event_model->geteventusedthemesdetails($eventId,$userId);
    
    
    $data['eventThemeSavesUseData'] = $this->ci->event_model->getThemeDataFromTable($eventId, $userId, $themeId);
    if(!empty($data['eventThemeSavesUseData'])) {
        $data['eventThemeSavesUseData'] = $data['eventThemeSavesUseData'][0];
    }else {
        $data['eventThemeSavesUseData'] ='';
    }
    if(!empty($themeId)) {
        $data['loadSelectedTheme']  = $themeId;
    }else {
        $data['loadSelectedTheme']  = '';
    }
    
    /*
     * get event theme fonts
     * fonts tableeventusedthemesdetails
     */  
    $data['fontslist'] = $this->ci->event_model->geteventthemesfonts();
    /*
     * get event theme form data
     * @param eventid
     */  
    $themewhere = array('event_id'=>$eventId);
    $data['allSavedThemes'] = $this->ci->common_model->getDataFromTabel('event_themes_data','',$themewhere);
    
    $data['allDefaultThemes'] = $this->ci->common_model->getDataFromTabel('event_default_themes','*','','','id','ASC');
    
    $where = array('id'=>$eventId);
    $whereEvent = array('event_id'=>$eventId);
    $data['eventsponsorslogos'] = $this->ci->common_model->getDataFromTabel('event_sponsors_logos','*',$whereEvent);
    $data['eventdata'] = $this->ci->common_model->getDataFromTabel('event','event_title',$where,'','id','ASC');
    
    //get invitation data
    $wherei = array('event_id'=>$eventId);
    $data['eventinvitations'] = $this->ci->common_model->getDataFromTabel('event_invitations','*',$wherei,'','invitations_order','ASC');
    
    //$data['customizeformsdata'] = $this->ci->common_model->getDataFromTabel('customize_forms', '*', $whereEvent, '', 'id', 'ASC');
    $themeId = decode($themeId);
    
  
    
    if($themetype==1){
        $whereEvent = array('id'=>$themeId,'event_id'=>$eventId);
    }else {
            $whereEvent = array('event_id'=>$eventId);
    }
    $dataThemes = $this->ci->common_model->getDataFromTabel('event_themes_data', '*', $whereEvent, '', 'last_modified', 'DESC',1);
	
    if($themetype==2){
        $whereEvent1 = array('id'=>$themeId);
        $defaultThemeData = $this->ci->common_model->getDataFromTabel('event_default_themes','*',$whereEvent1,'','id');
    } else{
        $whereEvent1 = array('event_theme_order'=>1);
        $defaultThemeData = $this->ci->common_model->getDataFromTabel('event_default_themes','*',$whereEvent1,'','id','ASC');
    }
    
    if($themetype==1){
        $data['singleThemeData'] = $dataThemes;
    } else if($themetype==2){
        $data['singleThemeData'] = $defaultThemeData;
    } else{
           if(count($dataThemes) > 0 && !empty($dataThemes)) {
                $data['singleThemeData'] = $dataThemes;
            }else { 
                $data['singleThemeData'] = $defaultThemeData;
            }
    }
    
    
    //~ if(count($dataThemes) > 0 && !empty($dataThemes)) {
        //~ $data['singleThemeData'] = $dataThemes;
    //~ }else {
        //~ $whereEvent1 = array('event_theme_order'=>1);
        //~ $data['singleThemeData'] = $this->ci->common_model->getDataFromTabel('event_default_themes','*',$whereEvent1,'','id','ASC');
    //~ }
    
    $data['previousthemes'] = $this->ci->event_model->getpreviouscolourthemes($eventId, $userId);
    $this->ci->template->load('template','customizeforms/customize_forms',$data,TRUE);
  }
  
  /*
   * @access: private
   * @description: This function is used to insert update color theme
   * @return void
   */ 
   
  private function insertupdatecolortheme() {
      
        $getData = array();
        
        $imagenameheader = date('Ymd').rand(999,99999);
        /* Get header plugin image data */
        $baseimage = $this->ci->input->post('baseimage'); 
        $img = str_replace('data:image/png;base64,', '', $baseimage);
        file_put_contents('./media/event_header/header_'.$imagenameheader.'.png', base64_decode($img));
        
        $getData['header_image_name'] = 'header_'.$imagenameheader.'.png';
            
        
        $is_ajax_request = $this->ci->input->is_ajax_request();
        //echo '<pre>'; print_r($this->ci->input->post());exit; 
        $headerImageData['x_axis'] = $this->ci->input->post('x_axis'); 
        $headerImageData['y_axis'] = $this->ci->input->post('y_axis'); 
        $headerImageData['scale'] = $this->ci->input->post('scale'); 
        $getData['header_image_size'] = json_encode($headerImageData); 
        
        $eventId = $this->ci->input->post('eventId'); 
        $logo_image_status = $this->ci->input->post('logo_image_status'); 
        $is_theme_used_data = $this->ci->input->post('is_theme_used_data'); 
        /* Set as default  theme used = 1 */
        $is_theme_used_data = 1;
        //save header image and logo image
        $logo_gallery_image = $this->ci->input->post('logo_gallery_image');
        $gallery_action = $this->ci->input->post('gallery_action');
        if(!empty($logo_gallery_image)) {            
            $getData['logo_image'] = $logo_gallery_image;
            $getData['logo_image_path'] = '/media/gallery/';
            $dirSavePath = '/media/gallery/';
            $fileName = $logo_gallery_image;
            $saveStatus = true;
        }
        
        $header_gallery_image = $this->ci->input->post('header_gallery_image');
        //$gallery_action = $this->ci->input->post('gallery_action');
        if(!empty($header_gallery_image)) {            
            
            $getData['header_image'] = $header_gallery_image;
            $getData['header_image_path'] = '/media/gallery/';
            $dirSavePath = '/media/gallery/';
            $fileName = $header_gallery_image;
            $saveStatus = true;
        }
        
        if(!empty($_FILES['headerImage']['tmp_name'])) {
            $dirUploadPath = './media/event_header/';
            $dirSavePath = '/media/event_header/';
            $config['upload_path'] = $dirUploadPath;
            $config['allowed_types'] = 'gif|jpg|png';
            $config['encrypt_name'] = TRUE;
            $this->ci->load->library('upload', $config);
            $this->ci->upload->initialize($config);
            if (! $this->ci->upload->do_upload('headerImage'))
            {
                $error = array('error' => $this->ci->upload->display_errors());
                print_r($error);die;
            }else{
                $uploadData = $this->ci->upload->data();   
                $fileName = $uploadData['file_name'];     
            }            
            $getData['header_image'] = $fileName;
            $getData['header_image_path'] = $dirSavePath;
        }
        if(!empty($_FILES['logoImage']['tmp_name'])) {
            
            $dirUploadPathLogo = './media/event_logo/';
            $dirSavePathLogo = '/media/event_logo/';

            $config1['upload_path'] = $dirUploadPathLogo;
            $config1['allowed_types'] = 'gif|jpg|png';
            $config1['encrypt_name'] = TRUE;
            $this->ci->load->library('upload', $config1);
            $this->ci->upload->initialize($config1);
            if (! $this->ci->upload->do_upload('logoImage'))
            {
                $error = array('error' => $this->ci->upload->display_errors());
                print_r($error);die;
            }else{
                $uploadDataLogo = $this->ci->upload->data();   
                $fileNameLogo = $uploadDataLogo['file_name'];  
            }
            $getData['logo_image'] = $fileNameLogo;
            $getData['logo_image_path'] = $dirSavePathLogo;
        }
        
        $getData['event_theme_name'] = $this->ci->input->post('event_theme_name');
        //set theme use status
        $getData['is_theme_used']    = $is_theme_used_data;
        $userId = isLoginUser();
        $getData['user_id']    = $userId;
        $eventId = $this->ci->input->post('eventId');
        /* get primary key */
        $savedDataId = $this->ci->input->post('data_id');
        /* get theme type */
        $theme_type = $this->ci->input->post('theme_type');
        $getData['logo_image_status'] = $logo_image_status;
        $getData['event_theme_header_bg'] = $this->ci->input->post('colour_header');
        $getData['is_header_or_bgcolor'] = $this->ci->input->post('is_header_or_bgcolor');
        $getData['event_theme_screen_bg'] = $this->ci->input->post('event_theme_screen_bg');
        $getData['primary_color'] = $this->ci->input->post('primary_color');
        $getData['secondary_color'] = $this->ci->input->post('secondary_color');
        $getData['event_theme_link_highlighted_color'] = $this->ci->input->post('event_theme_link_highlighted_color');
        $getData['event_theme_text_color'] = $this->ci->input->post('event_theme_text_color');
        $fontval = $this->ci->input->post('font_package_value');
        if(!empty($fontval)) {
            $getData['font_package'] = $this->ci->input->post('font_package_value');
        }
        $getData['event_id'] = $this->ci->input->post('eventId');
        $getData['user_id'] = $userId;
        $getData['last_modified'] = date('Y-m-d H:i:s');
        $logoData['image_position_top'] = $this->ci->input->post('image_position_top');    
        $logoData['image_position_left'] = $this->ci->input->post('image_position_left');    
        $logoData['image_width'] = $this->ci->input->post('image_width');    
        $logoData['image_height'] = $this->ci->input->post('image_height');    
        $getData['logo_size'] = json_encode($logoData);   
        
        /* sponsor canvas object as json */
        $sponsor_img = $this->ci->input->post('sponsor_img');
        $sponsor_canvas_data = $this->ci->input->post('sponsor_canvas_data');
        
		if(!empty($sponsor_canvas_data)){
			$getData['sponsor_image_object'] = $sponsor_canvas_data;
		}
            
      
        $whereIsused = array('event_id'=>$eventId,'user_id'=> $userId);
        $getThemeResult = $this->ci->common_model->getDataFromTabel('event_themes_data','',$whereIsused);
        if(!empty($getThemeResult)){
         
            $setData['is_theme_used'] =0;
            $where1 = array('event_id'=>$eventId,'user_id'=> $userId);
            if($is_theme_used_data==1){
                $this->ci->common_model->updateDataFromTabel('event_themes_data',$setData,$where1);
            }
        }
        
        
        //$where1 = array('event_id'=>$eventId,'user_id'=> $userId,'theme_id'=> $themeTypeId);
        $where1 = array('id'=> $savedDataId);
        $getResult = $this->ci->common_model->getDataFromTabel('event_themes_data','',$where1);
        if(!empty($getResult)){
            
            $headerId = $getResult[0]->id;
            //$where2 = array('event_id'=>$eventId,'theme_id'=> $themeTypeId);
            $this->ci->common_model->updateDataFromTabel('event_themes_data',$getData,$where1);
        }
        else{
            // insert data id by id
            if($theme_type == 2) {
                $headerId = $this->ci->common_model->addDataIntoTabel('event_themes_data', $getData);
            }            
        }
        /* sponsor image object as json */
         if(!empty($sponsor_img)){
			
                list($type, $sponsor_img) = explode(';', $sponsor_img); 
                list(, $sponsor_img)      = explode(',', $sponsor_img);
                $sponsor_img = str_replace(' ', '+', $sponsor_img);
                $sponsor_img = base64_decode($sponsor_img); // base 64 decoding
                
                if (!file_exists('./media/events_sponsors_logo/user_'.$userId.'/event_'.$eventId.'/sponsors_'.$headerId.'.png')) {
                    mkdir('./media/events_sponsors_logo/user_'.$userId,0777);
                    chmod('./media/events_sponsors_logo/user_'.$userId,0777);
                    
                    mkdir('./media/events_sponsors_logo/user_'.$userId.'/event_'.$eventId,0777);
                    chmod('./media/events_sponsors_logo/user_'.$userId.'/event_'.$eventId,0777);
                }
                @unlink('./media/events_sponsors_logo/user_'.$userId.'/event_'.$eventId.'/sponsors_'.$headerId.'.png');
                file_put_contents('./media/events_sponsors_logo/user_'.$userId.'/event_'.$eventId.'/sponsors_'.$headerId.'.png', $sponsor_img);
                
            }
            
            //~ 
            //~ $baseimage = $this->ci->input->post('baseimage'); 
            //~ $img = str_replace('data:image/png;base64,', '', $baseimage);
            //~ file_put_contents('./media/event_header/header_'.$savedDataId.'.png', base64_decode($img));
			
			//Set flash data
			$this->ci->session->set_flashdata('themestatus', 'save');
			
            $headerId = encode($headerId);
            $msg = lang('event_msg_colour_theme_apply');
              if($is_ajax_request){
                //echo json_message('msg',$msg);
                $action = $this->ci->input->post('next_save');
                if($action == 'next_save'){
                  $returnArray=array('msg'=>$msg,'is_success'=>'true','url'=>$this->ci->uri->uri_string());   
                } else {
                  $returnArray=array('msg'=>$msg,'is_success'=>'true','url'=>$this->ci->uri->uri_string());   
                }
                
                echo json_encode($returnArray);
                //set_global_messages($msgArray);
              }else{
                $msgArray = array('msg'=>$msg,'is_success'=>'true');
                set_global_messages($msgArray);
                redirect('event/customizeforms/');
              }
   
  } 
    /*
   * @access:    public
   * @description: This function is used to delete type of registration 
   * @return void
   * 
   */   
  
  
  public function deleteeventinvitation(){
    
    $deleteid   = $this->ci->input->post('deleteId');
    //delete registant record from other table
    $where    = array('registrant_id'=>$deleteid);
    $this->ci->common_model->deleteRowFromTabel('custom_form_fields',$where);
    
    //delete registrant from table
    $where = array('id'=>$deleteid);
    $this->ci->common_model->deleteRowFromTabel('event_invitations',$where);
    $msg = lang('event_msg_type_invitation_deleted');
    $returnArray=array('msg'=>$msg,'is_success'=>'true');
    //set_global_messages($returnArray);
    echo json_encode($returnArray);
  }
  
  // ------------------------------------------------------------------------ 
  
  /*
   * @access:    public
   * @description: This function is used to duplicate type of registration 
   * @return void
   * 
   */ 
   
  public function duplicateeventinvitation(){
    
    if($this->ci->input->post()){
      $invitationName   = $this->ci->input->post('invitationName');
      $invitationTypeId = $this->ci->input->post('invitationTypeId');
      $eventId      = $this->ci->input->post('eventId');
      
      //get data form registrants
      $where = array('id'=>$invitationTypeId);
      $getInvitationData = $this->ci->common_model->getDataFromTabel('event_invitations','*',$where);
        
      
      if(!empty($getInvitationData)){
        $getInvitationData = $getInvitationData[0];
        foreach($getInvitationData as $key => $value){
          if($key=="invitation_name"){
            $insertData[$key] =  $invitationName; 
          }else{
            $insertData[$key] =  $value; 
          }
        }
        unset($insertData['id']);
        unset($insertData['invitation_limit']);
        //registrant type add
        $invitationId = $this->ci->common_model->addDataIntoTabel('event_invitations', $insertData);
        
      $msg = lang('event_msg_type_invitation_duplicate_created');
      //This function is used to add form fields 
      $this->addformfields($eventId,'4',$invitationId);
      
      $returnArray=array('msg'=>$msg,'is_success'=>'true');
      //set_global_messages($returnArray);
      echo json_encode($returnArray);
    }else{
      redirect(base_url('dashboard'));
    }
  }
}


 /*
     * @access: public
     * @description: This function is used to  delete event
     * @param : eventId
     * 
     */
    
    public function deleteevent(){    
      $eventId  = $this->ci->input->post('deleteId');
      
      $whereCondi = array('event_id'=>$eventId);   
      $this->ci->common_model->deleteRowFromTabel('event_contact_person',$whereCondi);
      
      $this->ci->common_model->deleteRowFromTabel('event_social_media',$whereCondi);
      
      $this->ci->common_model->deleteRowFromTabel('event_dietary',$whereCondi);
      
      $this->ci->common_model->deleteRowFromTabel('custom_form_fields',$whereCondi);
     
      $getResult = $this->ci->common_model->getDataFromTabel('event_term_conditions','document_file,document_path',$whereCondi);
      if(!empty($getResult)){
        //delte image from directory
        findFileNDelete($getResult[0]->document_path, $getResult[0]->document_file);
      }
      $this->ci->common_model->deleteRowFromTabel('event_term_conditions',$whereCondi);
      
      $this->ci->common_model->deleteRowFromTabel('event_categories',$whereCondi);    
      
      $this->ci->common_model->deleteRowFromTabel('corporate_package_benefit',$whereCondi);
      
      $this->ci->common_model->deleteRowFromTabel('corporate_purchase_item',$whereCondi);
      
      $this->ci->common_model->deleteRowFromTabel('side_event',$whereCondi);
      
      
      $getbkResult = $this->ci->common_model->getDataFromTabel('breakouts','','',$whereCondi);
      foreach($getbkResult as $bkid) {
          $wherebkCondi = array('breakout_id'=>$bkid->id);
          $this->ci->common_model->deleteRowFromTabel('breakout_session',$wherebkCondi);
          $this->ci->common_model->deleteRowFromTabel('breakout_stream',$wherebkCondi);
      }
      //print_r($getbkResult);die;
      
      $this->ci->common_model->deleteRowFromTabel('breakouts',$whereCondi);
            
      $getResult = $this->ci->common_model->getDataFromTabel('event_sponsors_logos','logo_file_path,logo_file_name',$whereCondi);
      if(!empty($getResult)){
        //delte image from directory
        findFileNDelete($getResult[0]->logo_file_path, $getResult[0]->logo_file_name);
      }
      $where = array('id'=>$eventId);
      $this->ci->common_model->deleteRowFromTabel('event',$where);
      $msg = lang('event_msg_event_deleted_successfully');
      $msgArray = array('msg'=>$msg,'is_success'=>'true');
      //set_global_messages($msgArray);
      echo json_message('msg',$msg,'is_success','true');
     } 
    
    public function addediteventtheme() {
        
        $is_ajax_request = $this->ci->input->is_ajax_request();
        $eventId = $this->ci->input->post('eventId'); 
        $is_theme_used_data = $this->ci->input->post('is_theme_used_data'); 
        $is_theme_used_data = 1;
        //save header image and logo image
        $logo_gallery_image = $this->ci->input->post('logo_gallery_image');
        $gallery_action = $this->ci->input->post('gallery_action');
        if(!empty($gallery_action) && !empty($logo_gallery_image)) {            
            $getData['logo_image'] = $logo_gallery_image;
            $getData['logo_image_path'] = '/media/gallery/';
            $dirSavePath = '/media/gallery/';
            $fileName = $logo_gallery_image;
            $saveStatus = true;
        }
        
        $header_gallery_image = $this->ci->input->post('header_gallery_image');
        //$gallery_action = $this->ci->input->post('gallery_action');
        if(!empty($gallery_action) && !empty($header_gallery_image)) {            
            
            $getData['header_image'] = $header_gallery_image;
            $getData['header_image_path'] = '/media/gallery/';
            $dirSavePath = '/media/gallery/';
            $fileName = $header_gallery_image;
            $saveStatus = true;
        }
      
        if(!empty($_FILES['headerImage']['tmp_name'])) {
            $dirUploadPath = './media/event_header/';
            $dirSavePath = '/media/event_header/';
            $config['upload_path'] = $dirUploadPath;
            $config['allowed_types'] = 'gif|jpg|png';
            $config['encrypt_name'] = TRUE;
            $this->ci->load->library('upload', $config);
            $this->ci->upload->initialize($config);
            if (! $this->ci->upload->do_upload('headerImage'))
            {
                $error = array('error' => $this->ci->upload->display_errors());
                print_r($error);die;
            }else{
                $uploadData = $this->ci->upload->data();   
                $fileName = $uploadData['file_name'];     
            }            
            $getData['header_image'] = $fileName;
            $getData['header_image_path'] = $dirSavePath;
        }
        if(!empty($_FILES['logoImage']['tmp_name'])) {
            
            $dirUploadPathLogo = './media/event_logo/';
            $dirSavePathLogo = '/media/event_logo/';

            $config1['upload_path'] = $dirUploadPathLogo;
            $config1['allowed_types'] = 'gif|jpg|png';
            $config1['encrypt_name'] = TRUE;
            $this->ci->load->library('upload', $config1);
            $this->ci->upload->initialize($config1);
            if (! $this->ci->upload->do_upload('logoImage'))
            {
                $error = array('error' => $this->ci->upload->display_errors());
                print_r($error);die;
            }else{
                $uploadDataLogo = $this->ci->upload->data();   
                $fileNameLogo = $uploadDataLogo['file_name'];  
            }
            $getData['logo_image'] = $fileNameLogo;
            $getData['logo_image_path'] = $dirSavePathLogo;
        }
        
        $getData['event_theme_name'] = $this->ci->input->post('event_theme_name');
        //set theme use status
        $getData['is_theme_used']    = $is_theme_used_data;
        $userId = isLoginUser();
        $getData['user_id']    = $userId;
        $eventId = $this->ci->input->post('eventId');
        /* get primary key */
        $savedDataId = $this->ci->input->post('data_id');
        /* get theme type */
        $theme_type = $this->ci->input->post('theme_type');
        
        $getData['event_theme_header_bg'] = $this->ci->input->post('colour_header');
        
        $getData['event_theme_screen_bg'] = $this->ci->input->post('event_theme_screen_bg');
        $getData['primary_color'] = $this->ci->input->post('primary_color');
        $getData['secondary_color'] = $this->ci->input->post('secondary_color');
        $getData['event_theme_link_highlighted_color'] = $this->ci->input->post('event_theme_link_highlighted_color');
        //~ $getData['event_theme_link_color'] = $this->ci->input->post('event_theme_link_color');
        $getData['event_theme_text_color'] = $this->ci->input->post('event_theme_text_color');
        $getData['font_package'] = $this->ci->input->post('font_package');
        //$getData['is_theme_used'] = $this->ci->input->post('is_theme_used');
        $getData['event_id'] = $this->ci->input->post('eventId');
        //$getData['theme_id'] = $themeTypeId;
        $getData['user_id'] = $userId;
        $getData['last_modified'] = date('Y-m-d H:i:s');
        
        $logoData['image_position_top'] = $this->ci->input->post('image_position_top');    
        $logoData['image_position_left'] = $this->ci->input->post('image_position_left');    
        $logoData['image_width'] = $this->ci->input->post('image_width');    
        $logoData['image_height'] = $this->ci->input->post('image_height');    
        $getData['logo_size'] = json_encode($logoData);   
        
        /* sponsor canvas object as json */
        $sponsor_img = $this->ci->input->post('sponsor_img');
        $sponsor_canvas_data = $this->ci->input->post('sponsor_canvas_data');
        
		if(!empty($sponsor_canvas_data)){
			$getData['sponsor_image_object'] = $sponsor_canvas_data;
		}
            
       
        $whereIsused = array('event_id'=>$eventId,'user_id'=> $userId);
        $getThemeResult = $this->ci->common_model->getDataFromTabel('event_themes_data','',$whereIsused);
        if(!empty($getThemeResult)){
         
            $setData['is_theme_used'] =0;
            $where1 = array('event_id'=>$eventId,'user_id'=> $userId);
            $this->ci->common_model->updateDataFromTabel('event_themes_data',$setData,$where1);
        }
        
        
        //$where1 = array('event_id'=>$eventId,'user_id'=> $userId,'theme_id'=> $themeTypeId);
        $where1 = array('id'=> $savedDataId);
        $getResult = $this->ci->common_model->getDataFromTabel('event_themes_data','',$where1);
        if(!empty($getResult)){
            
            $headerId = $getResult[0]->id;
            //$where2 = array('event_id'=>$eventId,'theme_id'=> $themeTypeId);
            $this->ci->common_model->updateDataFromTabel('event_themes_data',$getData,$where1);
        }
        else{
            // insert data id by id
            if($theme_type == 2) {
                $headerId = $this->ci->common_model->addDataIntoTabel('event_themes_data', $getData);
            }            
        }
        /* sponsor image object as json */
         if(!empty($sponsor_img)){
			
                list($type, $sponsor_img) = explode(';', $sponsor_img); 
                list(, $sponsor_img)      = explode(',', $sponsor_img);
                $sponsor_img = str_replace(' ', '+', $sponsor_img);
                $sponsor_img = base64_decode($sponsor_img); // base 64 decoding
                
                if (!file_exists('./media/events_sponsors_logo/user_'.$userId.'/event_'.$eventId.'/sponsors_'.$headerId.'.png')) {
                    mkdir('./media/events_sponsors_logo/user_'.$userId,0777);
                    chmod('./media/events_sponsors_logo/user_'.$userId,0777);
                    
                    mkdir('./media/events_sponsors_logo/user_'.$userId.'/event_'.$eventId,0777);
                    chmod('./media/events_sponsors_logo/user_'.$userId.'/event_'.$eventId,0777);
                }
                @unlink('./media/events_sponsors_logo/user_'.$userId.'/event_'.$eventId.'/sponsors_'.$headerId.'.png');
                file_put_contents('./media/events_sponsors_logo/user_'.$userId.'/event_'.$eventId.'/sponsors_'.$headerId.'.png', $sponsor_img);
                
            }
        
            $headerId = encode($headerId);
            $msg = lang('event_msg_colour_theme_apply');
              if($is_ajax_request){
                //echo json_message('msg',$msg);
                $returnArray=array('msg'=>$msg,'is_success'=>'true','url'=>$this->ci->uri->uri_string()); 
                echo json_encode($returnArray);
                //set_global_messages($msgArray);
              }else{
                $msgArray = array('msg'=>$msg,'is_success'=>'true');
                set_global_messages($msgArray);
                redirect('event/customizeforms/');
              }
    }
    
    public function add_edit_custom_theme_popup()
    {
        $data['eventId']  = $this->ci->input->post('eventId');
        $data['themeid']  = $this->ci->input->post('themeid');
        $data['theme_name']  = $this->ci->input->post('theme_name');
        $data['colorThemedata'] = $this->ci->input->post();
        
        /*
         * get event details
         * @param eventid, userid
         */  
        $eventId = $this->ci->input->post('eventId'); 
        $userId  = isLoginUser(); 
        $data['eventdetails'] = $this->ci->event_model->geteventdetails($eventId,$userId);
        $whereEvent = array('event_id'=>$eventId);
        $data['customizeformsdata'] = $this->ci->common_model->getDataFromTabel('customize_forms', '*', $whereEvent, '', 'id', 'ASC');
        $this->ci->load->view('customizeforms/form_customize_color_theme_popup_zoom',$data); 
    }
    
    public function use_custom_theme_apply() {
        //print_r($_FILES);
        $data['eventId']      = $this->ci->input->post('eventId');
        $data['themeid']  = $this->ci->input->post('themeid');
        $data['colorThemedata'] = $this->ci->input->post();
        $eventId = $this->ci->input->post('eventId'); 
        $userId  = isLoginUser(); 
        $data['eventdetails'] = $this->ci->event_model->geteventdetails($eventId,$userId);
        
        $this->ci->load->view('customizeforms/form_customize_color_theme_popup',$data); 
    }
    
    public function saveCanvasObject(){
    
		$userId = isLoginUser();
		$eventId = currentEventId();
		$eventId =  decode($eventId);
		$post = $this->ci->input->post();
		
		if(!empty($post['myImage'])){
			$data = $post['myImage'];
			list($type, $data) = explode(';', $data); 
			list(, $data)      = explode(',', $data);
			$data = str_replace(' ', '+', $data);
			$data = base64_decode($data); // base 64 decoding
			
			if (!file_exists('./media/events_sponsors_logo/user_'.$userId.'/event_'.$eventId.'/sponsors.png')) {
				mkdir('./media/events_sponsors_logo/user_'.$userId,0777);
				chmod('./media/events_sponsors_logo/user_'.$userId,0777);
				
				mkdir('./media/events_sponsors_logo/user_'.$userId.'/event_'.$eventId,0777);
				chmod('./media/events_sponsors_logo/user_'.$userId.'/event_'.$eventId,0777);
			}
			@unlink('./media/events_sponsors_logo/user_'.$userId.'/event_'.$eventId.'/sponsors.png');
			file_put_contents('./media/events_sponsors_logo/user_'.$userId.'/event_'.$eventId.'/sponsors.png', $data);
			echo json_encode(array('img_url'=>base_url().'media/events_sponsors_logo/user_'.$userId.'/event_'.$eventId.'/sponsors.png'));
		}
		if(!empty($post['value'])){
			$updateData['event_id'] = $eventId;
			$updateData['sponsor_image_object'] = $post['value'];
			$where = array('event_id'=>$eventId);
			$getResult = $this->ci->common_model->getDataFromTabel('customize_forms','id',$where);
			if(!empty($getResult)){
				// update data id by id
				$headerId = $getResult[0]->id;
				$upWhere = array('id'=>$headerId);
				$this->ci->common_model->updateDataFromTabel('customize_forms',$updateData,$upWhere);
				$saveStatus = true;
			}else{
				// insert data id by id
				$headerId = $this->ci->common_model->addDataIntoTabel('customize_forms', $updateData);
				$saveStatus = true;
			} 
			echo json_encode(array('id'=>$headerId,'fileId'=>$fileId));
		}
  }
  
  //for preview form
   public function customizeformpreview($themeId=''){

    $themetype = $this->ci->uri->segment(4); 
    //current open eventId
    $eventId = currentEventId();
    
    //decode entered eventid
    $eventId = decode($eventId); 
    //chheck if eventId is zero 
    if($eventId==0){
      redirect(base_url('dashboard'));
    }
    
    $userId = isLoginUser();
    $data['userId'] = $userId;
    $data['eventId'] =$eventId;
    $data['popupHeaderBg'] ='bg_807f83'; // popup header bg class
    
    /*
     * get event details
     * @param eventid, userid
     */  
    $data['eventdetails'] = $this->ci->event_model->geteventdetails($eventId,$userId);
    /*
     * get event default theme details
     */  
    $data['eventthemesdetails'] = $this->ci->event_model->geteventthemesdetails();
    
    /*
     * get event used theme details
     */  
    $data['eventusedthemesdetails'] = $this->ci->event_model->geteventusedthemesdetails($eventId,$userId);
    
    
    $data['eventThemeSavesUseData'] = $this->ci->event_model->getThemeDataFromTable($eventId, $userId, $themeId);
    if(!empty($data['eventThemeSavesUseData'])) {
        $data['eventThemeSavesUseData'] = $data['eventThemeSavesUseData'][0];
    }else {
        $data['eventThemeSavesUseData'] ='';
    }
    if(!empty($themeId)) {
        $data['loadSelectedTheme']  = $themeId;
    }else {
        $data['loadSelectedTheme']  = '';
    }
    
    /*
     * get event theme fonts
     * fonts tableeventusedthemesdetails
     */  
    $data['fontslist'] = $this->ci->event_model->geteventthemesfonts();
    /*
     * get event theme form data
     * @param eventid
     */  
    $themewhere = array('event_id'=>$eventId);
    $data['allSavedThemes'] = $this->ci->common_model->getDataFromTabel('event_themes_data','',$themewhere);
    
    $data['allDefaultThemes'] = $this->ci->common_model->getDataFromTabel('event_default_themes','*','','','id','ASC');
    
    $where = array('id'=>$eventId);
    $whereEvent = array('event_id'=>$eventId);
    $data['eventsponsorslogos'] = $this->ci->common_model->getDataFromTabel('event_sponsors_logos','*',$whereEvent);
    $data['eventdata'] = $this->ci->common_model->getDataFromTabel('event','event_title',$where,'','id','ASC');
    
    //get invitation data
    $wherei = array('event_id'=>$eventId);
    $data['eventinvitations'] = $this->ci->common_model->getDataFromTabel('event_invitations','*',$wherei,'','invitations_order','ASC');
    
    //$data['customizeformsdata'] = $this->ci->common_model->getDataFromTabel('customize_forms', '*', $whereEvent, '', 'id', 'ASC');
    $themeId = decode($themeId);
    
  
    
    if($themetype==1){
        $whereEvent = array('id'=>$themeId,'event_id'=>$eventId);
    }else {
            $whereEvent = array('event_id'=>$eventId);
    }
    $dataThemes = $this->ci->common_model->getDataFromTabel('event_themes_data', '*', $whereEvent, '', 'last_modified', 'DESC',1);
	
    if($themetype==2){
        $whereEvent1 = array('id'=>$themeId);
        $defaultThemeData = $this->ci->common_model->getDataFromTabel('event_default_themes','*',$whereEvent1,'','id');
    } else{
        $whereEvent1 = array('event_theme_order'=>1);
        $defaultThemeData = $this->ci->common_model->getDataFromTabel('event_default_themes','*',$whereEvent1,'','id','ASC');
    }
    
    if($themetype==1){
        $data['singleThemeData'] = $dataThemes;
    } else if($themetype==2){
        $data['singleThemeData'] = $defaultThemeData;
    } else{
           if(count($dataThemes) > 0 && !empty($dataThemes)) {
                $data['singleThemeData'] = $dataThemes;
            }else { 
                $data['singleThemeData'] = $defaultThemeData;
            }
    }
    
    
    //~ if(count($dataThemes) > 0 && !empty($dataThemes)) {
        //~ $data['singleThemeData'] = $dataThemes;
    //~ }else {
        //~ $whereEvent1 = array('event_theme_order'=>1);
        //~ $data['singleThemeData'] = $this->ci->common_model->getDataFromTabel('event_default_themes','*',$whereEvent1,'','id','ASC');
    //~ }
    
    $whereSocialMedia           = array('event_id' => $eventId);
    $data['eventSocialMedia']   = $this->ci->common_model->getDataFromTabel('event_social_media','*',$whereSocialMedia);
    $data['eventSocialMediaCustom']   = $this->ci->common_model->getDataFromTabel('event_social_media_custom','*',$whereSocialMedia);
    
    $data['previousthemes'] = $this->ci->event_model->getpreviouscolourthemes($eventId, $userId);
    $this->ci->template->load('template','customizeforms/form_previewtheme',$data,TRUE);
  }//end of preview form 
  
  /*
   * @access: public
   * @description: This function is used for confirm detail & checked all required details in forms
   * @param: eventId
   * @return: void
   * 
   */ 
  
  public function confirmdetails()
  {
    //current open eventId
    $eventId = currentEventId();
    
    //decode entered eventid
    $eventId = decode($eventId); 
    
    // load registrant form view
    $this->confirmdetailsviews($eventId);
    
  }
  
   /* get all data related to events for ref- customizeformsviews method */
   public function confirmdetailsviews($eventId="0"){
    
    //chheck if eventId is zero 
    if($eventId==0){
      redirect(base_url('dashboard'));
    }
    
    
    $requiredArrEventSetup = array(
                                'event_title' => 'Event Name', 'short_form_title'=>'Short Form Name' , 'starttime'=>'Start', 
                                'endtime'=>'Finish', 'time_zone'=>'Time Zone', 'number_of_rego'=>'Total Invitation Limit', 
                                'reg_startdate'=>'Invitation Start'
                                );  //'last_reg_date'=>'Invitation Cut Off '
                                
                                
    $requiredArrEventContact = array(
                                'first_name'=>'First Name','last_name'=>'Last Name','email'=>'Email Id','phone1_mobile'=>'Contact','phone1_landline'=>'Contact',
                                );//Event Contact Person
                                
    
    $userId = isLoginUser();
    $data['userId'] = $userId;
    $data['eventId'] =$eventId;
    $data['popupHeaderBg'] ='bg_807f83'; // popup header bg class
    
    /*
     * get event details
     * @param eventid, userid
     */  
     $data['eventdetails'] = $this->ci->event_model->geteventdetails($eventId,$userId);
     //get required fields array
     $dataReq = array();
     foreach($requiredArrEventSetup as $key => $keyValue){
            if(empty($data['eventdetails']->$key) || $data['eventdetails']->$key==NULL || $data['eventdetails']->$key=='0000-00-00 00:00:00'){
                $dataReq[$key] = $keyValue;
            }
     }
     $data['requiredFields'] = $dataReq;
    
    $dataContactReq = array();
    foreach($requiredArrEventContact as $key => $keyValue){
            if(empty($data['eventdetails']->$key) || $data['eventdetails']->$key==NULL || $data['eventdetails']->$key=='0000-00-00 00:00:00'){
                if($key == 'phone1_mobile' || $key == 'phone1_landline')
                {
                    if(!empty($data['eventdetails']->phone1_mobile) || !empty($data['eventdetails']->phone1_landline)){
                    }else{
                        $dataContactReq['phone1_mobile'] = $keyValue;
                    }
                }else{
                    $dataContactReq[$key] = $keyValue;
                }
            }
     }
     $data['requiredFieldsContact'] = $dataContactReq;
    
    
    $whereEvent = array('event_id'=>$eventId);
    $data['eventthemes'] = $this->ci->common_model->getDataFromTabel('event_themes_data', '*', $whereEvent, '', 'last_modified', 'DESC',1);
    
    //get invitation data
    $wherei = array('event_id'=>$eventId);
    $data['eventinvitations'] = $this->ci->common_model->getDataFromTabel('event_invitations','*',$wherei,'','invitations_order','ASC');
    //~ $where = array('id'=>$eventId);
    //~ $whereEvent = array('event_id'=>$eventId);
    //~ $regisWhere = array('event_id'=>$eventId);
    //~ $data['eventdata'] = $this->ci->common_model->getDataFromTabel('event','*',$where,'','id','ASC');
    //~ $data['eventregistrants'] = $this->ci->common_model->getDataFromTabel('event_registrants','*',$regisWhere,'','registrants_order','ASC');
    $this->ci->template->load('template','confirmdetails/confirm_details_free',$data,TRUE);
  }
  
 /*
  * @access:  public
  * @description: This function is used to publish and unpublish event 
  * @return void
  * 
  */ 
  
  public function confirmstatus(){
    //~ $statusValue = $this->ci->input->post('statusValue');
    //~ $eventId   = $this->ci->input->post('eventId');
    //current open eventId
    $eventId = currentEventId();
    //decode entered eventid
    $eventId = decode($eventId); 
    $statusValue = '1';
    //$eventId   = $this->ci->input->post('eventId');
    $updateData = array('is_approved'=>$statusValue); 
    $upWhere = array('id'=>$eventId);
    $status = $this->ci->common_model->updateDataFromTabel('event',$updateData,$upWhere);
    if($statusValue=='1'){
      $msg = lang('event_conf_status_pub_msg');
    }else{
      $msg = lang('event_conf_status_unpub_msg');
    } 
    $msgArray = array('msg'=>$msg,'is_success'=>'true');
    set_global_messages($msgArray);
    redirect(base_url('dashboard'));
    //echo json_message('msg',$msg);
  } 
  
  // new updates
  /*
   * @access: private
   * @description: This function is used to check validate & save general event venue details 
   * @return void
   * 
   */ 
  
  private function eventVenueContactPersonSave(){
    $is_ajax_request = $this->ci->input->is_ajax_request();
    $eventId = $this->ci->input->post('eventId');
    $action = $this->ci->input->post('next_save');
    if($action == 'next_save'){
		#echo '1'; exit;
          $this->_venueContactPersonSave($eventId);
		  $msg = lang('event_msg_general_event_setup_saved');
		  if($is_ajax_request){
			echo json_encode(array('msg'=>$msg,'is_success'=>'true','url'=>'event/eventdetails'));
		  }else{
			$msgArray = array('msg'=>$msg,'is_success'=>'true');
			set_global_messages($msgArray);
			redirect('event/eventdetails/'.encode($eventId));
		  }	
	}
    if ($eventId){
	  $this->_venueContactPersonSave($eventId);
      $msg = lang('event_msg_general_event_setup_saved');
      if($is_ajax_request){
        echo json_encode(array('msg'=>$msg,'is_success'=>'true','url'=>'event/eventdetails'));
      }else{
        $msgArray = array('msg'=>$msg,'is_success'=>'true');
        set_global_messages($msgArray);
        redirect('event/eventdetails/'.encode($eventId));
      }
    }
    else{
      $errors = $this->ci->form_validation->error_array();
      if($is_ajax_request){
        echo json_message('msg',$errors,'is_success','false');
      }else{
        $this->eventshowview($eventId);
      }
    }
  }
  
  /*
   * Save venue Contact
   * Person details
   * @param eventId
   */ 
   private function _venueContactPersonSave($eventId){
    $updateData['venue_contact_firstname']   = $this->ci->input->post('venue_contact_firstname');
    $updateData['venue_contact_lastname']    = $this->ci->input->post('venue_contact_lastname');
    $updateData['venue_contact_email']       = $this->ci->input->post('venue_contact_email');
    
    $updateData['venue_contact_mobile']      = $this->ci->input->post('input_hidden_mobile2');
    $updateData['venue_contact_landline']    = $this->ci->input->post('input_hidden_landline2');
    
    $where = array('id'=>$eventId);
    $this->ci->common_model->updateDataFromTabel('event',$updateData,$where); 
  }
  
  public function publishEventAction(){
    /* Event Status (is_approved)  */
    /* 1 = Publish, 2 = Pending (In case admin will approved), 0 = Unpublish */  
    $event_id       = $this->ci->input->post('event_id');
    $event_title    = $this->ci->input->post('event_title');
    $isEventPublished = 1;
    if(!empty($isEventPublished)){
        /* Update Event Status */
        $this->ci->common_model->updateDataFromTabel('event',array('is_approved'=>'1'),array('id'=>$event_id)); 
        /* Load Email Library */
        $this->ci->load->library('email'); 
        $this->ci->email->from( FROM_EMAIL, FROM_NAME ); // variable define in config  
        /* Get login user details */ 
        
        /* Get admin details */  		
        
        /* set data on template view */
        $data['event_id'] = $event_id;
        $data['event_title'] = $event_title;
        $user_email = $this->ci->session->userdata('userdata')->email;
        if(empty($user_email)){
            $userid=isLoginUser();
            $userData = getUserDataById($userid);
            $user_email = $userData[0]->email;
        }
        
        $subject_admin = 'New event has been published';
        $subject_user  = 'Your event has been published successfully!';
        /* send email notification to admin */
        
         /* Sendgrid */
            $apiKey = ADMINNEWMAN_APIKEY;
            $this->ci->load->helper('sendgrid');
            $admin_templateId = AdminEventTemplateId; //
            $paramArray = array(
            'sub' => array(':event_title' => array($event_title)),
            'filters' => array('templates' => array('settings' => array('enable' => 1, 'template_id' => $admin_templateId)))
            );
            $tempSubject = $subject_admin;
            $templateBody = '.';
            $toId = ADMIN_EMAIL;
            sendgrid_mail_template($admin_templateId,$tempSubject,$templateBody,$toId,$paramArray,$apiKey,eventFromEmail);
        /* 
            $this->ci->email->to(ADMIN_EMAIL);
            $this->ci->email->subject($subject_admin);
            $template_to_admin = $this->ci->load->view('email_template_admin',$data,true);
            $this->ci->email->message($template_to_admin);  
            $this->ci->email->send();
        */
        
        /* send email notification to user */
            $apiKey = ADMINNEWMAN_APIKEY;
            $user_templateId = userEventTemplateId; //
            $paramArray = array(
            'sub' => array(':event_title' => array($event_title)),
            'filters' => array('templates' => array('settings' => array('enable' => 1, 'template_id' => $user_templateId)))
            );
            $tempSubject = $subject_user;
            $templateBody = '.';
            $toId = $user_email;//$admin_email;
            sendgrid_mail_template($user_templateId,$tempSubject,$templateBody,$toId,$paramArray,$apiKey,eventFromEmail);
        /*
            $this->ci->email->to($user_email); 	
            $this->ci->email->subject($subject_user);
            $template_to_user = $this->ci->load->view('email_template_user',$data,true);
            $this->ci->email->message($template_to_user);  
            $this->ci->email->send();
         */
        echo 'success';
    }else{
        echo 'error';
    }
  }
  
  public function uploadtermattachmentterm(){
    $dirUploadPath         = './media/event_attachment/';
    $dirSavePath         = '/media/event_attachment/';
    $uploadData            = $this->ci->process_upload->upload_file($dirUploadPath);
    $eventId           = $_REQUEST['event_id'];
    $updateData['document_file'] = $uploadData['filename'];
    $updateData['document_path'] = $dirSavePath;
    $fileId            = remove_extension($uploadData['filename']);
    // get document id by event id
    $where = array('event_id'=>$eventId);
    $getResult = $this->ci->common_model->getDataFromTabel('event_term_conditions','id',$where);
    // update document id by id
    $termId = $getResult[0]->id;
    $upWhere = array('id'=>$termId);
    $this->ci->common_model->updateDataFromTabel('event_term_conditions',$updateData,$upWhere);
    echo json_encode(array('id'=>$termId,'fileId'=>$fileId));
  } 
  
  
  public function cancelEventAction(){
        /* Event Status (is_approved)  */
        /* 1 = Publish, 2 = Pending (In case admin will approved), 0 = Unpublish */  
        $event_id       = $this->ci->input->post('event_id');
        $event_title       = $this->ci->input->post('event_title');
        $isEventCancel = 1;
        
        /* Update Event Status */
        $this->ci->common_model->updateDataFromTabel('event',array('is_cancelled'=>'1'),array('id'=>$event_id)); 
        $where = array('event_id'=>$event_id);
        $getResult = $this->ci->common_model->getDataFromTabel('invitation_registration','*',$where);
        if($getResult){
            foreach($getResult as $listEmails){
                /* Sendgrid */
                $apiKey = ADMINNEWMAN_APIKEY;
                $this->ci->load->helper('sendgrid');
                //$templateId = '4924157e-892a-4408-87d7-c720fe582f66'; 
                $templateId = '37470d89-35c7-4428-8b5c-38511169d6be'; 
                $paramArray = array(
                'sub' => array(':event_title' => array($event_title)),
                'filters' => array('templates' => array('settings' => array('enable' => 1, 'template_id' => $templateId)))
                );
                $tempSubject = 'Event Cancelled';
                $templateBody = '.';
                $toId = $listEmails->email;
                sendgrid_mail_template($templateId,$tempSubject,$templateBody,$toId,$paramArray,$apiKey,eventFromEmail);
            }
        }
        echo 'success';
        
  }
  
   
}//factory class end


/* End of file factory.php */
/* Location: ./system/application/modules/event/libraries/factoryfree.php */
