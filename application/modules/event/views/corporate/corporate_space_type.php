<?php

if(!empty($spaceTypeRecord)){
    $corporateFloorTypeId           = encode($spaceTypeRecord->floor_plan_type_id); 
    $spaceName                  = $spaceTypeRecord->space_type;
    $spaceAvailablevalue          = $spaceTypeRecord->total_space_available;
    $availableBooths            = (!empty($spaceTypeRecord->available_booth)) ? unserialize($spaceTypeRecord->available_booth) : array();
}else{
   $corporateFloorTypeId   = "0"; 
   $spaceName          = "";
   $spaceAvailablevalue    = "";
   $availableBooths      = array();
}



$formCorporateSpaceType = array(
    'name'   => 'formCorporateSpaceType',
    'id'   => 'formCorporateSpaceType',
    'method' => 'post',
    'class'  => 'wpcf7-form',
    'data-parsley-validate' => '',
); 
// get avaliable space
$availableFloorSpace = getCorporateRemainingSpacesAvailable(); 
  
$availableFloorSpaceHidden = array(
    'name'  => 'available_space',
    'id'  => 'available_space',
    'type'  => 'hidden',
    'value' => $availableFloorSpace
);  

$spaceTypeName = array(
    'name'  => 'space_name',
    'value' => $spaceName,
    'id'  => 'space_name',
    'type'  => 'text',
    'required'  => '',
    'class' =>'small',
    'autocomplete' => 'off',
    'data-parsley-error-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
);  


$maxSpaceAvailable = $availableFloorSpace+1;
$spaceAvailable = array(
    'name'  => 'space_ava',
    'value' => $spaceAvailablevalue,
    'id'  => 'space_ava',
    'type'  => 'number',
    'min'   => '1',
    'max' => ($corporateFloorTypeId=="0") ? $maxSpaceAvailable : ($availableFloorSpace+$spaceAvailablevalue),
    'step'  => '1', 
    'readonly'=>'readonly',
    'required'  => '',
    'class' =>'small dark ',
    'autocomplete' => 'off',
    'data-parsley-error-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
);

$spaceTypeNo = array(
    'name'  => 'space[]',
    'type'  => 'checkbox',
    'required'  => '',
    'class'   =>'small',
    'autocomplete' => 'off',
    'data-parsley-error-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'change',
);  


?>
<div id="space_type_popup" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" >
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header delegates">
        <h4 class="medium dt-large modal-title"><?php echo ($corporateFloorTypeId==0) ? lang("add_corporate_floor_plan") : lang("edit_corporate_floor_plan"); ?></h4>
      </div>
<?php //print_r($spaceTypeRecord); ?>
      <div class="modal-body small">
        <div class="modelinner ">
          <?php echo form_open($this->uri->uri_string(),$formCorporateSpaceType); ?>
            <input type="hidden" value="collapseOne" name="collapseValue">
            <?php echo form_input($availableFloorSpaceHidden); ?>
            <input type="hidden" value="<?php echo $eventId; ?>" name="eventId"  />
            <input type="hidden" value="<?php echo $corporateFloorTypeId; ?>" name="floorPlanTypeId"  />
            <input type="hidden" value="<?php echo $removeDivID; ?>" id="removeDivID"  />
            <div class="control-group mb10 eventdashboard_popup small">

              <div class="row-fluid">
                <label class="pull-left" for="space_name"><?php echo lang('corporate_space_name'); ?> <span class="astrik">*</span>
                  <span class="info_btn"><span class="field_info xsmall"><?php echo lang('tooltip_corporate_space_name'); ?></span></span>
                </label>
                <?php echo form_input($spaceTypeName); ?>
              </div>

              <div class="row-fluid floorplanspaceavail">
                <label class="pull-left" for="space_ava" class="ml_m8"><?php echo lang('corporate_space_available'); ?>  <span class="astrik">*</span>
                  <span class="info_btn"><span class="field_info xsmall"><?php echo lang('tooltip_corporate_space_available'); ?></span></span>
                </label>
                <?php echo form_input($spaceAvailable); ?>
              </div>
        
        <div class="row-fluid dn" id="spaceNotAvaliable">
                <label class="pull-left" for="space_ava"></label>
                <span></span>
          </div>

              
              <div class="row-fluid spaces_check">
               <?php $totalSpaceAvalible = (isset($totalSpaceAvalible) && $totalSpaceAvalible!="") ? $totalSpaceAvalible : "0";  
              
                for($i=1;$i<=$totalSpaceAvalible;$i++){
          
          unset($spaceTypeNo['checked']);
            unset($spaceTypeNo['disabled']);
            $title="";
          
          $boothNo = "";
          $boothNo .= (strlen($i)==1) ? '0' : "";
          $boothNo .= $i;
          $spaceTypeNo['id']    = 'space'.$i;
          $spaceTypeNo['value'] = $boothNo;
          
          if(in_array($boothNo,$availableBooths)){ // for edit functionality
            $spaceTypeNo['checked'] = "checked";
          }else{
            
            if(in_array($boothNo,$totalAvailableBooths)){
              $spaceTypeNo['checked'] = "checked";
              $spaceTypeNo['disabled'] = "disabled";
              $title           = "Booked";
            }
            }
          
          
               ?>
                   <div class="checkbox_wrapper">
            <?php echo form_checkbox($spaceTypeNo);; ?>
                      <label for="space<?php echo $i; ?>" title="<?php echo $title; ?>"><?php echo $boothNo; ?></label>
                  </div>
               <?php } // end loop  ?>
              
             
              </div>
              <div class="btn_wrapper">
          <?php 
            echo form_hidden('formActionName', 'corporateSpaceType');
        ?>  
          <input type="submit" name="loginsubmit" id="corporateSpaceTypeButton" value="Save" class=" submitbtn pull-right medium">
          <input type="submit" class="popup_cancel submitbtn pull-right medium" value="Cancel" name="logincancel" data-dismiss="modal">
              </div>

            </div>
         <?php echo form_close(); ?>
        </div>
      </div>
    </div>
  </div>
</div>
