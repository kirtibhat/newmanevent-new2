<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$formDefaultPackageBenefit = array(
    'name'   => 'formcorporateDefaultPackageBenefit',
    'id'   => 'formcorporateDefaultPackageBenefit',
    'method' => 'post',
    'class'  => '',
    'data-parsley-validate' => '',
);
?>


<div class="panel event_panel">
      <div class="panel-heading ">
        <h4 class="panel-title medium dt-large ">
        <a class=" " data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"><?php echo lang('corporate_default_package_benefit'); ?></a>
        </h4>
      </div>
      <div style="height: auto;" id="collapseTwo" class="accordion_content collapse apply_content <?php echo getIsCollapseOpen("collapseTwo"); ?>">
        <?php echo form_open($this->uri->uri_string(),$formDefaultPackageBenefit); ?>
        <div class="panel-body ls_back dashboard_panel small" id="showhideformdivcorporatedefaultbenefit">
          
          
          <?php if(!empty($corporatedefaultPacBenefit)) { 
            foreach($corporatedefaultPacBenefit as $key=>$value){
          ?>  
            <div class="row-fluid-15 category_wrapper" id="benefit_div_id_<?php echo $key; ?>">
              <span class="category_label pull-left"><?php echo $value->benefit_name; ?></span>
              <span class="category_btn pull-right">
                <a href="javascript:void(0)" class="eventsetup_btn td_btn editdefaultpackagebenifit" data-contant="<?php echo encode($value->package_benefit_id); ?>" ><span>&nbsp;</span></a>
                <a href="javascript:void(0)" class="delete_btn td_btn deletedefaultpackagebenifit" deleteid="<?php echo encode($value->package_benefit_id); ?>" ><span>&nbsp;</span></a>
              </span>
            </div>
            
          <?php } // end loop
          } // end if
          ?>
          
          <!--end of row-fluid-->
          
          <div class="input_fields_wrap" id="eventcustomcontactlistId">
              <input type="hidden" id="hidden_input_no" value="0" /> 
              <div id="hidden_new_benefit_array"></div>
          </div>
          

          <div class="row-fluid-15">
            <a href="javascript:void(0)" class="add_btn medium" id="add_benefit"><?php echo lang('corporate_default_add_benefit'); ?></a>
          </div>
          <!--end of row-fluid-->
          
          <div class="btn_wrapper ">
            <a href="#collapseTwo" class="pull-left scroll_top"><?php echo lang('comm_top'); ?></a>
            <!--<a href="#" class="default_btn btn pull-right medium"><?php echo lang('comm_save_changes'); ?></a>
            <a href="#" class="default_btn btn pull-right medium"><?php echo lang('comm_reset'); ?></a>-->


            <?php 
            echo form_hidden('eventId', $eventId);
            echo form_hidden('formActionName', 'corporateDefaultPackageBenefit');

            //button show of save and reset
            $formButton['showbutton']   = array('save','reset');
            $formButton['labletext']  = array('save'=>lang('comm_save_changes'),'reset'=>lang('comm_reset'));
            $formButton['buttonclass']  = array('save'=>'default_btn btn pull-right medium','reset'=>'default_btn btn pull-right medium reset_form');
            $this->load->view('common_save_reset_button',$formButton);
            ?>
          </div>
        </div>
        </form>
    </div>
</div>
<!--end of panel-->


<script>
  ajaxdatasave('formCorporateDefaultBenefit','<?php echo $this->uri->uri_string(); ?>',true,true,false,true,false,'#showhideformdivcorporatedefaultbenefit','#showhideformdivdefaultaddpurchaseitem','',true,'add_benefit_popup');
  
  /*  
  $(document).on("click", ".error_close_button", function(e){
    // check model popup
    $("#add_benefit_popup").modal({ keyboard: false}); // show the model
  });
  */
    
  $(document).on("click", "#add_benefit", function(e){
    formPostData = {popupType:'addBenefitPopup'};
    //call ajax popup function
    ajaxpopupopen('add_benefit_popup','event/corporatepopup',formPostData,'add_benefit');
    //eventcustompopup('event/corporatepopup','add_benefit_popup',formPostData);
  });
  
  $(document).on("click", ".editdefaultpackagebenifit", function(e){
    var package_benefit_id  = $(this).attr('data-contant');
    formPostData = {popupType:'addBenefitPopup',package_benefit_id:package_benefit_id };
    //eventcustompopup('event/corporatepopup','add_benefit_popup',formPostData);
    //call ajax popup function
    ajaxpopupopen('add_benefit_popup','event/corporatepopup',formPostData,'editdefaultpackagebenifit');
  });
  
  // call delete function to delete the particular default package
  customconfirm('deletedefaultpackagebenifit','event/deletedefaultpackagebenefit','','','',true);
  
    $(document).on("click",".stepper-step",function(e){
    
     var qty = $("#benefit_qty").val();
     
     if($(this).hasClass('up')){ // increment
      if(qty<10){
        var currentId = 'benefits_'+qty; 
        $("#benefit_qty").val((qty*1)+1);
        $(".qty_fields").append('<input type="text" class="small" value="" required  data-parsley-error-message = "<?php echo lang('common_field_required'); ?>" data-parsley-error-class="custom_li" name="benefits[]" id="'+currentId+'" />');
      }   
     }else{
      var currentId = 'benefits_'+(qty*1-1); 
      if(qty>1){  
        $("#benefit_qty").val((qty*1)-1); 
        $("#"+currentId).remove();
       }  
     }
    
       
  });
  

var IsEditCustomObj = null;
$(document).on("click", "#add_custom_benefit_button", function(e){   
  // For set collapse hight
  $(".accordion_content").css("height",'auto');                      
  //Validate form
  if($("#formCorporateDefaultBenefit").parsley().isValid()){
    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
    var hidden_input_no = $("#hidden_input_no").val();  
    e.preventDefault();
    var benefit_qty =    $.trim($("#benefit_qty").val());
    
    
    if(IsEditCustomObj!=null){
      $(IsEditCustomObj).parent().parent().find('label').html(benefits+'<input type="hidden" name="benefits[]" value="'+benefits+'">');
      IsEditCustomObj = null;
    }else{
        $('input[name^="benefits"]').each(function() {
            var benefits =  $(this).val();
            var benefits = benefits.charAt(0).toUpperCase() + benefits.substring(1); 
            //console.log(benefits); 
            $(wrapper).append('<div class="row-fluid-15 category_wrapper '+benefits+'"><span class="category_label pull-left">'+benefits+'<input type="hidden" value="'+benefits+'" name="benefits[]"  /></span></label><span class="category_btn pull-right"><a href="javascript:void(0)" class="eventsetup_btn td_btn editdefaultpackagebenifit" data-contant="'+benefits+'" ></a><a href="javascript:void(0)" class="delete_btn td_btn deleteDefaultPackageBenefit" deleteid="'+benefits+'" ></a></span></div>');
        });
    }
    
    $('#add_benefit_popup').modal('hide');
    $("#hidden_input_no").val((($("#hidden_input_no").val()*1)+1));
    
    $("#formCorporateDefaultBenefit")[0].reset();
       
  } 
  
});



// delete custom benefit
$(document).on("click", ".deleteDefaultPackageBenefit", function(e){
  // For set collapse hight
  $(".accordion_content").css("height",'auto');
  var contactId = $(this).attr('data-contact');
  $(this).parent().parent('div').remove();
});



$( "#formcorporateDefaultPackageBenefit" ).submit(function( event ){
    //save contact person details
    ajaxdatasave('formcorporateDefaultPackageBenefit','<?php echo $this->uri->uri_string(); ?>',true,true,false,false,false,'#showhideformdivcorporatedefaultbenefit','#showhideformdivdefaultaddpurchaseitem');    
});
</script>
