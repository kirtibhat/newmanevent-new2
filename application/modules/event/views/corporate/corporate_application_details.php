<?php
//--------------personal form details---------------//

$formApplicationDetails = array(
    'name' => 'formApplicationDetails',
    'id' => 'formApplicationDetails',
    'method' => 'post',
    'class' => '',
);

//get master field values array 
$fieldsmastervalue = fieldsmastervalue();

$emailfield = array(
    //  'name'  => 'emailfield',
    'value' => '',
    'id' => 'emailfield',
    'type' => 'text',
    'placeholder' => 'Displayed & Mandatory',
    'readonly' => '',
    'class' => 'small short_field',
);

$confirmemailfield = array(
    //  'name'  => 'emailfield',
    'value' => '',
    'id' => 'emailfield',
    'type' => 'text',
    'placeholder' => 'Displayed & Mandatory',
    'readonly' => '',
    'class' => 'small short_field',
);
?>

<div class="panel event_panel">
    <div class="panel-heading ">
        <h4 class="panel-title medium dt-large ">
            <a class=" " data-toggle="collapse" data-parent="#accordion" href="#collapseFour">Corporate Application Details</a>
        </h4>
    </div>
    <div style="height: auto;" id="collapseFour" class="accordion_content collapse apply_content <?php echo getIsCollapseOpen("collapseFour"); ?>">
        <?php echo form_open($this->uri->uri_string(), $formApplicationDetails); ?>

        <div class="panel-body ls_back dashboard_panel small"  id="showhideformApplicationDetails">

            <?php
            if (!empty($masterformfielddata)) {
                foreach ($masterformfielddata as $formfield) {

                    $fieldCreateBy = $formfield->field_create_by;

                    if ($fieldCreateBy == "1") { // 1 belongs to admin    
                        ?>
                        <?php
                        echo form_hidden('fieldid[]', $formfield->id);
                        if ($formfield->field_name == "email") {
                            ?>

                            <div class="row-fluid-15">
                                <label for="co_email">Email <span class="astrik">*</span>
                                    <span class="info_btn"><span class="field_info xsmall">Email Address Info</span></span>
                                </label>
                                <?php
                                $fieldName = 'field_' . $formfield->id;
                                echo form_input($emailfield);
                                $fieldName = 'field_' . $formfield->id;
                                echo form_hidden($fieldName, $formfield->field_status);
                                ?>

                            </div>
                            
                            <div class="row-fluid-15">
                                <label for="co_email">Confirm Email <span class="astrik">*</span>
                                    <span class="info_btn"><span class="field_info xsmall">Confirm Email Address Info</span></span>
                                </label>
                                <?php
                                $fieldName = 'field_' . $formfield->id;
                                echo form_input($confirmemailfield);
                                $fieldName = 'field_' . $formfield->id;
                                echo form_hidden($fieldName, $formfield->field_status);
                                ?>

                            </div>

                        <?php } else { ?>

                            <div class="row-fluid-15">
                                <label for="co_org"><?php echo ucwords($formfield->field_name); ?> <span class="astrik">*</span>
                                </label>

                                <?php
                                $other = 'id="field_' . $formfield->id . '" class="small custom-select short_field" ';
                                $fieldName = 'field_' . $formfield->id;
                                echo form_dropdown($fieldName, $fieldsmastervalue, $formfield->field_status, $other);
                                ?>
                                <?php if ($formfield->is_editable == "1") { ?>   
                                    <span class="pull-right select_edit editapplicationcustomformfield"  customfieldid="<?php echo encode($formfield->id); ?>"><a href="javascript:void(0)" class="eventsetup_btn td_btn"><span>&nbsp;</span></a></span>
                                <?php } ?> 
                            </div>
                            <!--end of row-fluid-->


                        <?php } ?>


                        <?php
                    } // end if
                } // end loop 
            } // end if 
            ?>



            <?php
            if (!empty($masterformfielddata)) {
                foreach ($masterformfielddata as $key => $formfield) {

                    $fieldCreateBy = (isset($formfield->field_create_by) && $formfield->field_create_by == "0") ? $formfield->field_create_by : NULL;
                    if ($fieldCreateBy == "0") {
                        ?>

                        <div class="row-fluid-15" id="deleteappcustomfielddiv_<?php echo $key; ?>">
                            <label for="co_owned"><?php echo ucwords($formfield->field_name); ?></label>

                            <?php
                            echo form_hidden('fieldid[]', $formfield->id);
                            $other = 'id="field_' . $formfield->id . '" class="small custom-select short_field"';
                            $fieldName = 'field_' . $formfield->id;
                            echo form_dropdown($fieldName, $fieldsmastervalue, $formfield->field_status, $other);
                            ?>
                            <span class="pull-right select_edit">
                                <a href="javascript:void(0)" class="eventsetup_btn td_btn editapplicationcustomformfield" customfieldid="<?php echo encode($formfield->id); ?>"><span>&nbsp;</span></a>
                                <a href="javascript:void(0)" class="delete_btn td_btn deleteapplicationcustomformfield"  deleteid="<?php echo $formfield->id; ?>"><span>&nbsp;</span></a>
                            </span>
                        </div>
                        <!--end of row-fluid-->

                    <?php
                    }
                }
            }
            ?>

            <div class="row-fluid-15">
                <a class="add_btn medium" href="javascript:void(0)" id="add_field_custom"><?php echo lang('corporate_add_custom_field'); ?></a>
            </div>
            <!--end of row-fluid-->   

<?php echo form_hidden('eventId', $eventId, 'id="eventId"'); ?>
<?php echo form_hidden('formActionName', 'corporateApplicationDetails'); ?>
            <div class="btn_wrapper ">
                <a href="#collapseFour" class="pull-left scroll_top"><?php echo lang('comm_top'); ?></a>

                <?php
                //button show of save and reset
                $formButton['showbutton'] = array('save');
                $formButton['labletext'] = array('save' => lang('comm_save_changes'));
                $formButton['buttonclass'] = array('save' => 'default_btn btn pull-right medium');
                $this->load->view('event/common_save_reset_button', $formButton);
                ?>

                <!--<a href="#" class="default_btn btn pull-right medium">Save</a>-->
                <a href="#" class="default_btn btn pull-right medium">Clear</a>
            </div>
        </div>
        </form>
    </div>
</div>
<!--end of panel-->

<script type="text/javascript">
    $(document).ready(function() {
        //personal details save
        ajaxdatasave('formApplicationDetails', '<?php echo $this->uri->uri_string(); ?>', false, false, false, false, false, '#showhideformApplicationDetails', '#showhideformApplicationDetails');
        
        //personal details save
        ajaxdatasave('formAddCustomeField', '<?php echo $this->uri->uri_string(); ?>', true, true, false, true, false, '#showhideformdiv', '#showhideformdivdieteryrequirement', '', true, 'add_field_popup');

// call delete function to delete the particular Custom Field
        customconfirm('deleteapplicationcustomformfield', 'event/deleteeditcustomefield', '', '', '', true,'',false,'<?php echo lang('event_msg_custom_field_deleted') ?>');

        /*
         $(document).on("click", ".error_close_button", function(e){
         // check model popup
         $("#add_field_popup").modal({ keyboard: false}); // show the model
         });
         */

        $(document).on("click", "#add_field_custom", function(e) {
            formPostData = {popupType: 'addCustomFieldPopup', eventId: $("#eventId").val()};
            //eventcustompopup('event/corporatepopup','add_field_popup',formPostData,'custom_field_qty');
            //call ajax popup function
            ajaxpopupopen('add_field_popup', 'event/corporatepopup', formPostData, 'add_field_custom', '');
        });

        $(document).on("click", ".editapplicationcustomformfield", function(e) {
            var customfieldid = $(this).attr('customfieldid');
            formPostData = {popupType: 'addCustomFieldPopup', eventId: $("#eventId").val(), customfieldid: customfieldid};
            ajaxpopupopen('add_field_popup', 'event/corporatepopup', formPostData, 'editapplicationcustomformfield', '');
            //eventcustompopup('event/corporatepopup','add_field_popup',formPostData,'custom_field_qty');
        });


        /*
         *  This section is used to select custome field type select
         */
        $(document).on('click', '.fieldtype', function() {
            var getval = $(this).val();
            var customFieldId = parseInt($("#customFieldId").val());
            //if use add field then remove old data
            if (customFieldId == 0) {
                $("#selectedField").val('0');
                $(".default_value_list").html('');
            }
            if (getval == "text" || getval == "file") {
                $("#show_dropdown_feature").hide();
            } else {
                $("#show_dropdown_feature").show();
                $("#custom_field_qty").val('1');
                $(".default_value_list").html('<label class="pull-left" for="field_1">Field 1</label><input type="text" class="small" value="" required  data-parsley-error-message = "<?php echo lang('common_field_required'); ?>" data-parsley-error-class="custom_li"  data-parsley-trigger="keyup" name="defaultValue[]" id="defaultValue_0" />');
            }
        });

        /*
         *  This section is used select option fields
         */
        $(document).on('click', '.stepper-step', function() {
            var qty = parseInt($("#custom_field_qty").val());
            if ($(this).hasClass('up')) { // increment
                if (qty < 10) {
                    var currentId = 'defaultValue_' + qty;
                    var currentLblId = 'defaultLblValue_' + qty;
                    $("#custom_field_qty").val((qty * 1) + 1);
                    $(".default_value_list").append('<label class="pull-left" for="field_1" id="' + currentLblId + '">Field ' + ((qty * 1) + 1) + '</label><input type="text" class="small" value="" required  data-parsley-error-message = "<?php echo lang('common_field_required'); ?>" data-parsley-error-class="custom_li"  data-parsley-trigger="keyup" name="defaultValue[]" id="' + currentId + '" />');
                }
            } else {
                var currentId = 'defaultValue_' + ((qty * 1) - 1);
                var currentLblId = 'defaultLblValue_' + ((qty * 1) - 1);
                if (qty > 1) {
                    $("#custom_field_qty").val(((qty * 1) - 1));
                    $("#" + currentId).remove();
                    $("#" + currentLblId).remove();
                }
            }
        });
    });
</script>
