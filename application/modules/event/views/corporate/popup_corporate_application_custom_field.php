<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
if (!empty($customFieldRecord)) {

    $customfieldidval = (!empty($customFieldRecord[0]->id)) ? $customFieldRecord[0]->id : "";
    $fieldNameValue = (!empty($customFieldRecord[0]->field_name)) ? $customFieldRecord[0]->field_name : '';
    $fieldTypeValue = (!empty($customFieldRecord[0]->field_type)) ? $customFieldRecord[0]->field_type : '';
    $fieldDefaultValue = (!empty($customFieldRecord[0]->default_value)) ? json_decode($customFieldRecord[0]->default_value) : '';
} else {
    $customfieldidval = 0;
    $fieldNameValue = "";
    $fieldTypeValue = "";
    $fieldDefaultValue = "";
}

$headerAction = ($customfieldidval > 0) ? 'Edit' : 'Add';

//convert object into array
$fieldDefaultValue = objecttoarray($fieldDefaultValue);

$fieldDefaultValueCount = '';
if (!empty($fieldDefaultValue)) {
    $fieldDefaultValueCount = count($fieldDefaultValue);
}

$isTextChecked = true;
$isSelectChecked = false;
$isRadioChecked = false;
$isFileChecked = false;
$isSelectListDiv = 'dn';

if (!empty($fieldNameValue)) {


    if ($fieldTypeValue == "selectbox") {
        $isSelectChecked = true;
        $isSelectListDiv = '';
    } elseif ($fieldTypeValue == "radio") {
        $isRadioChecked = true;
        $isSelectListDiv = '';
    } elseif ($fieldTypeValue == "file") {
        $isFileChecked = true;
    } elseif ($fieldTypeValue == "text") {
        $isTextChecked = true;
    }
}

/* ------- form field for custome field---------------- */
$formAddCustomeField = array(
    'name' => 'formAddCustomeField',
    'id' => 'formAddCustomeField',
    'method' => 'post',
    'class' => 'wpcf7-form',
    'data-parsley-validate' => '',
);

$fieldName = array(
    'name' => 'fieldName',
    'value' => $fieldNameValue,
    'class' => 'small',
    'id' => 'fieldName',
    'type' => 'text',
    'required' => '',
    'class' => 'small',
    'autocomplete' => 'off',
    'data-parsley-error-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
);


$fieldTypeText = array(
    'name' => 'fieldType',
    'value' => 'text',
    'id' => 'fieldTypeText',
    'type' => 'radio',
    'class' => 'fieldtype small',
    'required' => '',
    'autocomplete' => 'off',
    'data-parsley-error-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
    'checked' => $isTextChecked,
);


$fieldTypeSelect = array(
    'name' => 'fieldType',
    'value' => 'selectbox',
    'id' => 'fieldTypeSelect',
    'type' => 'radio',
    'class' => 'fieldtype',
    'required' => '',
    'checked' => $isSelectChecked,
);



$fieldTypeRadio = array(
    'name' => 'fieldType',
    'value' => 'radio',
    'id' => 'fieldTypeRadio',
    'type' => 'radio',
    'class' => 'fieldtype',
    'required' => '',
    'checked' => $isRadioChecked,
);

$fieldTypeFile = array(
    'name' => 'fieldType',
    'value' => 'file',
    'id' => 'fieldTypeFile',
    'type' => 'radio',
    'class' => 'fieldtype',
    'required' => '',
    'checked' => $isFileChecked,
);

$selectedField = array(
    'name' => 'selectedField',
    'value' => $fieldDefaultValueCount,
    'id' => 'selectedField',
    'type' => 'text',
    'class' => 'width_50px height_27 ft_16 clr_425968 spinner_selectbox',
    'required' => '',
    'disabled' => '',
    'min' => '0',
);

$customFieldId = array(
    'name' => 'customFieldId',
    'value' => (!empty($customfieldidval) ? $customfieldidval : "0"),
    'id' => 'customFieldId',
    'type' => 'hidden',
);
/* ------- form field for custome field---------------- */
?>

<!--end of pop4-->
<div id="add_field_popup" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header delegates">
                <h4 class="medium dt-large modal-title"><?php echo $headerAction; ?> <?php echo lang('form_custom_field'); ?></h4>
            </div>

            <div class="modal-body small">
                <div class="modelinner ">
<?php echo form_open(base_url('event/addeditcustomefieldsave'), $formAddCustomeField); ?>
                    <input type="hidden" value="collapseFour" name="collapseValue">
                    <div class="control-group mb10 eventdashboard_popup small">

                        <div class="row-fluid">
                            <label class="pull-left" for="field_name"><?php echo lang('event_custom_form_field_name'); ?> <span class="astrik">*</span></label>
<?php echo form_input($fieldName); ?>
                        </div>

                        <div class="row-fluid">
                            <label class="pull-left" for="f_type"><?php echo lang('event_custom_form_field_type'); ?></label>
                            <div class="radio_wrapper">
                                <?php echo form_radio($fieldTypeText); ?> 
                                <label for="fieldTypeText"><?php echo lang('event_custom_form_field_text'); ?></label>

                                <?php echo form_radio($fieldTypeSelect); ?>
                                <label for="fieldTypeSelect"><?php echo lang('event_custom_form_field_ddm'); ?></label>

                                <?php echo form_radio($fieldTypeRadio); ?>
                                <label for="fieldTypeRadio"><?php echo lang('event_custom_form_field_radio'); ?></label>

<?php echo form_radio($fieldTypeFile); ?>
                                <label for="fieldTypeFile"><?php echo lang('event_custom_form_field_file'); ?></label>
                            </div>                
                        </div>

                        <div id="show_dropdown_feature" class="<?php echo $isSelectListDiv; ?>">
                            <div class="row-fluid">
                                <label class="pull-left" for="custom_field_qty"><?php echo lang('event_custom_form_quantity'); ?><span class="astrik">*</span>
                                    <span class="info_btn"><span class="field_info xsmall"><?php echo lang('event_custom_form_quantity'); ?></span></span>
                                </label>

                                <div class="stepper ">
                                    <input id="custom_field_qty"  class="small dark stepper-input" type="number" readonly  required="" value="<?php echo (!empty($fieldDefaultValueCount)) ? $fieldDefaultValueCount : '1'; ?>" name="field_qty">
                                    <span class="stepper-step up"> </span>
                                    <span class="stepper-step down"> </span>
                                </div>
                            </div>




                            <div class="row-fluid default_value_list">

                                <?php
                                if (!empty($fieldDefaultValueCount)) {

                                    foreach ($fieldDefaultValue as $key => $value) {
                                        $i = $key + 1;
                                        echo '<label class="pull-left" for="field_' . $key . '" id="defaultLblValue_' . $key . '">Field ' . $i . '</label>
                   <input type="text" value="' . $value . '" id="defaultValue_' . $key . '" name="defaultValue[]" class="small" data-parsley-error-message="' . lang('common_field_required') . '" data-parsley-error-class = "custom_li"  data-parsley-trigger="keyup" >';
                                    }
                                } else {
                                    ?>
                                    <label class="pull-left" for="field_1"><?php echo lang('event_custom_form_field_1'); ?></label>
                                    <input type="text" value="" id="defaultValue_1" name="defaultValue[]"  class="small" data-parsley-error-class = "custom_li"  data-parsley-trigger="keyup" />
<?php } ?>
                            </div>


                        </div>


                        <div class="btn_wrapper">
                            <?php
                            echo form_hidden('eventId', $eventId);
                            echo form_hidden('registrantId', 0);
                            echo form_input($customFieldId);
                            echo form_hidden('form_id', '2'); // 2 for Corporate Application Details 
                            echo form_hidden('formActionName', 'masterPersonalDetails');
                            ?>
                            <input type="submit" name="loginsubmit" value="Save" class=" submitbtn pull-right medium">
                            <input type="button" class="popup_cancel submitbtn pull-right medium" value="Cancel" name="logincancel" data-dismiss="modal">
                        </div>

                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script >
    $(document).ready(function() {
        $("#formAddCustomeField").parsley();
    });
</script>
<!-- Modal -->    
