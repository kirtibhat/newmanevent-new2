<?php
if (!empty($default_package_benefit)) {
    $defaultPackageBenefitName = $default_package_benefit[0]->benefit_name;
    $defaultPackageBenefitId = encode($default_package_benefit[0]->package_benefit_id);
} else {
    $defaultPackageBenefitName = "";
    $defaultPackageBenefitId = "";
}

//Default Benefit 
$formCorporateBenefit = array(
    'name' => 'formCorporateDefaultBenefit',
    'id' => 'formCorporateDefaultBenefit',
    'method' => 'post',
    'class' => 'wpcf7-form',
    'data-parsley-validate' => '',
);

$benefitName = array(
    'name' => 'benefits[]',
    'value' => $defaultPackageBenefitName,
    'id' => 'benefits',
    'type' => 'text',
    'class' => 'small benefits',
    'required' => '',
    'autocomplete' => 'off',
    'data-parsley-error-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup'
);
?>
<!--end of pop1-->
<div id="add_benefit_popup" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header delegates">
                <h4 class="medium dt-large modal-title"><?php echo ($defaultPackageBenefitId == "") ? lang('corporate_default_add_benefit') : lang('corporate_default_edit_benefit'); ?></h4>
            </div>

            <div class="modal-body small">
                <div class="modelinner ">
                    <?php echo form_open($this->uri->uri_string(), $formCorporateBenefit); ?>
                    <input type="hidden" value="<?php echo (isset($openPopupId) && $openPopupId != "") ? $openPopupId : "collapseTwo"; ?>" name="collapseValue">

                    <div class="control-group mb10 eventdashboard_popup small">

                        <?php if ($defaultPackageBenefitId == "") { ?>
                            <div class="row-fluid">
                                <label class="pull-left" for="benefit_qty"><?php echo lang('corporate_benefit_quantity'); ?><span class="astrik">*</span>
                                    <span class="info_btn"><span class="field_info xsmall"><?php echo lang('tooltips_corporate_benefit_quantity'); ?></span></span>
                                </label>

                                <div class="stepper ">
                                    <input id="benefit_qty"  class="small dark stepper-input " type="number" readonly  required="" value="1" name="benefit_qty">
                                    <span class="stepper-step up"> </span>
                                    <span class="stepper-step down"> </span>
                                </div>

                            </div>
                        <?php } ?>

                        <div class="row-fluid <?php echo ($defaultPackageBenefitId == "") ? 'qty_fields' : ''; ?>">
                            <?php if ($defaultPackageBenefitId != "") { ?> 
                                <label class="pull-left" for="benefit_qty"><?php echo lang('corporate_benefit_name'); ?><span class="astrik">*</span></label>
                            <?php } ?> 
                            <?php echo form_input($benefitName); ?>
                        </div>

                        <div class="btn_wrapper">

                            <?php
                            echo form_hidden('formActionName', 'corporateDefaultBenefit');
                            echo form_hidden('packageBenefitId', $defaultPackageBenefitId);
                            echo form_hidden('packageId', $packageId);

//button show of save and reset
                            $formButton['showbutton'] = array('save');
                            $formButton['labletext'] = array('save' => lang('comm_save_changes'));
                            $formButton['buttonclass'] = array('save' => 'submitbtn pull-right medium');
//$this->load->view('event/common_save_reset_button',$formButton);                  
                            ?>
                            <input type="submit" name="customBenefit" value="Save" class=" submitbtn pull-right medium" id="add_custom_benefit_button">
                            <input type="submit" class="popup_cancel submitbtn pull-right medium" value="Cancel" name="logincancel" data-dismiss="modal">

                        </div>

                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $("#formCorporateDefaultBenefit").parsley();
    });
</script>
