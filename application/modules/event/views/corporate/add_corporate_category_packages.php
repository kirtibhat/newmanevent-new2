





<?php
// get is_exhibition_space from floor plan 
if (!empty($corporateFloorPlan)) {
    $isExhibitionSpace = $corporateFloorPlan[0]->is_exhibition_space;
} else {
    $isExhibitionSpace = 0;
}

if (!empty($corporatepackagedetails)) {
    foreach ($corporatepackagedetails as $packageDetailsKey => $packageDetails) {

        $packageId = $packageDetails->id;

        $formCorporateDetails = array(
            'name' => 'formCorporatePackageDetails_' . $packageId,
            'id' => 'formCorporatePackageDetails_' . $packageId,
            'method' => 'post',
            'class' => 'wpcf7-form',
            'data-parsley-validate' => '',
        );


        $packagesubjectyes = array(
            'name' => 'package_subject_' . $packageId,
            'value' => '1',
            'id' => 'app_subj_yes_' . $packageId,
            'type' => 'radio',
            'class' => 'small',
            'required' => '',
            'autocomplete' => 'off',
            'data-parsley-error-message' => lang('common_field_required'),
            'data-parsley-error-class' => 'custom_li',
            'data-parsley-trigger' => 'keyup',
        );

        $packagesubjectno = array(
            'name' => 'package_subject_' . $packageId,
            'value' => '0',
            'id' => 'app_subj_no_' . $packageId,
            'type' => 'radio',
            'class' => 'small',
            'required' => '',
            'autocomplete' => 'off',
            'data-parsley-error-message' => lang('common_field_required'),
            'data-parsley-error-class' => 'custom_li',
            'data-parsley-trigger' => 'keyup',
        );

        $packagepassreqyes = array(
            'name' => 'package_pass_req_' . $packageId,
            'value' => '1',
            'id' => 'pwd_req_yes_' . $packageId,
            'type' => 'radio',
            'packageId' => $packageId,
            'class' => 'small checkpassreq',
            'required' => '',
            'autocomplete' => 'off',
            'data-parsley-error-message' => lang('common_field_required'),
            'data-parsley-error-class' => 'custom_li',
            'data-parsley-trigger' => 'keyup',
        );

        $packagepassreqno = array(
            'name' => 'package_pass_req_' . $packageId,
            'value' => '0',
            'id' => 'pwd_req_no_' . $packageId,
            'type' => 'radio',
            'class' => 'small checkpassreq',
            'packageId' => $packageId,
            'required' => '',
            'autocomplete' => 'off',
            'data-parsley-error-message' => lang('common_field_required'),
            'data-parsley-error-class' => 'custom_li',
            'data-parsley-trigger' => 'keyup',
        );

        $packagepass = array(
            'name' => 'password_' . $packageId,
            'value' => $packageDetails->password,
            'id' => 'password_' . $packageId,
            'type' => 'password',
            'class' => 'short_field small',
            //'required'=>'',
            'autocomplete' => 'off',
            'data-parsley-error-message' => lang('common_field_required'),
            'data-parsley-error-class' => 'custom_li',
            'data-parsley-trigger' => 'keyup',
        );

        $packageconpass = array(
            'name' => 'con_password_' . $packageId,
            'value' => $packageDetails->password,
            'id' => 'con_password_' . $packageId,
            'type' => 'password',
            'class' => 'short_field small',
            //'required'=>'',
            'autocomplete' => 'off',
            'data-parsley-error-message' => lang('common_field_required'),
            'data-parsley-error-class' => 'custom_li',
            'data-parsley-trigger' => 'keyup',
        );


        $packagedescription = array(
            'name' => 'package_description_' . $packageId,
            'value' => $packageDetails->description,
            'id' => 'package_description_' . $packageId,
            'type' => 'textarea',
            'class' => 'short_field small',
            'rows' => '3',
            'cols' => '5',
                //'required'=>'',
                //'autocomplete' => 'off',
                //'data-parsley-error-message' => lang('common_field_required'),
                //'data-parsley-error-class' => 'custom_li',
                //'data-parsley-trigger' => 'keyup',
        );

        $packageavailable = array(
            'name' => 'package_available_' . $packageId,
            'value' => $packageDetails->limit_package_available,
            'id' => 'package_available_' . $packageId,
            'type' => 'text',
            'class' => 'short_field small package_available_class',
            'required' => '',
            'autocomplete' => 'off',
            'data-parsley-error-message' => lang('common_field_required'),
            'data-parsley-error-class' => 'custom_li',
            'data-parsley-trigger' => 'keyup',
        );

        $packagetotalprice = array(
            'name' => 'package_totalprice_' . $packageId,
            'value' => $packageDetails->total_price,
            'id' => 'package_totalprice_' . $packageId,
            'type' => 'text',
            'class' => 'short_field small packageTotalPriceEnter',
            'required' => '',
            'packageId' => $packageId,
            'autocomplete' => 'off',
            'data-parsley-error-message' => lang('common_field_required'),
            'data-parsley-error-class' => 'custom_li',
            'data-parsley-trigger' => 'keyup',
        );
        $packagegstinclude = array(
            'name' => 'package_gst_include_' . $packageId,
            'value' => $packageDetails->gst,
            'id' => 'packageGSTVal' . $packageId,
            'type' => 'text',
            'class' => 'short_field',
            'readonly' => true,
            'required' => '',
            'autocomplete' => 'off',
            'data-parsley-error-message' => lang('common_field_required'),
            'data-parsley-error-class' => 'custom_li',
            'data-parsley-trigger' => 'keyup',
        );

        $packageregistraionincludeyes = array(
            'name' => 'reg_include_' . $packageId,
            'value' => '1',
            'id' => 'reg_include_yes_' . $packageId,
            'type' => 'radio',
            'class' => 'short_field',
            'required' => '',
            'autocomplete' => 'off',
            'data-parsley-error-message' => lang('common_field_required'),
            'data-parsley-error-class' => 'custom_li',
            'data-parsley-trigger' => 'keyup',
        );

        if ($packageDetails->registration_included == '1') {
            $packageregistraionincludeyes['checked'] = "checked";
        }

        $packageregistraionincludeno = array(
            'name' => 'reg_include_' . $packageId,
            'value' => '0',
            'id' => 'reg_include_no_' . $packageId,
            'type' => 'radio',
            'class' => 'short_field',
            'required' => '',
            'autocomplete' => 'off',
            'data-parsley-error-message' => lang('common_field_required'),
            'data-parsley-error-class' => 'custom_li',
            'data-parsley-trigger' => 'keyup',
        );

        if ($packageDetails->registration_included == '0') {
            $packageregistraionincludeno['checked'] = "checked";
        }

        $packageboothno = array(
            'name' => 'booth_no_' . $packageId,
            'value' => '0',
            'id' => 'refr_no_' . $packageId,
            'type' => 'number',
            'class' => 'small dark',
            'step' => '1',
            'min' => '1',
            'max' => '',
            'readonly' => '',
            'required' => '',
            'autocomplete' => 'off',
            'data-parsley-error-message' => lang('common_field_required'),
            'data-parsley-error-class' => 'custom_li',
            'data-parsley-trigger' => 'keyup',
        );

        if ($packageDetails->subject_to_approval == 1) {
            $packagesubjectyes['checked'] = true;
        } elseif ($packageDetails->subject_to_approval == 0) {
            $packagesubjectno['checked'] = "checked";
        }
// Password required
        if ($packageDetails->request_password == 1) {
            $packagepassreqyes['checked'] = true;
            $request_password_class = '';
        } elseif ($packageDetails->request_password == 0) {
            $packagepassreqno['checked'] = "checked";
            $request_password_class = 'dn';
        }
        ?>
        <div class="panel event_panel">
            <div class="panel-heading " >
                <h4 class="panel-title medium dt-large heading_btn" id="corporatepackage_<?php echo $packageId; ?>">
                    <a class=" " data-toggle="collapse" data-parent="#accordion" href="#collapseSix<?php echo $packageDetailsKey; ?>"><?php echo $packageDetails->corporate_package_name; ?></a>
                    <span class="heading_btn_wrapper">
                        <a href="javascript:void(0)" class="delete_btn td_btn deletecorporatepackage" deleteid="<?php echo $packageId; ?>"><span>&nbsp;</span></a>
                        <a href="javascript:void(0)" class="eventsetup_btn td_btn editcorporatepackage" data-cat-id="<?php echo $packageId; ?>"><span>&nbsp;</span></a>
                        <a href="javascript:void(0)" class="copy_btn td_btn copycorporatepackage" data-cat-id="<?php echo $packageId; ?>" ><span>&nbsp;</span></a>
                    </span>
                </h4>
            </div>
            <div style="height: auto;" id="collapseSix<?php echo $packageDetailsKey; ?>" class="accordion_content collapse apply_content ">
                <?php echo form_open("", $formCorporateDetails) ?>
                <div class="panel-body ls_back dashboard_panel small">

                    <div class="row-fluid-15">
                        <label for="co_cat">Corporate Category <span class="astrik">*</span>
                            <span class="info_btn"><span class="field_info xsmall">Corporate Category</span></span>
                        </label>
                        <?php
                        $other = 'id="corporate_package_id_' . $packageId . '" class="small custom-select custom_li short_field" required ';
                        $option[""] = "Select Category";
                        if (!empty($eventCorporateCategories)) {
                            foreach ($eventCorporateCategories as $key => $value) {
                                $option[$value->category_id] = $value->category;
                            }
                        }//end if 
                        echo form_dropdown('corporate_package_id_' . $packageId, $option, $packageDetails->category_id, $other);
                        ?>

                    </div>
                    <!--end of row-fluid-->

                    <div class="row-fluid-15">
                        <label for="app_subj">Subject to Approval <span class="astrik">*</span>
                            <span class="info_btn"><span class="field_info xsmall">Subject to Approval</span></span>
                        </label>
                        <div class="radio_wrapper ">
                            <?php echo form_radio($packagesubjectyes); ?>
                            <label for="app_subj_yes_<?php echo $packageId; ?>">Yes</label>
                            <?php echo form_radio($packagesubjectno); ?>
                            <label for="app_subj_no_<?php echo $packageId; ?>">No</label>
                        </div>
                    </div>
                    <!--end of row-fluid-->

                    <!--<div class="row-fluid-15">
                      <label for="pwd_req">Password Required <span class="astrik">*</span>
                        <span class="info_btn"><span class="field_info xsmall">Password required</span></span>
                      </label>
                      <div class="radio_wrapper ">
                        
                    <?php //echo form_radio($packagepassreqyes);  ?>
                        <label for="pwd_req_yes_<?php //echo $packageId; ?>">Yes</label>
              
                    <?php //echo form_radio($packagepassreqno);  ?>
                        <label for="pwd_req_no_<?php //echo $packageId; ?>">No</label>
              
                        
                      </div>
                    </div>
              
                    <div id="pass_req_div_<?php //echo $packageId;  ?>" class="<?php //echo $request_password_class  ?>">
                      <div class="row-fluid-15">
                        <label for="pwd">Password <span class="astrik">*</span></label>
                    <?php //echo form_input($packagepass);  ?>
                      </div>
              
              
                      <div class="row-fluid-15">
                        <label for="cpwd">Confirm Password <span class="astrik">*</span></label>
                    <?php //echo form_input($packageconpass);  ?>
                      </div>
              
                      <div class="row-fluid-15">
                        <label>Show/Hide Password</label>
                        <span class="checkbox_wrapper">
                          <input type="checkbox" name='showhide<?php //echo $packageId   ?>' id='showhide<?php //echo $packageId   ?>' onclick="textpass('<?php //echo 'password_'.$packageId   ?>','<?php //echo 'con_password_'.$packageId   ?>');">
                          <label for="showhide<?php //echo $packageId   ?>"></label>
                        </span>
                      </div>
                    </div>-->  

                    <div class="row-fluid-15">
                        <label for="co_desc">Description 
                            <span class="info_btn"><span class="field_info xsmall">Description</span></span>
                        </label>
                        <?php echo form_textarea($packagedescription); ?>
                    </div>
                    <!--end of row-fluid-->

                    <div class="row-fluid-15">
                        <label for="packg_avb">Packages Available <span class="astrik">*</span></label>
                        <?php echo form_input($packageavailable); ?>
                    </div>
                    <!--end of row-fluid-->

                    <div class="row-fluid-15">
                        <label for="totalprice">Total Price $  <span class="astrik">*</span></label>
                        <?php echo form_input($packagetotalprice); ?>
                    </div>
                    <!--end of row-fluid-->

                    <div class="row-fluid-15">
                        <label for="gst">GST Included $</label>
                        <?php echo form_input($packagegstinclude); ?>
                    </div>
                    <!--end of row-fluid-->

                    <hr class="panel_inner_seprator" />


                    <?php
                    // get benefit which belongs to this corporate 
                    $packageBenefitResult = getCorporatePackageBenefit($packageDetails->id);
                    $packageBenefitIds = $packageDetails->package_benefit_ids;
                    $packageBenefitIds = (!empty($packageBenefitIds)) ? unserialize($packageBenefitIds) : array();
                    if (!empty($packageBenefitResult)) {
                        foreach ($packageBenefitResult as $key => $value) {
                            ?>
                            <div class="row-fluid-15 category_wrapper" id="package_benefit_list_<?php echo $key; ?>">
                                <span class="category_label pull-left">
                                    <span class="checkbox_wrapper">
                                        <?php $benefitsChecked = (in_array($value->package_benefit_id, $packageBenefitIds)) ? 'checked="checked"' : ''; ?>
                                        <input type="checkbox" id="available_benefit_<?php echo $packageId . '_' . $key; ?>" name="available_benefit_<?php echo $packageId . '[]'; ?>" value="<?php echo $value->package_benefit_id; ?>" <?php echo $benefitsChecked; ?> />
                                        <label for="available_benefit_<?php echo $packageId . '_' . $key; ?>"><?php echo $value->benefit_name; ?></label>
                                    </span>
                                </span>
                                <?php if ($value->corporate_package_id != 0) { ?>  
                                    <span class="category_btn pull-right">
                                        <a href="javascript:void(0)" class="eventsetup_btn td_btn edit_corporate_benefit" data-cat-id="<?php echo $packageId; ?>" data-contant="<?php echo encode($value->package_benefit_id); ?>" ><span>&nbsp;</span></a>
                                        <a href="javascript:void(0)" class="delete_btn td_btn delete_corporate_benefit" deleteid="<?php echo encode($value->package_benefit_id); ?>" ><span>&nbsp;</span></a>
                                    </span>
                                <?php } ?>
                            </div>
                            <?php
                        } // end loop
                    } //end foreach
                    ?>
                    <!--end of row-fluid-->

                    <div class="row-fluid-15">
                        <a id="custom_emails" href="javascript:void(0)" class="add_btn medium add_corporate_benefit" data-cat-id="<?php echo $packageId; ?>" >Add Benefit</a>
                    </div>


                    <hr class="panel_inner_seprator" />

                    <div class="row-fluid-15">
                        <label for="reg_include">Registration Included <span class="astrik">*</span>
                            <span class="info_btn"><span class="field_info xsmall">Registration Included</span></span>
                        </label>
                        <div class="radio_wrapper ">

                            <?php echo form_radio($packageregistraionincludeyes); ?>
                            <label for="reg_include_yes_<?php echo $packageId; ?>">Yes</label>

                            <?php echo form_radio($packageregistraionincludeno); ?>
                            <label for="reg_include_no_<?php echo $packageId; ?>">No</label>
                        </div>
                    </div>
                    <!--end of row-fluid-->
                    <input type="hidden" value="<?php echo $isExhibitionSpace; ?>" name="is_exhibition_space_<?php echo $packageId; ?>" />
                    <?php if ($isExhibitionSpace == '1') { ?>

                        <hr class="panel_inner_seprator" />
                        <?php
                        // get exhibition space
                        $exhibitionDetails = getExhibitionSpaceDetails(array('corporate_package_id' => $packageDetails->id, 'event_id' => $packageDetails->event_id));


                        if (!empty($exhibitionDetails)) {
                            $floorPlanType = $exhibitionDetails[0]->floor_plan_type_id;
                            $totalSpaceAvailable = $exhibitionDetails[0]->total_space_available;

                            foreach ($exhibitionDetails as $key => $boothPosition) {
                                $reservedBoothPositions[] = $boothPosition->booth_position;
                            }
                        } else {
                            $floorPlanType = "";
                            $totalSpaceAvailable = "";
                            $reservedBoothPositions = array();
                        }
                        ?>
                        <div class="row-fluid-15">
                            <label for="exhibition_type">Exhibition Space Type <span class="astrik">*</span>
                                <span class="info_btn"><span class="field_info xsmall">Exhibition Space Type</span></span>
                            </label>
                            <select class="small custom-select short_field exhibitor_type_class" name="exhibition_type_<?php echo $packageId; ?>" id="exhibition_type_<?php echo $packageId; ?>">
                                <option value="">Select</option>
            <?php
            // get space type if exist
            $AllocateReservedBooths = array();
            if (!empty($spaceTypeRecord)) {
                foreach ($spaceTypeRecord as $value) {
                    $selectedFloorPlan = "";
                    if (($floorPlanType == $value->floor_plan_type_id)) {
                        $selectedFloorPlan = "selected=selected";
                        $AllocateReservedBooths = (!empty($value->available_booth)) ? unserialize($value->available_booth) : "";
                    }
                    echo '<option value="' . $value->floor_plan_type_id . '" ' . $selectedFloorPlan . '  data-available="' . $value->total_space_available . '" >' . $value->space_type . '</option>';
                } // end loop
            } //end if
            ?>
                            </select>
                        </div>

                        <!--end of row-fluid-->
                        <div class="row-fluid-15">
                            <label for="exhibition_type">Booths per Package <span class="astrik">*</span>
                                <span class="info_btn"><span class="field_info xsmall">Booths per Package</span></span>
                            </label>
            <?php
            $totalSpaceAvailable = (!empty($totalSpaceAvailable)) ? $totalSpaceAvailable : "0";
            $packageDetailslimit = (!empty($packageDetails->limit_package_available)) ? $packageDetails->limit_package_available : "0";


            if (($totalSpaceAvailable >= $packageDetailslimit) && ($totalSpaceAvailable != 0) && ($packageDetailslimit != 0)) {
                $totalSpaceAvailable = floor($totalSpaceAvailable / $packageDetails->limit_package_available);
            } else {
                $totalSpaceAvailable = 0;
            }

            $packageboothno['value'] = $totalSpaceAvailable;
            $packageboothno['max'] = $totalSpaceAvailable;
            echo form_input($packageboothno);
            ?>
                        </div>
                        <!--end of row-fluid-->

                        <div class="row-fluid-15 checkbox_row">
                            <label for="exhibition_type" class="full_width_label">Allocate Reserved Booths <span class="astrik">*</span>
                                <span class="info_btn"><span class="field_info xsmall">Allocate Reserved Booths</span></span>
                            </label>
                            <span class="checkbox_wrapper">
                                <input type="checkbox" name="tick_all" class="tick_all_checked" packageId="<?php echo $packageId; ?>" id="tick_all_<?php echo $packageId; ?>">
                                <label for="tick_all_<?php echo $packageId; ?>">Tick All</label>
                            </span>
                            <div class="all_checkbox_wrapper" id="ticket_position_div_<?php echo $packageId; ?>">
            <?php
            // get Already reserved booths for other packages for the same event id
            $reservedBoothForOtherPac = getReservedBoothForOtherPackage($packageId);

            foreach ($AllocateReservedBooths as $booth) {
                $booth = (strlen($booth) == 1) ? "0" . $booth : $booth;
                $checkedVal = (in_array($booth, $reservedBoothPositions)) ? 'checked="checked"' : "";
                $disabled = (in_array($booth, $reservedBoothForOtherPac)) ? 'disabled="disabled" checked="checked" ' : "";
                ?>
                                    <span class="checkbox_wrapper">
                                        <input type="checkbox" <?php echo $checkedVal . " " . $disabled; ?>  name="allocate_reserved_booths_<?php echo $packageId . '[]' ?>" id="tick_<?php echo $packageId . '_' . $booth; ?>" value="<?php echo $booth; ?>" data-Productid="<?php echo $packageId; ?>" class="sub_check reserved_booth"  />
                                        <label for="tick_<?php echo $packageId . '_' . $booth ?>"><?php echo (strlen($booth) == 1) ? '0' . $booth : $booth; ?></label>
                                    </span>

            <?php } ?>

                            </div>
                            <!--end of all_checkbox_wrapper -->
                        </div>
                        <!--end of row-fluid-->

        <?php } // end if  ?>

                    <hr class="panel_inner_seprator" />



        <?php
        // get purchase items 
        $packagePurchaseIds = array();
        $packagePurchaseIds = (!empty($packageDetails->purchase_Item_ids)) ? unserialize($packageDetails->purchase_Item_ids) : array();

        $getCorporatePurchaseItem = getCorporatePurchaseItem($packageDetails->id);

        if (!empty($getCorporatePurchaseItem)) {
            foreach ($getCorporatePurchaseItem as $key => $value) {
                $selectedPackagePurchase = (in_array($value->purchase_item_id, $packagePurchaseIds)) ? 'checked="checked"' : '';
                ?>
                            <div class="row-fluid-15 category_wrapper">
                                <span class="category_label pull-left">
                                    <span class="checkbox_wrapper">
                                        <input type="checkbox" id="s_insert_<?php echo $packageId . '_' . $key; ?>" value="<?php echo $value->purchase_item_id; ?>" name="purchase_items_<?php echo $packageId . '[]'; ?>" <?php echo $selectedPackagePurchase; ?>>
                                        <label for="s_insert_<?php echo $packageId . '_' . $key; ?>"><?php echo $value->name; ?></label>
                                    </span>
                                </span>
                <?php if ($value->corporate_package_id != 0) { ?> 
                                    <span class="category_btn pull-right">
                                        <a href="javascript:void(0)" class="eventsetup_btn td_btn edit_corporate_purchase" data-contant="<?php echo encode($value->purchase_item_id); ?>" data-cat-id="<?php echo $packageId; ?>"><span>&nbsp;</span></a>
                                        <a href="javascript:void(0)" class="delete_btn td_btn delete_corporate_purchase" deleteid="<?php echo encode($value->purchase_item_id); ?>"><span>&nbsp;</span></a>
                                    </span>
                <?php } else { ?>
                                    <span class="category_btn pull-right mwidth_74">
                                        <a href="javascript:void(0)" class=" "><span>&nbsp;</span></a>
                                        <a href="javascript:void(0)" class=" "><span>&nbsp;</span></a>
                                    </span>
                <?php } ?>  
                                <span class="category_value"><?php echo "$" . $value->total_price; ?></span>
                            </div>
                            <!--end of row-fluid-->
            <?php
            } // end loop
        } // end if
        ?>

                    <div class="row-fluid-15">
                        <a class="add_btn medium add_corporate_purchase" href="javascript:void(0)" id="custom_emails" data-cat-id="<?php echo $packageId; ?>">Add Purchase Items</a>
                    </div>
                    <!--end of row-fluid-->  

                    <div class="btn_wrapper" form-name="formCorporatePackageDetails_<?php echo $packageId; ?>">
                        <a href="#collapseSix" class="pull-left scroll_top"><?php echo lang('comm_top'); ?></a>

        <?php
        echo form_hidden('eventId', $eventId);
        echo form_hidden('formActionName', 'corporatePackageDetailsSave');

        //button show of save and reset
        $formButton['showbutton'] = array('save', 'reset');
        $formButton['labletext'] = array('save' => lang('comm_save_changes'), 'reset' => lang('comm_reset'));
        $formButton['buttonclass'] = array('save' => 'default_btn btn pull-right medium corporate-save', 'reset' => 'default_btn btn pull-right medium');
        $this->load->view('common_save_reset_button', $formButton);
        ?>

                    </div>
                </div>
                <input type="hidden" value="<?php echo encode($packageId); ?>" id="package_id" name="package_id" />
                </form>
            </div>
        </div>
    <?php
    } // end loop
} // end if
?>
<!--end of panel-->

<script>
    $(document).ready(function() {
        $(document).on("click", ".corporate-save", function(e) {

            var form_name = $(this).parent().attr('form-name');

            //save event bank details
            ajaxdatasave(form_name, '<?php echo $this->uri->uri_string(); ?>', true, true, false, true, true, '#showhideformdivbankSection', '#showhideformdivtermSection', false);

        });

        ////// For adding Package Benefit ////
        //ajaxdatasave('formCorporatePackage','<?php echo $this->uri->uri_string(); ?>',true,true,false,true,false,'#showhideformdiv','#showhideformdivdieteryrequirement','',true,'add_package_popup');

        $(document).on("click", ".add_corporate_benefit", function(e) {

            var packageId = $(this).attr('data-cat-id');
            var openPopupId = $("#formCorporatePackageDetails_" + packageId).parent().attr('id');

            formPostData = {popupType: 'addBenefitPopup', eventId: $("#eventId").val(), packageId: packageId, 'openPopupId': openPopupId};
            eventcustompopup('event/corporatepopup', 'add_benefit_popup', formPostData);
        });

        $(document).on("click", ".edit_corporate_benefit", function(e) {

            var packageId = $(this).attr('data-cat-id');
            var openPopupId = $("#formCorporatePackageDetails_" + packageId).parent().attr('id');

            var package_benefit_id = $(this).attr('data-contant');
            formPostData = {popupType: 'addBenefitPopup', package_benefit_id: package_benefit_id, packageId: packageId, 'openPopupId': openPopupId};
            eventcustompopup('event/corporatepopup', 'add_benefit_popup', formPostData);
        });

        // call delete function to delete the particular package benefit
        customconfirm('delete_corporate_benefit', 'event/deletedefaultpackagebenefit', '', '', '', true);

        // For adding purchase benefit item   
        $(document).on("click", ".add_corporate_purchase", function(e) {
            var packageId = $(this).attr('data-cat-id');
            var openPopupId = $("#formCorporatePackageDetails_" + packageId).parent().attr('id');
            formPostData = {popupType: 'addDefaultPurchasePopup', eventId: $("#eventId").val(), packageId: packageId, 'openPopupId': openPopupId};
            eventcustompopup('event/corporatepopup', 'add_purchase_popup', formPostData);
        });

        $(document).on("click", ".edit_corporate_purchase", function(e) {

            var packageId = $(this).attr('data-cat-id');
            var openPopupId = $("#formCorporatePackageDetails_" + packageId).parent().attr('id');

            var purchase_item_id = $(this).attr('data-contant');
            formPostData = {popupType: 'addDefaultPurchasePopup', eventId: $("#eventId").val(), purchase_item_id: purchase_item_id, packageId: packageId, 'openPopupId': openPopupId};
            eventcustompopup('event/corporatepopup', 'add_purchase_popup', formPostData);
        });

        // call delete function to delete the particular default package
        customconfirm('delete_corporate_purchase', 'event/deletedefaultpurchaseitem', '', '', '', true);


        // Exhibition Space Type Change function
        $(document).on('change', '.exhibitor_type_class', function() {

            //set collapse height
            $(this).parents(".collapse.in").css("height", 'auto');

            var availableSpace = $('option:selected', this).attr('data-available');

            floorPlanTypeId = $('option:selected', this).val();
            //get product id
            var str = $(this).attr('id');
            var res = str.split("_");

            var packageAvailable = $("#package_available_" + res[2]).val();
            if (packageAvailable > 0) {
                var boothsperpackage = 0;
                if (availableSpace * 1 >= packageAvailable * 1) {
                    var boothsperpackage = Math.floor((availableSpace * 1) / (packageAvailable * 1));

                    // get details of floor type
                    $.post(baseUrl + "event/getfloorplantype", {productId: res[2], floorPlanTypeId: floorPlanTypeId}, function(data) {
                        $("#ticket_position_div_" + res[2]).html(data);
                    });

                } // end if
                else {
                    custom_popup("Selected exhibition space type having only " + availableSpace + " Spaces.So Can not be allocated booths per package.", false);
                }

                // put Booths per Package value
                $("#refr_no_" + res[2]).val(boothsperpackage);
                $("#refr_no_" + res[2]).attr("max", boothsperpackage);


            } else {
                custom_popup("Packages Available should be greater than or equal to 1.", false);
            }

        });

        // package available Change function for reset exibition space type
        $(document).on('change', '.package_available_class', function() {

            var str = $(this).attr('id');
            var res = str.split("_");

            var packageId = res[2];

            // Reset exibition space type Value
            $('#exhibition_type_' + packageId + ' option[value=""]').attr('selected', 'selected');

            var selectedOption = $('#exhibition_type_' + packageId).find(":selected").text();
            $('#exhibition_type_' + packageId).parent(".select-wrapper").find(".holder").text(selectedOption);
            $("#refr_no_" + packageId).val("0");

            $("#ticket_position_div_" + packageId).html("");

            //set collapse height
            $(this).parents(".collapse.in").css("height", 'auto');

        });


        $(document).on('click', '.reserved_booth', function() {
            var myArray = [];
            //get product id
            var productId = $(this).attr('data-Productid');
            var boothPerPackage = $("#refr_no_" + productId).val();
            if (boothPerPackage == 0) {
                //open error message
                custom_popup("Booths per package is not available.", false);
                $(this).attr('checked', false);
            } else {
                $('input[name="allocate_reserved_booths_' + productId + '[]"]:checked').each(function() {

                    if ($(this).prop('disabled') == false) {
                        myArray.push($(this).val());
                        var checkBooth = myArray.length;
                        if (checkBooth > boothPerPackage) {
                            //open error message
                            custom_popup("Exhibition Space Type having " + boothPerPackage + " booth only.", false);
                            $(this).attr('checked', false);
                        }
                    }
                });
            }

        });

        $(document).on("click", ".editcorporatepackage", function(e) {
            formPostData = {popupType: 'addCorporatePackagePopup', eventId: $("#eventId").val(), packageId: $(this).attr('data-cat-id')};
            eventcustompopup('event/corporatepopup', 'add_package_popup', formPostData);
        });

        $(document).on("click", ".copycorporatepackage", function(e) {
            formPostData = {isCopy: '1', popupType: 'addCorporatePackagePopup', eventId: $("#eventId").val(), packageId: $(this).attr('data-cat-id')};
            eventcustompopup('event/corporatepopup', 'add_package_popup', formPostData);
        });

        $(document).on("click", ".checkpassreq", function(e) {
            //set collapse height
            $(this).parents(".collapse.in").css("height", 'auto');

            var packageId = $(this).attr('packageId');
            if ($(this).val() == 1) {

                $("#pass_req_div_" + packageId).show();

                //$("#password_"+packageId).attr('required',"");
                //$("#con_password_"+packageId).attr('required',"");

            } else {
                $("#pass_req_div_" + packageId).hide();
                /*  $("#password_"+packageId).removeAttr('required');
                 $("#con_password_"+packageId).removeAttr('required');
                 
                 $("#password_"+packageId).removeAttr('data-parsley-required');
                 $("#con_password_"+packageId).removeAttr('data-parsley-required');
                 */
            }

        });

        $(document).on("click", ".tick_all_checked", function(e) {
            //set collapse height

            var packageId = $(this).attr('packageId');
            var boothPerPackage = $("#refr_no_" + packageId).val();

            var checkedCount = 0;
            if ($(this).prop("checked")) {

                $("#ticket_position_div_" + packageId + " input[type=checkbox]").each(function() {
                    if ($(this).attr("disabled")) {
                    } else {
                        $(this).attr("checked", true);
                    }
                });

                /*$("#ticket_position_div_"+packageId+" input[type=checkbox]").each(function () {
                 if($(this).attr("disabled")){
                 }else{
                 checkedCount++;
                 }
                 });
                 
                 if(checkedCount>=boothPerPackage){
                 $("#ticket_position_div_"+packageId+" input[type=checkbox]").each(function () {
                 $(this).attr("checked",true);
                 });
                 }else{
                 custom_popup("Do not having space to allocated Booths per Package",false);
                 } 
                 
                 }else{
                 $("#ticket_position_div_"+packageId+" input[type=checkbox]").each(function () {
                 if($(this).attr("disabled")){}else{$(this).attr("checked",false);}
                 });*/
            } else {
                $("#ticket_position_div_" + packageId + " input[type=checkbox]").each(function() {
                    if ($(this).attr("disabled")) {
                    } else {
                        $(this).attr("checked", false);
                    }
                });
            }

        });

        // call delete function to delete the particular default package
        customconfirm('deletecorporatepackage', 'event/deletecorporatepackage', '', '', '', true);
    });

    //Function to show/hide password on click show/hide checkbox
    function textpass(elID, elIDc) {
        var el = document.getElementById(elID) || false;
        var elc = document.getElementById(elIDc) || false;
        if (!el) {
            return;
        }
        if (el.getAttribute('type') == 'text') {
            el.setAttribute('type', 'password');
        }
        else {
            el.setAttribute('type', 'text');
        }
        if (!elc) {
            return;
        }
        if (elc.getAttribute('type') == 'text') {
            elc.setAttribute('type', 'password');
        }
        else {
            elc.setAttribute('type', 'text');
        }
    }
</script>
