<?php
if (!empty($default_purchase_item)) {
    $purchaseItemId = encode($default_purchase_item[0]->purchase_item_id);
    $name = $default_purchase_item[0]->name;
    $limit = $default_purchase_item[0]->item_limit;
    $totalPrice = $default_purchase_item[0]->total_price;
    $gstIncludePrice = $default_purchase_item[0]->gst_include_price;
} else {
    $purchaseItemId = "";
    $name = "";
    $limit = "";
    $totalPrice = "";
    $gstIncludePrice = "";
}

//Default Benefit 
$formCorporateBenefit = array(
    'name' => 'formCorporateDefaultPurchaseitem',
    'id' => 'formCorporateDefaultPurchaseitem',
    'method' => 'post',
    'class' => 'wpcf7-form',
    'data-parsley-validate' => '',
);

$formSatchelInsertName = array(
    'name' => 'purchase_name',
    'value' => $name,
    'id' => 'purchase_name',
    'type' => 'text',
    'required' => '',
    'class' => 'small',
    'autocomplete' => 'off',
    'data-parsley-error-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
);

$formSatchelInsertLimit = array(
    'name' => 'purchase_limit',
    'value' => $limit,
    'id' => 'purchase_limit',
    'type' => 'text',
    'required' => '',
    'class' => 'small short_field ',
    'autocomplete' => 'off',
    'data-parsley-type' => 'number',
    'data-parsley-type-message' => lang("enter_numeric_value"),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
);

$formSatchelInsertPrice = array(
    'name' => 'purchase_price',
    'value' => $totalPrice,
    'id' => 'purchase_price',
    'type' => 'text',
    'class' => 'small short_field totalPriceEnter',
    'required' => '',
    'autocomplete' => 'off',
    'data-parsley-type' => 'number',
    //'data-parsley-error-message' => lang('common_field_required'),
    'data-parsley-type-message' => lang("enter_numeric_value"),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
);

$formSatchelInsertGstPrice = array(
    'name' => 'purchase_price_gst',
    'value' => $gstIncludePrice,
    'id' => 'purchaseGSTVal',
    'type' => 'text',    
    'readonly' => true,
    'required' => '',
    'class' => 'disable_input small short_field',
    'autocomplete' => 'off',
    'data-parsley-error-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
);
?>

<!--end of pop3-->
<div id="add_purchase_popup" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header delegates">
                <h4 class="medium dt-large modal-title"><?php echo ($purchaseItemId == "") ? lang('add_corporate_pop_default_purchase_item') : lang('edit_corporate_pop_default_purchase_item'); ?></h4>
            </div>

            <div class="modal-body small">
                <div class="modelinner ">
                    <?php echo form_open($this->uri->uri_string(), $formCorporateBenefit); ?>
                    <input type="hidden" value="<?php echo (isset($openPopupId) && $openPopupId != "") ? $openPopupId : "collapseThree"; ?>" name="collapseValue">
                    <div class="control-group mb10 eventdashboard_popup small">

                        <div class="row-fluid">
                            <label class="pull-left" for="purchase_name"><?php echo lang('corporate_default_purchase_name'); ?> <span class="astrik">*</span>
                                <span class="info_btn"><span class="field_info xsmall"><?php echo lang('title_corporate_default_purchase_name'); ?></span></span>
                            </label>
                            <?php echo form_input($formSatchelInsertName); ?> 
                        </div>

                        <div class="row-fluid">
                            <label class="pull-left" for="purchase_limit"><?php echo lang('corporate_default_purchase_limit'); ?> <span class="astrik">*</span>
                                <span class="info_btn"><span class="field_info xsmall"><?php echo lang('title_corporate_default_purchase_limit'); ?></span></span>
                            </label>
                            <?php echo form_input($formSatchelInsertLimit); ?>  
                        </div>

                        <div class="row-fluid">
                            <label class="pull-left" for="purchase_price"><?php echo lang('corporate_default_purchase_total_price'); ?><span class="astrik">*</span>
                            </label>
                            <?php echo form_input($formSatchelInsertPrice); ?>
                        </div>

                        <div class="row-fluid">
                            <label class="pull-left" for="purchase_price"><?php echo lang('corporate_default_purchase_gst_price'); ?>
                            </label>
                            <?php echo form_input($formSatchelInsertGstPrice); ?>
                        </div>


                        <div class="btn_wrapper">
                            <?php
                            echo form_hidden('formActionName', 'corporateDefaultPurchaseItem');
                            echo form_hidden('purchaseitemid', $purchaseItemId);
                            echo form_hidden('packageId', $packageId);


                            $extraSave = 'class="submitbtn pull-right medium" id="addDefaultPurchaseitem" ';
                            echo form_submit('save', lang('comm_save_changes'), $extraSave);

                            $extraSave = 'class="popup_cancel submitbtn pull-right medium" data-dismiss="modal" ';
                            echo form_submit('cancle', lang('comm_cancle'), $extraSave);
                            ?>
                        </div>

                    </div>
<?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<!--end of pop4-->

<script >
$(document).ready(function() {
    $("#formCorporateDefaultPurchaseitem").parsley();                                                           
    var gstRate = parseInt('<?php echo (!empty($eventdetails->gst_rate)) ? $eventdetails->gst_rate : "0"; ?>'); 
    /*----This function is used to unit price enter and show-----*/
    unitPriceManage('totalPriceEnter', false, 'purchaseGSTVal', 'purchase_price', gstRate);
});
</script>
