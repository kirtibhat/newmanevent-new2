<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
  |--------------------------------------------------------------------------|
  | This Field define for temparay open popup by add section id        |
  | and edit record id                           |
  |--------------------------------------------------------------------------|
 */

$tempHidden = array(
    'type' => 'hidden',
    'name' => 'tempHidden',
    'id' => 'tempHidden',
    'value' => '',
);

echo form_input($tempHidden);

$this->load->view('event/event_menus');
?>

<div class="row-fluid-15">
    <div id="page_content" class="span9">

        <div id="accordion" class="panel-group">
            <?php
            // load corporate floor plan
            $this->load->view('form_corporate_floor_plan');

            // load default package benefit
            $this->load->view('form_default_package_benefit');

            // load default additional purchase item
            $this->load->view('default_additional_purchase_item');

            /*
             * this view load master personal details for set for new
             * created registrant personal details form
             */

            $this->load->view('corporate_application_details');

            // load corporate package details
            $this->load->view('add_corporate_package');



            // load corporate category
            //$this->load->view('add_corporate_category');
            // load corporate package details
            $this->load->view('add_corporate_package_details');
            ?>
        </div>

            <?php
            //next and previous button
            $buttonData['viewbutton'] = array('next', 'preview', 'back');
            $buttonData['linkurl'] = array('back' => base_url('event/eventdetails'), 'next' => base_url('event/setupregistrant'), 'preview' => '');
            $this->load->view('common_back_next_buttons', $buttonData);
            ?>

        <!--end of page btn wrapper-->  
    </div>
    <!--end of page content-->
</div>
<!--end of row-->


<div id="opentModelBox"></div>

<!--start custom tooltip-->
<div id="tooltip_wrapper"></div>
<!--end custom tooltip-->


<script>
$(document).ready(function() {
    var gstRate = parseInt('<?php echo (!empty($eventdetails->gst_rate)) ? $eventdetails->gst_rate : "0"; ?>');
    /*----This function is used to unit price enter and show-----*/
    unitPriceManage('packageTotalPriceEnter', 'packageId', 'packageGSTVal', 'package_totalprice_', gstRate, 'packageGSTIncVal');

    $(document).on('keyup', '.filterpackage', function(){
        var filter_key = $(this).data('key');
        var filter_text = $(this).val();
        //var filter_text = filter_text.toLowerCase();
        console.log(filter_key + "====" + filter_text);
        if(filter_text){             
            $('.filter_packages'+filter_key).hide();
            $(".accordion_content").css("height", 'auto');
            $("h4[class*="+filter_text+"]").show();    
            //console.log("h4[class*="+filter_text+"]").html();    
        } else {
           $('.filter_packages'+filter_key).show();
           $(".accordion_content").css("height", 'auto');
        }
    });
    
});
</script> 
