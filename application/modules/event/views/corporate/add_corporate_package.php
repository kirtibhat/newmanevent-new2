<p class="small">
				Select the fields below that you would like to display, hide or have as mandatory on the <br/>
				Registration form. If there is a additional field that you need for your event that is not shown <br/>
				here, click on the plus and name the field you need.
				<span class="clearfix"></span>
				<a href="javascript:void(0)" class="btn medium add_package" id="add_corporate_package"><?php echo lang('add_corporate_package'); ?></a>
				<?php echo form_hidden('eventId',$eventId); ?>
</p>

<script>
$(document).ready(function() { 
    ajaxdatasave('formCorporatePackage','<?php echo $this->uri->uri_string(); ?>',true,true,false,true,false,'#showhideformdiv','#showhideformdivdieteryrequirement','',true,'add_package_popup');

    $(document).on("click", "#add_corporate_package", function(e){
        formPostData = {popupType:'addCorporatePackagePopup',eventId:$("#eventId").val()};
        eventcustompopup('event/corporatepopup','add_package_popup',formPostData);
    });
});
</script>
