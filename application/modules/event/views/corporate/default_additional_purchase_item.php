<?php
$deafultAdditionalPurchaseItem = array(
    'name' => 'deafultAdditionalPurchaseItem',
    'id' => 'deafultAdditionalPurchaseItem',
    'method' => 'post',
    'class' => 'form-horizontal',
    'data-parsley-validate' => '',
);
?>
<div class="panel event_panel">
    <div class="panel-heading ">
        <h4 class="panel-title medium dt-large ">
            <a class=" " data-toggle="collapse" data-parent="#accordion" href="#collapseThree"><?php echo lang('corporate_default_additional_purchase_item'); ?></a>
        </h4>
    </div>
    <div style="height: auto;" id="collapseThree" class="accordion_content collapse apply_content <?php echo getIsCollapseOpen("collapseThree"); ?>">
       <?php echo form_open($this->uri->uri_string(), $deafultAdditionalPurchaseItem); ?> 
            <div class="panel-body ls_back dashboard_panel small" id="showhideformdivdefaultaddpurchaseitem">


                <!--end of row-fluid-->
                <?php if (!empty($corporatedefaultpurchaseitem)) {
                    foreach ($corporatedefaultpurchaseitem as $key => $value) {
                        ?>
                        <div class="row-fluid-15 category_wrapper" id="purchase_item_div_<?php echo $key; ?>">
                            <span class="category_label pull-left"><?php echo $value->name; ?></span>
                            <span class="category_btn pull-right">
                                <a class="eventsetup_btn td_btn editdefaultpurchaseitem" data-contant="<?php echo encode($value->purchase_item_id); ?>" href="javascript:void(0)"><span>&nbsp;</span></a>
                                <a class="delete_btn td_btn deletedefaultpurchaseitem" deleteid="<?php echo encode($value->purchase_item_id); ?>" href="javascript:void(0)"><span>&nbsp;</span></a>
                            </span>
                            <span class="category_value pull-right">$<?php echo $value->total_price; ?></span>
                        </div>

                        <?php
                    } // end loop
                }//end if 
                ?>
                <!--end of row-fluid-->

                <div class="row-fluid-15">
                    <a id="add_purchase" href="javascript:void(0)" class="add_btn medium"><?php echo lang('corporate_default_purchase_item'); ?></a>
                </div>

                <div class="btn_wrapper ">
                    <a href="#collapseThree" class="pull-left scroll_top"><?php echo lang('comm_top'); ?></a>
                    <?php
                    echo form_hidden('formActionName', 'deafultAdditionalPurchaseItem');
                    echo form_input(array('type' => 'hidden', 'value' => $eventId, 'name' => 'eventId', 'id' => 'eventId'));
                    $extraSave = 'class="default_btn btn pull-right medium" id="floor_plan_btn" ';
                    echo form_submit('save', lang('comm_save_changes'), $extraSave);

                    $extraSave = 'class="default_btn btn pull-right medium reset_form" ';
                    echo form_button('clear', lang('comm_reset'), $extraSave);
                    ?>
                </div>

            </div>
            <?php echo form_input(array('type' => 'hidden', 'value' => $eventId, 'name' => 'eventId', 'id' => 'eventId')); ?>
        </form>
    </div>
</div>
<!--end of panel-->


<script>
$(document).ready(function() {
    //Save for value and show next form
    ajaxdatasave('deafultAdditionalPurchaseItem', '<?php echo $this->uri->uri_string(); ?>', false, false, false, false, true, '#showhideformdivdefaultaddpurchaseitem', '#showhideformApplicationDetails');

    $(document).on("click", ".error_close_button", function(e) {
        // check model popup
        $("#add_purchase_popup").modal({keyboard: false}); // show the model
    });

    $(document).on("click", "#add_purchase", function(e) {
        formPostData = {popupType: 'addDefaultPurchasePopup', eventId: $("#eventId").val()};
        //eventcustompopup('event/corporatepopup','add_purchase_popup',formPostData);
        //call ajax popup function
        ajaxpopupopen('add_purchase_popup', 'event/corporatepopup', formPostData, 'add_purchase');
    });

    $(document).on("click", ".editdefaultpurchaseitem", function(e) {
        var purchase_item_id = $(this).attr('data-contant');
        formPostData = {popupType: 'addDefaultPurchasePopup', eventId: $("#eventId").val(), purchase_item_id: purchase_item_id};
        //eventcustompopup('event/corporatepopup','add_purchase_popup',formPostData);
        //call ajax popup function
        ajaxpopupopen('add_purchase_popup', 'event/corporatepopup', formPostData, 'editdefaultpurchaseitem');
    });

    // call delete function to delete the particular default package
    customconfirm('deletedefaultpurchaseitem', 'event/deletedefaultpurchaseitem', '', '', '', true,'',false,'<?php echo lang('default_purchase_deleted_success') ?>');

    //Save Addtional Purchase Item 
    ajaxdatasave('formCorporateDefaultPurchaseitem', '<?php echo $this->uri->uri_string(); ?>', true, true, false, true, false, '#showhideformdivdefaultaddpurchaseitem', '#showhideformdivdefaultaddpurchaseitem', '', true, 'add_purchase_popup');
});
</script> 

