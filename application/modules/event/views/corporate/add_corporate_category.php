<div class="panel event_panel co_cat co_cat_corporate">
    <div class="panel-heading ">
        <h4 class="panel-title medium dt-large ">
            <a class=" " data-toggle="collapse" data-parent="#accordion" href="#collapseFive"><?php echo lang('corporate_category'); ?></a>
            <input type="text" name="" value="" id="filterpackage" placeholder="Filter Package" class="small short_field">
        </h4>
    </div>
    <div style="height: auto;" id="collapseFive" class="accordion_content collapse apply_content <?php echo getIsCollapseOpen("collapseFive"); ?>">
        <form>
            <div class="panel-body ls_back dashboard_panel small co_category">
                <!--<ul class="medium category_ul">
                <?php /* ?><?php if(!empty($eventCorporateCategories)){ ?>
                  <?php foreach($eventCorporateCategories as $value){ ?>
                  <li><?php echo $value->category; ?></li>
                  <?php } // end loop
                  } // end if
                  echo "<li>All Packages</li>";
                  ?> <?php */ ?>
                </ul>-->




                <div id="accordion_sub" class="panel-group sub-panel-group">
                    <div class="panel event_panel sub_panel">
                        <div class="panel-heading ">
                            <h4 class="panel-title medium dt-large">
                                <a class="" data-toggle="collapse" data-parent="#accordion_sub" href="#collapseSubOne">
                                    Default Package
                                </a>
                                <span class="heading_btn_wrapper pull-right">
                                    <a href="javascript:void(0)" class="eventsetup_btn td_btn editcorporatepackage default_catbtn" data-cat-id=""><span>&nbsp;</span></a>
                                </span>
                            </h4>
                        </div>
                        <!--end of panel heading-->

                        <div id="collapseSubOne" class="accordion_content collapse apply_content ">
                            <div class="row-fluid-15">
                                <label for="info_org">Name of Stream 1 <span class="astrik">*</span></label>
                                <input type="text" value="Stream 1" id="info_org" name="info_org" class=" small">
                            </div>
                            <!--end of row-fluid-->

                            <div class="row-fluid-15">
                                <label for="info_org">Location </label>
                                <input type="text" value="Location" id="info_org" name="info_org" class=" small">
                            </div>
                            <!--end of row-fluid-->

                            <hr class="panel_inner_seprator">

                            <div class="row-fluid-15">
                                <label for="info_org">Session 2 Topic <span class="astrik">*</span>
                                    <span class="info_btn">
                                        <span class="field_info xsmall">This date is the  last day you will accept earlybird registrations
                                        </span>
                                    </span>
                                </label>
                                <input type="text" value="Session Topic" id="info_org" name="info_org" class=" small">
                            </div>
                            <!--end of row-fluid-->

                        </div>  
                        <!--end of panel content-->
                    </div>
                    <!--end of panel-->  

                    <div class="panel event_panel sub_panel">
                        <div class="panel-heading ">
                            <h4 class="panel-title medium dt-large">
                                <a class="" data-toggle="collapse" data-parent="#accordion_sub" href="#collapseSubTwo">
                                    Gold
                                </a>
                                <span class="heading_btn_wrapper pull-right">
                                    <a href="javascript:void(0)" class="delete_btn td_btn deletecorporatepackage" deleteid="37"><span>&nbsp;</span></a>
                                    <a href="javascript:void(0)" class="eventsetup_btn td_btn editcorporatepackage" data-cat-id="37"><span>&nbsp;</span></a>
                                    <a href="javascript:void(0)" class="copy_btn td_btn copycorporatepackage" data-cat-id="37"><span>&nbsp;</span></a>
                                </span>
                            </h4>
                        </div>
                        <!--end of panel heading-->

                        <div id="collapseSubTwo" class="accordion_content collapse apply_content ">
                            <div class="row-fluid-15">
                                <label for="info_org">Name of Stream 1 <span class="astrik">*</span></label>
                                <input type="text" value="Stream 1" id="info_org" name="info_org" class=" small">
                            </div>
                            <!--end of row-fluid-->

                            <div class="row-fluid-15">
                                <label for="info_org">Location </label>
                                <input type="text" value="Location" id="info_org" name="info_org" class=" small">
                            </div>
                            <!--end of row-fluid-->

                            <hr class="panel_inner_seprator">

                            <div class="row-fluid-15">
                                <label for="info_org">Session 2 Topic <span class="astrik">*</span>
                                    <span class="info_btn">
                                        <span class="field_info xsmall">This date is the  last day you will accept earlybird registrations
                                        </span>
                                    </span>
                                </label>
                                <input type="text" value="Session Topic" id="info_org" name="info_org" class=" small">
                            </div>
                            <!--end of row-fluid-->

                        </div>  
                        <!--end of panel content-->
                    </div>
                    <!--end of panel-->  

                    <div class="panel event_panel sub_panel">
                        <div class="panel-heading ">
                            <h4 class="panel-title medium dt-large">
                                <a class="" data-toggle="collapse" data-parent="#accordion_sub" href="#collapseSubThree">
                                    Silver
                                </a>
                                <span class="heading_btn_wrapper pull-right">
                                    <a href="javascript:void(0)" class="delete_btn td_btn deletecorporatepackage" deleteid="37"><span>&nbsp;</span></a>
                                    <a href="javascript:void(0)" class="eventsetup_btn td_btn editcorporatepackage" data-cat-id="37"><span>&nbsp;</span></a>
                                    <a href="javascript:void(0)" class="copy_btn td_btn copycorporatepackage" data-cat-id="37"><span>&nbsp;</span></a>
                                </span>
                            </h4>
                        </div>
                        <!--end of panel heading-->

                        <div id="collapseSubThree" class="accordion_content collapse apply_content ">
                            <div class="row-fluid-15">
                                <label for="info_org">Name of Stream 1 <span class="astrik">*</span></label>
                                <input type="text" value="Stream 1" id="info_org" name="info_org" class=" small">
                            </div>
                            <!--end of row-fluid-->

                            <div class="row-fluid-15">
                                <label for="info_org">Location </label>
                                <input type="text" value="Location" id="info_org" name="info_org" class=" small">
                            </div>
                            <!--end of row-fluid-->

                            <hr class="panel_inner_seprator">

                            <div class="row-fluid-15">
                                <label for="info_org">Session 2 Topic <span class="astrik">*</span>
                                    <span class="info_btn">
                                        <span class="field_info xsmall">This date is the  last day you will accept earlybird registrations
                                        </span>
                                    </span>
                                </label>
                                <input type="text" value="Session Topic" id="info_org" name="info_org" class=" small">
                            </div>
                            <!--end of row-fluid-->

                        </div>  
                        <!--end of panel content-->
                    </div>
                    <!--end of panel-->  

                    <div class="panel event_panel sub_panel">
                        <div class="panel-heading ">
                            <h4 class="panel-title medium dt-large">
                                <a class="" data-toggle="collapse" data-parent="#accordion_sub" href="#collapseSubFour">
                                    Platinum
                                </a>
                                <span class="heading_btn_wrapper pull-right">
                                    <a href="javascript:void(0)" class="delete_btn td_btn deletecorporatepackage" deleteid="37"><span>&nbsp;</span></a>
                                    <a href="javascript:void(0)" class="eventsetup_btn td_btn editcorporatepackage" data-cat-id="37"><span>&nbsp;</span></a>
                                    <a href="javascript:void(0)" class="copy_btn td_btn copycorporatepackage" data-cat-id="37"><span>&nbsp;</span></a>
                                </span>
                            </h4>
                        </div>
                        <!--end of panel heading-->

                        <div id="collapseSubFour" class="accordion_content collapse apply_content ">
                            <div class="row-fluid-15">
                                <label for="info_org">Name of Stream 1 <span class="astrik">*</span></label>
                                <input type="text" value="Stream 1" id="info_org" name="info_org" class=" small">
                            </div>
                            <!--end of row-fluid-->

                            <div class="row-fluid-15">
                                <label for="info_org">Location </label>
                                <input type="text" value="Location" id="info_org" name="info_org" class=" small">
                            </div>
                            <!--end of row-fluid-->

                            <hr class="panel_inner_seprator">

                            <div class="row-fluid-15">
                                <label for="info_org">Session 2 Topic <span class="astrik">*</span>
                                    <span class="info_btn">
                                        <span class="field_info xsmall">This date is the  last day you will accept earlybird registrations
                                        </span>
                                    </span>
                                </label>
                                <input type="text" value="Session Topic" id="info_org" name="info_org" class=" small">
                            </div>
                            <!--end of row-fluid-->

                        </div>  
                        <!--end of panel content-->
                    </div>
                    <!--end of panel-->  

                </div>
                <!--end of panel-group-->
                <!--end of categories-->



            </div>
        </form>
    </div>
</div>
<!--end of panel-->
