<?php
$deafultPackageBenefit = array(
    'name' => 'deafultPackageBenefit',
    'id' => 'deafultPackageBenefit',
    'method' => 'post',
    'class' => 'form-horizontal',
    'data-parsley-validate' => '',
);
?>
<div class="panel event_panel">
    <div class="panel-heading ">
        <h4 class="panel-title medium dt-large ">
            <a class=" " data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"><?php echo lang('corporate_default_package_benefit'); ?></a>
        </h4>
    </div>
    <div style="height: auto;" id="collapseTwo" class="accordion_content collapse apply_content <?php echo getIsCollapseOpen("collapseTwo"); ?>">        
        <?php echo form_open($this->uri->uri_string(), $deafultPackageBenefit); ?> 
            <div class="panel-body ls_back dashboard_panel small" id="showhideformdivcorporatedefaultbenefit">


                <?php
                if (!empty($corporatedefaultPacBenefit)) {
                    foreach ($corporatedefaultPacBenefit as $key => $value) {
                        ?>  
                        <div class="row-fluid-15 category_wrapper" id="benefit_div_id_<?php echo $key; ?>">
                            <span class="category_label pull-left"><?php echo $value->benefit_name; ?></span>
                            <span class="category_btn pull-right">
                                <a href="javascript:void(0)" class="eventsetup_btn td_btn editdefaultpackagebenifit" data-contant="<?php echo encode($value->package_benefit_id); ?>" ><span>&nbsp;</span></a>
                                <a href="javascript:void(0)" class="delete_btn td_btn deletedefaultpackagebenifit" deleteid="<?php echo encode($value->package_benefit_id); ?>" ><span>&nbsp;</span></a>
                            </span>
                        </div>

                    <?php
                    } // end loop
                } // end if
                ?>

                <!--end of row-fluid-->

                <div class="row-fluid-15">
                    <a href="javascript:void(0)" class="add_btn medium" id="add_benefit"><?php echo lang('corporate_default_add_benefit'); ?></a>
                </div>
                <!--end of row-fluid-->

                <div class="btn_wrapper ">
                    <a href="#collapseTwo" class="pull-left scroll_top"><?php echo lang('comm_top'); ?></a>
                    <?php
                    echo form_hidden('formActionName', 'deafultPackageBenefit');
                    echo form_input(array('type' => 'hidden', 'value' => $eventId, 'name' => 'eventId', 'id' => 'eventId'));
                    $extraSave = 'class="default_btn btn pull-right medium" id="floor_plan_btn" ';
                    echo form_submit('save', lang('comm_save_changes'), $extraSave);

                    $extraSave = 'class="default_btn btn pull-right medium reset_form" ';
                    echo form_button('clear', lang('comm_reset'), $extraSave);
                    ?>

                </div>
            </div>
        </form>
    </div>
</div>
<!--end of panel-->


<script>
$(document).ready(function() {
    ajaxdatasave('formCorporateDefaultBenefit', '<?php echo $this->uri->uri_string(); ?>', true, true, false, true, false, '#showhideformdivcorporatedefaultbenefit', '#showhideformdivdefaultaddpurchaseitem', '', true, 'add_benefit_popup');

    ajaxdatasave('deafultPackageBenefit', '<?php echo $this->uri->uri_string(); ?>', false, false, false, false, true, '#showhideformdivcorporatedefaultbenefit', '#showhideformdivdefaultaddpurchaseitem');

    /*  
     $(document).on("click", ".error_close_button", function(e){
     // check model popup
     $("#add_benefit_popup").modal({ keyboard: false}); // show the model
     });
     */

    $(document).on("click", "#add_benefit", function(e) {
        formPostData = {popupType: 'addBenefitPopup'};
        //call ajax popup function
        ajaxpopupopen('add_benefit_popup', 'event/corporatepopup', formPostData, 'add_benefit');
        //eventcustompopup('event/corporatepopup','add_benefit_popup',formPostData);
    });

    $(document).on("click", ".editdefaultpackagebenifit", function(e) {
        var package_benefit_id = $(this).attr('data-contant');
        formPostData = {popupType: 'addBenefitPopup', package_benefit_id: package_benefit_id};
        //eventcustompopup('event/corporatepopup','add_benefit_popup',formPostData);
        //call ajax popup function
        ajaxpopupopen('add_benefit_popup', 'event/corporatepopup', formPostData, 'editdefaultpackagebenifit');
    });

    // call delete function to delete the particular default package
    customconfirm('deletedefaultpackagebenifit', 'event/deletedefaultpackagebenefit', '', '', '', true,'',false,'<?php echo lang('corporate_benefit_delete_success') ?>');

    $(document).on("click", ".stepper-step", function(e) {

        var qty = $("#benefit_qty").val();

        if ($(this).hasClass('up')) { // increment
            if (qty < 10) {
                var currentId = 'benefits_' + qty;
                $("#benefit_qty").val((qty * 1) + 1);
                $(".qty_fields").append('<input type="text" class="small" value="" required  data-parsley-error-message = "<?php echo lang('common_field_required'); ?>" data-parsley-error-class="custom_li" name="benefits[]" id="' + currentId + '" />');
            }
        } else {
            var currentId = 'benefits_' + (qty * 1 - 1);
            if (qty > 1) {
                $("#benefit_qty").val((qty * 1) - 1);
                $("#" + currentId).remove();
            }
        }


    });
});
</script>
