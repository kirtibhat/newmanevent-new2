<?php
if ($packageDetails) {
    $packageDetailId = $packageDetails->id;
    $packageDetailName = $packageDetails->corporate_package_name;
    $packageDetailCategory = $packageDetails->category_id;
} else {
    $packageDetailId = "";
    $packageDetailName = "";
    $packageDetailCategory = "";
}

//Default Benefit 
$formCorporatePackage = array(
    'name' => 'formCorporatePackage',
    'id' => 'formCorporatePackage',
    'method' => 'post',
    'class' => 'wpcf7-form',
    'data-parsley-validate' => '',
);

$formCorporateName = array(
    'name' => 'corporate_package_name',
    'value' => $packageDetailName,
    'id' => 'corporate_package_name',
    'type' => 'text',
    'required' => '',
    'class' => 'small',
    'autocomplete' => 'off',
    'data-parsley-error-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
);
?>
<!-- Modal -->
<div id="add_package_popup" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header delegates">
                <h4 class="medium dt-large modal-title"><?php echo ($packageDetailId == "") ? 'Add Corporate Package' : 'Edit Corporate Package'; ?></h4>
            </div>

            <div class="modal-body small">
                <div class="modelinner ">
                    <?php echo form_open($this->uri->uri_string(), $formCorporatePackage); ?>
                    <input type="hidden" value="collapseFive" name="collapseValue">
                    <?php echo form_hidden('packageId', $packageDetailId); ?>
                    <div class="control-group mb10 eventdashboard_popup small">

                        <div class="row-fluid">
                            <label class="pull-left" for="ctype">Name   <span class="astrik">*</span>
                                <span class="info_btn"><span class="field_info xsmall">Name of corporate package</span></span>
                            </label>
                            <?php echo form_input($formCorporateName); ?>
                        </div>

                        <div class="row-fluid">
                            <label class="pull-left" for="ctype_details">Category  <span class="astrik">*</span>
                                <span class="info_btn"><span class="field_info xsmall">Corporate Category</span></span>
                            </label>

                            <?php
                            $other = 'id="corporate_package_id" class="small custom-select-corporate short_field" required="" autocomplete="off" data-parsley-error-message="' . lang('common_field_required') . '" data-parsley-error-class="custom_li" data-parsley-trigger="keyup" ';
                            $eventCorporateCategories = getDataFromTabel('event_categories', '*', array('is_corporate' => '1', 'event_id' => $eventId));
                            $option[""] = "Select Category";
                            if (!empty($eventCorporateCategories)) {
                                foreach ($eventCorporateCategories as $key => $value) {
                                    $option[$value->category_id] = $value->category;
                                }
                            }//end if 
                            echo form_dropdown('corporate_package_id', $option, $packageDetailCategory, $other);
                            ?>


                        </div>

                        <div class="btn_wrapper">
                            <?php
                            echo form_hidden('formActionName', 'formCorporatePackage');
                            echo form_hidden('eventId', $eventId);
                            echo form_hidden('isCopyAction', $isCopy);
                            //echo form_hidden('purchaseitemid',$purchaseItemId);
                            //button show of save and reset
                            $formButton['showbutton'] = array('save');
                            $formButton['labletext'] = array('save' => lang('comm_save_changes'));
                            $formButton['buttonclass'] = array('save' => 'submitbtn pull-right medium');
                            $this->load->view('event/common_save_reset_button', $formButton);
                            ?>
                            <input type="submit" class="popup_cancel submitbtn pull-right medium" value="Cancel" name="logincancel" data-dismiss="modal">

                        </div>

                    </div>
<?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        ini_custom_dropdown('custom-select-corporate');
        $("#formCorporatePackage").parsley();
    });
</script>

