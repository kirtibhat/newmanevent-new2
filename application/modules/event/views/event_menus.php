<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$method = $this->router->fetch_method();
?>

<div id="dashboardnav_intro">
        <div id="dashboardnav_intro_left" class="small">
        <h1 class="xlarge"><?php echo lang('event_event_setup_title'); ?></h1>
        </div>
</div>

 <ul id="ed_nav" class="small">
       
   <li class="event_detail">
    <a href="<?php echo base_url('event/eventdetails'); ?>" class="page_leave <?php echo ($method=='eventdetails')?"active":""; ?>">
        <span><img alt="launch icon" src="<?php echo IMAGE; ?>de_nav.png"></span>
         <span class="nav_text"><?php echo lang('event_event_details'); ?></span>
    </a>
  </li>
   
  <li class="corporate">
    <a href="<?php echo base_url('event/corporatedetails'); ?>" class="page_leave <?php echo ($method=='corporatedetails')?"active":""; ?>">
      <span><img alt="launch icon" src="<?php echo IMAGE; ?>de_nav.png"></span>
      <span class="nav_text"><?php echo lang('event_setup_corporate'); ?></span>
     </a>
  </li>    
  
  <li class="registertion" >
    <a href="<?php echo base_url('event/setupregistrant'); ?>" class="page_leave <?php echo ($method=='setupregistrant')?"active":""; ?>" >
      <span><img alt="launch icon" src="<?php echo IMAGE; ?>de_nav.png"></span>
      <span class="nav_text"><?php echo lang('event_setup_registrant'); ?></span>
    </a>
  </li>    
  
  <li class="program">
    <a href="<?php echo base_url('event/programdetails'); ?>" class="page_leave <?php echo ($method=='programdetails')?"active":""; ?>">
      <span><img alt="launch icon" src="<?php echo IMAGE; ?>de_nav.png"></span>
      <span class="nav_text"><?php echo lang('event_setup_program'); ?></span>
    </a>
   </li>    
  
  <li class="side_event">
    <a href="<?php echo base_url('event/setupsideevents'); ?>" class="page_leave <?php echo ($method=='setupsideevents')?"active":""; ?>">
      <span><img alt="launch icon" src="<?php echo IMAGE; ?>de_nav.png"></span>
      <span class="nav_text"><?php echo lang('event_setup_side_event'); ?></span>
    </a>
  </li>    
  
  <li class="extras">
    <a href="<?php echo base_url('event/setupextras'); ?>" class="page_leave <?php echo ($method=='setupextras')?"active":""; ?>">
      <span><img alt="launch icon" src="<?php echo IMAGE; ?>de_nav.png"></span>
      <span class="nav_text"><?php echo lang('event_setup_extras'); ?></span>
    </a>
  </li>    
  
  <li class="payment_opt">
    <a href="<?php echo base_url('event/setuppayment') ?>"  class="page_leave <?php echo ($method=='setuppayment')?"active":""; ?>">
      <span><img alt="launch icon" src="<?php echo IMAGE; ?>de_nav.png"></span>
      <span class="nav_text"><?php echo lang('event_setup_payment_options'); ?></span>
    </a>
  </li>    
  <li class="customize_form">
    <a href="<?php echo base_url('event/customizeforms') ?>" class="page_leave <?php echo ($method=='customizeforms')?"active":""; ?>">
      <span><img alt="launch icon" src="<?php echo IMAGE; ?>de_nav.png"></span>
      <span class="nav_text"><?php echo lang('event_setup_customize_forms'); ?></span>
    </a>
  </li>    
  <li class="confirm_detail">
    <a href="<?php echo base_url('event/confirmdetails') ?>" class="page_leave <?php echo ($method=='confirmdetails')?"active":""; ?>">
      <span><img alt="launch icon" src="<?php echo IMAGE; ?>de_nav.png"></span>
      <span class="nav_text"><?php echo lang('event_setup_confirm_details'); ?></span>
    </a>
  </li> 
</ul>

<script type="text/javascript">
//page leave alert show
pageLeave();
</script>

