<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

//define current and next form div id
$FormDivId      = 'SeventhForm';
$nextFormDivId  = 'SeventhForm';

$refundfeedetails         = (!empty($refundfeedetails))?$refundfeedetails[0]:'';
$paymentRefundId          = (!empty($refundfeedetails->id))?$refundfeedetails->id:'0';
$whoWillPayFeeValue       = (isset($refundfeedetails->fee_to_be_paid_by))?$refundfeedetails->fee_to_be_paid_by:'';
$regisUnitPriceValue      = (isset($refundfeedetails->price_per_registrant))?$refundfeedetails->price_per_registrant:'';
$regisGSTIncludedValue    = (isset($refundfeedetails->gst))?$refundfeedetails->gst:'';
$regisTotalPriceValue     = (isset($refundfeedetails->total_price))?$refundfeedetails->total_price:'';
$refundFeeValue           = (!empty($refundfeedetails->refund_fee))?$refundfeedetails->refund_fee:'';

$formRefundFeesSetup = array(
    'name'   => 'formRefundFeesSetup',
    'id'   => 'formRefundFeesSetup',
    'method' => 'post',
    'class'  => 'form-horizontal',
    'data-parsley-validate' => '',
  );

$refundFeeYes = array(
    'name'  => 'refundFee',
    'value' => '1',
    'id'  => 'refundFeeYesRefund',
    'type'  => 'radio',
    'required'  => '',
    'class' => 'refundFee',
    'data-parsley-error-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
  );  

$refundFeeNo = array(
    'name'  => 'refundFee',
    'value' => '0',
    'id'  => 'refundFeeNoRefund',
    'type'  => 'radio',
    'required'  => '',         
    'class' => 'refundFee',
  );

//set value
if($refundFeeValue=='1'){
    $refundFeeYes['checked'] = true;
    $refundFeeClass = '';
}else{
    $refundFeeNo['checked'] = true;
    $refundFeeClass = 'dn';
}
  
$whoWillPayFeeRegis = array(
    'name'  => 'whoWillPayFee',
    'value' => '0',
    'id'  => 'payTheRegistrationFeeYesRefund',
    'type'  => 'radio',
    'required'  => '',
  );

$whoWillPayFeeOrgai = array(
    'name'  => 'whoWillPayFee',
    'value' => '1',
    'id'  => 'whoWillPayFeeOrgaiRefund',
    'type'  => 'radio',
    'required'  => '',
  );

//set value
if($whoWillPayFeeValue=='0'){
    $whoWillPayFeeRegis['checked'] = true;
}else{
    $whoWillPayFeeOrgai['checked'] = true;
}  

$regisTotalPrice = array(
    'name'  => 'regisTotalPrice',
    'value' =>  $regisTotalPriceValue,
    'id'  => 'regisRefundTotalPrice',
    //'required'  => '',
    'type' => 'text',
    'class' => 'small short_field regisRefundTotalPriceEnter numValue',
    'data-parsley-error-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
  );
  
$regisGSTIncluded = array(
    'name'  => 'regisGSTIncluded',
    'value' =>  $regisGSTIncludedValue,
    'id'    => 'regisRefundGSTIncluded',
    //'required'  => '',
    'type'    => 'text',
    'readonly'    => 'true',
    'class' => 'small short_field',
    'data-parsley-error-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
  );
?>



<div class="panel event_panel">
  <div class="panel-heading ">
      <h4 class="panel-title medium dt-large heading_btn ">
      <a class="" data-toggle="collapse" data-parent="#accordion" href="#collapseSix">
      <?php echo lang('event_payment_refund_fees'); ?>
     </a>
      </h4>
  </div>

  <div style="height: auto;" id="collapseSix" class="accordion_content collapse apply_content">
  <?php echo form_open($this->uri->uri_string(),$formRefundFeesSetup); ?>
    <div class="form-horizontal form_open_chk" id="showhideformdiv<?php echo $FormDivId; ?>">    
      <div class="panel-body ls_back dashboard_panel small">


        <div class="row-fluid-15">
          <label for="info_org"><?php echo lang('event_payment_pay_refund_fees'); ?><span class="astrik">*</span>
            <span class="info_btn">
                <span class="field_info xsmall"><?php echo lang('event_payment_pay_refund_fees'); ?>
                  </span>
              </span>
          </label>
           
          <div class="radio_wrapper ">
            <?php echo form_radio($refundFeeYes); ?> 
            <label for="refundFeeYesRefund"><?php echo lang('event_paymen_fees_yes'); ?> </label>
            <?php echo form_radio($refundFeeNo); ?>
            <label for="refundFeeNoRefund"><?php echo lang('event_paymen_fess_no'); ?> </label>
          </div>
        </div>

        <div class="<?php echo $refundFeeClass; ?>" id="refundFeeDiv">
            <div class="row-fluid-15">
              <label for="info_org"><?php echo lang('event_payment_pay_transaction_fees'); ?> <span class="astrik">*</span>
                <span class="info_btn">
                    <span class="field_info xsmall"><?php echo lang('event_payment_pay_transaction_fees'); ?>
                      </span>
                  </span>
              </label>
                <div class="radio_wrapper ">
                  <?php echo form_radio($whoWillPayFeeRegis); ?> 
                  <label for="payTheRegistrationFeeYesRefund"><?php echo lang('event_paymen_paid_transction_by_1'); ?> </label>
                  <?php echo form_radio($whoWillPayFeeOrgai); ?>
                  <label for="whoWillPayFeeOrgaiRefund"><?php echo lang('event_paymen_paid_transction_by_2'); ?> </label>
                </div>
                <!--end of session sub category-->          
            </div>
            
            <div class="row-fluid-15">
                <label for="reg_limit"><?php echo lang('event_payment_register_total_price'); ?> <span class="astrik">*</span>
                  <span class="info_btn"></span>
                </label>
                <?php echo form_input($regisTotalPrice); ?>
                <?php echo form_error('regisTotalPrice'); ?>
            </div>

            <div class="row-fluid-15">
            <label for="reg_limit"><?php echo lang('event_payment_register_gst_include'); ?></label>
            <?php echo form_input($regisGSTIncluded); ?>
            <?php echo form_error('regisGSTIncluded'); ?>
        </div>
        </div>

        <div class="btn_wrapper ">
          <a href="#collapseSix" class="pull-left scroll_top"><?php echo lang('comm_top'); ?></a>
          <?php 
            echo form_hidden('eventId', $eventId);
            echo form_hidden('formActionName', 'refundFees');
            echo form_hidden('paymentRefundId', $paymentRefundId);
            
            //button show of save and reset
            $formButton['showbutton']   = array('save','reset');
            $formButton['labletext']  = array('save'=>lang('comm_save_changes'),'reset'=>lang('comm_reset'));
            $formButton['buttonclass']  = array('save' => 'default_btn btn pull-right medium', 'reset' => 'default_btn btn pull-right medium reset_form');
            $this->load->view('common_save_reset_button',$formButton);
          ?>
        </div>
      </div>
    </div>
  <?php echo form_close(); ?>
  </div>
</div>
<!--end of panel-->
<script type="text/javascript">
$(document).ready(function() { 
    //payment refund fees save 
    ajaxdatasave('formRefundFeesSetup','<?php echo $this->uri->uri_string(); ?>',true,true,false,true,true,'#showhideformdiv<?php echo $FormDivId; ?>','#showhideformdiv<?php echo $nextFormDivId; ?>',false);
    
    showHideArea('click','.refundFee','#refundFeeDiv','regisRefundTotalPrice','regisRefundGSTIncluded');
});
</script>

