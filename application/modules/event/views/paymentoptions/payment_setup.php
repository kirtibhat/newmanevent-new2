<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$tempHidden = array(
    'type' => 'hidden',
    'name' => 'tempHidden',
    'id' => 'tempHidden',
    'value' => '',
);

echo form_input($tempHidden);

$this->load->view('event_menus');

$catFilterList = array(); 
if(!empty($registrantCategory)){
    foreach($registrantCategory as $value){
        $catFilterList[$value->category_id][$value->category][$value->registrant_id] = $value->registrant_name; 
    }//end loop
}
$sendData['catFilterList'] = $catFilterList;
//echo '<pre>';print_r($catFilterList);
?>

<div class="row-fluid-15">
    <div id="page_content" class="span9">
      <div id="accordion" class="panel-group">
        <?php $this->load->view('form_payment_payment_option', $sendData); ?>

        <?php $this->load->view('form_payment_gateway'); ?>

        <?php $this->load->view('form_payment_registration_fees'); ?>

        <?php $this->load->view('form_payment_bank_fees'); ?>

        <?php $this->load->view('form_payment_credit_card_fees'); ?>

        <?php $this->load->view('form_payment_cheque_fees.php'); ?>

        <?php $this->load->view('form_payment_refund_fees.php'); ?>
      </div>
      <!--end of accordion content-->

      <div class="page_btnwrapper">
        <?php 
          //next and previous button
          $buttonData['viewbutton'] = array('back','next','preview');
          $buttonData['linkurl']  = array('back'=>base_url('event/setupextras'),'next'=>base_url('event/customizeforms'),'preview'=>'');
          $this->load->view('common_back_next_buttons',$buttonData);
        ?>
      </div>
      <!--end of page btn wrapper-->
        
    </div>
    <!--end of page content-->
</div>
<!--end of row-->

<!--start custom tooltip-->
<div id="tooltip_wrapper"></div>
<!--end custom tooltip-->

<script>
$(document).ready(function() {
    var gstRate = parseInt('<?php echo (!empty($eventdetails->gst_rate))?$eventdetails->gst_rate:"0"; ?>');

    /*----This function is used to unit price enter and show payment option-----*/
    unitPriceManage('regisTotalPriceEnter','','regisGSTIncluded','regisTotalPrice',gstRate);

    /*----This function is used to unit price enter and show transaction fees option-----*/
    unitPriceManage('regisTranTotalPriceEnter',false,'regisTranGSTIncluded','regisTranTotalPrice',gstRate);

    /*----This function is used to unit price enter and show transaction fees option-----*/
    unitPriceManage('regisChequeTotalPriceEnter',false,'regisChequeGSTIncluded','regisChequeTotalPrice',gstRate);

    /*----This function is used to unit price enter and show transaction fees option-----*/
    unitPriceManage('regisRefundTotalPriceEnter',false,'regisRefundGSTIncluded','regisRefundTotalPrice',gstRate);

    /*---This function is used to delete custom field for payment option -----*/
    customconfirm('delete_payment_option','event/deletepaymentoption', '', '', '', true,true,false,'<?php echo lang('event_msg_payment_option_delete_successfully') ?>');

    /*-----Open add alternative payment option  popup-------*/
    $(document).on('click','.add_edit_payment_option',function(){  
      var formAction = $(this).attr('actionform');

      if($(this).attr('paymentModeId')!==undefined){
          var paymentModeId = parseInt($(this).attr('paymentModeId'));
          //set value in assing
          $("#tempHidden").val(paymentModeId);
      }else{
          //get value form temp hidden and set
          var paymentModeId = parseInt($("#tempHidden").val());
      }

      var eventId = '<?php echo $eventId ?>';
      var sendData = {"paymentModeId":paymentModeId, "eventId":eventId, "formAction":formAction};
      ajaxpopupopen('add_edit_payment_popup','event/addeditpaymentoptionview',sendData,'add_edit_payment_popup');
    });
});
</script>
