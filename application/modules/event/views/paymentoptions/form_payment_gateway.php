<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

$paymentgatewaydata     = (!empty($paymentgatewaydata))?$paymentgatewaydata[0]:'';
$getwayId           = (!empty($paymentgatewaydata->id))?$paymentgatewaydata->id:'0';
$merchantIdValue      = (!empty($paymentgatewaydata->merchant_id))?$paymentgatewaydata->merchant_id:'';
$paymentGatewayTypeValue  = (!empty($paymentgatewaydata->payment_gateway_type))?$paymentgatewaydata->payment_gateway_type:'';

$paymentTypeList = array('paypal'=>'Paypal');

//define current and next form div id
$FormDivId = 'SecondForm';
$nextFormDivId = 'ThirdForm';

$formPaymentGateway = array(
    'name'   => 'formPaymentGateway',
    'id'   => 'formPaymentGateway',
    'method' => 'post',
    'class'  => 'form-horizontal',
  );

$merchantIdField = array(
    'name'  => 'merchantIdField',
    'value' =>  $merchantIdValue,
    'id'  => 'merchantIdField',
    'required'  => '',
    'type' => 'text',
  );
  
  
?>


<div class="panel event_panel">
  <div class="panel-heading ">
      <h4 class="panel-title medium dt-large heading_btn ">
      <a class="" data-toggle="collapse" data-parent="#accordion" href="#collapsesev">
      <?php echo lang('event_payment_payment_gateway'); ?>
     </a>
      </h4>
  </div>



  <div style="height: auto;" id="collapsesev" class="accordion_content collapse apply_content">
    <?php echo form_open($this->uri->uri_string(),$formPaymentGateway); ?>
      <div class="form-horizontal form_open_chk" id="showhideformdiv<?php echo $FormDivId; ?>">    
        <div class="panel-body ls_back dashboard_panel small">
          <div class="row-fluid-15">
            <label for="info_org"><?php echo lang('event_payment_payment_gateway'); ?><span class="astrik">*</span>
              <span class="info_btn">
                  <span class="field_info xsmall"><?php echo lang('event_payment_payment_gateway'); ?>
                    </span>
                </span>
            </label>
             
            <div class="radio_wrapper ">
              <?php 
                $other = ' id="paymentGetwayType" required =""';
                echo form_dropdown('paymentGetwayType',$paymentTypeList,$paymentGatewayTypeValue,$other);
              ?>
            </div>
            
            <div class="row-fluid-15">
              <label for="reg_limit"><?php echo lang('event_payment_marchant_id'); ?> <span class="astrik">*</span>
                <span class="info_btn"></span>
              </label>
              <?php echo form_input($merchantIdField); ?>
              <?php echo form_error('merchantIdField'); ?>
            </div>


      
            <div class="btn_wrapper ">
              <a href="#collapsesev" class="pull-left scroll_top"><?php echo lang('comm_top'); ?></a>
              <?php 
                echo form_hidden('eventId', $eventId);
                echo form_hidden('getwayId', $getwayId);
                echo form_hidden('formActionName', 'paymentGateway');
                
                //button show of save and reset
                $formButton['showbutton']   = array('save','reset');
                $formButton['labletext']  = array('save'=>lang('comm_save_changes'),'reset'=>lang('comm_reset'));
                $formButton['buttonclass']  = array('save' => 'default_btn btn pull-right medium', 'reset' => 'default_btn btn pull-right medium reset_form');
                $this->load->view('common_save_reset_button',$formButton);
              ?>
            </div>
      </div>
    </div>
    </div>
  <?php echo form_close(); ?>
  </div>
</div>
<!--end of panel-->

<script type="text/javascript">
  //payment credit card fees save 
  ajaxdatasave('formPaymentGateway','<?php echo $this->uri->uri_string(); ?>',true,true,false,false,false,'#showhideformdiv<?php echo $FormDivId; ?>','#showhideformdiv<?php echo $nextFormDivId; ?>');
</script> 
