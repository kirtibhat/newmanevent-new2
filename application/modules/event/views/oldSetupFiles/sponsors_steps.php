<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 |--------------------------------------------------------------------------|
 | This Field define for temparay open popup by add section id 				|
 | and edit record id														|	
 |--------------------------------------------------------------------------|
*/
 
$tempHidden = array(
	'type' => 'hidden',
	'name' => 'tempHidden',
	'id' => 'tempHidden',
	'value' => '',
);

echo form_input($tempHidden);

?>

<?php $this->load->view('event_menus');  ?>

<div class="row-fluid mt20">
	<div class="span7">
		<div class="formparag pl0 pr0">
			 <?php echo lang('event_sponsors_msg'); ?>
		</div>
		<div class="spacer15">
		</div>
		
		<!----load sponsor exhibitor plan view start---->
			<?php $this->load->view('sponsors_exhibitor_floor_plan_view'); ?>	
		<!----load sponsor exhibitor plan view end---->
		
		<!-- /commonform_bg -->
		<div class="regidetrailHeading">
			<a href="javascript:void(0)"  class="add_edit_sponsors" sponsorsid="0">
			<div class="addbtn_form bg_731472">
			</div>
			<div class="addtype clr_A54399">
				<?php echo lang('event_sponsors_add_sponsors'); ?>
			</div>
			</a>
			<div class="clearfix">
			</div>
		</div>
		<div class="spacer15"></div>
	
		<?php 
			if(!empty($eventsponsor)){
				
				//prepare array data for current and next form hide/show  
				foreach($eventsponsor as $sponsor){
					$recordId[] =  $sponsor->id;
				}
				
				//show sponsor forms	
				$rowCount = 0;	
				foreach($eventsponsor as $sponsor){
					$nextId = $rowCount+1;
					$sendData['formdata'] = $sponsor; 
					$sendData['nextRecordId']	= (isset($recordId[$nextId]))?$recordId[$nextId]:'0';
					// daynamic load all send data forms
					$this->load->view('form_sponsors_steps',$sendData);
					$rowCount++;
				}
			}
		?>
	
		<?php 
			//next and previous button
			$buttonData['viewbutton'] = array('back','next','preview');
			$buttonData['linkurl'] 	= array('back'=>base_url('event/setupbreakouts'),'next'=>base_url('event/setupexhibitors'),'preview'=>'');
			$this->load->view('common_back_next_buttons',$buttonData);
		?>
	
	</div>
	
	<!----Right side advertisement view load start------>
		<?php $this->load->view('right_side_advert'); ?>
	<!----Right side advertisement view load end------>
	
</div>



<script type="text/javascript" language="javascript">
	
	/*--------registrant selection Limit---------*/
	$(".registrantSelectionLimit").click(function(){
		var getid=$(this).attr('registrantselectionlimitid');
		var getchecked=$(this).attr('checked');
		if(getchecked=="checked" && getchecked!=undefined ){
			showarea("#registrant_selection_limit_div"+getid);
		}else{
			hidearea("#registrant_selection_limit_div"+getid);
		}	
	});
	
	/*--------This section is used to sponsor complimentary section show and hide---------*/
	$(".complimentaryRegistration").click(function(){
		var getId=$(this).attr('sponsorid');
		var getStatus=$(this).is(':checked');
		if(getStatus){
			showarea("#is_complimentary_registrations"+getId);
		}else{
			hidearea("#is_complimentary_registrations"+getId);
		}
	});
	
	/*--------This section is used to sponsor type attend selection---------*/
	$(".additionalGuests").click(function(){
		var getId=$(this).attr('additionalguestsid');
		var getValue=parseInt($(this).val());
		if(getValue=="1"){
			showarea("#additional_guests_div"+getId);
			$(".additionalGuestFieldDis"+getId).attr('required',''); 
		}else{
			hidearea("#additional_guests_div"+getId);
			$(".additionalGuestFieldDis"+getId).removeAttr('required');
		}
	});
	
	
	//*--------This section is used to add  & upload floor plane------*//
	$( "#formExhibitorFloorPlan" ).submit(function( event ) {
		var fromData=$("#formExhibitorFloorPlan").serialize();
		var url = baseUrl+'event/addexhibitorfloorplan';
		$.post(url,fromData, function(data) {
			  if(data){
				// console.log(typeof(data.is_success));
					if(data.is_success=='true'){
						$('#div_ar2RA_a').trigger('click');
						//$("#exhibitorFloorPlanIdField").val(data.exhibitorfloorplanid);
						//custom_popup(data.msg,true);	
						setTimeout(refreshPge,2000);
					}
				}
			},"json");
		return false;	 
	});
		
	// Custom example logic for file upload
	var postData = {event_id : '<?php echo $eventId; ?>'}
	var uploadUrl = baseUrl+'event/updateexhibitorfloorplan';

	var otherObj = {
			pickfiles:"attch_pickfiles", 
			dropArea:"attch_dropArea", 
			FileContainer:"attch_FileContainer", 
			fileTypes:"pdf", 
			deleteUrl:baseUrl+'event/deleteexhibitorfloorplan', 
			maxfilesize:"10mb", 
			multiselection:false, 
			isFileList:true, 
			uniqueNames:true, 
			autoStart:false, 
			isDelete:true, 
			maxfileupload:1, 
			mainDivFileList:"attach_file_list_main",
			listAddFile:"attch_file_list",
			adddedfile:"attchadddedfile",
		}

	upload_file(uploadUrl,postData,otherObj);
	
	//call function for delete file
	deleteUploadFiles();
	
	/*-----Open add and edit sponsor popup-------*/
	$(document).on('click','.add_edit_sponsors',function(){
		
		if($(this).attr('sponsorsid')!==undefined){
			var sponsorsId = parseInt($(this).attr('sponsorsid'));
			//set value in assing
			$("#tempHidden").val(sponsorsId);
		}else{
			//get value form temp hidden and set
			var sponsorsId = parseInt($("#tempHidden").val());
		}
		
		var eventId = '<?php echo $eventId ?>';
		var sendData = {"sponsorsId":sponsorsId, "eventId":eventId};
		var numberOfBooths = parseInt($("#numberOfBooths").val());
		//var floorPlanName = $("#exhibitorFloorPlanNameField").val();
		
		if(numberOfBooths==0){
			custom_popup('Please select booth number.',false);
			return false;
		}
		
		//call ajax popup sponsor add and edit 
		ajaxpopupopen('add_edit_sponsors_popup_div','event/addeditsponsorsview',sendData,'add_edit_sponsors');
		
	});
	
	//save sponsor add and edit data
	ajaxdatasave('formAddSponsors','event/addeditsponsorssave',false,true,true);
	
	/*---This function is used to delete delete sponsors -----*/
	customconfirm('delete_sponsor','event/deletesponsors');
	
	
	/*-----Open add and edit sponsor additional purchase items popup-------*/
	$(document).on('click','.add_edit_purchase_item',function(){
	
		
		
		//set value in temparray hidden field for if again open
		if($(this).attr('itemid') !== undefined && $(this).attr('sponsorsid') !== undefined ){
		
			var itemId = parseInt($(this).attr('itemid'));
			var sponsorsId = parseInt($(this).attr('sponsorsid'));
			
			//concanate two value in assing
			var tempObj = itemId+','+sponsorsId;
			$("#tempHidden").val(tempObj);
		}else{
			var getTempData = $("#tempHidden").val();
			var getData = getTempData.split(',');
			
			//get value from hidden field and set value
			itemId = getData[0];
			sponsorsId = getData[1];
		}
		
		var sendData = {"sponsorsId":sponsorsId, "itemId":itemId};
		
		//call ajax popup sponsor additional purchase items add and edit 
		ajaxpopupopen('sponsors_purchase_item_popup','event/addeditpurchaseitemview',sendData,'add_edit_purchase_item');
	});
	
	//save sponsor additional purchase items add and edit data
	ajaxdatasave('formAddAdditionalItemPurchase','event/addeditpurchaseitemsave',false,true,true);	
	
	
	/*---This function is used to delete delete purchase item -----*/
	customconfirm('delete_purchase_item','event/deletepurchaseitem');
	
	
	/*-----This funtion is used to disbaled checkbox------------*/
	
	$(".setupspons_dis").click(function(e) {
			e.preventDefault();
	});
</script>	
