<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 


//set field value in variable
$baccountname = (!empty($eventdetails->account_name))?$eventdetails->account_name:'';
$bbank = (!empty($eventdetails->bank))?$eventdetails->bank:'';
$bbankloaction = (!empty($eventdetails->bank_location))?$eventdetails->bank_location:'';
$bbsb = (!empty($eventdetails->bsb))?$eventdetails->bsb:'';
$bacountnumber = (!empty($eventdetails->account_number))?$eventdetails->account_number:'';

$formBankDetails = array(
		'name'	 => 'formBankDetails',
		'id'	 => 'formBankDetails',
		'method' => 'post',
		'class'  => 'form-horizontal',
);

$bankAccountName = array(
		'name'	=> 'bankAccountName',
		'value'	=> $baccountname,
		'id'	=> 'bankAccountName',
		'type'	=> 'text',
		'required'	=> '',
		'class'=>'alphaNumValue',
	);	
$eventBank = array(
		'name'	=> 'eventBank',
		'value'	=> $bbank,
		'id'	=> 'eventBank',
		'type'	=> 'text',
		'required'	=> '',
		'class'=>'alphaNumValue',
	);
$bankLocation = array(
		'name'	=> 'bankLocation',
		'value'	=> $bbankloaction,
		'id'	=> 'bankLocation',
		'type'	=> 'text',
		'required'	=> '',
	);	

$eventBSB = array(
		'name'	=> 'eventBSB',
		'value'	=> $bbsb,
		'id'	=> 'eventBSB',
		'type'	=> 'text',
		'required'	=> '',
		'class'=>'alphaNumValue',
	);

$eventAccountNumber = array(
		'name'	=> 'eventAccountNumber',
		'value'	=> $bacountnumber,
		'id'	=> 'eventAccountNumber',
		'type'	=> 'text',
		'required'	=> '',
		'class' =>'numValue', 
	);	
	
?>	

<div class="commonform_bg pb0 ">
	<div class="headingbg bg_0073ae ptr ml_0 mb0">
		<div class="typeofregistV pull-left  showhideaction ptr " id="bankSection">
			<?php echo lang('event_bank_details'); ?>
		</div>
		<div class="clearfix"></div>
	</div>
	<?php echo form_open($this->uri->uri_string(),$formBankDetails); ?>
	
	
	<div class="form-horizontal dn pt20 pb36 bg_E0E8E9 borderR36 form_open_chk" id="showhideformdivbankSection">
			 <?php 	//echo lang('event_bank_details_msg'); ?>
	    <!--	<div class="formparag mb20 pr" >
			
			<div class="infoiconbg commontooltio eventdetai_tooltip  help_icon_para bg_F89728">
				<span class="show_tool_tip" title="" data-original-title="A 6 digit number provided by your bank for online payments. Please ensure you include BSB details otherwise bank deposits will not be accepted"> </span>
			</div>
			
		</div> -->
	
		<div class="control-group mb10">
			<label for="inputEmail" class="control-label"><?php echo lang('event_bank_account_name'); ?>
				<font class="mandatory_cls">*</font>
				 <div class="infoiconbg commontooltio eventdetai_tooltip bg_F89728">
				    <span class="show_tool_tip" title='<?php echo lang('tooltip_bank_details_msg'); ?>'> </span> 
		     	</div>
			</label>
			<div class="controls">
				<?php echo form_input($bankAccountName); ?>
				<?php echo form_error('bankAccountName'); ?>
			</div>
		</div>
		<div class="control-group mb10">
			<label for="inputEmail" class="control-label"><?php echo lang('event_bank_bank'); ?>
				<font class="mandatory_cls">*</font>
			</label>
			<div class="controls">
				<?php echo form_input($eventBank); ?>
				<?php echo form_error('eventBank'); ?>
			</div>
		</div>
		<div class="control-group mb10">
			<label for="inputEmail" class="control-label"><?php echo lang('event_bank_bank_loaction'); ?>
				<font class="mandatory_cls">*</font>
			</label>
			<div class="controls">
				<?php echo form_input($bankLocation); ?>
				<?php echo form_error('bankLocation'); ?>
			</div>
		</div>
		
		<div class="control-group mb10">
			<label for="inputEmail" class="control-label"><?php echo lang('event_bank_bsb'); ?>
				<font class="mandatory_cls">*</font>
			<div class="infoiconbg commontooltio eventdetai_tooltip">
				<span class="show_tool_tip" title='<?php echo lang('tooltip_bank_bsb'); ?>'> </span>
			</div>
			</label>
			<div class="controls">
				<?php echo form_input($eventBSB); ?>
				<?php echo form_error('eventBSB'); ?>
			</div>
		</div> 
		<div class="control-group mb10">
			<label for="inputEmail" class="control-label"><?php echo lang('event_bank_account_number'); ?>
			<font class="mandatory_cls">*</font>
			<div class="infoiconbg commontooltio eventdetai_tooltip bg_F89728">
				 <span class="show_tool_tip" title='<?php echo lang('tooltip_bank_account_number'); ?>'> </span> 
			</div>  
			</label> 
			<div class="controls">
				<?php echo form_input($eventAccountNumber); ?>
				<?php echo form_error('eventAccountNumber'); ?>
			</div>
		</div>
		<div class="row-fluid">
			<?php 
				echo form_hidden('eventId', $eventId);
				echo form_hidden('formActionName', 'eventBankDetails');
				
				//button show of save and reset
				$formButton['showbutton']   = array('save','reset');
				$formButton['labletext'] 	= array('save'=>lang('comm_save_changes'),'reset'=>lang('comm_reset'));
				$formButton['buttonclass']  = array('save'=>'submitbtn_cus mr_16','reset'=>'submitbtn_cus reset_form');
				$this->load->view('common_save_reset_button',$formButton);
			?>
		</div>
	</div>
	
	
	<?php echo form_close();?>
</div>
<!-- /commonform_bg -->

<script>
	//save event bank details
	ajaxdatasave('formBankDetails','<?php echo $this->uri->uri_string(); ?>',true,true,false,false,false,'#showhideformdivbankSection','#showhideformdivtermSection');
</script>	
