<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
  
  
	//define variable for value
	$registrationLimitId = "0";
	$regisSelectionCheckBoxDisable  = 	false;
	$registrantSelectionLimitVal	= 	false;
	$registrantLimitVal		 		= 	'';
	$registrantSelectionLimitDiv    = 'dn';
	$getSingleDayRegistId = ($singleDayRegistId==0)?NULL:$singleDayRegistId;
	
	//get value by side event id and registraint id 
	$where = array('breakout_id'=>$breakoutId,'registrant_id'=>$registrantId,'registrant_daywise_id'=>$getSingleDayRegistId);
	$breakoutRegist = getDataFromTabel('breakouts_registrant','*',$where);
	
	
	if(!empty($breakoutRegist)){
		$breakoutRegist = $breakoutRegist[0];
		//define variable for value
		$registrantLimitVal	 	 	 		= 	$breakoutRegist->registrant_limit;
		$registrationLimitId 				=	$breakoutRegist->id;
		$registrantSelectionLimitVal		= 	true;
		$registrantSelectionLimitDiv  	    =   '';
		$regisSelectionCheckBoxDisable 	    = 	false;
	}
	
	
	$registrantSelectionLimit = array(
					'name'	=> 'registrantSelectionLimit'.$regisBreakoutId,
					'value'	=> '1',
					'id'	=> 'registrantSelectionLimit'.$regisBreakoutId,
					'type'	=> 'checkbox',
					'class'	=> 'registrantSelectionLimit',
					'registrantselectionlimitid'	=> $regisBreakoutId,
					'checked'=> $registrantSelectionLimitVal,
				);	
	if($regisSelectionCheckBoxDisable){
		$registrantSelectionLimit['disabled'] = 'disabled';
	}			
	
	$registrantLimit = array(
					'name'	=> 'registrantLimit'.$regisBreakoutId,
					'value'	=> $registrantLimitVal,
					'id'	=> 'registrantLimit'.$regisBreakoutId,
					'type'	=> 'text',
				//	'required'	=> '',
					'class'	=> 'field_disable limit_input registrantselection'.$regisBreakoutId,
				);	
				
	
	// if registraint selection is checked	
	if($registrantSelectionLimitVal){
		$registrantLimit['required']	 	  =	'';
	}	
	
	$breakoutsRegistrantId = array(
				'name'	=> 'breakoutsRegistrantId'.$breakoutId.'['.$registrantId.']['.$singleDayRegistId.']',
				'value'	=> $registrationLimitId,
				'id'	=> 'breakoutsRegistrantId'.$regisBreakoutId,
				'type'	=> 'hidden',
			);					
							
	echo form_input($breakoutsRegistrantId);	
	
?>

	<!------This div section show registrant and day wise registrant list start------->
	<div class="tickbox_cont <?php echo ($countRowRegis==1)?'height_35':''; ?>">
		<div class="standerd_container">
			<div class="controls checkradiobg setupcheck ml0 pl0 pr0">
				<div class="checkbox pl0 position_R breakputcheck">
					<?php echo form_checkbox($registrantSelectionLimit); ?>
					<label for="registrantSelectionLimit<?php echo $regisBreakoutId; ?>" class="width_auto ml0 mt10 eventDcheck">&nbsp; <?php echo $registrantName; ?></label>
				</div>
			</div>
		</div>
		<div class="setlimit <?php echo $registrantSelectionLimitDiv; ?>" id="registrant_selection_limit_div<?php echo $regisBreakoutId; ?>">
			<div class="control-group">
				<label class="control-label width_auto" for="inputEmail"><?php echo lang('event_breakkout_regis_limit'); ?>
				<div class="infoiconbg bg_46166b breakcount_tooltip">
					<span class="show_tool_tip" title='<?php echo lang('tooltip_breakkout_regis_limit'); ?>'> </span>
				</div>
				</label>
				<div class="controls ml0 pr0 pl0">
					<?php echo form_input($registrantLimit); ?>
					<?php echo form_error('registrantLimit'.$regisBreakoutId); ?>
				</div>
			</div>
		</div>
		<div class="clearfix"> </div>
	</div>
	<!------This div section show registrant and day wise registrant list end------->
