<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<div class="model_customise modal fade alertmodelbg dn" id="customize_form_help_popup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg_807f83">
				<h4 class="modal-title"><?php echo lang('tooltip_help_suggested_colors'); ?></h4>
			</div>
			<div class="modal-body plpr20">
				<table class="cstmform_pup">
				<tr>
					<th>
						&nbsp;
					</th>
					<th>
						<?php echo lang('tooltip_help_color_1'); ?>
					</th>
					<th>
						<?php echo lang('tooltip_help_color_2'); ?>
					</th>
					<th>
						<?php echo lang('tooltip_help_color_3'); ?>
					</th>
					<th>
						<?php echo lang('tooltip_help_color_4'); ?>
					</th>
				</tr>
				<tr>
					<td rowspan="2" class="custom_tdsize">
						<?php echo lang('tooltip_help_text'); ?>
					</td>
					<td>
						<?php echo lang('tooltip_help_header'); ?>
					</td>
					<td>
						<?php echo lang('tooltip_help_header_1'); ?>
					</td>
					<td>
						<?php echo lang('tooltip_help_link'); ?>
					</td>
					<td>
						&nbsp;
					</td>
				</tr>
				<tr>
					<td>
						<?php echo lang('tooltip_help_table_text_1'); ?>
					</td>
					<td>
						<?php echo lang('tooltip_help_table_text_2'); ?>
					</td>
					<td>
						&nbsp;
					</td>
					<td>
						&nbsp;
					</td>
				</tr>
				<tr>
					<td rowspan="3" class="fs16">
						<?php echo lang('tooltip_help_image'); ?>
					</td>
					<td>
						<?php echo lang('tooltip_help_table_bg_1'); ?>
					</td>
					<td>
						<?php echo lang('tooltip_help_header__bar_bg'); ?>
					</td>
					<td>
						<?php echo lang('tooltip_help_button_icon_1'); ?>
					</td>
					<td>
						<?php echo lang('tooltip_help_bg_1'); ?>
					</td>
				</tr>
				<tr>
					<td>
						&nbsp;
					</td>
					<td>
						<?php echo lang('tooltip_help_navigation_bar_bg'); ?>
					</td>
					<td>
						<?php echo lang('tooltip_help_button_icon_2'); ?>
					</td>
					<td>
						<?php echo lang('tooltip_help_bg_2'); ?>
					</td>
				</tr>
				<tr>
					<td>
						&nbsp;
					</td>
					<td>
						<?php echo lang('tooltip_help_table_bg_2'); ?>
					</td>
					<td>
						&nbsp;
					</td>
					<td>
						&nbsp;
					</td>
				</tr>
				</table>
			</div>
			
			<div class="modal-footer">
				<?php 
					$extraCancel 	= 'class="btn btn-default bg_5f6062 custombtn" data-dismiss="modal" ';
					echo form_button('cancel',lang('comm_close'),$extraCancel);
				?>
			</div>
		</div>
	</div>
</div>
