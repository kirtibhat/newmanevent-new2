<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	//define variable for value
	$registrantSelectionDiv			 	 	 = 	'dn';
	$registrantSelectionIsChcked		 	 = 	false;
	
	//set value in textbox
	$SideEventRegistaintIdVal		 	 	 = 	'0';
	$registrantLimitVal		 				 = 	'';
	$SideEventRegisAddiDetailsVal	 	 	 = 	'';
	$SideEventRegisUnitPriceVal				 = 	'';
	$SideEventRegisTotalPriceVal			 = 	'';
	$SideEventRegisGSTIncludeVal			 = 	'';
	$SideEventRegisEarlyUnitPriceVal		 = 	'';
	$SideEventRegisEarlyTotalPriceVal		 = 	'';
	$SideEventRegisEarlyGSTIncludeVal		 = 	'';
	$SideEventRegisComaplimentaryDiv		 = 	'dn';
	$SideEventRegisComaplimentaryVal		 = 	true;
	$SideRegisAddiGuestsVal			 		 = 	false;
	$SideRegisAddiGuestsIsShow		 		 = 	'dn';
	$SideRegisAddiGuestsMaxNoVal			 = 	'0';
	$SideRegisAddiGuestsUnitPriceVal	 	 = 	'';
	$SideRegisAddiGuestsPriceVal		 	 = 	'';
	$SideRegisAddiGuestsGSTVal		 	 	 = 	'';
	$SideRegisCompliGuestsVal		 		 = 	false;
	$SideRegisCompliGuestsIsShow	 		 = 	'dn';
	$SideRegisCompliGuestsMaxNoVal			 = 	'0';
	$SideRegisCompliGuestsUnitPriceVal 		 = 	'';
	$SideRegisCompliGuestsPriceVal	 		 = 	'';
	$SideRegisCompliGuestsGSTVal	 	 	 = 	'';
	$getSingleDayRegistId = ($singleDayRegistId==0)?NULL:$singleDayRegistId;
	
	//get value by side event id and registraint id 
	$where = array('side_event_id'=>$sideEventId,'registrant_id'=>$registrantId,'registrant_daywise_id'=>$getSingleDayRegistId);
	$sideEventRegist = getDataFromTabel('side_event_registrant','*',$where);
	
	if(!empty($sideEventRegist)){
		
		$sideEventRegist = $sideEventRegist[0];
		//define variable for value
		$registrantSelectionDiv			 	 	 = 	'';
		$registrantSelectionIsChcked		 	 = 	true;
		$SideEventRegistaintIdVal		 	 	 = 	$sideEventRegist->id;
		$registrantLimitVal	 	 	 			 = 	$sideEventRegist->registrant_limit;
		$SideEventRegisAddiDetailsVal	 	 	 = 	$sideEventRegist->additional_details;
		$SideEventRegisUnitPriceVal				 = 	$sideEventRegist->unit_price;
		$SideEventRegisTotalPriceVal			 = 	$sideEventRegist->total_price;
		$SideEventRegisGSTIncludeVal			 = 	$sideEventRegist->gst;
		$SideEventRegisEarlyUnitPriceVal		 = 	$sideEventRegist->unit_price_early_bird;
		$SideEventRegisEarlyTotalPriceVal		 = 	$sideEventRegist->early_bird_price;
		$SideEventRegisEarlyGSTIncludeVal		 = 	$sideEventRegist->early_bird_gst;
		$SideEventRegisComaplimentaryVal		 = 	(!empty($sideEventRegist->complementry) && $sideEventRegist->complementry == '1' )?true:false;
		$SideEventRegisComaplimentaryDiv         =  ($SideEventRegisComaplimentaryVal==true)?'dn':'';
		$SideRegisAddiGuestsVal			 		 =  (!empty($sideEventRegist->additional_guest) && $sideEventRegist->additional_guest == '1' )?true:false;
		$SideRegisAddiGuestsIsShow		 		 = 	($SideRegisAddiGuestsVal)?'':'dn';
		$SideRegisAddiGuestsMaxNoVal	 		 = 	(!empty($sideEventRegist->maximum_number_of_additional_guest))?$sideEventRegist->maximum_number_of_additional_guest:'0';
		$SideRegisAddiGuestsUnitPriceVal		 = 	$sideEventRegist->unit_price_for_additional_guest;
		$SideRegisAddiGuestsPriceVal		 	 = 	$sideEventRegist->total_price_for_additional_guest;
		$SideRegisAddiGuestsGSTVal		 	 	 = 	$sideEventRegist->gst_for_additional_guest;
		$SideRegisCompliGuestsVal			 	 = 	(!empty($sideEventRegist->complementory_guest) && $sideEventRegist->complementory_guest == '1' )?true:false;
		$SideRegisCompliGuestsIsShow	 		 = 	($SideRegisCompliGuestsVal)?'':'dn';;
		$SideRegisCompliGuestsMaxNoVal	 		 = 	(!empty($sideEventRegist->maximum_number_of_complementory_guest))?$sideEventRegist->maximum_number_of_complementory_guest:'0';
		$SideRegisCompliGuestsUnitPriceVal		 = 	$sideEventRegist->unit_price_for_complementory_guest;
		$SideRegisCompliGuestsPriceVal		 	 = 	$sideEventRegist->total_price_for_complementory_guest;
		$SideRegisCompliGuestsGSTVal		 	 = 	$sideEventRegist->gst_for_complementory_guest;
	}
	
	$registrantSelection = array(
					'name'	=> 'registrantSelection'.$regisSideEventId,
					'value'	=> '1',
					'id'	=> 'registrantSelection'.$regisSideEventId,
					'type'	=> 'checkbox',
					'class'	=> 'registrantSelection pull-left ml10 mr15 checkbox',
					//'disabled'	=> 'disabled',
					'registrantselectionid'	=> $regisSideEventId,
					'checked'=> $registrantSelectionIsChcked,
				);	
	
	if($registrantSelectionIsChcked){
		unset($registrantSelection['disabled']);
	}			
	
	
	$registrantLimit = array(
		'name'	=> 'registrantLimit'.$regisSideEventId,
		'value'	=> $registrantLimitVal,
		'id'	=> 'registrantLimit'.$regisSideEventId,
		'type'	=> 'text',
	//	'required'	=> '',
		'class'	=> 'width50per registrantselection'.$regisSideEventId,
	);	
							
	
	$SideEventRegisAddiDetails = array(
					'name'	=> 'SideEventRegisAddiDetails'.$regisSideEventId,
					'value'	=> $SideEventRegisAddiDetailsVal,
					'id'	=> 'SideEventRegisAddiDetails'.$regisSideEventId,
					'type'	=> 'textarea',
					'rows'	=> '3',
					'class' => 'bdr_n registrantselection'.$regisSideEventId,	
				);	
	
	$SideEventRegisUnitPrice = array(
				'name'	=> 'SideEventRegisUnitPrice'.$regisSideEventId,
				'value'	=> $SideEventRegisUnitPriceVal,
				'id'	=> 'SideEventRegisUnitPrice'.$regisSideEventId,
				'type'	=> 'text',
				'regisSideEventId'	=> $regisSideEventId,
				'class' => 'width50per unitPriceEnter registrantcomplimentary'.$regisSideEventId,	
			);	
	
	$SideEventRegisTotalPrice = array(
					'name'	=> 'SideEventRegisTotalPrice'.$regisSideEventId,
					'value'	=> $SideEventRegisTotalPriceVal,
					'id'	=> 'SideEventRegisTotalPrice'.$regisSideEventId,
					'type'	=> 'text',
					'class' => 'width50per registrantcomplimentary'.$regisSideEventId,	
				);		
				
	$SideEventRegisGSTInclude = array(
					'name'	=> 'SideEventRegisGSTInclude'.$regisSideEventId,
					'value'	=> $SideEventRegisGSTIncludeVal,
					'id'	=> 'SideEventRegisGSTInclude'.$regisSideEventId,
					'type'	=> 'text',
					'class' => 'width50per registrantcomplimentary'.$regisSideEventId,	
				);	
	
	$SideEventRegisEarlyIsAllowed = array(
					'name'	=> 'SideEventRegisEarlyIsAllowed'.$regisSideEventId,
					'value'	=> '1',
					'id'	=> 'SideEventRegisEarlyIsAllowed'.$regisSideEventId,
					'type'	=> 'hidden',
					'class'	=> 'width50per',
				);	
	
	$SideEventRegisEarlyUnitPrice = array(
					'name'	=> 'SideEventRegisEarlyUnitPrice'.$regisSideEventId,
					'value'	=> $SideEventRegisEarlyUnitPriceVal,
					'id'	=> 'SideEventRegisEarlyUnitPrice'.$regisSideEventId,
					'type'	=> 'text',
			//		'required'	=> '',
					'earlyRegisSideEventId'	=> $regisSideEventId,
					'class' => 'width50per earlyUnitPriceEnter registrantcomplimentary'.$regisSideEventId,	
				);		
							
						
				
	$SideEventRegisEarlyTotalPrice = array(
					'name'	=> 'SideEventRegisEarlyTotalPrice'.$regisSideEventId,
					'value'	=> $SideEventRegisEarlyTotalPriceVal,
					'id'	=> 'SideEventRegisEarlyTotalPrice'.$regisSideEventId,
					'type'	=> 'text',
			//		'required'	=> '',
					'class' => 'width50per registrantcomplimentary'.$regisSideEventId,	
				);		
				
	$SideEventRegisEarlyGSTInclude = array(
					'name'	=> 'SideEventRegisEarlyGSTInclude'.$regisSideEventId,
					'value'	=> $SideEventRegisEarlyGSTIncludeVal,
					'id'	=> 'SideEventRegisEarlyGSTInclude'.$regisSideEventId,
					'type'	=> 'text',
			//		'required'	=> '',
					'class' => 'width50per registrantcomplimentary'.$regisSideEventId,	
				);				
	
	$SideEventRegisComaplimentary = array(
					'name'	=> 'SideEventRegisComaplimentary'.$regisSideEventId,
					'value'	=> '1',
					'id'	=> 'SideEventRegisComaplimentary'.$regisSideEventId,
					'type'	=> 'checkbox',
					'class'	=> 'pull-left ml10 mr15 checkbox sideEventregiscomaplimentary',
					'sideEventregiscomaplimentaryid'	=> $regisSideEventId,
					'checked'=> $SideEventRegisComaplimentaryVal,
				);	
				
				
	$SideRegisAddiGuests = array(
					'name'	=> 'SideRegisAddiGuests'.$regisSideEventId,
					'value'	=> '1',
					'id'	=> 'SideRegisAddiGuests'.$regisSideEventId,
					'type'	=> 'checkbox',
					'class'	=> 'SideRegisAddiGuests pull-left ml10 mr15 checkbox',
					'sideregisaddiguestsid'	=> $regisSideEventId,
					'checked'=> $SideRegisAddiGuestsVal,
				);																
					
				
	$SideRegisAddiGuestsUnitPrice = array(
					'name'	=> 'SideRegisAddiGuestsUnitPrice'.$regisSideEventId,
					'value'	=> $SideRegisAddiGuestsUnitPriceVal,
					'id'	=> 'SideRegisAddiGuestsUnitPrice'.$regisSideEventId,
					'type'	=> 'text',
					'addRegisSideEventId'	=> $regisSideEventId,
					'class'	=> 'width50per addUnitPriceEnter  registrantselectionaddi'.$regisSideEventId,
				);					
				
	$SideRegisAddiGuestsGST = array(
					'name'	=> 'SideRegisAddiGuestsGST'.$regisSideEventId,
					'value'	=> $SideRegisAddiGuestsGSTVal,
					'id'	=> 'SideRegisAddiGuestsGST'.$regisSideEventId,
					'type'	=> 'text',
					'class'	=> 'width50per registrantselectionaddi'.$regisSideEventId,
				);	
				
	$SideRegisAddiGuestsPrice = array(
					'name'	=> 'SideRegisAddiGuestsPrice'.$regisSideEventId,
					'value'	=> $SideRegisAddiGuestsPriceVal,
					'id'	=> 'SideRegisAddiGuestsPrice'.$regisSideEventId,
					'type'	=> 'text',
					'class'	=> 'width50per registrantselectionaddi'.$regisSideEventId,
				);				
	
	$SideRegisCompliGuests = array(
					'name'	=> 'SideRegisCompliGuests'.$regisSideEventId,
					'value'	=> '1',
					'id'	=> 'SideRegisCompliGuests'.$regisSideEventId,
					'type'	=> 'checkbox',
					'class'	=> 'SideRegisCompliGuests pull-left ml10 mr15 checkbox',
					'sideregiscompliguestsid'	=> $regisSideEventId,
					'checked'=> $SideRegisCompliGuestsVal,
				);																
					
					
	$SideRegisCompliGuestsUnitPrice = array(
					'name'	=> 'SideRegisCompliGuestsUnitPrice'.$regisSideEventId,
					'value'	=> $SideRegisCompliGuestsUnitPriceVal,
					'id'	=> 'SideRegisCompliGuestsUnitPrice'.$regisSideEventId,
					'type'	=> 'text',
					'compliRegisSideEventId'	=> $regisSideEventId,
					'class'	=> 'width50per compliUnitPriceEnter registrantselectioncompli'.$regisSideEventId,
				);		
				
	$SideRegisCompliGuestsGST = array(
					'name'	=> 'SideRegisCompliGuestsGST'.$regisSideEventId,
					'value'	=> $SideRegisCompliGuestsGSTVal,
					'id'	=> 'SideRegisCompliGuestsGST'.$regisSideEventId,
					'type'	=> 'text',
					'class'	=> 'width50per registrantselectioncompli'.$regisSideEventId,
				);			
	
	$SideRegisCompliGuestsPrice = array(
				'name'	=> 'SideRegisCompliGuestsPrice'.$regisSideEventId,
				'value'	=> $SideRegisCompliGuestsPriceVal,
				'id'	=> 'SideRegisCompliGuestsPrice'.$regisSideEventId,
				'type'	=> 'text',
				'class'	=> 'width50per registrantselectioncompli'.$regisSideEventId,
			);					
	
	$SideEventRegistaintId = array(
					'name'	=> 'SideEventRegistaintId'.$sideEventId.'['.$registrantId.']['.$singleDayRegistId.']',
					'value'	=> $SideEventRegistaintIdVal,
					'id'	=> 'SideEventRegistaintId'.$regisSideEventId,
					'type'	=> 'hidden',
				);		
				
	
	$SideRegisAddiGuestsMaxNo = array(
		'name'	=> 'SideRegisAddiGuestsMaxNo'.$regisSideEventId,
		'value'	=> $SideRegisAddiGuestsMaxNoVal,
		'id'	=> 'SideRegisAddiGuestsMaxNo'.$regisSideEventId,
		'type'	=> 'text',
		'class' => 'width_50px clr_425968 spinner_selectbox registrantselectionaddi'.$regisSideEventId,
		//'required'	=> '',
		//'disabled' => '',
		'min' => '0',
		);	
		
	$SideRegisCompliGuestsMaxNo = array(
		'name'	=> 'SideRegisCompliGuestsMaxNo'.$regisSideEventId,
		'value'	=> $SideRegisCompliGuestsMaxNoVal,
		'id'	=> 'SideRegisCompliGuestsMaxNo'.$regisSideEventId,
		'type'	=> 'text',
		'class' => 'width_50px clr_425968 spinner_selectbox registrantselectioncompli'.$regisSideEventId,
		//'required'	=> '',
		//'disabled' => '',
		'min' => '0',
		);								
								
	echo form_input($SideEventRegistaintId);	
	

	// if registraint selection is checked	
	if($registrantSelectionIsChcked){
		$SideEventRegisAddiDetails['required']	 	 	 = 	'';
		//check if complimentary not checked then set required
		if($SideEventRegisComaplimentaryVal==false){
			$SideEventRegisUnitPrice['required']			 = 	'';
			$SideEventRegisTotalPrice['required']			 = 	'';
			$SideEventRegisGSTInclude['required']			 = 	'';
			$SideEventRegisEarlyUnitPrice['required']		 = 	'';
			$SideEventRegisEarlyTotalPrice['required']		 = 	'';
			$SideEventRegisEarlyGSTInclude['required']		 = 	'';
		}
	}
	
	// if registraint additional is checked	
	$AddiGuestsMaxNoRequired = '';
	if($SideRegisAddiGuestsVal){
		$SideRegisAddiGuestsUnitPrice['required']	 	 	 = 	'';
		$SideRegisAddiGuestsPrice['required']	 	 	 = 	'';
		$SideRegisAddiGuestsGST['required']				 = 	'';
		$AddiGuestsMaxNoRequired = 'required = ""';
	}
	
	// if registraint complimentary is checked	
	$CompliGuestsMaxNoRequired = '';
	if($SideRegisCompliGuestsVal){
		$SideRegisCompliGuestsUnitPrice['required']	 	 	 = 	'';
		$SideRegisCompliGuestsPrice['required']	 	 	 = 	'';
		$SideRegisCompliGuestsGST['required']			 = 	'';
		$CompliGuestsMaxNoRequired = 'required = ""';
	}
	
?>

	<div class="formparag mb10">
		<div class="accordian_headingbg bg_0073ae">
			<div class="checkradiobg typregisrcheck">
				<div class="checkbox pl0 position_R setupcheck">
					<?php echo form_checkbox($registrantSelection); ?>
					<label for="registrantSelection<?php echo $regisSideEventId; ?>" id="<?php echo $regisSideEventId; ?>" class=" width_auto ml0 eventDcheck">&nbsp;<?php echo $registrantName; ?></label>
				</div>
			</div>
		</div>
		
		<div class="accordian_cnt <?php echo $registrantSelectionDiv ?>" id="side_event_registraint_list<?php echo $regisSideEventId ?>">
		
		<div class="control-group mb10 allowword">
			<label class="control-label" for="inputEmail"><?php echo lang('event_side_complimentary'); ?></label>
			<div class="controls checkradiobg setupcheck">
				<div class="checkbox pl0 position_R">
					<?php echo form_checkbox($SideEventRegisComaplimentary); ?>
					<label for="SideEventRegisComaplimentary<?php echo $regisSideEventId; ?>" class="width_auto ml0 mt10 eventDcheck"></label>
				</div>
			</div>
		</div>
		
		<div class="control-group mb10">
			<label for="inputEmail" class="control-label"> <?php echo lang('event_side_regis_limit'); ?>
				<font class="mandatory_cls">*</font>
				<div class="infoiconbg eventdetai_tooltip bg_F89728">
					<span class="show_tool_tip" title='<?php echo lang('tooltip_side_regis_limit'); ?>'> </span>
				</div>
			</label>
			<div class="controls">
				<?php echo form_input($registrantLimit); ?>	
			</div>
		</div>
		
		<div class="control-group mb10">
			<label for="inputEmail" class="control-label"> <?php echo lang('event_side_additional_details'); ?>
				<font class="mandatory_cls">*</font>
				<div class="infoiconbg eventdetai_tooltip bg_F89728">
					<span class="show_tool_tip" title='<?php echo lang('tooltip_side_additional_details'); ?>'> </span>
				</div>
			</label>
			<div class="controls">
				<?php echo form_textarea($SideEventRegisAddiDetails); ?>	
			</div>
		</div>
		
		
		<div  class="<?php echo $SideEventRegisComaplimentaryDiv; ?>" id="compli_checked_div_section<?php echo $regisSideEventId; ?>">
			<div class="control-group mb10">
				<label for="inputEmail" class="control-label">	<?php echo lang('event_side_unit_price'); ?>
				<font class="mandatory_cls">*</font>
				<div class="infoiconbg eventdetai_tooltip bg_F89728">
					<span class="show_tool_tip" title='<?php echo lang('tooltip_side_unit_price'); ?>'> </span>
				</div>
				</label>
				<div class="controls">
					<?php echo form_input($SideEventRegisUnitPrice); ?>	
					<?php echo form_error('SideEventRegisUnitPrice'.$regisSideEventId); ?>
				</div>
			</div>
			<div class="control-group mb10">
				<label for="inputEmail" class="control-label">	<?php echo lang('event_side_gst_include'); ?>
					<font class="mandatory_cls">*</font>
				<div class="infoiconbg eventdetai_tooltip bg_F89728">
					<span class="show_tool_tip" title='<?php echo lang('tooltip_side_gst_include'); ?>'> </span>
				</div>
				</label>
				<div class="controls">
					<?php echo form_input($SideEventRegisGSTInclude); ?>	
					<?php echo form_error('SideEventRegisGSTInclude'.$regisSideEventId); ?>
				</div>
			</div>
			
			<div class="control-group mb10">
				<label for="inputEmail" class="control-label">	<?php echo lang('event_side_total_price'); ?>
				<font class="mandatory_cls">*</font>
				<div class="infoiconbg eventdetai_tooltip bg_F89728">
					<span class="show_tool_tip" title='<?php echo lang('tooltip_side_total_price'); ?>'> </span>
				</div>
				</label>
				<div class="controls">
					<?php echo form_input($SideEventRegisTotalPrice); ?>	
					<?php echo form_error('SideEventRegisTotalPrice'.$regisSideEventId); ?>
				</div>
			</div>
			
			<?php 
			//if user checked in registrant for allow early bird then show this section
			if($allowEarlybird=="1"){ 
				
				//set value in hidden field
				echo form_input($SideEventRegisEarlyIsAllowed);	
				?>
			
			
			
			<div class="control-group mb10">
				<label for="inputEmail" class="control-label">	<?php echo lang('event_side_early_unit_price'); ?>
					<font class="mandatory_cls">*</font>
				<div class="infoiconbg eventdetai_tooltip bg_F89728">
					<span class="show_tool_tip" title='<?php echo lang('tooltip_side_early_unit_price'); ?>'> </span>
				</div>
				</label>
				<div class="controls">
					<?php echo form_input($SideEventRegisEarlyUnitPrice); ?>	
					<?php echo form_error('SideEventRegisEarlyUnitPrice'.$regisSideEventId); ?>
				</div>
			</div>
			<div class="control-group mb10">
				<label for="inputEmail" class="control-label">	<?php echo lang('event_side_early_gst_include'); ?>
					<font class="mandatory_cls">*</font>
				<div class="infoiconbg eventdetai_tooltip bg_F89728">
					<span class="show_tool_tip" title='<?php echo lang('tooltip_side_early_gst_include'); ?>'> </span>
				</div>
				</label>
				<div class="controls">
					<?php echo form_input($SideEventRegisEarlyGSTInclude); ?>	
					<?php echo form_error('SideEventRegisEarlyGSTInclude'.$regisSideEventId); ?>
				</div>
			</div>
			
			<div class="control-group mb10">
				<label for="inputEmail" class="control-label">	<?php echo lang('event_side_early_total_price'); ?>
					<font class="mandatory_cls">*</font>
				<div class="infoiconbg eventdetai_tooltip bg_F89728">
					<span class="show_tool_tip" title='<?php echo lang('tooltip_side_early_total_price'); ?>'> </span>
				</div>
				</label>
				<div class="controls">
					<?php echo form_input($SideEventRegisEarlyTotalPrice); ?>	
					<?php echo form_error('SideEventRegisEarlyTotalPrice'.$regisSideEventId); ?>
				</div>
			</div>
		
		<?php	} ?>
		
		</div>
		
		
		<div class="mb10">
			<?php echo lang('event_side_dietary_msg'); ?>
		</div>
		
		<!------Additional Guests section start----->
			<div class="control-group mb10">
				<label for="inputEmail" class="control-label"> <?php echo lang('event_side_additional_guest'); ?>
				<div class="infoiconbg eventdetai_tooltip bg_F89728">
					<span class="show_tool_tip" title='<?php echo lang('tooltip_side_additional_guest'); ?>'> </span>
				</div>
				</label>
				<div class="controls checkradiobg setupcheck">
					<div class="checkbox pl0 position_R">
						<?php echo form_checkbox($SideRegisAddiGuests); ?>
						<label for="SideRegisAddiGuests<?php echo $regisSideEventId; ?>" class="width_auto ml0 mt10 eventDcheck"></label>
					</div>
				</div>
			</div>
			
			<div class="<?php echo $SideRegisAddiGuestsIsShow; ?> side_regis_addi_guests_div<?php echo $regisSideEventId; ?>">
				
				<div class="control-group mb10 registrat_setup">
					<label for="inputEmail" class="control-label"><?php echo lang('event_side_add_max_num'); ?>
					 <font class="mandatory_cls">*</font>
					</label>
					<div class="controls ">
						<?php 
							echo form_input($SideRegisAddiGuestsMaxNo); 
							echo form_error('SideRegisAddiGuestsMaxNo');
						?>
					</div>
				</div>
				
				<div class="control-group mb10">
					<label for="inputEmail" class="control-label">	<?php echo lang('event_side_add_unit_price'); ?>
						<font class="mandatory_cls">*</font>
					<div class="infoiconbg eventdetai_tooltip bg_F89728">
						<span class="show_tool_tip" title='<?php echo lang('tooltip_side_add_unit_price'); ?>'> </span>
					</div>
					</label>
					<div class="controls">
						<?php echo form_input($SideRegisAddiGuestsUnitPrice); ?>	
						<?php echo form_error('SideRegisAddiGuestsUnitPrice'.$regisSideEventId); ?>
					</div>
				</div>
				
				
				<div class="control-group mb10">
					<label for="inputEmail" class="control-label">	<?php echo lang('event_side_add_gst_include'); ?>
						<font class="mandatory_cls">*</font>
					<div class="infoiconbg eventdetai_tooltip bg_F89728">
						<span class="show_tool_tip" title='<?php echo lang('tooltip_side_add_gst_include'); ?>'> </span>
					</div>
					</label>
					<div class="controls">
						<?php echo form_input($SideRegisAddiGuestsGST); ?>	
						<?php echo form_error('SideRegisAddiGuestsGST'.$regisSideEventId); ?>
					</div>
				</div>
				
				<div class="control-group mb10">
					<label for="inputEmail" class="control-label">	<?php echo lang('event_side_add_total_price'); ?>
						<font class="mandatory_cls">*</font>
					<div class="infoiconbg eventdetai_tooltip bg_F89728">
						<span class="show_tool_tip" title='<?php echo lang('tooltip_side_add_total_price'); ?>'> </span>
					</div>
					</label>
					<div class="controls">
						<?php echo form_input($SideRegisAddiGuestsPrice); ?>	
						<?php echo form_error('SideRegisAddiGuestsPrice'.$regisSideEventId); ?>
					</div>
				</div>
			</div>
		<!------Additional Guests section end----->
			
		<!------Complimentary Guests section start----->
			<div class="control-group mb10 ">
				<label for="inputEmail" class="control-label"> <?php echo lang('event_side_compliemntary_guest'); ?>
				<div class="infoiconbg eventdetai_tooltip bg_F89728">
					<span class="show_tool_tip" title='<?php echo lang('tooltip_side_compliemntary_guest'); ?>'> </span>
				</div>
				</label>
				<div class="controls checkradiobg setupcheck">
					<div class="checkbox pl0 position_R">
						<?php echo form_checkbox($SideRegisCompliGuests); ?>
						<label for="SideRegisCompliGuests<?php echo $regisSideEventId; ?>"  class="width_auto ml0 mt10 eventDcheck">
						  
						</label>
					</div>
				</div>
			</div>
		
			<div class="<?php echo $SideRegisCompliGuestsIsShow; ?> side_regis_compli_guests_div<?php echo $regisSideEventId; ?>">
				<div class="control-group mb10 registrat_setup">
					<label for="inputEmail" class="control-label"><?php echo lang('event_side_compli_max_num'); ?>
					 	<font class="mandatory_cls">*</font>
					</label>
					<div class="controls">
						<?php 
							echo form_input($SideRegisCompliGuestsMaxNo); 
							echo form_error('SideRegisCompliGuestsMaxNo');
						?>
					</div>
				</div>
				<div class="control-group mb10">
					<label for="inputEmail" class="control-label">	<?php echo lang('event_side_compli_unit_price'); ?>
						<font class="mandatory_cls">*</font>
					<div class="infoiconbg eventdetai_tooltip bg_F89728">
						<span class="show_tool_tip" title='<?php echo lang('tooltip_side_compli_unit_price'); ?>'> </span>
					</div>
					</label>
					<div class="controls">
						<?php echo form_input($SideRegisCompliGuestsUnitPrice); ?>	
						<?php echo form_error('SideRegisCompliGuestsUnitPrice'.$regisSideEventId); ?>
					</div>
				</div>
				
				<div class="control-group mb10">
					<label for="inputEmail" class="control-label">	<?php echo lang('event_side_compli_gst_include'); ?>
						<font class="mandatory_cls">*</font>
					<div class="infoiconbg eventdetai_tooltip bg_F89728">
						<span class="show_tool_tip" title='<?php echo lang('tooltip_side_compli_gst_include'); ?>'> </span>
					</div>
					</label>
					<div class="controls">
						<?php echo form_input($SideRegisCompliGuestsGST); ?>	
						<?php echo form_error('SideRegisCompliGuestsGST'.$regisSideEventId); ?>
					</div>
				</div>
				
				<div class="control-group mb10">
					<label for="inputEmail" class="control-label">	<?php echo lang('event_side_compli_total_price'); ?>
						<font class="mandatory_cls">*</font>
					<div class="infoiconbg eventdetai_tooltip bg_F89728">
						<span class="show_tool_tip" title='<?php echo lang('tooltip_side_compli_total_price'); ?>'> </span>
					</div>
					</label>
					<div class="controls">
						<?php echo form_input($SideRegisCompliGuestsPrice); ?>	
						<?php echo form_error('SideRegisCompliGuestsPrice'.$regisSideEventId); ?>
					</div>
				</div>
			
			</div>
	
		<!------Complimentary Guests section end----->
	
	</div>

</div>
		

	
