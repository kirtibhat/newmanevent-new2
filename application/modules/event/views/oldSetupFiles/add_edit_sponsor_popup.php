<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//--------add type fo registration fields---------//	

$sponsorNameValue = (!empty($sponsorsdata->sponsor_name))?$sponsorsdata->sponsor_name:''; 
$headerAction = ($sponsorsIdValue > 0)?lang('event_sponsor_edit'):lang('event_sponsor_add'); 


$formAddSponsors = array(
		'name'	 => 'formAddSponsors',
		'id'	 => 'formAddSponsors',
		'method' => 'post',
		'class'  => 'form-horizontal mt10',
	);
	
$sponsorName = array(
		'name'	=> 'sponsorName',
		'value'	=> $sponsorNameValue,
		'id'	=> 'sponsorName',
		'type'	=> 'text',
	//	'placeholder'	=> 'Sponsor Name',
		'required'	=> '',
	);
	
$sponsorsId = array(
		'name'	=> 'sponsorsId',
		'value'	=> $sponsorsIdValue,
		'id'	=> 'sponsorsId',
		'type'	=> 'hidden',
	);


?>

<!--------Add braakout div start-------->
	<div class="modal fade alertmodelbg dn" id="add_edit_sponsors_popup_div" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg_a54399">
				<h4 class="modal-title"><?php echo $headerAction; ?></h4>
			</div>
			<?php  echo form_open(base_url('event/addeditsponsorssave'),$formAddSponsors); ?>
				<div class="modal-body ">
				<h4><?php echo lang('event_sponsor_add_title'); ?></h4>
				
					<div class="control-group mb10 min_height_75">
						<label for="inputEmail" class="control-label"><?php echo lang('event_sponsor_name'); ?>
							<font class="mandatory_cls">*</font>
						</label>
						<div class="controls">
							<?php echo form_input($sponsorName); ?>
						</div>
					</div>
			</div>
			<div class="modal-footer">
				<?php 
					echo form_hidden('eventId', $eventId);
					echo form_input($sponsorsId);
					$extraCancel 	= 'class="btn btn-default custombtn" data-dismiss="modal" ';
					$extraSave 	= 'class="btn btn-primary  bg_a54399 custombtn bg_731472" ';
					echo form_button('cancel',lang('comm_cancle'),$extraCancel);
					echo form_submit('save',lang('comm_save'),$extraSave);
				?>
			</div>
		</div>
		<?php echo form_close(); ?>
	</div>
</div>
<!--------Add braakout div end-------->
