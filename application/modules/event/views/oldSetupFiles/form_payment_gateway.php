<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

$paymentgatewaydata			= (!empty($paymentgatewaydata))?$paymentgatewaydata[0]:'';
$getwayId 					= (!empty($paymentgatewaydata->id))?$paymentgatewaydata->id:'0';
$merchantIdValue 			= (!empty($paymentgatewaydata->merchant_id))?$paymentgatewaydata->merchant_id:'';
$paymentGatewayTypeValue	= (!empty($paymentgatewaydata->payment_gateway_type))?$paymentgatewaydata->payment_gateway_type:'';

$paymentTypeList = array('paypal'=>'Paypal');

//define current and next form div id
$FormDivId = 'SecondForm';
$nextFormDivId = 'ThirdForm';

$formPaymentGateway = array(
		'name'	 => 'formPaymentGateway',
		'id'	 => 'formPaymentGateway',
		'method' => 'post',
		'class'  => 'form-horizontal',
	);

$merchantIdField = array(
		'name'	=> 'merchantIdField',
		'value'	=>  $merchantIdValue,
		'id'	=> 'merchantIdField',
		'required'	=> '',
		'type' => 'text',
	);
	
	
?>


<div class="commonform_bg pb0">
	<div class="headingbg bg_0073ae position_R">
		<div class="typeofregistV pull-left showhideaction ptr" id="<?php echo $FormDivId; ?>">
			<?php echo lang('event_payment_payment_gateway'); ?>
		</div>
		<div class="clearfix">
		</div>
	</div>
	<?php echo form_open($this->uri->uri_string(),$formPaymentGateway); ?>
		<div class="form-horizontal dn form_open_chk" id="showhideformdiv<?php echo $FormDivId; ?>">
	
		
		<div class="control-group mb10">
			<label class="control-label" for="inputEmail">Payment Gateway <span class="color_f89828">*</span>
			<div class="infoiconbg">
			</div>
			</label>
			<div class="controls">
				<div class="pull-left select-style register select_personal">
					<?php 
							$other = ' id="paymentGetwayType" required =""';
							echo form_dropdown('paymentGetwayType',$paymentTypeList,$paymentGatewayTypeValue,$other);
						?>
				</div>
			</div>
		</div>
		
		<div class="control-group mb10">
			<label class="control-label" for="inputEmail"><?php echo lang('event_payment_marchant_id'); ?>
				<font class="mandatory_cls">*</font>
			</label>
			<div class="controls">
				<?php echo form_input($merchantIdField); ?>
				<?php echo form_error('merchantIdField'); ?>
			</div>
		</div>
		
		<div class="spacer10"></div>
		<div class="row-fluid">
			<?php 
				echo form_hidden('eventId', $eventId);
				echo form_hidden('getwayId', $getwayId);
				echo form_hidden('formActionName', 'paymentGateway');
				
				//button show of save and reset
				$formButton['showbutton']   = array('save','reset');
				$formButton['labletext'] 	= array('save'=>lang('comm_save_changes'),'reset'=>lang('comm_reset'));
				$formButton['buttonclass']  = array('save'=>'submitbtn_cus mr_16','reset'=>'submitbtn_cus reset_form');
				$this->load->view('common_save_reset_button',$formButton);
			?>
		</div>
		<div class="spacer36">
		</div>
	</div>
	<?php echo form_close(); ?>
</div>
<!-- /commonform_bg -->

<script type="text/javascript">
	//payment credit card fees save 
	ajaxdatasave('formPaymentGateway','<?php echo $this->uri->uri_string(); ?>',true,true,false,false,false,'#showhideformdiv<?php echo $FormDivId; ?>','#showhideformdiv<?php echo $nextFormDivId; ?>');
</script>	
