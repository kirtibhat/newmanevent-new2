<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

//set exhibitor form data
$exhibitorId 					   = (!empty($formdata->id))?$formdata->id:'';;
$exhibitorName 					   = (!empty($formdata->exhibitor_name))?$formdata->exhibitor_name:'';
$additionalDetailsValue			   = (!empty($formdata->additional_details))?$formdata->additional_details:'';
$limitOfPackageValue			   = (!empty($formdata->limit_of_package))?$formdata->limit_of_package:'';
$totalPriceValue 				   = (!empty($formdata->total_price))?$formdata->total_price:'';
$GSTIncludedValue 				   = (!empty($formdata->gst))?$formdata->gst:'';
$attendBreakoutsValue	    	   = (isset($formdata->attend_breakouts))?$formdata->attend_breakouts:'';
$attendBreakoutsYesValue = true;
$attendBreakoutsNoValue = false;

//set attend breakouts value
if($attendBreakoutsValue=='1'){
	$attendBreakoutsYesValue = true;
	$attendBreakoutsNoValue  = false;
}else{
	$attendBreakoutsYesValue = false;
	$attendBreakoutsNoValue  = true;
}

//form create variable
$formSetupExhibitor = array(
		'name'	 => 'formSetupExhibitor'.$exhibitorId,
		'id'	 => 'formSetupExhibitor'.$exhibitorId,
		'method' => 'post',
		'class'  => 'form-horizontal',
	);

$additionalDetails = array(
		'name'	=> 'additionalDetails'.$exhibitorId,
		'value'	=>  $additionalDetailsValue,
		'id'	=> 'additionalDetails'.$exhibitorId,
		'required'	=> '',
		'rows' 	=> '2',
		'class' => 'bdr_n',
	);		
	
$limitOfPackage = array(
		'name'	=> 'limitOfPackage'.$exhibitorId,
		'value'	=>  $limitOfPackageValue,
		'id'	=> 'limitOfPackage'.$exhibitorId,
		'required'	=> '',
		'type' => 'text',
		'class' => 'width50per',
	);

$totalPrice = array(
		'name'	=> 'totalPrice'.$exhibitorId,
		'value'	=>  $totalPriceValue,
		'id'	=> 'totalPrice'.$exhibitorId,
		'required'	=> '',
		'type' => 'text',
		'class' => 'width50per',
	);

$GSTIncluded = array(
		'name'	=> 'GSTIncluded'.$exhibitorId,
		'value'	=>  $GSTIncludedValue,
		'id'	  => 'GSTIncluded'.$exhibitorId,
		'required'	=> '',
		'type'    => 'text',
		'class' => 'width50per',
	);
	
	
		
$attendBreakoutsYes = array(
		'name'	=> 'attendBreakouts'.$exhibitorId,
		'value'	=> '1',
		'id'	=> 'attendBreakoutsYes'.$exhibitorId,
		'type'	=> 'radio',
		'required'	=> '',
		'class'	=> 'moveRegistrant',
		'checked'	=> $attendBreakoutsYesValue,
		
	);	

$attendBreakoutsNo = array(
		'name'	=> 'attendBreakouts'.$exhibitorId,
		'value'	=> '0',
		'id'	=> 'attendBreakoutsNo'.$exhibitorId,
		'type'	=> 'radio',
		'required'	=> '',
		'class'	=> 'moveRegistrant',
		'checked'	=> $attendBreakoutsNoValue,
	);	

?>

	<div class="commonform_bg pb0">
		<div class="headingbg bg_79496a position_R">
			<div class="typeofregistV pull-left showhideaction ptr" id="<?php echo $exhibitorId; ?>">
				<?php echo $exhibitorName; ?>
			</div>
			<div class="contPadiv">
				<div class="icontitlecontainer right_del delete_exhibitor" deleteid="<?php echo $exhibitorId; ?>">
					<div class="delet_icon">
					</div>
					<div class="icontext">
						<?php echo lang('comm_delete'); ?>
					</div>
				</div>
				<div class="icontitlecontainer right_edit add_edit_exhibitor" exhibitorid="<?php echo $exhibitorId; ?>">
					<div class="edittitle_icon bg_5c2946">
					</div>
					<div class="icontext">
						<?php echo lang('comm_edit_title'); ?>
					</div>
				</div>
			</div>
			<div class="clearfix">
			</div>
		</div>
		<?php  echo form_open($this->uri->uri_string(),$formSetupExhibitor); ?>
		<div class="form-horizontal dn form_open_chk" id="showhideformdiv<?php echo $exhibitorId; ?>">
			<div class="control-group mb10">
				<label class="control-label" for="inputEmail">	<?php echo lang('event_exhibitor_additionla_details'); ?>
					<font class="mandatory_cls">*</font>
					<div class="infoiconbg bg_5c2946 exhibitor_tooltip">
						<span class="show_tool_tip" title='<?php echo lang('tooltip_exhibitor_additionla_details'); ?>'> </span>
					</div>
				</label>
				<div class="controls">
					<?php echo form_textarea($additionalDetails); ?>
					<?php echo form_error('additionalDetails'.$exhibitorId); ?>
				</div>
			</div>
			<div class="control-group mb10">
				<label class="control-label" for="inputEmail"><?php echo lang('event_exhibitor_limit_package'); ?>
					<font class="mandatory_cls">*</font>
				</label>
				<div class="controls">
					<?php echo form_input($limitOfPackage); ?>
					<?php echo form_error('limitOfPackage'.$exhibitorId); ?>
				</div>
			</div>
			<div class="control-group mb10">
				<label class="control-label" for="inputEmail"><?php echo lang('event_exhibitor_total_price'); ?>
					<font class="mandatory_cls">*</font>
				</label>
				<div class="controls">
					<?php echo form_input($totalPrice); ?>
					<?php echo form_error('totalPrice'.$exhibitorId); ?>
				</div>
			</div>
			<div class="control-group mb10">
				<label class="control-label" for="inputEmail"><?php echo lang('event_exhibitor_gst_include'); ?>
					<font class="mandatory_cls">*</font>
				</label>
				<div class="controls">
					<?php echo form_input($GSTIncluded); ?>
					<?php echo form_error('GSTIncluded'.$exhibitorId); ?>
				</div>
			</div>
			<div class="spacer10">
			</div>
			<div class="formparag mb20">
				<div class="bdr_B_spons">
				</div>
			</div>
			<div class="formparag mb20">
				<div>
					<?php echo lang('event_exhibitor_booth_available'); ?>
				</div>
				<div class="clearfix">
				</div>
				
					<?php if(!empty($exhibitorFloorPlan)){
					
					//get exhibitor folor plane listing data
					$where = array('exhibitor_id'=>$exhibitorId);
					$exhibitorBoothsData = getDataFromTabel('sponsor_exhibitor_booths','id,booth_position',$where);
					$exhibitorBoothsArray = array();
					$eventBoothsUsedArray = array();
					
					
					//prepare event booth array for finding value
					if(!empty($exhibitorbooths)){
						foreach($exhibitorbooths as $booths){
							$eventBoothsUsedArray[$booths->id] = $booths->booth_position;
						}
					}
					
					//prepare exhibitor booth array for finding value
					if(!empty($exhibitorBoothsData)){
						foreach($exhibitorBoothsData as $booths){
							$exhibitorBoothsArray[$booths->id] = $booths->booth_position;
						}
					}
					
					//show floor plan numbers 
					$exhibitorFloorPlanData = $exhibitorFloorPlan[0];
					$numberofFloorPlan 		= $exhibitorFloorPlanData->number_of_booth;
					for($i=1;$i<=$numberofFloorPlan;$i++){
					$boothNumberVal = $i;	
					$boothNumberValSet = $i;
					$boothNumberShow = singlenumbertwo($i);	
					$boolNumberAvailableField = $exhibitorId.'_'.$boothNumberVal;
					$isCheckBooth = false;
					$boothPosition = '0';
					$checkBoxBgClass = 'setupExhicheck';
					
					//check value in event booth array
					if(!empty($eventBoothsUsedArray)){
						$boothPosition = array_search($boothNumberVal, $eventBoothsUsedArray);
						if($boothPosition){
							$isCheckBooth = true;
							$checkBoxBgClass='setupspons_dis'; // set class for event booth checked
							$boothNumberValSet = 0;
						}
					}
					
					//check value in exhibitor booth array
					if(!empty($exhibitorBoothsArray)){
						$boothPosition = array_search($boothNumberVal, $exhibitorBoothsArray);
						if($boothPosition){
							$isCheckBooth = true;
							$checkBoxBgClass='setupspons'; // set class for exhibitor booth checked
							$boothNumberValSet = $boothNumberVal;
						}
					}
					
					$boolNumberAvailable = array(
						'name'	=> 'boolNumberAvailable'.$boolNumberAvailableField,
						'value'	=>  $boothNumberValSet,
						'id'	=> 'boolNumberAvailable'.$boolNumberAvailableField,
						'type' => 'checkbox',
						'class' => 'pull-left ml10 mr15 checkbox',
						'checked' => $isCheckBooth,
					);
					
					
					$boolNumberAvailableHidden = array(
						'name'	=> 'boolNumberAvailableHidden'.$exhibitorId.'['.$boothNumberVal.']',
						'value'	=>  $boothPosition,
						'type' => 'hidden',
					);
					
					echo form_input($boolNumberAvailableHidden);
				?>
				
					<div class="checkradiobg <?php echo $checkBoxBgClass; ?>  setupspons_1 pull-left ml25">
						<div class="checkbox">
							<?php echo form_checkbox($boolNumberAvailable);?>
							<label for="boolNumberAvailable<?php echo $boolNumberAvailableField; ?>" class="width_auto ml0 mt10 eventDcheck"><?php echo $boothNumberShow; ?></label>
						</div>
					</div>
				<?php } } ?>
				
			
				<div class="clearfix">
				</div>
			</div>
			<div class="spacer10">
			</div>
			<div class="formparag mb20">
				<div class="bdr_B_spons">
				</div>
			</div>
			<div class="formparag mb20">
				 <?php echo lang('event_exhibitor_side_event_msg'); ?>
			</div>
			
			<?php  if(!empty($eventsideevent)) { 
			foreach($eventsideevent as $eventside){ 
				$sideEventName = $eventside->side_event_name;
				
				$sideEventId = $eventside->id;
				$rowKeyId = $exhibitorId.'_'.$sideEventId;
				
				//This section for single day registrant listing start
				$where = array('side_event_id'=>$sideEventId,'exhibitor_id'=>$exhibitorId);
				$exhibitorSideeventData = getDataFromTabel('exhibitor_side_event','id,side_event_limit',$where);
				$exhibitorSideeventId	  		  = '0';
				$complimentarySideeventVal	  = '0';
				if(!empty($exhibitorSideeventData)){
					$exhibitorSideeventData = $exhibitorSideeventData[0];
					$exhibitorSideeventId = $exhibitorSideeventData->id;
					$complimentarySideeventVal = $exhibitorSideeventData->side_event_limit;
				}
				
				$complimentarySideevent = array(
					'name'	=> 'complimentarySideevent'.$rowKeyId,
					'value'	=> $complimentarySideeventVal,
					'id'	=> 'complimentarySideevent',
					'type'	=> 'text',
					'class' => 'width_50px clr_425968 spinner_selectbox',
					'readonly' => '',
					'min' => '0',
					);	
					
				$complimentarySideeventHidden = array(
					'name'	=> 'complimentarySideeventHidden'.$exhibitorId.'['.$sideEventId.']',
					'value'	=> $exhibitorSideeventId,
					'type'	=> 'hidden',
					);	
					
			echo form_input($complimentarySideeventHidden);		
						
			?>
			
			<div class="control-group mb10">
				<label class="control-label" for="inputEmail"><?php echo $sideEventName; ?></label>
				<div class="controls exhibitor_setup">
					<div class="inputnubcontainer ml0 pull-left">
						<?php echo form_input($complimentarySideevent); ?>
					</div>
				</div>
			</div>
			<?php  } }?>
			
			<div class="spacer10">
			</div>
			<div class="formparag mb20">
				<div class="bdr_B_spons">
				</div>
			</div>
			<div class="formparag mb20">
				 <?php echo lang('event_exhibitor_item_msg'); ?>
			</div>
			<div class="formparag mb20">
				<div class="regidetrailHeading">
					<a href="javascript:void(0)" class="add_edit_exhibitor_item" itemid="0" exhibitorid="<?php echo $exhibitorId; ?>">
					<div class="addbtn_form bg_5c2946">
					</div>
					<div class="addtype clr_79496A">
						<?php echo lang('event_exhibitor_add_item_title'); ?>
					</div>
					</a>
					<div class="clearfix">
					</div>
				</div>
			</div>
			
			<?php 
				//get value by item list by exhibitor id 
				$where = array('exhibitor_id'=>$exhibitorId);
				$exhibitorItemData = getDataFromTabel('exhibitor_item','*',$where,'','id','ASC');
				
				//check data is not empty
				$rowCount=1;
				if(!empty($exhibitorItemData)){
				foreach($exhibitorItemData as $exhibitorItem){	
					
				$exhibitorItemId = $exhibitorItem->id;
				$itemDescriptionValue = $exhibitorItem->item;		
				
				
				$rowKeyId = $exhibitorId.'_'.$exhibitorItemId;	
			
				$itemDescription = array(
						'name'	=> 'itemDescription'.$rowKeyId,
						'value'	=>  $itemDescriptionValue,
						'id'	  => 'itemDescription'.$rowKeyId,
						'required'	=> '',
						'rows' => '2',
						'class' => 'bdr_n exhibitor_textarea itemDescription'.$rowKeyId,
					);	
				
				$exhibitorItemIdHidden = array(
							'name'	=> 'exhibitorItemIdHidden'.$exhibitorId.'[]',
							'value'	=> $exhibitorItemId,
							'id'	=> 'exhibitorItemIdHidden'.$exhibitorId,
							'type'	=> 'hidden',
						);					
										
				echo form_input($exhibitorItemIdHidden);
			?>
				<div class="control-group mb10">
					<label class="control-label" for="inputEmail">Item <?php echo $rowCount; ?></label>
					<div class="controls greentooltip_del">
						<?php echo form_textarea($itemDescription); ?>
						<?php echo form_error('itemDescription'.$rowKeyId); ?>
						<a href="javascript:void(0)" class="deletsmall_icon delete_exhibitor_item show_tool_tip" title='<?php echo lang('tooltip_delete'); ?>' deleteid="<?php echo $exhibitorItemId; ?>" ></a>
					</div>
				</div>
				
			<?php } } ?>
			
			<div class="spacer10">
			</div>
			<div class="formparag mb20">
				<div class="bdr_B_spons">
				</div>
			</div>
			<div class="control-group mb10">
				<label class="control-label lineH16" for="inputEmail">	<?php echo lang('event_exhibitor_attend_breakout'); ?>
					<div class="infoiconbg bg_5c2946 exhibitor_tooltip">
						<span class="show_tool_tip" title='<?php echo lang('tooltip_exhibitor_attend_breakout'); ?>'> </span>
					</div>
				</label>
				<div class="controls">
					<div class="tworadiocontainer">
						<div class="checkradiobg">
							<div class="radio exhibitors">
								<?php echo form_radio($attendBreakoutsYes); ?>
								<label for="attendBreakoutsYes<?php echo $exhibitorId; ?>"><?php echo lang('comm_yes'); ?> &nbsp; &nbsp;</label>
								<?php echo form_radio($attendBreakoutsNo); ?>
								<label for="attendBreakoutsNo<?php echo $exhibitorId; ?>"><?php echo lang('comm_no'); ?> &nbsp; &nbsp;</label>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="spacer10">
			</div>
			<div class="row-fluid">
				<?php 
					echo form_hidden('eventId', $eventId);
					echo form_hidden('exhibitorId', $exhibitorId);
					
					//button show of save and reset
					$formButton['saveButtonId'] = 'id="'.$exhibitorId.'"'; // click button id
					$formButton['showbutton']   = array('save','reset');
					$formButton['labletext'] 	= array('save'=>lang('comm_save_changes'),'reset'=>lang('comm_reset'));
					$formButton['buttonclass']  = array('save'=>'submitbtn_cus mr_16 submit_regis_type','reset'=>'submitbtn_cus reset_form');
					$this->load->view('common_save_reset_button',$formButton);
				?>
			</div>
			<div class="spacer36">
			</div>
		</div>
		<?php echo form_close(); ?>
	</div>
	
<script type="text/javascript">
	//exhibitor event dtails save type details save
	ajaxdatasave('formSetupExhibitor<?php echo $exhibitorId; ?>','<?php echo $this->uri->uri_string(); ?>',true,true,false,false,false,'#showhideformdiv<?php echo $exhibitorId; ?>','#showhideformdiv<?php echo $nextRecordId; ?>');
</script>
