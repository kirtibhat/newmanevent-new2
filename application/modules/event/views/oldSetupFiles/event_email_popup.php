<!--end of pop2-->
<?php 

if(!empty($emailDetails)){
	$emailId 		= encode($emailDetails->email_id);
	$emailName 		= $emailDetails->email_title;
	$emailSubject   = $emailDetails->subject;
	$emailFromEmail = $emailDetails->from_email;
	$emailFromName  = $emailDetails->from_name;
	$emailContents  = $emailDetails->contents;
	$emailCategories = (!empty($emailDetails->category_id)) ?  unserialize($emailDetails->category_id) : ""; 
	if(empty($emailCategories)){
		$emailCategories = array();
	}
}else{
	$emailId		= "";
	$emailName 		= "";
	$emailSubject   = "";
	$emailFromEmail = "";
	$emailFromName  = "";
	$emailContents  = "";
	$emailCategories = array();
}

$formEventEmail = array(
		'name'	 => 'formEventEmail',
		'id'	 => 'formEventEmail',
		'method' => 'post',
		'class'  => 'wpcf7-form',
		'data-parsley-validate' => '',
		
);

$eventEmailName = array(
		'name'	=> 'ce_name',
		'value'	=> $emailName,
		'id'	=> 'ce_name',
		'type'	=> 'text',
		'class'	=> 'small',
		'required'=>'',
		'autocomplete' => 'off',
		'data-parsley-error-message' => lang('common_field_required'),
		'data-parsley-error-class' => 'custom_li',
		'data-parsley-trigger' => 'keyup'
);	

$eventEmailSubject = array(
		'name'	=> 'ce_subject',
		'value'	=> $emailSubject,
		'id'	=> 'ce_subject',
		'type'	=> 'text',
		'class'	=> 'small',
		'required'=>'',
		'autocomplete' => 'off',
		'data-parsley-error-message' => lang('common_field_required'),
		'data-parsley-error-class' => 'custom_li',
		'data-parsley-trigger' => 'keyup'
);	

$eventEmailFromEmail = array(
		'name'	=> 'ce_from',
		'value'	=> $emailFromEmail,
		'id'	=> 'ce_from',
		'type'	=> 'email',
		'class'	=> 'small',
		'required'=>'',
		'autocomplete' => 'off',
		//'data-parsley-error-message' => lang('common_field_required'),
		'data-parsley-type-message' => "This value should be a valid email.",
		'data-parsley-error-class' => 'custom_li',
		'data-parsley-trigger' => 'keyup'
);

$eventEmailFromName = array(
		'name'	=> 'ce_from_name',
		'value'	=> $emailFromName,
		'id'	=> 'ce_from_name',
		'type'	=> 'text',
		'class'	=> 'small',
		'required'=>'',
		'autocomplete' => 'off',
		'data-parsley-error-message' => lang('common_field_required'),
		'data-parsley-error-class' => 'custom_li',
		'data-parsley-trigger' => 'keyup'
);

$eventEmailContent = array(
		'name'	=> 'ce_content',
		'value'	=> $emailContents,
		'id'	=> 'ce_content',
		'type'	=> 'text',
		'class'	=> 'small',
		'cols'  => '3',
		'rows'  => '2',
		'required'=>'',
		'autocomplete' => 'off',
		'data-parsley-error-message' => lang('common_field_required'),
		'data-parsley-error-class' => 'custom_li',
		'data-parsley-trigger' => 'keyup'
);               
?>

<div id="custom_email" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" >
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header delegates"> 
        <h4 class="medium dt-large modal-title"><?php echo ($emailId=="") ? lang('add_custom_email_popup') : lang('edit_custom_email_popup'); ?></h4>
      </div>

      <div class="modal-body small">
        <div class="modelinner ">
           <?php echo form_open($this->uri->uri_string(),$formEventEmail); ?>
           <input type="hidden" value="collapseSeven" name="collapseValue">
           <input type="hidden" value="<?php echo $emailId; ?>" name="emailId" />
           
            <div class="control-group mb10 eventdashboard_popup small" >

              <div class="row-fluid">
              	<label class="pull-left" for="ce_name"><?php echo lang('custom_email_name'); ?>
                	<span class="info_btn"><span class="field_info xsmall"><?php echo lang('tooltip_custom_email_name'); ?></span></span>
                </label>
                <?php echo form_input($eventEmailName); ?>
              </div>

              <div class="row-fluid">
              	<label class="pull-left" for="ce_subject"><?php echo lang('custom_email_subject'); ?></label>
                <?php echo form_input($eventEmailSubject); ?>
              </div>

              <div class="row-fluid">
              	<label class="pull-left" for="ce_from"><?php echo lang('custom_email_from_email'); ?></label>
              	<?php echo form_input($eventEmailFromEmail); ?>
              </div>

              <div class="row-fluid">
              	<label class="pull-left" for="ce_from_name"><?php echo lang('custom_email_from_name'); ?></label>
              	<?php echo form_input($eventEmailFromName); ?>
              </div>

              <div class="row-fluid">
              	<label class="pull-left" for="ce_from_name"><?php echo lang('custom_email_content'); ?></label>
                <?php echo form_textarea($eventEmailContent); ?>
              </div>

           <!---------- Enail Attachment : Start ----------------->	
          
           <!---------- Enail Attachment : Start ----------------->	
            <div class="row-fluid" id="attch_FileContainer_email">
					<label for="attach_doc"><?php echo lang('custom_email_attachment'); ?></label>
					<div class="custom-upload" id="attch_pickfiles_email">
						<span class="label_text">Select or Drop File</span>
						<input type="file" name="attach_doc_email" id="attach_doc_email" draggable="true">
					</div>
					<div id="attch_file_listEmail" class="custom-upload-file "></div>
			</div>
			
           <?php // get email attachments
				 $attachmentData=getDataFromTabelWhereOr('event_email_attachments','*',array('email_id'=>decode($emailId)));
           ?>
            <div id="attach_file_list_mainEmail"  class="<?php echo (empty($attachmentData))?'dn':' '; ?>">
				<div id="attch_file_listEmail" class="controls ">
					<?php if(!empty($attachmentData)){ 
						    foreach($attachmentData as $attachmentfiles){
						    $fileId= remove_extension($attachmentfiles->document_file);	
							?>
							  <div class="attchadddedfileEmail uploaded_file" id="<?php echo $fileId; ?>"> 
									<div class="imagename_cont pr mr_5px"><?php echo $attachmentfiles->document_file; ?>
										<div class="pa ptr top_6 right_10 delete_file" deleteurl='event/deleteemailachment' id="cancel_<?php echo $fileId; ?>" deleteid="<?php echo $attachmentfiles->attachment_id; ?>"  fileid="<?php echo $fileId; ?>">
											<img src="<?php echo base_url() ?>themes/assets/images/value_clear.png">
										</div>
									</div>
							  </div> 
						<?php } 
						}?>
				</div>
			</div>
			<!---------- Email Attachment : End ----------------->	
           <?php 
                 // get all categories which belongs to this events 
				$getEventCategories = getEventCategories();
				if(!empty($getEventCategories)){
			?>	
           
              <div class="row-fluid">
              	<label class="pull-left" for="ce_name"><?php echo lang('custom_email_send_to'); ?>
                	<span class="info_btn">
						<span class="field_info xsmall"><?php echo lang('tooltip_email_send_to'); ?></span>
					</span>
                </label>
              
                <div class="send_checkbox_wrapper">
                    <div class="checkbox_wrapper">
                        <input type="checkbox" checked="" name="ce_checked_all" id="ce_checked_all">
                        <label for="ce_checked_all">All </label>
                    </div>
                    <?php 
                   
                    foreach($getEventCategories as $key=>$value){ ?>
                    	<div class="checkbox_wrapper">
							<?php $checked=(in_array($value->category_id,$emailCategories)) ? 'checked="checked"' : "" ?>
							<input type="checkbox"  name="ce_category[]" <?php echo $checked; ?>  id="ce_cat<?php echo $key; ?>" value="<?php echo $value->category_id; ?>" >
							<label for="ce_cat<?php echo $key; ?>"><?php echo $value->category; ?></label>
						</div>
                    <?php } //end loop 
				    ?>
                </div>                
				
              </div>
			<?php } // end if ?>
			<?php 
				echo form_hidden('eventId', $eventId);
				echo form_hidden('formActionName', 'formEventEmail');
			?>	
				
              <div class="btn_wrapper">
                  <input type="submit" name="loginsubmit" value="Save" id="event_email_btn" class=" submitbtn pull-right medium">
                  <input type="submit" class="popup_cancel submitbtn pull-right medium"  value="Cancel" name="logincancel" data-dismiss="modal">
              </div>

            </div>
          <?php echo form_close(); ?>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Modal -->

<script >
	$("#formEventEmail").parsley();
</script>

<script>
function email_attachment(){   
	  
		// Custom example logic
		//alert("");
		var postData = {event_id : '<?php echo $eventId; ?>'}
		var uploadUrl = baseUrl+'event/addupdateeventemailattach';

		var otherObj = {
				pickfiles:"attch_pickfiles_email", 
				dropArea:"attch_dropArea_email", 
				FileContainer:"attch_FileContainer_email", 
				fileTypes:"pdf,doc,jpg,jpeg,gif,png,txt", 
				deleteUrl:baseUrl+'event/deleteemailachment', 
				maxfilesize:"10mb", 
				multiselection:false, 
				isFileList:true, 
				uniqueNames:true, 
				autoStart:false, 
				isDelete:true, 
				maxfileupload:3, 
				mainDivFileList:"attach_file_list_mainEmail",
				listAddFile:"attch_file_listEmail",
				adddedfile:"attchadddedfileEmail",
		}
	upload_file(uploadUrl,postData,otherObj,'custom_email');
}

email_attachment();

//Delete common delete function
deleteUploadFiles('custom_email');

</script>	
