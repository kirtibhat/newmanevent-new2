<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 |--------------------------------------------------------------------------|
 | This Field define for temparay open popup by add section id 				|
 | and edit record id														|	
 |--------------------------------------------------------------------------|
*/
 
$tempHidden = array(
	'type' => 'hidden',
	'name' => 'tempHidden',
	'id' => 'tempHidden',
	'value' => '',
);

echo form_input($tempHidden);
?>

<?php $this->load->view('event_menus'); ?>

<div class="row-fluid mt20">
	<div class="span7">
		<div class="formparag pl0 pr0">
			 <?php echo lang('event_exhibitor_msg'); ?>
		</div>
		<div class="spacer15">
		</div>
		<div class="regidetrailHeading">
			<a href="javascript:void(0)" class="add_edit_exhibitor" exhibitorid="0">
			<div class="addbtn_form bg_5c2946">
			</div>
			<div class="addtype clr_79496A">
				<?php echo lang('event_exhibitor_add_exhibitor'); ?>
			</div>
			</a>
			<div class="clearfix">
			</div>
		</div>
		<div class="spacer15">
		</div>
		
		<?php 
			if(!empty($exhibitordata)){
				
				//prepare array data for current and next form hide/show  
				foreach($exhibitordata as $exhibitor){
					$recordId[] =  $exhibitor->id;
				}
				
				//show exhibitors forms	
				$rowCount = 0;	
				foreach($exhibitordata as $exhibitor){
					$nextId = $rowCount+1;
					$sendData['formdata'] = $exhibitor; 
					$sendData['nextRecordId']	= (isset($recordId[$nextId]))?$recordId[$nextId]:'0';
					// daynamic load all send data forms
					$this->load->view('form_exhibitors_steps',$sendData);
					$rowCount++;
				}
			}
		?>
		
		<?php 
			//next and previous button
			$buttonData['viewbutton'] = array('back','next','preview');
			$buttonData['linkurl'] 	= array('back'=>base_url('event/setupsponsors'),'next'=>base_url('event/setupsideevents'),'preview'=>'');
			$this->load->view('common_back_next_buttons',$buttonData);
		?>
	
	</div>
	
	<!----Right side advertisement view load start------>
		<?php $this->load->view('right_side_advert'); ?>
	<!----Right side advertisement view load end------>
	
</div>	

<script type="text/javascript" language="javascript">

	/*-----Open add and edit exhibitor popup-------*/
	$(document).on('click','.add_edit_exhibitor',function(){
		
		if($(this).attr('exhibitorid')!==undefined){
			var exhibitorId = parseInt($(this).attr('exhibitorid'));
			//set value in assing
			$("#tempHidden").val(exhibitorId);
		}else{
			//get value form temp hidden and set
			var exhibitorId = parseInt($("#tempHidden").val());
		}
		
		var eventId = '<?php echo $eventId ?>';
		var sendData = {"exhibitorId":exhibitorId, "eventId":eventId};
		
		//call ajax popup exhibitor add and edit 
		ajaxpopupopen('add_edit_exhibitor_popup','event/addeditexhibitorsview',sendData,'add_edit_exhibitor');
		
	});
	
	//save exhibitor add and edit data
	ajaxdatasave('formAddExhibitor','event/addeditexhibitorsave',false,true,true);
	
	
	/*---This function is used to delete exhibitor -----*/
	customconfirm('delete_exhibitor','event/deleteexhibitor');
	
	/*-----This funtion is used to disbaled checkbox------------*/
	
	$(".setupspons_dis").click(function(e) {
			e.preventDefault();
	});


	/*-----Open add and edit exhibitor additional items popup-------*/
	$(document).on('click','.add_edit_exhibitor_item',function(){
	
		//set value in temparray hidden field for if again open
		if($(this).attr('itemid') !== undefined && $(this).attr('exhibitorid') !== undefined ){
		
			var itemId = $(this).attr('itemid');
			var exhibitorId = $(this).attr('exhibitorid');
			
			//concanate two value in assing
			var tempObj = itemId+','+exhibitorId;
			$("#tempHidden").val(tempObj);
		}else{
			var getTempData = $("#tempHidden").val();
			var getData = getTempData.split(',');
			
			//get value from hidden field and set value
			itemId = getData[0];
			exhibitorId = getData[1];
		}
		
		var sendData = {"exhibitorId":exhibitorId, "itemId":itemId};
		
		//call ajax popup exhibitor additional items add and edit 
		ajaxpopupopen('exhibitor_purchase_item_popup','event/addeditexhibitoritemview',sendData,'add_edit_exhibitor_item');
		
	});
	
	//save sponsor exhibitor additional items add and edit data
	ajaxdatasave('formAddAdditionalItem','event/addeditexhibitoritemsave',false,true,true);
	
	/*---This function is used to delete delet eexhibitor item-----*/
	customconfirm('delete_exhibitor_item','event/deleteexhibitoritem');
	
</script>
