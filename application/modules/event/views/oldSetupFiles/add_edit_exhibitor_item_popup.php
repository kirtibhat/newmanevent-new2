<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//--------add type fo registration fields---------//	

$itemNameValue = (!empty($additionalitemsdata->item))?$additionalitemsdata->item:''; 
$headerAction = ($itemIdValue > 0)?lang('event_exhibitor_edit_item'):lang('event_exhibitor_add_item'); 


$formAddAdditionalItem = array(
		'name'	 => 'formAddAdditionalItem',
		'id'	 => 'formAddAdditionalItem',
		'method' => 'post',
		'class'  => 'form-horizontal mt10',
	);
	
$itemName = array(
		'name'	=> 'itemName',
		'value'	=> $itemNameValue,
		'id'	=> 'itemName',
		'type'	=> 'text',
		'required'	=> '',
	);
	
$exhibitorId = array(
		'name'	=> 'exhibitorId',
		'value'	=> $exhibitorIdValue,
		'id'	=> 'exhibitorId',
		'type'	=> 'hidden',
	);
$itemId = array(
		'name'	=> 'itemId',
		'value'	=> $itemIdValue,
		'id'	=> 'itemId',
		'type'	=> 'hidden',
	);

?>

<!--------Add braakout div start-------->
	<div class="modal fade alertmodelbg dn" id="exhibitor_purchase_item_popup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		
		<div class="modal-content">
			
			<div class="modal-header bg_79496a">
				<h4 class="modal-title"><?php echo $headerAction; ?></h4>
			</div>
			<?php  echo form_open(base_url('event/addeditexhibitoritemsave'),$formAddAdditionalItem); ?>
				<div class="modal-body ">
				<h4><?php echo lang('event_exhibitor_add_item_msg') ?></h4>
				
					<div class="control-group mb10 min_height_75">
						<label for="inputEmail" class="control-label"><?php echo lang('event_exhibitor_add_item_field') ?>
							<font class="mandatory_cls">*</font>
						</label>
						<div class="controls">
							<?php echo form_input($itemName); ?>
						</div>
					</div>
			</div>
			<div class="modal-footer">
				<?php 
					echo form_input($exhibitorId);
					echo form_input($itemId);
					$extraCancel 	= 'class="btn btn-default custombtn" data-dismiss="modal" ';
					$extraSave 	= 'class="btn btn-primary  bg_5c2946 custombtn bg_f26531" ';
					echo form_button('cancel',lang('comm_cancle'),$extraCancel);
					echo form_submit('save',lang('comm_save'),$extraSave);
				?>
			</div>
		</div>
		<?php echo form_close(); ?>
	</div>
</div>
<!--------Add braakout div end-------->
