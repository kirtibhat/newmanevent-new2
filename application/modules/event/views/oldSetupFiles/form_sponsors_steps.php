<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


//set sponsor form data
$sponsorId 							= $formdata->id;
$sponsorName 						= (!empty($formdata->sponsor_name))?$formdata->sponsor_name:'';
$additionalDetailsValue 			= (!empty($formdata->sponsor_name))?$formdata->additional_detail:'';
$limitPackageValue 					= (!empty($formdata->limit_package))?$formdata->limit_package:'';
$totalPriceValue 					= (!empty($formdata->total_price))?$formdata->total_price:'';
$gstValue 							= (!empty($formdata->gst))?$formdata->gst:'';
$additionalBoothPriceValue			= (!empty($formdata->additional_booth_price))?$formdata->additional_booth_price:'';
$additionalBoothGSTIncludedValue	= (!empty($formdata->additional_booth_gst))?$formdata->additional_booth_gst:'';
$complementaryRegistrationValue 	= (!empty($formdata->complementary_registration))?true:false;
$complementaryRegistrationNumValue 	= (!empty($formdata->number_complimentary_registrations))?$formdata->number_complimentary_registrations:'0';
$canAttendBreakoutValue 			= (!empty($formdata->can_attend_breakout) && $formdata->can_attend_breakout == "1")?$formdata->can_attend_breakout:"0";
$isAdditionalGuestValue	 			= (!empty($formdata->is_additional_guests) && $formdata->is_additional_guests == "1")?true:false;
$additionalLimitPerPerson			= (!empty($formdata->limit_per_person))?$formdata->limit_per_person:'0';
$additionalTotalPriceVal			= (!empty($formdata->additi_guest_total_price))?$formdata->additi_guest_total_price:'';
$additionalGSTIncludedVal			= (!empty($formdata->additi_guest_gst))?$formdata->additi_guest_gst:'';

//check additional guest
$isComplementaryRegistration = "dn";
if($complementaryRegistrationValue){
	$isComplementaryRegistration = "";
}

//check additional guest
$additionalGuestsYesVal = false;
$additionalGuestsNoVal = true;
$isAdditionalGuestShow = "dn";

if($isAdditionalGuestValue){
	$additionalGuestsYesVal = true;
	$additionalGuestsNoVal = false;
	$isAdditionalGuestShow = "";
}


//check breakout 
$attendBreakoutsYesValue = false;
$attendBreakoutsNoValue = false;
if($canAttendBreakoutValue=="1"){
	$attendBreakoutsYesValue = true;
}else{
	$attendBreakoutsNoValue = true;
}



//form create variable
$formSetupSponsors = array(
		'name'	 => 'formSetupSponsors'.$sponsorId,
		'id'	 => 'formSetupSponsors'.$sponsorId,
		'method' => 'post',
		'class'  => 'form-horizontal',
	);
	
$additionalDetails = array(
		'name'	=> 'additionalDetails'.$sponsorId,
		'value'	=>  $additionalDetailsValue,
		'id'	=> 'additionalDetails'.$sponsorId,
		'required'	=> '',
		'class'	=> 'bdr_n',
		'rows' => '2',
	);
	
$limitOfPackage = array(
		'name'	=> 'limitOfPackage'.$sponsorId,
		'value'	=>  $limitPackageValue,
		'id'	=> 'limitOfPackage'.$sponsorId,
		'required'	=> '',
		'type' => 'text',
		'class' => 'width50per',
	);

$totalPrice = array(
		'name'	=> 'totalPrice'.$sponsorId,
		'value'	=>  $totalPriceValue,
		'id'	=> 'totalPrice'.$sponsorId,
		'required'	=> '',
		'type' => 'text',
		'class' => 'width50per',
	);

$GSTIncluded = array(
		'name'	=> 'GSTIncluded'.$sponsorId,
		'value'	=>  $gstValue,
		'id'	  => 'GSTIncluded'.$sponsorId,
		'required'	=> '',
		'type'    => 'text',
		'class' => 'width50per',
	);

$additionalBoothPrice = array(
		'name'	=> 'additionalBoothPrice'.$sponsorId,
		'value'	=>  $additionalBoothPriceValue,
		'id'	=> 'additionalBoothPrice'.$sponsorId,
		'required'	=> '',
		'type' => 'text',
		'class' => 'width50per',
	);
		
$additionalBoothGSTIncluded = array(
		'name'	=> 'additionalBoothGSTIncluded'.$sponsorId,
		'value'	=>  $additionalBoothGSTIncludedValue,
		'id'	  => 'additionalBoothGSTIncluded'.$sponsorId,
		'required'	=> '',
		'type'    => 'text',
		'class' => 'width50per',
	);
	
$complimentaryRegistration = array(
			'name'	=> 'complimentaryRegistration'.$sponsorId,
			'value'	=>  '1',
			'checked'=>  $complementaryRegistrationValue,
			'id'	=> 'complimentaryRegistration'.$sponsorId,
			'type' => 'checkbox',
			'sponsorid'	=> $sponsorId,
			'class' => 'pull-left ml10 mr15 complimentaryRegistration checkbox',
		);	
			
			
$complimentaryRegistrationsNum = array(
		'name'	=> 'complimentaryRegistrationsNum'.$sponsorId,
		'value'	=> $complementaryRegistrationNumValue,
		'id'	=> 'complimentaryRegistrationsNum'.$sponsorId,
		'type'	=> 'text',
		'class' => 'width_50px clr_425968 spinner_selectbox',
		'readonly' => '',
		'min' => '0',
		);				
	

$additionalGuestsYes  = array(
		'name'	=> 'additionalGuests'.$sponsorId,
		'value'	=> '1',
		'id'	=> 'additionalGuestsYes'.$sponsorId,
		'type'	=> 'radio',
		'class'	=> 'additionalGuests',
		'additionalguestsid'	=> $sponsorId,
		'checked'	=> $additionalGuestsYesVal,
	);	

$additionalGuestsNo = array(
		'name'	=> 'additionalGuests'.$sponsorId,
		'value'	=> '0',
		'id'	=> 'additionalGuestsNo'.$sponsorId,
		'type'	=> 'radio',
		'class'	=> 'additionalGuests',
		'additionalguestsid'	=> $sponsorId,
		'checked'	=> $additionalGuestsNoVal,
	);	
	
$additionalLimitPerPerson = array(
		'name'	=> 'additionalLimitPerPerson'.$sponsorId,
		'value'	=> $additionalLimitPerPerson,
		'id'	=> 'additionalLimitPerPerson'.$sponsorId,
		'type'	=> 'text',
		'class' => 'width_50px clr_425968 spinner_selectbox',
		'readonly' => '',
		'min' => '0',
		);		

$additionalTotalPrice = array(
		'name'	=> 'additionalTotalPrice'.$sponsorId,
		'value'	=>  $additionalTotalPriceVal,
		'id'	=> 'additionalTotalPrice'.$sponsorId,
		'type' => 'text',
		'class' => 'width50per additionalGuestFieldDis'.$sponsorId,
	);

$additionalGSTIncluded = array(
		'name'	=> 'additionalGSTIncluded'.$sponsorId,
		'value'	=>  $additionalGSTIncludedVal,
		'id'	  => 'additionalGSTIncluded'.$sponsorId,
		'type'    => 'text',
		'class' => 'width50per additionalGuestFieldDis'.$sponsorId,
	);



$attendBreakoutsYes = array(
		'name'	=> 'attendBreakouts'.$sponsorId,
		'value'	=> '1',
		'id'	=> 'attendBreakoutsYes'.$sponsorId,
		'type'	=> 'radio',
		'required'	=> '',
		'class'	=> 'attendBreakouts',
		'checked'	=> $attendBreakoutsYesValue,
		
	);	

$attendBreakoutsNo = array(
		'name'	=> 'attendBreakouts'.$sponsorId,
		'value'	=> '0',
		'id'	=> 'attendBreakoutsNo'.$sponsorId,
		'type'	=> 'radio',
		'required'	=> '',
		'class'	=> 'attendBreakouts',
		'checked'	=> $attendBreakoutsNoValue,
	);
		
	
?>
	<div class="commonform_bg pb0">
		<div class="headingbg bg_a54399 position_R">
			<div class="typeofregistV pull-left showhideaction ptr" id="<?php echo $sponsorId; ?>">
				<?php echo $sponsorName; ?>
			</div>
			<div class="contPadiv">
				<div class="icontitlecontainer right_del delete_sponsor" deleteid="<?php echo $sponsorId ?>">
					<div class="delet_icon">
					</div>
					<div class="icontext">
						<?php echo lang('comm_delete'); ?>
					</div>
				</div>
				<div class="icontitlecontainer right_edit add_edit_sponsors" sponsorsid="<?php echo $sponsorId ?>">
					<div class="edittitle_icon bg_731472">
					</div>
					<div class="icontext">
						<?php echo lang('comm_edit_title'); ?>
					</div>
				</div>
			</div>
			<div class="clearfix">
			</div>
		</div>
		<?php  echo form_open($this->uri->uri_string(),$formSetupSponsors); ?>
		<div class="form-horizontal dn form_open_chk" id="showhideformdiv<?php echo $sponsorId; ?>">
			<div class="control-group mb10">
				<label class="control-label" for="inputEmail"><?php echo lang('event_sponsors_additional_details'); ?>
					<font class="mandatory_cls">*</font>
					<div class="infoiconbg bg_731472 sponsors_tooltip">
						<span class="show_tool_tip" title='<?php echo lang('tooltip_sponsors_additional_details'); ?>'> </span>
					</div>
				</label>
				<div class="controls">
					<?php echo form_textarea($additionalDetails); ?>
					<?php echo form_error('additionalDetails'.$sponsorId); ?>
				</div>
			</div>
			<div class="control-group mb10">
				<label class="control-label" for="inputEmail"><?php echo lang('event_sponsors_limit_package'); ?>
				<font class="mandatory_cls">*</font>
					<div class="infoiconbg bg_731472 sponsors_tooltip">
						<span class="show_tool_tip" title='<?php echo lang('tooltip_sponsors_limit_package'); ?>'> </span>
					</div>
				</label>
				<div class="controls">
					<?php echo form_input($limitOfPackage); ?>
					<?php echo form_error('limitOfPackage'.$sponsorId); ?>
				</div>
			</div>
			<div class="control-group mb10">
				<label class="control-label" for="inputEmail"><?php echo lang('event_sponsors_total_price'); ?>
					<font class="mandatory_cls">*</font>
				</label>
				<div class="controls">
					<?php echo form_input($totalPrice); ?>
					<?php echo form_error('totalPrice'.$sponsorId); ?>
				</div>
			</div>
			<div class="control-group mb10">
				<label class="control-label" for="inputEmail"><?php echo lang('event_sponsors_gst_include'); ?>
					<font class="mandatory_cls">*</font>
				</label>
				<div class="controls">
					<?php echo form_input($GSTIncluded); ?>
					<?php echo form_error('GSTIncluded'.$sponsorId); ?>
				</div>
			</div>
			<div class="control-group mb10">
				<label class="control-label" for="inputEmail"><?php echo lang('event_sponsors_additional_booth'); ?>
					<font class="mandatory_cls">*</font>
				<div class="infoiconbg bg_731472 sponsors_tooltip">
					<span class="show_tool_tip" title='<?php echo lang('tooltip_sponsors_additional_booth'); ?>'> </span>
				</div>
				</label>
				<div class="controls">
					<?php echo form_input($additionalBoothPrice); ?>
					<?php echo form_error('additionalBoothPrice'.$sponsorId); ?>
				</div>
			</div>
			<div class="control-group mb10">
				<label class="control-label" for="inputEmail">	<?php echo lang('event_sponsors_gst_include'); ?>
					<font class="mandatory_cls">*</font>
				<div class="infoiconbg bg_731472 sponsors_tooltip">
					<span class="show_tool_tip" title='<?php echo lang('tooltip_sponsors_gst_include'); ?>'> </span>
				</div>
				</label>
				<div class="controls">
					<?php echo form_input($additionalBoothGSTIncluded); ?>
					<?php echo form_error('additionalBoothGSTIncluded'.$sponsorId); ?>
				</div>
			</div>
			<div class="spacer10">
			</div>
			<div class="formparag mb20">
				<div class="bdr_B_spons">
				</div>
			</div>
			<div class="formparag mb20">
				<div>
					<?php echo lang('event_sponsors_booth_available'); ?>
				</div>
				<div class="clearfix">
				</div>
				
				<?php if(!empty($exhibitorFloorPlan)){
					
					//get exhibitor folor plane listing data
					$where = array('sponsor_id'=>$sponsorId);
					$sponsorBoothsData = getDataFromTabel('sponsor_exhibitor_booths','id,booth_position',$where);
					$sponsorBoothsArray = array();
					$eventBoothsUsedArray = array();
					
					
					//prepare event booth array for finding value
					if(!empty($exhibitorbooths)){
						foreach($exhibitorbooths as $booths){
							$eventBoothsUsedArray[$booths->id] = $booths->booth_position;
						}
					}
					
					//prepare sponsor booth array for finding value
					if(!empty($sponsorBoothsData)){
						foreach($sponsorBoothsData as $booths){
							$sponsorBoothsArray[$booths->id] = $booths->booth_position;
						}
					}
					
					//show floor plan numbers 
					$exhibitorFloorPlanData = $exhibitorFloorPlan[0];
					$numberofFloorPlan 		= $exhibitorFloorPlanData->number_of_booth;
					for($i=1;$i<=$numberofFloorPlan;$i++){
					$boothNumberVal = $i;	
					$boothNumberValSet = $i;
					$boothNumberShow = singlenumbertwo($i);	
					$boolNumberAvailableField = $sponsorId.'_'.$boothNumberVal;
					$isCheckBooth = false;
					$boothPosition = 0;
					$checkBoxBgClass = 'setupspons';
					
					//check value in event booth array
					if(!empty($eventBoothsUsedArray)){
						$boothPosition = array_search($boothNumberVal, $eventBoothsUsedArray);
						if($boothPosition){
							$isCheckBooth = true;
							$checkBoxBgClass='setupspons_dis'; // set class for event booth checked
							$boothNumberValSet = 0;
						}
					}
					
					//check value in sponsor booth array
					if(!empty($sponsorBoothsArray)){
						$boothPosition = array_search($boothNumberVal, $sponsorBoothsArray);
						if($boothPosition){
							$isCheckBooth = true;
							$checkBoxBgClass='setupspons'; // set class for sponsor booth checked
							$boothNumberValSet = $boothNumberVal;
						}
					}
					
					$boolNumberAvailable = array(
						'name'	=> 'boolNumberAvailable'.$boolNumberAvailableField,
						'value'	=>  $boothNumberValSet,
						'id'	=> 'boolNumberAvailable'.$boolNumberAvailableField,
						'type' => 'checkbox',
						'class' => 'pull-left ml10 mr15 checkbox',
						'checked' => $isCheckBooth,
					);
					
					
					$boolNumberAvailableHidden = array(
						'name'	=> 'boolNumberAvailableHidden'.$sponsorId.'['.$boothNumberVal.']',
						'value'	=>  $boothPosition,
						'type' => 'hidden',
					);
					
					echo form_input($boolNumberAvailableHidden);
				?>
				
					<div class="checkradiobg <?php echo $checkBoxBgClass; ?>  setupspons_1 pull-left ml25">
						<div class="checkbox">
							<?php echo form_checkbox($boolNumberAvailable);?>
							<label for="boolNumberAvailable<?php echo $boolNumberAvailableField; ?>" class="width_auto ml0 mt10 eventDcheck"><?php echo $boothNumberShow; ?></label>
						</div>
					</div>
				<?php } } ?>
				<div class="clearfix">
				</div>
			</div>
			<div class="formparag mb20">
				<div class="bdr_B_spons">
				</div>
			</div>
			<div class="formparag">
				<div>
					<?php echo lang('event_sponsors_registrant_include'); ?>
				</div>
				<div class="checkradiobg setupspons pull-left ml25">
					<div class="checkbox">
						<?php echo form_checkbox($complimentaryRegistration);?>
						<label for="complimentaryRegistration<?php echo $sponsorId; ?>" class="width_auto ml0 mt10 eventDcheck"><?php echo lang('event_sponsors_complimentary'); ?></label>
					</div>
				</div>
				<div class="clearfix">
				</div>
			</div>
			<div class="spacer10">
			</div>
			
			<div class="<?php echo  $isComplementaryRegistration; ?>" id="is_complimentary_registrations<?php echo $sponsorId; ?>">
				<div class="control-group mb10">
					<label class="control-label lineH16" for="inputEmail"><?php echo lang('event_sponsors_how_many_compli'); ?></label>
					<div class="controls sponsor_setup">
						<div class="inputnubcontainer ml0 pull-left">
							<?php echo form_input($complimentaryRegistrationsNum); ?>
							<?php echo form_error('complimentaryRegistrationsNum'); ?>
						</div>
					</div>
				</div>
				
					<!----- listing of registrant start section------>
					<?php if(!empty($eventregistrants)) {  ?>
					
					<div class="control-group mb10 allowword">
						<label class="control-label lineH16 fs14" for="inputEmail">	<?php echo lang('event_sponsors_registrant_select'); ?> </label>
						<div>
							<div class="controls registrat_setup">
								<?php
								$countRegis = 1;
								$countRowRegis = 1;
								foreach($eventregistrants as $registrants) {
								
									$registrantId = $registrants->id;
									$regisSponsorId = $sponsorId.'_'.$registrantId;
									$registrantName = $registrants->registrant_name;
										
									//prepare send data	
									$sendViewData['registrantId'] 	  	= $registrantId;	
									$sendViewData['singleDayRegistId']	= 0;	
									$sendViewData['regisSponsorId'] 	= $regisSponsorId;	
									$sendViewData['registrantName']   	= $registrantName;	
									$sendViewData['sponsorId']   		= $sponsorId;	
									$sendViewData['countRowRegis']   	= $countRowRegis;	
									
									//load view for registrant
									$this->load->view('sponsors_registrant_list_view',$sendViewData);		
								
									//This section for single day registrant listing start
									$where = array('registrant_id'=>$registrantId);
									$singleDayRegistList = getDataFromTabel('event_registrant_daywise','id,day',$where);
									
									if(!empty($singleDayRegistList)){
										foreach($singleDayRegistList as $singleDayRegistArray){ 
											$countRowRegis++; 
											$singleDayRegistId = $singleDayRegistArray->id; // single day registrant id
											$singleDayRegistDay= $singleDayRegistArray->day;
											$singleDayRegistDayName= $registrants->registrant_name.' '.$singleDayRegistDay; // single day registrant name
											$singleDayRegistSponsorId = $regisSponsorId.'_'.$singleDayRegistId; // single day registrant row id
											
											//prepare send data	
											$sendViewData['registrantId'] 	    = $registrantId;	
											$sendViewData['singleDayRegistId'] 	= $singleDayRegistId;	
											$sendViewData['regisSponsorId']    = $singleDayRegistSponsorId;	
											$sendViewData['registrantName']     = $singleDayRegistDayName;	
											$sendViewData['sponsorId']  	    = $sponsorId;	
											$sendViewData['countRowRegis']      = $countRowRegis;	
											
											//load view for single day wise registrant
											$this->load->view('sponsors_registrant_list_view',$sendViewData);		
										}	
									}
									
									$countRowRegis++; 
									$countRegis++; 
									
									} 
										
								?>
						<div class="clearfix"></div>
							</div>
						</div>
					</div>
				<?php } ?>
				<!----- listing of registrant end section------>	
				
			</div>
			
			
			<div class="formparag mb20">
				<div class="bdr_B_spons">
				</div>
			</div>
			<div class="control-group mb10">
				<label class="control-label lineH16" for="inputEmail"><?php echo lang('event_sponsors_additional_guest'); ?>
				<div class="infoiconbg bg_731472 sponsors_tooltip">
					<span class="show_tool_tip" title='<?php echo lang('tooltip_sponsors_additional_guest'); ?>'> </span>
				</div>
				</label>
				<div class="controls">
					<div class="tworadiocontainer">
						<div class="checkradiobg">
							<div class="radio esponsorradio">
								<?php echo form_radio($additionalGuestsYes); ?>
								<label for="additionalGuestsYes<?php echo $sponsorId; ?>"><?php echo lang('comm_yes'); ?> &nbsp; &nbsp;</label>
								<?php echo form_radio($additionalGuestsNo); ?>
								<label for="additionalGuestsNo<?php echo $sponsorId; ?>"><?php echo lang('comm_no'); ?> &nbsp; &nbsp;</label>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="<?php echo $isAdditionalGuestShow; ?>" id="additional_guests_div<?php echo $sponsorId; ?>">
				<div class="control-group mb10">
					<label class="control-label lineH16" for="inputEmail">	<?php echo lang('event_sponsors_limit_person'); ?></label>
					<div class="controls sponsor_setup">
						<div class="inputnubcontainer ml0 pull-left">
							<?php echo form_input($additionalLimitPerPerson); ?>
						</div>
					</div>
				</div>
				<div class="control-group mb10">
					<label for="inputEmail" class="control-label"><?php echo lang('event_sponsors_total_price'); ?>
						<font class="mandatory_cls">*</font>
					</label>
					<div class="controls">
						<?php echo form_input($additionalTotalPrice); ?>
					</div>
				</div>
				<div class="control-group mb10">
					<label for="inputEmail" class="control-label"><?php echo lang('event_sponsors_gst_include'); ?>
						<font class="mandatory_cls">*</font>
					</label>
					<div class="controls">
						<?php echo form_input($additionalGSTIncluded); ?>
					</div>
				</div>
			</div>
			<div class="spacer10">
			</div>
			<div class="formparag mb20">
				<div class="bdr_B_spons">
				</div>
			</div>
			<div class="formparag mb20">
				 <?php echo lang('event_sponsors_compli_attendance'); ?>
			</div>
			<?php  if(!empty($eventsideevent)) { 
			foreach($eventsideevent as $eventside){ 
				$sideEventName = $eventside->side_event_name;
				
				$sideEventId = $eventside->id;
				$rowKeyId = $sponsorId.'_'.$sideEventId;
				
				//This section for single day registrant listing start
				$where = array('side_event_id'=>$sideEventId,'sponsor_id'=>$sponsorId);
				$sponsorSideeventData = getDataFromTabel('sponsor_side_event','id,side_event_limit',$where);
				$sponsorSideeventId	  		  = '0';
				$complimentarySideeventVal	  = '0';
				if(!empty($sponsorSideeventData)){
					$sponsorSideeventData = $sponsorSideeventData[0];
					$sponsorSideeventId = $sponsorSideeventData->id;
					$complimentarySideeventVal = $sponsorSideeventData->side_event_limit;
				}
				
				$complimentarySideevent = array(
					'name'	=> 'complimentarySideevent'.$rowKeyId,
					'value'	=> $complimentarySideeventVal,
					'id'	=> 'complimentarySideevent',
					'type'	=> 'text',
					'class' => 'width_50px clr_425968 spinner_selectbox',
					'readonly' => '',
					'min' => '0',
					);	
					
				$complimentarySideeventHidden = array(
					'name'	=> 'complimentarySideeventHidden'.$sponsorId.'['.$sideEventId.']',
					'value'	=> $sponsorSideeventId,
					'type'	=> 'hidden',
					);	
					
			echo form_input($complimentarySideeventHidden);		
						
			?>
			
			<div class="control-group mb10">
				<label class="control-label" for="inputEmail"><?php echo $sideEventName; ?></label>
				<div class="controls sponsor_setup">
					<div class="inputnubcontainer ml0 pull-left">
						<?php echo form_input($complimentarySideevent); ?>
					</div>
				</div>
			</div>
			<?php  } }?>
		
			<!----package listing div start ----->
				<div class="formparag mb20">
					<div class="bdr_B_spons">
					</div>
				</div>
				<div class="formparag mb20">
						<?php echo lang('event_sponsors_additional_items'); ?>
					<div class="formparag mt10">
						<a href="javascript:void(0)" class="add_edit_purchase_item" itemid="0" sponsorsid="<?php echo $sponsorId; ?>">
						<div class="addbtn_form bg_731472">
						</div>
						<div class="addtype clr_A54399">
							<?php echo lang('event_sponsors_purchase_item'); ?>
						</div>
						</a>
						<div class="clearfix">
						</div>
					</div>
						<?php 
							//get additional purchase item listing
							$whereAdditi = array('sponsor_id'=>$sponsorId);
							$sponsorAdditionalItems = getDataFromTabel('sponsor_benefits','id,item_title,item_status',$whereAdditi);
						
							//show additional item listing	
							if(!empty($sponsorAdditionalItems)){
							foreach($sponsorAdditionalItems as $additionalItems){	
								
							$itemTitle   = $additionalItems->item_title;	
							$itemId		 = $additionalItems->id;	
							$itemStatus  = $additionalItems->item_status;	
							
							$additionalItemFieldValue = ($itemStatus==1)?true:false;
							
							$additionalItemsRow = $sponsorId.'_'.$itemId;	
							
							$additionalItemField = array(
								'name'	=> 'additionalItemField'.$additionalItemsRow,
								'value'	=>  '1',
								'checked'=>  $additionalItemFieldValue,
								'id'	=> 'additionalItemField'.$additionalItemsRow,
								'type' => 'checkbox',
								'class' => 'pull-left ml10 mr15 checkbox',
							);
							
							$additionalItemHidden = array(
								'name'	=> 'additionalItemHidden'.$sponsorId.'[]',
								'value'	=>  $itemId,
								'type' => 'hidden',
							);	
							
							echo form_input($additionalItemHidden);	

						?>
					
							<div class="checkradiobg setupspons ml25 position_R setupspon_height">
						
								<div class="checkbox min_width_440">
									<?php echo form_checkbox($additionalItemField); ?>
									<label for="additionalItemField<?php echo $additionalItemsRow; ?>" class="width_auto ml0 mt10 eventDcheck"><?php echo $itemTitle; ?></label>
								</div>
								
								<div class="greentooltip">
									<span class="add-on_1 add-on_green_1 show_tool_tip add_edit_purchase_item" id="example1" title='<?php echo lang('tooltip_edit'); ?>' itemid="<?php echo $itemId; ?>" sponsorsid="<?php echo $sponsorId; ?>">
									<img src="<?php echo IMAGE; ?>smalledittooltio.png" alt="wdit">
									</span>
								</div>
								<div class="greentooltip_del">
									<span class="add-on_1 add-on_red_1 show_tool_tip delete_purchase_item" id="example5" deleteid="<?php echo $itemId; ?>" title='<?php echo lang('tooltip_delete'); ?>'>
									<img src="<?php echo IMAGE; ?>smalledittooltiodel.png" alt="wdit"></span>
								</div>
							</div>
							
						<?php }  } ?>	
				
					<div class="clearfix">
					</div>
				</div>
			
			<!----package listing div start ----->
			
			<div class="spacer10"></div>
			<div class="formparag mb20">
				<div class="bdr_B_spons">
				</div>
			</div>
			<div class="control-group mb10">
				<label class="control-label lineH16" for="inputEmail"><?php echo lang('event_sponsors_attend_breakouts'); ?></label>
				<div class="controls">
					<div class="tworadiocontainer">
						<div class="checkradiobg">
							<div class="radio esponsorradio">
								<?php echo form_radio($attendBreakoutsYes); ?>
								<label for="attendBreakoutsYes<?php echo $sponsorId; ?>">Yes &nbsp; &nbsp;</label>
								<?php echo form_radio($attendBreakoutsNo); ?>
								<label for="attendBreakoutsNo<?php echo $sponsorId; ?>">No &nbsp; &nbsp;</label>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="spacer10"></div>
			<div class="row-fluid">
				<?php 
				echo form_hidden('eventId', $eventId);
				echo form_hidden('sponsorId', $sponsorId);
				
				//button show of save and reset
				$formButton['saveButtonId'] = 'id="'.$sponsorId.'"'; // click button id
				$formButton['showbutton']   = array('save','reset');
				$formButton['labletext'] 	= array('save'=>lang('comm_save_changes'),'reset'=>lang('comm_reset'));
				$formButton['buttonclass']  = array('save'=>'submitbtn_cus mr_16 submit_regis_type','reset'=>'submitbtn_cus reset_form');
				$this->load->view('common_save_reset_button',$formButton);
			?>
			</div>
			<div class="spacer36">
			</div>
		</div>
		<?php echo form_close(); ?>
	</div>
	
<script type="text/javascript">
	//side event dtails save type details save
	ajaxdatasave('formSetupSponsors<?php echo $sponsorId; ?>','<?php echo $this->uri->uri_string(); ?>',true,true,false,false,false,'#showhideformdiv<?php echo $sponsorId; ?>','#showhideformdiv<?php echo $nextRecordId; ?>');
</script>
