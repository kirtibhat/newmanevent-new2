<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
//set field value in variable
if(!empty($exhibitorFloorPlan)){
	$exhibitorFloorPlan = $exhibitorFloorPlan[0];
}

$exhibitorFloorPlanid = (!empty($exhibitorFloorPlan->id))?$exhibitorFloorPlan->id:'0';
$exhibitorFloorPlanName = (!empty($exhibitorFloorPlan->floor_plan))?$exhibitorFloorPlan->floor_plan:'';
$numberOfBoothsVal = (!empty($exhibitorFloorPlan->number_of_booth))?$exhibitorFloorPlan->number_of_booth:'0';


//add exhibitor floor plan form 
$formExhibitorFloorPlan = array(
		'name'	 => 'formExhibitorFloorPlan',
		'id'	 => 'formExhibitorFloorPlan',
		'method' => 'post',
		'class'  => 'form-horizontal',
	);
	
$exhibitorFloorPlanIdField = array(
		'name'		  => 'exhibitorFloorPlanIdField',
		'value'		  => $exhibitorFloorPlanid,
		'id'		  => 'exhibitorFloorPlanIdField',
		'type'		  => 'hidden',
	);	

$numberOfBooths = array(
		'name'	=> 'numberOfBooths',
		'value'	=> $numberOfBoothsVal,
		'id'	=> 'numberOfBooths',
		'type'	=> 'text',
		'class' => 'width_50px clr_425968 spinner_selectbox breakoutNumberOfBlocks',
		'readonly' => '',
		'min' => '0',
		);	
		
?>
<div class="commonform_bg pb0">
	<div class="headingbg bg_a54399">
		<div class="typeofregistH pull-left showhideaction ptr" id="flowPlane">
			<?php echo lang('event_sponsors_floor_plan'); ?>
		</div>
		<div class="clearfix">
		</div>
	</div>
	<?php echo form_open($this->uri->uri_string(),$formExhibitorFloorPlan); ?>	
		<div class="form-horizontal form_open_chk " id="showhideformdivflowPlane">
		<div class="formparag mb20">
			<?php echo lang('event_sponsors_floor_plan_msg1'); ?>
			<a href="#" class="floorplane"> <?php echo lang('event_sponsors_floor_plan_msg2'); ?> </a>
		</div>
		<div class="control-group mb10"  id="attch_FileContainer">
			<label class="control-label" for="inputEmail">	<?php echo lang('event_sponsors_floor_plan_att'); ?>
			<div class="infoiconbg bg_731472 sponsors_tooltip">
				<span class="show_tool_tip" title='<?php echo lang('tooltip_sponsors_floor_plan_att'); ?>'> </span>
			</div>
			</label>
			<div class="controls" id="attch_pickfiles">
				<div id="attch_dropArea">
					<div class="dashbdr clr_731472 dashbdr_731472">
						<?php echo lang('comm_file_upload_drop'); ?>
					<div class="sponsoruparrow">
					</div>
				</div>
				</div>
			</div>
		</div>
		<!---file list div-->
		<div class="control-group mb10  <?php echo (empty($exhibitorFloorPlanName))?'dn':' '; ?>" id="attach_file_list_main" >
				<div class="controls " id="attch_file_list" >
						<?php if(!empty($exhibitorFloorPlanName)){ 
						$fileId= remove_extension($exhibitorFloorPlanName);	
							?>
							 <div class="attchadddedfile" id="<?php echo $fileId; ?>"> 
									<div class="imagename_cont pr mr_5px"><?php echo $exhibitorFloorPlanName; ?>
										<div class="pa ptr top_6 right_10 delete_file" deleteurl='event/deleteexhibitorfloorplan'  id="cancel_<?php echo $fileId; ?>" deleteid="<?php echo $exhibitorFloorPlanid; ?>"  fileid="<?php echo $fileId; ?>">
											<img src="<?php echo IMAGE; ?>close_icon.png">
										</div>
									</div>
								</div> 
						<?php } ?>	
				</div>
		</div>
			
		<!---
		<div class="control-group mb10">
			<label class="control-label cusinp_font" for="inputEmail">&nbsp;</label>
			<div class="controls">
				<div class="imagename_cont">
					Logo.jpg <a class="filename_closebox"></a>
				</div>
			</div>
		</div> --->
		
		
		<div class="control-group mb10">
			<label class="control-label" for="inputEmail"><?php echo lang('event_sponsors_number_of_booth'); ?>
			<font class="mandatory_cls">*</font>
			<div class="sponsoruparrow">
			</div>
			</label>
			<div class="controls sponsor_setup">
				<div class="inputnubcontainer ml0 pull-left">
				<!---	<input id="spinner" name="value" max="100" min="1 Am" value="10" class="clr_425968">--->
				<?php 
					echo form_input($numberOfBooths);
					echo form_error('numberOfBooths');
				?>
				</div>
			</div>
		</div>
		<div class="spacer10">
		</div>
		<div class="row-fluid">
			<?php 
				echo form_hidden('eventId', $eventId);
				echo form_input($exhibitorFloorPlanIdField);
				$extraSave 	= 'class="submitbtn_cus bg_731472 submitbtn_floor_plane" ';
				echo form_submit('save',lang('comm_save_changes'),$extraSave);
			?>
		</div>
		<div class="spacer36">
		</div>
	</div>
	<?php echo form_close();?>	
</div>
	
	
