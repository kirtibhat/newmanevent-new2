<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//--------add type fo registration fields---------//	

$itemNameValue = (!empty($additionalitemsdata->item_title))?$additionalitemsdata->item_title:''; 
$headerAction = ($itemIdValue > 0)?lang('event_exhibitor_item_edit'):lang('event_exhibitor_item_add'); 


$formAddAdditionalItemPurchase = array(
		'name'	 => 'formAddAdditionalItemPurchase',
		'id'	 => 'formAddAdditionalItemPurchase',
		'method' => 'post',
		'class'  => 'form-horizontal mt10',
	);
	
$itemName = array(
		'name'	=> 'itemName',
		'value'	=> $itemNameValue,
		'id'	=> 'itemName',
		'type'	=> 'text',
		'required'	=> '',
	);
	
$sponsorsId = array(
		'name'	=> 'sponsorsId',
		'value'	=> $sponsorsIdValue,
		'id'	=> 'sponsorsId',
		'type'	=> 'hidden',
	);
$itemId = array(
		'name'	=> 'itemId',
		'value'	=> $itemIdValue,
		'id'	=> 'itemId',
		'type'	=> 'hidden',
	);

?>

<!--------Add braakout div start-------->
	<div class="modal fade alertmodelbg dn" id="sponsors_purchase_item_popup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		
		<div class="modal-content">
			
			<div class="modal-header bg_a54399">
				<h4 class="modal-title"><?php echo $headerAction; ?></h4>
			</div>
			<?php  echo form_open(base_url('event/addeditpurchaseitemsave'),$formAddAdditionalItemPurchase); ?>
				<div class="modal-body ">
				<h4><?php echo lang('event_exhibitor_item_msg'); ?></h4>
				
					<div class="control-group mb10 min_height_75">
						<label for="inputEmail" class="control-label"><?php echo lang('event_exhibitor_item_field'); ?></label>
						<div class="controls">
							<?php echo form_input($itemName); ?>
						</div>
					</div>
			</div>
			<div class="modal-footer">
				<?php 
					echo form_input($sponsorsId);
					echo form_input($itemId);
					$extraCancel 	= 'class="btn btn-default custombtn" data-dismiss="modal" ';
					$extraSave 	= 'class="btn btn-primary  bg_a54399 custombtn bg_f26531" ';
					echo form_button('cancel',lang('comm_cancle'),$extraCancel);
					echo form_submit('save',lang('comm_save'),$extraSave);
				?>
			</div>
		</div>
		<?php echo form_close(); ?>
	</div>
</div>
<!--------Add braakout div end-------->
