	<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 |--------------------------------------------------------------------------|
 | This Field define for temparay open popup by add section id 				|
 | and edit record id														|	
 |--------------------------------------------------------------------------|
*/
 
$tempHidden = array(
	'type' => 'hidden',
	'name' => 'tempHidden',
	'id' => 'tempHidden',
	'value' => '',
);

echo form_input($tempHidden);
?>

<?php $this->load->view('event_menus');  ?>

<div class="row-fluid mt20">
	<div class="span7">
	
	<div class="formparag pl0 pr0">
		<?php echo lang('event_breakout_msg'); ?>
	</div>
	<div class="spacer15"></div>
            
            <div class="addanother_event_new">
                <a href="javascript:void(0)" class="add_edit_breakouts" breakoutid="0">
					<div class="add_img">&nbsp;</div><?php echo lang('event_breakout_add_breakout'); ?>
			    </a>
                <div class="clearfix"></div>
			</div>
		<div class="clearfix"></div>
		<div class="spacer36"></div>
		
		<?php 
			if(!empty($eventbreakouts)){
				
				//prepare array data for current and next form hide/show  
				foreach($eventbreakouts as $breakouts){
					$recordId[] =  $breakouts->id;
				}
				
				//show breakout forms	
				$rowCount = 0;
				foreach($eventbreakouts as $breakouts){
					$nextId = $rowCount+1;
					$sendData['formdata'] = $breakouts; 
					$sendData['nextRecordId']	= (isset($recordId[$nextId]))?$recordId[$nextId]:'0';
					// daynamic load all breakouts forms
					$this->load->view('form_breakout_types',$sendData);
					$rowCount++;
				}
			}
		?>
		
		<?php 
			//next and previous button
			$buttonData['viewbutton'] = array('back','next','preview');
			$buttonData['linkurl'] 	= array('back'=>base_url('event/setupregistrant'),'next'=>base_url('event/setupsponsors'),'preview'=>'');
			$this->load->view('common_back_next_buttons',$buttonData);
		?>
		
    </div>
	
	<!----Right side advertisement view load start------>
		<?php $this->load->view('right_side_advert'); ?>
	<!----Right side advertisement view load end------>
	
</div>
 
<script type="text/javascript" language="javascript">
	
	var registrantObj = $.parseJSON('<?php echo json_encode($eventregistrants); ?>');
 
	
	/*-----Open add and edit breakout popup-------*/
	$(document).on('click','.add_edit_breakouts',function(){
		
		if($(this).attr('breakoutid')!==undefined){
			var breakoutId = parseInt($(this).attr('breakoutid'));
			//set value in assing
			$("#tempHidden").val(breakoutId);
		}else{
			//get value form temp hidden and set
			var breakoutId = parseInt($("#tempHidden").val());
		}
		var eventId = '<?php echo $eventId ?>';
		var sendData = {"breakoutId":breakoutId, "eventId":eventId};
		
		//call ajax popup breakout add and edit 
		ajaxpopupopen('add_edit_brakout_popup_div','event/addeditbreakoutview',sendData,'add_edit_breakouts');
		
	});
	
	//save breakout add and edit data
	ajaxdatasave('formTypeofBreakout','event/addeditbreakoutsave',false,true,true,false,false,false,false,false);
	
		
	/*---This function is used to delete breakout -----*/
	customconfirm('delete_breakout','event/deletebreakout');
	
	/*--------registrant selection Limit---------*/
	$(".registrantSelectionLimit").click(function(){
		var getid=$(this).attr('registrantselectionlimitid');
		var getchecked=$(this).attr('checked');
		if(getchecked=="checked" && getchecked!=undefined ){
			showarea("#registrant_selection_limit_div"+getid);
		}else{
			hidearea("#registrant_selection_limit_div"+getid);
		}	
	});
	
	
	/*--------add daynamic bearkout number of block----------*/
	/*$(document).on('click','.clicknumberblock .ui-spinner-button',function(){
			
		var getId=$(this).siblings('input').attr('blockbreakoutid');
		var blockLimit= parseInt($(this).siblings('input').val());
		var blockBreakoutNumber = parseInt($("#blockBreakoutNumbHidden"+getId).val());
		var getValueSession= parseInt($("#breakoutNumberOfSession"+getId).val());
		
		//check select block for breakout
		if(getValueSession > 0){
			var tempblockLimit = 1;
			if(blockLimit > 1){
				tempstreamLimit = blockLimit-1;
			}
			custom_popup('Please set session value zero for changing this value.',false);
			$("#breakoutNumberOfBlocks"+getId).val(tempblockLimit);
			return false;
		}
		
			// if selected is greater then then add condition
			if(blockLimit > blockBreakoutNumber){
				for(i=0;i<blockLimit;i++){
					var block_html = '';
					var blockPosition = i+1;
					var blockFieldId = getId+'_'+blockPosition;
					//parepare html 
					block_html += '<div id="blocks_breakout_row'+blockPosition+'">';			
					block_html += '<div class="control-group mb10"><label class="control-label" for="inputEmail">Block  '+blockPosition+' Title </label>';
					block_html += '<div class="controls"><input type="text" class="blocktitleshow'+getId+' blocktitleshowenter" breakoutid="'+getId+'" blockrowid="'+blockPosition+'" name="blockTitle'+blockFieldId+'" value="Block '+blockPosition+' Title" id="blockTitle'+blockFieldId+'" required=""></div></div>';
					block_html += '<div class="control-group mb10 breakouttime"  ><label for="inputEmail" class="control-label">Block '+blockPosition+', Start</label>';
					block_html += '<div class="controls registrat_setup"> <div class="inputnubcontainer ml0 customNumbertype pull-left calender_container">';
					block_html += '<input type="text" readonly="" required="" name="blockStartTime'+blockFieldId+'" id="blockStartTime'+blockFieldId+'" class = "width_100px timepicker" /></div>';
					block_html += '<div class="customcheckbox_div allowword custom_finish calender_container calle_last"><label class="control-label mt10 mr15 pl0" for="inputEmail">Finish</label>';
					block_html += '<div class="inputnubcontainer ml0 customNumbertype pull-left"><input type="text" readonly="" required="" name="blockEndTime'+blockFieldId+'" id="blockEndTime'+blockFieldId+'" class ="width_100px timepicker" /></div>';
					block_html += '<input type="hidden" name="breakoutBlockId'+getId+'['+blockPosition+']" value="0" id="breakoutBlockId'+blockFieldId+'">';
					block_html += '</div></div></div>';
					block_html += '</div>';
				
					if(blockBreakoutNumber==0){
						$('#block_data'+getId).append(block_html);
					}else{
							if(blockBreakoutNumber < blockPosition){
								$('#block_data'+getId).append(block_html);
							}
					}
				}
				
				//call time picker
				timepickercall();
			}else{
				//remove field condition
				for(j=blockBreakoutNumber;j>blockLimit;j--){
					$("#blocks_breakout_row"+j).remove();
				}	
			}
			
			$("#blockBreakoutNumbHidden"+getId).val(blockLimit);
		//}
	});
	*/
	
	
	/*----This function is use set block title value in selectbox----*/
	/*$(document).on('blur','.blocktitleshowenter',function(){
		var blockOptionValue = '<option value="">Select block</option>'; // default 
		var breakoutid = $(this).attr('breakoutid');
		//console.log(breakoutid);
		//create block option value
		$(".blocktitleshow"+breakoutid).each(function(){
			var getValueTitle = $(this).val();
			var getTitleRow = $(this).attr('blockrowid');
			if(getValueTitle!=""){
				blockOptionValue += '<option value="'+getTitleRow+'">'+getValueTitle+'</option>';
			}
		});
		//set create new create option value
		$('.blocktitlelisting'+breakoutid).html(blockOptionValue);
	});	
	
	*/
	
	
	/*----This function is use change stream session header title----*/
	$(document).on('keyup','.streamtitlechange',function(){
		
		var streamvalue = $(this).val();
		var breakoutid = $(this).attr('breakoutid');
		var streamrowid = $(this).attr('streamrowid');
		var preparebreakstreamid = breakoutid+'_'+streamrowid;
		if(streamvalue==""){
			//set default header of stream session details 
			$("#"+preparebreakstreamid).html('Session details '+ streamrowid);
		}else{
			//set new header of stream session details 
			$("#"+preparebreakstreamid).html(streamvalue);
		}
	});	
	
	
	/*-------add daynamic bearkout number of stream--------*/
	$(document).on('click','.clicknumberstream .ui-spinner-button',function(){
		
		var getId=$(this).siblings('input').attr('streambreakoutid');
		
		var streamLimit= parseInt($(this).siblings('input').val());
		var getValueSession= parseInt($("#breakoutNumberOfSession"+getId).val());
		var streamsBreakoutNumber = parseInt($("#streamsBreakoutNumbHidden"+getId).val());
	    
		//check select session for breakout
		if(getValueSession > 0){
			var tempstreamLimit = 1;
			if(streamLimit > 1){
				tempstreamLimit = streamLimit-1;
			}
			custom_popup('Please set session value zero for changing this value.',false);
			$("#breakoutNumberOfStreams"+getId).val(tempstreamLimit);
			return false;
		}
		 
				   
		    //$('#stream_data'+getId).html('');
		    //if(streamLimit!=0){
			
			// if selected is greater then then add condition
			var brekout_sesson_content='';
			
			 var stream_html = '';
			if(streamLimit > streamsBreakoutNumber){
				for(i=streamsBreakoutNumber;i<streamLimit;i++){
				   
					var streamPosition = i+1;
					var streamFieldId = getId+'_'+streamPosition;
					stream_html += '<div id="streams_breakout_row'+streamFieldId+'">';
					stream_html += '<div class="control-group mb10"><label class="control-label" for="inputEmail">Name of Stream '+ streamPosition  +' <font class="mandatory_cls">*</font> </label>';
					stream_html += '<div class="controls">';
					stream_html += '<input type="text" class="streamtitlechange" breakoutid="'+getId+'" streamrowid="'+streamPosition+'" required="" id="breakoutStreamName'+streamFieldId+'" value="" name="breakoutStreamName'+streamFieldId+'">';
					stream_html += '</div></div>';
					//Venue field
					stream_html += '<div class="control-group mb10"><label class="control-label" for="inputEmail">Stream Location <font class="mandatory_cls">*</font></label>';
					stream_html += '<div class="controls">';
					stream_html += '<input type="text" class="" required="" id="breakoutStreamVenue'+streamFieldId+'" value="" name="breakoutStreamVenue'+streamFieldId+'">';
					stream_html += '<input type="hidden" name="breakoutStreamId'+getId+'['+streamPosition+']" value="0" id="breakoutStreamId'+streamFieldId+'">';
					stream_html += '</div></div>';
					//limit field
					stream_html += '<div class="control-group mb10"><label class="control-label" for="inputEmail">Limit <font class="mandatory_cls">*</font></label>';
					stream_html += '<div class="controls">';
					stream_html += '<input type="text" class="width50per" required="" id="breakoutLimit'+streamFieldId+'" value="" name="breakoutLimit'+streamFieldId+'">';
					stream_html += '</div></div></div>';
				
			    }
			      
			      
				//to add session fileds
				
				for(i=streamsBreakoutNumber;i<streamLimit;i++){
					 
					 //to heading count
					 var numStream=parseInt(streamsBreakoutNumber)+parseInt('2');
					 var detailsNum=parseInt(streamsBreakoutNumber)+parseInt('3');   
					 $('.default_step_2'+getId).hide();
					 
					 $('.session_detail_heading').html(detailsNum);                   
					 brekout_sesson_content+='<div class="sessionData'+streamFieldId+'"><div class="formparag mb20 setupbreak_inheading">STEP '+numStream+' - Setup your streams</div>';
					 brekout_sesson_content+= '<div class="control-group mb10"><label class="control-label lineH16" for="inputEmail">Sessions for Stream '+streamPosition+'<div class="infoiconbg" title="Can registrants move between streams?"></div></label>';
					 brekout_sesson_content+='<div class="controls breakout_setup"><div class="inputnubcontainer ml0 clicknumbersession"><input type="text" name="breakoutNumberOfSession'+streamFieldId+'" value="0" id="breakoutNumberOfSession'+streamFieldId+'" class="width_50px clr_425968 spinner_selectbox breakoutNumberOfSession ui-spinner-input" sessionbreakoutid="'+getId+'" readonly="" min="0" ></div></div></div>';
					 brekout_sesson_content+='<div class="control-group mb10"><label class="control-label lineH16" for="inputEmail">Can registrants move between streams? <div class="infoiconbg" title="Can registrants move between streams?" ></div></label>';
					 brekout_sesson_content+='<div class="controls"><div class="tworadiocontainer"><div class="checkradiobg"><div class="radio breakputcheck"><input type="radio" name="moveRegistrant'+streamFieldId+'" value="1" id="moveRegistrantYes'+streamFieldId+'" required="" class="moveRegistrant"><label for="moveRegistrantYes'+streamFieldId+'">Yes &nbsp; &nbsp;</label> <input type="radio" name="moveRegistrant'+streamFieldId+'" value="0" checked="checked" id="moveRegistrantNo'+streamFieldId+'" required="" class="moveRegistrant"><label for="moveRegistrantNo'+streamFieldId+'">No &nbsp; &nbsp;</label></div></div></div></div></div><div class="breakoutNumberOfSession'+streamFieldId+'"></div><input type="hidden" name="sessionBreakoutNumbHidden'+streamFieldId+'" id="sessionBreakoutNumbHidden'+streamFieldId+'" value="0"></div>';
				     brekout_sesson_content+='<input type="hidden" name="sessionStreamsBrkoutNoHidden'+streamFieldId+'" id="sessionStreamsBrkoutNoHidden'+streamFieldId+'" value="0">';

				} 
		        //for side event details registrant details for side even
		    
				if(streamsBreakoutNumber==0){
					$('#stream_data'+getId).append(stream_html);
				}else{
					if(streamsBreakoutNumber < streamPosition){
						$('#stream_data'+getId).append(stream_html);
					}
				}
				
				  var sessionData='<div id="sesson_data'+streamFieldId+'" class="sessionData"></div>';
				  $('#brekout_sesson_content'+getId).append(sessionData);
                
				   $('#sesson_data'+streamFieldId).html(brekout_sesson_content);
				  //to call spinner
				  $( ".spinner_selectbox" ).spinner();
               		
			}else{
				//remove field condition
				for(j=streamsBreakoutNumber;j>streamLimit;j--){
					 var num='1';  
					 var brkId=getId+'_'+j;
					 if(j=='1')
					 {
						 num='2';
						 $('.default_step_2'+getId).show();
					 }
					 var detailsNum=parseInt(j)+parseInt(num); 
					 
				    $('.sessionData'+brkId).remove();
					$("#streams_breakout_row"+brkId).remove();
					$('.session_detail_heading').html(detailsNum);
				}	
			}
			 
			$("#streamsBreakoutNumbHidden"+getId).val(streamLimit);
		//}
	});
	
	
	
	/*--------add daynamic breakout number of session-----------*/
	$(document).on('click','.clicknumbersession .ui-spinner-button',function(){
		 
		var getId=$(this).siblings('input').attr('sessionbreakoutid');
		
		var sessionId=$(this).siblings('input').attr('id');
        var sessionBreakoutId = sessionId.replace('breakoutNumberOfSession', '');
		var getValueSessionMain = $(this).siblings('input').val();
		var getValueSession = parseInt($(this).siblings('input').val());
		var getValueStream = parseInt($("#breakoutNumberOfStreams"+getId).val());
		var sessionstreamBrkNumber = parseInt($("#sessionStreamsBrkoutNoHidden"+sessionBreakoutId).val());
	    var sessionBreakoutNumber = parseInt($("#sessionBreakoutNumbHidden"+sessionBreakoutId).val());
	   
		//var blockOptionValue = '<option value="">Select block</option>';
		//create block option value
		/*$(".blocktitleshow"+getId).each(function(){
			//console.log(this);
			var getValueTitle = $(this).val();
			var getTitleRow = $(this).attr('blockrowid');
			if(getValueTitle!=""){
				blockOptionValue += '<option value="'+getTitleRow+'">'+getValueTitle+'</option>';
			}
		});*/
		 
		//check selected stream for breakout
		if(getValueStream==0){
			//custom_popup('Please select streams first.',false);
			//$("#breakoutNumberOfSession"+getId).val('0');
			//return false;
		}
		//getValueSession!=0 &&
		if( getValueSessionMain!=""){
		$("#breakoutNumberOfStreams"+getId).attr('disabled',true); 	
		$("#step4"+getId).show(); //show step 4 header	
			
			// if selected is greater then then add condition session list
			//if(getValueSession > sessionBreakoutNumber){	
			
			if(getValueSession > sessionBreakoutNumber){	
				for(i=sessionBreakoutNumber;i<getValueSession;i++){
				    
					var genrate_html ='';
					var iPosition = i+1;
					var iFieldId = sessionBreakoutId+iPosition;
				     
				     	var breakRegis=addBreakoutRegistrant(iFieldId);	
	                   
						genrate_html += '<div id="session_breakout_row'+iFieldId+'">';			
						genrate_html += '<div class="control-group mb10 breakouttime"  ><label for="inputEmail" class="control-label">Session '+iPosition+', Start <font class="mandatory_cls">*</font></label>';
						genrate_html += '<div class="controls registrat_setup"> <div class="inputnubcontainer ml0 customNumbertype pull-left calender_container">';
						genrate_html += '<input type="text" readonly="" required="" name="sessionStartTime'+iFieldId+'" id="sessionStartTime'+iFieldId+'" class = "width_100px timepicker" /></div>';
						genrate_html += '<div class="customcheckbox_div allowword custom_finish calender_container calle_last"><label class="control-label mt10 mr15 pl0" for="inputEmail">Finish</label>';
						genrate_html += '<div class="inputnubcontainer ml0 customNumbertype pull-left"><input type="text" readonly="" required="" name="sessionEndTime'+iFieldId+'" id="sessionEndTime'+iFieldId+'" class ="width_100px timepicker" /></div>';

						genrate_html += '<input type="hidden" name="breakoutSessionId'+sessionBreakoutId+'['+iFieldId+']" value="0" id="breakoutSessionId'+iFieldId+'">';
						genrate_html += '</div>'+breakRegis+'</div></div>';
						 
						/*
						genrate_html += '<div class="control-group mb10"><label class="control-label" for="inputEmail">Session '+iPosition+', Limit</label>';
						genrate_html += '<div class="controls"><input type="text" class="" name="sessionLimit'+iFieldId+'" value="" id="sessionLimit'+iFieldId+'" required=""></div></div>';
						genrate_html += '<div class="control-group mb10"><label for="inputEmail" class="control-label">Select Block</label>';
						genrate_html += '<div class="controls pr0"><div class="pull-left select-style register select_breakout">';
						genrate_html += '<select name="sessionblockselect'+iFieldId+'" class="blocktitlelisting'+getId+'" required=" " id="sessionblockselect'+iFieldId+'">';
						genrate_html +=  blockOptionValue;
						genrate_html += '</select></div></div></div>';*/
						
						genrate_html += '</div>';
						
						//width50per			
								
					if(sessionBreakoutNumber==0){
						//$('#session_list_div'+getId).append(genrate_html); 
						      
							$('.'+sessionId).append(genrate_html);
							
							
					}else{
							if(sessionBreakoutNumber < iPosition)
							{
								 $('.'+sessionId).append(genrate_html);
								 
								//$('#session_list_div'+getId).append(genrate_html); 
							}
					}
					//call time picker
					timepickercall();
				}
			}else{
				//remove field condition
				for(jrm=sessionBreakoutNumber;jrm>getValueSession;jrm--){
			
					$("#session_breakout_row"+sessionBreakoutId+jrm).remove();
					$("#session_details_row"+sessionBreakoutId+jrm).remove();
					
					
				}	
			}
			 	 
			//create stream session details row section
			for(j=0;j<1;j++){
		
				var jPosition = j+1;
				var jFieldId = sessionBreakoutId; //getId+'_'+jPosition;
				var session_details_html =  '';
				
				var getStreamTitle = $("#breakoutStreamName"+jFieldId).val();
				if(getStreamTitle==""){
					getStreamTitle = 'Session details '+jPosition;
				}
			      
				if($("#"+sessionBreakoutId).length == '0')
				{
					
					session_details_html += '<div class="formparag mb10" id="session_heading_'+jFieldId+'"><div class="accordian_headingbg bg_0073ae">';
					session_details_html += '<div class="typeofregistH_small pull-left ptr action_session_details" id="'+jFieldId+'"> '+getStreamTitle+' </div>';
					session_details_html += '<div class="clearfix"></div></div>';
					session_details_html += '<div id="session_details_list_div'+jFieldId+'" class="dn session_details_list_div_cls'+jFieldId+'"></div></div>';
					$('#session_details'+getId).append(session_details_html);
				}
			
				//add main div of session details
				//if(sessionstreamBrkNumber==0){
					
					//$('#session_details'+getId).append(session_details_html);
				//}
				
			  // console.log("getValueSession"+getValueSession);
			  //console.log("sessionBreakoutNumber"+sessionBreakoutNumber);
				
				// if selected is greater then then add session details list
				
				if(getValueSession > sessionBreakoutNumber){
					
					//old code k=0 24-jan-2013
					for(k=sessionBreakoutNumber;k<getValueSession;k++){
						var session_details_list_html =  '';
						var kPosition = k+1;
						var rowPosition = jPosition+'_'+kPosition;
						var kFieldId = sessionBreakoutId+kPosition; //getId+'_'+jPosition+kPosition;
						
						session_details_list_html += '<div class="accordian_cnt" id="session_details_row'+sessionBreakoutId+kPosition+'">';
						session_details_list_html += '<div class="control-group mb10">';
						session_details_list_html += '<label for="inputEmail" class="control-label">About Session '+kPosition+'</label>';
						session_details_list_html += '<div class="controls"><textarea class="bdr_n" placeholder="" id="breakoutAboutSession'+kFieldId+'" name="breakoutAboutSession'+kFieldId+'" rows="3" cols="40"></textarea></div></div>';
						session_details_list_html += '<div class="control-group mb10">';
						session_details_list_html += '<label for="inputEmail" class="control-label">Speakers</label>';
						session_details_list_html += '<div class="controls"><input type="text" class="bdr_n" id="breakoutSpeakers'+kFieldId+'" name="breakoutSpeakers'+kFieldId+'" /></div>';
						session_details_list_html += '</div>';
						session_details_list_html += '<div class="control-group mb10">';
						session_details_list_html += '<label for="inputEmail" class="control-label">Organisation</label>';
						session_details_list_html += '<div class="controls"><input type="text" class="bdr_n"  id="breakoutOrganisation'+kFieldId+'" name="breakoutOrganisation'+kFieldId+'" /></div>';
						session_details_list_html += '<input type="hidden" name="breakoutStreamSessionId'+getId+'['+rowPosition+']" value="0" id="breakoutStreamSessionId'+kFieldId+'">';
						session_details_list_html += '</div></div>';
						
						//check condition and add field
						if(sessionBreakoutNumber==0){
							  $('#session_details_list_div'+sessionBreakoutId).append(session_details_list_html); 
						}else{
								if(sessionBreakoutNumber < kPosition){
									
									$('#session_details_list_div'+sessionBreakoutId).append(session_details_list_html);
								}
						}
						
					}
					
				}else{
					
					//remove field condition
					for(rm=sessionBreakoutNumber;rm>getValueSession;rm--){
						var rowPosition = jPosition+'_'+rm;
						$("#session_details_row"+getId+rowPosition).remove();
					    $(".sessionDetailRow"+getId+rowPosition).remove();
						
					}	
				}	
					
			}
			 $("#sessionStreamsBrkoutNoHidden"+sessionBreakoutId).val(getValueStream)
			$("#sessionBreakoutNumbHidden"+sessionBreakoutId).val(getValueSession);
			
		} else {
			//$("#sessionBreakoutNumbHidden"+getId).val(getValueSession);
			$("#breakoutNumberOfStreams"+getId).attr('disabled',false); 
			$('#session_list_div'+getId).html('');
		} 
	});
	
	
	/*----This function is used to show and hide session details div----*/
	$(document).on('click','.action_session_details',function(){
		 
		var getId = $(this).attr('id');
	
		var showStatus = $(".session_details_list_div_cls"+getId).is(':visible');
  
		if(showStatus==false){
			$(this).removeClass('typeofregistH_small').addClass('typeofregistV_small');//open div
			
			  showarea(".session_details_list_div_cls"+getId);
		}else{
			$(this).removeClass('typeofregistV_small').addClass('typeofregistH_small');//close div
			hidearea(".session_details_list_div_cls"+getId);
		}
	})
	
	//*----------This function is userd to create registrant for breakout */
	
		function addBreakoutRegistrant(iFieldId)
	    {
		    var regis='';
		    $.each(registrantObj, function(key, value){
				
			var sessionRegisId=	iFieldId+'_'+value['id'];
		 
			regis+='<div class="tickbox_cont">';
			regis+='<div class="standerd_container">';
			regis+='<div class="controls checkradiobg setupcheck ml0 pl0 pr0">';
			regis+='<div class="checkbox pl0 position_R setupcheck">';
			regis+='<input type="checkbox" name="registrantSelectionLimit'+sessionRegisId+'" value="'+value['id']+'" id="registrantSelectionLimit'+sessionRegisId+'" class="registrantSelectionLimit regisChecked'+iFieldId+'" registrantselectionlimitid="'+sessionRegisId+'" regisFieldId="'+iFieldId+'"><label for="registrantSelectionLimit'+sessionRegisId+'" class="width_auto ml0 mt10 eventDcheck">&nbsp;'+value['registrant_name']+'</label></div></div></div>';
			regis+='<div class="setlimit regisLimit dn" id="registrant_selection_limit_div'+sessionRegisId+'">';
			regis+='<div class="control-group">';
			regis+='<label class="control-label width_auto" for="inputEmail">Limit';
			regis+='<div class="infoiconbg" title="Limit"> </div></label>';
			regis+='<div class="controls ml0 pr0 pl0"><input type="text" name="registrantLimit'+sessionRegisId+'" value="" id="registrantLimit'+sessionRegisId+'" class="field_disable limit_input registrantselection'+sessionRegisId+'"></div>';
			regis+='</div></div><div class="clearfix"> </div></div>';
			regis+='<input type="hidden" name="session_registrant'+iFieldId+'['+sessionRegisId+']"  value="">';

            });
            return regis;
	   }
	   
   
    //to slide show and hide for registrants
	$(document).on('click','.registrantSelectionLimit',function(){
		
		var registId='';
		var getid=$(this).attr('registrantselectionlimitid');
		var fieldId=$(this).attr('regisFieldId');
	  
		var getchecked=$(this).attr('checked');
		if(getchecked=="checked" && getchecked!=undefined ){
			showarea("#registrant_selection_limit_div"+getid);
			$("#registrantSelection"+getid).removeAttr('disabled');
		}else{
			hidearea("#registrant_selection_limit_div"+getid);
			$("#registrantSelection"+getid).attr('disabled','disabled'); 
		}	
		//get registrant checked id
		$('.regisChecked'+fieldId).each(function(){
			 
			if($(this).is(':checked',true))
			{
				 var value=$(this).val();
				 if(registId!='')
				 {
					 registId+=',';
				 }
				 registId+=value;
				
				
			}
	
		});
		 
		$('#session_registrant'+fieldId).val(registId);
	});
	
	
	/*----This function is used to create duplicate copy of any breakout----*/
	
	$('.duplicate_breakout').click(function(){
		var breakoutId = parseInt($(this).attr('breakoutid'));
		var eventId = '<?php echo $eventId ?>';
		var sendData = {"breakoutId":breakoutId, "eventId":eventId};
		
		//call ajax popup breakout add and edit 
		ajaxpopupopen('duplicate_brakout_popup_div','event/duplicatebreakoutview',sendData,'duplicate_breakout');
		
	});
	
	//save breakout duplicate create data
	ajaxdatasave('formTypeofBreakoutDuplicate','event/duplicatebreakoutsave',false,true,true,false,false,false,false,false);
	
	/*----This function is used to session details open icon div----*/
	$(document).on('mouseenter','.action_session_details',function(){
		$(this).removeClass('typeofregistH_small').addClass('typeofregistV_small');
	})
	
	/*----This function is used to session details close icon div----*/
	$(document).on('mouseout','.action_session_details',function(){
		var idName = $(this).attr('id');
		if($('.session_details_list_div_cls'+idName).is(':visible')==false){
			$(this).removeClass('typeofregistV_small').addClass('typeofregistH_small');
		}
	})
	
</script>	
