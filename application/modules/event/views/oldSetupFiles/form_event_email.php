<div class="panel event_panel">
	<div class="panel-heading ">
		<h4 class="panel-title medium dt-large ">
			<a class=" " data-toggle="collapse" data-parent="#accordion" href="#collapseSeven"><?php echo lang('custom_email_email_heading'); ?></a>
		</h4>
	</div> 
	<div style="height: auto;" id="collapseSeven" class="accordion_content collapse apply_content <?php echo getIsCollapseOpen("collapseSeven"); ?>">
		<form>
		<div class="panel-body ls_back dashboard_panel small" id="showhideformdivemailSection">
			<div class="row-fluid-15">
				<a class="add_btn medium" href="javascript:void(0)" id="add_custom_emails_btn"><?php echo lang('add_custom_email'); ?></a>
			</div>
			<!--end of row-fluid-->
			
			<div id="eventcustomemaillistid">
				<?php 
					//echo form_hidden('eventId', $eventId);
					//echo form_hidden('formActionName', 'contactPerson');
				?>
				<?php echo $this->load->view('custom_email_list',array('eventId'=>$eventId)); ?>
			</div>
			
			<div class="btn_wrapper ">
				<a href="#collapseSeven" class="pull-left scroll_top">Top</a>
					<a href="#" class="default_btn btn pull-right medium">Save</a>
					<a href="#" class="default_btn btn pull-right medium">Clear</a>
			</div>
			
		</div>
		</form>
	</div>
 </div>
<!--end of panel-->
          
                
<script>
	
ajaxdatasave('formEventEmail','<?php echo $this->uri->uri_string(); ?>',true,true,false,true,true,'#showhideformdivemailSection','#showhideformdivtermSection',false,true,'custom_email');	
	
	
$(document).ready(function() {
  
	$("#add_custom_emails_btn").click(function(){
		formPostData = {};
		//eventcustompopup('event/eventcustomemailpopup','custom_email',formPostData);
		//call ajax popup function
		ajaxpopupopen('custom_email','event/eventcustomemailpopup',formPostData,'add_custom_emails_btn');
		
	});
});

//edit custom contact
$(document).on("click", ".editeventcustomemail", function(e){
	    
		var emailId = $(this).attr('data-email-id');
		formPostData = {emailId:emailId};
		//call ajax popup function
		ajaxpopupopen('custom_email','event/eventcustomemailpopup',formPostData,'editeventcustomemail');
		//eventcustompopup('event/eventcustomemailpopup/'+emailId,'custom_email',formPostData);
	
});

// delete custom contact
customconfirm('deleteeventcustomemail','event/deleteeventemail');



</script>
