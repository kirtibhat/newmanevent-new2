<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	//define variable for value
	$registrationLimitId = "0";
	$regisSelectionCheckBoxDisable  = 	false;
	$registrantSelectionLimitVal	= 	false;
	$registrantLimitVal		 		= 	'';
	$registrantSelectionLimitDiv    = 'dn';
	$getSingleDayRegistId = ($singleDayRegistId==0)?NULL:$singleDayRegistId;
	
	//get value by side event id and registraint id 
	$where = array('sponsor_id'=>$sponsorId,'registrant_id'=>$registrantId,'registrant_daywise_id'=>$getSingleDayRegistId);
	$sponsorRegist = getDataFromTabel('nm_sponsor_registrant','*',$where);
	
	
	if(!empty($sponsorRegist)){
		$sponsorRegist = $sponsorRegist[0];
		//define variable for value
		$registrantLimitVal	 	 	 		= 	$sponsorRegist->registrant_limit;
		$registrationLimitId 				=	$sponsorRegist->id;
		$registrantSelectionLimitVal		= 	true;
		$registrantSelectionLimitDiv  	    =   '';
		$regisSelectionCheckBoxDisable 	    = 	false;
	}
	
	
	$registrantSelectionLimit = array(
					'name'	=> 'registrantSelectionLimit'.$regisSponsorId,
					'value'	=> '1',
					'id'	=> 'registrantSelectionLimit'.$regisSponsorId,
					'type'	=> 'checkbox',
					'class'	=> 'registrantSelectionLimit',
					'registrantselectionlimitid'	=> $regisSponsorId,
					'checked'=> $registrantSelectionLimitVal,
				);	
	if($regisSelectionCheckBoxDisable){
		$registrantSelectionLimit['disabled'] = 'disabled';
	}			
	
	$registrantLimit = array(
					'name'	=> 'registrantLimit'.$regisSponsorId,
					'value'	=> $registrantLimitVal,
					'id'	=> 'registrantLimit'.$regisSponsorId,
					'type'	=> 'text',
				//	'required'	=> '',
					'class'	=> 'field_disable limit_input registrantselection'.$regisSponsorId,
				);	
				
	
	// if registraint selection is checked	
	if($registrantSelectionLimitVal){
		$registrantLimit['required']	 	  =	'';
	}	
	
	$sponsorRegistrantId = array(
				'name'	=> 'sponsorRegistrantId'.$sponsorId.'['.$registrantId.']['.$singleDayRegistId.']',
				'value'	=> $registrationLimitId,
				'id'	=> 'sponsorRegistrantId'.$regisSponsorId,
				'type'	=> 'hidden',
			);					
							
	echo form_input($sponsorRegistrantId);	
	
?>

	<!------This div section show registrant and day wise registrant list start------->
	<div class="tickbox_cont <?php echo ($countRowRegis==1)?'height_35':''; ?>">
		<div class="standerd_container">
			<div class="controls checkradiobg setupspons ml0 pl0 pr0">
				<div class="checkbox pl0 position_R setupspons">
					<?php echo form_checkbox($registrantSelectionLimit); ?>
					<label for="registrantSelectionLimit<?php echo $regisSponsorId; ?>" class="width_auto ml0 mt10 eventDcheck">&nbsp; <?php echo $registrantName; ?></label>
				</div>
			</div>
		</div>
		<div class="setlimit <?php echo $registrantSelectionLimitDiv; ?>" id="registrant_selection_limit_div<?php echo $regisSponsorId; ?>">
			<div class="control-group">
				<label class="control-label width_auto" for="inputEmail"><?php echo lang('comm_limit'); ?>
				<div class="infoiconbg bg_731472 sponsors_tooltip">
					<span class="show_tool_tip" title='<?php echo lang('tooltip_limit'); ?>'> </span>
				</div>
				</label>
				<div class="controls ml0 pr0 pl0">
					<?php echo form_input($registrantLimit); ?>
					<?php echo form_error('registrantLimit'.$regisSponsorId); ?>
				</div>
			</div>
		</div>
		<div class="clearfix"> </div>
	</div>
	<!------This div section show registrant and day wise registrant list end------->
