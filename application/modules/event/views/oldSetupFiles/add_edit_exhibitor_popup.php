<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//--------add type fo registration fields---------//	

$exhibitorNameValue = (!empty($exhibitordata->exhibitor_name))?$exhibitordata->exhibitor_name:''; 
$headerAction = ($exhibitorIdValue > 0)?lang('event_exhibitor_edit_exhibitor'):lang('event_exhibitor_add_exhibitor'); 


$formAddExhibitor = array(
		'name'	 => 'formAddExhibitor',
		'id'	 => 'formAddExhibitor',
		'method' => 'post',
		'class'  => 'form-horizontal mt10',
	);
	
$exhibitorName = array(
		'name'	=> 'exhibitorName',
		'value'	=> $exhibitorNameValue,
		'id'	=> 'exhibitorName',
		'type'	=> 'text',
		'required'	=> '',
	);	

	
$exhibitorId = array(
		'name'	=> 'exhibitorId',
		'value'	=> $exhibitorIdValue,
		'id'	=> 'exhibitorId',
		'type'	=> 'hidden',
	);


?>

<!--------Add exhibitor div start-------->
	<div class="modal fade alertmodelbg dn" id="add_edit_exhibitor_popup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
			
			<div class="modal-header bg_79496a">
				<h4 class="modal-title"><?php echo $headerAction; ?></h4>
			</div>
			<?php  echo form_open(base_url('event/addeditexhibitorsave'),$formAddExhibitor); ?>
				<div class="modal-body ">
				<h4><?php echo lang('event_exhibitor_add_msg'); ?></h4>
				
					<div class="control-group mb10 min_height_75">
						<label for="inputEmail" class="control-label"><?php echo lang('event_exhibitor_add_field'); ?></label>
						<div class="controls">
							<?php echo form_input($exhibitorName); ?>
						</div>
					</div>
			</div>
			<div class="modal-footer">
				<?php 
					echo form_hidden('eventId', $eventId);
					echo form_input($exhibitorId);
					$extraCancel 	= 'class="btn btn-default custombtn" data-dismiss="modal" ';
					$extraSave 	= 'class="btn btn-primary  bg_5c2946 custombtn bg_f26531" ';
					echo form_button('cancel',lang('comm_cancle'),$extraCancel);
					echo form_submit('save',lang('comm_save'),$extraSave);
				?>
			</div>
		</div>
		<?php echo form_close(); ?>
	</div>
</div>
<!--------Add braakout div end-------->
