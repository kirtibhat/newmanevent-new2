<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	//define variable for value
	$regisSelectionCheckBoxDisable  = 	false;
	$registrantSelectionLimitVal	= 	false;
	$registrantLimitVal		 		= 	'';
	$registrantSelectionLimitDiv    = 'dn';
	$getSingleDayRegistId = ($singleDayRegistId==0)?NULL:$singleDayRegistId;
	
	//get value by side event id and registraint id 
	$where = array('side_event_id'=>$sideEventId,'registrant_id'=>$registrantId,'registrant_daywise_id'=>$getSingleDayRegistId);
	$sideEventRegist = getDataFromTabel('side_event_registrant','*',$where);
	
	if(!empty($sideEventRegist)){
		$sideEventRegist = $sideEventRegist[0];
		//define variable for value
		$registrantLimitVal	 	 	 		= 	$sideEventRegist->registrant_limit;
		$registrantSelectionLimitVal		= 	true;
		$registrantSelectionLimitDiv  	    =   '';
		$regisSelectionCheckBoxDisable 	    = 	true;
	}
	

	$registrantSelectionLimit = array(
					'name'	=> 'registrantSelectionLimit'.$regisSideEventId,
					'value'	=> '1',
					'id'	=> 'registrantSelectionLimit'.$regisSideEventId,
					'type'	=> 'checkbox',
					'class'	=> 'registrantSelectionLimit',
					'registrantselectionlimitid'	=> $regisSideEventId,
					'checked'=> $registrantSelectionLimitVal,
				);	
	if($regisSelectionCheckBoxDisable){
		$registrantSelectionLimit['disabled'] = 'disabled';
	}			
	
	$registrantLimit = array(
					'name'	=> 'registrantLimit'.$regisSideEventId,
					'value'	=> $registrantLimitVal,
					'id'	=> 'registrantLimit'.$regisSideEventId,
					'type'	=> 'text',
				//	'required'	=> '',
					'class'	=> 'field_disable limit_input registrantselection'.$regisSideEventId,
				);	
				
	
	// if registraint selection is checked	
	if($registrantSelectionLimitVal){
		$registrantLimit['required']	 	  =	'';
	}	
	
?>

	<!------This div section show registrant and day wise registrant list start------->
	<div class="tickbox_cont <?php echo ($countRowRegis==1)?'height_35':''; ?>">
		<div class="standerd_container">
			<div class="controls checkradiobg setupcheck ml0 pl0 pr0">
				<div class="checkbox pl0 position_R setupcheck">
					<?php echo form_checkbox($registrantSelectionLimit); ?>
					<label for="registrantSelectionLimit<?php echo $regisSideEventId; ?>" class="width_auto ml0 mt10 eventDcheck">&nbsp; <?php echo $registrantName; ?></label>
				</div>
			</div>
		</div>
		<div class="setlimit <?php echo $registrantSelectionLimitDiv; ?>" id="registrant_selection_limit_div<?php echo $regisSideEventId; ?>">
			<div class="control-group">
				<label class="control-label width_auto" for="inputEmail"><?php echo lang('event_side_regis_limit'); ?>
				<div class="infoiconbg bg_362c66 sideevent_tooltip">
					<span class="show_tool_tip" title='<?php echo lang('tooltip_side_regis_limit'); ?>'> </span>
				</div>
				</label>
				<div class="controls ml0 pr0 pl0">
					<?php echo form_input($registrantLimit); ?>
					<?php echo form_error('registrantLimit'.$regisSideEventId); ?>
				</div>
			</div>
		</div>
		<div class="clearfix"> </div>
	</div>
	<!------This div section show registrant and day wise registrant list end------->
	
	
