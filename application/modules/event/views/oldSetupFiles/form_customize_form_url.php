<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$registrationURLValue = '';
if(!empty($eventdata)){
	$eventdata = $eventdata[0];
	$eventUrlId = encode($eventId);
	$registrationURLValue = base_url('frontend/index/'.$eventUrlId);
}

$formRegistrationURL = array(
		'name'	 => 'formRegistrationURL',
		'id'	 => 'formRegistrationURL',
		'method' => 'post',
		'class'  => 'form-horizontal',
	);
$registrationURL = array(
		'name'	=> 'registrationURL',
		'value'	=>  $registrationURLValue,
		'id'	  => 'callback-paragraph',
		'required'	=> '',
		'readonly'	=> true,
		'type'    => 'text',
	);
	
		
?>

<div class="commonform_bg pb0">
	<div class="headingbg bg_807f83">
		<div class="typeofregistV pull-left showhideaction ptr" id="registrationUrl">
			<?php echo lang('event_customize_form_url'); ?>
		</div>
		<div class="clearfix">
		</div>
	</div>
	<?php echo form_open($this->uri->uri_string(),$formRegistrationURL); ?>
		<div class="form-horizontal dn form_open_chk" id="showhideformdivregistrationUrl">
		<div class="formparag mb20">
			<?php echo lang('event_customize_form_url_msg'); ?>
		</div>
		<div class="control-group mb10">
			<label class="control-label" for="inputEmail"><?php echo lang('event_customize_form_url_field'); ?>
				<div class="infoiconbg bg_5f6062 customize_tooltip">
					<span class="show_tool_tip" title="<?php echo lang('tooltip_customize_form_url_field'); ?>"> </span>
				</div>
			</label>
			<div class="controls">
				<?php echo form_input($registrationURL); ?>
				<?php echo form_error('registrationURL'); ?>
			</div>
		</div>
		<div class="spacer10">
		</div>
		<div class="row-fluid">
			<?php 
				$extraCopy 	= ' class="submitbtn_cus bg_5f6062 clr_99BBCC  savechange_regd ptr"  id="copy-callbacks"';
				echo form_button('copy',lang('event_customize_url_copy'),$extraCopy);
				$extraEmail 	= ' class="submitbtn_cus bg_5f6062 clr_99BBCC savechange_regd mr_5px" ';
				echo form_button('email',lang('event_customize_url_email'),$extraEmail);
			?>
		</div>
		<div class="spacer36">
		</div>
	</div>
	<?php echo form_close(); ?>
</div>
<!-- /commonform_bg -->

<script src="<?php echo base_url('templates/system/js/zclip/zclip.min.js') ?>" type="text/javascript"></script>
<script src="<?php echo base_url('templates/system/js/zclip/jquery.zclip.js') ?>" type="text/javascript"></script>
<script>
$(document).ready(function(){
    $("#copy-callbacks").zclip({
        path:'<?php echo base_url('templates/system/js/zclip/zeroClipboard.swf') ?>',
        copy:$('#callback-paragraph').val(),
        beforeCopy:function(){
      
        },
        afterCopy:function(){
           custom_popup('Event url copied.',true)
        }
    });

});
</script>	
