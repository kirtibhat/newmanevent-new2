<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$invitationId       = $invitationData->id;
$invitationPerFrm   = $invitationId.'_perosnal'.$formSectionName;
$nextinvitationFrm  = $nextRecordId.'_perosnal'.$formSectionName;
$regisFormName      = $invitationData->invitation_name;
$formPersonalDetails = array(
'name'          => 'formPersonalDetails'.$invitationId.$formSectionName,
'id'            => 'formPersonalDetails'.$invitationId.$formSectionName,
'method'        => 'post',
'class'         => 'form-horizontal mb0',
);

$fieldsmastervalue = fieldsmastervalue();
$emailfield = array(
'name'          => 'emailfield',
'value'         => '',
'id'            => 'emailfield',
'type'          => 'text',
'placeholder'   => 'Mandatory',
'readonly'      => '',
'class'         => 'medium_input',
);  
?>        
<div class="addpersonaldiv">
<?php 
$whereFrmCond  = array('event_id'=>$eventId,'form_id'=>'4','registrant_id'=>$invitationId);
$formfielddata = getDataFromTabel('custom_form_fields','*',$whereFrmCond);
if(!empty($formfielddata)){
foreach($formfielddata as $formfield){
$fieldCreateBy = $formfield->field_create_by;
if($fieldCreateBy=="1"){        
    if($formfield->field_name=="email"){ 
    ?>
        <div class="row">
           <div class="col-3">
              <div class="labelDiv">
                 <label for="email" class="form-label text-right">Email</label>
<!--
                 <span class="info_btn"><span class="field_info xsmall"><?php echo lang('title_invitation_email'); ?></span></span>
-->
              </div>
           </div>
           <div class="col-5">
              <?php 
                  /*
                    $fieldName= 'field_'.$formfield->id;    
                    echo form_input($emailfield);
                    $fieldName= 'field_'.$formfield->id;    
                    echo form_hidden($fieldName, $formfield->field_status);
                    echo form_hidden('fieldid[]', $formfield->id);
                  */ 
                  
                  
                   echo form_hidden('fieldid[]', $formfield->id);
                    $other = 'id="field_'.$formfield->id.'"  class="custom-select medium_select" ';
                    $fieldName= 'field_'.$formfield->id;    
                    echo form_dropdown($fieldName, $fieldsmastervalue, $formfield->field_status,$other);     
                  
                ?>
              <span id="error-dinner_dance6"></span> 
           </div>
        </div>

    <?php }else {  ?>
        
        <div class="row">
           <div class="col-3">
              <div class="labelDiv">
                 <label for="<?php echo ucwords($formfield->field_name); ?>" id="masterfieldlbl_<?php echo $formfield->id; ?>" class="form-label text-right"><?php echo ucwords($formfield->field_name); ?></label>
              </div>
           </div>
           <div class="col-5">
                <?php 
                    $other = 'id="field_'.$formfield->id.'" class="custom-select medium_select"';
                    $fieldName= 'field_'.$formfield->id;    
                    echo form_dropdown($fieldName, $fieldsmastervalue, $formfield->field_status,$other); 
                    echo form_hidden('fieldid[]', $formfield->id);   
                ?>
                
                <?php  if($formfield->is_editable=="1"){ ?>
                    <div class="editButtonGroup pull-left">
                    <a href="javascript:void(0);" class="btn add_personal_form_field" invitationid="<?php echo $invitationId; ?>" customfieldid="<?php echo $formfield->id; ?>"  data-parent-form-id="formInvitationTypeSave<?php echo $invitationId; ?>"  formId="4"> 
                        <span class="medium_icon"> <i class="icon-edit"></i> </span>
                    </a> 
                    </div>
                <?php } ?> 
              <span id="error-dinner_dance6"></span> 
           </div>
        </div>
     <?php } ?>
        <?php  } //end if
} //end loop ?>
                
<?php  if(!empty($formfielddata)){
foreach($formfielddata as $key=>$formfield){
    $fieldCreateBy = (isset($formfield->field_create_by) && $formfield->field_create_by=="0")?$formfield->field_create_by:NULL;
    if($fieldCreateBy=="0"){        
    ?>
    <div class="row" id="invitationcustomfield_<?php echo $formfield->id.'_'.$key; ?>">
       <div class="col-3">
          <div class="labelDiv">
             <label for="<?php echo ucwords($formfield->field_name); ?>" id="masterfieldlbl_<?php echo $formfield->id; ?>" class="form-label text-right"><?php echo ucwords($formfield->field_name); ?></label>
          </div>
       </div>
       <div class="col-5">
            <?php
                echo form_hidden('fieldid[]', $formfield->id);
                $other = 'id="field_'.$formfield->id.'" class="custom-select medium_select"';
                $fieldName= 'field_'.$formfield->id;
                echo form_dropdown($fieldName, $fieldsmastervalue, $formfield->field_status,$other);    
            ?>
            <div class="editButtonGroup pull-left">
            
            <a class="btn add_personal_form_field" invitationid="<?php echo $invitationId; ?>" customfieldid="<?php echo $formfield->id; ?>" formId="4"  data-parent-form-id="formInvitationTypeSave<?php echo $invitationId; ?>"> 
                <span class="medium_icon"> <i class="icon-edit"></i> </span>
            </a> 
            <a class="btn red delete_personal_form_field"   deleteid="<?php echo $formfield->id; ?>"> 
                <span class="medium_icon"> <i class="icon-delete"></i> </span>
            </a>  
            <span id="error-dinner_dance6"></span> 
            </div>
       </div>
    </div>
    <?php }  //end if
    } // end loop 
} // end if ?>

<div class="row custom_field_div" id="custom_field_div"></div>
<div class="row mT30">
   <div class="col-1">
   </div>
   <div class="col-5">
      <a class="btn-icon btn-icon-large btn add_personal_form_field" href="javascript:void(0)" invitationid="<?php echo $invitationId; ?>" customfieldid="0"  formId="4" id="add_field" data-parent-form-id="formInvitationTypeSave<?php echo $invitationId; ?>">
        <span class="medium_icon"><i class="icon-add"></i></span><div class="font-small"><?php echo lang('event_registration_custom_field'); ?></div>
      </a>
   </div>
</div>
<?php } //end form field ?>
</div>
