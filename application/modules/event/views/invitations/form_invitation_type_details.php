<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
$openTab        =  ''; 
$cls            = '';
$triggerCls     = ( $typeCount==0 ) ? 'trigger_limit' : '';
$requiredCheck  =  $this->input->get('req');
if($requiredCheck=='ilMt'){ if( $typeCount == 0 ) { $openTab =  'display:block;'; $cls = 'open'; } else { $openTab =  ''; $cls = ''; } } 
$invitationId = $formdata->id;
if(!empty($invitationDataId)){ if($invitationId==$invitationDataId) { $openTab =  'display:block;'; $cls = 'open'; }else { $openTab =  ''; $cls = ''; } }
if(!empty($invitationtypedetailsid)){ if($invitationId==$invitationtypedetailsid) { $openTab =  'display:block;'; $cls = 'open'; }else { $openTab =  ''; $cls = ''; } }

$invitationName             = (!empty($formdata->invitation_name))?$formdata->invitation_name:'';
$invitationPassword         = (!empty($formdata->password))?$formdata->password:'';
$invitationDescription      = (!empty($formdata->description))?$formdata->description:'';
$additionalDetailsVal       = (!empty($formdata->details))?$formdata->details:'';
$limitVal                   = (isset($formdata->invitation_limit))?$formdata->invitation_limit:'';
$limit_invitation_val           = (isset($formdata->limit_per_invitation))?$formdata->limit_per_invitation:0;
$show_remaining_invitations = (isset($formdata->show_remaining_invitations))?$formdata->show_remaining_invitations:'0';
$allow_waiting_list         = (isset($formdata->allow_waiting_list))?$formdata->allow_waiting_list:'0';
$passwordVal                = (!empty($formdata->password))?$formdata->password:'';

//set password check and uncheck
$passwordAccessYesChk       = false;
$passwordAccessNoChk        = true;
$isPassDivshow = 'dn';
if(!empty($passwordVal)){
$passwordAccessYesChk       = true;
$passwordAccessNoChk        = false;
$isPassDivshow              = '';
} 
$allow_waiting_listYesChk   = false;
$allow_waiting_listNoChk    = true;
if(!empty($allow_waiting_list)){
$allow_waiting_listYesChk   = true;
$allow_waiting_listNoChk    = false;
}
$remaining_invitationsYesChk = false;
$remaining_invitationsNoChk  = true;
if(!empty($show_remaining_invitations)){
$remaining_invitationsYesChk = true;
$remaining_invitationsNoChk  = false;
}
//form create variable
$formInvitationType = array(
'name'      => 'formInvitationTypeSave'.$invitationId,
'id'        => 'formInvitationTypeSave'.$invitationId,
'method'    => 'post',
'class'     => 'form-horizontal',
);

$passwordAccessNo = array(
'name'      => 'passwordAccess'.$invitationId,
'value'     => '0',
'id'        => 'passwordAccessNo'.$invitationId,
'type'      => 'radio',
'class'     => 'ispasswordaccess radioButton',
'passwordaccessid' => $invitationId,
'checked'   => $passwordAccessNoChk,
);    

$passwordAccessYes = array(
'name'      => 'passwordAccess'.$invitationId,
'value'     => '1',
'id'        => 'passwordAccessYes'.$invitationId,
'type'      => 'radio',
'class'     => 'ispasswordaccess radioButton',
'passwordaccessid' => $invitationId,
'checked'   => $passwordAccessYesChk,
);  

$password = array(
'name'      => 'password'.$invitationId,
'value'     => $passwordVal,
'id'        => 'password'.$invitationId,
'type'      => 'password',
'class'     => 'medium_input',
);  

$confirmpassword = array(
'name'      => 'confirmpassword'.$invitationId,
'value'     => $passwordVal,
'id'        => 'confirmpassword'.$invitationId,
'type'      => 'password',
'class'     => 'medium_input',
);    

$limit = array(
'name'      => 'limit'.$invitationId,
'value'     => $limitVal,
'id'        => 'limit'.$invitationId,
'type'      => 'text',
'class'     => 'medium_input',
'autocomplete' => 'off',
);  


$limit_invitation = array(
'name'      => 'limit_invitation'.$invitationId,
'value'     => $limit_invitation_val,
'id'        => 'limit_invitation'.$invitationId,
'type'      => 'number',
'step'      => '1',
'class'     => 'small_input dark stepper-input pull-left',
'min'       => '0',
'style'     => 'opacity: 5',
);
$remaining_invitationsNo = array(
'name'      => 'remaining_invitations'.$invitationId,
'value'     => '0',
'id'        => 'remaining_invitationsNo'.$invitationId,
'type'      => 'radio',
'class'     => 'radioButton',
'checked'   => $remaining_invitationsNoChk,
);    

$remaining_invitationsYes = array(
'name'      => 'remaining_invitations'.$invitationId,
'value'     => '1',
'id'        => 'remaining_invitationsYes'.$invitationId,
'type'      => 'radio',
'class'     => 'radioButton',
'checked'   => $remaining_invitationsYesChk,
);
$allow_waiting_listNo = array(
'name'      => 'allow_waiting_list'.$invitationId,
'value'     => '0',
'id'        => 'allow_waiting_listNo'.$invitationId,
'type'      => 'radio',
'class'     => 'radioButton',
'checked'   => $allow_waiting_listNoChk,
);    

$allow_waiting_listYes = array(
'name'      => 'allow_waiting_list'.$invitationId,
'value'     => '1',
'id'        => 'allow_waiting_listYes'.$invitationId,
'type'      => 'radio',
'class'     => 'radioButton',
'checked'   => $allow_waiting_listYesChk,
); 

$show_password = array(
'name'      => 'show_password'.$invitationId,
'id'        => 'show_password'.$invitationId,
'value'     => 'yes',
'class' 	=> 'checkBox show_password_event',
);
?> 
<!--sub menu two-->
<div class="sabPanel <?php echo $invitationName; ?> checkdata_<?php echo $invitationId; ?>  filter_credentials <?php echo $cls; ?>" id="invitationsid_<?php echo $invitationId; ?>" lang="<?php echo $invitationId; ?>">
   <div class="sabPanel_header <?php echo $cls; ?> filter_invitations showHideTitle<?php echo $invitationId;?>"  name="<?php echo str_replace(' ','_',strtolower($invitationName)); ?>" id="invitation_<?php echo $invitationId; ?>" lang="<?php echo str_replace(' ','_',strtolower($invitationName)); ?>" alt="<?php echo $invitationId; ?>"> 
      <?php echo $invitationName; ?>
      <div class="editButtonGroup three pull-right">
         <button class="btn edit_space_details add_edit_login_type" type="button" id="<?php echo $invitationId; ?>" pageaction="duplicate" eventid="<?php echo $eventId; ?>"><span class="large_icon"> <i class="icon-copy"></i> </span></button>
         <button class="btn edit_space_details add_edit_login_type" type="button" id="<?php echo $invitationId; ?>" pageaction="edit" registitle="<?php echo ucwords($invitationName); ?>" regispassword="<?php echo ucwords($invitationPassword); ?>" regisdescrip="<?php echo ucwords($invitationDescription); ?>"><span class="large_icon"> <i class="icon-edit"></i> </span></button>
         <button class="btn red delete_space_details delete_invitation" id="<?php echo $invitationId; ?>" deleteid="<?php echo $invitationId; ?>" pageaction="delete" type="button" ><span class="large_icon"> <i class="icon-delete"></i> </span></button>
      </div>
   </div>
   <div class="sabPanel_contentWrapper sabPanel_contentWrapper_find_<?php echo $invitationId; ?>"  id="collapseinvitation_<?php echo $invitationId; ?>" style="<?php echo $openTab; ?>">
      <?php echo form_open($this->uri->uri_string(),$formInvitationType); ?>
         <div class="panel_content">
            <div class="row">
               <div class="col-3">
                  <div class="labelDiv">
                     <label class="form-label text-right"><?php echo lang('event_regist_regist_password_access'); ?><span class="required" aria-required="true">*</span></label>
                     <span class="info_btn"><span class="field_info xsmall"><?php echo lang('password_access_info'); ?></span></span>
                  </div>
               </div>
               <div class="col-5">
                  <div class="radioDiv">
                      <?php echo form_radio($passwordAccessYes); ?>
                     <label class="form-label pull-left"  for="passwordAccessYes<?php echo $invitationId; ?>"><span><?php echo lang('comm_yes'); ?></span></label>
                  </div>
                  <div class="radioDiv">
                     <?php echo form_radio($passwordAccessNo); ?>
                     <label class="form-label pull-left" for="passwordAccessNo<?php echo $invitationId; ?>"><span><?php echo lang('comm_no'); ?></span></label>
                  </div>
                  <span id="error-dinner_dance6"></span> 
               </div>
            </div>
           
           <div id="password_access_div<?php echo $invitationId; ?>" class="<?php echo $isPassDivshow; ?>"> 
                <div class="row">
                   <div class="col-3">
                      <div class="labelDiv">
                         <label class="form-label text-right"><?php echo lang('event_regist_regist_password'); ?><span class="required" aria-required="true">*</span> </label>
                      </div>
                   </div>
                   <div class="col-5">
                      <?php echo form_input($password); ?>
                      <?php echo form_error('password'.$invitationId); ?>
                      <span id="error-password<?php echo $invitationId; ?>"></span> 
                   </div>
                </div>
                
                <div class="row">
                   <div class="col-3">
                      <div class="labelDiv">
                         <label class="form-label text-right"><?php echo lang('event_regist_regist_confir_pass'); ?><span class="required" aria-required="true">*</span> </label>
                      </div>
                   </div>
                   <div class="col-5">
                        <?php echo form_input($confirmpassword); ?>
                        <?php echo form_error('confirmpassword'.$invitationId); ?>
                      <span id="error-confirmpassword<?php echo $invitationId; ?>"></span> 
                   </div>
                </div>
                
                <div class="row">
                    <div class="col-3">
                        <div class="labelDiv">
                            <label class="form-label text-right"><?php echo lang('show_password');?></label>
                        </div>
                    </div>
                    <div class="col-5">
                        <?php echo form_checkbox($show_password); ?>
                        <label class="form-label mT5" for="show_password<?php echo $invitationId; ?>"></label>                    
                    </div>
                </div>
                
            </div>
            
            <div class="row">
               <div class="col-3">
                  <div class="labelDiv">
                     <label class="form-label text-right"><?php echo lang('event_regist_regist_limit'); ?><span class="required" aria-required="true">*</span> </label>
                     <span class="info_btn"><span class="field_info xsmall"><?php echo lang('tooltip_regist_regist_limit'); ?></span></span>
                  </div>
               </div>
               <div class="col-5">
                    <?php echo form_input($limit); ?>
                    <?php echo form_error('limit'.$invitationId); ?>
                  <span id="error-limit<?php echo $invitationId;?>"></span> 
               </div>
            </div>
            
            
            <div class="row">
               <div class="col-3">
                  <div class="labelDiv">
                     <label class="form-label text-right" for="limit_invitation<?php echo $invitationId; ?>">
                     <?php echo lang('event_limit_per_Invitation'); ?><span class="required" aria-required="true">*</span> </label> 
                     <span class="info_btn"><span class="field_info xsmall"><?php echo lang('event_limit_per_Invitation_info'); ?></span></span>
                  </div>
               </div>
               <div class="col-5">
                    <?php
                        echo form_input($limit_invitation);
                    ?>
                  <span id="error-limit_invitation<?php echo $invitationId;?>" class="w_100 display_inline mT5"></span> 
               </div>
            </div>
            <div class="row">
               <div class="col-3">
                  <div class="labelDiv">
                     <label class="form-label text-right" for="remaining_invitations<?php echo $invitationId; ?>"><?php echo lang('event_show_invitations_remaining'); ?><span class="required" aria-required="true">*</span></label>
                     <span class="info_btn"><span class="field_info xsmall"><?php echo lang('invitation_countdown_info'); ?></span></span>
                  </div>
               </div>
               <div class="col-5">
                  <div class="radioDiv">
                     <?php echo form_radio($remaining_invitationsYes); ?>
                     <label class="form-label pull-left" for="remaining_invitationsYes<?php echo $invitationId; ?>"><span><?php echo lang('comm_yes'); ?></span></label>
                  </div>
                  <div class="radioDiv">
                     <?php echo form_radio($remaining_invitationsNo); ?>
                     <label class="form-label pull-left" for="remaining_invitationsNo<?php echo $invitationId; ?>"><span><?php echo lang('comm_no'); ?></span></label>
                  </div>
                  <span id="error-dinner_dance6"></span> 
               </div>
            </div>
            <div class="row">
               <div class="col-3">
                  <div class="labelDiv">
                     <label class="form-label text-right" for="allow_waiting_list<?php echo $invitationId; ?>"><?php echo lang('event_allow_waiting_list'); ?><span class="required" aria-required="true">*</span></label>
                     <span class="info_btn"><span class="field_info xsmall"><?php echo lang('allow_waiting_list_info'); ?></span></span>
                  </div>
               </div>
               <div class="col-5">
                  <div class="radioDiv">
                    <?php echo form_radio($allow_waiting_listYes); ?>
                     <label class="form-label pull-left" for="allow_waiting_listYes<?php echo $invitationId; ?>"><span><?php echo lang('comm_yes'); ?></span></label>
                  </div>
                  <div class="radioDiv">
                     <?php echo form_radio($allow_waiting_listNo); ?>
                     <label class="form-label pull-left" for="allow_waiting_listNo<?php echo $invitationId; ?>"><span><?php echo lang('comm_no'); ?></span></label>
                  </div>
                  <span id="error-dinner_dance6"></span> 
               </div>
            </div>
            <hr class="panelSeprator">
            </hr>
            
            <?php
              //set invitation array
              if(isset($eventinvitations[$recordCount]) && !empty($eventinvitations[$recordCount])){
                $sendData['invitationData']  = $eventinvitations[$recordCount];
                $sendData['formSectionName']  = 'regisType';
                $this->load->view('form_personal_details_for_form',$sendData);
              }
            ?>
            
           
         </div>
         
         <div class="panel_footer">
            <a href="javascript:void();" class="scrollTopTrg" style=""><img alt="Logo image" src="<?php echo IMAGE; ?>scroll_top.png">
            <?php echo lang('comm_top'); ?>
            </a>
            <div class="pull-right">
                <?php 
                    echo form_hidden('eventId', $eventId);
                    echo form_hidden('formActionName', 'invitationsDetails');
                    echo form_hidden('invitationId', $invitationId);
                    
                    //button show of save and reset
                    $formButton['saveButtonId'] = 'id="'.$invitationId.'"'; // save button id
                    $formButton['showbutton']   = array('save','reset');
                    $formButton['labletext']  = array('save'=>lang('comm_save_changes'),'reset'=>lang('comm_reset'));
                    $formButton['buttonclass']  = array('save'=>'btn-normal btn '.$triggerCls.'','reset'=>'btn-normal btn reset_space reset_form');
                    $this->load->view('common_save_reset_button',$formButton);
                ?>
            </div>
         </div>
      <?php echo form_close(); ?>
   </div>
   <script type="text/javascript">
$(document).ready(function(){ 
  //invitation type details save
  ajaxdatasave('formInvitationTypeSave<?php echo $invitationId; ?>','<?php echo $this->uri->uri_string(); ?>',true,true,false,true,true,'#showhideformdiv<?php echo $invitationId; ?>','#showhideformdiv<?php echo $nextRecordId; ?>',false);
  
  $('.next_step_2').live('click',function(){
    //alert('manish');
    var $inputs = $('#formInvitationTypeSave<?php echo $invitationId; ?> :input');
    $('#formInvitationTypeSave<?php echo $invitationId; ?>').append("<input type='hidden' name='next_save' value='next_save'/>"); 
      var values_invitation = {};
      $inputs.each(function() {
          values_invitation[this.name] = $(this).val();

    });
      //console.log(values_invitation); 
  ajaxdatasave('formInvitationTypeSave<?php echo $invitationId; ?>','<?php echo $this->uri->uri_string(); ?>',false,false,false,false,false,'#showhideformdiv<?php echo $invitationId; ?>','#showhideformdiv<?php echo $nextRecordId; ?>',false,'','','','',values_invitation); 
  
  });
  /* */


});


$("#passwordAccessNo<?php echo $invitationId;?>").on("click", function(e){
    if($(this).prop('checked')){
        $('#password<?php echo $invitationId;?>').val('');
        $('#confirmpassword<?php echo $invitationId;?>').val('');
    }
});


$('#formInvitationTypeSave<?php echo $invitationId;?>').validate({ 
    ignore: [],
    rules: {
			password<?php echo $invitationId;?>: {
				required: false,
			},
			confirmpassword<?php echo $invitationId;?>: {
				equalTo: "#password<?php echo $invitationId;?>",
			},
			limit<?php echo $invitationId;?>: {
				required: true,
				number:true
			},
			limit_invitation<?php echo $invitationId;?>: {
				required: true,
                number:true
			},
		},
		messages: {
            password<?php echo $invitationId;?>:"Password is required",
            confirmpassword<?php echo $invitationId;?>:"Password must be same",
            limit<?php echo $invitationId;?>:{
            required:"Please enter limit.",
			number:"Please enter valid number."},
            limit_invitation<?php echo $invitationId;?>:{
            required:"Please enter numer.",
            number:"Please enter valid number.",
            },
		},
    errorPlacement: function ($error, $element) {
			var name = $element.attr("name");
			$("#error-" + name).append($error);
		}
    });
    
    $("#show_password<?php echo $invitationId;?>").on("click", function(e){
        if($(this).prop('checked')){
            $('#password<?php echo $invitationId;?>').attr('type','text');
            $('#confirmpassword<?php echo $invitationId;?>').attr('type','text');
        }else {
            $('#password<?php echo $invitationId;?>').attr('type','password');
            $('#confirmpassword<?php echo $invitationId;?>').attr('type','password');
        }
    });
    


</script>
<script>
    $(document).click(function(){
        if ($(".panel-heading.opened").length > 0){ 
            $(".panel.event_panel.co_cat.co_cat_corporate").addClass("overFlowVis");
        }else{
            $(".panel.event_panel.co_cat.co_cat_corporate").removeClass("overFlowVis");
        }
    });
</script>  
<script>
<?php if($requiredCheck=='ilMt') { ?>    
    $("document").ready(function() {
    setTimeout(function() {
        $( ".trigger_limit" ).trigger( "click" );
    },500);
});
<?php } ?>
</script>
</div>


