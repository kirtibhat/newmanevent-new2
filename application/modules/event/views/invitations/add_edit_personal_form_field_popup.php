<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$fieldNameValue         = (!empty($eventregistrants->field_name))?$eventregistrants->field_name:'';
$fieldTypeValue         = (!empty($eventregistrants->field_type))?$eventregistrants->field_type:'';
$fieldDefaultValue      = (!empty($eventregistrants->default_value))?json_decode($eventregistrants->default_value):'';
$headerAction           = ($customfieldidval > 0)?'Edit':'Add'; 
$fieldDefaultValue      =  objecttoarray($fieldDefaultValue);
$defaultTitleArr = array( '0' => 'Miss', '1' => 'Ms', '2' => 'Mrs', '3' => 'Mr' );
if(empty($fieldDefaultValue[0])) {
    $fieldDefaultValue = $defaultTitleArr; 
}




$fieldDefaultValueCount = '';
if(!empty($fieldDefaultValue)){
  $fieldDefaultValueCount = count($fieldDefaultValue);
}
$isTextChecked          = true;
$isSelectChecked        = false;
$isRadioChecked         = false;
$isFileChecked          = false;
$isSelectListDiv        = 'dn';
if(!empty($fieldNameValue)){
  if($fieldTypeValue=="selectbox"){
    $isSelectChecked    = true;
    $isSelectListDiv    = '';
  }elseif($fieldTypeValue=="radio"){
    $isRadioChecked     = true;
    $isSelectListDiv    = '';
  }elseif($fieldTypeValue=="file"){
    $isFileChecked      = true;
  }elseif($fieldTypeValue=="text"){
    $isTextChecked      = true;
  }
}
$formAddCustomeField = array(
    'name'          => 'formAddCustomeField',
    'id'            => 'formAddCustomeField',
    'method'        => 'post',
    'class'         => 'wpcf7-form ',
);
$fieldName = array(
    'name'          => 'fieldName',
    'value'         => $fieldNameValue,
    'id'            => 'fieldName',
    'type'          => 'text',
    'class'         => 'xxlarge_input trim_check keypresseventinvitation',
    'autocomplete'  => 'off',
);
$fieldTypeText = array(
    'name'          => 'fieldType',
    'value'         => 'text',
    'id'            => 'fieldTypeText',
    'type'          => 'radio',
    'class'         => 'radioButton fieldtype',
    'required'      => '',
    'autocomplete'  => 'off',
    'checked' => $isTextChecked,
);
$fieldTypeSelect = array(
    'name'          => 'fieldType',
    'value'         => 'selectbox',
    'id'            => 'fieldTypeSelect',
    'type'          => 'radio',
    'class'         => 'radioButton fieldtype',
    'checked'       => $isSelectChecked,
);  
$fieldTypeRadio = array(
    'name'          => 'fieldType',
    'value'         => 'radio',
    'id'            => 'fieldTypeRadio',
    'type'          => 'radio',
    'class'         => 'radioButton fieldtype',
    'checked'       => $isRadioChecked,
);
$fieldTypeFile = array(
    'name'          => 'fieldType',
    'value'         => 'file',
    'id'            => 'fieldTypeFile',
    'type'          => 'radio',
    'class'         => 'radioButton fieldtype',
    'checked'       => $isFileChecked,
);
$selectedField = array(
    'name'          => 'selectedField',
    'value'         => $fieldDefaultValueCount,
    'id'            => 'selectedField',
    'type'          => 'text',
    'class'         => 'width_50px height_27 ft_16 clr_425968 spinner_selectbox',
    'disabled'      => '',
    'min'           => '0',
);  
$fieldRegistantId = array(
    'name'          => 'fieldRegistantId',
    'value'         => $registrantidval,
    'id'            => 'fieldRegistantId',
    'type'          => 'hidden',
);
$customFieldId = array(
    'name'          => 'customFieldId',
    'value'         => $customfieldidval,
    'id'            => 'customFieldId',
    'type'          => 'hidden',
);      
$parentFormId = array(
    'name'          => 'parentFormId',
    'value'         => $parentFormId,
    'id'            => 'parentFormId',
    'type'          => 'hidden',
);  
$insertFormId = array(
    'name'          => 'formId',
    'value'         => $formId,
    'id'            => 'formId',
    'type'          => 'hidden',
);      
?>
 <div id="add_personal_form_field_popup" class="modal fade addField_popup in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
         <div class="modal-dialog">
            <div class="modal-content">
               <div class="modal-header">
                  <h4 class="modal-title"><?php echo $headerAction; ?> Custom Field</h4>
               </div>
               <?php  echo form_open($this->uri->uri_string(),$formAddCustomeField); ?>
                   <div class="modal-body">
                      <div class="modal-body-content">
                        
                        <div class="row">
                            <div class="labelDiv pull-left">
                               <label class="form-label text-right" for="field_name"><?php echo lang('event_custom_form_field_name'); ?><span aria-required="true" class="required">*</span></label>
                            </div>
                            <?php echo form_input($fieldName); ?>			
                            <span id="error-fieldName"></span>
                         </div><br/>
                         
                         <div class="row">
                            <div class="labelDiv pull-left">
                               <label class="form-label text-right" for="f_type"><?php echo lang('event_custom_form_field_type'); ?></label>
                            </div>
                            <div class="radioDiv w_100">
                                <?php echo form_radio($fieldTypeText); ?> 
                               <label class="form-label pull-left" for="fieldTypeText"><span><?php echo lang('event_custom_form_field_text'); ?></span></label>
                            </div>
                            <div class="radioDiv w_100">
                               <?php echo form_radio($fieldTypeSelect); ?>
                               <label class="form-label pull-left" for="fieldTypeSelect"><span><?php echo lang('event_custom_form_field_ddm'); ?></span></label>
                            </div>
                            <div class="radioDiv w_100">
                               <?php echo form_radio($fieldTypeRadio); ?>
                               <label class="form-label pull-left" for="fieldTypeRadio"><span><?php echo lang('event_custom_form_field_radio'); ?></span></label>
                            </div>
                            <div class="radioDiv w_100">
                               <?php echo form_radio($fieldTypeFile); ?>
                               <label class="form-label pull-left" for="fieldTypeFile"><span><?php echo lang('event_custom_form_field_file'); ?></span></label>
                            </div>
                            <span id="error-link_name"></span>
                         </div>
                         
                         <div id="show_dropdown_feature" class="<?php echo $isSelectListDiv; ?>">
                                
                                <div class="row clearFix">
                                    <div class="labelDiv pull-left w_100">
                                       <label class="form-label text-right"  for="custom_field_qty"><?php echo lang('event_custom_form_quantity'); ?>
                                       <span class="info_btn"><span class="field_info xsmall"><?php echo lang('event_custom_form_quantity'); ?></span></span></label>
                                    </div>
                                    <!--
                                       <input type="number" min="1" name="fieldName" value="<?php echo (!empty($fieldDefaultValueCount)) ? $fieldDefaultValueCount : '1'; ?>" id="fieldName"  class="small_input trim_check step-personal" autocomplete="off" >	-->
                                       
                                       <div class="stepper ">
                                            <input id="custom_field_qty_personal"  class="small_input dark stepper-input pull-left" type="number" readonly  required="" value="<?php echo (!empty($fieldDefaultValueCount)) ? $fieldDefaultValueCount : '1'; ?>" name="field_qty">
                                            <span class="small_icon stepper-step "> 
                                            <i class="icon-uparrow step-personal up"></i>
                                            <i class="icon-downarrow step-personal down"></i>
                                            </span>
                                            <!--<span class="small_icon stepper-step step-personal down"> </span>-->
                                        </div>		
                                    <span id="error-link_name"></span>
                                 </div>
                                
                                <div class="row-fluid default_value_list_personal">      
                                    <?php 
                                    //echo '<pre>';
                                    //print_r($fieldDefaultValue[0]);
                                     if(!empty($fieldDefaultValueCount)){               
                                        foreach($fieldDefaultValue as $key=>$value){
                                          $i = $key+1;
                                          //if(!empty($value)) {
                                    ?>  
                                        <div class="row clearFix">
                                            <div class="labelDiv pull-left">
                                               <label class="form-label text-right"
                                               for="field_<?php echo $key; ?>" id="defaultLblValue_<?php echo $key; ?>"
                                               >Field <?php echo $i; ?><!--<span aria-required="true" class="required">*</span>--></label>
                                            </div>
                                           
                                            <input type="text" value="<?php echo $value; ?>" id="defaultValue_<?php echo $key; ?>" name="defaultValue[]" class="xxlarge_input trim_check" data-parsley-error-message="'.lang('common_field_required').'" data-parsley-error-class = "custom_li"  data-parsley-trigger="keyup" >
                                            <span id="error-link_name"></span>
                                        </div>
                                    <?php
                                           // }
                                        }
                                    }else{ ?>
                                         <div class="row clearFix">
                                            <div class="labelDiv pull-left">
                                               <label class="form-label text-right" for="field_1"><?php echo lang('event_custom_form_field_1'); ?>
                                               <!--<span aria-required="true" class="required">*</span></label>-->
                                            </div>
                                            <input type="text" value="" id="defaultValue_1" name="defaultValue[]"  class="xxlarge_input trim_check" data-parsley-error-class = "custom_li"  data-parsley-trigger="keyup" />	
                                            <span id="error-link_name"></span>
                                        </div>
                                    <?php } ?>
                                </div>
                        </div>
                      </div>
                      <div class="modal-footer">
                         <div class="pull-right">
                            <?php
                                echo form_hidden('eventId', $eventId);
                                echo form_hidden('registrantId', $registrantidval);
                                echo form_input($customFieldId);
                                echo form_input($parentFormId);
                                echo form_input($insertFormId);
                                echo form_hidden('form_id', $formId); // 2 for Corporate Application Details 
                                //echo form_hidden('formActionName', 'masterPersonalDetails');
                                echo form_hidden('formActionName', $formAction);
                                $extraCancel    = 'class="popup_cancel btn-normal btn" data-dismiss="modal" ';
                                $extraSave  = 'class="saveCustomeField btn-normal btn" ';
                                
                                echo form_submit('cancel',lang('comm_cancle'),$extraCancel);   
                                echo form_submit('save',lang('comm_save'),$extraSave);                 
                            ?>
                         </div>
                      </div>
                   </div>
                </form>
            </div>
         </div>
      </div>
<script>
$(document).ready(function(){     
   
$('#formAddCustomeField').validate({ 
		rules: {
			fieldName: {
				required: true,
			},
		},
		
		messages: {
            required : "This is required field",
		},
		errorPlacement: function ($error, $element) {
			var name = $element.attr("name");
			$("#error-" + name).append($error);
		}
});
});

$(document).ready(function(){
    $('.keypresseventinvitation').keypress(function(event){
        if (event.which == 13) { 
            event.preventDefault();
           $('.saveCustomeField').trigger('click');
        }
    });    
});   
</script>
