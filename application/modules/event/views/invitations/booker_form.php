<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$registrantId       = '0';
$bookerDetailsFrm   = 'booker_details';
$formBookerDetails  = array(
'name'          => 'formbookerdetails',
'id'            => 'formbookerdetails',
'method'        => 'post',
'class'         => '',
);

$fieldsmastervalue = fieldsmastervalue();
$emailfield = array(
'value'         => '',
'id'            => 'emailbookerfield',
'type'          => 'email',
'placeholder'   => 'Mandatory',
'readonly'      => '',
'class'         => 'medium_input',
);  

$openForm       = ( $this->uri->segment(3) =='' && $this->input->get('dAtaiD') =='' && $this->input->get('req') == '' && $this->input->get('q') == '') ? 'display:block;' : '';
$openClass      = ( $this->uri->segment(3) =='' && $this->input->get('dAtaiD') =='' && $this->input->get('req') == '' && $this->input->get('q') == '') ? 'open' : '';
?>

<div class="panel <?php echo $openClass; ?> open" id="<?php echo $bookerDetailsFrm; ?>">
   <div class="panel_header " ><?php echo lang('booker_details'); ?></div>
   <div class="panel_contentWrapper" id="showhideformdiv<?php echo $bookerDetailsFrm; ?>" style="<?php echo $openForm;  ?>">
      <div class="infobar">
        <p class="dn" id="booker_form_info"><?php echo lang('booker_form_info'); ?></p>
        <span class="info_btn" onclick="showHideInfoBar('booker_form_info');"></span>
      </div>
      <!--sub menu one-->
      <?php echo form_open($this->uri->uri_string(),$formBookerDetails); ?>
         <div class="panel_content">
            
             <?php 
                if(!empty($bookerformfielddata)){
                    foreach($bookerformfielddata as $formfield){
                    $fieldCreateBy = $formfield->field_create_by;
                    if($fieldCreateBy=="1"){    
                    ?>
                          <?php
                            
                            if($formfield->field_name=="email"){ 
                          ?>
                           <div class="row">
                               <div class="col-3">
                                  <div class="labelDiv">
                                     <label class="form-label text-right">Email</label>
                                     <span class="info_btn"><span class="field_info xsmall"><?php echo lang('booker_email_field_info') ?></span></span>
                                  </div>
                               </div>
                               <div class="col-5">
                                     <?php 
                                      $fieldName= 'field_'.$formfield->id;  
                                      echo form_input($emailfield);
                                      $fieldName= 'field_'.$formfield->id;  
                                      echo form_hidden($fieldName, $formfield->field_status);
                                      echo form_hidden('fieldid[]', $formfield->id);
                                    ?>
                                  <span id="error-dinner_dance6"></span> 
                               </div>
                            </div>
               
                          <?php }else {  ?>
               
                           <div class="row">
                               <div class="col-3">
                                  <div class="labelDiv">
                                     <label class="form-label text-right" for="dft_fname" id="masterfieldlbl_<?php echo $formfield->id; ?>"><?php echo ucwords($formfield->field_name); ?></label>
                                  </div>
                               </div>
                               <div class="col-5">
                                    <?php 
                                      $other = 'id="field_'.$formfield->id.'"  class="custom-select medium_select" ';
                                      $fieldName= 'field_'.$formfield->id;  
                                      echo form_dropdown($fieldName, $fieldsmastervalue, $formfield->field_status,$other);  
                                      echo form_hidden('fieldid[]', $formfield->id);
                                    ?>
                                      <?php  if($formfield->is_editable=="1"){ ?> 
                                        
                                           <div class="editButtonGroup pull-left">
                                            <a href="javascript:void(0);" class="add_personal_form_field btn" invitationid="0" formAction="bookerCustomFieldSave" formId="5" customfieldid="<?php echo $formfield->id; ?>" data-parent-form-id="formbookerdetails" id="add_field" > 
                                               <span class="medium_icon"> <i class="icon-edit"></i> </span>
                                            </a>               
                                            <span id="error-dinner_dance6"></span> 
                                           </div> 
                                            
                                    <?php } ?> 
                               </div>
                            </div>
                              
                          <?php } // end else ?>
                          
                      
                  <?php } } } ?>
                
                
                <?php  if(!empty($bookerformfielddata)){
                    foreach($bookerformfielddata as $key=> $formfield){
                    
                    $fieldCreateBy = (isset($formfield->field_create_by) && $formfield->field_create_by=="0")?$formfield->field_create_by:NULL;
                    if($fieldCreateBy=="0"){ ?>
                        
                        <div class="row" id="masterformregistrant_<?php echo $key; ?>">
                           <div class="col-3">
                              <div class="labelDiv">
                                 <label class="form-label text-right" id="masterfieldlbl_<?php echo $formfield->id; ?>"><?php echo ucwords($formfield->field_name); ?></label>
                              </div>
                           </div>
                           <div class="col-5">
                               
                               <?php
                                  echo form_hidden('fieldid[]', $formfield->id);
                                  $other    = 'id="field_'.$formfield->id.'" class="custom-select medium_select" ';
                                  $fieldName  = 'field_'.$formfield->id;
                                  echo form_dropdown($fieldName, $fieldsmastervalue, $formfield->field_status,$other);  
                                ?>
                                <div class="editButtonGroup pull-left">

                                <a class=" btn add_personal_form_field " deleteid="<?php echo $formfield->id; ?>" invitationid="0" formAction="bookerCustomFieldSave" formId="4" customfieldid="<?php echo $formfield->id; ?>" data-parent-form-id="formbookerdetails">
                                    <span class="medium_icon"> <i class="icon-edit"></i> </span>
                                </a>
                                
                                <a class="btn red delete_personal_form_field " deleteid="<?php echo $formfield->id; ?>">
                                    <span class="medium_icon"> <i class="icon-delete"></i> </span>
                                </a>
                                <span id="error-dinner_dance6"></span> 
                                </div>
                           </div>
                        </div>
                    
                  <?php } } } ?>

                <div class="row custom_field_div" id="custom_field_div"></div>
            
            
            <div class="row mT30">
               <div class="col-1">
               </div>
               <div class="col-5">
                  <a class="btn-icon btn-icon-large btn add_personal_form_field" invitationid="0" formAction="bookerCustomFieldSave" formId="5" customfieldid="0" data-parent-form-id="formbookerdetails">
                  <span class="medium_icon"><i class="icon-add"></i></span><div class="font-small"><?php echo lang('event_registration_custom_field'); ?></div></a>
               </div>
            </div>
         
         </div>
         <div class="panel_footer">
            <div class="pull-right">
                <?php 
                    echo form_hidden('eventId', $eventId);
                    echo form_hidden('registrantId', $registrantId);
                    echo form_hidden('formActionName', 'bookerDetails');

                    //button show of save and reset
                    $formButton['showbutton']   = array('save','reset');
                    $formButton['labletext']  = array('save'=>lang('comm_save_changes'),'reset'=>lang('comm_reset'));
                    $formButton['buttonclass']  = array('save'=>'btn-normal btn','reset'=>'btn-normal btn reset_space reset_form');
                    $this->load->view('common_save_reset_button',$formButton);
                ?>
            </div>
      
         </div>
      <?php echo form_close(); ?>
   </div>
</div>
