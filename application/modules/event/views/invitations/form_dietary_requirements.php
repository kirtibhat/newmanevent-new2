<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$dietaryRequirementsValue   = (!empty($eventdetails->dietary_requirments))?$eventdetails->dietary_requirments :'0';
$formdietaryrequirements = array(
'name'   => 'formdietaryrequirements',
'id'     => 'formdietaryrequirements',
'method' => 'post',
'class'  => 'wpcf7-form',
);
$dietaryrequirements = array(
'name'  => 'dietaryrequirements',
'value' => '1',
'id'    => 'dietaryrequirements',
'type'  => 'checkbox',
'class' => '',
);

if(!empty($dietaryRequirementsValue) && $dietaryRequirementsValue > 0){
    $dietaryrequirements['checked']=true;
}   
$formAddTypeofDietary = array(
'name'   => 'formAddTypeofDietary',
'id'     => 'formAddTypeofDietary',
'method' => 'post',
'class'  => 'wpcf7-form',
);

$dietaryName = array(
'name'  => 'dietaryName',
'value' => '',
'id'    => 'dietaryName',
'type'  => 'text',
'class' => 'xxlarge_input trim_check keypresseventdietary',
'autocomplete' => 'off',
);
?>
<div class="panel" id="panel0">
      <div class="panel_header"><?php echo lang('event_dietary_requirements_title'); ?></div>
      <div class="panel_contentWrapper" id="showhideformdivdieteryrequirement">
      <div class="infobar">
        <p class="dn" id="event_dietary_info">The Access ID# is a unique identifier assigned to every registrant using this site. Use it to access, edit, cancel, pay and rebook items in your registration.</p>
        <span class="info_btn" onclick="showHideInfoBar('event_dietary_info');"></span>
      </div>
      	<!--sub menu one-->
        <?php echo form_open($this->uri->uri_string(),$formdietaryrequirements); ?>
              <div class="panel_content">
              <div class="row requirement_ul">
                <?php if(!empty($eventdietary)){ 
                    foreach($eventdietary as $dietary){ ?>
                    <div class="col-2 mL35" id="row_<?php echo $dietary->id; ?>">
                      <div class="inputFileWrapper">
                        <div class="crosslable">
                            <span><?php echo $dietary->dietary_name; ?></span>
                            <?php if($dietary->is_undeletable==0){ ?>
                                <span class="small_icon delete_dietary"  id="<?php echo $dietary->id; ?>">
                                    <img src="<?php echo IMAGE; ?>crox.png">
                                </span>
                            <?php }else{ ?>
                            <span></span>
                            <?php } ?>
                        </div>
                      </div>
                    </div>
                <?php }  }  ?>
               
              
                </div>
              <div class="row mT30">
				<div class="col-5">
					<a  href="javascript:void(0)" class="btn-icon btn-icon-large btn add_dietary_type">
						<span class="medium_icon"><i class="icon-add"></i></span><div class="font-small"><?php echo lang('event_dietary_add_dietary_req_title'); ?></div>
                    </a>
				</div>
			</div>
              </div>
              <div class="panel_footer"> 
                <div class="pull-right">
                    <?php 
                        echo form_hidden('eventId', $eventId);
                        echo form_hidden('formActionName', 'dietaryrequirements');
                        $extraSave  = 'class="submitbtn_cus savechange_regd" ';
                    ?>
                  <button name="form_reset" type="button" class="btn-normal btn reset_space ">Clear</button>
                  <input type="button" name="save" value="Save" class="btn-normal btn savedietary">
                </div>
              </div>
            <?php echo form_close(); ?> 
    </div>
</div>

<!--end of pop1-->
<div class="modal fade addField_popup" id="add_type_dietary_popup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
     <div class="modal-dialog">
        <div class="modal-content">
           <div class="modal-header">
              <h4 class="modal-title"><?php echo lang('event_dietary_popup_add_field_title'); ?></h4>
           </div>
            <?php  echo form_open($this->uri->uri_string(),$formAddTypeofDietary); ?>
            
               <div class="modal-body">
                  <div class="modal-body-content">
                     <div class="row">
                        <div class="labelDiv pull-left">
                           <label class="form-label text-right">Name<span aria-required="true" class="required">*</span></label>
                           <span class="info_btn"><span class="field_info xsmall"><?php echo lang('event_dietary_field_name'); ?></span></span>
                        </div>
                        <?php echo form_input($dietaryName); ?>
                        <span id="error-dietaryName"></span>
                     </div>
                  </div>
                  <div class="modal-footer">
                     <div class="pull-right">
                        <?php 
                            echo form_hidden('eventId', $eventId);
                            echo form_hidden('formActionName', 'dietaryrequirements');

                            $extraCancel    = 'class="btn-normal btn" data-dismiss="modal" ';
                            $extraSave  = 'class="saveDietaryCustomeField btn-normal btn" ';
                            
                            echo form_submit('cancel',lang('comm_cancle'),$extraCancel); 
                            echo form_submit('save',lang('comm_save'),$extraSave);
                        ?> 
                     </div>
                  </div>
               </div>
            <?php echo form_close(); ?>    
        </div>
     </div>
    </div>
<!--end of pop2-->
<!-- Modal -->
<script>
$('.savedietary').click(function(){
    $( "#showhideformdivdieteryrequirement" ).removeAttr( "style" );
});
$(document).ready(function(){     
    $('#formAddTypeofDietary').validate({ 
        rules: {
            dietaryName: {
                required: true,
            },
        },
        
        messages: {
            required : "This is required field",
        },
        errorPlacement: function ($error, $element) {
            var name = $element.attr("name");
            $("#error-" + name).append($error);
        }
    });
});

$(document).ready(function(){
    $('.keypresseventdietary').keypress(function(event){
        if (event.which == 13) { 
            event.preventDefault();
           $('.saveDietaryCustomeField').trigger('click');
        }
    });    
});   
</script>
