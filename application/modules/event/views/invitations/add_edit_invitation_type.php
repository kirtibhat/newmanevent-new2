<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$formTypeofInvitation = array(
'name'          => 'formInvitationType',
'id'            => 'formInvitationType',
'method'        => 'post',
'class'         => 'wpcf7-form formInvitationType formInvitationTypeCheck',
);
$invitationName = array(
'name'          => 'invitationName',
'value'         => '',
'id'            => 'invitationName',
'type'          => 'text',
'class'         => 'xxlarge_input trim_check keypresseventinvitationtype',
'autocomplete'  => 'off',
);
$invitationTypeId = array(
'name'          => 'invitationTypeId',
'value'         => '0',
'id'            => 'invitationTypeId',
'type'          => 'hidden',
);  
?>
<div class="modal fade addField_popup" id="add_type_login_popup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
 <div class="modal-dialog">
    <div class="modal-content">
       <div class="modal-header">
          <h4 class="modal-title" id="registrantTypeTitle"><?php echo lang('event_invitation_add'); ?> <?php echo " ".lang('event_invitation_addtype'); ?></h4>
       </div>
        <?php  echo form_open(base_url('event/addediteventInvitation'),$formTypeofInvitation); ?>
           <div class="modal-body">
              <div class="modal-body-content">
                 <div class="row">
                    <div class="labelDiv pull-left">
                       <label class="form-label text-right" for="ctype">
                        <?php echo lang('event_regis_name'); ?>
                        <span aria-required="true" class="required">*</span>
                        <span class="info_btn"><span class="field_info xsmall"><?php echo lang('add_invitation_info'); ?></span></span>
                        </label>
                    </div>
                    <?php echo form_input($invitationName); ?>
                    <span id="error-invitationName"></span>
                 </div>
              </div>
              <div class="modal-footer">
                 <div class="pull-right">
                    <?php 
                        echo form_input($invitationTypeId); 
                        echo form_hidden('eventId', $eventId);
                        $extraCancel  = 'class="btn-normal btn " data-dismiss="modal" ';
                        $extraSave    = 'class="btn-normal btn saveinvitationtype" ';
                        
                       
                        echo form_submit('cancel',lang('comm_cancle'),$extraCancel);
                        echo form_submit('save',lang('comm_save'),$extraSave);
                      ?>
                 </div>
              </div>
           </div>
        <?php echo form_close(); ?>
    </div>
 </div>
</div>
<script>
$(document).ready(function(){
    $('.keypresseventinvitationtype').keypress(function(event){
        if (event.which == 13) { 
            event.preventDefault();
           $('.saveinvitationtype').trigger('click');
        }
    });    
});   
</script>
