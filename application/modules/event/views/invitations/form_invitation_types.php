<?php
//Get url param for required check for confirm details page
$requiredCheck              =  $this->input->get('req');
$invitationTypeDataId       =  $this->input->get('dAtaiD');
//$invitationtypedetailsid    =  $this->input->get('q');
$invitationtypedetailsid    =  '';
$dataId = !empty($invitationTypeDataId) ? $invitationTypeDataId : ''; 
$datainvitationtypedetailsid = !empty($invitationtypedetailsid) ? $invitationtypedetailsid : ''; 
if($requiredCheck=='ilMt'){ $isOpenTab =  'in'; $isCls = 'opened'; } else { $isOpenTab =  ''; $isCls = '';}
if(!empty($invitationTypeDataId) || !empty($invitationtypedetailsid)){ $openTab =  'in'; $cls = 'opened'; } else { $openTab =  ''; $cls = ''; }

$where = array('event_id'=>$eventId);
$eventinvitationsdata = getDataFromTabel('event_invitations','*', $where,'','invitations_order, id','ASC');
if(empty($eventinvitationsdata)) {
    $class_pd = '';
}else {
    $class_pd='';
}

$openForm       = ( $this->uri->segment(3) =='' && $this->input->get('dAtaiD') =='') ? '' : 'display:block;';
$openClass      = ( $this->uri->segment(3) =='' && $this->input->get('dAtaiD') =='' ) ? '' : 'open';      
                
?>
<div class="row mT30 ">
     <div class="col-5 mL30">
        <a class="btn-icon btn-icon-large btn add_edit_login_type" pageaction="add">
            <span class="large_icon"><i class="icon-add"></i></span>
            <?php echo lang('event_invitation_type_free'); ?>
        </a>
     </div>
</div>
<div class="panel open" id="panel0">
     <div class="panel_header <?php echo $cls; ?> <?php echo $isCls; ?>"><?php echo lang('invitation_type_free'); ?>
        <input type="text" name="filterinvitation" value="" id="filterinvitation" placeholder="" class="medium_input pull-right filterinvitation" data-key="">
     </div>
     <div class="panel_contentWrapper credential_filter_div invitation_filter_div <?php echo  ' '.$isOpenTab;?>  <?php echo $class_pd; ?> accordion_content" style="display:block" id="focusdv">
         <?php                
                //$where = array('event_id'=>$eventId);
                //$eventinvitationsdata = getDataFromTabel('event_invitations','*', $where,'','invitations_order, id','ASC');
                if(!empty($eventinvitationsdata)){
                  unset($recordId);
                  foreach($eventinvitationsdata as $invitations){
                    $recordId[] =  $invitations->id;
                  }
                  $recordCount = 0;
                  $typeCount = 0;
                  foreach($eventinvitationsdata as $invitations){
                    $nextId                         = $recordCount+1;        
                    $sendData['invitationName']     = $invitations->invitation_name;     
                    $sendData['formdata']           = $invitations; 
                    $sendData['nextRecordId']       = (isset($recordId[$nextId]))?$recordId[$nextId]:'0';
                    $sendData['recordCount']        = $recordCount; 
                    $sendData['invitationDataId']   = $dataId; 
                    $sendData['invitationtypedetailsid']  = $datainvitationtypedetailsid; 
                    $sendData['typeCount']          = $typeCount; 
                    // dynamic load all invitation forms
                    $this->load->view('form_invitation_type_details',$sendData);
                    $recordCount++;
                    $typeCount++;
                  }
                }else {
                    echo "<div class='panel_content'><p class=''>".lang('invitation_type_no_added_message')."</p></div>";
                }
                     // end if 
          ?>
       
     </div>
</div>
                  

<script>
<?php if(( $dataId > 0 || $datainvitationtypedetailsid > 0 )  || ($requiredCheck!='' && $requiredCheck!='nOinV') ) { ?>
$("document").ready(function() {
    setTimeout(function() {    
        var invitationId = '<?php echo $dataId; ?>';  
        //window.location.hash = '#invitation_'+invitationId;
        window.location.hash = '#focusdv';
   },1000);
});
<?php } ?>
</script>
