<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$requiredCheck  =  $this->input->get('req');
$openTab        = !empty($requiredCheck) ? 'in' : '';
$tempHidden = array(
  'type'        => 'hidden',
  'name'        => 'tempHidden',
  'id'          => 'tempHidden',
  'value'       => '',
);
echo form_input($tempHidden);

$this->load->view('event_menus'.$this->config->item('fileSuffix'));  
?>
<div class="page_content">
    <div class="container">
        <div class="row">
            <div class="col-9">
            <div class="col-5"></div>
            <?php 
            $this->load->view('booker_form');

            $this->load->view('master_form_personal_details_for_form');
                
            $this->load->view('form_dietary_requirements');          

            $this->load->view('add_edit_invitation_type');

            $this->load->view('form_invitation_types');

            $buttonData['viewbutton'] = array('next','preview','back');
            $buttonData['linkurl']  = array('back'=>base_url('event/eventdetails'),'next'=>base_url('event/customizeforms'),'preview'=>'');
            $this->load->view('common_back_next_buttons',$buttonData);
            ?>      
            </div>
        </div>
    </div> 
</div>
<div id="tooltip_wrapper"></div>
<script type="text/javascript">
	var event_msg_type_registration_deleted = '<?php echo lang('event_msg_type_registration_deleted') ?>';
	var eventId = '<?php echo $eventId ?>';
	var event_msg_custom_field_deleted = '<?php echo lang('event_msg_custom_field_deleted') ?>';
	var gstRate = parseInt('<?php echo (!empty($eventdetails->gst_rate))?$eventdetails->gst_rate:"0"; ?>');
	var common_field_required = '<?php echo lang("common_field_required"); ?>';
	$('.collapsed').click(function(){
		$('#collapse').css('height','auto');
	 });
<?php if(!empty($requiredCheck) && $requiredCheck=='nOinV') { ?>    
    $("document").ready(function() {
    setTimeout(function() {
        $( ".add_edit_login_type" ).trigger( "click" );
    },500);
});
<?php } ?>
$( "#filterinvitation" ).click(function( event ) {
  event.stopPropagation();
});
</script>
