<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 |--------------------------------------------------------------------------|
 | This Field define for temparay open popup by add section id              |
 | and edit record id                                                       |   
 |--------------------------------------------------------------------------|
*/
 
$tempHidden = array(
    'type' => 'hidden',
    'name' => 'tempHidden',
    'id' => 'tempHidden',
    'value' => '',
);

echo form_input($tempHidden);

$this->load->view('event/event_menus');

$catFilterList = array(); 
if(!empty($registrantCategory)){
    foreach($registrantCategory as $value){
        $catFilterList[$value->category_id][$value->category][$value->registrant_id] = $value->registrant_name; 
    }//end loop
}
?>


<div class="row-fluid-15">
        <div id="page_content" class="span9">
            <div id="accordion" class="panel-group">
                <p>
                    <a href="javascript:void(0)" class="btn medium add_presenter add_big_btn event_btn_margin add_edit_side_event" actionform="add" sideeventid="0"><?php echo lang('event_add_side_event'); ?></a>
                </p>
                <?php 
                    if(!empty($sideeventdata)){
                        
                        //prepare array data for current and next form hide/show  
                        foreach($sideeventdata as $sideevent){
                            $recordId[] =  $sideevent->id;
                        }
                        
                        //show side event forms 
                        $rowCount = 0;  
                        foreach($sideeventdata as $sideevent){
                            $nextId = $rowCount+1;
                            $sendData['sideevent']          =$sideevent; 
                            $sendData['catFilterList']      =$catFilterList; 
                            $sendData['nextRecordId']   = (isset($recordId[$nextId]))?$recordId[$nextId]:'0';
                            // daynamic load all registrant forms
                            $this->load->view('sideevents/form_side_events_setup',$sendData);
                            $rowCount++;
                        }
                    }
                ?>
                
            </div>
            <!--end of accordion content-->
            
            <div class="page_btnwrapper">
                <?php 
                    //next and previous button
                    $buttonData['viewbutton'] = array('back','next','preview');
                    $buttonData['linkurl']  = array('back'=>base_url('event/programdetails'),'next'=>base_url('event/setupextras'),'preview'=>'');
                    $this->load->view('common_back_next_buttons',$buttonData);
                ?>
            </div>
            <!--end of page btn wrapper-->

    </div>
    <!--end of page content-->
</div>
<!--end of row-->  


<!--start custom tooltip-->
<div id="tooltip_wrapper"></div>
<!--end custom tooltip-->
 
<script type="text/javascript" language="javascript">
$(document).ready(function(){
    $(document).on('click','.registrantCat',function(){
        // For set collapse hight
            $(".accordion_content").css("height",'auto');
            
            var divId = $(this).attr('data-append-id');
            if($(this).prop("checked")){
                $("#category_list_"+divId).show();
            }else{
                $("#category_list_"+divId).hide();
            }
    });

    $(document).on('click','.complementary',function(){
        $(".accordion_content").css("height",'auto');
        var appendId  = $(this).attr('appendId');
        var getchecked = $(this).prop("checked");      
        if($(this).val()==1){
            $("#complementarydiv"+appendId).hide('slow');

            $('#total_price_comp'+appendId).attr('data-parsley-required', 'false');
            $('#gst_included_total_price_comp'+appendId).attr('data-parsley-required', 'false');

            $('#total_price_comp'+appendId).removeAttr('required');
            $('#gst_included_total_price_comp'+appendId).removeAttr('required'); 
        } else if($(this).val()==0) {   
            $("#complementarydiv"+appendId).show('slow');            
            $('#total_price_comp'+appendId).attr('required','');
            $('#gst_included_total_price_comp'+appendId).attr('required','');
        }
    });

    $(document).on('click','.allowEarlyBirdComp',function(){
        $(".accordion_content").css("height",'auto');
        var appendId  = $(this).attr('appendId');
        var getchecked = $(this).prop("checked");      
        if($(this).val()==0){
            $("#allowEarlyBirdCompDiv"+appendId).hide('slow');

            $('#earlyBird_limit'+appendId).attr('data-parsley-required', 'false');
            $('#total_early_bird_price_comp'+appendId).attr('data-parsley-required', 'false');
            $('#gst_included_early_bird_comp'+appendId).attr('data-parsley-required', 'false');

            $('#earlyBird_limit'+appendId).removeAttr('required');            
            $('#total_early_bird_price_comp'+appendId).removeAttr('required');
            $('#gst_included_early_bird_comp'+appendId).removeAttr('required');   
        } else if($(this).val()==1) {   
            $("#allowEarlyBirdCompDiv"+appendId).show('slow');
                       
            $('#earlyBird_limit'+appendId).attr('required','');           
            $('#total_early_bird_price_comp'+appendId).attr('required','');
            $('#gst_included_early_bird_comp'+appendId).attr('required','');  
        }
    });

    
        
    $(document).on('click','.additional_guests',function(){
        $(".accordion_content").css("height",'auto');
        var appendId  = $(this).attr('appendId');
        var getchecked = $(this).prop("checked");      
        if($(this).val()==1){
            $("#additionalguestsdiv"+appendId).show('slow');

            $('#guests_limit'+appendId).attr('required','');
            $('#guests_limit'+appendId).val(1);
            $('#guests_limit'+appendId).parent().removeClass('stepper');
            $('#total_price_ad_guest'+appendId).attr('required','');
            $('#gst_included_total_price_ad_guest'+appendId).attr('required','');
        } else if($(this).val()==0) {
            $("#additionalguestsdiv"+appendId).hide('slow');
            
            $('#guests_limit'+appendId).removeAttr('required');
            $('#total_price_ad_guest'+appendId).removeAttr('required');
            $('#gst_included_total_price_ad_guest'+appendId).removeAttr('required');
        }
    });


    $(document).on('click','.allowEarlyBirdAdGuest',function(){
        $(".accordion_content").css("height",'auto');
        var appendId  = $(this).attr('appendId');
        var getchecked = $(this).prop("checked");      
        if($(this).val()==0){
            $("#allowEarlyBirdAdGuestDiv"+appendId).hide('slow');

            $('#earlyBirdAdGuest_limit'+appendId).attr('data-parsley-required', 'false');
            $('#total_early_bird_price_ad_guest'+appendId).attr('data-parsley-required', 'false');
            $('#gst_included_total_price_early_bird_ad_guest'+appendId).attr('data-parsley-required', 'false');

            $('#earlyBirdAdGuest_limit'+appendId).removeAttr('required');            
            $('#total_early_bird_price_ad_guest'+appendId).removeAttr('required');
            $('#gst_included_total_price_early_bird_ad_guest'+appendId).removeAttr('required');   
        } else if($(this).val()==1) {   
            $("#allowEarlyBirdAdGuestDiv"+appendId).show('slow');
                       
            $('#earlyBirdAdGuest_limit'+appendId).attr('required','');           
            $('#total_early_bird_price_ad_guest'+appendId).attr('required','');
            $('#gst_included_total_price_early_bird_ad_guest'+appendId).attr('required','');  
        }
    });

    

    $(document).on('click','.complementary_guests',function(){
        $(".accordion_content").css("height",'auto');
        var appendId  = $(this).attr('appendId');
        var getchecked = $(this).prop("checked");      
        if($(this).val()==1){
            $("#complementaryguestsdiv"+appendId).show('slow');
            
            $('#complementary_guests_limit'+appendId).attr('required','');
            $('#complementary_guests_limit'+appendId).val(2);
            $('#complementary_guests_limit'+appendId).parent().removeClass('stepper');
        } else if($(this).val()==0) {
            $("#complementaryguestsdiv"+appendId).hide('slow');
            
            $('#complementary_guests_limit'+appendId).removeAttr('required');
        }
    });

    $(document).on('click','.registrantlisttype',function(){
      $(".accordion_content").css("height",'auto');
      // get text
      var RegistrantType = $(this).parent().find('label').text();
      var sideeventId  = $(this).attr('sideeventid');
      var registrantId = $(this).attr('registrantid');
      var appendId = sideeventId+"_"+registrantId;
      var side_event = '';
      
      var getchecked = $(this).prop("checked");      
      if(getchecked==true && getchecked!=undefined ){
        //alert(appendId);    
        $("#registrantText"+appendId).html(RegistrantType); 
        $("#registrantTypeDetailedForm"+appendId).removeClass('dn');            
        $('#reg_limit'+appendId).attr('required','');  
        $('#RegistaintId'+appendId).val(registrantId);

        $('#total_price_comp'+appendId).attr('required','');
        $('#gst_included_total_price_comp'+appendId).attr('required','');
        $('#total_early_bird_price_comp'+appendId).attr('required','');
        $('#gst_included_early_bird_comp'+appendId).attr('required','');  
      
      } else {
        $("#registrantTypeDetailedForm"+appendId).addClass('dn');    

        $('#reg_limit'+appendId).attr('data-parsley-required', 'false');
        $('#total_price_comp'+appendId).attr('data-parsley-required', 'false');
        $('#gst_included_total_price_comp'+appendId).attr('data-parsley-required', 'false');
        $('#total_early_bird_price_comp'+appendId).attr('data-parsley-required', 'false');
        $('#gst_included_early_bird_comp'+appendId).attr('data-parsley-required', 'false');

        $('#reg_limit'+appendId).removeAttr('required');
        $('#total_price_comp'+appendId).removeAttr('required');
        $('#gst_included_total_price_comp'+appendId).removeAttr('required');
        $('#total_early_bird_price_comp'+appendId).removeAttr('required');
        $('#gst_included_early_bird_comp'+appendId).removeAttr('required');
      } 

    });

    /*-----allow Restrict Event Access for side event-------*/
    $(".restrictEventAccess").click(function(){
        $(".accordion_content").css("height",'auto');
        var sideEventId=$(this).attr('sideEventId');
        if($(this).val()=="1"){            
            $("#registrantCatDiv"+sideEventId).show();
        }else{
            $("#registrantCatDiv"+sideEventId).hide();
            $(".registrantTypeDetailedForm").addClass('dn');
        }
    });

    /*-----allow early bird date for side event-------*/
    $(".isallowearlybirddate").click(function(){
        var getid=$(this).attr('sideeventid');
        var getchecked=$(this).prop('checked');
        if(getchecked==true && getchecked!=undefined ){
            showarea("#is_show_allow_earlybirddate"+getid);
            $(".earlybirdaction"+getid).attr('required',''); 
            $(".earlybirdaction"+getid).removeAttr('disabled');
        }else{
            hidearea("#is_show_allow_earlybirddate"+getid);
            $(".earlybirdaction"+getid).removeAttr('required');
            $(".earlybirdaction"+getid).attr('disabled',''); 
        }
    });


    //allow earlybird selection in registrant type section
    $(".allowEarlybirdselection").click(function(){
        $(".accordion_content").css("height",'auto');
        var getregisid = $(this).attr('sideEventId');
        var getchecked=$(this).prop('checked');
        if(getchecked==true && getchecked!=undefined ){
          showarea("#side_events_early_bird"+getregisid);
          $('.earlyBirdLimit').removeAttr('disabled');
          $('.earlyBirdLimit').attr('required','');
          $('.unitPriceEarlyBird').removeAttr('disabled');
          $('.unitPriceEarlyBird').attr('required','');
        }else{
          hidearea("#side_events_early_bird"+getregisid);
          $('.earlyBirdLimit').removeAttr('required');
          $('.earlyBirdLimit').attr('disabled','disabled'); 
          $('.unitPriceEarlyBird').removeAttr('required');
          $('.unitPriceEarlyBird').attr('disabled','disabled'); 
        }
    });

    //Tick the box if this type of registration is allowed to have Group bookings
    $(".sideEventGroupbookings").click(function(){
        // For set collapse hight
        $(".accordion_content").css("height",'auto');
        var getid=$(this).attr('groupbookingsid');
        var getchecked=$(this).prop('checked');
        if(getchecked==true && getchecked!=undefined ){
          $("#group_bookings_div_"+getid).show('slow');
          $("#sideEventGroupBookingsSize"+getid).parent().removeClass('disabled');
          $("#sideEventGroupBookingsSize"+getid).removeAttr('disabled');
          $("#sideEventGroupBookingsPercent"+getid).removeAttr('disabled');
        }else{
          $("#group_bookings_div_"+getid).hide('slow');
          $("#sideEventGroupBookingsSize"+getid).attr('disabled','disabled'); 
          $("#sideEventGroupBookingsPercent"+getid).attr('disabled','disabled'); 
        }   
    });

    $(document).on('click','.restrictStreamAccess',function(){
        // For set collapse hight
        $(".accordion_content").css("height",'auto');
        var sideEventId = $(this).attr('sideEventId');
        if($(this).val()=="1"){
            $('#registrantCatDiv'+sideEventId).show();
        }else{
            $('#registrantCatDiv'+sideEventId).hide();
        }
    });

    //registrant selection Limit
    $(".registrantSelectionLimit").click(function(){
        var getid=$(this).attr('registrantselectionlimitid');
        
        var getchecked=$(this).prop('checked');
        if(getchecked==true && getchecked!=undefined ){
            showarea("#registrant_selection_limit_div"+getid);
            $("#registrantSelection"+getid).removeAttr('disabled');
        }else{
            hidearea("#registrant_selection_limit_div"+getid);
            $("#registrantSelection"+getid).attr('disabled','disabled'); 
        }   
    });

    //single day registrant selection Limit
    $(".singleRegistrantSelectionLimit").click(function(){
        var getid=$(this).attr('singleregistrantselectionlimitid');
        var getchecked=$(this).prop('checked');
        if(getchecked==true && getchecked!=undefined ){
            showarea("#single_registrant_selection_limit_div"+getid);
            $("#singleregistrantselection"+getid).removeAttr('disabled');
        }else{
            hidearea("#single_registrant_selection_limit_div"+getid);
            $("#singleregistrantselection"+getid).attr('disabled','disabled'); 
        }   
    });

    //registrant Selection
    $(".registrantSelection").click(function(){
        var getid=$(this).attr('registrantselectionid');
        var getchecked=$(this).prop('checked');
        if(getchecked==true && getchecked!=undefined ){
            showarea("#side_event_registraint_list"+getid);
            $("#registrantSelectionLimit"+getid).attr('disabled','disabled'); 
            $(".registrantselection"+getid).attr('required',''); 
        }else{
            hidearea("#side_event_registraint_list"+getid);
            $("#registrantSelectionLimit"+getid).removeAttr('disabled');
            $(".registrantselection"+getid).removeAttr('required');
        }   
    });

    //check registrant details selection popup
    /*$(".checkboxdisable").click(function() {
        var getId = $(this).attr('id');
        if($('#registrantSelection'+getId).is(':disabled')){
            custom_popup('Please selecte type of registrant.',false);
        }
    });*/




    /*-----complimentary checked show price, GST Include, earlybird price etc.-------*/
    $(".sideEventregiscomaplimentary").click(function(){
        var getid=$(this).attr('sideEventregiscomaplimentaryid');
        var getchecked=$(this).prop('checked');
        if(getchecked==true && getchecked!=undefined ){
            hidearea("#compli_checked_div_section"+getid);
            $(".registrantcomplimentary"+getid).removeAttr('required');
        }else{
            showarea("#compli_checked_div_section"+getid);
            $(".registrantcomplimentary"+getid).attr('required',''); 
        }   
    });


    /*-----regitrant listing select if required additiona guest.-------*/
    $(".SideRegisAddiGuests").click(function(){
        var getid=$(this).attr('sideregisaddiguestsid');
        var getchecked=$(this).prop('checked');
        if(getchecked==true && getchecked!=undefined ){
            showarea(".side_regis_addi_guests_div"+getid);
            $(".registrantselectionaddi"+getid).attr('required',''); 
        }else{
            hidearea(".side_regis_addi_guests_div"+getid);
            $(".registrantselectionaddi"+getid).removeAttr('required');
        }   
    });


    /*-----regitrant listing select if required complimentary guest.-------*/
    $(".SideRegisCompliGuests").click(function(){
        var getid=$(this).attr('sideregiscompliguestsid');
        var getchecked=$(this).prop('checked');
        if(getchecked==true && getchecked!=undefined ){
            showarea(".side_regis_compli_guests_div"+getid);
            $(".registrantselectioncompli"+getid).attr('required',''); 
        }else{
            hidearea(".side_regis_compli_guests_div"+getid);
            $(".registrantselectioncompli"+getid).removeAttr('required');
        }   
    });


    /*-----Open add and edit side event popup-------*/
    $(document).on('click','.add_edit_side_event',function(){
        
        var formAction = $(this).attr('actionform');
        
        if($(this).attr('sideeventid')!==undefined){
            var sideeventid = parseInt($(this).attr('sideeventid'));
            //set value in assing
            $("#tempHidden").val(sideeventid);
        }else{
            //get value form temp hidden and set
            var sideeventid = parseInt($("#tempHidden").val());
        }

        var eventId = '<?php echo $eventId ?>';
        var sendData = {"sideeventid":sideeventid, "eventId":eventId, "formAction":formAction};
        
        //call ajax popup function
        ajaxpopupopen('add_side_event_popup','event/addeditduplicatesideeventview',sendData,'add_edit_side_event');
    });


    //save side event add and edit data
    ajaxdatasave('formAddSideEvent','event/addeditduplicatesideeventsave',true,true,false,true,true,false,false,false);

    /*---This function is used to delete custome field for side event -----*/
    customconfirm('delete_side_event','event/deletesideevent','','','',true,true);

    var gstRate = parseInt('<?php echo (!empty($eventdetails->gst_rate))?$eventdetails->gst_rate:"0"; ?>');

    /*----This function is used to unit price enter and show early bird-----*/
    unitPriceManage('unitPriceEarlyBird','sideEventId','earlyGSTInclude','earlyBirdTotalPrice',gstRate);

    /*----This function is used to complementary total price enter and show complementary total price-----*/
    unitPriceManage('total_price_compEnter','appendid','gst_included_total_price_comp','total_price_comp',gstRate,'total_price_inc_gst_comp');

    /*----This function is used to complementary total early bird price enter and show complementary total early bird price-----*/
    unitPriceManage('total_early_bird_price_compEnter','appendid','gst_included_early_bird_comp','total_early_bird_price_comp',gstRate,'total_early_bird_price_gst_inc_comp');

    /*----This function is used to additional guest total price enter and show additional guest total price-----*/
    unitPriceManage('total_price_ad_guestEnter','appendid','gst_included_total_price_ad_guest','total_price_ad_guest',gstRate,'total_price_gst_inc_ad_guest');

    /*----This function is used to additional guest total early bird price enter and show additional guest total early bird price-----*/
    unitPriceManage('total_early_bird_price_ad_guestEnter','appendid','gst_included_total_price_early_bird_ad_guest','total_early_bird_price_ad_guest',gstRate,'total_early_bird_price_gst_inc_ad_guest');


    /*
    *  This section is used for select option fields
    */   
    $(document).on('click','.stepper-step-guest',function(){
      var appendId = $(this).parent('.stepper').attr('appendId');
      var qty = parseInt($("#guests_limit"+appendId).val());
      //alert(qty);return false;
      if($(this).hasClass('up')){ // increment
        if(qty<20){
          $("#guests_limit"+appendId).val((qty*1)+1);
        }   
      }else{
        if(qty>1){  
          $("#guests_limit"+appendId).val(((qty*1)-1));
        }  
      } 
    });

    $(document).on('click','.stepper-step-comp',function(){
      var appendId = $(this).parent('.stepper').attr('appendId');
      var qty = parseInt($("#complementary_guests_limit"+appendId).val());
      //alert(qty);return false;
      if($(this).hasClass('up')){ // increment
        if(qty<20){
          $("#complementary_guests_limit"+appendId).val((qty*1)+2);
        }   
      }else{
        if(qty>2){  
          $("#complementary_guests_limit"+appendId).val(((qty*1)-2));
        }  
      } 
    });

    $(document).on('click','.commonPrice',function(){
        $(".accordion_content").css("height",'auto');
        var sideEventId  = $(this).attr('sideEventId');
        if($(this).val()==1){
            $(".defaultPricing").show('slow');            
        } else if($(this).val()==0) { 
            $(".defaultPricing").hide('slow');
        }
    });

    /*----This function is used to common total price enter and show common total price-----*/
    unitPriceManage('commonTotalPriceEnter','sideEventId','common_gst_included','common_total_price',gstRate);

    var eventStartDate = '<?php echo (!empty($eventdetails->starttime)) ? date('d M Y', strtotime($eventdetails->starttime)) : ''; ?>';
    var eventEndDate = '<?php echo (!empty($eventdetails->endtime)) ? date('d M Y', strtotime($eventdetails->endtime)) : ''; ?>';

    $( ".sideeventdatetimepicker" ).datetimepicker({
      //startDate: new Date(),
      minDate: eventStartDate,
      maxDate: eventEndDate,
      changeMonth: true,
      changeYear: true,      
      buttonImage: "<?php echo base_url(); ?>themes/system/images/calender_icon.png",
      dateFormat: 'dd M yy',
      timeFormat: 'HH:mm',
      showOn: "both",
      buttonText: 'Calendar',
      controlType: 'select',      
      buttonImageOnly: true,
      pickerPosition: "bottom-left",
     
    });
    
});
</script>      
