<div class="panel-body ls_back dashboard_panel small defaultPricing <?php echo ($sideevent->common_price == '1') ? '' : 'dn' ?>">  
<?php
$registrantId = 0;
$appendId = $sideEventId . '_'.$registrantId;
//get value by breakout id 
$where = array('side_event_id' => $sideEventId, 'registrant_id' => $registrantId);
$sessionRegistrantData = getDataFromTabel('sideevent_registrant', '*', $where,'','id','ASC');
//echo '<pre>';print_r($sessionRegistrantData);
$sessionRegData =  $sessionRegistrantData[0];
//echo '<pre>';print_r($sessionRegistrantData);
if(!empty($sessionRegistrantData)){
    $regName = lang('event_side_default_pricing'); 
    $registrantTypeDetailedClass = '';
    $reg_limitVal = (!empty($sessionRegData) ? $sessionRegData->reg_limit : '');
    $earlyBird_limitVal = (!empty($sessionRegData) ? $sessionRegData->complimentary_earlybird_limit : '');
    $earlyBirdAdGuest_limitVal = (!empty($sessionRegData) ? $sessionRegData->earlybird_limit_ad_guest : '');
    $total_price_compVal = (!empty($sessionRegData) ? $sessionRegData->total_price_comp : '');
    $gst_included_total_price_compVal = (!empty($sessionRegData) ? $sessionRegData->gst_included_total_price_comp : '');
    $total_price_inc_gst_compVal = (!empty($sessionRegData) ? $sessionRegData->total_price_inc_gst : '');
    $total_early_bird_price_compVal = (!empty($sessionRegData) ? $sessionRegData->total_early_bird_price_comp : '');
    $gst_included_early_bird_compVal = (!empty($sessionRegData) ? $sessionRegData->gst_included_early_bird_comp : '');
    $total_early_bird_price_gst_inc_compVal = (!empty($sessionRegData) ? $sessionRegData->total_early_bird_price_gst_inc_comp : '');
    $guests_limitVal = (!empty($sessionRegData) ? $sessionRegData->guests_limit : '1');
    $total_price_ad_guestVal = (!empty($sessionRegData) ? $sessionRegData->total_price_ad_guest : '');
    $gst_included_total_price_ad_guestVal = (!empty($sessionRegData) ? $sessionRegData->gst_included_total_price_ad_guest : '');
    $total_price_gst_inc_ad_guestVal = (!empty($sessionRegData) ? $sessionRegData->total_price_gst_inc_ad_guest : '');
    $total_early_bird_price_ad_guestVal = (!empty($sessionRegData) ? $sessionRegData->total_early_bird_price_ad_guest : '');
    $gst_included_total_price_early_bird_ad_guestVal = (!empty($sessionRegData) ? $sessionRegData->gst_included_total_price_early_bird_ad_guest : '');
    $total_early_bird_price_gst_inc_ad_guestVal = (!empty($sessionRegData) ? $sessionRegData->total_early_bird_price_gst_inc_ad_guest : '');
    $complementary_guests_limitVal = (!empty($sessionRegData) ? $sessionRegData->complementary_guests_limit : '0');
    $SideEventRegistaintIdVal = (!empty($sessionRegData) ? $sessionRegData->id : '');

    $complementaryVal = (!empty($sessionRegData) && $sessionRegData->complementary) ? $sessionRegData->complementary : 0;
    $additional_guestsVal = (!empty($sessionRegData) && $sessionRegData->additional_guests) ? $sessionRegData->additional_guests : 0;
    $complementary_guestsVal = (!empty($sessionRegData) && $sessionRegData->complementary_guests) ? $sessionRegData->complementary_guests : 0;
    $allowEarlyBirdCompVal = (!empty($sessionRegData) && $sessionRegData->complimentary_allow_earlybird) ? $sessionRegData->complimentary_allow_earlybird : 0;
    $allowEarlyBirdAdGuestVal = (!empty($sessionRegData) && $sessionRegData->allow_earlybird_ad_guest) ? $sessionRegData->allow_earlybird_ad_guest : 0;
    $allowGroupBookingVal = (!empty($sessionRegData) && $sessionRegData->complimentary_allow_group_booking) ? $sessionRegData->complimentary_allow_group_booking : 0;

    $complementary_guestsClass = (!empty($sessionRegData) && $sessionRegData->complementary_guests) ? '' : 'dn';
    $complementaryClass = (!empty($sessionRegData) && $sessionRegData->complementary) ? 'dn' : '';
    $allowEarlyBirdCompClass = (!empty($sessionRegData) && $sessionRegData->complimentary_allow_earlybird) ? '' : 'dn';
    $allowEarlyBirdAdGuestClass = (!empty($sessionRegData) && $sessionRegData->allow_earlybird_ad_guest) ? '' : 'dn';
    $additional_guestsClass = (!empty($sessionRegData) && $sessionRegData->additional_guests) ? '' : 'dn';
} else {
    $registrantTypeDetailedClass = '';
    $regName = lang('event_side_default_pricing');  
    $reg_limitVal = ''; 
    $earlyBird_limitVal = ''; 
    $earlyBirdAdGuest_limitVal = ''; 
    $total_price_compVal = ''; 
    $gst_included_total_price_compVal = ''; 
    $total_price_inc_gst_compVal = ''; 
    $total_early_bird_price_compVal = ''; 
    $gst_included_early_bird_compVal = ''; 
    $total_early_bird_price_gst_inc_compVal = ''; 
    $guests_limitVal = '1'; 
    $total_price_ad_guestVal = ''; 
    $gst_included_total_price_ad_guestVal = ''; 
    $total_price_gst_inc_ad_guestVal = ''; 
    $total_early_bird_price_ad_guestVal = ''; 
    $gst_included_total_price_early_bird_ad_guestVal = ''; 
    $total_early_bird_price_gst_inc_ad_guestVal = ''; 
    $complementary_guests_limitVal = '2';
    
    $complementaryVal = 0;
    $additional_guestsVal = 0;
    $complementary_guestsVal = 0;
    $allowEarlyBirdCompVal = 0;
    $allowEarlyBirdAdGuestVal = 0;
    $allowGroupBookingVal = 0;
    $complementary_guestsClass = 'dn';
    $complementaryClass = '';
    $allowEarlyBirdCompClass = 'dn';
    $allowEarlyBirdAdGuestClass = 'dn';
    $additional_guestsClass = 'dn';
    
    $SideEventRegistaintIdVal = 0;
}


$reg_limit = array(
    'name' => 'reg_limit' . $appendId,
    'value' => $reg_limitVal,
    'id' => 'reg_limit' . $appendId,
    'type' => 'text',
    'class' => 'small short_field numValue',
    'required' => '',
    'data-parsley-error-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
);  


$complementaryYes = array(
    'name' => 'complementary' . $appendId,
    'value' => 1,
    'id' => 'complementaryYes' . $appendId,
    'type' => 'radio',
    'appendId' => $appendId,
    'class' => 'complementary',
    'checked' => ($complementaryVal == '1') ? 'checked="checked"' : '',
);
$complementaryNo = array(
    'name' => 'complementary' . $appendId,
    'value' => 0,
    'id' => 'complementaryNo' . $appendId,
    'type' => 'radio',
    'appendId' => $appendId,
    'class' => 'complementary',
    'checked' => ($complementaryVal == '0') ? 'checked="checked"' : '',
);  

$total_price_comp = array(
    'name' => 'total_price_comp' . $appendId,
    'value' => $total_price_compVal,
    'id' => 'total_price_comp' . $appendId,
    'type' => 'text',
    'required' => '',
    'appendId' => $appendId,
    'class' => 'small short_field total_price_compEnter numValue',
    'data-parsley-error-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
    'data-parsley-error-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
);
$gst_included_total_price_comp = array(
    'name' => 'gst_included_total_price_comp' . $appendId,
    'value' => $gst_included_total_price_compVal,
    'id' => 'gst_included_total_price_comp' . $appendId,
    'type' => 'hidden',
    'readonly' => '',
    'appendId' => $appendId,
);
$total_price_inc_gst_comp = array(
    'name' => 'total_price_inc_gst_comp' . $appendId,
    'value' => $total_price_inc_gst_compVal,
    'id' => 'total_price_inc_gst_comp' . $appendId,
    'type' => 'text',
    'readonly' => '',
    'appendId' => $appendId,
    'class' => 'small short_field disable_input',
    'data-parsley-error-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
);

$allowEarlyBirdCompYes = array(
    'name' => 'allowEarlyBirdComp' . $appendId,
    'value' => 1,
    'id' => 'allowEarlyBirdCompYes' . $appendId,
    'type' => 'radio',
    'appendId' => $appendId,
    'class' => 'allowEarlyBirdComp',
    'checked' => ($allowEarlyBirdCompVal == '1') ? 'checked="checked"' : '',
);
$allowEarlyBirdCompNo = array(
    'name' => 'allowEarlyBirdComp' . $appendId,
    'value' => 0,
    'id' => 'allowEarlyBirdCompNo' . $appendId,
    'type' => 'radio',
    'appendId' => $appendId,
    'class' => 'allowEarlyBirdComp',
    'checked' => ($allowEarlyBirdCompVal == '0') ? 'checked="checked"' : '',
);
                   
$earlyBird_limit = array(
    'name' => 'earlyBird_limit' . $appendId,
    'value' => $earlyBird_limitVal,
    'id' => 'earlyBird_limit' . $appendId,
    'type' => 'text',
    'class' => 'small short_field numValue',
    'data-parsley-error-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
);

                
$total_early_bird_price_comp = array(
    'name' => 'total_early_bird_price_comp' . $appendId,
    'value' => $total_early_bird_price_compVal,
    'id' => 'total_early_bird_price_comp' . $appendId,
    'type' => 'text',
    'appendId' => $appendId,
    'class' => 'small  short_field total_early_bird_price_compEnter numValue',
    'data-parsley-error-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
    'data-parsley-error-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
);
$gst_included_early_bird_comp = array(
    'name' => 'gst_included_early_bird_comp' . $appendId,
    'value' => $gst_included_early_bird_compVal,
    'id' => 'gst_included_early_bird_comp' . $appendId,
    'type' => 'hidden',
    'readonly' => '',
    'appendId' => $appendId,
);
$total_early_bird_price_gst_inc_comp = array(
    'name' => 'total_early_bird_price_gst_inc_comp' . $appendId,
    'value' => $total_early_bird_price_gst_inc_compVal,
    'id' => 'total_early_bird_price_gst_inc_comp' . $appendId,
    'type' => 'text',
    'readonly' => '',
    'appendId' => $appendId,
    'class' => 'small short_field disable_input',
    'data-parsley-error-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
);


$allowGroupBookingYes = array(
    'name' => 'allowGroupBooking' . $appendId,
    'value' => 1,
    'id' => 'allowGroupBookingYes' . $appendId,
    'type' => 'radio',
    'appendId' => $appendId,
    'checked' => ($allowGroupBookingVal == '1') ? 'checked="checked"' : '',
);
$allowGroupBookingNo = array(
    'name' => 'allowGroupBooking' . $appendId,
    'value' => 0,
    'id' => 'allowGroupBookingNo' . $appendId,
    'type' => 'radio',
    'appendId' => $appendId,
    'checked' => ($allowGroupBookingVal == '0') ? 'checked="checked"' : '',
);


$additional_guestsYes = array(
    'name' => 'additional_guests' . $appendId,
    'value' => 1,
    'id' => 'additional_guestsYes' . $appendId,
    'type' => 'radio',
    'appendId' => $appendId,
    'class' => 'additional_guests',
    'checked' => ($additional_guestsVal == '1') ? 'checked="checked"' : '',
);
$additional_guestsNo = array(
    'name' => 'additional_guests' . $appendId,
    'value' => 0,
    'id' => 'additional_guestsNo' . $appendId,
    'type' => 'radio',
    'appendId' => $appendId,
    'class' => 'additional_guests',
    'checked' => ($additional_guestsVal == '0') ? 'checked="checked"' : '',
);


$guests_limit = array(
    'name' => 'guests_limit' . $appendId,
    'value' => $guests_limitVal,
    'step' => '1',
    'min' => '1',
    'max' => '20',
    //'readonly' => '',
    'appendId' => $appendId,
    'id' => 'guests_limit' . $appendId,
    'type' => 'number',
    'appendId' => $appendId,
    'class' => 'small dark stepper-input numValue',
    'data-parsley-max' => '20',
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
);
$total_price_ad_guest = array(
    'name' => 'total_price_ad_guest' . $appendId,
    'value' => $total_price_ad_guestVal,
    'id' => 'total_price_ad_guest' . $appendId,
    'type' => 'text',
    'appendId' => $appendId,
    'class' => 'small short_field total_price_ad_guestEnter numValue',
    'data-parsley-error-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
);
$gst_included_total_price_ad_guest = array(
    'name' => 'gst_included_total_price_ad_guest' . $appendId,
    'value' => $gst_included_total_price_ad_guestVal,
    'id' => 'gst_included_total_price_ad_guest' . $appendId,
    'type' => 'hidden',
    'readonly' => '',
    'appendId' => $appendId,
);
$total_price_gst_inc_ad_guest = array(
    'name' => 'total_price_gst_inc_ad_guest' . $appendId,
    'value' => $total_price_gst_inc_ad_guestVal,
    'id' => 'total_price_gst_inc_ad_guest' . $appendId,
    'type' => 'text',
    'readonly' => '',
    'appendId' => $appendId,
    'class' => 'small short_field disable_input',
    'data-parsley-error-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
);

$allowEarlyBirdAdGuestYes = array(
    'name' => 'allowEarlyBirdAdGuest' . $appendId,
    'value' => 1,
    'id' => 'allowEarlyBirdAdGuestYes' . $appendId,
    'type' => 'radio',
    'appendId' => $appendId,
    'class' => 'allowEarlyBirdAdGuest',
    'checked' => ($allowEarlyBirdCompVal == '1') ? 'checked="checked"' : '',
);
$allowEarlyBirdAdGuestNo = array(
    'name' => 'allowEarlyBirdAdGuest' . $appendId,
    'value' => 0,
    'id' => 'allowEarlyBirdAdGuestNo' . $appendId,
    'type' => 'radio',
    'appendId' => $appendId,
    'class' => 'allowEarlyBirdAdGuest',
    'checked' => ($allowEarlyBirdAdGuestVal == '0') ? 'checked="checked"' : '',
);
                   
$earlyBirdAdGuest_limit = array(
    'name' => 'earlyBirdAdGuest_limit' . $appendId,
    'value' => $earlyBirdAdGuest_limitVal,
    'id' => 'earlyBirdAdGuest_limit' . $appendId,
    'type' => 'text',
    'class' => 'small short_field numValue',
    'data-parsley-error-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
);


$total_early_bird_price_ad_guest = array(
    'name' => 'total_early_bird_price_ad_guest' . $appendId,
    'value' => $total_early_bird_price_ad_guestVal,
    'id' => 'total_early_bird_price_ad_guest' . $appendId,
    'type' => 'text',
    'appendId' => $appendId,
    'class' => 'small  short_field total_early_bird_price_ad_guestEnter numValue',
    'data-parsley-error-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
);
$gst_included_total_price_early_bird_ad_guest = array(
    'name' => 'gst_included_total_price_early_bird_ad_guest' . $appendId,
    'value' => $gst_included_total_price_early_bird_ad_guestVal,
    'id' => 'gst_included_total_price_early_bird_ad_guest' . $appendId,
    'type' => 'hidden',
    'readonly' => '',
    'appendId' => $appendId,
);
$total_early_bird_price_gst_inc_ad_guest = array(
    'name' => 'total_early_bird_price_gst_inc_ad_guest' . $appendId,
    'value' => $total_early_bird_price_gst_inc_ad_guestVal,
    'id' => 'total_early_bird_price_gst_inc_ad_guest' . $appendId,
    'type' => 'text',
    'readonly' => '',
    'appendId' => $appendId,
    'class' => 'small short_field disable_input',
    'data-parsley-error-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
);

$complementary_guestsYes = array(
    'name' => 'complementary_guests' . $appendId,
    'value' => 1,
    'id' => 'complementary_guestsYes' . $appendId,
    'type' => 'radio',
    'appendId' => $appendId,
    'class' => 'complementary_guests',
    'checked' => ($complementary_guestsVal == '1') ? 'checked="checked"' : '',
);
$complementary_guestsNo = array(
    'name' => 'complementary_guests' . $appendId,
    'value' => 0,
    'id' => 'complementary_guestsNo' . $appendId,
    'type' => 'radio',
    'appendId' => $appendId,
    'class' => 'complementary_guests',
    'checked' => ($complementary_guestsVal == '0') ? 'checked="checked"' : '',
);

$complementary_guests_limit = array(
    'name' => 'complementary_guests_limit' . $appendId,
    'value' => $complementary_guests_limitVal,
    'step' => '2',
    'min' => '2',
    'max' => '20',
    //'readonly' => '',
    'id' => 'complementary_guests_limit' . $appendId,
    'type' => 'number',
    'appendId' => $appendId,
    'class' => 'small dark numValue',
    'data-parsley-max' => '20',
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
);
$SideEventRegistaintId = array(
    'name' => 'SideEventRegistaintId' . $appendId,
    'value' => $SideEventRegistaintIdVal,
    'id' => 'SideEventRegistaintId' . $appendId,
    'type' => 'hidden',
);

//form create variable
$formSideEventRegistrants = array(
    'name' => 'formSideEventRegistrants',
    'id' => 'formSideEventRegistrants',
    'method' => 'post',
    'class' => 'form-horizontal',
    'data-parsley-validate' => '',
);
?>

<div id="accordion_sub" class="panel-group sub-panel-group sub_panel full_subpanel">
  <div id="registrantTypeDetailedForm<?php echo $appendId; ?>" class="registrantTypeDetailedForm <?php echo $registrantTypeDetailedClass ?>">
    <div class="panel event_panel sub_panel" id="registrantformpanel<?php echo $appendId; ?>">
      <div class="panel-heading darkcolor">
        <h4 class="panel-title medium dt-large">
          <a class="" data-toggle="collapse" data-parent="#accordion_sub" href="#collapseSubOne<?php echo $appendId ?>" id="registrantText<?php echo $appendId; ?>" ><?php echo $regName; ?></a>
        </h4>
      </div>
      <!--end of panel heading-->
      
      
      <div id="collapseSubOne<?php echo $appendId ?>" style="height:auto;" class="accordion_content collapse apply_content ">
      <?php echo form_open($this->uri->uri_string(), $formSideEventRegistrants); ?>
        <div class="row-fluid-15">
          <label for="reg_limit"><?php echo lang('event_side_limit'); ?> <span class="astrik">*</span>
            <span class="info_btn">
              <span class="field_info xsmall"><?php echo lang('event_side_limit'); ?></span>
            </span>
          </label>
          <?php echo form_input($reg_limit); ?>
        </div>
        <!--end of row-fluid-->

        <!-- First Complementary :start --> 
        <div class="row-fluid-15">
          <label for="amc_con"><?php echo lang('event_side_complimentary'); ?>
            <span class="info_btn">
              <span class="field_info xsmall"><?php echo lang('event_side_complimentary'); ?></span>
            </span>
          </label>   
            <div class="radio_wrapper ">
              <?php echo form_radio($complementaryYes); ?>
              <label for="complementaryYes<?php echo $appendId; ?>"><?php echo lang('event_side_yes'); ?></label> 
              <?php echo form_radio($complementaryNo); ?>
              <label for="complementaryNo<?php echo $appendId; ?>"><?php echo lang('event_side_no'); ?></label>
            </div>
        </div>
        <!--end of row-fluid-->
        
        <div class="<?php echo $complementaryClass ?>" id="complementarydiv<?php echo $appendId; ?>">
          <div class="row-fluid-15">
            <label for="total_price_comp"><?php echo lang('event_side_compli_total_price'); ?> <span class="astrik">*</span>
              
            </label>
            <?php echo form_input($total_price_comp); ?>
            <?php echo form_input($gst_included_total_price_comp); ?>
          </div>
          <!--end of row-fluid-->

          <div class="row-fluid-15">
            <label for="gst_included_total_price_comp"><?php echo lang('event_side_compli_gst_include'); ?> <span class="astrik"></span>
              
            </label>
            <?php echo form_input($total_price_inc_gst_comp); ?>
          </div>
          <!--end of row-fluid-->


        <div class="row-fluid-15">
          <label for="amc_con"><?php echo lang('event_regist_allow_earlybird'); ?>
            <span class="info_btn">
              <span class="field_info xsmall"><?php echo lang('event_regist_allow_earlybird'); ?></span>
            </span>
          </label>   
            <div class="radio_wrapper ">
              <?php echo form_radio($allowEarlyBirdCompYes); ?> 
              <label for="allowEarlyBirdCompYes<?php echo $appendId; ?>"><?php echo lang('event_side_yes'); ?></label> 
              <?php echo form_radio($allowEarlyBirdCompNo); ?>
              <label for="allowEarlyBirdCompNo<?php echo $appendId; ?>"><?php echo lang('event_side_no'); ?></label>
            </div>
        </div>    

        <div class="<?php echo $allowEarlyBirdCompClass ?>" id="allowEarlyBirdCompDiv<?php echo $appendId; ?>">
            <div class="row-fluid-15">
              <label for="reg_limit"><?php echo lang('event_side_earlybird_limit'); ?> <span class="astrik">*</span>
                <span class="info_btn">
                  <span class="field_info xsmall"><?php echo lang('event_side_earlybird_limit'); ?></span>
                </span>
              </label>
              <?php echo form_input($earlyBird_limit); ?>
            </div>

            <div class="row-fluid-15">
                <label for="total_early_bird_price_comp" class="ml_m8"><?php echo lang('event_side_earlybird_total_price'); ?> <span class="astrik">*</span>
                  
                </label>                                            
                <?php echo form_input($total_early_bird_price_comp); ?>
                <?php echo form_input($gst_included_early_bird_comp); ?>
            </div>
            <!--end of row-fluid-->

            <div class="row-fluid-15">
                <label for="gst_included_early_bird_comp" class="ml_m8"><?php echo lang('event_side_earlybird_gst_inc'); ?> <span class="astrik"></span>
                  
                </label>
                <?php echo form_input($total_early_bird_price_gst_inc_comp); ?>
            </div>
            <!--end of row-fluid-->
        </div>
          
        <div class="row-fluid-15">
          <label for="amc_con"><?php echo lang('event_side_grop_booking'); ?>
            <span class="info_btn">
              <span class="field_info xsmall"><?php echo lang('event_side_grop_booking'); ?></span>
            </span>
          </label>   
            <div class="radio_wrapper ">
              <?php echo form_radio($allowGroupBookingYes); ?> 
              <label for="allowGroupBookingYes<?php echo $appendId; ?>"><?php echo lang('event_side_yes'); ?></label> 
              <?php echo form_radio($allowGroupBookingNo); ?>
              <label for="allowGroupBookingNo<?php echo $appendId; ?>"><?php echo lang('event_side_no'); ?></label>
            </div>
        </div> 

    </div>      

        <!-- First Complementary :end -->               

        <hr class="panel_inner_seprator"/>

        <!-- second Additional Guests  :start -->        
        <div class="row-fluid-15">
            <label for="amc_con"><?php echo lang('event_side_additional_guest'); ?>
                <span class="info_btn">
                  <span class="field_info xsmall"><?php echo lang('event_side_additional_guest'); ?></span>
                </span>
            </label>
            <div class="radio_wrapper ">
              <?php echo form_radio($additional_guestsYes); ?> 
              <label for="additional_guestsYes<?php echo $appendId; ?>"><?php echo lang('event_side_yes'); ?></label> 
              <?php echo form_radio($additional_guestsNo); ?>
              <label for="additional_guestsNo<?php echo $appendId; ?>"><?php echo lang('event_side_no'); ?></label>
            </div>
        </div>
        <!--end of row-fluid-->

        <div id="additionalguestsdiv<?php echo $appendId; ?>" class="<?php echo $additional_guestsClass; ?>">
          <div class="row-fluid-15">
            <label for="refr_no"><?php echo lang('event_side_guest_limit'); ?>
              
            </label>
            <div class="stepper " appendId="<?php echo $appendId ?>">
              <?php echo form_input($guests_limit); ?>
            </div>
          </div>
          <!--end of row-fluid-->

          <div class="row-fluid-15">
            <label for="reg_limit"><?php echo lang('event_side_add_total_price'); ?> <span class="astrik">*</span></label>
            <?php echo form_input($total_price_ad_guest); ?>
            <?php echo form_input($gst_included_total_price_ad_guest); ?>                                           
          </div>
          <!--end of row-fluid-->

          <div class="row-fluid-15">
            <label for="reg_limit"><?php echo lang('event_side_add_gst_include'); ?> <span class="astrik"></span></label>
            <?php echo form_input($total_price_gst_inc_ad_guest); ?>
          </div>
          <!--end of row-fluid-->


        <div class="row-fluid-15">
          <label for="amc_con"><?php echo lang('event_regist_allow_earlybird'); ?>
            <span class="info_btn">
              <span class="field_info xsmall"><?php echo lang('event_regist_allow_earlybird'); ?></span>
            </span>
          </label>   
            <div class="radio_wrapper ">
              <?php echo form_radio($allowEarlyBirdAdGuestYes); ?> 
              <label for="allowEarlyBirdAdGuestYes<?php echo $appendId; ?>"><?php echo lang('event_side_yes'); ?></label> 
              <?php echo form_radio($allowEarlyBirdAdGuestNo); ?>
              <label for="allowEarlyBirdAdGuestNo<?php echo $appendId; ?>"><?php echo lang('event_side_no'); ?></label>
            </div>
        </div>    

        <div class="<?php echo $allowEarlyBirdAdGuestClass ?>" id="allowEarlyBirdAdGuestDiv<?php echo $appendId; ?>">
            <div class="row-fluid-15">
              <label for="reg_limit"><?php echo lang('event_side_earlybird_limit'); ?> <span class="astrik">*</span>
                <span class="info_btn">
                  <span class="field_info xsmall"><?php echo lang('event_side_earlybird_limit'); ?></span>
                </span>
              </label>
              <?php echo form_input($earlyBirdAdGuest_limit); ?>
            </div>



            <div class="row-fluid-15">
                <label for="reg_limit" class="ml_m8"><?php echo lang('event_side_total_early_price'); ?> <span class="astrik">*</span>
                </label>
                <?php echo form_input($total_early_bird_price_ad_guest); ?>
                <?php echo form_input($gst_included_total_price_early_bird_ad_guest); ?>
            </div>
            <!--end of row-fluid-->

            <div class="row-fluid-15">
                <label for="reg_limit" class="ml_m8"><?php echo lang('event_side_earlybird_gst_inc'); ?> <span class="astrik"></span></label>
                <?php echo form_input($total_early_bird_price_gst_inc_ad_guest); ?>
            </div>
            <!--end of row-fluid-->
        </div>


        </div> 

        <hr class="panel_inner_seprator"/>
        <!-- second Additional Guests  :end -->          

        <!-- Third Complementary Guests  :start -->             
        <div class="row-fluid-15">
          <label for="amc_con"><?php echo lang('event_side_complementary_guests'); ?>
            <span class="info_btn">
              <span class="field_info xsmall"><?php echo lang('event_side_complementary_guests'); ?></span>
            </span>
          </label>
            <div class="radio_wrapper ">
              <?php echo form_radio($complementary_guestsYes); ?> 
              <label for="complementary_guestsYes<?php echo $appendId; ?>"><?php echo lang('event_side_yes'); ?></label> 
              <?php echo form_radio($complementary_guestsNo); ?>
              <label for="complementary_guestsNo<?php echo $appendId; ?>"><?php echo lang('event_side_no'); ?></label>
            </div>
        </div>
        <!--end of row-fluid-->


        <div id="complementaryguestsdiv<?php echo $appendId; ?>" class="<?php echo $complementary_guestsClass ?>" id="complementarydiv<?php echo $appendId; ?>">
          <div class="row-fluid-15">
            <label for="refr_no"><?php echo lang('event_side_guests_limit'); ?></label>
            <div class="stepper " appendId="<?php echo $appendId ?>">
              <?php echo form_input($complementary_guests_limit); ?>
            </div>
          </div>
          <!--end of row-fluid-->
        </div>
      

        <div class="btn_wrapper ">
            <a href="#collapseFive<?php echo $sideEventId; ?>" class="pull-left scroll_top"><?php echo lang('comm_top'); ?></a>
            <?php
            echo form_hidden('eventId', $eventId);
            echo form_hidden('sideEventId', $sideEventId);
            echo form_hidden('registrantId', $registrantId);
            echo form_input($SideEventRegistaintId);
            echo form_hidden('formActionName', 'sideeventRegistrantDetails');

            //button show of save and reset
            $formButton['saveButtonId'] = 'id="' . $registrantId . '"'; // click button id
            $formButton['showbutton'] = array('save', 'reset');
            $formButton['labletext'] = array('save' => lang('comm_save_changes'), 'reset' => lang('comm_reset'));
            $formButton['buttonclass'] = array('save' => 'default_btn btn pull-right medium saveRegistrant', 'reset' => 'default_btn btn pull-right medium reset_form');
            $this->load->view('common_save_reset_button', $formButton);
            ?>
        </div>
        <?php echo form_close(); ?>
      </div>
    </div>
    <!--end of panel-->                 
 </div>
</div>
<!--end of panel-group-->
</div>
