<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//--------add type fo registration fields---------//  

$sideEventNameValue = (!empty($sideeventdata->side_event_name))?$sideeventdata->side_event_name:''; 

$formAddSideEvent = array(
    'name'   => 'formAddSideEvent',
    'id'   => 'formAddSideEvent',
    'method' => 'post',
    'class'  => 'wpcf7-form',
    'data-parsley-validate' => '',
);
  
$sideEventName = array(
    'name'  => 'sideEventName',
    'value' => $sideEventNameValue,
    'id'  => 'sideEventName',
    'type'  => 'text',
    'class' => 'small',
    'required'  => '',
    'autocomplete' => 'off',
    'data-parsley-error-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
);
  
$sideEventId = array(
    'name'  => 'sideEventId',
    'value' => $sideeventid,
    'id'  => 'sideEventId',
    'type'  => 'hidden',
  );  
  
$formAction = array(
    'name'  => 'formAction',
    'value' => $formActionValue,
    'id'  => 'formAction',
    'type'  => 'hidden',
  );   
      
?>

<!--------add and edit side event popup div start-------->
<div id="add_side_event_popup" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" >
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header delegates">
        <h4 class="medium dt-large modal-title"><?php echo $headerAction; ?></h4>
      </div>

      <div class="modal-body small">
        <div class="modelinner ">
      <?php  echo form_open(base_url('event/addeditduplicatesideeventview'),$formAddSideEvent); ?>
            <div class="control-group mb10 eventdashboard_popup small">

              <div class="row-fluid">
                <label class="pull-left" for="presenter_title"><?php echo lang('event_side_popup_title'); ?> <span class="astrik">*</span>
                <span class="info_btn"><span class="field_info xsmall"><?php echo lang('event_side_popup_title'); ?></span></span>
                </label>
                <?php echo form_input($sideEventName); ?> 
              </div>
              <div class="btn_wrapper">
                  <?php 
                    echo form_hidden('eventId', $eventId);
                    echo form_input($sideEventId);
                    echo form_input($formAction);
                    $extraCancel  = 'class="popup_cancel submitbtn pull-right medium" data-dismiss="modal" ';
                    $extraSave    = 'class="submitbtn pull-right medium" ';
                    echo form_submit('save',lang('comm_save'),$extraSave);
                    echo form_submit('cancel',lang('comm_cancle'),$extraCancel);                    
                  ?>
              </div>
            </div>
          <?php echo form_close(); ?>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Modal -->

<script>
$("#formAddSideEvent").parsley();
</script>
