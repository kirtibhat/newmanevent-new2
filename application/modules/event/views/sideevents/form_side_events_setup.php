<?php if (!defined('BASEPATH'))  exit('No direct script access allowed');
//set value in the variable 
$sideEventId = (!empty($sideevent->id)) ? $sideevent->id : '0';
$sideEventNameValue = (!empty($sideevent->side_event_name)) ? $sideevent->side_event_name : '';
$sideEventLimitValue = (isset($sideevent->side_event_limit)) ? $sideevent->side_event_limit : '';
$sideEventVenueValue = (!empty($sideevent->venue)) ? $sideevent->venue : '';
$sideEventAddDetailsValue = (!empty($sideevent->additional_details)) ? $sideevent->additional_details : '';
$sideEventStartDateValue = (!empty($sideevent->start_datetime)) ? dateFormate($sideevent->start_datetime, 'd M Y H:i') : '';
$sideEventEndDateValue = (!empty($sideevent->end_datetime)) ? dateFormate($sideevent->end_datetime, 'd M Y H:i') : '';
$sideEventCommonPriceVal = (!empty($sideevent->common_price)) ? true : false;

$commonTotalPriceVal = (!empty($sideevent->common_total_price)) ? $sideevent->common_total_price:'';
$commonGstIncludedVal = (!empty($sideevent->common_gst_included)) ? $sideevent->common_gst_included:'';

$earlyBirdDateValue = (!empty($sideevent->earlybird_date)) ? dateFormate($sideevent->earlybird_date, 'd M Y') : '';
$sideEventAllowEarlybirdValue = (!empty($sideevent->allow_earlybird) && $sideevent->allow_earlybird == '1' ) ? true : false;
$sideEventlimitEarlybirdVal = (!empty($sideevent->early_bird_limit)) ? $sideevent->early_bird_limit : '';
$sideEventtotalPriceEarlyBirdVal = (!empty($sideevent->early_bird_total_price)) ? $sideevent->early_bird_total_price : '';
$sideEventgstEarlyBird = (isset($sideevent->early_bird_gst_included)) ? $sideevent->early_bird_gst_included : '';
$sideEventRegistrants = (isset($sideevent->sideevent_registrants)) ? json_decode($sideevent->sideevent_registrants) : '';

//get value by side event id 
$where = array('event_id'=>$sideEventId);
$sideEventData = getDataFromTabel('side_event','*',$where,'','event_id','ASC');

//group value set
$groupBookingAllowedVal = (!empty($sideevent->allow_group_booking)) ? true : false;
$minGroupBookingSizeVal = (!empty($sideevent->min_group_booking_size)) ? $sideevent->min_group_booking_size : 0;
$percentageDiscountGroupBookingVal = (!empty($sideevent->percentage_discount_for_group_booking)) ? $sideevent->percentage_discount_for_group_booking : '0';

$restrictEventAccess = (!empty($sideevent->event_restrict_access) && $sideevent->event_restrict_access == '1' ) ? true : false;
//set value for early bird field
$registrantCatDiv = 'dn';
if (!empty($restrictEventAccess)) {
  $registrantCatDiv = '';
}

//set value for early bird field
$isShowEarlyBirdVal = 'dn';
$isShowEarlyBirdFieldDisable = true;
if (!empty($sideEventAllowEarlybirdValue)) {
  $isShowEarlyBirdVal = '';
  $isShowEarlyBirdFieldDisable = true;
}

$unitPriceEarlyBirdVal = (!empty($formdata->unit_price_early_bird)) ? $formdata->unit_price_early_bird : '';
$totalPriceEarlyBirdVal = (!empty($formdata->total_price_early_bird)) ? $formdata->total_price_early_bird : '';
$gstEarlyBird = (isset($formdata->gst_early_bird)) ? $formdata->gst_early_bird : '';
$limitEarlybirdVal = (isset($formdata->registrants_limit_early_bird)) ? $formdata->registrants_limit_early_bird : '';

//form create variable
$formSideEvent = array(
    'name' => 'formSideEvent' . $sideEventId,
    'id' => 'formSideEvent' . $sideEventId,
    'method' => 'post',
    'class' => 'form-horizontal',
    'data-parsley-validate' => '',
);

$sideEventStartDate = array(
    'name' => 'sideEventStartDate' . $sideEventId,
    'value' => $sideEventStartDateValue,
    'id' => 'sideEventStartDate' . $sideEventId,
    'type' => 'text',
    'required' => '',
    //'readonly' => 'readonly',
    'class' => 'date_input small sideeventdatetimepicker',
    'data-format' => 'yyyy-MM-dd hh:mm:ss',
    'data-parsley-error-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'change',
);

$sideEventEndDate = array(
    'name' => 'sideEventEndDate' . $sideEventId,
    'value' => $sideEventEndDateValue,
    'id' => 'sideEventEndDate' . $sideEventId,
    'type' => 'text',
    'required' => '',
    //'readonly' => 'readonly',
    'class' => 'date_input small sideeventdatetimepicker',
    'data-format' => 'yyyy-MM-dd hh:mm:ss',
    'data-parsley-error-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'change',
);

$sideEventVenue = array(
    'name' => 'sideEventVenue' . $sideEventId,
    'value' => $sideEventVenueValue,
    'id' => 'sideEventVenue' . $sideEventId,
    'type' => 'text',
    'required' => '',
    'class' => 'small',
    'data-format' => 'yyyy-MM-dd hh:mm:ss',
    'data-parsley-error-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
);

$sideEventAddDetail = array(
    'name' => 'sideEventAddDetail' . $sideEventId,
    'value' => $sideEventAddDetailsValue,
    'id' => 'sideEventAddDetail' . $sideEventId,
    'type' => 'text',
    'rows' => '3',
    'class' => 'small'
);

$sideEventLimit = array(
    'name' => 'sideEventLimit' . $sideEventId,
    'value' => $sideEventLimitValue,
    'id' => 'sideEventLimit' . $sideEventId,
    'type' => 'text',
    //'required' => '',
    'class' => 'small  short_field numValue',
    'data-parsley-error-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
);

$sideEventCommonPriceYes = array(
    'name' => 'sideEventCommonPrice' . $sideEventId,
    'value' => 1,
    'id' => 'sideEventCommonPriceYes' . $sideEventId,
    'type' => 'radio',
    'sideEventId' => $sideEventId,
    'class' => 'commonPrice',
    'checked' => ($sideEventCommonPriceVal == '1') ? 'checked="checked"' : '',
);
$sideEventCommonPriceNo = array(
    'name' => 'sideEventCommonPrice' . $sideEventId,
    'value' => 0,
    'id' => 'sideEventCommonPriceNo' . $sideEventId,
    'type' => 'radio',
    'sideEventId' => $sideEventId,
    'class' => 'commonPrice',
    'checked' => ($sideEventCommonPriceVal == '0') ? 'checked="checked"' : '',
);

$commonTotalPrice = array(
    'name' => 'common_total_price' . $sideEventId,
    'id' => 'common_total_price' . $sideEventId,  
    'value' => $commonTotalPriceVal,
    'sideEventId' => $sideEventId,
    'type' => 'text',
    'class' => 'small  short_field commonTotalPriceEnter',
    'data-parsley-error-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
);

$commonGstIncluded = array(
    'name' => 'common_gst_included' . $sideEventId,
    'id' => 'common_gst_included' . $sideEventId, 
    'value' => $commonGstIncludedVal,
    'sideEventId' => $sideEventId,
    'type' => 'text',
    'readonly' => '',
    'class' => 'small  short_field',
    'data-parsley-error-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
);

$sideEventAllowEarlybird = array(
    'name' => 'sideEventAllowEarlybird' . $sideEventId,
    'value' => '1',
    'id' => 'sideEventAllowEarlybird' . $sideEventId,
    'sideEventId' => $sideEventId,
    'type' => 'checkbox',
    'class' => 'mt10 allowEarlybirdselection',
    'checked' => $sideEventAllowEarlybirdValue,
);

$earlyBirdLimit = array(
    'name' => 'earlyBirdLimit' . $sideEventId,
    'value' => $sideEventlimitEarlybirdVal,
    'id' => 'earlyBirdLimit' . $sideEventId,
    'type' => 'text',
    'disabled' => '',
    'sideEventId' => $sideEventId,
    'class' => 'short_field small numValue earlyBirdLimit',
    'data-parsley-error-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
);

$earlyBirdTotalPrice = array(
    'name' => 'earlyBirdTotalPrice' . $sideEventId,
    'value' => $sideEventtotalPriceEarlyBirdVal,
    'id' => 'earlyBirdTotalPrice' . $sideEventId,
    'type' => 'text',
    'disabled' => '',
    'sideEventId' => $sideEventId,
    'class' => 'short_field small unitPriceEarlyBird numValue',
    'data-parsley-error-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
);

$earlyGSTInclude = array(
    'name' => 'earlyGSTInclude' . $sideEventId,
    'value' => $sideEventgstEarlyBird,
    'id' => 'earlyGSTInclude' . $sideEventId,
    'type' => 'text',
    //  'required'  => '',
    'readonly' => '',
    'class' => 'short_field small',
);

//remove disable from early bird section field
if ($isShowEarlyBirdFieldDisable) {
  unset($earlyBirdLimit['disabled']);
  unset($earlyBirdTotalPrice['disabled']);
  unset($earlyGSTInclude['disabled']);
}

//-------------add and remove disable for group book fields section------------------//
//group book checkebox area     
$sideEventGroupbookings = array(
    'name' => 'sideEventGroupbookings' . $sideEventId,
    'value' => '1',
    'id' => 'sideEventGroupbookings' . $sideEventId,
    'groupbookingsid' => $sideEventId,
    'type' => 'checkbox',
    'class' => 'sideEventGroupbookings',
    'checked' => $groupBookingAllowedVal,
);
$sideEventGroupBookingsSize = array(
    'name' => 'sideEventGroupBookingsSize' . $sideEventId,
    'value' => $minGroupBookingSizeVal,
    'id' => 'sideEventGroupBookingsSize' . $sideEventId,
    'type' => 'number',
    'class' => 'small numValue',
    //'required'    => '',
    'autocomplete' => 'off',
    'data-parsley-error-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'change',
    //'disabled' => '',
    'min' => '0',
    'max' => '10',
    'step' => '1'
);

$sideEventGroupBookingsPercent = array(
    'name' => 'sideEventGroupBookingsPercent' . $sideEventId,
    'value' => $percentageDiscountGroupBookingVal,
    'id' => 'sideEventGroupBookingsPercent' . $sideEventId,
    'type' => 'text',
    'class' => 'small numValue clr_425968 spinner_selectbox',
    'required' => '',
    'autocomplete' => 'off',
    'data-parsley-error-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
    //'disabled' => '',
    'min' => '0',
);

$restrictEventAccessyes = array(
    'name' => 'restrictEventAccess' . $sideEventId,
    'value' => 1,
    'id' => 'restrictEventAccessyes' . $sideEventId,
    'type' => 'radio',
    'sideEventId' => $sideEventId,
    'class' => 'restrictEventAccess',
    'checked' => ($restrictEventAccess == '1') ? 'checked="checked"' : '',
);
$restrictEventAccessno = array(
    'name' => 'restrictEventAccess' . $sideEventId,
    'value' => 0,
    'id' => 'restrictEventAccessno' . $sideEventId,
    'type' => 'radio',
    'sideEventId' => $sideEventId,
    'class' => 'restrictEventAccess',
    'checked' => ($restrictEventAccess == '0') ? 'checked="checked"' : '',
);

//check group book allow then show div otherwise hide
$isgroupBookingShowDiv = 'dn';
$isDisableGroup = 'disabled';
if ($groupBookingAllowedVal) {
  $isgroupBookingShowDiv = '';
  unset($sideEventGroupBookingsSize['disabled']);
  unset($sideEventGroupBookingsPercent['disabled']);
  $isDisableGroup = '';
}
?>

<div class="panel event_panel co_cat co_cat_corporate">
  <div class="panel-heading" id="sideevent_<?php echo $sideEventId; ?>">
    <h4 class="panel-title medium dt-large heading_btn">
      <a class="" data-toggle="collapse" data-parent="#accordion" href="#collapseFive<?php echo $sideEventId; ?>"><?php echo ucwords($sideEventNameValue); ?></a>
      <span class="heading_btn_wrapper">
        <a href="javascript:void(0)" class="delete_btn td_btn delete_side_event" deleteid="<?php echo $sideEventId; ?>"><span>&nbsp;</span></a>
        <a href="javascript:void(0)" class="eventsetup_btn td_btn add_edit_side_event" actionform="edit" sideeventid="<?php echo $sideEventId; ?>"><span>&nbsp;</span></a>
        <a href="javascript:void(0)" class="copy_btn td_btn add_edit_side_event" actionform="duplicate" sideeventid="<?php echo $sideEventId; ?>"><span>&nbsp;</span></a>
      </span>
    </h4>
  </div>

  <div style="height: auto;" id="collapseFive<?php echo $sideEventId; ?>" class="accordion_content collapse apply_content ">
    
    <div class="panel-body ls_back dashboard_panel small ">
    <?php echo form_open($this->uri->uri_string(), $formSideEvent); ?>
      <div class="row-fluid-15">
        <label for="start_date"><?php echo lang('event_side_start'); ?> <span class="astrik">*</span></label>
        <div class="date_wrapper" id="datetimepickerstart<?php echo $sideEventId; ?>">
            <?php echo form_input($sideEventStartDate); ?>
            <?php echo form_error('sideEventStartDate' . $sideEventId); ?>
        </div>
      </div>
      <!--end of row-fluid-->

      <div class="row-fluid-15">
        <label for="start_date"><?php echo lang('event_side_finish'); ?> <span class="astrik">*</span></label>
        <div class="date_wrapper" id="datetimepickerend<?php echo $sideEventId; ?>">
            <?php echo form_input($sideEventEndDate); ?>
            <?php echo form_error('sideEventEndDate' . $sideEventId); ?>
        </div>
      </div>
      <!--end of row-fluid-->

      <div class="row-fluid-15">
        <label for="info_org"><?php echo lang('event_side_venue'); ?> <span class="astrik">*</span>
          <span class="info_btn">
            <span class="field_info xsmall"><?php echo lang('event_side_venue'); ?></span>
          </span>
        </label>
        <?php echo form_input($sideEventVenue); ?>
        <?php echo form_error('sideEventVenue' . $sideEventId); ?>
      </div>
      <!--end of row-fluid-->

      <div class="row-fluid-15">
        <label for="info_org"><?php echo lang('event_side_additional_details'); ?>
          <span class="info_btn">
            <span class="field_info xsmall"><?php echo lang('event_side_additional_details'); ?></span>
          </span>
        </label>
        <?php echo form_textarea($sideEventAddDetail); ?>
        <?php echo form_error('sideEventAddDetail' . $sideEventId); ?>
      </div>
      <!--end of row-fluid-->

      <div class="row-fluid-15">
        <label for="reg_limit"><?php echo lang('event_side_limit'); ?> <span class="astrik">*</span>
          <span class="info_btn">
            <span class="field_info xsmall"><?php echo lang('event_side_limit'); ?></span>
          </span>
        </label>
        <?php echo form_input($sideEventLimit); ?>
        <?php echo form_error('sideEventLimit' . $sideEventId); ?>
      </div>
      <!--end of row-fluid-->


      <div class="row-fluid-15">
        <label for="mem_compl"><?php echo lang('event_side_default_pricing'); ?>
          <span class="info_btn">
            <span class="field_info xsmall"><?php echo lang('event_side_default_pricing'); ?></span>
          </span>
        </label>
        <div class="radio_wrapper ">
          <?php echo form_radio($sideEventCommonPriceYes); ?>
          <label for="sideEventCommonPriceYes<?php echo $sideEventId; ?>"><?php echo lang('event_side_yes'); ?></label> 
          <?php echo form_radio($sideEventCommonPriceNo); ?>
          <label for="sideEventCommonPriceNo<?php echo $sideEventId; ?>"><?php echo lang('event_side_no'); ?></label>
        </div>
      </div>
      <!--end of row-fluid-->

        

      <div class="row-fluid-15">
        <label for="info_org"><?php echo lang('event_side_restrict_event_access'); ?> 
          <span class="info_btn">
            <span class="field_info xsmall"><?php echo lang('event_side_restrict_event_access'); ?>
            </span>
          </span>
        </label>
        <div class="radio_wrapper ">
          <?php echo form_radio($restrictEventAccessyes); ?>
          <label for="restrictEventAccessyes<?php echo $sideEventId; ?>"><?php echo lang('event_side_yes'); ?></label> 
          <?php echo form_radio($restrictEventAccessno); ?>
          <label for="restrictEventAccessno<?php echo $sideEventId; ?>"><?php echo lang('event_side_no'); ?></label>
        </div>

        <div class="session_subcat l_margin <?php echo ($sideevent->event_restrict_access == '0') ? 'dn' : ""; ?>" id="registrantCatDiv<?php echo $sideEventId; ?>">
          <?php

          //Convert object into array
          $sideeventRegistrantData = array();
          if (!empty($sideeventRegistrantData)) {
            foreach ($sideeventRegistrantData as $key => $value) {
              $selectedregistrant = array(
                  'name' => 'selectedregistrant' . $sideEventId . '[' . $value->registrant_id . ']',
                  'value' => $value->session_limit,
                  'id' => 'hiddenSelectedRegistrant' . $sideEventId,
                  'type' => 'hidden',
                  'sideEventId' => $sideEventId,
                  'class' => 'registrantIds',
                  'key' => $value->registrant_id,
              );
              echo form_input($selectedregistrant);
            }
          }



            if (!empty($catFilterList)) {
              foreach ($catFilterList as $key => $list) {
                foreach ($list as $cat => $registrant) {
                  $registrantCat = array(
                      'name' => 'registrant_cat_' . $sideEventId,
                      //'value'     => '',
                      'id' => 'registrant_cat_' . $sideEventId,
                      'type' => 'checkbox',
                      'sideEventId' => $sideEventId,
                      'class' => 'registrantCat',
                      'data-append-id' => $key . '_' . $sideEventId,
                      'data-category-id' => $key,
                  );

                  $catCheckedVal = 0;
                  foreach ($registrant as $registrantId => $value) {                            
                      /*$where = array('side_event_id ' => $sideEventId, 'registrant_id' => $registrantId);                    
                      $sessionRegistrantData = getDataFromTabel('sideevent_registrant', '*', $where, '', 'id', 'DESC');                      
                      if(!empty($sessionRegistrantData)){
                        foreach($sessionRegistrantData as $sessionRegData){
                            $catCheckedVal = $catCheckedVal+1;
                        }
                      } */
                      if(!empty($sideEventRegistrants)){
                        $catCheckedVal = 1;
                      }
                  }
         
                  $catChecked = (($catCheckedVal > 0) ? 'checked="checked"' : '');
                  ?>
                  <div class="row-fluid-15">
                    <span class="checkbox_wrapper">
                    <?php echo form_input($registrantCat, 1, $catChecked); ?>
                      <label for="registrant_cat_<?php echo $sideEventId; ?>"><?php echo $cat; ?></label></span></div>
                      <div id="category_list_<?php echo $key; ?>_<?php echo $sideEventId; ?>" data-category="<?php echo $key; ?>_<?php echo $sideEventId; ?>" class="<?php echo (($catCheckedVal > 0) ? '' : 'dn') ?>">
                    <?php
                    
                    $regLoop = -1;
                    foreach ($registrant as $registrantId => $value) {      
                      $regLoop++;                      
                      $where = array('side_event_id ' => $sideEventId, 'registrant_id' => $registrantId);                    
                      $sessionRegistrantData = getDataFromTabel('sideevent_registrant', '*', $where, '', 'id', 'DESC');
              
                      /*if(!empty($sessionRegistrantData)){
                        foreach($sessionRegistrantData as $sessionRegData){
                            $registrantChecked = (in_array($registrantId,(array)$sessionRegData)) ? 'checked="checked"' : '';
                        }
                      } else {
                        $registrantChecked = '';
                      } */


                      $registrantChecked = (in_array($registrantId,(array)$sideEventRegistrants)) ? 'checked="checked"' : '';
                        
                      $registrantCheckbox = array(
                          'name' => 'registrantcheckbox' . $sideEventId . '[' . $registrantId . ']',
                          //'value' => '1',
                          'id' => 'registrantcheckbox' . $sideEventId . '_' . $registrantId,
                          'type' => 'checkbox',
                          'sideEventId' => $sideEventId,
                          'registrantId' => $registrantId,
                          'class' => 'registrantlisttype',
                          'data-append-id' => $key . '_' . $sideEventId,
                          'data-category-id' => $key,
                      );
                      ?>
                      <div class="row-fluid-15 sub_check"><span class="checkbox_wrapper">
                      <?php echo form_input($registrantCheckbox, 1, $registrantChecked); ?>
                        <label for="registrantcheckbox<?php echo $sideEventId; ?><?php echo '_' . $registrantId; ?>"><?php echo $value; ?></label></span>
                      </div>
                  <?php  } //end loop ?>
                  </div>
                <?php
                } // end loop
              } //end loop
            } // end if                                                                                       
          ?>
        </div>              
        <!--end of session sub category-->



          <div class="btn_wrapper ">
            <a href="#collapseFive<?php echo $sideEventId; ?>" class="pull-left scroll_top"><?php echo lang('comm_top'); ?></a>
            <?php
            echo form_hidden('eventId', $eventId);
            echo form_hidden('sideEventId', $sideEventId);
            echo form_hidden('formActionName', 'sideeventDetails');

            //button show of save and reset
            $formButton['saveButtonId'] = 'id="' . $sideEventId . '"'; // click button id
            $formButton['showbutton'] = array('save', 'reset');
            $formButton['labletext'] = array('save' => lang('comm_save_changes'), 'reset' => lang('comm_reset'));
            $formButton['buttonclass'] = array('save' => 'default_btn btn pull-right medium', 'reset' => 'default_btn btn pull-right medium reset_form');
            $this->load->view('common_save_reset_button', $formButton);
            ?>

          </div>  
        
      </div>
      </div>
      <?php echo form_close(); ?>

      
      <?php
        //Load Registrant View
        $dsdata['sideEventId']  = $sideEventId;        
        $dsdata['sideevent']    = $sideevent;        
        $this->load->view('form_side_events_default_registrant',$dsdata);
        
        //Load Registrant View
        $sdata['sideevent']    = $sideevent;
        $sdata['sideEventId'] = $sideEventId;
        $sdata['sideEventRegistrants'] = $sideEventRegistrants;
        $this->load->view('form_side_events_registrant',$sdata);
      ?>

    
      
    </div>
  </div>
</div>

<script type="text/javascript">
//side event details save type details save
ajaxdatasave('formSideEvent<?php echo $sideEventId; ?>', '<?php echo $this->uri->uri_string(); ?>',true,true,false,true,true,'#showhideformdiv<?php echo $sideEventId; ?>', '',false);

ajaxdatasave('formSideEventRegistrants', '<?php echo $this->uri->uri_string(); ?>',true,true,false,true,true,'#showhideformdiv<?php echo $sideEventId; ?>', '',false);
</script>
