<div class="panel event_panel co_cat">
		<div class="panel-heading ">
			<h4 class="panel-title medium dt-large ">
			<a class=" " data-toggle="collapse" data-parent="#accordion" href="#collapseFive"><?php echo lang('event_registrant_category'); ?></a>
			</h4>
		</div>
		<div style="height: auto;" id="collapseFive" class="accordion_content collapse apply_content ">
			<form>
			<div class="panel-body ls_back dashboard_panel small co_category">
				<ul class="medium category_ul">
					<?php if(!empty($registrantCategorydata)){
					  foreach($registrantCategorydata as $value){	
						 ?>
								<li><?php echo $value['category']; ?></li>
								<!--<li>All Registrant Types</li>-->
					 <?php } // end loop
					} // end if ?>		
				</ul>
			</div>
			</form>
		</div>
</div>
<!--end of panel-->
