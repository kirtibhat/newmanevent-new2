<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//set value in variable
if(!empty($customizeformsdata)){
	$customizeformsdata = $customizeformsdata[0];
}

$LogoFileId = (!empty($customizeformsdata->id))?$customizeformsdata->id:'';
$LogoFileName = (!empty($customizeformsdata->logo_image))?$customizeformsdata->logo_image:'';


//add exhibitor floor plan form 
$formEventLogo = array(
		'name'	 => 'formEventLogo',
		'id'	 => 'formEventLogo',
		'method' => 'post',
		'class'  => 'form-horizontal',
	);
	

?>
<div class="commonform_bg pb0">
	<div class="headingbg bg_807f83">
		<div class="typeofregistV pull-left showhideaction ptr" id="formLogo">
			<?php echo lang('event_customize_logo') ?>
		</div>
		<div class="clearfix">
		</div>
	</div>
	<?php echo form_open($this->uri->uri_string(),$formEventLogo); ?>	
		<div class="form-horizontal form_open_chk dn" id="showhideformdivformLogo">
			
		<div class="control-group mb10" id="attch_FileContainer_logo">
			<label class="control-label" for="inputEmail"><?php echo lang('event_customize_logo_field') ?>
				<div class="infoiconbg bg_5f6062 customize_tooltip">
					<span class="show_tool_tip" title="<?php echo lang('tooltip_customize_logo_field') ?>"> </span>
				</div>
			</label>
			<div class="controls" id="attch_pickfiles_logo">
				<div id="attch_dropArea_logo">
					<div class="dashbdr bdr_5f6062 clr_5f6062">
						<?php echo lang('comm_file_upload_drop'); ?>
						<div class="filedropbg_cusform">
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="control-group mb10">
			<div class="controls">
					<?php echo lang('event_customize_logo_size_msg') ?>
			</div>
		</div>
		
		<div class="control-group mb10  <?php echo (empty($LogoFileName))?'dn':' '; ?>" id="attach_file_list_main_logo" >
		<label class="control-label cusinp_font" for="inputEmail">&nbsp;</label>
			<div class="controls " id="attch_file_list_logo" >
					<?php if(!empty($LogoFileName)){ 
					$fileId= remove_extension($LogoFileName);	
						?>
						 <div class="attchadddedfile_logo" id="<?php echo $fileId; ?>"> 
								<div class="imagename_cont pr mr_5px"><?php echo $LogoFileName; ?>
									<div class="pa ptr top_6 right_10 delete_file" deleteurl='event/deletecustomizeformslogo' id="cancel_<?php echo $fileId; ?>" deleteid="<?php echo $LogoFileId; ?>"  fileid="<?php echo $fileId; ?>">
										<img src="<?php echo IMAGE; ?>close_icon.png">
									</div>
								</div>
							</div> 
					<?php } ?>	
			</div>
		</div>
		
		<div class="spacer10">
		</div>
		<div class="row-fluid">
			<?php 
				$extraSave 	= 'class="submitbtn_cus  bg_5f6062 clr_99BBCC submitbtn_logo" ';
				echo form_submit('save',lang('comm_save_changes'),$extraSave);
			?>
		</div>
		<div class="spacer36">
		</div>
	</div>
	<?php echo form_close();?>	
</div>
<!-- /commonform_bg -->

<script type="text/javascript" language="javascript">
	
	//This function is used to 
	$( ".submitbtn_logo").click(function() {
		$('#div_ar2RA_a').trigger('click');
		custom_popup('Event logo uploaded successfully.',true);
		return false;	 
	});
		
	// Custom example logic for file upload
	var postData = {event_id : '<?php echo $eventId; ?>',action_post : 'logo'}
	var uploadUrl = baseUrl+'event/customizeformsmedia';

	var otherObj = {
			pickfiles:"attch_pickfiles_logo", 
			dropArea:"attch_dropArea_logo", 
			FileContainer:"attch_FileContainer_logo", 
			fileTypes:"jpg,jpeg,png,gif", 
			deleteUrl:baseUrl+'event/deletecustomizeformslogo', 
			maxfilesize:"10mb", 
			multiselection:false, 
			isFileList:true, 
			uniqueNames:true, 
			autoStart:false, 
			isDelete:true, 
			maxfileupload:1, 
			mainDivFileList:"attach_file_list_main_logo",
			listAddFile:"attch_file_list_logo",
			adddedfile:"attchadddedfile_logo",
		}

	// initilization upload config
	upload_file(uploadUrl,postData,otherObj);
	
	
</script>	
		
