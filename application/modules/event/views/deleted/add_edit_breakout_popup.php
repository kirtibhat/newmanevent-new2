<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//--------add type fo registration fields---------//	

$breakoutNameValue = (!empty($breakoutdata->breakout_name))?$breakoutdata->breakout_name:''; 
$breakoutDateValue = (!empty($breakoutdata->date))?dateFormate($breakoutdata->date, "d M Y"):''; 
$breakoutStartTimeValue = (!empty($breakoutdata->start_time))?timeFormate($breakoutdata->start_time,'h:i A'):''; 
$breakoutEndTimeValue = (!empty($breakoutdata->end_time))?timeFormate($breakoutdata->end_time,'h:i A'):''; 
$headerAction = ($breakoutIdValue > 0)?lang('event_breakout_edit_title'):lang('event_breakout_add_title'); 


$formTypeofBreakout = array(
		'name'	 => 'formTypeofBreakout',
		'id'	 => 'formTypeofBreakout',
		'method' => 'post',
		'class'  => 'form-horizontal mt10',
	);
	
$breakoutName = array(
		'name'	=> 'breakoutName',
		'value'	=> $breakoutNameValue,
		'id'	=> 'breakoutName',
		'type'	=> 'text',
	//	'placeholder'	=> 'Breakout Name',
		'required'	=> '',
	);

$breakoutDate = array(
		'name'	=> 'breakoutDate',
		'value'	=>  $breakoutDateValue,
		'id'	=> 'breakoutDate',
		'type'	=> 'text',
		'required'	=> '',
		'readonly' => '',
		'class' => 'datepicker',
		'data-format' => 'yyyy-MM-dd',
	);
	
	
$blockStartTime = array(
		'name'	=> 'blockStartTime',
		'value'	=>  $breakoutStartTimeValue,
		'id'	=> 'blockStartTime',
		'type'	=> 'text',
		'required'	=> '',
		'readonly' => '',
		'class' => 'width_100px timepicker',
	);

$blockEndTime = array(
		'name'	=> 'blockEndTime',
		'value'	=>  $breakoutEndTimeValue,
		'id'	=> 'blockEndTime',
		'type'	=> 'text',
		'required'	=> '',
		'readonly' => '',
		'class' => 'width_100px timepicker',
	);		
		
$breakoutId = array(
		'name'	=> 'breakoutId',
		'value'	=> $breakoutIdValue,
		'id'	=> 'breakoutId',
		'type'	=> 'hidden',
	);


?>

<!--------Add braakout div start-------->
	<div class="modal fade alertmodelbg dn" id="add_edit_brakout_popup_div" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		
		<div class="modal-content">
			
			<div class="modal-header bg_0073ae">
				<h4 class="modal-title"><?php echo $headerAction; ?></h4>
			</div>
			<?php  echo form_open(base_url('event/addbreakouttype'),$formTypeofBreakout); ?>
				<div class="modal-body overflow_visible">
					<!---<h4><?php //echo lang('event_breakout_popup_add_msg'); ?></h4>--->
					<div class="spacer10">	</div>
					<div class="control-group mb10 popupinput">
						<label for="inputEmail" class="control-label position_R"><?php echo lang('event_breakout_popup_add_field'); ?>
							<font class="mandatory_cls">*</font>
						
							<div class="infoiconbg infoegist infoiconbg_popupp eventdetai_tooltip bg_F89728">
								<span class="show_tool_tip" title="<?php echo lang('tooltip_breakout_popup_add_field'); ?>"> </span>
							</div>
						
						</label>
						<div class="controls">
							<?php echo form_input($breakoutName); ?>
						</div>
					</div>
					
					<div class="control-group mb10 calendra_brakout_popup popupinput">
						<label for="inputEmail" class="control-label position_R"><?php echo lang('event_breakkout_date'); ?>
							<font class="mandatory_cls">*</font>
							
							<div class="infoiconbg infoegist infoiconbg_popupp eventdetai_tooltip bg_F89728">
								<span class="show_tool_tip" title="<?php echo lang('tooltip_breakkout_date'); ?>"> </span>
							</div>
							
						</label>
						<div class="controls">
							<div id="datetimepicker4" class="input-append date">
								<?php echo form_input($breakoutDate); ?>
							</div>
						</div>
					</div>
					
					<div class="control-group mb10 breakouttimenew popupinput">
						<label for="inputEmail" class="control-label position_R"><?php echo lang('event_breakkout_time'); ?>
							<font class="mandatory_cls">*</font>
							
							<div class="infoiconbg infoegist infoiconbg_popupp eventdetai_tooltip bg_F89728">
								<span class="show_tool_tip" title="<?php echo lang('tooltip_breakkout_time'); ?>"> </span>
							</div>
							
						</label>
						<div class="controls registrat_setup">
							<div class="inputnubcontainer ml0 customNumbertype pull-left calender_container_new">
								<?php echo form_input($blockStartTime); ?>
							</div>
							<div class="customcheckbox_div allowword custom_finish calender_container_new calle_last">
								<label class="control-label mt10 mr15 pl0" for="inputEmail">Finish</label>
								<div class="inputnubcontainer ml0 customNumbertype pull-left">
									<?php echo form_input($blockEndTime); ?>
								</div>
							</div>
						</div>
					</div>
			
			</div>
				<div class="spacer15">	</div>
			<div class="modal-footer">
				<?php 
					echo form_hidden('eventId', $eventId);
					echo form_input($breakoutId);
					$extraCancel 	= 'class="btn btn-default custombtn" data-dismiss="modal" ';
					$extraSave 	= 'class="btn btn-primary custombtn custombtn_save" ';
					echo form_button('cancel',lang('comm_cancle'),$extraCancel);
					echo form_submit('save',lang('comm_save'),$extraSave);
				?>
			</div>
		</div>
		<?php echo form_close(); ?>
	</div>
</div>
<!--------Add braakout div end-------->
