<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$registrantId 		= $registrantsData->id;
$registrantPerFrm   = $registrantId.'_perosnal'.$formSectionName;
$nextRegistrantFrm   = $nextRecordId.'_perosnal'.$formSectionName;
$regisFormName 		= $registrantsData->registrant_name;

//--------------personal form details---------------//

$formPersonalDetails = array(
		'name'	 => 'formPersonalDetails'.$registrantId.$formSectionName,
		'id'	 => 'formPersonalDetails'.$registrantId.$formSectionName,
		'method' => 'post',
		'class'  => 'form-horizontal mb0',
);
	
//get master field values array	
$fieldsmastervalue = fieldsmastervalue();

$emailfield = array(
				'name'	=> 'emailfield',
				'value'	=> '',
				'id'	=> 'emailfield',
				'type'	=> 'text',
				'placeholder'	=> 'Displayed & Mandatory',
				'readonly' => '',
				'class'	=> 'small short_field',
);	

?>

	<!---personal details form fields start---->
		
	<div class="addpersonaldiv">
	
		<?php 
		
		//get user create field data
		$whereFrmCond  = array('event_id'=>$eventId,'form_id'=>'1','registrant_id'=>$registrantId);
		$formfielddata = getDataFromTabel('custom_form_fields','*',$whereFrmCond);

		if(!empty($formfielddata)){
					foreach($formfielddata as $formfield){
					
						$fieldCreateBy = $formfield->field_create_by;
						
									if($fieldCreateBy=="1"){		
									?>
												<?php
												echo form_hidden('fieldid[]', $formfield->id);
												if($formfield->field_name=="email"){ 
												?>
													
													<div class="row-fluid-15">
														<label for="bo_email">Email<span class="astrik">*</span>
															<span class="info_btn"><span class="field_info xsmall"><?php echo lang('title_registrant_email'); ?></span></span>
														</label>
														<?php 
																	$fieldName= 'field_'.$formfield->id;	
																	echo form_input($emailfield);
																	$fieldName= 'field_'.$formfield->id;	
																	echo form_hidden($fieldName, $formfield->field_status);
														?>
													</div>
											
												<?php }else {  ?>
													
													 <div class="row-fluid-15">
														<label for="bo_title"><?php echo ucwords($formfield->field_name); ?></label>
														<?php 
																$other = 'id="field_'.$formfield->id.'" class="small custom-select short_field"';
																$fieldName= 'field_'.$formfield->id;	
																echo form_dropdown($fieldName, $fieldsmastervalue, $formfield->field_status,$other);	
														?>
														<?php  if($formfield->is_editable=="1"){ ?>
																<span class="pull-right select_edit "><a class="eventsetup_btn td_btn add_personal_form_field" registrantid="<?php echo $registrantId; ?>" customfieldid="<?php echo $formfield->id; ?>" href="javascript:void(0)"><span>&nbsp;</span></a></span>
														<?php } ?>	
													</div>
												 <?php } ?>
								<?php  } //end if
				} //end loop ?>
		
					
					<div class="row-fluid-15 addtype">
                           <a class="add_btn medium add_personal_form_field" href="javascript:void(0)" registrantid="<?php echo $registrantId; ?>" customfieldid="0" id="add_field"><?php echo lang('event_registration_custom_field'); ?></a>
                    </div>
						
				
					<?php  if(!empty($formfielddata)){
					foreach($formfielddata as $key=>$formfield){
					
						$fieldCreateBy = (isset($formfield->field_create_by) && $formfield->field_create_by=="0")?$formfield->field_create_by:NULL;
						if($fieldCreateBy=="0"){		
						?>
						<div class="row-fluid-15" id="registrantcustomfield_<?php echo $formfield->id.'_'.$key; ?>">
							<label for="bo_title"><?php echo ucwords($formfield->field_name); ?></label>
							<?php
											echo form_hidden('fieldid[]', $formfield->id);
											$other = 'id="field_'.$formfield->id.'" class="small custom-select short_field"';
											$fieldName= 'field_'.$formfield->id;
											echo form_dropdown($fieldName, $fieldsmastervalue, $formfield->field_status,$other);	
											
											?>
												 <span class="pull-right select_edit">
														<a class="eventsetup_btn td_btn add_personal_form_field" href="javascript:void(0)" registrantid="<?php echo $registrantId; ?>" customfieldid="<?php echo $formfield->id; ?>"><span>&nbsp;</span></a>
														<a class="delete_btn td_btn delete_personal_form_field"  deleteid="<?php echo $formfield->id; ?>" href="javascript:void(0)"><span> </span></a>
												 </span>
											
						</div>
						
						<?php }  //end if
						} // end loop 
					} // end if ?>
	 <?php } //end form field ?>
	</div>
		
	<!---personal details form fields end---->

<script type="text/javascript">
		//personal details save
		ajaxdatasave('formPersonalDetails<?php echo $registrantId.$formSectionName; ?>','<?php echo $this->uri->uri_string(); ?>',true,true,false,false,false,'#showhideformdiv<?php echo $registrantPerFrm; ?>','#showhideformdiv<?php echo $nextRegistrantFrm; ?>');
</script>
