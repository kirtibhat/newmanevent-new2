<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//--------add type fo registration fields---------//	

$sideEventNameValue = (!empty($sideeventdata->side_event_name))?$sideeventdata->side_event_name:''; 

$formAddSideEvent = array(
		'name'	 => 'formAddSideEvent',
		'id'	 => 'formAddSideEvent',
		'method' => 'post',
		'class'  => 'form-horizontal mt10',
	);
	
$sideEventName = array(
		'name'	=> 'sideEventName',
		'value'	=> $sideEventNameValue,
		'id'	=> 'sideEventName',
		'type'	=> 'text',
	//	'placeholder'	=> 'Side Event Name',
		'required'	=> '',
	);
	
$sideEventId = array(
		'name'	=> 'sideEventId',
		'value'	=> $sideeventid,
		'id'	=> 'sideEventId',
		'type'	=> 'hidden',
	);	
	
$formAction = array(
		'name'	=> 'formAction',
		'value'	=> $formActionValue,
		'id'	=> 'formAction',
		'type'	=> 'hidden',
	);		
	

?>

<!--------add and edit side event popup div start-------->
<div class="modal fade alertmodelbg dn" id="add_side_event_popup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		
		<div class="modal-content">
			
			<div class="modal-header bg_0073ae">
				<h4 class="modal-title"><?php echo $headerAction; ?> </h4>
			</div>
			<?php  echo form_open(base_url('event/addeditduplicatesideeventview'),$formAddSideEvent); ?>
				<div class="modal-body">
				
					<div class="spacer15"></div>
				<!---<h4><?php //echo lang('event_side_popup_add_msg'); ?></h4>--->
				
					<div class="control-group mb10">
						<label for="inputEmail" class="control-label"><?php echo lang('event_side_popup_add_field'); ?>
							<font class="mandatory_cls">*</font>
						</label>
						<div class="controls">
							<?php echo form_input($sideEventName); ?>
						</div>
					</div>
			</div>
			<div class="modal-footer">
				<?php 
					echo form_hidden('eventId', $eventId);
					echo form_input($sideEventId);
					echo form_input($formAction);
					$extraCancel 	= 'class="btn btn-default custombtn" data-dismiss="modal" ';
					$extraSave 	= 'class="btn btn-primary custombtn custombtn_save" ';
					echo form_button('cancel',lang('comm_cancle'),$extraCancel);
					echo form_submit('save',lang('comm_save'),$extraSave);
				?>
			</div>
		</div>
		<?php echo form_close(); ?>
	</div>
</div>

