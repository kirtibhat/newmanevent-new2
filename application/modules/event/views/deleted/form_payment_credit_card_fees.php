<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

$creditcarddetails          = (!empty($creditcarddetails))?$creditcarddetails[0]:'';
$creditCardId               = (!empty($creditcarddetails->id))?$creditcarddetails->id:'0';
$whoWillPayFeeValue         = (isset($creditcarddetails->who_will_pay_fee))? $creditcarddetails->who_will_pay_fee : '';
$acceptedCreditCardsValue   = (!empty($creditcarddetails->accepted_credit_cards))?json_decode($creditcarddetails->accepted_credit_cards):'';

$creditCardArray = array('1'=>'visa','2'=>'american express','3'=>'mastercard','4'=>'diners');

$whoWillPayFeeRegisValue  = false;
$whoWillPayFeeOrgaiValue  = false;

//set value
if($whoWillPayFeeValue=='0'){
  $whoWillPayFeeRegisValue = true;
  $whoWillPayFeeOrgaiValue = false;
}else{
  $whoWillPayFeeRegisValue = false;
  $whoWillPayFeeOrgaiValue = true;
}

//define current and next form div id
$FormDivId = 'FourthForm';
$nextFormDivId = 'FifthForm';

$formCreditCardFees = array(
    'name'   => 'formCreditCardFees',
    'id'   => 'formCreditCardFees',
    'method' => 'post',
    'class'  => 'form-horizontal',
    'data-parsley-validate' => '',
  );
  
$whoWillPayFeeRegis = array(
    'name'  => 'whoWillPayFee',
    'value' => '0',
    'id'  => 'payTheRegistrationFeeYesCC',
    'type'  => 'radio',
    'required'  => '',
    'checked' => $whoWillPayFeeRegisValue,
    'data-parsley-error-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',    
  );  

$whoWillPayFeeOrgai = array(
    'name'  => 'whoWillPayFee',
    'value' => '1',
    'id'  => 'whoWillPayFeeOrgaiCC',
    'type'  => 'radio',
    'required'  => '',
    'checked' => $whoWillPayFeeOrgaiValue,
  );  
      
  
?>


<div class="panel event_panel">
  <div class="panel-heading ">
      <h4 class="panel-title medium dt-large heading_btn ">
      <a class="" data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
      <?php echo lang('event_payment_cardi_card_fees'); ?>
     </a>
      </h4>
  </div>  
  <div style="height: auto;" id="collapseFour" class="accordion_content collapse apply_content">
  <?php echo form_open($this->uri->uri_string(),$formCreditCardFees); ?>
    <div class="form-horizontal form_open_chk" id="showhideformdiv<?php echo $FormDivId; ?>">    
      <div class="panel-body ls_back dashboard_panel small">
        <div class="row-fluid-15">

          <label for="info_org"><?php echo lang('event_payment_pay_credit_fees'); ?><span class="astrik">*</span>
            <span class="info_btn">
                <span class="field_info xsmall"><?php echo lang('tooltip_payment_pay_credit_fees'); ?>
                  </span>
              </span>
          </label>
          
          <div class="radio_wrapper ">
            <?php echo form_radio($whoWillPayFeeRegis); ?> 
            <label for="payTheRegistrationFeeYesCC"><?php echo lang('event_paymen_paid_credit_by_1'); ?></label>
            <?php echo form_radio($whoWillPayFeeOrgai); ?>
            <label for="whoWillPayFeeOrgaiCC"><?php echo lang('event_paymen_paid_credit_by_2'); ?></label>
            <?php echo form_error('whoWillPayFee'); ?>
          </div>
          <!--end of session sub category-->          
        </div>
        
        <div class="row-fluid-15">                      
          <label for="info_org" class="ml_m8"><?php echo lang('event_payment_accepted_card'); ?>
            <span class="info_btn">
                <span class="field_info xsmall"><?php echo lang('event_payment_accepted_card'); ?></span>
              </span>
          </label>
          <div class="session_subcat l_margin">
            <?php
            if(!empty($creditCardArray)) {
              foreach($creditCardArray as $key=>$value){ 
              $creditCardIdValue = $key;
              $creditCardName = $value;
              $acceptedCreditCardsChk = false;
              //check selected credit card and set true if exist
              if(!empty($acceptedCreditCardsValue)){
                foreach($acceptedCreditCardsValue as $creditCardsValue){
                  if($creditCardsValue==$creditCardIdValue){
                    $acceptedCreditCardsChk = true;
                  }
                }
              }
              $acceptedCreditCards = array(
              'name'  => 'acceptedCreditCards[]',
              'value' => $creditCardIdValue,
              'id'  => 'acceptedCreditCards'.$creditCardIdValue,
              'type'  => 'checkbox',
              'class' => 'acceptedCreditCards pull-left ml10 mr15 checkbox',
              'checked' => $acceptedCreditCardsChk,
              );  
            ?>
            <div class="row-fluid-15">
            <span class="checkbox_wrapper">
              <?php echo form_checkbox($acceptedCreditCards); ?>
              <label for="acceptedCreditCards<?php echo $creditCardIdValue; ?>"><?php echo ucwords($creditCardName); ?></label> 
            </span>
            </div>
            <?php } } ?>  
          <!--end of session sub category-->  
          </div>      
        </div>      

       <div class="btn_wrapper ">
          <a href="#collapseTwo" class="pull-left scroll_top">Top</a>
          <?php 
            echo form_hidden('eventId', $eventId);
            echo form_hidden('formActionName', 'creditCardFees');
            echo form_hidden('creditCardId', $creditCardId);
            
            //button show of save and reset
            $formButton['showbutton']   = array('save','reset');
            $formButton['labletext']  = array('save'=>lang('comm_save_changes'),'reset'=>lang('comm_reset'));
            $formButton['buttonclass']  = array('save' => 'default_btn btn pull-right medium', 'reset' => 'default_btn btn pull-right medium reset_form');
            $this->load->view('common_save_reset_button',$formButton);
          ?>
        </div>
      </div>
    </div>
  <?php echo form_close(); ?>
  </div>
</div>
<!--end of panel--> 

<script type="text/javascript">
  //payment credit card fees save 
  ajaxdatasave('formCreditCardFees','<?php echo $this->uri->uri_string(); ?>',true,true,false,false,false,'#showhideformdiv<?php echo $FormDivId; ?>','#showhideformdiv<?php echo $nextFormDivId; ?>');
</script>
