<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//set value in the variable 

$sideEventId = (!empty($sideevent->id))?$sideevent->id:'0';;
$sideEventNameValue = (!empty($sideevent->side_event_name))?$sideevent->side_event_name:'';
$sideEventLimitValue = (isset($sideevent->side_event_limit))?$sideevent->side_event_limit:'';
$sideEventVenueValue = (!empty($sideevent->venue))?$sideevent->venue:'';
$sideEventStartDateValue = (!empty($sideevent->start_datetime))?dateFormate($sideevent->start_datetime,'d M Y H:i'):'';
$sideEventEndDateValue = (!empty($sideevent->end_datetime))?dateFormate($sideevent->end_datetime,'d M Y H:i'):'';
$earlyBirdDateValue = (!empty($sideevent->earlybird_date))?dateFormate($sideevent->earlybird_date,'d M Y'):'';
$sideEventShowTimeOnFormValue = (!empty($sideevent->show_time_on_form) && $sideevent->show_time_on_form=='1' )?true:false;
$sideEventAllowEarlybirdValue = (!empty($sideevent->allow_earlybird) && $sideevent->allow_earlybird=='1' )?true:false;
//$sideEventStartTimeValue = (!empty($sideevent->start_time))?timeFormate($sideevent->start_time,'h:i A'):'';
//$sideEventEndTimeValue	 = (!empty($sideevent->end_time))?timeFormate($sideevent->end_time,'h:i A'):'';


//form create variable
$formSideEvent = array(
		'name'	 => 'formSideEvent'.$sideEventId,
		'id'	 => 'formSideEvent'.$sideEventId,
		'method' => 'post',
		'class'  => 'form-horizontal',
	);
	
$sideEventLimit = array(
		'name'	=> 'sideEventLimit'.$sideEventId,
		'value'	=> $sideEventLimitValue,
		'id'	=> 'sideEventLimit'.$sideEventId,
		'type'	=> 'text',
		'required'	=> '',
	);

$sideEventVenue = array(
		'name'	=> 'sideEventVenue'.$sideEventId,
		'value'	=> $sideEventVenueValue,
		'id'	=> 'sideEventVenue'.$sideEventId,
		'type'	=> 'text',
		'required'	=> '',
	);	


$sideEventStartDate = array(
		'name'	=> 'sideEventStartDate'.$sideEventId,
		'value'	=> $sideEventStartDateValue,
		'id'	=> 'sideEventStartDate'.$sideEventId,
		'type'	=> 'text',
		'required'	=> '',
		'readonly'	=> 'readonly',
		'class'	=> 'width50per datetimepicker',
		'data-format'	=> 'yyyy-MM-dd',
	);	
	
$sideEventEndDate = array(
		'name'	=> 'sideEventEndDate'.$sideEventId,
		'value'	=> $sideEventEndDateValue,
		'id'	=> 'sideEventEndDate'.$sideEventId,
		'type'	=> 'text',
		'required'	=> '',
		'readonly'	=> 'readonly',
		'class'	=> 'width50per datetimepicker',
		'data-format'	=> 'yyyy-MM-dd',
	);		

$sideEventShowTimeOnForm = array(
		'name'	=> 'sideEventShowTimeOnForm'.$sideEventId,
		'value'	=> '1',
		'id'	=> 'sideEventShowTimeOnForm'.$sideEventId,
		'type'	=> 'checkbox',
		'checked'	=> $sideEventShowTimeOnFormValue,
		'sideeventshowtimeonformid'	=> $sideEventId,
		'class	'	=> 'pull-left ml10 mr15 checkbox issideeventshowtimeonform',
	);	
	
/*
$sideEventStartTime = array(
		'name'	=> 'sideEventStartTime'.$sideEventId,
		'value'	=> $sideEventStartTimeValue,
		'id'	=> 'sideEventStartTime'.$sideEventId,
		'type'	=> 'text',
		'required'	=> '',
		'readonly'	=> 'readonly',
		'disabled'	=> 'disabled',
		'class'	=> 'width100px timepicker timestartendrequired'.$sideEventId,
	);	
	
$sideEventEndTime = array(
		'name'	=> 'sideEventEndTime'.$sideEventId,
		'value'	=> $sideEventEndTimeValue,
		'id'	=> 'sideEventEndTime'.$sideEventId,
		'type'	=> 'text',
		'required'	=> '',
		'readonly'	=> 'readonly',
		'disabled'	=> 'disabled',
		'class'	=> 'width100px timepicker timestartendrequired'.$sideEventId,
	);*/			
	
$sideEventAllowEarlybird = array(
		'name'	=> 'sideEventAllowEarlybird'.$sideEventId,
		'value'	=> '1',
		'id'	=> 'sideEventAllowEarlybird'.$sideEventId,
		'type'	=> 'checkbox',
		'class'	=> 'isallowearlybirddate',
		'sideeventid'	=> $sideEventId,
		'checked'	=> $sideEventAllowEarlybirdValue,
	);		
		
		
$earlyBirdDate = array(
		'name'	=> 'earlyBirdDate'.$sideEventId,
		'value'	=> $earlyBirdDateValue,
		'id'	=> 'earlyBirdDate'.$sideEventId,
		'type'	=> 'text',
		'required'	=> '',
		'readonly'	=> 'readonly',
		'disabled'	=> 'disabled',
		'class'	=> 'width50per datepicker earlybirdaction'.$sideEventId,
		'data-format'	=> 'yyyy-MM-dd',
	);

//set condition for show time on form

//$showTimeOnFormHide = 'dn';	
if($sideEventShowTimeOnFormValue=='1'){
	//$showTimeOnFormHide = '';
	//unset($sideEventStartTime['disabled']);
	//unset($sideEventEndTime['disabled']);
}

//set condition for allow early bird 
$showAllowEarlybirddate = 'dn';	
if($sideEventAllowEarlybirdValue=='1'){
	$showAllowEarlybirddate = '';
	unset($earlyBirdDate['disabled']);
}	
			
?>

<div class="commonform_bg pb0">
	
	<div class="headingbg bg_0073ae position_R">
		<div class="typeofregistV pull-left showhideaction ptr" id="<?php echo $sideEventId; ?>">
			 <?php echo ucwords($sideEventNameValue); ?>
		</div>
		<div class="contPadiv">
			<div class="icontitlecontainer right_del delete_side_event greytooltip_del" deleteid="<?php echo $sideEventId; ?>">
				 <span class="show_tool_tip" title="<?php echo lang('comm_delete'); ?>" ><div class="delet_icon"></div></span>
				<div class="icontext">
					<?php echo lang('comm_delete'); ?>
				</div>
			</div>
			
			<div class="icontitlecontainer right_edit add_edit_side_event greytooltip"  actionform="edit" sideeventid="<?php echo $sideEventId; ?>">
				<span class="show_tool_tip" title="<?php echo lang('comm_edit_title'); ?>" ><div class="edittitle_icon"></div></span>
				<div class="icontext ">
					<?php echo lang('comm_edit_title'); ?>
				</div>
			</div>
			
			<div class="icontitlecontainer right_dup add_edit_side_event greytooltip" actionform="duplicate" sideeventid="<?php echo $sideEventId; ?>" >
				<span class="show_tool_tip" title="<?php echo lang('comm_dulicate'); ?>" ><div class="duplicate_icon"></div></span>
				<div class="icontext"><?php echo lang('comm_dulicate'); ?></div>
			</div>
			
		</div>
		<div class="clearfix">
		</div>
	</div>
	
	<?php echo form_open($this->uri->uri_string(),$formSideEvent); ?>
		<div class="form-horizontal dn form_open_chk" id="showhideformdiv<?php echo $sideEventId; ?>">
		<div class="control-group mb10">
			<label class="control-label" for="inputEmail"><?php echo lang('event_side_event_limit'); ?>
				<font class="mandatory_cls">*</font>
			<div class="infoiconbg eventdetai_tooltip bg_F89728">
				<span class="show_tool_tip" title='<?php echo lang('tooltip_side_event_limit'); ?>'> </span>
			</div>
			</label>
			<div class="controls">
				<?php echo form_input($sideEventLimit); ?>
				<?php echo form_error('sideEventLimit'.$sideEventId); ?>
			</div>
		</div>
		<div class="control-group mb10 allowword">
			<label class="control-label" for="inputEmail"><?php echo lang('event_side_event_venue'); ?>
				<font class="mandatory_cls">*</font>
				<div class="infoiconbg eventdetai_tooltip bg_F89728">
					<span class="show_tool_tip" title='<?php echo lang('tooltip_side_event_venue'); ?>'> </span>
				</div>
			</label>
			<div class="controls">
				<?php echo form_input($sideEventVenue); ?>
				<?php echo form_error('sideEventVenue'.$sideEventId); ?>
			</div>
		</div>
		<div class="control-group mb10">
			<label class="control-label" for="inputEmail"><?php echo lang('event_side_event_start_data'); ?>
				<font class="mandatory_cls">*</font>
			</label>
			<div class="controls side_event_date_new">
				<?php echo form_input($sideEventStartDate); ?>
				<?php echo form_error('sideEventStartDate'.$sideEventId); ?>
			</div>
		</div>
		
		<div class="control-group mb10">
			<label class="control-label" for="inputEmail"><?php echo lang('event_side_event_end_data'); ?>
				<font class="mandatory_cls">*</font>
			</label>
			<div class="controls side_event_date_new">
				<?php echo form_input($sideEventEndDate); ?>
				<?php echo form_error('sideEventEndDate'.$sideEventId); ?>
			</div>
		</div>
		
		
		<div class="control-group mb10 allowword">
			<label class="control-label" for="inputEmail"><?php echo lang('event_side_event_time_on_form'); ?></label>
			<div class="controls checkradiobg setupcheck">
				<div class="checkbox pl0 position_R">
					<?php echo form_checkbox($sideEventShowTimeOnForm); ?>
					<label for="sideEventShowTimeOnForm<?php echo $sideEventId; ?>" class="width_auto ml0 mt10 eventDcheck"></label>
				</div>
			</div>
		</div>
		
		<!---
		<div class="control-group mb10 <?php //echo $showTimeOnFormHide; ?>" id="isallow_timestartend_<?php //echo $sideEventId; ?>">
			<label class="control-label" for="inputEmail"><?php //echo lang('event_side_time_start'); ?>
			<font class="mandatory_cls">*</font>
			</label>
			<div class="controls registrat_setup">
				<div class="inputnubcontainer ml0 customNumbertype pull-left calender_container">
					<?php //echo form_input($sideEventStartTime); ?>
					<?php //echo form_error('sideEventStartTime'.$sideEventId); ?>
				</div>
				<div class="customcheckbox_div allowword custom_finish calender_container calle_last">
					<label class="control-label mt10 mr15 pl0" for="inputEmail"><?php echo lang('event_side_time_finish'); ?></label>
					<div class="inputnubcontainer ml0 customNumbertype pull-left">
						<?php //echo form_input($sideEventEndTime); ?>
						<?php //echo form_error('sideEventEndTime'.$sideEventId); ?>
					</div>
				</div>
			</div>
		</div>
		--->
		<div class="control-group mb10 allowword">
			<label class="control-label" for="inputEmail"><?php echo lang('event_side_allow_earlybird'); ?></label>
			<div class="controls checkradiobg setupcheck">
				<div class="checkbox pl0 position_R">
					<?php echo form_checkbox($sideEventAllowEarlybird); ?>
					<label for="sideEventAllowEarlybird<?php echo $sideEventId; ?>" class="width_auto ml0 mt10 eventDcheck"></label>
				</div>
			</div>
		</div>
		
		<div class="control-group mb10 <?php echo $showAllowEarlybirddate; ?>" id="is_show_allow_earlybirddate<?php echo $sideEventId; ?>">
			<label class="control-label" for="inputEmail"><?php echo lang('event_side_earlybird_date'); ?>
				<font class="mandatory_cls">*</font>
			</label>
			<div class="controls side_event_date_new">
				<?php echo form_input($earlyBirdDate); ?>
				<?php echo form_error('earlyBirdDate'.$sideEventId); ?>
			</div>
		</div>
		
		<div class="formparag pb_20">
			<a href="<?php echo base_url('event/setupregistrant'); ?>" target="_blank" class="add_personal_form_field"  customfieldid="0">
			<div class="addbtn_form">
			</div>
			<div class="addtype">
				<?php echo lang('event_side_add_registrant'); ?>
			</div>
			</a>
			<div class="clearfix">	</div>
			
		</div>
		
		
		<?php if(!empty($eventregistrants)) {  
			
			foreach($eventregistrants as $registrants) {
				
			$registrantId = $registrants->id;
			$regisSideEventId = $sideEventId.'_'.$registrantId;
			$registrantName = $registrants->registrant_name;
			$allowEarlybird = $registrants->allow_earlybird;
			
			//prepare send data	
			$sendDetailViewData['registrantId'] 	    = $registrantId;	
			$sendDetailViewData['singleDayRegistId']	= 0;	
			$sendDetailViewData['regisSideEventId'] 	= $regisSideEventId;	
			$sendDetailViewData['registrantName']   	= $registrantName;	
			$sendDetailViewData['allowEarlybird']   	= $allowEarlybird;	
			$sendDetailViewData['sideEventId']   		= $sideEventId;	
			
			$this->load->view('side_event_registrant_list_details_view',$sendDetailViewData);
				
					//This section for single day registrant details listing start
					$where = array('registrant_id'=>$registrantId);
					$singleDayRegistList = getDataFromTabel('event_registrant_daywise','id,day',$where);
					
					if(!empty($singleDayRegistList)){
						foreach($singleDayRegistList as $singleDayRegistArray){ 
							$singleDayRegistId = $singleDayRegistArray->id; // single day registrant id
							$singleDayRegistDay= $singleDayRegistArray->day;
							$singleDayRegistDayName= $registrants->registrant_name.' '.$singleDayRegistDay; // single day registrant name
							$singleDayRegistSideEventId = $regisSideEventId.'_'.$singleDayRegistId; // single day registrant row id
							
							//prepare send data	
							$sendDetailViewData['registrantId'] 	  = $registrantId;	
							$sendDetailViewData['singleDayRegistId']  = $singleDayRegistId;
							$sendDetailViewData['regisSideEventId']   = $singleDayRegistSideEventId;	
							$sendDetailViewData['registrantName']     = $singleDayRegistDayName;	
							$sendDetailViewData['allowEarlybird']     = $allowEarlybird;	
							
							$this->load->view('side_event_registrant_list_details_view',$sendDetailViewData);	
						}	
					}
				} 
			} ?>
	
		<div class="spacer10">
		</div>
		<div class="row-fluid">
			<?php 
				echo form_hidden('eventId', $eventId);
				echo form_hidden('sideEventId', $sideEventId);
				
				//button show of save and reset
				$formButton['saveButtonId'] = 'id="'.$sideEventId.'"'; // click button id
				$formButton['showbutton']   = array('save','reset');
				$formButton['labletext'] 	= array('save'=>lang('comm_save_changes'),'reset'=>lang('comm_reset'));
				$formButton['buttonclass']  = array('save'=>'submitbtn_cus mr_16 submit_regis_type','reset'=>'submitbtn_cus reset_form');
				$this->load->view('common_save_reset_button',$formButton);
			?>
		</div>
		<div class="spacer36">
		</div>
	</div>
	<?php echo form_close(); ?>
</div>
<!-- /commonform_bg -->

<script type="text/javascript">
	//side event dtails save type details save
	ajaxdatasave('formSideEvent<?php echo $sideEventId; ?>','<?php echo $this->uri->uri_string(); ?>',false,true,false,false,false,'#showhideformdiv<?php echo $sideEventId; ?>','#showhideformdiv<?php echo $nextRecordId; ?>');
</script>
