<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//set breakout form data
$breakoutId 			= $formdata->id;
$breakoutName 			= (!empty($formdata->breakout_name))?$formdata->breakout_name:'';
$breakoutDate 		    = (!empty($formdata->date))?dateFormate($formdata->date, "d F"):'';
$breakoutStartTimeValue = (!empty($formdata->start_time))?timeFormate($formdata->start_time,'h:i A'):''; 
$breakoutEndTimeValue 	= (!empty($formdata->end_time))?timeFormate($formdata->end_time,'h:i A'):''; 
$numberOfBlockVal 		= (!empty($formdata->number_of_blocks))?$formdata->number_of_blocks:'0';
$numberOfStreamsVal 	= (!empty($formdata->number_of_streams))?$formdata->number_of_streams:'0';
$numberOfSessionVal     = (!empty($formdata->number_of_session))?$formdata->number_of_session:'0';
$moveRegistrantVal 	    = (!empty($formdata->registrant_move_in_stream))?'1':'0';

//get value by breakout id 
$where = array('breakout_id'=>$breakoutId);
//$breakoutBlockData = getDataFromTabel('breakout_block','*',$where,'','block','ASC');
$breakoutStreamData = getDataFromTabel('breakout_stream','*',$where,'','stream','ASC');

//$breakoutSessionData = getDataFromTabel('breakout_session','*',$where,'','session','ASC');

//set value in variable
if($moveRegistrantVal){
	$moveRegistrantYesVal = true;
	$moveRegistrantNoVal = false;	
}else{
	$moveRegistrantYesVal = false;
	$moveRegistrantNoVal = true;	
}

//form create variable
$formBreakoutSetup = array(
		'name'	 => 'formBreakoutSetup'.$breakoutId,
		'id'	 => 'formBreakoutSetup'.$breakoutId,
		'method' => 'post',
		'class'  => 'form-horizontal',
	);
	
$breakoutNumberOfBlocks = array(
		'name'	=> 'breakoutNumberOfBlocks'.$breakoutId,
		'value'	=> $numberOfBlockVal,
		'id'	=> 'breakoutNumberOfBlocks'.$breakoutId,
		'type'	=> 'text',
		'class' => 'width_50px clr_425968 spinner_selectbox breakoutNumberOfBlocks',
		'blockbreakoutid'	=> $breakoutId,
		'readonly' => '',
		'min' => '0',
		);	
	
$breakoutNumberOfStreams = array(
		'name'	=> 'breakoutNumberOfStreams'.$breakoutId,
		'value'	=> $numberOfStreamsVal,
		'id'	=> 'breakoutNumberOfStreams'.$breakoutId,
		'type'	=> 'text',
		'class' => 'width_50px clr_425968 spinner_selectbox breakoutNumberOfStreams',
		'streambreakoutid'	=> $breakoutId,
		'readonly' => '',
		'min' => '0',
		);	
		
/*
$breakoutNumberOfSession = array(
		'name'	=> 'breakoutNumberOfSession'.$breakoutId.'_1',
		'value'	=> $numberOfSessionVal,
		'id'	=> 'breakoutNumberOfSession'.$breakoutId.'_1',
		'type'	=> 'text',
		'class' => 'width_50px clr_425968 spinner_selectbox breakoutNumberOfSession',
		'sessionbreakoutid'	=> $breakoutId,
		'readonly' => '',
		'min' => '0',
		);				

$moveRegistrantYes = array(
		'name'	=> 'moveRegistrant'.$breakoutId,
		'value'	=> '1',
		'id'	=> 'moveRegistrantYes'.$breakoutId,
		'type'	=> 'radio',
		'required'	=> '',
		'class'	=> 'moveRegistrant',
		'checked'	=> $moveRegistrantYesVal,
		
	);	

$moveRegistrantNo = array(
		'name'	=> 'moveRegistrant'.$breakoutId,
		'value'	=> '0',
		'id'	=> 'moveRegistrantNo'.$breakoutId,
		'type'	=> 'radio',
		'required'	=> '',
		'class'	=> 'moveRegistrant',
		'checked'	=> $moveRegistrantNoVal,
	);
*/		
	
$blockBreakoutNumbHidden = array(
						'name'	=> 'blockBreakoutNumbHidden'.$breakoutId,
						'value'	=> $numberOfBlockVal,
						'id'	=> 'blockBreakoutNumbHidden'.$breakoutId,
						'type'	=> 'hidden',
					);
							
	
$streamsBreakoutNumbHidden = array(
						'name'	=> 'streamsBreakoutNumbHidden'.$breakoutId,
						'value'	=> $numberOfStreamsVal,
						'id'	=> 'streamsBreakoutNumbHidden'.$breakoutId,
						'type'	=> 'hidden',
					);	
/*							
$sessionStreamsBrkoutNoHidden = array(
						'name'	=> 'sessionStreamsBrkoutNoHidden'.$breakoutId,
						'value'	=> $numberOfStreamsVal,
						'id'	=> 'sessionStreamsBrkoutNoHidden'.$breakoutId,
						'type'	=> 'hidden',
					);	
							
$sessionBreakoutNumbHidden = array(
						'name'	=> 'sessionBreakoutNumbHidden'.$breakoutId,
						'value'	=> '1',
						'id'	=> 'sessionBreakoutNumbHidden'.$breakoutId,
						'type'	=> 'hidden',
					);						
*/				
									
	?>

<div class="commonform_bg pb0">
	<div class="headingbg bg_0073ae position_R ">
		<div class="typeofregistV pull-left showhideaction width_355px ft_20 ptr" id="<?php echo $breakoutId; ?>">
			<?php echo $breakoutName.' '.$breakoutDate;?> ( <?php echo $breakoutStartTimeValue; ?> - <?php echo $breakoutEndTimeValue; ?>)
		</div>
		<div class="contPadiv">
			<div class="icontitlecontainer right_del delete_breakout greytooltip_del" deleteid="<?php echo $breakoutId; ?>">
				<span class="show_tool_tip" title="<?php echo lang('comm_delete'); ?>" ><div class="delet_icon"></div></span>
				<div class="icontext">
					<?php echo lang('comm_delete'); ?>
				</div>
			</div>
			<div class="icontitlecontainer right_edit add_edit_breakouts greytooltip" breakoutid="<?php echo $breakoutId; ?>">
				<span class="show_tool_tip" title="<?php echo lang('comm_edit_title'); ?>" ><div class="edittitle_icon"></div></span>
				<div class="icontext">
					<?php echo lang('comm_edit_title'); ?>
				</div>
			</div>

			<div class="icontitlecontainer right_dup duplicate_breakout greytooltip" eventid="<?php echo $eventId; ?>" breakoutid="<?php echo $breakoutId; ?>">
				 <span class="show_tool_tip" title="<?php echo lang('comm_dulicate'); ?>" ><div class="duplicate_icon"></div></span>
				 <div class="icontext"><?php echo lang('comm_dulicate'); ?></div>
			</div>
			
		</div>
		<div class="clearfix">
		</div>
	</div>
		<?php
		   echo form_open($this->uri->uri_string(),$formBreakoutSetup);	
		   echo form_input($blockBreakoutNumbHidden); //set hidden field for get value for block 
		   echo form_input($streamsBreakoutNumbHidden); //set hidden field for get value for stream 
		   //echo form_input($sessionStreamsBrkoutNoHidden); //set hidden field for get value for session & session
		   //echo form_input($sessionBreakoutNumbHidden);//set hidden field for get value for session  
		?>
		   
		<div class="form-horizontal dn form_open_chk" id="showhideformdiv<?php echo $breakoutId; ?>">
	
		<div class="control-group mb10">
			   <div class="formparag mb20 setupbreak_inheading">STEP 1 - Setup date of breakout and limits</div>
			   
			<label class="control-label lineH16" for="inputEmail"><?php echo lang('event_breakkout_many_stream'); ?>
				<div class="infoiconbg" title="<?php echo lang('tooltip_breakkout_many_stream'); ?>">
					
				</div>
			</label>
			<div class="controls breakout_setup">
				<div class="inputnubcontainer ml0 clicknumberstream">
					<?php 
						echo form_input($breakoutNumberOfStreams);
						echo form_error('breakoutNumberOfStreams'.$breakoutId); 
					?>
				</div>
			</div>
		</div>
		
		<!--- listing of stream div start--->
		   
			<div id="stream_data<?php echo $breakoutId; ?>">
				<?php if(!empty($breakoutStreamData)){
					foreach($breakoutStreamData as $streamData){ 
						$streamSerial 			= $streamData->stream;
						$breakoutStreamId 		= $streamData->id;
						$BreakoutStreamRowId 	= $breakoutId.'_'.$streamSerial;
					    //to get session by stream id
					 
					?>
					
					<div id="streams_breakout_row<?php echo $BreakoutStreamRowId;  ?>">
							<div class="control-group mb10">
								<label class="control-label" for="inputEmail"><?php echo lang('event_breakkout_stream_name'); ?> <?php echo $streamSerial; ?>
									<font class="mandatory_cls">*</font>
								</label>
								<div class="controls">
									<input type="text" class="streamtitlechange" breakoutid="<?php echo $breakoutId; ?>" streamrowid="<?php echo $streamSerial; ?>" required="" id="breakoutStreamName<?php echo $BreakoutStreamRowId ?>" value="<?php echo $streamData->stream_name; ?>" name="breakoutStreamName<?php echo $BreakoutStreamRowId ?>">
								</div>
							</div>
							<div class="control-group mb10">
								<label class="control-label" for="inputEmail"><?php echo lang('event_breakkout_venue_stream'); ?> <?php echo $streamSerial; ?>
									<font class="mandatory_cls">*</font>
								</label>
								<div class="controls">
									<input type="text" class="" required="" id="breakoutStreamVenue<?php echo $BreakoutStreamRowId ?>" value="<?php echo $streamData->stream_venue; ?>" name="breakoutStreamVenue<?php echo $BreakoutStreamRowId ?>">
									<input type="hidden" name="breakoutStreamId<?php echo $breakoutId; ?>[<?php echo $streamData->stream; ?>]" value="<?php echo $breakoutStreamId; ?>" id="breakoutStreamId<?php echo $BreakoutStreamRowId ?>">
								</div>
							</div>
							<div class="control-group mb10">
								<label class="control-label" for="inputEmail"><?php echo lang('event_breakkout_stream_limit'); ?> <?php echo $streamSerial; ?>
									<font class="mandatory_cls">*</font>
								</label>
								<div class="controls">
										<input type="text" class="" required="" id="breakoutLimit<?php echo $BreakoutStreamRowId ?>" value="<?php echo $streamData->block_limit; ?>" name="breakoutLimit<?php echo $BreakoutStreamRowId ?>">
								</div>
							</div>
					</div>
				
				<?php	}  } ?>
		</div>
		
	    <div class="formparag mb20">
				<div class="bdrb_block"></div>
			</div>
		<!--- listing of session div start--->
			<div id="session_list_div<?php echo $breakoutId; ?>">
		          
			<?php 
			   
			    if(!empty($breakoutStreamData))
			    {
			        $count='0';
			        foreach($breakoutStreamData as $streamData)
			        {
						 $where=array('stream_id'=>$streamData->id); 
						 $breakoutSessionData = getDataFromTabel('breakout_session','*',$where,'','session','ASC');
                         $count+=1;
						 $sessonBrkId=$breakoutId.'_'.$count; 
						if(!empty($breakoutSessionData))
						{
						?>
						 <!--- listing of stream div end--->
				
							<div class="spacer10"></div>
							  
							  <div class="sessionData<?php echo $breakoutId.'_'.$count; ?>">  
							     <div class="formparag mb20 setupbreak_inheading">STEP <?php echo $count+1; ?> - Setup your streams</div>

								
						 
							 <div class="control-group mb10">
									<label class="control-label lineH16" for="inputEmail"><?php  echo 'Sessions for stream '.$count ; //lang('event_breakkout_each_stream'); ?>
										<div class="infoiconbg" title="<?php   echo 'Can registrants move between streams?"'; ///echo lang('tooltip_breakkout_each_stream'); ?>"> </div>
									
									</label>
								<div class="controls breakout_setup">
									<div class="inputnubcontainer ml0 clicknumbersession">
										 <?php 
											//echo form_input($breakoutNumberOfSession);
											//echo form_error('breakoutNumberOfSession'.$breakoutId); 
										?>	
										<input type="text" name="breakoutNumberOfSession<?php echo $sessonBrkId;?>" id="breakoutNumberOfSession<?php echo $sessonBrkId; ?>" value="<?php echo count($breakoutSessionData); ?>" class="width_50px clr_425968 spinner_selectbox breakoutNumberOfSession" sessionbreakoutid="<?php echo $breakoutId; ?>" readonly min="0" >			
									</div>
								</div>
							 </div>
							<div class="control-group mb10">
								<label class="control-label lineH16" for="inputEmail"><?php echo 'Can registrants move between streams?'; ?>
									<div class="infoiconbg"  title="<?php echo 'Can registrants move between streams?'; ?>"> </div>
								</label>
								<div class="controls">
									<div class="tworadiocontainer">
										<div class="checkradiobg">
											<div class="radio breakputcheck">
											     <input type="radio" name="moveRegistrantYes<?php echo $streamData->id; ?>" class="moveRegistrant" >	

											 <?php //echo form_radio($moveRegistrantYes); ?>
												<label for="moveRegistrantYes<?php echo $breakoutId; ?>"><?php echo lang('comm_yes'); ?> &nbsp; &nbsp;</label>
												 <?php  //echo form_radio($moveRegistrantNo); ?>
												 	<input type="radio" name="moveRegistrantNo<?php echo $streamData->id; ?>" class="moveRegistrant" checked >	

												<label for="moveRegistrantNo<?php echo $breakoutId; ?>"><?php echo lang('comm_no'); ?> &nbsp; &nbsp;</label>
												<?php echo form_error('moveRegistrant'); ?>
												
											</div>
										</div>
									</div>
								</div>

								<input type="hidden" name="sessionBreakoutNumbHidden<?php echo $sessonBrkId; ?>" id="sessionBreakoutNumbHidden<?php echo $sessonBrkId; ?>" value="<?php echo count($breakoutSessionData); ?>">
							</div> 	
															
							<?php
							foreach($breakoutSessionData as $sessionData)
						    { 
								
								$sessionSerial 			= $sessionData->session;
								$sessionStartTime 		= timeFormate($sessionData->start_time,'h:i A');
								$sessionEndTime 		= timeFormate($sessionData->end_time,'h:i A');
								$breakoutSessionId 		= $sessionData->id;
								$BreakoutSessionRowId 	= $breakoutId.'_'.$count.$sessionSerial;
								
							     ?>
							
							
								 <div id="session_breakout_row<?php echo $breakoutId.'_'.$count.$sessionSerial; ?>">
									<div class="control-group mb10 breakouttime">
										<label for="inputEmail" class="control-label"><?php echo lang('event_breakkout_session'); ?> <?php echo $sessionSerial; ?>, <?php echo lang('event_breakkout_start'); ?>
												<font class="mandatory_cls">*</font>
										</label>
										<div class="controls registrat_setup">
													<div class="inputnubcontainer ml0 customNumbertype pull-left calender_container">
														<input type="text" readonly="" value="<?php echo $sessionStartTime;  ?>" required="" name="sessionStartTime<?php echo $BreakoutSessionRowId; ?>" id="sessionStartTime<?php echo $BreakoutSessionRowId; ?>" class="width_100px timepicker">
													</div>
													<div class="customcheckbox_div allowword custom_finish calender_container calle_last">
														<label class="control-label mt10 mr15 pl0" for="inputEmail"><?php echo lang('event_breakkout_finish'); ?></label>
														<div class="inputnubcontainer ml0 customNumbertype pull-left">
															<input type="text" readonly=""  value="<?php echo $sessionEndTime;  ?>" required="" name="sessionEndTime<?php echo $BreakoutSessionRowId; ?>" id="sessionEndTime<?php echo $BreakoutSessionRowId; ?>" class="width_100px timepicker">
														</div>
														<input type="hidden" name="breakoutSessionId<?php echo $breakoutId.'_'.$count; ?>[<?php echo $BreakoutSessionRowId; ?>]" value="<?php echo $breakoutSessionId; ?>" id="breakoutSessionId<?php echo $BreakoutSessionRowId; ?>">
													</div>
										
									<?php
								   
									$sessionRegisId='';
									if(isset($eventregistrants) && !empty($eventregistrants))
								    {
										
										foreach($eventregistrants as $sessionRegis)
									    {
											$checked	='';
											$displayNone='dn';
											$sessionRegisData = getDataFromTabel('breakouts_session_registrants','session_limit',array('registrant_id'=>$sessionRegis->id,'session_id'=>$sessionData->id));
									
											$sessionDataId=$BreakoutSessionRowId.'_'.$sessionRegis->id;
											if(!empty($sessionRegisData))
											{
												if($sessionRegisId!='')
												{
													 $sessionRegisId+='';  
												}
												  $sessionRegisId+=$sessionRegis->id;
												 $checked='checked'; 
												 $displayNone='';
											}
										
									     ?>
											
										 <div class="tickbox_cont">
											   <div class="standerd_container">
												  <div class="controls checkradiobg setupcheck ml0 pl0 pr0">
													  <div class="checkbox pl0 position_R setupcheck">
														  <input   type="checkbox" name="registrantSelectionLimit<?php echo $sessionDataId;?>" value="<?php echo $sessionRegis->id; ?>" id="registrantSelectionLimit<?php echo $sessionDataId;?>" class="registrantSelectionLimit regisChecked<?php $BreakoutSessionRowId; ?>" registrantselectionlimitid="<?php echo $sessionDataId; ?>" regisFieldId="<?php echo $BreakoutSessionRowId; ?>" <?php echo $checked; ?>><label for="registrantSelectionLimit<?php echo $sessionDataId; ?>" class="width_auto ml0 mt10 eventDcheck" >&nbsp;<?php echo $sessionRegis->registrant_name; ?></label>
													  </div>
													</div>
												</div>
												
												<div class="setlimit regisLimit <?php echo $displayNone; ?>" id="registrant_selection_limit_div<?php echo $sessionDataId; ?>">
												  <div class="control-group">
													 <label class="control-label width_auto" for="inputEmail">Limit
													   <div class="infoiconbg" title="Limit"></div></label>
											
														<div class="controls ml0 pr0 pl0">
															<input type="text" name="registrantLimit<?php echo $sessionDataId; ?>" value="<?php if(!empty($sessionRegisData)){ echo $sessionRegisData[0]->session_limit; } ?>" id="registrantLimit<?php echo $sessionDataId; ?>" class="field_disable limit_input registrantselection<?php echo $sessionDataId; ?>">
														</div>
														</div>
													</div>
														<div class="clearfix"> 

													  <input type="hidden" name="session_registrant<?php echo $BreakoutSessionRowId; ?>[<?php echo $sessionDataId; ?>]"  value="<?php if(!empty($sessionRegisData)){ echo $sessionRegis->id; } ?>">
								
									
												</div>
											</div>
										<?php		
								        }
									}  //end of inner if
									?>
									 </div>
										
								</div>
									
							 </div> <!-- end of session_breakout_row -->
									
								<?php	
			                } 
		                
		                 ?>
		                      <div class="breakoutNumberOfSession<?php echo $sessonBrkId; ?>"></div>	 
		                      </div>
		                  
		                  <?php
				    	}
				    	
		            }
				}
				 $default_step_2='dn';
			    if(empty($breakoutStreamData))
			    {
					$default_step_2='';
				}    
			
			
			?>	
			   	<div class="formparag mb20 setupbreak_inheading default_step_2<?php echo $breakoutId; ?> <?php echo $default_step_2; ?>">STEP 2 - Setup your streams</div>

				<div class="spacer10"></div>
			     <div id="brekout_sesson_content<?php  echo $breakoutId;?>"></div>
								
		</div>
		<!--- listing of session div end--->
		
		<div class="spacer10"></div>
			<div class="formparag mb20">
				<div class="bdrb_block"></div>
			</div>	
		<div class="spacer10"></div>
		 
		 <div class="formparag mb20 setupbreak_inheading">STEP <span class="session_detail_heading"><?php echo count($breakoutStreamData)+'2'; ?></span> - Fill out details of each session</div>

		
		<div class="formparag mb20 setupbreak_inheading">
			<?php echo lang('event_breakkout_setup_5'); ?>
		</div>
		
		<!--- listing of session details div start--->
			<div id="session_details<?php echo $breakoutId; ?>">
				
				<?php 
			
				//check stream data and display 
				if(!empty($breakoutStreamData)){
					$countFirstRow=1;
					foreach($breakoutStreamData as $breakoutStream){ 
						$BreakoutStreamId = $breakoutStream->id;
						$BreakoutStreamOrder = $breakoutStream->stream;
						$BreakoutStreamName = $breakoutStream->stream_name;
						$where = array('breakout_stream_id'=>$BreakoutStreamId);
						
						$breakoutStreamSessionData = getDataFromTabel('breakout_stream_session','*',$where);
						$sessionHeaderRowId = $breakoutId.'_'.$BreakoutStreamOrder;
					?>
							<div class="formparag mb10 sessionData<?php echo $breakoutId.'_'.$countFirstRow; ?>" id="session_heading_15_1">
								<div class="accordian_headingbg bg_0073ae">
									<div class="typeofregistH_small pull-left ptr action_session_details" id="<?php echo $sessionHeaderRowId; ?>">
										 <?php echo $BreakoutStreamName; ?>
									</div>
									<div class="clearfix">
									</div>
								</div>	
								
								<div id="session_details_list_div<?php echo $sessionHeaderRowId; ?>" class="dn session_details_list_div_cls<?php echo $sessionHeaderRowId; ?>" >
									 
									<?php
									  
									    
									    if(!empty($breakoutStreamSessionData)){ 
										$countRow=1;
										foreach($breakoutStreamSessionData as $breakoutStreamSession) {
											
											$breakoutStreamSessionId = $breakoutStreamSession->id;
											$breakoutStreamSessionAbout = $breakoutStreamSession->about_session;
											$breakoutStreamSessionSpearker = $breakoutStreamSession->speakers;
											$breakoutStreamSessionOrganisation = $breakoutStreamSession->organisation;
											
											$breakoutRowKeyId = $breakoutId.'_'.$countFirstRow.$countRow;
											$breakoutRowKeyIdHidden = $countFirstRow.'_'.$countRow;
										?>
											<div class="accordian_cnt" id="session_details_row<?php echo $breakoutRowKeyId; ?>">
												<div class="control-group mb10">
													<label for="inputEmail" class="control-label"><?php echo lang('event_breakkout_about_session'); ?> <?php echo $countRow; ?></label>
													<div class="controls">
														<textarea placeholder="" class="bdr_n"  id="breakoutAboutSession<?php echo $breakoutRowKeyId; ?>" name="breakoutAboutSession<?php echo $breakoutRowKeyId; ?>" rows="3" cols="40"><?php echo $breakoutStreamSessionAbout; ?></textarea>
													</div>
												</div>
												
												<div class="control-group mb10">
													<label for="inputEmail" class="control-label"><?php echo lang('event_breakkout_speaker'); ?></label>
													<div class="controls">
														<input type="text" id="breakoutSpeakers<?php echo $breakoutRowKeyId; ?>" name="breakoutSpeakers<?php echo $breakoutRowKeyId; ?>" value="<?php echo $breakoutStreamSessionSpearker; ?>" />
													</div>
												</div>
												
												<div class="control-group mb10">
													<label for="inputEmail" class="control-label"><?php echo lang('event_breakkout_organisation'); ?></label>
													<div class="controls">
														
														<input type="text" id="breakoutOrganisation<?php echo $breakoutRowKeyId; ?>" name="breakoutOrganisation<?php echo $breakoutRowKeyId; ?>" value="<?php echo $breakoutStreamSessionOrganisation; ?>" />
														<input type="hidden" name="breakoutStreamSessionId<?php echo $breakoutId; ?>[<?php echo $breakoutRowKeyIdHidden; ?>]" value="<?php echo $breakoutStreamSessionId; ?>" id="breakoutStreamSessionId<?php echo $breakoutRowKeyId; ?>">
													</div>
												</div>
											        
											</div>
									           
									<?php  $countRow++; }  } ?>
								
								</div>
							
							</div>
					<?php $countFirstRow++;  }
				}   ?>
		</div>
		<!--- listing of session details div end--->
		
		<div class="spacer10">
		</div>
		<div class="row-fluid">
			<?php 
				echo form_hidden('eventId', $eventId);
				echo form_hidden('breakoutId', $breakoutId);
				
				//button show of save and reset
				$formButton['saveButtonId'] = 'id="'.$breakoutId.'"'; // click button id
				$formButton['showbutton']   = array('save','reset');
				$formButton['labletext'] 	= array('save'=>lang('comm_save_changes'),'reset'=>lang('comm_reset'));
				$formButton['buttonclass']  = array('save'=>'submitbtn_cus mr_16 submit_regis_type','reset'=>'submitbtn_cus reset_form');
				$this->load->view('common_save_reset_button',$formButton);
				
			?>
		</div>
		<div class="spacer36">
		</div>
	</div>
	<?php echo form_close();  ?>
</div>
<!-- /commonform_bg -->	

<script type="text/javascript">
	/*----------breakout details save by common function----------*/
	//ajaxdatasave('formBreakoutSetup<?php echo $breakoutId; ?>','<?php echo $this->uri->uri_string(); ?>',true,true,false,false,false,'#showhideformdiv<?php echo $breakoutId; ?>','#showhideformdiv<?php echo $nextRecordId; ?>');
    ajaxdatasave('formBreakoutSetup<?php echo $breakoutId; ?>','<?php echo $this->uri->uri_string(); ?>',false,true,true);
</script>
