<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//set value in variable
if(!empty($customizeformsdata)){
	$customizeformsdata = $customizeformsdata[0];
}

$HeaderFileId = (!empty($customizeformsdata->id))?$customizeformsdata->id:'';
$HeaderFileName = (!empty($customizeformsdata->header_image))?$customizeformsdata->header_image:'';


//add exhibitor floor plan form 
$formEventHeader = array(
		'name'	 => 'formEventHeader',
		'id'	 => 'formEventHeader',
		'method' => 'post',
		'class'  => 'form-horizontal',
	);

?>
<div class="commonform_bg pb0">
	<div class="headingbg bg_807f83">
		<div class="typeofregistV pull-left showhideaction ptr" id="formHeaderImage">
			<?php echo lang('event_customize_header'); ?>
		</div>
		<div class="clearfix">
		</div>
	</div>
	<?php echo form_open($this->uri->uri_string(),$formEventHeader); ?>	
		<div class="form-horizontal form_open_chk dn"  id="showhideformdivformHeaderImage">
			
		<div class="control-group mb10" id="attch_FileContainer">
			<label class="control-label" for="inputEmail"> <?php echo lang('event_customize_header_field'); ?>
				<div class="infoiconbg bg_5f6062 customize_tooltip">
					<span class="show_tool_tip" title="<?php echo lang('tooltip_customize_header_field'); ?>"> </span>
				</div>
			</label>
			<div class="controls" id="attch_pickfiles">
				<div id="attch_dropArea">
					<div class="dashbdr bdr_5f6062 clr_5f6062">
						<?php echo lang('comm_file_upload_drop'); ?>
						<div class="filedropbg_cusform">
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="control-group mb10">
			<div class="controls">
					<?php echo lang('event_customize_header_size_msg'); ?>
			</div>
		</div>
		
		<div class="control-group mb10  <?php echo (empty($HeaderFileName))?'dn':' '; ?>" id="attach_file_list_main" >
		<label class="control-label cusinp_font" for="inputEmail">&nbsp;</label>
			<div class="controls " id="attch_file_list" >
					<?php if(!empty($HeaderFileName)){ 
					$fileId= remove_extension($HeaderFileName);	
						?>
						 <div class="attchadddedfile" id="<?php echo $fileId; ?>"> 
								<div class="imagename_cont pr mr_5px"><?php echo $HeaderFileName; ?>
									<div class="pa ptr top_6 right_10 delete_file" deleteurl='event/deletecustomizeformsheader'  id="cancel_<?php echo $fileId; ?>" deleteid="<?php echo $HeaderFileId; ?>"  fileid="<?php echo $fileId; ?>">
										<img src="<?php echo IMAGE; ?>close_icon.png">
									</div>
								</div>
							</div> 
					<?php } ?>	
			</div>
		</div>
		
		<div class="spacer10">
		</div>
		<div class="row-fluid">
			<?php 
				$extraSave 	= 'class="submitbtn_cus  bg_5f6062 clr_99BBCC submitbtn_header" ';
				echo form_submit('save',lang('comm_save_changes'),$extraSave);
			?>
		</div>
		<div class="spacer36">
		</div>
	</div>
	<?php echo form_close();?>	
</div>
<!-- /commonform_bg -->

<script type="text/javascript" language="javascript">
	
	//This function is used to 
	$( ".submitbtn_header").click(function() {
		$('#div_ar2RA_a').trigger('click');
		custom_popup('Event header uploaded successfully.',true);
		return false;	 
	});
		
	// Custom example logic for file upload
	var postDataHeader = {event_id : '<?php echo $eventId; ?>',action_post : 'header'}
	var uploadUrlHeader = baseUrl+'event/customizeformsmedia';

	var otherObjHeader = {
			pickfiles:"attch_pickfiles", 
			dropArea:"attch_dropArea", 
			FileContainer:"attch_FileContainer", 
			fileTypes:"jpg,jpeg,png,gif", 
			deleteUrl:baseUrl+'event/deletecustomizeformsheader', 
			maxfilesize:"10mb", 
			multiselection:false, 
			isFileList:true, 
			uniqueNames:true, 
			autoStart:false, 
			isDelete:true, 
			maxfileupload:1, 
			mainDivFileList:"attach_file_list_main",
			listAddFile:"attch_file_list",
			adddedfile:"attchadddedfile",
		}

	// initilization upload config
	upload_file(uploadUrlHeader,postDataHeader,otherObjHeader);
	
</script>	
		
