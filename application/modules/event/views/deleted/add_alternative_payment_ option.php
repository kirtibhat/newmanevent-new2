<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//--------payment option add form section---------//  

$formpaymentOptionAdd = array(
    'name'   => 'formpaymentOptionAdd',
    'id'   => 'formpaymentOptionAdd',
    'method' => 'post',
    'class'  => 'wpcf7-form',
    'data-parsley-validate' => '',
  );
  
$paymentOptionModeName = array(
    'name'  => 'paymentOptionModeName',
    'value' => '',
    'id'  => 'paymentOptionModeName',
    'type'  => 'text',
    'required'  => '',
    'data-parsley-error-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
  );
?>

<div id="add_edit_payment_popup" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" >
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header delegates">
        <h4 class="medium dt-large modal-title"><?php echo lang('event_payment_add_payment_option'); ?></h4>
      </div>

      <div class="modal-body small">
        <div class="modelinner ">
          <?php  echo form_open($this->uri->uri_string(),$formpaymentOptionAdd); ?>
            <div class="control-group mb10 eventdashboard_popup small">

              <div class="row-fluid">
                <label class="pull-left" for="presenter_title"><?php echo lang('event_payment_add_alternative_field'); ?> <span class="astrik">*</span>
                <span class="info_btn"><span class="field_info xsmall"><?php echo lang('event_payment_add_alternative_field'); ?></span></span>
                </label>
                <?php echo form_input($paymentOptionModeName); ?>
              </div>

              <div class="btn_wrapper">
                  <?php 
                    echo form_hidden('eventId', $eventId);
                    $extraCancel  = 'class="popup_cancel submitbtn pull-right medium"  data-dismiss="modal" ';
                    $extraSave  = 'class="submitbtn pull-right medium" ';
                    echo form_submit('cancel',lang('comm_cancle'),$extraCancel);
                    echo form_submit('save',lang('comm_save'),$extraSave);
                  ?>
              </div>
            </div>
          <?php echo form_close(); ?>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
$("#formpaymentOptionAdd").parsley();
</script>
