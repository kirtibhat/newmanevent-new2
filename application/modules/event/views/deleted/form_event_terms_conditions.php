<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

//set field value in variable
$tctermconditionid     = (!empty($eventdetails->id))?$eventdetails->id:'';
$tctermcondition     = (!empty($eventdetails->term_conditions))?$eventdetails->term_conditions:'';
$tcdocumentfile      = (!empty($eventdetails->document_file))?$eventdetails->document_file:'';

$formTermsCondition = array(
    'name'   => 'formTermsCondition',
    'id'   => 'formTermsCondition',
    'method' => 'post',
    'class'  => '',
);

$eventnTermsCondition = array(
    'name'  => 'eventnTermsCondition',
    'value' => (empty($tctermcondition)) ? lang('event_term_condition_default_msg') : $tctermcondition,
    'id'  => 'eventnTermsCondition',
    'type'  => 'text',
    'required'  => '',
    'rows'  => '5',
    //'placeholder' =>,
    'class' => 'small'
);  

$termSelection = array(
    'name'  => 'termSelection',
    'value' => '0',
    'id'  => 'termSelection',
    'type'  => 'hidden',
);  


?>


 <div class="panel event_panel">
    <div class="panel-heading ">
      <h4 class="panel-title medium dt-large ">
        <a class=" " data-toggle="collapse" data-parent="#accordion" href="#collapseEight"><?php echo lang('event_term_term_condi_title'); ?></a>
      </h4>
    </div>
    <div style="height: auto;" id="collapseEight" class="accordion_content collapse apply_content ">
     <?php echo form_open($this->uri->uri_string(),$formTermsCondition); ?>
      <div class="panel-body ls_back dashboard_panel small" id="showhideformdivtermSection">

        <div class="row-fluid-15">
          <label for="tandc"><?php echo lang('event_term_term_condition'); ?> <span class="info_btn"><span class="field_info xsmall"><?php echo lang('tooltip_term_term_condi'); ?></span></span>
          </label>
          <?php echo form_textarea($eventnTermsCondition); ?>
        </div>
        <!--end of row-fluid-->
  
  
        <div class="row-fluid-15" id="attch_FileContainerTerm">
        <div class="row-fluid-15" id="attch_dropAreaTerm">
          <label for="attach_doc"><?php echo lang('comm_file_upload_drop'); ?></label>
          
          <div class="custom-upload" id="attch_pickfilesTerm">
            <span class="label_text"><?php echo lang('event_term_term_select_or_drop_file'); ?></span>
            <input type="file" name="attach_doc" id="attach_doc" draggable="true" />
          </div>
          <div id="attch_file_listTerm" class="custom-upload-file "></div>
        </div>
        </div>
      
        <!---file list div-->
        
        <div class="control-group mb10  <?php echo (empty($tcdocumentfile))?'dn':' '; ?>" id="attach_file_list_mainTerm" >
          <div class="controls " id="attch_file_listTerm" >
              <?php  if(!empty($tcdocumentfile)){ 
               $fileId= remove_extension($tcdocumentfile);  
                ?>
                 <div class="attchadddedfileTerm uploaded_file" id="<?php echo $fileId; ?>"> 
                    <div class="imagename_cont pr mr_5px"><?php echo $tcdocumentfile; ?>
                      <div class="pa ptr top_6 right_10 delete_file" deleteurl='event/deletetermattachmentterm' id="cancel_<?php echo $fileId; ?>" deleteid="<?php echo $tctermconditionid; ?>"  fileid="<?php echo $fileId; ?>">
                        <img src="<?php echo IMAGE; ?>value_clear.png">
                      </div>
                    </div>
                  </div> 
              <?php }  ?> 
          </div>
        </div>
        

        <!--end of row-fluid-->

        <div class="btn_wrapper ">
          <a href="#collapseEight" class="pull-left scroll_top"><?php echo lang('comm_top'); ?></a>

          <?php 
            echo form_input($termSelection);
            echo form_hidden('eventId', $eventId);
            echo form_input(array('name' => 'term_condi_id', 'type'=>'hidden', 'id' =>'term_condi_id', 'value'=>$tctermconditionid));
            echo form_hidden('formActionName', 'eventTermsCondition');
            
            //button show of save and reset
            $formButton['showbutton']   = array('save','reset');
            $formButton['labletext']  = array('save'=>lang('comm_save_changes'),'reset'=>lang('comm_reset'));
            $formButton['buttonclass']  = array('save'=>'default_btn btn pull-right medium term_condi_submit','reset'=>'default_btn btn pull-right medium');
            $this->load->view('common_save_reset_button',$formButton);
          ?> 
          
        </div>
      </div>
       <?php echo form_close();?>
    </div>
  </div>
 <!--end of panel-->

<!-- /commonform_bg --> 




<script>
  
//call for term and condition data save and media upload
ajaxdatasave('formTermsCondition','<?php echo $this->uri->uri_string(); ?>',true,true,false,true,true,'#showhideformdivtermSection','',false);  

$(document).on('click','.term_condi_submit',function(){
  
  var termcount   = 0; // set term attachment count
  //set refund count
  $('.attchadddedfileTerm').each(function(){
    termcount = 1;
  });
  
  
  //set value in field
  $("#termSelection").val(termcount);
    
});
  
// Custom example logic
var postData = {event_id : '<?php echo $eventId; ?>'}
var uploadUrl = baseUrl+'event/uploadtermattachmentterm';

var otherObj = {
    pickfiles:"attch_pickfilesTerm", 
    dropArea:"attch_dropAreaTerm", 
    FileContainer:"attch_FileContainerTerm", 
    fileTypes:"pdf,doc", 
    deleteUrl:baseUrl+'event/deletetermattachmentterm', 
    maxfilesize:"10mb", 
    multiselection:false, 
    isFileList:true, 
    uniqueNames:true, 
    autoStart:false, 
    isDelete:true, 
    maxfileupload:1, 
    mainDivFileList:"attach_file_list_mainTerm",
    listAddFile:"attch_file_listTerm",
    adddedfile:"attchadddedfileTerm",
}
var popupmodelid = '';
upload_file(uploadUrl,postData,otherObj);
  
//Delete common delete function
deleteUploadFiles();  
</script> 

 
