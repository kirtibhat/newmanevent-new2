<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//add exhibitor floor plan form 
$formEventSponsorLogo = array(
		'name'	 => 'formEventSponsorLogo',
		'id'	 => 'formEventSponsorLogo',
		'method' => 'post',
		'class'  => 'form-horizontal',
	);
	

?>
<div class="commonform_bg pb0">
	<div class="headingbg bg_807f83">
		<div class="typeofregistV pull-left showhideaction ptr" id="formSponsorLogo">
			<?php echo lang('event_customize_sponsor_logo') ?>
		</div>
		<div class="clearfix">
		</div>
	</div>
	<?php echo form_open($this->uri->uri_string(),$formEventSponsorLogo); ?>	
		<div class="form-horizontal form_open_chk dn" id="showhideformdivformSponsorLogo">
			
		<div class="control-group mb10" id="attch_FileContainer_sponsor_logo">
			<label class="control-label" for="inputEmail"><?php echo lang('event_customize_sponsor_logo_field') ?>
				<div class="infoiconbg bg_5f6062 customize_tooltip">
					<span class="show_tool_tip" title="<?php echo lang('tooltip_customize_sponsor_logo_field') ?>"> </span>
				</div>
			</label>
			<div class="controls" id="attch_pickfiles_sponsor_logo">
				<div id="attch_dropArea_sponsor_logo">
					<div class="dashbdr bdr_5f6062 clr_5f6062">
						<?php echo lang('comm_file_upload_drop'); ?>
						<div class="filedropbg_cusform">
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="control-group mb10">
			<div class="controls">
					<?php echo lang('event_customize_sponsor_logo_size_msg') ?>
			</div>
		</div>
		
		<div class="control-group mb10  <?php echo (empty($eventsponsorslogos))?'dn':' '; ?>" id="attach_file_list_main_sponsor_logo" >
		<label class="control-label cusinp_font" for="inputEmail">&nbsp;</label>
			<div class="controls " id="attch_file_list_sponsor_logo" >
					<?php if(!empty($eventsponsorslogos)){ 
					 foreach($eventsponsorslogos as $imglist){ 
						 $fileId= remove_extension($imglist->logo_file_name);
						?>
						 <div class="attchadddedfile_sponsor_logo" id="<?php echo $fileId; ?>"> 
								<div class="imagename_cont pr mr_5px"><?php echo $imglist->logo_file_name; ?>
									<div class="pa ptr top_6 right_10 delete_file" deleteurl='event/deletesponsorslogo' id="cancel_<?php echo $fileId; ?>" deleteid="<?php echo $imglist->logo_id; ?>"  fileid="<?php echo $fileId; ?>">
										<img src="<?php echo IMAGE; ?>close_icon.png">
									</div>
								</div>
							</div> 
					<?php }  } ?>	
			</div>
		</div>
		
		<div class="spacer10">
		</div>
		<div class="row-fluid">
			<?php 
				$extraSave 	= 'class="submitbtn_cus  bg_5f6062 clr_99BBCC submitbtn_sponsor_logo" ';
				echo form_submit('save',lang('comm_save'),$extraSave);
			?>
		</div>
		<div class="spacer36">
		</div>
	</div>
	<?php echo form_close();?>	
</div>
<!-- /commonform_bg -->

<script type="text/javascript" language="javascript">
	
	//This function is used to 
	$( ".submitbtn_sponsor_logo").click(function() {
		$('#div_ar2RA_a').trigger('click');
		custom_popup('Event sponsor logo uploaded successfully.',true);
		return false;	 
	});
		
	// Custom example logic for file upload
	var postData = {event_id : '<?php echo $eventId; ?>'}
	var uploadUrl = baseUrl+'event/uploadsponsorslogo';

	var otherObj = {
			pickfiles:"attch_pickfiles_sponsor_logo", 
			dropArea:"attch_dropArea_sponsor_logo", 
			FileContainer:"attch_FileContainer_sponsor_logo", 
			fileTypes:"jpg,jpeg,png,gif", 
			deleteUrl:baseUrl+'event/deletesponsorslogo', 
			maxfilesize:"10mb", 
			multiselection:true, 
			isFileList:true, 
			uniqueNames:true, 
			autoStart:false, 
			isDelete:true, 
			maxfileupload:50, 
			mainDivFileList:"attach_file_list_main_sponsor_logo",
			listAddFile:"attch_file_list_sponsor_logo",
			adddedfile:"attchadddedfile_sponsor_logo",
	}

	// initilization upload config
	upload_file(uploadUrl,postData,otherObj);
	
	//Delete common delete function
	deleteUploadFiles();
</script>	
		
