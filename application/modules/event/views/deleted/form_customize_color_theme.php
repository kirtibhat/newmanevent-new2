<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//set data in the variable	
$customizeformsdata			= (!empty($customizeformsdata))?$customizeformsdata[0]:'';
$customizeformsId 			= (!empty($customizeformsdata->id))?$customizeformsdata->id:'0';	
$formColorObject 			= (!empty($customizeformsdata->form_color))?json_decode($customizeformsdata->form_color):'';	
$formTextColorValue 		= (!empty($customizeformsdata->form_text_color))?$customizeformsdata->form_text_color:'';	
$formButtonValue 			= (!empty($customizeformsdata->form_button))?$customizeformsdata->form_button:'';	
$header1ColorValue 			= (!empty($customizeformsdata->header1_color))?$customizeformsdata->header1_color:'';	
$header2ColorValue 			= (!empty($customizeformsdata->header2_color))?$customizeformsdata->header2_color:'';	
$linkColorValue 			= (!empty($customizeformsdata->header2_color))?$customizeformsdata->link_color:'';	
$tableText1ColorValue		= (!empty($customizeformsdata->table_text1_color))?$customizeformsdata->table_text1_color:'';	
$tableText2ColorValue		= (!empty($customizeformsdata->table_text2_color))?$customizeformsdata->table_text2_color:'';	
$background1ColorValue		= (!empty($customizeformsdata->background1_color))?$customizeformsdata->background1_color:'';	
$formBackgroundValue		= (!empty($customizeformsdata->form_background))?$customizeformsdata->form_background:'';	
$headerBarBackgroundValue	= (!empty($customizeformsdata->header_bar_background))?$customizeformsdata->header_bar_background:'';	
$buttonIconColorValue		= (!empty($customizeformsdata->button_icon_color))?$customizeformsdata->button_icon_color:'';	
$tableBackground1Value		= (!empty($customizeformsdata->table_background1))?$customizeformsdata->table_background1:'';	
$tableBackground2Value		= (!empty($customizeformsdata->table_background2))?$customizeformsdata->table_background2:'';	

$formColor1Value='#00274C'; // default color set
$formColor2Value='#0073AE'; // default color set
$formColor3Value='#00A88F'; // default color set
$formColor4Value='#dcddde'; // default color set

$headerColorFiledValue11=''; 
$headerColorFiledValue12=''; 
$headerColorFiledValue13=''; 
$headerColorFiledValue14=''; 

$headerColorFiledValue21=''; 
$headerColorFiledValue22=''; 
$headerColorFiledValue23=''; 
$headerColorFiledValue24='';

$linkColorValue1=''; 
$linkColorValue2=''; 
$linkColorValue3=''; 
$linkColorValue4='';  


$tableTextColorFieldValue11=''; 
$tableTextColorFieldValue12=''; 
$tableTextColorFieldValue13=''; 
$tableTextColorFieldValue14=''; 

$tableTextColorFieldValue21=''; 
$tableTextColorFieldValue22=''; 
$tableTextColorFieldValue23=''; 
$tableTextColorFieldValue24=''; 

$backgroundColorFiledValue1=''; 
$backgroundColorFiledValue2=''; 
$backgroundColorFiledValue3=''; 
$backgroundColorFiledValue4=''; 

$formBackgroundFieldValue1=''; 
$formBackgroundFieldValue2=''; 
$formBackgroundFieldValue3=''; 
$formBackgroundFieldValue4=''; 

$headerBarBackgroundFieldValue1=''; 
$headerBarBackgroundFieldValue2=''; 
$headerBarBackgroundFieldValue3=''; 
$headerBarBackgroundFieldValue4=''; 

$buttonIconColorFieldValue1=''; 
$buttonIconColorFieldValue2=''; 
$buttonIconColorFieldValue3=''; 
$buttonIconColorFieldValue4=''; 

$tableBackgroundFieldValue11=''; 
$tableBackgroundFieldValue12=''; 
$tableBackgroundFieldValue13=''; 
$tableBackgroundFieldValue14='';

$tableBackgroundFieldValue21=''; 
$tableBackgroundFieldValue22=''; 
$tableBackgroundFieldValue23=''; 
$tableBackgroundFieldValue24='';


//set value if data exist
if(!empty($formColorObject)){
	$formColor1Value	=	$formColorObject->col1;
	$formColor2Value	=	$formColorObject->col2;
	$formColor3Value	=	$formColorObject->col3;
	$formColor4Value	=	$formColorObject->col4;
	
	//check filed value in array
	$rowCount=1;
	foreach($formColorObject as $key => $value){
		//set header  1 value
		if($header1ColorValue==$value){	
			${'headerColorFiledValue1' .$rowCount} = 	'checked';
		}	
		
		//set header  2 value
		if($header2ColorValue==$value){	
			${'headerColorFiledValue2' .$rowCount} = 	'checked';
		}	
		
		//set link Color Value
		if($linkColorValue==$value){	
			${'linkColorValue' .$rowCount} = 	'checked';
		}	
		
		//set table text 1 color value
		if($tableText1ColorValue==$value){	
			${'tableTextColorFieldValue1' .$rowCount} = 	'checked';
		}
		
		//set table text 2 color value
		if($tableText2ColorValue==$value){	
			${'tableTextColorFieldValue2' .$rowCount} = 	'checked';
		}
		
		//set background Color Filed Value
		if($background1ColorValue==$value){	
			${'backgroundColorFiledValue' .$rowCount} = 	'checked';
		}
		
		//set form Background Field Value
		if($formBackgroundValue==$value){	
			${'formBackgroundFieldValue' .$rowCount} = 	'checked';
		}	
		
		//set header Bar Background Field Value
		if($headerBarBackgroundValue==$value){	
			${'headerBarBackgroundFieldValue' .$rowCount} = 	'checked';
		}		
		
		//set button Icon Color Field Value
		if($buttonIconColorValue==$value){	
			${'buttonIconColorFieldValue' .$rowCount} = 	'checked';
		}
		
		//set table Background Field Value 1
		if($tableBackground1Value==$value){	
			${'tableBackgroundFieldValue1' .$rowCount} = 	'checked';
		}
		
		//set table Background Field Value 2
		if($tableBackground2Value==$value){	
			${'tableBackgroundFieldValue2' .$rowCount} = 	'checked';
		}	
		
		$rowCount++;	
	}
	
	
}

//set form text value
$formTextColorBlack = '';
$formTextColorGray	= '';

if($formTextColorValue=='black'){
	$formTextColorBlack = 'checked';
}

if($formTextColorValue=='gray'){
	$formTextColorGray	= 'checked';
}



//set option button value
$formButtonOption1 = '';
$formButtonOption2	= '';

if($formButtonValue=='1'){
	$formButtonOption1 = 'checked';
}

if($formButtonValue=='2'){
	$formButtonOption2	= 'checked';
}

$formColourTheme = array(
		'name'	 => 'formColourTheme',
		'id'	 => 'formColourTheme',
		'method' => 'post',
		'class'  => 'form-horizontal',
	);


?>
<?php $this->load->view('customize_form_help_popup'); ?>

<div class="commonform_bg pb0">
	<div class="headingbg bg_807f83">
		<div class="typeofregistV pull-left showhideaction ptr" id="formCustomizeForms">
			<?php echo lang('event_customize_color_theme'); ?>
		</div>
		<div class="clearfix">
		</div>
	</div>
	<?php echo form_open($this->uri->uri_string(),$formColourTheme); ?>
		<div class="form-horizontal form_open_chk dn" id="showhideformdivformCustomizeForms">
		<div class="formparag mb10">
			<?php echo lang('event_customize_selecte_msg_1'); ?> <a href="javascript:void(0)" class="show_help_popup ptr">
			<div class="infoiconbg_1 bg_5f6062 disILB">
			</div>
			</a>
		</div>
		<div class="formparag">
			<div class="colorselectionbg">
				<div class="chooseclrbg color-picker ptr" id="color1" pick_color_id='1'>
					<?php echo lang('event_customize_selecte_color_1'); ?>
				</div>
				<span class="colorspan bg_58595b color1"></span>
				<input type="hidden" name="form_color1" class="select_color_1 default_set_bg" id="1" value="<?php echo $formColor1Value; ?>" />
			</div>
			<div class="colorselectionbg">
				<div class="chooseclrbg color-picker ptr"  id="color2" pick_color_id='2'>
					<?php echo lang('event_customize_selecte_color_2'); ?>
				</div>
				<span class="colorspan bg_58595b color2"></span>
				<input type="hidden" name="form_color2" class="select_color_2 default_set_bg" id="2" value="<?php echo $formColor2Value; ?>" />
			</div>
			<div class="colorselectionbg">
				<div class="chooseclrbg color-picker ptr" id="color3" pick_color_id='3'>
					<?php echo lang('event_customize_selecte_color_3'); ?>
				</div>
				<span class="colorspan bg_58595b color3"></span>
				<input type="hidden" name="form_color3" class="select_color_3 default_set_bg" id="3" value="<?php echo $formColor3Value; ?>" />
			</div>
			<div class="colorselectionbg" >
				<div class="chooseclrbg color-picker ptr" id="color4" pick_color_id='4'>
					<?php echo lang('event_customize_selecte_color_4'); ?>
				</div>
				<span class="colorspan bg_58595b color4"></span>
				<input type="hidden"  name="form_color4" class="select_color_4 default_set_bg" id="4" value="<?php echo $formColor4Value; ?>" />
			</div>
			<div class="clearfix">
			</div>
		</div>
		<div class="formparag mb10">
			<?php echo lang('event_customize_selecte_msg_2'); ?>
		</div>
		<div class="formparag">
			<div class="colorselectionbg_1">
				<div class="checkradiobg pull-left mt6">
					<div class="radio customform">
						<input type="radio" name="form_text_color" id="text_color_black" value="black"  <?php echo $formTextColorBlack; ?> />
						<label for="text_color_black"><?php echo lang('event_customize_colous_black'); ?></label>
					</div>
				</div>
				<span class="colorspan bg_000"></span>
			</div>
			<div class="colorselectionbg_1">
				<div class="checkradiobg pull-left mt6">
					<div class="radio customform">
						<input type="radio" name="form_text_color" id="text_color_gray" value="gray"  <?php echo $formTextColorGray; ?> />
						<label for="text_color_gray"><?php echo lang('event_customize_colous_grey'); ?></label>
					</div>
				</div>
				<span class="colorspan bg_58595b"></span>
			</div>
			<div class="clearfix">
			</div>
			<div class="ml10 fs12 mb10">
				<?php echo lang('event_customize_text_will_msg'); ?>
			</div>
			<div class="clearfix">
			</div>
		</div>
		<div class="formparag mb10">
			<?php echo lang('event_customize_selecte_msg_3'); ?>
		</div>
		<div class="formparag">
			<div class="themesoption">
				<div class="checkradiobg pull-left mt6">
					<div class="radio customform">
						 <input type="radio" name="form_button" id="button_option1"   value="1" <?php echo $formButtonOption1; ?> />
						<label for="button_option1"><?php echo lang('event_customize_option_1'); ?></label>
					</div>
				</div>
				<span class="themesimgcontainer"><img src="<?php echo IMAGE; ?>option1.png"></span>
				<div class="clearfix">
				</div>
			</div>
			<div class="themesoption">
				<div class="checkradiobg pull-left mt6">
					<div class="radio customform">
						<input type="radio" name="form_button" id="button_option2"   value="2" <?php echo $formButtonOption2; ?> />
						<label for="button_option2"><?php echo lang('event_customize_option_2'); ?></label>
					</div>
				</div>
				<span class="themesimgcontainer"><img src="<?php echo IMAGE; ?>option2.png"></span>
				<div class="clearfix">
				</div>
			</div>
			<div class="clearfix">
			</div>
		</div>
		<div class="formparag youcanpara mt20">
			 <?php echo lang('event_customize_colous_use_msg'); ?>
		</div>
		<div class="textclr_bgcontainer mb20">
			<div class="heading">
				<?php echo lang('event_customize_colous_theme'); ?>
			</div>
			<div class="clearfix">
			</div>
			<div class="partcontainer">
				<div class="headingbg_change">
					<?php echo lang('event_customize_colous_header_1'); ?>
					<div class="infoiconbg_1 bg_5f6062 disILB customize_tooltip">
						<span class="show_tool_tip" title="<?php echo lang('tooltip_customize_colous_header_1'); ?>" > </span>
					</div>
				
				</div>
				<div class="colorSelbg">
					<div class="checkradiobg pull-left">
						<div class="radio customform">
							<input type="radio" name="header1_color" id="header1_color1"   class="select_color_1" value="" <?php echo $headerColorFiledValue11; ?>/>
							<label for="header1_color1"></label>
						</div>
					</div>
					<div class="colorspan_1 color1">
					</div>
				</div>
				<div class="colorSelbg">
					<div class="checkradiobg pull-left">
						<div class="radio customform">
							<input type="radio" name="header1_color" id="header1_color2"   class="select_color_2" value="" <?php echo $headerColorFiledValue12; ?>/>
							<label for="header1_color2"></label>
						</div>
					</div>
					<div class="colorspan_1 color2">
					</div>
				</div>
				<div class="colorSelbg">
					<div class="checkradiobg pull-left">
						<div class="radio customform">
							<input type="radio" name="header1_color" id="header1_color3"   class="select_color_3"  value="" <?php echo $headerColorFiledValue13; ?>/>
							<label for="header1_color3"></label>
						</div>
					</div>
					<div class="colorspan_1 color3">
					</div>
				</div>
				<div class="colorSelbg">
					<div class="checkradiobg pull-left">
						<div class="radio customform">
							<input type="radio" name="header1_color" id="header1_color4"   class="select_color_4" value="" <?php echo $headerColorFiledValue14; ?>/>
							<label for="header1_color4 "></label>
						</div>
					</div>
					<div class="colorspan_1 color4">
					</div>
				</div>
			</div>
			<!-- /partcontainer -->
			<div class="partcontainer">
				<div class="headingbg_change">
					<?php echo lang('event_customize_colous_header_2'); ?>
					<div class="infoiconbg_1 bg_5f6062 disILB customize_tooltip">
						<span class="show_tool_tip" title="<?php echo lang('tooltip_customize_colous_header_2'); ?>"> </span>
					</div>
					
				</div>
				<div class="colorSelbg">
					<div class="checkradiobg pull-left">
						<div class="radio customform">
							<input type="radio" name="header2_color" id="header2_color1"   class="select_color_1" value="" <?php echo $headerColorFiledValue21; ?>/>
							<label for="header2_color1"></label>
						</div>
					</div>
					<div class="colorspan_1 color1">
					</div>
				</div>
				<div class="colorSelbg">
					<div class="checkradiobg pull-left">
						<div class="radio customform">
							<input type="radio" name="header2_color" id="header2_color2"   class="select_color_2"  value="" <?php echo $headerColorFiledValue22; ?>/>
							<label for="header2_color2"></label>
						</div>
					</div>
					<div class="colorspan_1 color2">
					</div>
				</div>
				<div class="colorSelbg">
					<div class="checkradiobg pull-left">
						<div class="radio customform">
							<input type="radio" name="header2_color" id="header2_color3"    class="select_color_3" value="" <?php echo $headerColorFiledValue23; ?> />
							<label for="header2_color3"></label>
						</div>
					</div>
					<div class="colorspan_1 color3">
					</div>
				</div>
				<div class="colorSelbg">
					<div class="checkradiobg pull-left">
						<div class="radio customform">
							<input type="radio" name="header2_color" id="header2_color4"   class="select_color_4" value="" <?php echo $headerColorFiledValue24; ?>/>
							<label for="header2_color4"></label>
						</div>
					</div>
					<div class="colorspan_1 color4">
					</div>
				</div>
			</div>
			<!-- /partcontainer -->
			<div class="partcontainer">
				<div class="headingbg_change">
					<?php echo lang('event_customize_colous_link'); ?>
					<div class="infoiconbg_1 bg_5f6062 disILB customize_tooltip">
						<span class="show_tool_tip" title="<?php echo lang('tooltip_customize_colous_link'); ?>" > </span>
					</div>
				</div>
				<div class="colorSelbg">
					<div class="checkradiobg pull-left">
						<div class="radio customform">
							<input type="radio" name="link_color" id="link_color1"   class="select_color_1" value=""  <?php echo $linkColorValue1; ?> />
							<label for="link_color1"></label>
						</div>
					</div>
					<div class="colorspan_1 color1">
					</div>
				</div>
				<div class="colorSelbg">
					<div class="checkradiobg pull-left">
						<div class="radio customform">
							<input type="radio" name="link_color" id="link_color2"   class="select_color_2"  value="" <?php echo $linkColorValue2; ?> />
							<label for="link_color2"></label>
						</div>
					</div>
					<div class="colorspan_1 color2">
					</div>
				</div>
				<div class="colorSelbg">
					<div class="checkradiobg pull-left">
						<div class="radio customform">
							<input type="radio" name="link_color" id="link_color3"   class="select_color_3" value="" <?php echo $linkColorValue3; ?>/>
							<label for="link_color3"></label>
						</div>
					</div>
					<div class="colorspan_1 color3">
					</div>
				</div>
				<div class="colorSelbg">
					<div class="checkradiobg pull-left">
						<div class="radio customform">
							<input type="radio" name="link_color" id="link_color4"   class="select_color_4" value="" <?php echo $linkColorValue4; ?>/>
							<label for="link_color4"></label>
						</div>
					</div>
					<div class="colorspan_1 color4">
					</div>
				</div>
			</div>
			<!-- /partcontainer -->
			<div class="partcontainer">
				<div class="headingbg_change">
					<?php echo lang('event_customize_colous_table_text_1'); ?>
					<div class="infoiconbg_1 bg_5f6062 disILB customize_tooltip">
						<span class="show_tool_tip" title="<?php echo lang('tooltip_customize_colous_table_text_1'); ?>" > </span>
					</div>
					
				</div>
				<div class="colorSelbg">
					<div class="checkradiobg pull-left">
						<div class="radio customform">
							<input type="radio" name="table_text1_color" id="table_text1_color1"   class="select_color_1" value="" <?php echo $tableTextColorFieldValue11; ?> />
							<label for="table_text1_color1"></label>
						</div>
					</div>
					<div class="colorspan_1 color1">
					</div>
				</div>
				<div class="colorSelbg">
					<div class="checkradiobg pull-left">
						<div class="radio customform">
							<input type="radio" name="table_text1_color" id="table_text1_color2"   class="select_color_2" value="" <?php echo $tableTextColorFieldValue12; ?> />
							<label for="table_text1_color2"></label>
						</div>
					</div>
					<div class="colorspan_1 color1">
					</div>
				</div>
				<div class="colorSelbg">
					<div class="checkradiobg pull-left">
						<div class="radio customform">
							<input type="radio" name="table_text1_color" id="table_text1_color3"   class="select_color_3" value="" <?php echo $tableTextColorFieldValue13; ?> />
							<label for="table_text1_color3"></label>
						</div>
					</div>
					<div class="colorspan_1 color3">
					</div>
				</div>
				<div class="colorSelbg">
					<div class="checkradiobg pull-left">
						<div class="radio customform">
							<input type="radio" name="table_text1_color" id="table_text1_color4"   class="select_color_4" value="" <?php echo $tableTextColorFieldValue14; ?> /> 
							<label for="table_text1_color4"></label>
						</div>
					</div>
					<div class="colorspan_1 color4">
					</div>
				</div>
			</div>
			<!-- /partcontainer -->
			<div class="partcontainer">
				<div class="headingbg_change">
					<?php echo lang('event_customize_colous_table_text_2'); ?>
					<div class="infoiconbg_1 bg_5f6062 disILB customize_tooltip">
						<span class="show_tool_tip" title="<?php echo lang('tooltip_customize_colous_table_text_2'); ?>" > </span>
					</div>
				</div>
				<div class="colorSelbg">
					<div class="checkradiobg pull-left">
						<div class="radio customform">
							<input type="radio" name="table_text2_color"   id="table_text2_color1" class="select_color_1" value="" <?php echo $tableTextColorFieldValue21; ?> />
							<label for="table_text2_color1"></label>
						</div>
					</div>
					<div class="colorspan_1 color1">
					</div>
				</div>
				<div class="colorSelbg">
					<div class="checkradiobg pull-left">
						<div class="radio customform">
							<input type="radio" name="table_text2_color" id="table_text2_color2"   class="select_color_2" value="" <?php echo $tableTextColorFieldValue22; ?> />
							<label for="table_text2_color2"></label>
						</div>
					</div>
					<div class="colorspan_1 color2">
					</div>
				</div>
				<div class="colorSelbg">
					<div class="checkradiobg pull-left">
						<div class="radio customform">
							<input type="radio" name="table_text2_color" id="table_text2_color3"   class="select_color_3" value="" <?php echo $tableTextColorFieldValue23; ?> />
							<label for="table_text2_color3"></label>
						</div>
					</div>
					<div class="colorspan_1 color3">
					</div>
				</div>
				<div class="colorSelbg">
					<div class="checkradiobg pull-left">
						<div class="radio customform">
							 <input type="radio" name="table_text2_color" id="table_text2_color4"    class="select_color_4" value="" <?php echo $tableTextColorFieldValue24; ?> />
							<label for="table_text2_color4"></label>
						</div>
					</div>
					<div class="colorspan_1 color4">
					</div>
				</div>
			</div>
			<!-- /partcontainer -->
			<div class="clearfix">
			</div>
		</div>
		<!-- textclr_bgcontainer -->
		<div class="textclr_bgcontainer mb20">
			<div class="heading">
				<?php echo lang('event_customize_colous_img_bg'); ?>
			</div>
			<div class="clearfix">
			</div>
			<div class="partcontainer">
				<div class="headingbg_change">
					<?php echo lang('event_customize_colous_bg_1'); ?>
					<div class="infoiconbg_1 bg_5f6062 disILB customize_tooltip">
						<span class="show_tool_tip" title="<?php echo lang('tooltip_customize_colous_bg_1'); ?>" > </span>
					</div>
				</div>
				<div class="colorSelbg">
					<div class="checkradiobg pull-left">
						<div class="radio customform">
							<input type="radio" name="background1_color" id="background_color1"   class="select_color_1" value="" <?php echo $backgroundColorFiledValue1; ?> />
							<label for="background_color1"></label>
						</div>
					</div>
					<div class="colorspan_1 color1">
					</div>
				</div>
				<div class="colorSelbg">
					<div class="checkradiobg pull-left">
						<div class="radio customform">
							<input type="radio" name="background1_color" id="background_color2"   class="select_color_2" value="" <?php echo $backgroundColorFiledValue2; ?> />
							<label for="background_color2"></label>
						</div>
					</div>
					<div class="colorspan_1 color2">
					</div>
				</div>
				<div class="colorSelbg">
					<div class="checkradiobg pull-left">
						<div class="radio customform">
							<input type="radio" name="background1_color" id="background_color3"   class="select_color_3" value="" <?php echo $backgroundColorFiledValue3; ?> />
							<label for="background_color3"></label>
						</div>
					</div>
					<div class="colorspan_1 color3">
					</div>
				</div>
				<div class="colorSelbg">
					<div class="checkradiobg pull-left">
						<div class="radio customform">
							<input type="radio" name="background1_color" id="background_color4"   class="select_color_4" value="" <?php echo $backgroundColorFiledValue4; ?> />
							<label for="background_color4"></label>
						</div>
					</div>
					<div class="colorspan_1 color4">
					</div>
				</div>
				<!--
				<div class="colorSelbg">
					<div class="checkradiobg pull-left">
						<div class="radio customform">
							<input type="radio" id="white10" name="Background" value="male" class="radio">
							<label for="white10"></label>
						</div>
					</div>
					<div class="colorspan_1 textclr_5">
					</div>
				</div>--->
			</div>
			<!-- /partcontainer -->
			<div class="partcontainer">
				<div class="headingbg_change">
					<?php echo lang('event_customize_colous_form_bg'); ?>
					<div class="infoiconbg_1 bg_5f6062 disILB customize_tooltip">
						<span class="show_tool_tip" title="<?php echo lang('tooltip_customize_colous_form_bg'); ?>" > </span>
					</div>
				</div>
				<div class="colorSelbg">
					<div class="checkradiobg pull-left">
						<div class="radio customform">
							<input type="radio" name="form_background" id="form_background1"   class="select_color_1" value="" <?php echo $formBackgroundFieldValue1; ?> />
							<label for="form_background1"></label>
						</div>
					</div>
					<div class="colorspan_1 color1">
					</div>
				</div>
				<div class="colorSelbg">
					<div class="checkradiobg pull-left">
						<div class="radio customform">
							<input type="radio" name="form_background" id="form_background2"   class="select_color_2" value="" <?php echo $formBackgroundFieldValue2; ?> />
							<label for="form_background2"></label>
						</div>
					</div>
					<div class="colorspan_1 color2">
					</div>
				</div>
				<div class="colorSelbg">
					<div class="checkradiobg pull-left">
						<div class="radio customform">
							<input type="radio" name="form_background" id="form_background3"   class="select_color_3" value="" <?php echo $formBackgroundFieldValue3; ?> />
							<label for="form_background3"></label>
						</div>
					</div>
					<div class="colorspan_1 color3">
					</div>
				</div>
				<div class="colorSelbg">
					<div class="checkradiobg pull-left">
						<div class="radio customform">
							<input type="radio" name="form_background" id="form_background4"   class="select_color_4" value="" <?php echo $formBackgroundFieldValue4; ?> />
							<label for="form_background4"></label>
						</div>
					</div>
					<div class="colorspan_1 color4">
					</div>
				</div>
				<!--
				<div class="colorSelbg">
					<div class="checkradiobg pull-left">
						<div class="radio customform">
							<input type="radio" id="white11" name="FormBackground" value="male" class="radio">
							<label for="white11"></label>
						</div>
					</div>
					<div class="colorspan_1 textclr_5">
					</div>
				</div>
				--->
			</div>
			<!-- /partcontainer -->
			<div class="partcontainer">
				<div class="headingbg_change">
					<?php echo lang('event_customize_colous_header_bar_bg'); ?>
					<div class="infoiconbg_1 bg_5f6062 disILB customize_tooltip">
						<span class="show_tool_tip" title="<?php echo lang('tooltip_customize_colous_header_bar_bg'); ?>" > </span>
					</div>
				</div>
				<div class="colorSelbg">
					<div class="checkradiobg pull-left">
						<div class="radio customform">
							<input type="radio" name="header_bar_background" id="header_bar_background1"   class="select_color_1" value="" <?php echo $headerBarBackgroundFieldValue1; ?> />
							<label for="header_bar_background1"></label>
						</div>
					</div>
					<div class="colorspan_1 color1">
					</div>
				</div>
				<div class="colorSelbg">
					<div class="checkradiobg pull-left">
						<div class="radio customform">
							<input type="radio" name="header_bar_background" id="header_bar_background2"   class="select_color_2" value="" <?php echo $headerBarBackgroundFieldValue2; ?> />
							<label for="header_bar_background2"></label>
						</div>
					</div>
					<div class="colorspan_1 color2">
					</div>
				</div>
				<div class="colorSelbg">
					<div class="checkradiobg pull-left">
						<div class="radio customform">
							<input type="radio" name="header_bar_background" id="header_bar_background3"   class="select_color_3"  value="" <?php echo $headerBarBackgroundFieldValue3; ?> />
							<label for="header_bar_background3"></label>
						</div>
					</div>
					<div class="colorspan_1 color3">
					</div>
				</div>
				<div class="colorSelbg">
					<div class="checkradiobg pull-left">
						<div class="radio customform">
							<input type="radio" name="header_bar_background" id="header_bar_background4"   class="select_color_4" value="" <?php echo $headerBarBackgroundFieldValue4; ?> />
							<label for="header_bar_background4"></label>
						</div>
					</div>
					<div class="colorspan_1 color4">
					</div>
				</div>
				<!---
				<div class="colorSelbg">
					<div class="checkradiobg pull-left">
						<div class="radio customform">
							<input type="radio" id="white12" name="HeaderBar" value="male" class="radio">
							<label for="white12"></label>
						</div>
					</div>
					<div class="colorspan_1 textclr_5">
					</div>
				</div>
				--->
			</div>
			<!-- /partcontainer -->
			<div class="partcontainer">
				<div class="headingbg_change">
					<?php echo lang('event_customize_colous_button_icon'); ?>
					<div class="infoiconbg_1 bg_5f6062 disILB customize_tooltip">
						<span class="show_tool_tip" title="<?php echo lang('tooltip_customize_colous_button_icon'); ?>" > </span>
					</div>
				</div>
				<div class="colorSelbg">
					<div class="checkradiobg pull-left">
						<div class="radio customform">
							<input type="radio" name="button_icon_color" id="button_icon_color1"    class="select_color_1" value="" <?php echo $buttonIconColorFieldValue1; ?>/>
							<label for="button_icon_color1"></label>
						</div>
					</div>
					<div class="colorspan_1 color1">
					</div>
				</div>
				<div class="colorSelbg">
					<div class="checkradiobg pull-left">
						<div class="radio customform">
							<input type="radio" name="button_icon_color" id="button_icon_color2"    class="select_color_2" value="" <?php echo $buttonIconColorFieldValue2; ?>/>
							<label for="button_icon_color2"></label>
						</div>
					</div>
					<div class="colorspan_1 color2">
					</div>
				</div>
				<div class="colorSelbg">
					<div class="checkradiobg pull-left">
						<div class="radio customform">
							<input type="radio" name="button_icon_color" id="button_icon_color3"     class="select_color_3" value="" <?php echo $buttonIconColorFieldValue3; ?>/>
							<label for="button_icon_color3"></label>
						</div>
					</div>
					<div class="colorspan_1 color3">
					</div>
				</div>
				<div class="colorSelbg">
					<div class="checkradiobg pull-left">
						<div class="radio customform">
							<input type="radio" name="button_icon_color" id="button_icon_color4"     class="select_color_4" value="" <?php echo $buttonIconColorFieldValue4; ?> />
							<label for="button_icon_color4"></label>
						</div>
					</div>
					<div class="colorspan_1 color4">
					</div>
				</div>
				<!---
				<div class="colorSelbg">
					<div class="checkradiobg pull-left">
						<div class="radio customform">
							<input type="radio" id="white13" name="Button" value="male" class="radio">
							<label for="white13"></label>
						</div>
					</div>
					<div class="colorspan_1 textclr_5">
					</div>
				</div>  --->
				
			</div>
			<!-- /partcontainer -->
			<div class="partcontainer">
				<div class="headingbg_change">
					<?php echo lang('event_customize_colous_table_bg_1'); ?>
					<div class="infoiconbg_1 bg_5f6062 disILB customize_tooltip">
						<span class="show_tool_tip" title="<?php echo lang('tooltip_customize_colous_table_bg_1'); ?>" > </span>
					</div>
				</div>
				<div class="colorSelbg">
					<div class="checkradiobg pull-left">
						<div class="radio customform">
							<input type="radio" name="table_background1" id="table_background1_1"   class="select_color_1" value="" <?php echo $tableBackgroundFieldValue11; ?>/> 
							<label for="table_background1_1"></label>
						</div>
					</div>
					<div class="colorspan_1 color1">
					</div>
				</div>
				<div class="colorSelbg">
					<div class="checkradiobg pull-left">
						<div class="radio customform">
							<input type="radio" name="table_background1" id="table_background1_2"   class="select_color_2" value="" <?php echo $tableBackgroundFieldValue12; ?>/> 
							<label for="table_background1_2"></label>
						</div>
					</div>
					<div class="colorspan_1 color2">
					</div>
				</div>
				<div class="colorSelbg">
					<div class="checkradiobg pull-left">
						<div class="radio customform">
							<input type="radio" name="table_background1" id="table_background1_3"    class="select_color_3" value="" <?php echo $tableBackgroundFieldValue13; ?>/>
							<label for="table_background1_3"></label>
						</div>
					</div>
					<div class="colorspan_1 color3">
					</div>
				</div>
				<div class="colorSelbg">
					<div class="checkradiobg pull-left">
						<div class="radio customform">
							<input type="radio" name="table_background1" id="table_background1_4"    class="select_color_4" value="" <?php echo $tableBackgroundFieldValue14; ?>/> 
							<label for="table_background1_4"></label>
						</div>
					</div>
					<div class="colorspan_1 color4">
					</div>
				</div>
				<!---
				<div class="colorSelbg">
					<div class="checkradiobg pull-left">
						<div class="radio customform">
							<input type="radio" id="white14" name="TableBackground1" value="male" class="radio">
							<label for="white14"></label>
						</div>
					</div>
					<div class="colorspan_1 textclr_5">
					</div>
				</div> ---> 
				
			</div>
			<!-- /partcontainer -->
			<div class="partcontainer">
				<div class="headingbg_change">
					<?php echo lang('event_customize_colous_table_bg_2'); ?>
					<div class="infoiconbg_1 bg_5f6062 disILB customize_tooltip">
						<span class="show_tool_tip" title="<?php echo lang('tooltip_customize_colous_table_bg_2'); ?>" > </span>
					</div>
				</div>
				<div class="colorSelbg">
					<div class="checkradiobg pull-left">
						<div class="radio customform">
							<input type="radio" name="table_background2" id="table_background2_1"    class="select_color_1" value="" <?php echo $tableBackgroundFieldValue21; ?>/>
							<label for="table_background2_1"></label>
						</div>
					</div>
					<div class="colorspan_1 color1">
					</div>
				</div>
				<div class="colorSelbg">
					<div class="checkradiobg pull-left">
						<div class="radio customform">
							<input type="radio" name="table_background2" id="table_background2_2"    class="select_color_2" value="" <?php echo $tableBackgroundFieldValue22; ?>/>
							<label for="table_background2_2"></label>
						</div>
					</div>
					<div class="colorspan_1 color2">
					</div>
				</div>
				<div class="colorSelbg">
					<div class="checkradiobg pull-left">
						<div class="radio customform">
							<input type="radio" name="table_background2" id="table_background2_3"    class="select_color_3" value="" <?php echo $tableBackgroundFieldValue23; ?>/>
							<label for="table_background2_3"></label>
						</div>
					</div>
					<div class="colorspan_1 color3">
					</div>
				</div>
				<div class="colorSelbg">
					<div class="checkradiobg pull-left">
						<div class="radio customform">
							 <input type="radio" name="table_background2" id="table_background2_4"    class="select_color_4" value="" <?php echo $tableBackgroundFieldValue24; ?> />
							<label for="table_background2_4"></label>
						</div>
					</div>
					<div class="colorspan_1 color4">
					</div>
				</div>
				<!---
				<div class="colorSelbg">
					<div class="checkradiobg pull-left">
						<div class="radio customform">
							<input type="radio" id="white15" name="TableBackground2" value="male" class="radio">
							<label for="white15"></label>
						</div>
					</div>
					<div class="colorspan_1 textclr_5">
					</div>
				</div> --->
				
			</div>
			<!-- /partcontainer -->
			<div class="clearfix">
			</div>
		</div>
		<!-- textclr_bgcontainer -->
		<div class="spacer10">
		</div>
		<div class="row-fluid">
			<?php 
				echo form_hidden('eventId', $eventId);
				$extraSave 	= 'class="submitbtn_cus bg_5f6062 clr_99BBCC"';
				echo form_submit('save',lang('comm_save_changes'),$extraSave);
			?>
		</div>
		<div class="spacer36">
		</div>
	</div>
	<?php echo form_close(); ?>
</div>
<!-- /commonform_bg -->


<script src="<?php echo base_url('templates/system/js/colpick/colpick.js') ?>" type="text/javascript"></script>
<link rel="stylesheet" href="<?php echo base_url('templates/system/js/colpick/colpick.css') ?>" type="text/css"/>

<script type="text/javascript">

	$('.color-picker').colpick({
		color:'ff8800',
		onSubmit:function(hsb,hex,rgb,el) {
			//$(el).css('background-color', '#'+hex);
			//$(el).val('#'+hex);
			var pick_color_id = $(el).attr('pick_color_id');
			$(".select_color_"+pick_color_id).val('#'+hex);
			$("."+$(el).attr('id')).each(function(){
				$(this).css('background-color', '#'+hex);
			});
			$(el).colpickHide();
		}
	});

	//set default color
	$('.default_set_bg').each(function(){
		var hex = $(this).val();
		var pick_color_id = $(this).attr('id');
		$(".color"+pick_color_id).css('background-color', hex);
		$(".select_color_"+pick_color_id).val(hex);
	});

	//submit choose theme data value	
	ajaxdatasave('formColourTheme','<?php echo $this->uri->uri_string(); ?>',false,true,true);
	
	//show help icon popup
	$(".show_help_popup").mouseenter(function(){
		open_model_popup('customize_form_help_popup');
	});
	
	
</script>
