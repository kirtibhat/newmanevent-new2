<?php
$formCorporateSpaceType = array(
		'name'	 => 'formCorporateSpaceType',
		'id'	 => 'formCorporateSpaceType',
		'method' => 'post',
		'class'  => 'wpcf7-form',
		'data-parsley-validate' => '',
); 
$spaceTypeName = array(
		'name'	=> 'space_name',
		'value'	=> '',
		'id'	=> 'space_name',
		'type'	=> 'text',
		'required'	=> '',
		'class' =>'small',
		'autocomplete' => 'off',
		'data-parsley-error-message' => lang('common_field_required'),
		'data-parsley-error-class' => 'custom_li',
		'data-parsley-trigger' => 'keyup',
);	


?>
<div id="space_type_popup" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" >
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header delegates">
        <h4 class="medium dt-large modal-title">Add Space Type</h4>
      </div>

      <div class="modal-body small">
        <div class="modelinner ">
          <?php echo form_open($this->uri->uri_string(),$formCorporateSpaceType); ?>
          
            <div class="control-group mb10 eventdashboard_popup small">

              <div class="row-fluid">
              	<label class="pull-left" for="space_name"><?php echo lang('corporate_space_name'); ?> <span class="astrik">*</span>
                	<span class="info_btn"><span class="field_info xsmall"><?php echo lang('tooltip_corporate_space_name'); ?></span></span>
                </label>
                <?php echo form_input($spaceTypeName); ?>
              </div>

              <div class="row-fluid">
              	<label class="pull-left" for="space_ava"><?php echo lang('corporate_space_available'); ?>  <span class="astrik">*</span>
                	<span class="info_btn"><span class="field_info xsmall"><?php echo lang('tooltip_corporate_space_available'); ?></span></span>
                </label>
                <input type="number" min="2" max="20" step="2" class="small dark" required value="6" name="space_ava" id="space_ava">
              </div>

              <div class="row-fluid spaces_check">
               <?php $totalSpaceAvalible = (isset($totalSpaceAvalible) && $totalSpaceAvalible!="") ? $totalSpaceAvalible : "0";  
                for($i=1;$i<=$totalSpaceAvalible;$i++){
					$boothNo = "";
					$boothNo .= (strlen($i)==1) ? '0' : "";
					$boothNo .= $i;
               ?>
                   <div class="checkbox_wrapper">
					  <input type="checkbox" checked="" name="space[]" value="<?php echo $boothNo; ?>" id="space<?php echo $i; ?>">
                      <label for="space<?php echo $i; ?>"><?php echo $boothNo; ?></label>
                  </div>
               <?php } // end loop  ?>
              
              </div>
              <div class="btn_wrapper">
				  
					<?php 
						echo form_hidden('eventId', $eventId);
						
					?>	 <input type="submit" name="loginsubmit" value="Save" class=" submitbtn pull-right medium">
						 <input type="submit" class="popup_cancel submitbtn pull-right medium" value="Cancel" name="logincancel" data-dismiss="modal">
              </div>

            </div>
         <?php echo form_close(); ?>
        </div>
      </div>
    </div>
  </div>
</div>
