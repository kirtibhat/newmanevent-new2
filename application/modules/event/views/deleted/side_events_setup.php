<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 |--------------------------------------------------------------------------|
 | This Field define for temparay open popup by add section id 				|
 | and edit record id														|	
 |--------------------------------------------------------------------------|
*/
 
$tempHidden = array(
	'type' => 'hidden',
	'name' => 'tempHidden',
	'id' => 'tempHidden',
	'value' => '',
);

echo form_input($tempHidden);
?>

<?php $this->load->view('event_menus');  ?>

<div class="row-fluid mt20">
	<div class="span7">
		<div class="formparag pl0 pr0">
            <?php echo lang('event_side_event_discription'); ?>
            </div>
            <div class="spacer15"></div>
				
				<div class="regidetrailHeading">
					<div class="addanother_event_new"><a href="javascript:void(0)" class="add_edit_side_event" actionform="add" sideeventid="0"><div class="add_img">&nbsp;</div> <?php echo lang('event_add_side_event'); ?></a></div>
					<div class="clearfix"></div>
				</div>
				
				<div class="spacer15"></div>

			<?php 
				if(!empty($sideeventdata)){
					
					//prepare array data for current and next form hide/show  
					foreach($sideeventdata as $sideevent){
						$recordId[] =  $sideevent->id;
					}
					
					//show side event forms	
					$rowCount = 0;	
					foreach($sideeventdata as $sideevent){
						$nextId = $rowCount+1;
						$sendData['sideevent'] 		=$sideevent; 
						$sendData['nextRecordId']	= (isset($recordId[$nextId]))?$recordId[$nextId]:'0';
						// daynamic load all registrant forms
						$this->load->view('form_side_events_setup',$sendData);
						$rowCount++;
					}
				}
			?>
			
		<?php 
			//next and previous button
			$buttonData['viewbutton'] = array('back','next','preview');
			$buttonData['linkurl'] 	= array('back'=>base_url('event/setupexhibitors'),'next'=>base_url('event/setuppayment'),'preview'=>'');
			$this->load->view('common_back_next_buttons',$buttonData);
		?>
		
    </div>
	
	<!----Right side advertisement view load start------>
		<?php $this->load->view('right_side_advert'); ?>
	<!----Right side advertisement view load end------>
	
</div>

<script type="text/javascript" language="javascript">
	
	/*-----show time on form for side event-------*/
	/*$(".issideeventshowtimeonform").click(function(){
		var getid=$(this).attr('sideeventshowtimeonformid');
		var getchecked=$(this).attr('checked');
		if(getchecked=="checked" && getchecked!=undefined ){
			showarea("#isallow_timestartend_"+getid);
			$(".timestartendrequired"+getid).attr('required',''); 
			$(".timestartendrequired"+getid).removeAttr('disabled');
		}else{
			hidearea("#isallow_timestartend_"+getid);
			$(".timestartendrequired"+getid).removeAttr('required');
			$(".timestartendrequired"+getid).attr('disabled',''); 
		}
	});*/
	
	/*-----allow early bird date for side event-------*/
	$(".isallowearlybirddate").click(function(){
		var getid=$(this).attr('sideeventid');
		var getchecked=$(this).attr('checked');
		if(getchecked=="checked" && getchecked!=undefined ){
			showarea("#is_show_allow_earlybirddate"+getid);
			$(".earlybirdaction"+getid).attr('required',''); 
			$(".earlybirdaction"+getid).removeAttr('disabled');
		}else{
			hidearea("#is_show_allow_earlybirddate"+getid);
			$(".earlybirdaction"+getid).removeAttr('required');
			$(".earlybirdaction"+getid).attr('disabled',''); 
		}
	});
	
	//registrant selection Limit
	$(".registrantSelectionLimit").click(function(){
		var getid=$(this).attr('registrantselectionlimitid');
		
		var getchecked=$(this).attr('checked');
		if(getchecked=="checked" && getchecked!=undefined ){
			showarea("#registrant_selection_limit_div"+getid);
			$("#registrantSelection"+getid).removeAttr('disabled');
		}else{
			hidearea("#registrant_selection_limit_div"+getid);
			$("#registrantSelection"+getid).attr('disabled','disabled'); 
		}	
	});
	
	//single day registrant selection Limit
	$(".singleRegistrantSelectionLimit").click(function(){
		var getid=$(this).attr('singleregistrantselectionlimitid');
		var getchecked=$(this).attr('checked');
		if(getchecked=="checked" && getchecked!=undefined ){
			showarea("#single_registrant_selection_limit_div"+getid);
			$("#singleregistrantselection"+getid).removeAttr('disabled');
		}else{
			hidearea("#single_registrant_selection_limit_div"+getid);
			$("#singleregistrantselection"+getid).attr('disabled','disabled'); 
		}	
	});
	
	//registrant Selection
	$(".registrantSelection").click(function(){
		var getid=$(this).attr('registrantselectionid');
		var getchecked=$(this).attr('checked');
		if(getchecked=="checked" && getchecked!=undefined ){
			showarea("#side_event_registraint_list"+getid);
			$("#registrantSelectionLimit"+getid).attr('disabled','disabled'); 
			$(".registrantselection"+getid).attr('required',''); 
		}else{
			hidearea("#side_event_registraint_list"+getid);
			$("#registrantSelectionLimit"+getid).removeAttr('disabled');
			$(".registrantselection"+getid).removeAttr('required');
		}	
	});
	
	//check registrant details selection popup
	/*$(".checkboxdisable").click(function() {
		var getId = $(this).attr('id');
		if($('#registrantSelection'+getId).is(':disabled')){
			custom_popup('Please selecte type of registrant.',false);
		}
	});*/
	
	
	
	
	/*-----complimentary checked show price, GST Include, earlybird price etc.-------*/
	$(".sideEventregiscomaplimentary").click(function(){
		var getid=$(this).attr('sideEventregiscomaplimentaryid');
		var getchecked=$(this).attr('checked');
		if(getchecked=="checked" && getchecked!=undefined ){
			hidearea("#compli_checked_div_section"+getid);
			$(".registrantcomplimentary"+getid).removeAttr('required');
		}else{
			showarea("#compli_checked_div_section"+getid);
			$(".registrantcomplimentary"+getid).attr('required',''); 
		}	
	});
	
	
	/*-----regitrant listing select if required additiona guest.-------*/
	$(".SideRegisAddiGuests").click(function(){
		var getid=$(this).attr('sideregisaddiguestsid');
		var getchecked=$(this).attr('checked');
		if(getchecked=="checked" && getchecked!=undefined ){
			showarea(".side_regis_addi_guests_div"+getid);
			$(".registrantselectionaddi"+getid).attr('required',''); 
		}else{
			hidearea(".side_regis_addi_guests_div"+getid);
			$(".registrantselectionaddi"+getid).removeAttr('required');
		}	
	});
	
	
	/*-----regitrant listing select if required complimentary guest.-------*/
	$(".SideRegisCompliGuests").click(function(){
		var getid=$(this).attr('sideregiscompliguestsid');
		var getchecked=$(this).attr('checked');
		if(getchecked=="checked" && getchecked!=undefined ){
			showarea(".side_regis_compli_guests_div"+getid);
			$(".registrantselectioncompli"+getid).attr('required',''); 
		}else{
			hidearea(".side_regis_compli_guests_div"+getid);
			$(".registrantselectioncompli"+getid).removeAttr('required');
		}	
	});
	
	
	/*-----Open add and edit side event popup-------*/
	$(document).on('click','.add_edit_side_event',function(){
		
		var formAction = $(this).attr('actionform');
		
		if($(this).attr('sideeventid')!==undefined){
			var sideeventid = parseInt($(this).attr('sideeventid'));
			//set value in assing
			$("#tempHidden").val(sideeventid);
		}else{
			//get value form temp hidden and set
			var sideeventid = parseInt($("#tempHidden").val());
		}
	
		var eventId = '<?php echo $eventId ?>';
		var sendData = {"sideeventid":sideeventid, "eventId":eventId, "formAction":formAction};
		
		//call ajax popup function
		ajaxpopupopen('add_side_event_popup','event/addeditduplicatesideeventview',sendData,'add_edit_side_event');
	});
	
	//save side event add and edit data
	ajaxdatasave('formAddSideEvent','event/addeditduplicatesideeventsave',false,true,true,false,false,false,false,false);
	
	/*---This function is used to delete custome field for side event -----*/
	customconfirm('delete_side_event','event/deletesideevent');
	
	var gstRate = parseInt('<?php echo (!empty($eventdetails->gst_rate))?$eventdetails->gst_rate:"0"; ?>');
	
	/*----This function is used to registrant selection unit price enter and show-----*/
	unitPriceManage('unitPriceEnter','regisSideEventId','SideEventRegisGSTInclude','SideEventRegisTotalPrice');
	
	/*----This function is used to registrant selection early unit price enter and show-----*/
	unitPriceManage('earlyUnitPriceEnter','earlyRegisSideEventId','SideEventRegisEarlyGSTInclude','SideEventRegisEarlyTotalPrice');
	
	/*----This function is used to registrant additional guest selection unit price enter and show-----*/
	unitPriceManage('addUnitPriceEnter','addRegisSideEventId','SideRegisAddiGuestsGST','SideRegisAddiGuestsPrice');
	
	/*----This function is used to registrant complite guest selection unit price enter and show-----*/
	unitPriceManage('compliUnitPriceEnter','compliRegisSideEventId','SideRegisCompliGuestsGST','SideRegisCompliGuestsPrice');
		
</script>	
