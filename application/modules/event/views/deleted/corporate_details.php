<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 |--------------------------------------------------------------------------|
 | This Field define for temparay open popup by add section id 				|
 | and edit record id														|	
 |--------------------------------------------------------------------------|
*/

$tempHidden = array(
	'type' => 'hidden',
	'name' => 'tempHidden',
	'id' => 'tempHidden',
	'value' => '',
);

echo form_input($tempHidden);

$this->load->view('event/event_menus'); 


?>

<div class="row-fluid-15">
        <div id="page_content" class="span9">
			
			<div id="accordion" class="panel-group">
				<?php 
					
					// load corporate floor plan
					$this->load->view('form_corporate_floor_plan');
					
					// load default package benefit
					$this->load->view('form_default_package_benefit');
					
					// load default additional purchase item
					$this->load->view('default_additional_purchase_item');
				
					/*
					* this view load master personal details for set for new
					* created registrant personal details form
					*/
					 
					$this->load->view('corporate_application_details');
					
					// load corporate package details
					$this->load->view('add_corporate_package');
					
					// load corporate category
					$this->load->view('add_corporate_category');
					
					// load corporate package details
					$this->load->view('add_corporate_package_details');
					
				?>
			</div>
			
			<?php 
				//next and previous button
				$buttonData['viewbutton'] = array('next','preview','back');
				$buttonData['linkurl'] 	= array('back'=>base_url('event/eventdetails'),'next'=>base_url('event/setupregistrant'),'preview'=>'');
				$this->load->view('common_back_next_buttons',$buttonData);
			?>
			
            <!--end of page btn wrapper-->	
		</div>
        <!--end of page content-->
</div>
<!--end of row-->


<div id="opentModelBox"></div>

<!--start custom tooltip-->
<div id="tooltip_wrapper"></div>
<!--end custom tooltip-->
