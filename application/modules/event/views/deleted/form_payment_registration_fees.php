<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//define current and next form div id
$FormDivId = 'SecondForm';
$nextFormDivId = 'ThirdForm';

$registfeedetails     = (!empty($registfeedetails))?$registfeedetails[0]:'';
$paymentRegisId       = (!empty($registfeedetails->id))?$registfeedetails->id:'0';
$registerationFeeValue    = (!empty($registfeedetails->registeration_fee))?$registfeedetails->registeration_fee:'';
$whoWillPayFeeValue     = (isset($registfeedetails->fee_to_be_paid_by))?$registfeedetails->fee_to_be_paid_by:'';
$regisUnitPriceValue    = (isset($registfeedetails->price_per_registrant))?$registfeedetails->price_per_registrant:'';
$regisGSTIncludedValue    = (isset($registfeedetails->gst))?$registfeedetails->gst:'';
$regisTotalPriceValue     = (isset($registfeedetails->total_price))?$registfeedetails->total_price:'';


$registerationFeeYesValue   = false;
$registerationFeeNoValue  = false;
$whoWillPayFeeRegisValue  = false;
$whoWillPayFeeOrgaiValue  = false;

//set value
if($registerationFeeValue=='1'){
  $registerationFeeYesValue = true;
  $registerationFeeNoValue = false;
}else{
  $registerationFeeYesValue = false;
  $registerationFeeNoValue = true;
}

//set value
if($whoWillPayFeeValue=='0'){
  $whoWillPayFeeRegisValue = true;
  $whoWillPayFeeOrgaiValue = false;
}else{
  $whoWillPayFeeRegisValue = false;
  $whoWillPayFeeOrgaiValue = true;
}

$formRegistrationFeesSetup = array(
    'name'   => 'formRegistrationFeesSetup',
    'id'   => 'formRegistrationFeesSetup',
    'method' => 'post',
    'class'  => 'form-horizontal',
    'data-parsley-validate' => '',
  );

$registrationFeeYes = array(
    'name'  => 'registrationFee',
    'value' => '1',
    'id'  => 'registrationFeeYes',
    'type'  => 'radio',
    'required'  => '',
    'checked' => $registerationFeeYesValue,
    'data-parsley-error-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
  );  

$registrationFeeNo = array(
    'name'  => 'registrationFee',
    'value' => '0',
    'id'  => 'registrationFeeNo',
    'type'  => 'radio',
    'required'  => '',
    'checked' => $registerationFeeNoValue,
  );  
  
$whoWillPayFeeRegis = array(
    'name'  => 'whoWillPayFee',
    'value' => '0',
    'id'  => 'payTheRegistrationFeeYes',
    'type'  => 'radio',
    'required'  => '',
    'checked' => $whoWillPayFeeRegisValue,
    
  );  

$whoWillPayFeeOrgai = array(
    'name'  => 'whoWillPayFee',
    'value' => '1',
    'id'  => 'whoWillPayFeeOrgai',
    'type'  => 'radio',
    'required'  => '',
    'checked' => $whoWillPayFeeOrgaiValue,
  );  

$regisTotalPrice = array(
    'name'  => 'regisTotalPrice',
    'value' =>  $regisTotalPriceValue,
    'id'  => 'regisTotalPrice',
    'required'  => '',
    'type' => 'text',
    'class' => 'small short_field regisTotalPriceEnter numValue',
    'data-parsley-error-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
  );

$regisGSTIncluded = array(
    'name'  => 'regisGSTIncluded',
    'value' =>  $regisGSTIncludedValue,
    'id'    => 'regisGSTIncluded',
    'required'  => '',
    'type'    => 'text',
    'readonly' => '',
    'class' => 'small short_field',
    'data-parsley-error-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
  );  
?>

<div class="panel event_panel">
  <div class="panel-heading ">
      <h4 class="panel-title medium dt-large heading_btn ">
      <a class="" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
      <?php echo lang('event_payment_registrant_fees'); ?>
     </a>
      </h4>
  </div>

  <div style="height: auto;" id="collapseTwo" class="accordion_content collapse apply_content">
  <?php echo form_open($this->uri->uri_string(),$formRegistrationFeesSetup); ?>
    <div class="form-horizontal form_open_chk" id="showhideformdiv<?php echo $FormDivId; ?>">    
      <div class="panel-body ls_back dashboard_panel small">
        <div class="row-fluid-15">
          <label for="info_org"><?php echo lang('event_payment_pay_registra_fees'); ?><span class="astrik">*</span>
            <span class="info_btn">
                <span class="field_info xsmall"><?php echo lang('tooltip_payment_pay_registra_fees'); ?>
                  </span>
              </span>
          </label>
           
          <div class="radio_wrapper ">
            <?php echo form_radio($registrationFeeYes); ?> 
            <label for="registrationFeeYes"><?php echo lang('event_paymen_fees_yes'); ?> </label>
            <?php echo form_radio($registrationFeeNo); ?>
            <label for="registrationFeeNo"><?php echo lang('event_paymen_fess_no'); ?> </label>
          </div>

          <label for="info_org"><?php echo lang('event_payment_paid_by'); ?> <span class="astrik">*</span>
            <span class="info_btn">
                <span class="field_info xsmall"><?php echo lang('event_payment_paid_by'); ?>
                  </span>
              </span>
          </label>
            <div class="radio_wrapper ">
              <?php echo form_radio($whoWillPayFeeRegis); ?> 
              <label for="payTheRegistrationFeeYes"><?php echo lang('event_paymen_pay_by_1'); ?> </label>
              <?php echo form_radio($whoWillPayFeeOrgai); ?>
              <label for="whoWillPayFeeOrgai"><?php echo lang('event_paymen_pay_by_2'); ?> </label>
            </div>
            <!--end of session sub category-->          
        </div>
        
        <div class="row-fluid-15">
            <label for="reg_limit"><?php echo lang('event_payment_register_total_price'); ?> <span class="astrik">*</span>
              <span class="info_btn"></span>
            </label>
            <?php echo form_input($regisTotalPrice); ?>
            <?php echo form_error('regisTotalPrice'); ?>
        </div>
        <div class="row-fluid-15">
            <label for="reg_limit"><?php echo lang('event_payment_register_gst_include'); ?></label>
            <?php echo form_input($regisGSTIncluded); ?>
            <?php echo form_error('regisGSTIncluded'); ?>
        </div>
        <div class="btn_wrapper ">
          <a href="#collapseTwo" class="pull-left scroll_top">Top</a>
          <?php 
            echo form_hidden('eventId', $eventId);
            echo form_hidden('formActionName', 'registrationFees');
            echo form_hidden('paymentRegisId', $paymentRegisId);
            
            //button show of save and reset
            $formButton['showbutton']   = array('save','reset');
            $formButton['labletext']  = array('save'=>lang('comm_save_changes'),'reset'=>lang('comm_reset'));
            $formButton['buttonclass']  = array('save' => 'default_btn btn pull-right medium', 'reset' => 'default_btn btn pull-right medium reset_form');
            $this->load->view('common_save_reset_button',$formButton);
          ?>
        </div>
      </div>
    </div>
  <?php echo form_close(); ?>
  </div>
</div>
<!--end of panel-->

<script type="text/javascript">
  //payment registration save 
  ajaxdatasave('formRegistrationFeesSetup','<?php echo $this->uri->uri_string(); ?>',true,true,false,false,false,'#showhideformdiv<?php echo $FormDivId; ?>','#showhideformdiv<?php echo $nextFormDivId; ?>');
</script>
