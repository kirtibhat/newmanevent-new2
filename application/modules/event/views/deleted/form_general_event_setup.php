<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

//set field value in variable
$geseventtitle           = (!empty($eventdetails->event_title))?$eventdetails->event_title:'';
$geseventdescription     = (!empty($eventdetails->description))?$eventdetails->description:'';
$geseventlocation        = (!empty($eventdetails->event_location))?$eventdetails->event_location:'';
$geseventvenue       = (!empty($eventdetails->event_venue))?$eventdetails->event_venue:'';
$geseventspeaker       = (!empty($eventdetails->event_speaker))?$eventdetails->event_speaker:'';
$geseventlimit       = (!empty($eventdetails->event_limit))?$eventdetails->event_limit:'';
$geseventnumberrego      = (!empty($eventdetails->number_of_rego))?$eventdetails->number_of_rego:'';
$sittingStyleValue     = (!empty($eventdetails->sitting_style))?$eventdetails->sitting_style:'';
$timeZoneValue       = (!empty($eventdetails->time_zone))?$eventdetails->time_zone:'';
$destinationValue      = (!empty($eventdetails->destination))?$eventdetails->destination:'';
$lastRegDateValue      = (!empty($eventdetails->last_reg_date))?$eventdetails->last_reg_date:'';
$eventNumberValue      = (!empty($eventdetails->event_number))?$eventdetails->event_number:'';
$eventShrtFrmTiteValue   = (!empty($eventdetails->short_form_title))?$eventdetails->short_form_title:getAcronyms($geseventtitle);
$eventRefNumberValue     = (!empty($eventdetails->event_reference_number))?$eventdetails->event_reference_number:'';

//get event number 
$eventNumberValueArray   = str_split($eventNumberValue); 

//set value
$geseventlastregdate = '';
if(!empty($eventdetails->last_reg_date)){
  if($eventdetails->last_reg_date=="0000-00-00 00:00:00"){
    $geseventlastregdate = '';
  }else{
    $geseventlastregdate = dateFormate($eventdetails->last_reg_date, "d M Y H:i");
  }
}

//event early bird date
$geseventearlybirdregdate = '';
if(!empty($eventdetails->early_bird_reg_date)){
  if($eventdetails->early_bird_reg_date==""){
    $geseventearlybirdregdate = '';
  }else{
    $geseventearlybirdregdate = dateFormate($eventdetails->early_bird_reg_date, "d M Y H:i");
  }
}


//$geseventlastregdate  = (!empty($eventdetails->last_reg_date)) ? dateFormate($eventdetails->last_reg_date, "d M Y"):'';
$geseventstarttime    = (!empty($eventdetails->starttime))?dateFormate($eventdetails->starttime, "d M Y H:i"):'';
$geseventendtime      = (!empty($eventdetails->endtime))?dateFormate($eventdetails->endtime, "d M Y H:i"):'';
$geseventeventid      = (!empty($eventdetails->event_id))?$eventdetails->event_id:'';

$timeZoneOptions      = array(''=>'Select Time Zone','AWST'=>'Australian Western Standard Time','ACST'=>'Australian Central Standard Time','AEST'=>'Australian Eastern Standard Time');

$formGeneralEventSetup = array(
    'name'   => 'formGeneralEventSetup',
    'id'   => 'formGeneralEventSetup',
    'method' => 'post',
    'class'  => '',
    'data-parsley-validate' => ''
);
  
$eventName = array(
    'name'  => 'eventName',
    'value' => $geseventtitle,
    'id'  => 'eventName',
    'type'  => 'text',
    'placeholder' => 'Event Name',
    'required'  => '',
    'class'=>'alphaNumValue small',
    'autocomplete' => 'off',
    'data-parsley-error-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
  );
$eventUrl = array(
    'name'  => 'eventUrl',
    'value' => replaceSpaceWithCharacter($geseventtitle,"_"),
    'id'  => 'eventUrl',
    'type'  => 'text',
    'placeholder' => 'Event URL',
    'required'  => '',
    'class'=>'small dark width_130',
    'autocomplete' => 'off',
    'data-parsley-error-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
  );  

$eventShortFormTitle = array(
    'name'  => 'eventShortFormTitle',
    'value' => $eventShrtFrmTiteValue,
    'id'  => 'eventShortFormTitle',
    'type'  => 'text',
    'class' => 'small dark width_214'
  );

$eventReferenceNumber = array(
    'name'  => 'eventReferenceNumber',
    'value' => $eventRefNumberValue,
    'id'  => 'eventReferenceNumber',
    'type'  => 'text',
    'class' => 'small'
);
  
$eventStartDate = array(
    'name'  => 'eventStartDate',
    'value' =>  $geseventstarttime,
    'id'  => 'eventStartDate',
    'type'  => 'text',
    'placeholder' => 'Event Start Date',
    'required'  => '',
    'readonly'  => '',
    'class' => 'date_input small',//' datetimepicker',
    'data-format' => 'yyyy-MM-dd hh:mm:ss',
    //data-format="dd/MM/yyyy hh:mm:ss"
    'autocomplete' => 'off',
    'data-parsley-error-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'change',
);
$eventEndDate = array(
    'name'  => 'eventEndDate',
    'value' => $geseventendtime,
    'id'  => 'eventEndDate',
    'type'  => 'text',
    'placeholder' => 'Event End Date',
    'required'  => '',
    'readonly'  => '',
    'class' => 'date_input small',
    'data-format' => 'yyyy-MM-dd hh:mm:ss',
    'autocomplete' => 'off',
    'data-parsley-error-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
  );
  
$eventDescription = array(
    'name'  => 'eventDescription',
    'value' => $geseventdescription,
    'id'  => 'eventDescription',
    'rows' => '3',
    'class' => 'small'
);  
$eventMaxRegistrants = array(
    'name'  => 'eventMaxRegistrants',
    'value' => $geseventnumberrego,
    'id'  => 'eventMaxRegistrants',
    'type'  => 'text',
    'class' => 'small  short_field',
    'required'  => '',
    'autocomplete' => 'off',
    'data-parsley-type'=>'integer',
    'data-parsley-type-message' => lang('enter_numeric_value'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
  );

$registerLastDate = array(
    'name'  => 'registerLastDate',
    'value' => $geseventlastregdate,
    'id'  => 'registerLastDate',
    'type'  => 'text',
    'required'  => '',
    'readonly'  => '',
    'class' => 'date_input small',
    'data-format' => 'yyyy-MM-dd hh:mm:ss',
    'autocomplete' => 'off',
    'data-parsley-error-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'change',
  );
  
  
   
$earlybirdCutOffDate = array(
  'name'  => 'earlybirdCutOffDate',
  'value' => $geseventearlybirdregdate,
  'id'  => 'earlybirdCutOffDate',
  'type'  => 'text',
  'readonly'  => '',
  'class' => 'date_input small',
  'data-format' => 'yyyy-MM-dd hh:mm:ss',
  'autocomplete' => 'off',
  'data-parsley-error-message' => lang('common_field_required'),
  'data-parsley-error-class' => 'custom_li',
  'data-parsley-trigger' => 'change',
);
  
$eventDestination = array(
    'name'  => 'eventDestination',
    'value' => $destinationValue,
    'id'  => 'eventDestination',
    'type'  => 'text',
    'class'   => 'small',
);
  
$eventLocation = array(
    'name'  => 'eventLocation',
    'value' => $geseventlocation,
    'id'  => 'eventLocation',
    'type'  => 'text',
    'class'     => 'small',
);

$eventVenue = array(
    'name'  => 'eventVenue',
    'value' => $geseventvenue,
    'id'  => 'eventVenue',
    'type'  => 'text',
    'required'  => '',
    'class' => 'small',
    'autocomplete' => 'off',
    'data-parsley-error-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
);
        
$eventSponsorsLogos = array(
    'name'  => 'eventSponsorsLogos',
    'value' => '1',
    'id'  => 'eventSponsorsLogos',
    'type'  => 'checkbox',
);
  
$logo_list_div = "dn";
if(!empty($eventsponsorslogos)){
  $logo_list_div = "";
  $eventSponsorsLogos['checked']=true;
}

    
?>

<!--start venue search popup start--->
  <?php $this->load->view('venue_search_popup'); ?>
<!--start venue search popup end--->


 <div class="panel event_panel">
                    <div class="panel-heading ">
                        <h4 class="panel-title medium dt-large ">
              <a class=" " data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><?php echo lang('event_general_event_setup'); ?></a>
                        </h4>
                    </div>
                    <div style="height: auto;" id="collapseOne" class="accordion_content collapse apply_content ">
                      <?php echo form_open($this->uri->uri_string(),$formGeneralEventSetup); ?>
                        <input type="hidden" name="collapseValue" value="collapseTwo" />
                        <div class="panel-body ls_back dashboard_panel small" id="showhideformdivgeneralSection">
          
                            <div class="row-fluid-15">
                                <label for="amc_con"><?php echo lang('event_event_name'); ?> <span class="astrik">*</span></label>
                                <?php echo form_input($eventName); ?>
                <?php echo form_error('eventName'); ?>
                            </div>
                            <!--end of row-fluid-->
          
                            <div class="row-fluid-15">
                                <label for="short_name"><?php echo lang('event_event_short_form_title'); ?> <span class="astrik">*</span>
                                  <span class="info_btn"><span class="field_info xsmall"><?php echo lang('tooltip_event_short_form_title'); ?></span></span>
                                </label>
                               
                                <input type="text" id="short_name_prefix" name="short_name_prefix" value="<?php 
                  echo (isset($eventNumberValueArray[0]))?$eventNumberValueArray[0]:""; 
                  echo (isset($eventNumberValueArray[1]))?$eventNumberValueArray[1]:"";
                  echo (isset($eventNumberValueArray[2]))?$eventNumberValueArray[2]:"";
                  echo (isset($eventNumberValueArray[3]))?$eventNumberValueArray[3]:"";
                                 ?>" required class="small dark width_55" disabled/>
                                
                                <span class="hyphen_seprator"></span>
                                <?php echo form_input($eventShortFormTitle); ?>
                <?php echo form_error('eventShortFormTitle'); ?>
                            </div>
                            <!--end of row-fluid-->
          
                            <div class="row-fluid-15">
                                <label for="refr_no"><?php echo lang('event_event_reference_number'); ?>
                                  <span class="info_btn"><span class="field_info xsmall"><?php echo lang('tooltip_event_reference_number'); ?></span></span>
                                </label>
                                <?php echo form_input($eventReferenceNumber); ?>
                <?php echo form_error('eventReferenceNumber'); ?>
                            </div>
                            <!--end of row-fluid-->
          
                            <div class="row-fluid-15">
                                <label for="event_url"><?php echo lang('event_event_url'); ?> <span class="astrik">*</span></label>
                                <input type="text" id="event_url_prefix" name="event_url_prefix" value="http://newmanevents.com/" required class="small dark width_192" disabled/>
                                <?php echo form_input($eventUrl); ?>
                <?php echo form_error('eventUrl'); ?>
                            </div>
                            <!--end of row-fluid-->
          
                            <div class="row-fluid-15">
                                <label for="start_date">Start <span class="astrik">*</span></label>
                                <div class="date_wrapper" id="datetimepicker1" >
                    <?php echo form_input($eventStartDate); ?>
                  <?php echo form_error('eventStartDate'); ?>
                                    <span class="add-on"><a href="javascript:void(0)" class="datepicker_btn datetimepicker"  data-date-icon="icon-calendar" data-time-icon="icon-time"></a></span>
                                </div>
                                
                            </div>
                            <!--end of row-fluid-->
          
                            <div class="row-fluid-15">
                                <label for="finish_date"><?php echo lang('event_event_end_data'); ?> <span class="astrik">*</span></label>
                                <div class="date_wrapper" id="datetimepicker2">
                                    <?php echo form_input($eventEndDate); ?>
                                    <?php echo form_error('eventEndDate'); ?>
                                    <span class="add-on"><a href="javascript:void(0)" class="datepicker_btn datetimepicker"  data-date-icon="icon-calendar" data-time-icon="icon-time"></a></span>
                                    <div class="common_errormsg xsmall dn" id="end_date_error">
                    <?php echo lang('event_end_date_error'); ?>
                  </div>
                                </div>
                            </div>
                            <!--end of row-fluid-->
          
                            <div class="row-fluid-15">
                                <label for="time_zone"><?php echo lang('event_time_zone'); ?> <span class="astrik">*</span>
                                  <span class="info_btn"><span class="field_info xsmall"><?php echo lang('tooltip_time_zone'); ?></span></span>
                                </label>
                               <?php 
                                $other = ' id="timeZone" required ="" class="small custom-select custom_li" data-parsley-error-message="'.lang('common_field_required').'" ';
                  echo form_dropdown('timeZone',$timeZoneOptions,$timeZoneValue,$other);
                    echo form_error('timeZone');
                 ?>
                            </div>
                            <!--end of row-fluid-->
          
                            <div class="row-fluid-15">
                                <label for="desc"><?php echo lang('event_description_details_event'); ?>
                                  <span class="info_btn"><span class="field_info xsmall"><?php echo lang('tooltip_description_details'); ?></span></span>
                                </label>
                                <?php echo form_textarea($eventDescription); ?>
                                <?php echo form_error('eventDescription'); ?>
                            </div>
                            <!--end of row-fluid-->
          
                            <div class="row-fluid-15">
                                <label for="reg_limit"><?php echo lang('event_max_registrant_event'); ?> <span class="astrik">*</span>
                                  <span class="info_btn"><span class="field_info xsmall"><?php echo lang('tooltip_max_registrant_event'); ?></span></span>
                                </label>
                                <?php echo form_input($eventMaxRegistrants); ?>
                <?php echo form_error('eventMaxRegistrants'); ?>
                            </div>
                            <!--end of row-fluid-->
          
                            <div class="row-fluid-15">
                                <label for="reg_cutoff"><?php echo lang('event_last_date_register'); ?> <span class="astrik">*</span>
                                  <span class="info_btn"><span class="field_info xsmall"><?php echo lang('tooltip_last_date_regis'); ?></span></span>
                                </label>
                                <div class="date_wrapper" id="datetimepicker3">
                                    <?php echo form_input($registerLastDate); ?>
                                    <span class="add-on"><a href="javascript:void(0)" class="datepicker_btn datetimepicker"  data-date-icon="icon-calendar" data-time-icon="icon-time"></a></span>
                  <div class="common_errormsg xsmall dn" id="last_regis_date_error">
                    <?php echo lang('event_event_register_last_error'); ?>
                  </div>
                                </div>
                               
                            </div>
                            <!--end of row-fluid-->
          
                            <div class="row-fluid-15">
                                <label for="early_cutoff"><?php echo lang('event_early_bird_date_register'); ?>
                                  <span class="info_btn"><span class="field_info xsmall"><?php echo lang('tooltip_earlybird_cutt_off_date'); ?></span></span>
                                </label>
                                <div class="date_wrapper" id="datetimepicker4">
                                    <?php echo form_input($earlybirdCutOffDate); ?>
                                    <span class="add-on"><a href="javascript:void(0)" class="datepicker_btn datetimepicker"  data-date-icon="icon-calendar" data-time-icon="icon-time"></a></span>
                                    <div class="common_errormsg xsmall dn" id="early_bird_regis_date_error">
                    <?php echo lang('event_event_register_early_bird_error'); ?>
                  </div>
                                </div>
                            </div>
                            <!--end of row-fluid-->
          
                            <div class="row-fluid-15">
                                <label for="seating_style"><?php echo lang('event_sitting_style'); ?>
                                  <span class="info_btn"><span class="field_info xsmall"><?php echo lang('tooltip_sitting_style'); ?></span></span>
                                </label>
                                <?php 
                  //define sitting style array  
                  $sittingStyleArray = $this->config->item('seating_style');  
                  
                  $other = ' id="sittingStyle" class="small custom-select "  ';
                  echo form_dropdown('sittingStyle',$sittingStyleArray,$sittingStyleValue,$other);
                    echo form_error('sittingStyle');
                ?>
                            </div>
                            <!--end of row-fluid-->
          
                            <div class="row-fluid-15">
                                <label for="destination"><?php echo lang('event_event_destination'); ?>
                                  <span class="info_btn"><span class="field_info xsmall"><?php echo lang('event_event_destination'); ?></span></span>
                                </label>
                                <?php echo form_input($eventDestination); ?>
                <?php echo form_error('eventDestination'); ?>
                            </div>
                            <!--end of row-fluid-->
          
                            <div class="row-fluid-15">
                                <label for="location">Location
                                  <span class="info_btn"><span class="field_info xsmall"><?php echo lang('tooltip_event_location'); ?></span></span>
                                </label>
                                <?php echo form_input($eventLocation); ?>
                <?php echo form_error('eventLocation'); ?>
                            </div>
                            <!--end of row-fluid-->
          
                            <div class="row-fluid-15">
                                <label for="venue"><?php echo lang('event_event_venue'); ?><span class="astrik">*</span></label>
                <?php echo form_input($eventVenue); ?>
                <?php echo form_error('eventVenue'); ?>
                               <a href="javascript:void(0)" class="open_venue_search venue_anchor"><?php echo lang('event_event_don_venue_search');?></a>
                            </div>
                            <!--end of row-fluid-->
          
                            <div class="btn_wrapper ">
                              <a href="#collapseOne" class="pull-left scroll_top"><?php echo lang('comm_top'); ?></a>
                              <?php 
                  echo form_hidden('eventId', $eventId);
                  echo form_hidden('formActionName', 'generalEventSetup');
                
                  //button show
                  $formButton['showbutton']   = array('save','reset');
                  $formButton['labletext']  = array('save'=>lang('comm_save_changes'),'reset'=>lang('comm_reset'));
                  $formButton['buttonclass']  = array('save'=>'default_btn btn pull-right medium','reset'=>'default_btn btn pull-right medium reset_form');
                  $this->load->view('common_save_reset_button',$formButton);
                ?>
        
                               
                            </div>
                        </div>
                       <?php echo form_close(); ?>
                    </div>
   </div>
   <!--end of panel-->  




<script type="text/javascript">
  
$("#eventSponsorsLogos").click(function(){
  if(this.checked){
    $(".sponsors_log_div").slideToggle('slow');
  }else{
    $(".sponsors_log_div").slideToggle('slow');
  } 
});
  
//call for event data save and media upload
ajaxdatasave('formGeneralEventSetup','<?php echo $this->uri->uri_string(); ?>',false,true,false,false,true,'#showhideformdivgeneralSection','#showhideformdivpersonalContact');

//show and hide exhibition
$(".exhibitionSelection").click(function(){
  var getValue = parseInt($(this).val());
  if(getValue==1){
    $("#exhibition_field_div").slideDown('slow');
    $("#exhibitionNumber").attr('required',''); 
  }else{
    $("#exhibition_field_div").slideUp('slow');
    $("#exhibitionNumber").removeAttr('required'); 
  }
});

// campare two date event end date greater than start date
//compare_greaterthan_date('eventStartDate','eventEndDate','event end date');

$("#eventEndDate").change(function(){
    var startdateval= $("#eventStartDate").val();
    var enddateval= $("#eventEndDate").val();
    var sDate = Date.parse(startdateval);
    var eDate2 = Date.parse(enddateval);
      if (sDate > eDate2) {
        $("#end_date_error").removeClass('dn');
        $("#eventEndDate").val(''); 
      }else{
        $("#end_date_error").addClass('dn');
      }
    return true;  
});

//earlybirdCutOffDate greater than event start date
//compare_lessthan_date('eventStartDate','earlybirdCutOffDate','event end date');


//registerLastDate (Registration Cut Off date) greater than  or equal to start date
$("#registerLastDate").change(function(){
    var enddateval= $("#eventEndDate").val(); // end date
    var lastdateval= $("#registerLastDate").val(); //Registration Cut Off date
    var eDate = Date.parse(enddateval);
    var lDate = Date.parse(lastdateval); //Registration Cut Off date
      if (eDate < lDate) {
        $("#last_regis_date_error").removeClass('dn');
        $("#registerLastDate").val(''); 
      }else{
        $("#last_regis_date_error").addClass('dn');
      }
    return true;  
}); 

</script>
