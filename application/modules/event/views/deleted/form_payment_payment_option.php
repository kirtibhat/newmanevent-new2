<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//define current and next form div id
$FormDivId = 'FirstForm';
$nextFormDivId = 'SecondForm';

$formpaymentOptionSetup = array(
    'name'   => 'formpaymentOptionSetup',
    'id'   => 'formpaymentOptionSetup',
    'method' => 'post',
    'class'  => 'form-horizontal',
    'data-parsley-validate' => '',
  ); 
  
$selectedPaymentOption = array(
    'name'   => 'selectedPaymentOption',
    'id'   => 'selectedPaymentOption',
    'value' => '',
    'type'  => 'hidden',
    
  );
?>
<div class="panel event_panel">
  <div class="panel-heading ">
      <h4 class="panel-title medium dt-large heading_btn" id="<?php echo $FormDivId; ?>">
        <a class="" data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><?php echo lang('event_payment_payment_option'); ?></a>      
      </h4>
  </div>

  <div style="height: auto;" id="collapseOne" class="accordion_content collapse apply_content">
    <?php  echo form_open($this->uri->uri_string(),$formpaymentOptionSetup); ?>
    <div class="panel-body ls_back dashboard_panel small">
      <div id="accordion_sub" class="panel-group sub-panel-group">



<?php
//prepare data for shwoing payment option
$paymentArray = '';
if(!empty($paymentoptionsdata)) { 
    
  foreach ($paymentoptionsdata as $paymentoptions){
    //here will show only created payment mode
    if($paymentoptions->payment_mode_master_id > 0){
      $paymentArray[$paymentoptions->payment_mode_master_id] = $paymentoptions;
    }
  }
}
 
if(!empty($paymentoptionsmaster)) {
  $fieldRow=1;
  foreach ($paymentoptionsmaster as $optionsmaster){
    $paymentModeMasterId = $optionsmaster->id;
    $paymentModeName = $optionsmaster->payment_mode;
    
    //set default value in variable
    $paymentModeId = '0';
    $detailsAppearOnStatementValue  = '';
    $paymentInstructionValue        = '';
    $typeOfRegistrant               = '';

    if(!empty($paymentArray)){
      
      if(isset($paymentArray[$paymentModeMasterId])){
        $getpaymentoptionsdata = $paymentArray[$paymentModeMasterId];
        //set value in variable
        $paymentModeId            = $getpaymentoptionsdata->id;
        $paymentModeName          = $getpaymentoptionsdata->payment_mode;
        $paymentModeMasterId      = $getpaymentoptionsdata->payment_mode_master_id;
        $paymentInstructionValue  = (!empty($getpaymentoptionsdata->payment_instruction))?$getpaymentoptionsdata->payment_instruction:'';
        $typeOfRegistrant         = (!empty($getpaymentoptionsdata->registrant_id))?json_decode($getpaymentoptionsdata->registrant_id):'';
      }
    }
    
    $detailsAppearOnStatement = array(
        'name'  => 'detailsAppearOnStatement'.$fieldRow,
        'value' =>  $detailsAppearOnStatementValue,
        'id'  => 'detailsAppearOnStatement'.$fieldRow ,
        'type' => 'text',
        'class' => 'width50per',
      );

    $paymentInstruction = array(
        'name'  => 'paymentInstruction'.$fieldRow,
        'value' =>  $paymentInstructionValue,
        'id'    => 'paymentInstruction'.$fieldRow,
        'type' => 'text',
        'rows' => '3',
        'class' => 'small'
      );
      
    $paymentModeIdField = array(
        'name'  => 'paymentModeIdField[]',
        'value' =>  $paymentModeId,
        'type'  => 'hidden',
      );          
              
    echo form_input($paymentModeIdField); 
    
    $paymentModeIdHidden = array(
        'name'  => 'paymentModeIdHidden'.$fieldRow,
        'value' =>  $paymentModeMasterId,
        'id'    => 'paymentModeIdHidden'.$fieldRow,
        'required'  => '',
        'type'    => 'hidden',
      );          
              
    echo form_input($paymentModeIdHidden);
    
    $paymentModeNameHidden = array(
        'name'  => 'paymentModeNameHidden'.$fieldRow,
        'value' =>  $paymentModeName,
        'id'    => 'paymentModeNameHidden'.$fieldRow,
        'type'    => 'hidden',
      );          
              
    echo form_input($paymentModeNameHidden);
?>
<div class="panel event_panel sub_panel">  
  <div class="panel-heading ">
      <h4 class="panel-title medium dt-large">
          <a class="click_payment_mode" data-toggle="collapse" data-parent="#accordion_sub" href="#collapseSub<?php  echo $fieldRow; ?>"  id="<?php echo $fieldRow; ?>">
            <?php echo $paymentModeName; ?>
          </a>
      </h4>
  </div>
  <!--end of panel heading-->

  <div id="collapseSub<?php  echo $fieldRow; ?>" class="accordion_content collapse apply_content ">
    <div class="row-fluid-15">
      <label for="info_org" class=""><?php echo lang('event_payment_allocate_payment_option'); ?> 
        <span class="info_btn">
            <span class="field_info xsmall"><?php echo lang('event_payment_allocate_payment_option'); ?></span>
          </span>
      </label>
      <div class="session_subcat l_margin">
        <?php
        if (!empty($catFilterList)) {
          foreach ($catFilterList as $key => $list) {
            foreach ($list as $cat => $registrant) {
              $registrantCat = array(
                  'name' => 'registrant_cat_' . $key,
                  //'value'     => '',
                  'id' => 'registrant_cat_' . $key.'_'.$fieldRow,
                  'type' => 'checkbox',
                  'category_id' => $key,
                  'class' => 'registrantCat',
                  'data-append-id' => $key.'_'.$fieldRow,
                  'data-category-id' => $key,
              );

              $catChecked = '';
              $catCheckedVal = 0;
              foreach ($registrant as $registrantId => $value) {                            
                if(in_array($registrantId,(array)$typeOfRegistrant)){
                  $catCheckedVal = $catCheckedVal+1;
                  $catChecked = 'checked="checked"';
                }  
              }

              ?>
              <div class="row-fluid-15">
                <span class="checkbox_wrapper">
                  <?php echo form_input($registrantCat, 1, $catChecked); ?>
                  <label for="registrant_cat_<?php echo $key.'_'.$fieldRow; ?>"><?php echo $cat; ?></label>
                </span>
              </div>
              <div id="category_list_<?php echo $key.'_'.$fieldRow; ?>" data-category="<?php echo $key; ?>" class="<?php echo (($catCheckedVal > 0) ? '' : 'dn') ?>">
                <?php
                foreach ($registrant as $registrantId => $value) {      
                  $registrantChecked = '';
                  $registrantChecked = (in_array($registrantId,(array)$typeOfRegistrant)) ? 'checked="checked"' : '';
                  $rowKey = $fieldRow.'_'.$registrantId;
                  $registrantCheckbox = array(
                      'name' => 'regisSlection' .$fieldRow.'[]',
                      'value' => $registrantId,
                      'id' => 'regisSlection'.$rowKey,
                      'type' => 'checkbox',
                      'registrantId' => $registrantId,
                      'class' => 'registrantlisttype',
                      'data-append-id' => $registrantId,
                      'data-category-id' => $registrantId,
                  );
                  ?>
                  <div class="row-fluid-15 sub_check"><span class="checkbox_wrapper">
                    <?php echo form_input($registrantCheckbox, 1, $registrantChecked); ?>
                    <label for="regisSlection<?php echo $rowKey; ?>"><?php echo $value; ?></label>                      
                  </div>
              <?php  } //end loop ?>
              </div>
            <?php
            } // end loop
          } //end loop
        } // end if 
        ?>
        
      </div>
      <!--end of session sub category-->
      </div>

      <div class="row-fluid-15">
        <label for="info_org"><?php echo lang('event_payment_payment_instruction'); ?>
            <span class="info_btn">
                <span class="field_info xsmall"><?php echo lang('tooltip_payment_payment_instruction'); ?></span>
            </span>
        </label>
          <?php echo form_textarea($paymentInstruction); ?>
          <?php echo form_error('paymentInstruction'.$paymentModeId); ?>
      </div>
    </div>  
    <!--end of panel content-->
    
    </div>
    <?php  $fieldRow++; } }  ?>
          
    <div class="row-fluid-15">
      <a href="javascript:void(0)" class="add_edit_payment_option add_btn medium" actionform="add" paymentModeId="0"><div class="addbtn_form"></div> <div class="addtype "><?php echo lang('event_payment_add_payment_option'); ?></div></a>
      <div class="clearfix"></div>
    </div>

    <?php 
    if(!empty($paymentoptionsdata)) {
      foreach ($paymentoptionsdata as $paymentoptions)
      {
        //here will show only created payment mode
        if($paymentoptions->payment_mode_master_id==0){ ?>
        
        <div class="panel event_panel sub_panel">  
        
          <?php
          $paymentModeId = $paymentoptions->id;
          $paymentModeName = $paymentoptions->payment_mode;
          $detailsAppearOnStatementValue  = (!empty($paymentoptions->details_appear_on_statement))?$paymentoptions->details_appear_on_statement:'';
          $paymentInstructionValue    = (!empty($paymentoptions->payment_instruction))?$paymentoptions->payment_instruction:'';
          $typeOfRegistrant       = (!empty($paymentoptions->registrant_id))?json_decode($paymentoptions->registrant_id):'';
          
          $detailsAppearOnStatement = array(
              'name'  => 'detailsAppearOnStatement'.$fieldRow,
              'value' =>  $detailsAppearOnStatementValue,
              'id'  => 'detailsAppearOnStatement'.$fieldRow ,
              'type' => 'text',
              'class' => 'width50per',
            );

          $paymentInstruction = array(
              'name'  => 'paymentInstruction'.$fieldRow,
              'value' =>  $paymentInstructionValue,
              'id'    => 'paymentInstruction'.$fieldRow,
              'type'  => 'text',
              'class' => 'width50per',
              'rows'  => '3',
            );
            
          $paymentModeIdField = array(
            'name'  => 'paymentModeIdField[]',
            'value' =>  $paymentModeId,
            'type'  => 'hidden',
          );          
                  
          echo form_input($paymentModeIdField); 
          
          $paymentModeIdHidden = array(
            'name'  => 'paymentModeIdHidden'.$fieldRow,
            'value' =>  '0',
            'id'    => 'paymentModeIdHidden'.$fieldRow,
            'type'    => 'hidden',
          );          
                  
          echo form_input($paymentModeIdHidden);
          
          $paymentModeNameHidden = array(
              'name'  => 'paymentModeNameHidden'.$fieldRow,
              'value' =>  $paymentModeName,
              'id'    => 'paymentModeNameHidden'.$fieldRow,
              'type'    => 'hidden',
            );          
                    
          echo form_input($paymentModeNameHidden);
        ?>

        <div class="panel-heading "  id="paymentMode_<?php echo $paymentModeId; ?>">
            <h4 class="panel-title medium dt-large heading_btn">
              <a class="click_payment_mode" data-toggle="collapse" data-parent="#accordion_sub" href="#collapseSub<?php echo $fieldRow; ?>"  id="<?php echo $fieldRow; ?>"><?php echo $paymentModeName; ?></a>
              <span class="heading_btn_wrapper">
                <a href="javascript:void(0)" class="delete_btn td_btn delete_payment_option" deleteid="<?php echo $paymentModeId; ?>"><span>&nbsp;</span></a>
                <a href="javascript:void(0)" class="eventsetup_btn td_btn add_edit_payment_option" actionform="edit" paymentModeId="<?php echo $paymentModeId; ?>"><span>&nbsp;</span></a>        
              </span>
            </h4>
        </div>
        <!--end of panel heading-->
      

          
        <div id="collapseSub<?php  echo $fieldRow; ?>" class="accordion_content collapse apply_content ">
          <div class="row-fluid-15">
            <label for="info_org" class=""><?php echo lang('event_payment_allocate_payment_option'); ?> 
              <span class="info_btn">
                  <span class="field_info xsmall"><?php echo lang('event_payment_allocate_payment_option'); ?></span>
                </span>
            </label>
            <div class="session_subcat l_margin">

              <?php
              if (!empty($catFilterList)) {
                foreach ($catFilterList as $key => $list) {
                  foreach ($list as $cat => $registrant) {
                    $registrantCat = array(
                        'name' => 'registrant_cat_' . $key,
                        //'value'     => '',
                        'id' => 'registrant_cat_' . $key.'_'.$fieldRow,
                        'type' => 'checkbox',
                        'category_id' => $key,
                        'class' => 'registrantCat',
                        'data-append-id' => $key.'_'.$fieldRow,
                        'data-category-id' => $key,
                    );

                    $catChecked = '';
                    $catCheckedVal = 0;
                    foreach ($registrant as $registrantId => $value) {                            
                      if(in_array($registrantId,(array)$typeOfRegistrant)){
                        $catCheckedVal = $catCheckedVal+1;
                        $catChecked = 'checked="checked"';
                      }  
                    }

                    ?>
                    <div class="row-fluid-15">
                      <span class="checkbox_wrapper">
                        <?php echo form_input($registrantCat, 1, $catChecked); ?>
                        <label for="registrant_cat_<?php echo $key.'_'.$fieldRow; ?>"><?php echo $cat; ?></label>
                      </span>
                    </div>
                    <div id="category_list_<?php echo $key.'_'.$fieldRow; ?>" data-category="<?php echo $key; ?>" class="<?php echo (($catCheckedVal > 0) ? '' : 'dn') ?>">
                      <?php
                      foreach ($registrant as $registrantId => $value) {      
                        $registrantChecked = '';
                        $registrantChecked = (in_array($registrantId,(array)$typeOfRegistrant)) ? 'checked="checked"' : '';
                        $rowKey = $fieldRow.'_'.$registrantId;
                        $registrantCheckbox = array(
                            'name' => 'regisSlection' .$fieldRow.'[]',
                            'value' => $registrantId,
                            'id' => 'regisSlection'.$rowKey,
                            'type' => 'checkbox',
                            'registrantId' => $registrantId,
                            'class' => 'registrantlisttype',
                            'data-append-id' => $registrantId,
                            'data-category-id' => $registrantId,
                        );
                        ?>
                        <div class="row-fluid-15 sub_check"><span class="checkbox_wrapper">
                          <?php echo form_input($registrantCheckbox, 1, $registrantChecked); ?>
                          <label for="regisSlection<?php echo $rowKey; ?>"><?php echo $value; ?></label>                      
                        </div>
                    <?php  } //end loop ?>
                    </div>
                  <?php
                  } // end loop
                } //end loop
              } // end if 
              ?>
            </div>
            <!--end of session sub category-->
          </div>

          <div class="row-fluid-15">
            <label for="info_org"><?php echo lang('event_payment_payment_instruction'); ?>
                <span class="info_btn">
                    <span class="field_info xsmall"><?php echo lang('tooltip_payment_payment_instruction'); ?></span>
                </span>
            </label>
              <?php echo form_textarea($paymentInstruction); ?>
              <?php echo form_error('paymentInstruction'.$paymentModeId); ?>
          </div>             
          
      </div>
    
        <div class="spacer15"> </div>
    </div>  
    <?php $fieldRow++; }  } }  ?>


          
          
        </div>
        <!--end of panel--> 
        <div class="btn_wrapper ">
          <a href="#collapseOne" class="pull-left scroll_top">Top</a>
          <?php
          echo form_input($selectedPaymentOption);
          echo form_hidden('eventId', $eventId);
          echo form_hidden('formActionName', 'paymentOptionSetup');
          
          //button show of save and reset
          $formButton['saveButtonId'] = 'id="' . $eventId . '"'; // click button id
          $formButton['showbutton'] = array('save', 'reset');
          $formButton['labletext'] = array('save' => lang('comm_save_changes'), 'reset' => lang('comm_reset'));
          $formButton['buttonclass'] = array('save' => 'default_btn btn pull-right medium', 'reset' => 'default_btn btn pull-right medium reset_form');
          $this->load->view('common_save_reset_button', $formButton);
          ?>
        </div>      
      </div>
    <?php echo form_close(); ?>
  </div>
</div>
<!--end of panel-->

<script type="text/javascript" language="javascript">
$(document).on('click','.registrantCat',function(){
    // For set collapse hight
        $(".accordion_content").css("height",'auto');
        
        var divId = $(this).attr('data-append-id');
        if($(this).prop("checked")){
            $("#category_list_"+divId).show();
        }else{
            $("#category_list_"+divId).hide();
        }
});

/*-----payment payment option settings save -------*/
ajaxdatasave('formpaymentOptionSetup','<?php echo $this->uri->uri_string(); ?>',true,true,false,false,false,'#showhideformdiv<?php echo $FormDivId; ?>','#showhideformdiv<?php echo $nextFormDivId; ?>');   

//save payment option add data
ajaxdatasave('formpaymentOptionAdd','event/addeditpaymentoptionsave',false,true,true,false,false,false,false,false);
  
//create empty array  
var paymentOption = [];

$(".click_payment_mode").click(function(){
  var getid=$(this).attr('id');
  var getStatus=$("#collapseSub"+getid).is(':visible');
  if(getStatus){
    var getIndex = paymentOption.indexOf(getid);
    if (getIndex > -1) {
      paymentOption.splice(getIndex, 1);
    }
  }else{
    //add element from array
    if(paymentOption.indexOf(getid) < 0){
      paymentOption.push(getid);
    }
  }
  
  //set selected option value
  $("#selectedPaymentOption").val(paymentOption); 
});

//mouse hover check div opened and change id position
$(".click_payment_mode").mouseenter(function(){
  var getid=$(this).attr('id');
  var getStatus=$("#collapseSub"+getid).is(':visible');
}); 

//mouse hover check div opened and change id position
$(".click_payment_mode").mouseout(function(){
  var getid=$(this).attr('id');
  var getStatus=$("#collapseSub"+getid).is(':visible');
});
</script>
