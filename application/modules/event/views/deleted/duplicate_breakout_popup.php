<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//--------add type fo registration fields---------//	

$breakoutNameValue 		= (!empty($breakoutdata->breakout_name))?$breakoutdata->breakout_name:''; 
$headerAction			= 		lang('event_breakout_duplicate_title'); 

$formTypeofBreakoutDuplicate = array(
		'name'	 => 'formTypeofBreakoutDuplicate',
		'id'	 => 'formTypeofBreakoutDuplicate',
		'method' => 'post',
		'class'  => 'form-horizontal mt10',
	);
	
$breakoutName = array(
		'name'	=> 'breakoutName',
		'value'	=> $breakoutNameValue,
		'id'	=> 'breakoutName',
		'type'	=> 'text',
	//	'placeholder'	=> 'Breakout Name',
		'required'	=> '',
	);
		
$breakoutId = array(
		'name'	=> 'breakoutId',
		'value'	=> $breakoutIdValue,
		'id'	=> 'breakoutId',
		'type'	=> 'hidden',
	);


?>

<!--------Add braakout div start-------->
	<div class="modal fade alertmodelbg dn" id="duplicate_brakout_popup_div" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		
		<div class="modal-content">
			
			<div class="modal-header bg_5a4099">
				<h4 class="modal-title"><?php echo $headerAction; ?></h4>
			</div>
			<?php  echo form_open(base_url('event/duplicatebreakoutsave'),$formTypeofBreakoutDuplicate); ?>
				<div class="modal-body min_height_47">
					<!---<h4><?php //echo lang('event_breakout_popup_add_msg'); ?></h4>--->
					<div class="spacer10">	</div>
					<div class="control-group mb10 ">
						<label for="inputEmail" class="control-label"><?php echo lang('event_breakout_popup_add_field'); ?>
							<font class="mandatory_cls">*</font>
						</label>
						<div class="controls">
							<?php echo form_input($breakoutName); ?>
						</div>
					</div>
			</div>
				<div class="spacer15">	</div>
			<div class="modal-footer">
				<?php 
					echo form_hidden('eventId', $eventId);
					echo form_input($breakoutId);
					$extraCancel 	= 'class="btn btn-default custombtn" data-dismiss="modal" ';
					$extraSave 	= 'class="btn btn-primary bg_46166b custombtn bg_f26531" ';
					echo form_button('cancel',lang('comm_cancle'),$extraCancel);
					echo form_submit('save',lang('comm_submit'),$extraSave);
				?>
			</div>
		</div>
		<?php echo form_close(); ?>
	</div>
</div>
<!--------Add braakout div end-------->
