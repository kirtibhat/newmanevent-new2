<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//--------add type of registration fields---------//  
$formTypeofRegistration = array(
    'name'   => 'formTypeofRegistration',
    'id'   => 'formTypeofRegistration',
    'method' => 'post',
    'class'  => 'wpcf7-form',
    'data-parsley-validate' => '',
  );
  
$registrationName = array(
    'name'  => 'registrationName',
    'value' => '',
    'id'  => 'registrationTypeName',
    'type'  => 'text',
    'class' => 'small',
    //'placeholder' => 'Name',
    'required'  => '',
    'autocomplete' => 'off',
    'data-parsley-error-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
);

$registrationTypeId = array(
    'name'  => 'registrationTypeId',
    'value' => '0',
    'id'  => 'registrationTypeId',
    'type'  => 'hidden',
  );  

//$registrantType   = array('0'=>'Main Registrant','1'=>'Side Event','2'=>'Sponsor','3'=>'Exhibitor');
// get Registrant category
//get master field for each event id

$registrantType[""] = "Select Category";
$whereCategoryField = array('is_corporate'=>'0');
$registrantCategorydata= getDataFromTabelWhereIn('event_categories','*','event_id',array($eventId),$whereCategoryField);
if(!empty($registrantCategorydata)){
  foreach($registrantCategorydata as $value){
    $registrantType[$value['category_id']] = $value['category'];
  }
}   

?>

<!-- Modal -->
<div id="add_type_login_popup" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" >
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header delegates">
        <h4 class="medium dt-large modal-title"><span id="registrantTypeTitle"><?php echo lang('event_regis_add'); ?></span><?php echo " ".lang('event_regis_addtype'); ?></h4>
      </div>

      <div class="modal-body small">
        <div class="modelinner ">
        <?php  echo form_open(base_url('event/addediteventregistration'),$formTypeofRegistration); ?>
            <div class="control-group mb10 eventdashboard_popup small">

              <div class="row-fluid">
                <label class="pull-left" for="ctype"><?php echo lang('event_regis_name'); ?>   <span class="astrik">*</span>
          <span class="info_btn"><span class="field_info xsmall"><?php echo lang('event_regis_contact_type_info'); ?></span></span>
                </label>
                <?php echo form_input($registrationName); ?>
              </div>

              <div class="row-fluid">
                <label class="pull-left" for="ctype_details"><?php echo lang('event_regis_category_type'); ?>  <span class="astrik">*</span>
                <span class="info_btn"><span class="field_info xsmall"><?php echo lang('tooltip_regis_button_name'); ?></span></span>
                </label>
                <?php 
          $other = ' id="registrantType" required ="" class="small custom-select full_select" data-parsley-error-message="'.lang('common_field_required').'" data-parsley-error-class="custom_li" ';
          echo form_dropdown('registrantType',$registrantType,'',$other);
          echo form_error('registrantType');
        ?>
              </div>

              <div class="btn_wrapper">
                  
                  <?php 
                    echo form_input($registrationTypeId); 
                    echo form_hidden('eventId', $eventId);
                    $extraCancel  = 'class="popup_cancel submitbtn pull-right medium" data-dismiss="modal" ';
                    $extraSave    = 'class="submitbtn pull-right medium" ';
                    
                    echo form_submit('save',lang('comm_save'),$extraSave);
                    echo form_submit('cancel',lang('comm_cancle'),$extraCancel);
                  ?>
                 
              </div>

            </div>
          <?php echo form_close(); ?>
        </div>
      </div>
    </div>
  </div>
</div>



