<?php 
//--------------personal form details---------------//

$formApplicationDetails = array(
		'name'	 => 'formApplicationDetails',
		'id'	 => 'formApplicationDetails',
		'method' => 'post',
		'class'  => '',
);

//get master field values array	
$fieldsmastervalue = fieldsmastervalue();

$emailfield = array(
				//	'name'	=> 'emailfield',
				'value'			=> '',
				'id'			=> 'emailfield',
				'type'			=> 'text',
				'placeholder'	=> 'Displayed & Mandatory',
				'readonly' 		=> '',
				'class'			=> 'small short_field',
			);
?>

<div class="panel event_panel">
	<div class="panel-heading ">
		<h4 class="panel-title medium dt-large ">
		<a class=" " data-toggle="collapse" data-parent="#accordion" href="#collapseFour">Corporate Application Details</a>
		</h4>
	</div>
	<div style="height: auto;" id="collapseFour" class="accordion_content collapse apply_content <?php echo getIsCollapseOpen("collapseFour"); ?>">
		<?php echo form_open($this->uri->uri_string(),$formApplicationDetails); ?>
		
		<div class="panel-body ls_back dashboard_panel small">
			
			<?php 
				
				if(!empty($masterformfielddata)){
						foreach($masterformfielddata as $formfield){
							
						$fieldCreateBy = $formfield->field_create_by;
						
						if($fieldCreateBy=="1"){ // 1 belongs to admin		
						?>
									<?php
										echo form_hidden('fieldid[]', $formfield->id);
										if($formfield->field_name=="email"){ 
										?>
										
										<div class="row-fluid-15">
											<label for="co_email">Email <span class="astrik">*</span>
												<span class="info_btn"><span class="field_info xsmall">Email Address Info</span></span>
											</label>
											<?php 
														$fieldName= 'field_'.$formfield->id;	
														echo form_input($emailfield);
														$fieldName= 'field_'.$formfield->id;	
														echo form_hidden($fieldName, $formfield->field_status);
										     ?>
											
										</div>
									
									<?php }else {  ?>
									
											<div class="row-fluid-15">
													<label for="co_org"><?php echo ucwords($formfield->field_name); ?> <span class="astrik">*</span>
													</label>
													
													<?php 
														$other 	  = 'id="field_'.$formfield->id.'" class="small custom-select short_field" ';
														$fieldName= 'field_'.$formfield->id;	
														echo form_dropdown($fieldName, $fieldsmastervalue, $formfield->field_status,$other);	 
													?>
													<?php  if($formfield->is_editable=="1"){ ?>		
														<span class="pull-right select_edit editapplicationcustomformfield"  customfieldid="<?php echo encode($formfield->id); ?>"><a href="javascript:void(0)" class="eventsetup_btn td_btn"><span>&nbsp;</span></a></span>
													 <?php } ?>	
											</div>
											<!--end of row-fluid-->

										
									<?php } ?>
									
							
						<?php } // end if
							} // end loop 
						} // end if ?>
			
			<div class="row-fluid-15">
				<a class="add_btn medium" href="javascript:void(0)" id="add_field_custom">Add Custom Field</a>
			</div>
			<!--end of row-fluid-->
			
			<?php  if(!empty($masterformfielddata)){
				foreach($masterformfielddata as $key=>$formfield){
				
				$fieldCreateBy = (isset($formfield->field_create_by) && $formfield->field_create_by=="0")?$formfield->field_create_by:NULL;
				if($fieldCreateBy=="0"){		
				?>
				
					<div class="row-fluid-15" id="deleteappcustomfielddiv_<?php echo $key; ?>">
						<label for="co_owned"><?php echo ucwords($formfield->field_name); ?></label>

						<?php
									echo form_hidden('fieldid[]', $formfield->id);
									$other = 'id="field_'.$formfield->id.'" class="small custom-select short_field"';
									$fieldName= 'field_'.$formfield->id;
									echo form_dropdown($fieldName, $fieldsmastervalue, $formfield->field_status,$other);	
						?>
						<span class="pull-right select_edit">
							<a href="javascript:void(0)" class="eventsetup_btn td_btn editapplicationcustomformfield" customfieldid="<?php echo encode($formfield->id); ?>"><span>&nbsp;</span></a>
							<a href="javascript:void(0)" class="delete_btn td_btn deleteapplicationcustomformfield"  deleteid="<?php echo $formfield->id; ?>"><span>&nbsp;</span></a>
						</span>
					</div>
					<!--end of row-fluid-->
				
				<?php } } } ?>
			 <?php echo form_hidden('eventId',$eventId,'id="eventId"'); ?>
			 <?php echo form_hidden('formActionName','corporateApplicationDetails'); ?>
			<div class="btn_wrapper ">
				<a href="#collapseFour" class="pull-left scroll_top">Top</a>
				
				<?php 
					//button show of save and reset
					$formButton['showbutton']   = array('save');
					$formButton['labletext'] 	= array('save'=>lang('comm_save_changes'));
					$formButton['buttonclass']  = array('save'=>'default_btn btn pull-right medium');
					$this->load->view('event/common_save_reset_button',$formButton);
				
				?>
				
				<!--<a href="#" class="default_btn btn pull-right medium">Save</a>-->
				<a href="#" class="default_btn btn pull-right medium">Clear</a>
			</div>
		</div>
		</form>
	</div>
</div>
<!--end of panel-->

<script type="text/javascript">
	//personal details save
		 ajaxdatasave('formApplicationDetails','<?php echo $this->uri->uri_string(); ?>',true,true,false,true,false,'#showhideformdiv','#showhideformdivdieteryrequirement','',true,'add_field_popup');
		//personal details save
		ajaxdatasave('formAddCustomeField','<?php echo $this->uri->uri_string(); ?>',true,true,false,true,false,'#showhideformdiv','#showhideformdivdieteryrequirement');
</script>

<script>
	
	// call delete function to delete the particular Custom Field
	customconfirm('deleteapplicationcustomformfield','event/deleteeditcustomefield','','','',true);
	
	/*
	$(document).on("click", ".error_close_button", function(e){
		// check model popup
		$("#add_field_popup").modal({ keyboard: false}); // show the model
	});
	*/	
		
	$(document).on("click", "#add_field_custom", function(e){
		formPostData = {popupType:'addCustomFieldPopup',eventId:$("#eventId").val()};
		//eventcustompopup('event/corporatepopup','add_field_popup',formPostData,'custom_field_qty');
		//call ajax popup function
		ajaxpopupopen('add_field_popup','event/corporatepopup',formPostData,'add_field_custom','custom_field_qty');
	});
	
	 $(document).on("click", ".editapplicationcustomformfield", function(e){
		var customfieldid = $(this).attr('customfieldid');
		formPostData = {popupType:'addCustomFieldPopup',eventId:$("#eventId").val(),customfieldid:customfieldid};
		ajaxpopupopen('add_field_popup','event/corporatepopup',formPostData,'editapplicationcustomformfield','custom_field_qty');
		//eventcustompopup('event/corporatepopup','add_field_popup',formPostData,'custom_field_qty');
	});
	
	
   /*
	*  This section is used select option fields
   */
		
	$(document).on('click','.stepper-step',function(){
		$(".default_value_list").html('');
		var getval = parseInt($("#custom_field_qty").val());	
		var addField = '';
		for(i=1;i<=getval;i++){
		    addField+='<label class="pull-left" for="field_1">Field '+i+'</label>';
            addField+='<input type="text" value="" id="defaultValue_'+i+'" name="defaultValue[]" class="small" required="" data-parsley-error-class = "custom_li"  data-parsley-trigger="keyup" >';
			
			$(".default_value_list").append(addField);
			addField = '';
		}
	});
	
	/*
	 *  This section is used to select custome field type select
	*/
		
	$(document).on('click','.fieldtype',function(){
			var getval = $(this).val();	
			var customFieldId = parseInt($("#customFieldId").val());
			//if use add field then remove old data
			if(customFieldId==0){
				$("#selectedField").val('1');
				$(".default_value_list").html('<label class="pull-left" for="field_1">Field 1</label><input type="text" value="" id="defaultValue_1" name="defaultValue[]" class="small" required="" data-parsley-error-class = "custom_li"  data-parsley-trigger="keyup" >');
			}
			if(getval=="text" || getval=="file"){
				$("#show_dropdown_feature").hide();
				
			}else{
				$("#show_dropdown_feature").show();
			}
		});
	

</script>	
             
