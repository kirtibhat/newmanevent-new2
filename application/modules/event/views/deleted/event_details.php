<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>
<?php $this->load->view('event_menus');  ?>

<div class="row-fluid-15">
        <div id="page_content" class="span9">
      
      <div id="accordion" class="panel-group">
        
        <?php 
          // load general_event_setup view
          $this->load->view('form_general_event_setup');
        ?>
        
        <!-- /commonform_bg -->
        <?php 
          // load form_contact view
          $this->load->view('form_contact_person');
        ?>
        <!-- /commonform_bg -->
        
        <?php 
          // load form_event_bank_details view
          $this->load->view('form_lost_password_contact');
        ?>

        <?php 
          // load form_contact view
          $this->load->view('form_event_invoice_details');
        ?>
        
        
        <?php 
          // load form_event_bank_details view
          $this->load->view('form_event_category');
        ?>
        
        <?php 
          // load form_event_bank_details view
          $this->load->view('form_event_group_booking');
        ?>
        
        <?php 
          // load form_event_bank_details view
          //$this->load->view('form_event_email');
        ?>
        
        <?php 
          // load form_event_bank_details view
          $this->load->view('form_event_terms_conditions');
        ?>
        
        
      </div>
      
      <?php 
        //next and previous button
        $buttonData['viewbutton'] = array('back','next','preview');
        $buttonData['linkurl']  = array('back'=>'','next'=>base_url('event/corporatedetails'),'preview'=>'');
        $this->load->view('common_back_next_buttons',$buttonData);
      ?>
            
            <!--end of page btn wrapper-->  
    </div>
        <!--end of page content-->
</div>
<!--end of row-->


<div id="opentModelBox"></div>

<!--start custom tooltip-->
<div id="tooltip_wrapper"></div>
<!--end custom tooltip-->
