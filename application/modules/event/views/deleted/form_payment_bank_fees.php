<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

//define current and next form div id
$FormDivId = 'ThirdForm';
$nextFormDivId = 'FourthForm';

$bankfeedetails           = (!empty($bankfeedetails))?$bankfeedetails[0]:'';
$paymentBankId            = (!empty($bankfeedetails->id))?$bankfeedetails->id:'0';
$whoWillPayFeeValue       = (isset($bankfeedetails->fee_to_be_paid_by))?$bankfeedetails->fee_to_be_paid_by:'';
$regisUnitPriceValue      = (isset($bankfeedetails->price_per_registrant))?$bankfeedetails->price_per_registrant:'';
$regisGSTIncludedValue    = (isset($bankfeedetails->gst))?$bankfeedetails->gst:'';
$regisTotalPriceValue     = (isset($bankfeedetails->total_price))?$bankfeedetails->total_price:'';
$registerationFeeValue    = (!empty($bankfeedetails->registeration_fee))?$bankfeedetails->registeration_fee:'';

//default variable for who paid
$whoWillPayFeeRegisValue  = false;
$whoWillPayFeeOrgaiValue  = false;
//set value
if($whoWillPayFeeValue=='0'){
  $whoWillPayFeeRegisValue = true;
  $whoWillPayFeeOrgaiValue = false;
}else{
  $whoWillPayFeeRegisValue = false;
  $whoWillPayFeeOrgaiValue = true;
}

//default variable for registration fee
$registerationFeeYesValue   = false;
$registerationFeeNoValue  = false;
//set value
if($registerationFeeValue=='1'){
  $registerationFeeYesValue = true;
  $registerationFeeNoValue = false;
}else{
  $registerationFeeYesValue = false;
  $registerationFeeNoValue = true;
}

$formBankFeesSetup = array(
    'name'   => 'formBankFeesSetup',
    'id'   => 'formBankFeesSetup',
    'method' => 'post',
    'class'  => 'form-horizontal',
    'data-parsley-validate' => '',
  );

$registrationFeeYes = array(
    'name'  => 'registrationFee',
    'value' => '1',
    'id'  => 'registrationFeeYesBank',
    'type'  => 'radio',
    'required'  => '',
    'checked' => $registerationFeeYesValue,
  );  

$registrationFeeNo = array(
    'name'  => 'registrationFee',
    'value' => '0',
    'id'  => 'registrationFeeNoBank',
    'type'  => 'radio',
    'required'  => '',
    'checked' => $registerationFeeNoValue,
  );
  
$whoWillPayFeeRegis = array(
    'name'  => 'whoWillPayFee',
    'value' => '0',
    'id'  => 'payTheRegistrationFeeYesBank',
    'type'  => 'radio',
    'required'  => '',
    'checked' => $whoWillPayFeeRegisValue,        
  );

$whoWillPayFeeOrgai = array(
    'name'  => 'whoWillPayFee',
    'value' => '1',
    'id'  => 'whoWillPayFeeOrgaiBank',
    'type'  => 'radio',
    'required'  => '',
    'checked' => $whoWillPayFeeOrgaiValue,
  );  

$regisTotalPrice = array(
    'name'  => 'regisTotalPrice',
    'value' =>  $regisTotalPriceValue,
    'id'  => 'regisTranTotalPrice',
    'required'  => '',
    'type' => 'text',
    'class' => 'small short_field regisTranTotalPriceEnter numValue',
    'data-parsley-error-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
  );
  
$regisGSTIncluded = array(
    'name'  => 'regisGSTIncluded',
    'value' =>  $regisGSTIncludedValue,
    'id'    => 'regisTranGSTIncluded',
    'required'  => '',
    'type'    => 'text',
    'readonly'    => 'true',
    'class' => 'small short_field regisTotalPriceEnter numValue',
    'data-parsley-error-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
  );
?>



<div class="panel event_panel">
  <div class="panel-heading ">
      <h4 class="panel-title medium dt-large heading_btn ">
      <a class="" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
      <?php echo lang('event_payment_transaction_fees'); ?>
     </a>
      </h4>
  </div>

  <div style="height: auto;" id="collapseThree" class="accordion_content collapse apply_content">
  <?php echo form_open($this->uri->uri_string(),$formBankFeesSetup); ?>
    <div class="form-horizontal form_open_chk" id="showhideformdiv<?php echo $FormDivId; ?>">    
      <div class="panel-body ls_back dashboard_panel small">


        <div class="row-fluid-15">
          <label for="info_org"><?php echo lang('event_payment_pay_registra_fees'); ?><span class="astrik">*</span>
            <span class="info_btn">
                <span class="field_info xsmall"><?php echo lang('tooltip_payment_pay_registra_fees'); ?>
                  </span>
              </span>
          </label>
           
          <div class="radio_wrapper ">
            <?php echo form_radio($registrationFeeYes); ?> 
            <label for="registrationFeeYesBank"><?php echo lang('event_paymen_fees_yes'); ?> </label>
            <?php echo form_radio($registrationFeeNo); ?>
            <label for="registrationFeeNoBank"><?php echo lang('event_paymen_fess_no'); ?> </label>
          </div>

          <label for="info_org"><?php echo lang('event_payment_pay_transaction_fees'); ?> <span class="astrik">*</span>
            <span class="info_btn">
                <span class="field_info xsmall"><?php echo lang('event_payment_pay_transaction_fees'); ?>
                  </span>
              </span>
          </label>
            <div class="radio_wrapper ">
              <?php echo form_radio($whoWillPayFeeRegis); ?> 
              <label for="payTheRegistrationFeeYesBank"><?php echo lang('event_paymen_paid_transction_by_1'); ?> </label>
              <?php echo form_radio($whoWillPayFeeOrgai); ?>
              <label for="whoWillPayFeeOrgaiBank"><?php echo lang('event_paymen_paid_transction_by_2'); ?> </label>
            </div>
            <!--end of session sub category-->          
        </div>
        
        <div class="row-fluid-15">
            <label for="reg_limit"><?php echo lang('event_payment_register_total_price'); ?> <span class="astrik">*</span>
              <span class="info_btn"></span>
            </label>
            <?php echo form_input($regisTotalPrice); ?>
            <?php echo form_error('regisTotalPrice'); ?>
        </div>
        <div class="row-fluid-15">
            <label for="reg_limit"><?php echo lang('event_payment_register_gst_include'); ?></label>
            <?php echo form_input($regisGSTIncluded); ?>
            <?php echo form_error('regisGSTIncluded'); ?>
        </div>
        <div class="btn_wrapper ">
          <a href="#collapseThree" class="pull-left scroll_top">Top</a>
          <?php 
            echo form_hidden('eventId', $eventId);
            echo form_hidden('formActionName', 'bankFees');
            echo form_hidden('paymentBankId', $paymentBankId);
            
            //button show of save and reset
            $formButton['showbutton']   = array('save','reset');
            $formButton['labletext']  = array('save'=>lang('comm_save_changes'),'reset'=>lang('comm_reset'));
            $formButton['buttonclass']  = array('save' => 'default_btn btn pull-right medium', 'reset' => 'default_btn btn pull-right medium reset_form');
            $this->load->view('common_save_reset_button',$formButton);
          ?>
        </div>
      </div>
    </div>
  <?php echo form_close(); ?>
  </div>
</div>
<!--end of panel-->
<script type="text/javascript">
  //payment bank fees save 
  ajaxdatasave('formBankFeesSetup','<?php echo $this->uri->uri_string(); ?>',true,true,false,false,false,'#showhideformdiv<?php echo $FormDivId; ?>','#showhideformdiv<?php echo $nextFormDivId; ?>');
</script>
