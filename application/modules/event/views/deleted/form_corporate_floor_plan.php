<?php
if(!empty($corporateFloorPlan)){
   
  $corporateFloorPlanId    = $corporateFloorPlan[0]->id;
  $exhibitionSpace     = $corporateFloorPlan[0]->is_exhibition_space;
  $attachFloorPlan       = $corporateFloorPlan[0]->floor_plan;
  $totalSpaceAvailable     = $corporateFloorPlan[0]->total_space_available;
  
}else{
  
  $corporateFloorPlanId    = "";
  $exhibitionSpace     = "1";
  $attachFloorPlan       = "";
  $totalSpaceAvailable   = 0;
}
 
//add exhibitor floor plan form 
$formCorporateFloorPlan = array(
    'name'   => 'formCorporateFloorPlan',
    'id'   => 'formCorporateFloorPlan',
    'method' => 'post',
    'class'  => 'form-horizontal',
    'data-parsley-validate' => '',
);
  
$formcorporateFloorPlanId = array(
    'name'      => 'corporateFloorPlanId',
    'value'     => $corporateFloorPlanId,
    'id'      => 'corporateFloorPlanId',
    'type'      => 'hidden',
);  

$formSpaceAvailable = array(
      'name'    => 'refr_no',
      'value'   => $totalSpaceAvailable,
      'id'    => 'refr_no',
      'type'    => 'number',
      'required'  => '',
      'step'    => '1',
      'class'   => 'small dark numValue',
      'readonly'  => '',
      //'max'   => '5',
      'min'     => '1',
      'required'  => '',
      'data-parsley-type'=>'number',
      'data-parsley-minlength'=>'1',
      'data-parsley-error-message' => lang('common_field_required'),
      'data-parsley-error-class' => 'custom_li',
      'data-parsley-trigger' => 'keyup'
);  


$formexhibitionSpaceYes = array(
      'name'    => 'exhibitionSpace',
      'value'   => '1',
      'id'    => 'space_yes',
      'type'    => 'radio',
      
);


$formexhibitionSpaceNo = array(
      'name'    => 'exhibitionSpace',
      'value'   => '0',
      'id'    => 'space_no',
      'type'    => 'radio',
);

if($exhibitionSpace==1){
  $formexhibitionSpaceYes['checked'] = true;
}elseif($exhibitionSpace==0){
  $formexhibitionSpaceNo['checked'] = true;
}

?> 
 
<div class="panel event_panel">
  <div class="panel-heading ">
    <h4 class="panel-title medium dt-large ">
      <a class=" " data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><?php echo lang('corporate_floor_plan'); ?></a>
    </h4>
  </div>
  <div style="height: auto;" id="collapseOne" class="accordion_content collapse apply_content <?php echo getIsCollapseOpen("collapseOne"); ?>">
    <?php echo form_open($this->uri->uri_string(),$formCorporateFloorPlan); ?>  
     <input type="hidden" value="collapseTwo" name="collapseValue">
    <?php 
      $hd_data = array(
          'type'  => 'hidden',    
          'name'  => 'hd_total_space_available',
          'value' => $totalSpaceAvailable,
          'id'   => 'hd_total_space_available'
      );

      echo form_input($hd_data);
    ?>
    <div class="panel-body ls_back dashboard_panel small" id="showhideformdivcorporatefloorplan">

      <div class="row-fluid-15">
        <label for="amc_con"><?php echo lang('corporate_exhibition_space'); ?> 
          <span class="info_btn"><span class="field_info xsmall"><?php echo lang('tooltip_corporate_exhibition_space'); ?></span></span>
        </label>
        <div class="radio_wrapper radio_right">
          <?php echo form_radio($formexhibitionSpaceYes); ?>
          <label for="space_yes">Yes</label>
          
          <?php echo form_radio($formexhibitionSpaceNo); ?>
          <label for="space_no">No</label>
        </div>
      </div>
      <!--end of row-fluid-->
            
       <div class="row-fluid-15" id="attch_FileContainer">
        <div id="attch_dropArea">
          <label for="attach_doc"><?php echo lang('corporate_attach_floor_plan'); ?>
            <span class="info_btn"><span class="field_info xsmall"><?php echo lang('corporate_attach_floor_plan'); ?></span></span>
          </label>
          <div class="custom-upload" id="attch_pickfiles">
            <span class="label_text">Select or Drop File</span>
            <input type="file" name="attach_doc_floor_plan" id="attach_doc_floor_plan" draggable="true">
          </div>
          <div id="attch_file_list" class="custom-upload-file "></div>
        </div>
      </div>
      
      <div class="row-fluid-15  <?php echo (empty($attachFloorPlan))?'dn':' '; ?>"  id="attach_file_list_main" >
        <div class="" id="attch_file_list" >
          <?php if(!empty($attachFloorPlan)){ 
            $fileId= remove_extension($attachFloorPlan);  
              ?>
               <div class="file_value attchadddedfile uploaded_file" id="<?php echo $fileId; ?>"> 
                  <div class="imagename_cont pr mr_5px"><?php echo $attachFloorPlan; ?>
                    <div class="pa ptr top_6 right_10 delete_file" deleteurl='event/deletecorporatefloorplan'  id="cancel_<?php echo $fileId; ?>" deleteid="<?php echo $corporateFloorPlanId; ?>"  fileid="<?php echo $fileId; ?>">
                      <img src="<?php echo IMAGE; ?>value_clear.png">
                    </div>
                  </div>
                </div> 
                
          <?php } ?>
         </div>   
      </div>  
      
      
      <!--end of row-fluid-->
      <div class="row-fluid-15">
        <label for="refr_no"><?php echo lang('corporate_total_space_available'); ?>
          <span class="info_btn"><span class="field_info xsmall"><?php echo lang('tooltip_corporate_total_space_available'); ?></span></span>
        </label>
        <?php 
          echo form_input($formSpaceAvailable);
          echo form_error('refr_no');
        ?>
      </div>
      <!--end of row-fluid-->

      <div class="row-fluid-15">
        <a id="corporate_add_space_type" href="javascript:void(0)" class="add_btn medium" ><?php echo lang('corporate_add_space_type'); ?></a>
      </div>          
      <!--end of row-fluid-->

      <div id="space_type_list">
          
        <?php // get space type if exist
         if(!empty($spaceTypeRecord)){
         foreach($spaceTypeRecord as $key=>$value){     
         ?> 
            <div class="row-fluid-15 category_wrapper" id="corporateSpaceTypeId_<?php echo $key; ?>">
              <span class="category_label pull-left"><?php echo $value->space_type; ?></span>
              <span class="category_btn pull-right">
                <a class="eventsetup_btn td_btn editcorporatespacetype" data-contact="<?php echo encode($value->floor_plan_type_id); ?>" href="javascript:void(0)"><span>&nbsp;</span></a>
                <a class="delete_btn td_btn deletecorporatespacetype" deleteid="<?php echo encode($value->floor_plan_type_id); ?>" href="javascript:void(0)"><span>&nbsp;</span></a>
              </span>
              <span class="category_value pull-right"><?php echo $value->total_space_available; ?></span>
            </div>
         <?php } // end loop 
         } //end if
         // end loop  ?>
      <!--end of row-fluid-->
        
      </div>
      

      <div class="btn_wrapper ">
        <a href="#collapseOne" class="pull-left scroll_top"><?php echo lang('comm_top'); ?></a>

        <?php 
          echo form_hidden('formActionName','corporateFloorPlan');
          echo form_input(array('type'=>'hidden','value'=>$eventId,'name'=>'eventId','id'=>'eventId'));
          echo form_input($formcorporateFloorPlanId);
          $extraSave  = 'class="default_btn btn pull-right medium" id="floor_plan_btn" ';
          echo form_submit('save',lang('comm_save_changes'),$extraSave);
          
          $extraSave  = 'class="default_btn btn pull-right medium reset_form" ';
          echo form_button('clear',lang('comm_reset'),$extraSave);
          
        ?>
        
      </div>
    </div>
    
    </form>
  </div>
</div>
<!--end of panel-->

<script>
  
  /*$(document).on("click", ".error_close_button", function(e){
    // check model popup
    $("#space_type_popup").modal({ keyboard: false}); // show the model
  }); */
  
  //save event bank details
  ajaxdatasave('formCorporateFloorPlan','<?php echo $this->uri->uri_string(); ?>',true,true,false,true,true,'#showhideformdivcorporatefloorplan','#showhideformdivcorporatedefaultbenefit',false);
    
  
  // Save corporate space type
  $(document).on('click',"#corporateSpaceTypeButton",function( event ) {
    
    //validate form
    if($("#formCorporateSpaceType").parsley().isValid()){
        
        event.preventDefault(); 
        var checkedCount=0;
         // get total checked checkbox
        $( "input[name='space[]']:checked" ).each(function(){
            if($(this).prop('disabled')==false){
             checkedCount = checkedCount+1;
            }
        });
        
       if(checkedCount==$("#space_ava").val()*1){ 
        //save event bank details
        ajaxdatasave('formCorporateSpaceType','<?php echo $this->uri->uri_string(); ?>',true,false,false,true,true,'#showhideformdivcorporatefloorplan','#showhideformdivcorporatedefaultbenefit',false);
    
       }else{
        $("#space_type_popup").modal('hide'); // hide the model
        //open error message
        custom_popup("The spaces selected exceed the amount of spaces available to this space type.",false);
      }
      } //end validation     
      
  });
  
  
  // Custom example logic for file upload
  var postData = {event_id : '<?php echo $eventId; ?>'}
  var uploadUrl = baseUrl+'event/updatecorporatefloorplan';

  var otherObj = {
      pickfiles:"attch_pickfiles", 
      dropArea:"attch_dropArea", 
      FileContainer:"attch_FileContainer", 
      fileTypes:"pdf", 
      deleteUrl:baseUrl+'event/deletecorporatefloorplan', 
      maxfilesize:"10mb", 
      multiselection:false, 
      isFileList:true, 
      uniqueNames:true, 
      autoStart:false, 
      isDelete:true, 
      maxfileupload:1, 
      mainDivFileList:"attach_file_list_main",
      listAddFile:"attch_file_list",
      adddedfile:"attchadddedfile",
    }

  upload_file(uploadUrl,postData,otherObj);
  
  //call function for delete file
  deleteUploadFiles();
  
</script>

<script>
  
  // call delete function to delete the particular default package
  customconfirm('deletecorporatespacetype','event/deletecorporatespacetype','','','',true);
  
  //edit custom contact
  $(document).on("click", ".editcorporatespacetype", function(e){
    if($("#hd_total_space_available").val()==$("#refr_no").val()){  
      var removeDivID = $(this).parent().parent().attr('id');
      var spaceId = $(this).attr('data-contact');
      var refr_no = $("#refr_no").val();
      var eventId = $("#eventId").val();
      
      formPostData = {popupType:'spaceTypePopup',floorPlanTypeId:spaceId,refr_no:refr_no,eventId:eventId,removeDivID:removeDivID};
      //eventcustompopup('event/corporatepopup/','space_type_popup',formPostData,'space_ava',removeDivID);
      //call ajax popup function
      ajaxpopupopen('space_type_popup','event/corporatepopup',formPostData,'editcorporatespacetype','space_ava');
      
    }else{
          custom_popup('Please save changes which you have done before ahead.',false);  
    }
    
  });

  // delete custom contact
  $(document).on("click", ".deletcorporatespacetype", function(e){
    
    var contactId = $(this).attr('data-contact');
    $("#hidden_custom_contact_array").append('<input type="hidden" name="hidden_deleted_custom_contact[]" value="'+contactId+'" />');
    $(this).parent().parent('div').remove();

  });
  

  // delete custom contact
  $(document).on("click", ".floorplanspaceavail .stepper-step", function(e){

    //get avaliable space
    var availableSpace = $("#available_space").val();
    var spaceCounter   = $("#space_ava").attr('max');
    //alert(spaceCounter);
    if($(this).hasClass('up')){
      if((spaceCounter*1) <= ($("#space_ava").val()*1)){
        $("#spaceNotAvaliable").show();
        $("#spaceNotAvaliable").find("span").css( "color", "red" ).html("Avaliable space limit is "+availableSpace+"" );
      }
    }else if($(this).hasClass('down')){
      if((spaceCounter*1) < (availableSpace*1)){
        $("#spaceNotAvaliable").find("span").html("");
      }
    }
    
  });
    
  $(document).on("click", "#corporate_add_space_type", function(e){
    
    
    //var contactId = $(this).attr('data-contact');
    var  corporateFloorPlanId = $("#corporateFloorPlanId").val();
    
    if($("#hd_total_space_available").val()==$("#refr_no").val()){
      var refrNo = $("#refr_no").val();
      var eventId = $("#eventId").val();
      if(corporateFloorPlanId!="" && corporateFloorPlanId!="0"){
        if(refrNo!="" && refrNo!="0"){
          formPostData = {popupType:'spaceTypePopup',refr_no:refrNo,eventId:eventId};
          //call ajax popup function
          ajaxpopupopen('space_type_popup','event/corporatepopup',formPostData,'corporate_add_space_type','space_ava');
          //eventcustompopup('event/corporatepopup','space_type_popup',formPostData,'space_ava');
        }else{
          custom_popup('Total spaces available is required.',false,false);
        } 
      }else{
          custom_popup('Please add floor plan before adding floor plan type.',false,false); 
      }
    }else{
          custom_popup('Please save changes which you have done before ahead.',false,false);  
    }
    
    });
  
  
</script>
