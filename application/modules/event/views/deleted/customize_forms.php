<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>

<?php $this->load->view('event_menus');  ?>

<div class="row-fluid mt20">
	<div class="span7">
		
		<?php $this->load->view('form_customize_form_url'); ?>
		
		<?php $this->load->view('form_customize_logo'); ?>
		
		<?php $this->load->view('form_customize_header'); ?>
		
		<?php $this->load->view('form_customize_sponsor_logo'); ?>
		
		<?php $this->load->view('form_customize_color_theme'); ?>
		
		<?php 
			//next and previous button
			$buttonData['viewbutton'] = array('back','next','preview');
			$buttonData['linkurl'] 	= array('back'=>base_url('event/setuppayment'),'next'=>base_url('event/confirmdetails'),'preview'=>'');
			$this->load->view('common_back_next_buttons',$buttonData);
		?>
		
	</div>
	
	<!----Right side advertisement view load start------>
		<?php $this->load->view('right_side_advert'); ?>
	<!----Right side advertisement view load end------>
	
</div>	
