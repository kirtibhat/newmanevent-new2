<?php 

$groupBookingAllowed 			= (!empty($eventdetails->group_booking_allowed))?$eventdetails->group_booking_allowed:"0";
$miniGroupBookingSize			= (!empty($eventdetails->min_group_booking_size))?$eventdetails->min_group_booking_size:"";
$discount 						= (!empty($eventdetails->percentage_discount_for_group_booking))?$eventdetails->percentage_discount_for_group_booking:"";

$formEventGroupBooking = array(
		'name'	 => 'formEventGroupBooking',
		'id'	 => 'formEventGroupBooking',
		'method' => 'post',
		'class'  => '',
		//'data-parsley-validate' => '',
);

$eventAllowGBookin = array(
		'name'	=> 'group_booking',
		'value'	=> '1',
		'id'	=> 'group_booking',
		'type'	=> 'checkbox',
		'class'	=> ''
);	
$showClass = 'dn';
if($groupBookingAllowed=='1'){
	$eventAllowGBookin['checked']=true;
	$showClass = '';
}	

$eventMiniGSize = array(
		'name'	=> 'min_gsize',
		'value'	=> $miniGroupBookingSize,
		'id'	=> 'min_gsize',
		'type'	=> 'text',
		//'required'=>'',
		'class'	=> 'small dark short_field numValue',
		'autocomplete' => 'off',
		'data-parsley-error-message' => lang('common_field_required'),
		'data-parsley-error-class' => 'custom_li',
		'data-parsley-trigger' => 'keyup'
		
);	

$eventdiscount = array(
		'name'	=> 'discount',
		'value'	=> $discount,
		'id'	=> 'discount',
		'type'	=> 'text',
		'class'	=> 'small numValue',
);		

 
 ?>
 <div class="panel event_panel">
                    <div class="panel-heading ">
                        <h4 class="panel-title medium dt-large ">
							<a class=" " data-toggle="collapse" data-parent="#accordion" href="#collapseSix"><?php echo lang('event_setup_group_bookings'); ?></a>
                        </h4>
                    </div>
                    <div style="height: auto;" id="collapseSix" class="accordion_content collapse apply_content">
                        <?php echo form_open($this->uri->uri_string(),$formEventGroupBooking); ?>
                        <div class="panel-body ls_back dashboard_panel small" id="showhideformdivgroupbooking">
          
                            <div class="row-fluid-15">
                                <label for="amc_con"><?php echo lang('event_setup_allow_group_booking'); ?></label>
                                <div class="checkbox_wrapper">
                                	<?php echo form_input($eventAllowGBookin); ?>
                                    <label for="group_booking">&nbsp;</label>
                                </div>
                            </div>
                            <!--end of row-fluid-->
						  <div id="showminimumgroupsize" class="<?php echo $showClass; ?>">	
                            <div class="row-fluid-15">
                                <label for="min_gsize"><?php echo lang('event_setup_minim_group_size'); ?>
                                	<span class="info_btn"><span class="field_info xsmall"><?php echo lang('tooltip_event_setup_mini_group_size'); ?></span></span>
                                </label>
                                <?php echo form_input($eventMiniGSize); ?>
                            </div>
                            <!--end of row-fluid-->
          
                            <div class="row-fluid-15">
                                <label for="discount"><?php echo lang('event_setup_discount'); ?></label>
                                <div class="rate">
									<?php echo form_input($eventdiscount); ?>
                                </div>
                            </div>
                            <!--end of row-fluid-->
						 </div>	
                            <div class="btn_wrapper ">
                              <a href="#collapseSix" class="pull-left scroll_top"><?php echo lang('comm_top'); ?></a>

                            	<?php 
									echo form_hidden('eventId', $eventId);
									echo form_hidden('formActionName', 'formEventGroupBooking');
									
									//button show of save and reset
									$formButton['showbutton']   = array('save','reset');
									$formButton['labletext'] 	= array('save'=>lang('comm_save_changes'),'reset'=>lang('comm_reset'));
									$formButton['buttonclass']  = array('save'=>'default_btn btn pull-right medium','reset'=>'default_btn btn pull-right medium reset_form');
									$this->load->view('common_save_reset_button',$formButton);
								?>
                            	
                            </div>
                        </div>
                         <?php echo form_close();?>
                    </div>
</div>


<script>
	//save contact person details
	ajaxdatasave('formEventGroupBooking','<?php echo $this->uri->uri_string(); ?>',true,true,false,false,false,'#showhideformdivgroupbooking','#showhideformdivemailSection');
	
	//edit custom contact //
	$(document).on("click", "#group_booking", function(e){
		// For set collapse hight
		$(".accordion_content").css("height",'auto');
		if($(this).is(":checked")){
			$("#showminimumgroupsize").show();
		}else{
			$("#showminimumgroupsize").hide();
		}		
	});
	
</script>	
