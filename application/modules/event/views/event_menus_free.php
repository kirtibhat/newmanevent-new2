<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$method = $this->router->fetch_method();
?>
<div class="page_content">
<div class="container">
<div class="">
  <div class="heading">
    <h1 class="font-xlarge event_title_event"><?php echo $eventdetails->event_title; ?></h1>
  </div>
  <div class="contentNavigation manager_setup">

    <ul id="ed_nav" class="">
       
   <li class="event_detail <?php echo ($method=='eventdetails')?"active":""; ?>">
    <a href="<?php echo base_url('event/eventdetails'); ?>" class="">
        <span class="xlarge_icon"> <i class="icon-eventdetail"></i></span><span class="text"><?php echo lang('event_event_details'); ?></span> 
    </a> 
  </li>
  
    <li class="registertion  <?php echo ($method=='invitations')?"active":""; ?>">
    <a href="<?php echo base_url('event/invitations'); ?>" class="">
        <span class="xlarge_icon"> <i class="icon-user"></i></span><span class="text"><?php echo lang('event_setup_invitations'); ?></span> 
    </a> 
  </li>

    <li class="customize_form <?php echo ($method=='customizeforms')?"active":""; ?>">
     <a href="<?php echo base_url('event/customizeforms') ?>" class=""><!-- page_leave -->
        <span class="xlarge_icon"> <i class="icon-customize"></i></span><span class="text"><?php echo lang('event_setup_customize_forms'); ?></span> 
    </a> 
  </li>
   
   <li class="confirm_detail  <?php echo ($method=='confirmdetails')?"active":""; ?>">
     <a href="<?php echo base_url('event/confirmdetails') ?>" class="">
        <span class="xlarge_icon"> <i class="icon-thumb"></i></span><span class="text"><?php echo lang('event_setup_confirm_details'); ?></span> 
    </a> 
  </li>
  
 
</ul>
    
    
  </div>
 </div>
</div>
 </div>
<script type="text/javascript">
//page leave alert show
pageLeave();
</script>

