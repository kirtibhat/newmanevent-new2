<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>
<?php $uri_param = $this->uri->segment(2); ?>
<?php //echo $uri_param; ?>
<div class="clearFix display_inline w_100 mT10"></div>	
<div class="col-9">
    <div class="btn-group pull-right">
    <?php if(array_search('back',$viewbutton)!==false){ ?> 
        <a href="<?php echo $linkurl['back']; ?>" class="btn-nav btn prevI form_back"> <i class="icon-leftarrow"></i>Back </a>
    <?php } ?>	    
    
    <?php 
    if($uri_param == 'customizeforms') {
        if(array_search('preview',$viewbutton)!==false){ ?> 	
		<a href="<?php echo  $linkurl['preview']; ?>" class="btn-nav btn prevI form_back"  target="_blank">Preview Form</a>
	<?php } 
    } ?>	
    
	<?php if(array_search('next',$viewbutton)!==false){ ?>	
		
        <a  href="<?php echo  $linkurl['next']; ?>"  class="btn-nav prevI btn form_next <?php if($uri_param == 'eventdetails'){ echo "next_step_1"; } else if( $uri_param=='invitations') { echo "next_step_2"; } else if($uri_param== 'customizeforms') { echo 'next_step_3'; } ?>"> Next<i class="icon-rightarrow"></i></span> </a>
	<?php } ?> 		
   </div> 
</div>
<script>
$('.next_step_1').click(function(){
	var $inputs = $('#formGeneralEventSetup :input');
	$('#formGeneralEventSetup').append("<input type='hidden' name='next_save' value='next_save'/>"); 
    var values = {};
    $inputs.each(function() {
        values[this.name] = $(this).val();
    });
    
    var $inputs = $('#formVenueEventSetup :input');
	$('#formVenueEventSetup').append("<input type='hidden' name='next_save' value='next_save'/>"); 
    var values_second = {};
    $inputs.each(function() {
        values_second[this.name] = $(this).val();
    });
    var $inputs = $('#formVenueContactPerson :input');
	$('#formVenueContactPerson').append("<input type='hidden' name='next_save' value='next_save'/>"); 
    var values_third = {};
    $inputs.each(function() {
        values_third[this.name] = $(this).val();
    });
    
    var $inputs = $('#formContactPerson :input');
	$('#formContactPerson').append("<input type='hidden' name='next_save' value='next_save'/>"); 
    var values_fourth = {};
    $inputs.each(function() {
        values_fourth[this.name] = $(this).val();
    });
    
    var $inputs = $('#formSocialMediaSetup :input');
	$('#formSocialMediaSetup').append("<input type='hidden' name='next_save' value='next_save'/>"); 
    var values_fifth = {};
    $inputs.each(function() {
        values_fifth[this.name] = $(this).val();
    });
    
    var $inputs = $('#formTermsCondition :input');
	$('#formTermsCondition').append("<input type='hidden' name='next_save' value='next_save'/>"); 
    var values_sixth = {};
    $inputs.each(function() {
        values_sixth[this.name] = $(this).val();
    });
    
    ajaxdatasave('formGeneralEventSetup', 'event/eventdetails', false, false, false, false, false, '#showhideformdivgeneralSection', '#showhideformdivVenueSection','','','','','',values);
    ajaxdatasave('formVenueEventSetup', 'event/eventdetails', false, false, false, false, false, '#showhideformdivVenueSection', '#showhideformdivVenueContactPerson','','','','','',values_second);
    ajaxdatasave('formVenueContactPerson', 'event/eventdetails', false, false, false, false, false, '#showhideformdivVenueContactPerson', '#showhideformdivpersonalContact','','','','','',values_third);
    ajaxdatasave('formContactPerson','event/eventdetails',false,false,false,false,false,'#showhideformdivpersonalContact','#showhideformdivSocialMedia','','','','','',values_fourth);
    ajaxdatasave('formSocialMediaSetup', 'event/eventdetails',false,false,false,false,false,'#showhideformdivSocialMedia','#showhideformdivtermSection','','','','','',values_fifth);
    ajaxdatasave('formTermsCondition','event/eventdetails',false,false,false,false,false,'#showhideformdivtermSection','','','','','','',values_fifth); 
    
});

$('.next_step_2').click(function(){
    var $inputs = $('#formbookerdetails :input');
    $('#formbookerdetails').append("<input type='hidden' name='next_save' value='next_save'/>"); 
    var values_second_first = {};
    $inputs.each(function() {
        values_second_first[this.name] = $(this).val();
    });
    //console.log(values_second_first);

    var $inputs = $('#formPersonalDetailsregisDetails :input');
    $('#formPersonalDetailsregisDetails').append("<input type='hidden' name='next_save' value='next_save'/>"); 
    var values_second_second = {};
    $inputs.each(function() {
        values_second_second[this.name] = $(this).val();
    });
    //console.log(values_second_second); 
    ajaxdatasave('formbookerdetails','event/invitations',false,false,false,false,false,'#showhideformdivbooker_details','#showhideformdivperosnal_regisDetails','','','','','',values_second_first);
    ajaxdatasave('formPersonalDetailsregisDetails','event/invitations',false,false,false,false,false,'#showhideformdivperosnal_regisDetails','#showhideformdivdieteryrequirement','','','','','',values_second_second);
});

$('.next_step_3').click(function(){
    var $inputs = $('#formAddEventTheme :input');
    $('#formAddEventTheme').append("<input type='hidden' name='next_save' value='next_save'/>"); 
    var values_third_first = {};
    $inputs.each(function() {
        values_third_first[this.name] = $(this).val();
    });
    ajaxdatasave('formAddEventTheme','event/customizeforms',false,false,false,false,false,'','','','','','','',values_third_first);
});


</script>
