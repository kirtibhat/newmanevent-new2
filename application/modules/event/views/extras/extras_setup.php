<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 |--------------------------------------------------------------------------|
 | This Field define for temparay open popup by add section id              |
 | and edit record id                                                       |   
 |--------------------------------------------------------------------------|
*/
 
$tempHidden = array(
    'type' => 'hidden',
    'name' => 'tempHidden',
    'id' => 'tempHidden',
    'value' => '',
);
echo form_input($tempHidden);

$this->load->view('event/event_menus');

$catFilterList = array();
if(!empty($registrantCategory)){
    foreach($registrantCategory as $value){
        $catFilterList[$value->category_id][$value->category][$value->registrant_id] = $value->registrant_name; 
    }//end loop
}
?>

<div class="row-fluid-15">
        <div id="page_content" class="span9">
            <div id="accordion" class="panel-group">
                <p>
                    <a href="javascript:void(0)" class="btn medium add_big_btn event_btn_margin add_edit_extras" actionform="add" extraid="0"><?php echo lang('event_extras_popup_add_title'); ?></a>
                </p>
                <?php
                
                    if(!empty($event_extra_data)){
                        
                        //prepare array data for current and next form hide/show  
                        foreach($event_extra_data as $event_extra){
                            $recordId[] =  $event_extra->id;
                        }
                        
                        //show side event forms 
                        $rowCount = 0;  
                        foreach($event_extra_data as $event_extra){
                            $nextId = $rowCount+1;
                            $sendData['extradata']          =$event_extra; 
                            $sendData['catFilterList']      =$catFilterList; 
                            $sendData['nextRecordId']   = (isset($recordId[$nextId]))?$recordId[$nextId]:'0';
                            // daynamic load all registrant forms
                            $this->load->view('extras/form_extra_setup',$sendData);
                            $rowCount++;
                        }
                    }
                ?>
                
            </div>
            <!--end of accordion content-->
            
            <div class="page_btnwrapper">
                <?php 
                    //next and previous button
                    $buttonData['viewbutton'] = array('back','next','preview');
                    $buttonData['linkurl']  = array('back'=>base_url('event/setupsideevents'),'next'=>base_url('event/setuppayment'),'preview'=>'');
                    $this->load->view('common_back_next_buttons',$buttonData);
                ?>
            </div>
            <!--end of page btn wrapper-->

    </div>
    <!--end of page content-->
</div>
<!--end of row-->  


<!--start custom tooltip-->
<div id="tooltip_wrapper"></div>
<!--end custom tooltip-->

<script>
$(document).ready(function(){
    /*-----Open add and edit side event popup-------*/
    $(document).on('click','.add_edit_extras',function(){
        
        var formAction = $(this).attr('actionform');
        
        if($(this).attr('extraid')!==undefined){
            var extraid = parseInt($(this).attr('extraid'));
            //set value in assing
            $("#tempHidden").val(extraid);
        }else{
            //get value form temp hidden and set
            var extraid = parseInt($("#tempHidden").val());
        }

        var eventId = '<?php echo $eventId ?>';
        var sendData = {"extraid":extraid, "eventId":eventId, "formAction":formAction};
        
        //call ajax popup function
        ajaxpopupopen('add_extras_popup','event/extrapopup',sendData,'add_extras_popup');
    });

    //save side event add and edit data
    ajaxdatasave('formAddExtra','event/addeditextrasave',true,true,false,true,true,false,false,false);

    /*---This function is used to delete custome field for side event -----*/
    customconfirm('delete_extraitem','event/deleteextraitem','','','',true,true);

    /*-----allow Restrict Purchase Access for side event-------*/
    $(".restrictPurchaseAccess").click(function(){
        $(".accordion_content").css("height",'auto');
        var extraId=$(this).attr('extraId');
        var registrantId = $(this).attr('registrantid');
        var appendId = extraId+"_"+registrantId;
        if($(this).val()=="1"){            
          $("#registrantCatDiv"+extraId).show();
        }else{
          $("#registrantCatDiv"+extraId).hide();
          $(".registrantTypeDetailedForm").addClass('dn');
        }
    });

    $(document).on('click','.registrantCat',function(){
        // For set collapse hight
        $(".accordion_content").css("height",'auto');
        
        var divId = $(this).attr('data-append-id');
        if($(this).prop("checked")){
          $("#category_list_"+divId).show();
        }else{
          $("#category_list_"+divId).hide();
        }
    });

    $(document).on('click','.registrantlisttype',function(){
      // For set collapse hight
      $(".accordion_content").css("height",'auto');
        
      // get text
      var RegistrantType = $(this).parent().find('label').text();
      var extraId  = $(this).attr('extraId');
      var registrantId = $(this).attr('registrantid');
      var appendId = extraId+"_"+registrantId;
      var getchecked = $(this).prop("checked");      
      if($(this).prop("checked")){
        //alert(appendId);    
        $("#registrantText"+appendId).html(RegistrantType); 
        $("#registrantTypeDetailedForm"+appendId).removeClass('dn');            
        $('#reg_limit'+appendId).attr('required','');  
        $('#RegistaintId'+appendId).val(registrantId);

        $('#total_price_comp'+appendId).attr('required','');
        $('#gst_included_total_price_comp'+appendId).attr('required',''); 
      } else {
        $("#registrantTypeDetailedForm"+appendId).addClass('dn');
        $('#reg_limit'+appendId).removeAttr('required');
      }
    });

    $(document).on('click','.complementary',function(){
        $(".accordion_content").css("height",'auto');
        var appendId  = $(this).attr('appendId');
        var getchecked = $(this).prop("checked");      
        if($(this).prop("checked")){
            $("#complementarydiv"+appendId).hide('slow');
            $('#total_price_comp'+appendId).attr('data-parsley-required', 'false');
            $('#total_price_comp'+appendId).removeAttr('required');

            $('#gst_included_total_price_comp'+appendId).attr('data-parsley-required', 'false');    
            $('#gst_included_total_price_comp'+appendId).removeAttr('required');
        } else { 
            $("#complementarydiv"+appendId).show('slow');            
            $('#total_price_comp'+appendId).attr('required','');
            $('#gst_included_total_price_comp'+appendId).attr('required',''); 
        }
    });

    var gstRate = parseInt('<?php echo (!empty($eventdetails->gst_rate))?$eventdetails->gst_rate:"0"; ?>');

    /*----This function is used to complementary total price enter and show complementary total price-----*/
    unitPriceManage('total_price_compEnter','appendid','gst_included_total_price_comp','total_price_comp',gstRate);


    /*-------This function is used to extra item custom field---------*/
    /*---This function is used to add and edit custome field for personal field -----*/
    $(document).on('click','.add_custom_field',function(){  
      //set value in temparray hidden field for if again open
      if($(this).attr('extraId') !== undefined && $(this).attr('customfieldid') !== undefined ){    
        var extraId = $(this).attr('extraId');
        var customfieldid = $(this).attr('customfieldid');
        var formAction = $(this).attr('formAction');
        var formId     = $(this).attr('formId');                                                      
        //concanate two value in assing
        var tempObj = extraId+','+customfieldid;
        $("#tempHidden").val(tempObj);
      }else{
        var getTempData = $("#tempHidden").val();
        var getData = getTempData.split(',');                                                         
        //get value from hidden field and set value
        extraId = getData[0];
        customfieldid = getData[1];
      }

      var sendData = {"extraId":extraId, "customfieldid":customfieldid, "formAction":formAction, "formId":formId};  
      //call ajax popup function
      ajaxpopupopen('add_custom_field_popup','event/eventextrascustomfield',sendData,'add_custom_field', '');
    });

    /*
    *  This section is used to select custome field type select
    */ 
    $(document).on('click','.fieldtype',function(){
      var getval = $(this).val(); 
      var customFieldId = parseInt($("#customFieldId").val());
      //if use add field then remove old data
      if(customFieldId==0){
        $("#selectedField").val('0');
        $(".default_value_list").html('');
      }
      if(getval=="text" || getval=="file"){
        $("#show_dropdown_feature").hide();
      }else{
        $("#show_dropdown_feature").show();
        $("#custom_field_qty").val('1');
        $(".default_value_list").html('<label class="pull-left" for="field_1">Field 1</label><input type="text" class="small" value="" required  data-parsley-error-message = "<?php echo lang('common_field_required'); ?>" data-parsley-error-class="custom_li"  data-parsley-trigger="keyup" name="defaultValue[]" id="defaultValue_0" />');
      }
    }); 

    /*
    *  This section is used select option fields
    */   
    $(document).on('click','.stepper-step',function(){
      var qty = parseInt($("#custom_field_qty").val());//$("#benefit_qty").val();     
      if($(this).hasClass('up')){ // increment
        if(qty<10){
          var currentId = 'defaultValue_'+qty; 
          var currentLblId = 'defaultLblValue_'+qty; 
          $("#custom_field_qty").val((qty*1)+1);
          $(".default_value_list").append('<label class="pull-left" for="field_1" id="'+currentLblId+'">Field '+((qty*1)+1)+'</label><input type="text" class="small" value="" required  data-parsley-error-message = "<?php echo lang('common_field_required'); ?>" data-parsley-error-class="custom_li"  data-parsley-trigger="keyup" name="defaultValue[]" id="'+currentId+'" />');
        }   
      }else{
        var currentId = 'defaultValue_'+((qty*1)-1); 
        var currentLblId = 'defaultLblValue_'+((qty*1)-1); 
        if(qty>1){  
          $("#custom_field_qty").val(((qty*1)-1)); 
          $("#"+currentId).remove();
          $("#"+currentLblId).remove();
        }  
      } 
    });

    /*---This function is used to delete custome field for personal field -----*/
    customconfirm('delete_custom_field','event/deleteextrascustomfield', '', '', '', true,'',false,'<?php echo lang('event_msg_custom_field_deleted') ?>');


    $(document).on('click','.commonPrice',function(){
        $(".accordion_content").css("height",'auto');
        var extraId  = $(this).attr('extraId');
        if($(this).prop("checked")){
            $("#commonPricediv"+extraId).show('slow');            
            $('#common_total_price'+extraId).attr('required','');
            $('#common_gst_included'+extraId).attr('required','');
        } else { 
            $("#commonPricediv"+extraId).hide('slow');
            $('#common_total_price'+extraId).attr('data-parsley-required', 'false');
            $('#common_total_price'+extraId).removeAttr('required');

            $('#common_gst_included'+extraId).attr('data-parsley-required', 'false');    
            $('#common_gst_included'+extraId).removeAttr('required');
        }
    });

    /*----This function is used to common total price enter and show common total price-----*/
    unitPriceManage('commonTotalPriceEnter','extraId','common_gst_included','common_total_price',gstRate); 

    //Save Extra Form Custome Field Via Ajax
    $(document).on("click", ".saveExtraField", function(e) {
        if($("#formAddCustomeField").parsley().isValid()){
            var fromData=$("#formAddCustomeField").serialize();
            var url = baseUrl+'event/addeditextrascustomfieldsave';                   
            var extraId = $("input[name=extraId]").val();
            $.ajax({
                type:'POST',
                data:fromData,
                url: url,
                dataType: 'json',
                async: true,
                cache: false,
                beforeSend: function( ) {                
                },
                success: function(data){
                    if(data.is_success=='true'){
                        var fieldName = $("#formAddCustomeField #fieldName").val();
                        var fieldId = data.id;
                        
                        var selectarr = '<select name="field_'+fieldId+'" id="field_'+fieldId+'" class="small custom-select short_field">';
                        $.each(data.fieldsmastervalue, function(index, val) {
                            var selectindex = (index=="1") ? "selected='selected'" : "";
                            selectarr+= '<option value="'+index+'" '+selectindex+'>'+val+'</option>';;
                        });
                        selectarr += '</select>';                        
                        var fieldHtml = '<input type="hidden" name="fieldid[]" value="'+fieldId+'">';
                        fieldHtml += '<div class="row-fluid-15" id="masterformregistrant_'+fieldId+'"><label for="dft_gender"   id="masterfieldlbl_'+fieldId+'">'+capWords(fieldName)+'</label>  '+ selectarr +' <span class="pull-right select_edit">   <a class="eventsetup_btn td_btn add_custom_field"  href="javascript:void(0)" extraId="'+extraId+'" formAction="eventExtrasCustomFieldSave" customfieldid="'+fieldId+'"><span>&nbsp;</span></a><a href="javascript:void(0)" class="delete_btn td_btn delete_custom_field" deleteid="'+fieldId+'"><span>&nbsp;</span></a></span></div>';
                        if(data.saveType=='insert'){
                            $("#formExtra"+extraId+" .custom_field_div"+extraId).append(fieldHtml);
                        } else if(data.saveType=='update'){
                            $("#formExtra"+extraId+" #masterfieldlbl_"+fieldId).html(capWords(fieldName));
                        }
                        $("#add_custom_field_popup").modal('hide')
                    }
                }
            });
        }              
        return false;
    });
});
</script>
