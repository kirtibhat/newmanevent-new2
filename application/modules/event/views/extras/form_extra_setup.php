<?php if (!defined('BASEPATH'))  exit('No direct script access allowed');
//set value in the variable 
$extraId = (!empty($extradata->id)) ? $extradata->id : '0';
$extraNameValue = (!empty($extradata->extra_name)) ? $extradata->extra_name:''; 
$eventExtraAddDetailVal = (!empty($extradata->additional_details)) ? $extradata->additional_details:'';
$eventExtraLimitVal = (!empty($extradata->extra_limit)) ? $extradata->extra_limit:'';
$eventExtraCommonPriceVal = (!empty($extradata->common_price)) ? $extradata->common_price:'';
$commonTotalPriceVal = (!empty($extradata->common_total_price)) ? $extradata->common_total_price:'';
$commonGstIncludedVal = (!empty($extradata->common_gst_included)) ? $extradata->common_gst_included:'';
$restrictPurchaseAccess = (!empty($extradata->restrict_purchase_access) && $extradata->restrict_purchase_access == '1' ) ? true : false;
//set value for early bird field
$registrantCatDiv = 'dn';
if (!empty($restrictPurchaseAccess)) {
  $registrantCatDiv = '';
}

//form create variable
$formExtra = array(
    'name' => 'formExtra' . $extraId,
    'id' => 'formExtra' . $extraId,
    'method' => 'post',
    'class' => 'form-horizontal',
    'data-parsley-validate' => '',
);
$eventExtraAddDetail = array(
    'name' => 'sideEventAddDetail' . $extraId,
    'value' => $eventExtraAddDetailVal,
    'id' => 'sideEventAddDetail' . $extraId,
    'type' => 'text',
    'rows' => '3',
    'class' => 'small'
);

$eventExtraLimit = array(
    'name' => 'eventExtraLimit' . $extraId,
    'value' => $eventExtraLimitVal,
    'id' => 'eventExtraLimit' . $extraId,
    'type' => 'text',
    'required' => '',
    'class' => 'small  short_field',
    'data-parsley-error-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
);

$eventExtraCommonPrice = array(
    'name' => 'eventExtraCommonPrice' . $extraId,
    'id' => 'eventExtraCommonPrice' . $extraId,
    'value' => '1',
    'extraId' => $extraId,
    'type' => 'checkbox',
    'class' => 'mt10 commonPrice',
    'checked' => $eventExtraCommonPriceVal,
);

$commonTotalPrice = array(
    'name' => 'common_total_price' . $extraId,
    'id' => 'common_total_price' . $extraId,  
    'value' => $commonTotalPriceVal,
    'extraId' => $extraId,
    'type' => 'text',
    'class' => 'small  short_field commonTotalPriceEnter',
    'data-parsley-error-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
);

$commonGstIncluded = array(
    'name' => 'common_gst_included' . $extraId,
    'id' => 'common_gst_included' . $extraId, 
    'value' => $commonGstIncludedVal,
    'extraId' => $extraId,
    'type' => 'text',
    'readonly' => '',
    'class' => 'small  short_field',
    'data-parsley-error-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
);

$restrictPurchaseAccessyes = array(
    'name' => 'restrictPurchaseAccess' . $extraId,
    'value' => 1,
    'id' => 'restrictPurchaseAccessyes' . $extraId,
    'type' => 'radio',
    'extraId' => $extraId,
    'class' => 'restrictPurchaseAccess',
    'checked' => ($restrictPurchaseAccess == '1') ? 'checked="checked"' : '',
);
$restrictPurchaseAccessno = array(
    'name' => 'restrictPurchaseAccess' . $extraId,
    'value' => 0,
    'id' => 'restrictPurchaseAccessno' . $extraId,
    'type' => 'radio',
    'extraId' => $extraId,
    'class' => 'restrictPurchaseAccess',
    'checked' => ($restrictPurchaseAccess == '0') ? 'checked="checked"' : '',
);
?>

<div class="panel event_panel co_cat">
  <div class="panel-heading" id="eventExtra_<?php echo $extraId; ?>">
    <h4 class="panel-title medium dt-large heading_btn">
      <a class="" data-toggle="collapse" data-parent="#accordion" href="#collapseFive<?php echo $extraId; ?>"><?php echo ucwords($extraNameValue); ?></a>
      <span class="heading_btn_wrapper">
        <a href="javascript:void(0)" class="delete_btn td_btn delete_extraitem" deleteid="<?php echo $extraId; ?>"><span>&nbsp;</span></a>
        <a href="javascript:void(0)" class="eventsetup_btn td_btn add_edit_extras" actionform="edit" extraId="<?php echo $extraId; ?>"><span>&nbsp;</span></a>
        <a href="javascript:void(0)" class="copy_btn td_btn add_edit_extras" actionform="duplicate" extraId="<?php echo $extraId; ?>"><span>&nbsp;</span></a>
      </span>
    </h4>
  </div>
  
  <div style="height: auto;" id="collapseFive<?php echo $extraId; ?>" class="accordion_content collapse apply_content ">
    <?php echo form_open($this->uri->uri_string(), $formExtra); ?>
    <div class="panel-body ls_back dashboard_panel small">


        <div class="row-fluid-15">
            <label for="info_org"><?php echo lang('event_extra_additional_details'); ?>
              <span class="info_btn">
                <span class="field_info xsmall"><?php echo lang('event_extra_additional_details'); ?></span>
              </span>
            </label>
            <?php echo form_textarea($eventExtraAddDetail); ?>
            <?php echo form_error('eventExtraAddDetail' . $extraId); ?>
        </div>
        <!--end of row-fluid-->

        <div class="row-fluid-15">
            <label for="reg_limit"><?php echo lang('event_extra_regis_limit'); ?> <span class="astrik">*</span>
              <span class="info_btn">
                <span class="field_info xsmall"><?php echo lang('event_extra_regis_limit'); ?></span>
              </span>
            </label>
            <?php echo form_input($eventExtraLimit); ?>
            <?php echo form_error('eventExtraLimit' . $extraId); ?>
        </div>
        <!--end of row-fluid-->


        <div class="row-fluid-15">
            <label for="mem_compl"><?php echo lang('event_extra_common_price'); ?>  <span class="astrik">*</span>
              <span class="info_btn">
                <span class="field_info xsmall"><?php echo lang('event_extra_common_price'); ?></span>
              </span>
            </label>
            <span class="checkbox_wrapper">
                <?php echo form_checkbox($eventExtraCommonPrice); ?>
                <label for="eventExtraCommonPrice<?php echo $extraId ?>" class="eventDcheck">&nbsp;</label>
                <?php echo form_error('eventExtraCommonPrice' . $extraId); ?>
            </span>
        </div>
        <!--end of row-fluid-->

        <div class="<?php echo ($eventExtraCommonPriceVal) ? '' : 'dn'; ?>" id="commonPricediv<?php echo $extraId ?>">
            <div class="row-fluid-15">
                <label for="reg_limit"><?php echo lang('event_extra_common_total_price'); ?> <span class="astrik">*</span>
                  <span class="info_btn">
                    <span class="field_info xsmall"><?php echo lang('event_extra_common_total_price'); ?></span>
                  </span>
                </label>
                <?php echo form_input($commonTotalPrice); ?>
                <?php echo form_error('commonTotalPrice' . $extraId); ?>
            </div>
            <!--end of row-fluid-->

            <div class="row-fluid-15">
                <label for="reg_limit"><?php echo lang('event_extra_common_price_gst'); ?> <span class="astrik">*</span>
                  <span class="info_btn">
                    <span class="field_info xsmall"><?php echo lang('event_extra_common_price_gst'); ?></span>
                  </span>
                </label>
                <?php echo form_input($commonGstIncluded); ?>
                <?php echo form_error('commonGstIncluded' . $extraId); ?>
            </div>
            <!--end of row-fluid-->
        </div>      
      
        <div class="row-fluid-15">
            <label for="info_org" class="ml_m8"><?php echo lang('event_extra_restrict_purchase_access'); ?> <span class="astrik">*</span>
              <span class="info_btn">
                <span class="field_info xsmall"><?php echo lang('event_extra_restrict_purchase_access'); ?>
                </span>
              </span>
            </label>
            <div class="radio_wrapper ">
              <?php echo form_radio($restrictPurchaseAccessyes); ?>
              <label for="restrictPurchaseAccessyes<?php echo $extraId; ?>"><?php echo lang('event_extra_purchase_access_yes'); ?></label>

              <?php echo form_radio($restrictPurchaseAccessno); ?>
              <label for="restrictPurchaseAccessno<?php echo $extraId; ?>"><?php echo lang('event_extra_purchase_access_no'); ?></label>
            </div>

            <div class="session_subcat l_margin <?php echo ($restrictPurchaseAccess == '0') ? 'dn' : ""; ?>" id="registrantCatDiv<?php echo $extraId; ?>">
              <?php
              

              //Convert object into array
              $sideeventRegistrantData = array();
              if (!empty($sideeventRegistrantData)) {
                foreach ($sideeventRegistrantData as $key => $value) {
                  $selectedregistrant = array(
                      'name' => 'selectedregistrant' . $extraId . '[' . $value->registrant_id . ']',
                      'value' => $value->session_limit,
                      'id' => 'hiddenSelectedRegistrant' . $extraId,
                      'type' => 'hidden',
                      'extraId' => $extraId,
                      'class' => 'registrantIds',
                      'key' => $value->registrant_id,
                  );
                  echo form_input($selectedregistrant);
                }
              }

                if (!empty($catFilterList)) {
                  foreach ($catFilterList as $key => $list) {
                    foreach ($list as $cat => $registrant) {
                      $registrantCat = array(
                          'name' => 'registrant_cat_' . $key . '_' . $extraId,
                          //'value'     => '',
                          'id' => 'registrant_cat_' . $key . '_' . $extraId,
                          'type' => 'checkbox',
                          'extraId' => $extraId,
                          'class' => 'registrantCat',
                          'data-append-id' => $key . '_' . $extraId,
                          'data-category-id' => $key,
                      );

                      $catCheckedVal = 0;
                      foreach ($registrant as $registrantId => $value) {                            
                          $where = array('extra_id ' => $extraId, 'registrant_id' => $registrantId);                    
                          $sessionRegistrantData = getDataFromTabel('event_extra_registrant', '*', $where, '', 'id', 'DESC');                       
                          if(!empty($sessionRegistrantData)){
                            foreach($sessionRegistrantData as $sessionRegData){
                                $catCheckedVal = $catCheckedVal+1;
                            }
                          }
                      }
             
                      $catChecked = (($catCheckedVal > 0) ? 'checked="checked"' : '');
                      ?>
                      <div class="row-fluid-15">
                        <span class="checkbox_wrapper">
                        <?php echo form_input($registrantCat, 1, $catChecked); ?>
                          <label for="registrant_cat_<?php echo $key; ?>_<?php echo $extraId; ?>"><?php echo $cat; ?></label></span></div>
                          <div id="category_list_<?php echo $key; ?>_<?php echo $extraId; ?>" data-category="<?php echo $key; ?>_<?php echo $extraId; ?>" class="<?php echo (($catCheckedVal > 0) ? '' : 'dn') ?>">
                        <?php
                        $regLoop = -1;
                        foreach ($registrant as $registrantId => $value) {      
                          $regLoop++;                      
                          $where = array('extra_id ' => $extraId, 'registrant_id' => $registrantId);                    
                          $sessionRegistrantData = getDataFromTabel('event_extra_registrant', '*', $where, '', 'id', 'DESC');
                  
                          if(!empty($sessionRegistrantData)){
                            foreach($sessionRegistrantData as $sessionRegData){
                                $registrantChecked = (in_array($registrantId,(array)$sessionRegData)) ? 'checked="checked"' : '';
                            }
                          } else {
                            $registrantChecked = '';
                          }

                          $registrantCheckbox = array(
                              'name' => 'registrantcheckbox' . $extraId . '[' . $registrantId . ']',
                              //'value' => '1',
                              'id' => 'registrantcheckbox' . $extraId . '_' . $registrantId,
                              'type' => 'checkbox',
                              'extraId' => $extraId,
                              'registrantId' => $registrantId,
                              'class' => 'registrantlisttype registrantlist_'.$key . '_' . $extraId,
                              'data-append-id' => $key . '_' . $extraId,
                              'data-category-id' => $key,
                          );
                          ?>
                          <div class="row-fluid-15 sub_check"><span class="checkbox_wrapper">
                          <?php echo form_input($registrantCheckbox, 1, $registrantChecked); ?>
                            <label for="registrantcheckbox<?php echo $extraId; ?><?php echo '_' . $registrantId; ?>"><?php echo $value; ?></label></span>
                          </div>
                      <?php  } //end loop ?>
                      </div>
                    <?php
                    } // end loop
                  } //end loop
                } // end if                                                                                       
              ?>
            </div>              
            <!--end of session sub category-->
        </div>

    <div class="add_custom_field row-fluid-15" extraId="<?php echo $extraId; ?>" formAction="eventExtrasCustomFieldSave" customfieldid="0"><a class="add_btn medium" href="javascript:void(0)" id="add_field"><?php echo lang('event_extra_custom_field'); ?></a></div>

    

    <?php
    //get master field values array 
    $fieldsmastervalue = fieldsmastervalue();
    //get booker field for each event id
    $whereMsterField = array('extra_id'=>$extraId);
    $customfielddata = getDataFromTabel('event_extras_custom_fields','*',$whereMsterField);
    if(!empty($customfielddata)){
      foreach($customfielddata as $key=> $formfield){

      $fieldCreateBy = (isset($formfield->field_create_by) && $formfield->field_create_by=="0")?$formfield->field_create_by:NULL;
      if($fieldCreateBy=="0"){ ?>

      <div class="row-fluid-15" id="masterformregistrant_<?php echo $formfield->id; ?>">
        <label for="dft_gender"  id="masterfieldlbl_<?php echo $formfield->id; ?>"><?php echo ucwords($formfield->field_name); ?></label>
        <?php
          echo form_hidden('fieldid[]', $formfield->id);
          $other    = 'id="field_'.$formfield->id.'" class="small custom-select short_field" ';
          $fieldName  = 'field_'.$formfield->id;
          echo form_dropdown($fieldName, $fieldsmastervalue, $formfield->field_status,$other);  
        ?>
        <span class="pull-right select_edit">
          <a class="eventsetup_btn td_btn add_custom_field"  href="javascript:void(0)" extraId="<?php echo $extraId; ?>" formAction="eventExtrasCustomFieldSave" customfieldid="<?php echo $formfield->id; ?>"><span>&nbsp;</span></a>
          <a href="javascript:void(0)" class="delete_btn td_btn delete_custom_field" deleteid="<?php echo $formfield->id; ?>"><span>&nbsp;</span></a>
        </span>
      </div>
    <?php } } } ?>

    <div class="custom_field_div<?php echo $extraId; ?>" id="custom_field_div<?php echo $extraId; ?>"></div>
    
    
      <?php
        if (!empty($catFilterList)) {
          foreach ($catFilterList as $key => $list) {
            foreach ($list as $cat => $registrant) {
              $registrantLoop = -1;
              foreach ($registrant as $registrantId => $value) {
                $registrantLoop++;
                $appendId = $extraId . '_' . $registrantId;
                //get value by breakout id 
                $where = array('extra_id' => $extraId, 'registrant_id' => $registrantId);
                $sessionRegistrantData = getDataFromTabel('event_extra_registrant', '*', $where);
                //echo '<pre>';print_r($sessionRegistrantData);

                $where = array('id' => $registrantId);
                $RegistrantData = getDataFromTabel('event_registrants', 'registrant_name', $where);
                //echo '<pre>';print_r($RegistrantData);

                if(!empty($sessionRegistrantData)){
                    $regName = $RegistrantData[0]->registrant_name; 
                    foreach($sessionRegistrantData as $sessionRegData){
                        $registrantTypeDetailedClass = (in_array($registrantId,(array)$sessionRegData)) ? '' : 'dn';
                        $reg_limitVal = (!empty($sessionRegData) ? $sessionRegData->reg_limit : '');
                        $total_price_compVal = (!empty($sessionRegData) ? $sessionRegData->total_price : '');
                        $gst_included_total_price_compVal = (!empty($sessionRegData) ? $sessionRegData->gst_included : '');
                        $complementary_checked = (!empty($sessionRegData) && $sessionRegData->complementary) ? 'checked="checked"' : '';
                        $complementaryClass = (!empty($sessionRegData) && $sessionRegData->complementary) ? 'dn' : '';
                        $eventExtraRegistaintIdVal = (!empty($sessionRegData) ? $sessionRegData->id : '');                    
                    }
                } else {
                    $registrantTypeDetailedClass = 'dn';
                    $regName = 'Member';
                    $reg_limitVal = ''; 
                    $total_price_compVal = ''; 
                    $gst_included_total_price_compVal = '';
                    $complementary_checked = '';
                    $complementaryClass = '';
                    $eventExtraRegistaintIdVal = 0;
                }


                $reg_limit = array(
                    'name' => 'reg_limit' . $appendId,
                    'value' => $reg_limitVal,
                    'id' => 'reg_limit' . $appendId,
                    'type' => 'text',
                    'class' => 'small short_field',
                    'data-parsley-error-message' => lang('common_field_required'),
                    'data-parsley-error-class' => 'custom_li',
                    'data-parsley-trigger' => 'keyup',
                );
                $complementary = array(
                    'name' => 'complementary' . $appendId,
                    'value' => '1',
                    'id' => 'complementary' . $appendId,
                    'type' => 'checkbox',
                    'appendId' => $appendId,
                    'class' => 'complementary',
                );
                $total_price_comp = array(
                    'name' => 'total_price_comp' . $appendId,
                    'value' => $total_price_compVal,
                    'id' => 'total_price_comp' . $appendId,
                    'type' => 'text',
                    'appendId' => $appendId,
                    'class' => 'small short_field total_price_compEnter numValue',
                    'data-parsley-error-message' => lang('common_field_required'),
                    'data-parsley-error-class' => 'custom_li',
                    'data-parsley-trigger' => 'keyup',
                );
                $gst_included_total_price_comp = array(
                    'name' => 'gst_included_total_price_comp' . $appendId,
                    'value' => $gst_included_total_price_compVal,
                    'id' => 'gst_included_total_price_comp' . $appendId,
                    'type' => 'text',
                    'readonly' => '',
                    'appendId' => $appendId,
                    'class' => 'small short_field',
                    'data-parsley-error-message' => lang('common_field_required'),
                    'data-parsley-error-class' => 'custom_li',
                    'data-parsley-trigger' => 'keyup',
                );
                
                $eventExtraRegistaintId = array(
                    'name' => 'eventExtraRegistaintId' . $appendId,
                    'value' => $eventExtraRegistaintIdVal,
                    'id' => 'eventExtraRegistaintId' . $appendId,
                    'type' => 'hidden',
                );
                echo form_input($eventExtraRegistaintId);
                ?>

                <div id="accordion_sub" class="panel-group sub-panel-group">
                  <div id="registrantTypeDetailedForm<?php echo $appendId; ?>" class="registrantTypeDetailedForm <?php echo $registrantTypeDetailedClass ?>">
                    <div class="panel event_panel sub_panel" id="registrantformpanel<?php echo $appendId; ?>">
                      <div class="panel-heading ">
                        <h4 class="panel-title medium dt-large">
                          <a class="" data-toggle="collapse" data-parent="#accordion_sub" href="#collapseSubOne<?php echo $appendId ?>" id="registrantText<?php echo $appendId; ?>" ><?php echo $regName; ?></a>
                        </h4>
                      </div>
                      <!--end of panel heading-->

                      <div id="collapseSubOne<?php echo $appendId ?>" class="accordion_content collapse apply_content ">
                        <div class="row-fluid-15">
                          <label for="reg_limit"><?php echo lang('event_side_limit'); ?> <span class="astrik">*</span>
                            <span class="info_btn">
                              <span class="field_info xsmall"><?php echo lang('event_side_limit'); ?></span>
                            </span>
                          </label>
                          <?php echo form_input($reg_limit); ?>
                        </div>
                        <!--end of row-fluid-->

                        <!-- First Complementary :start --> 
                        <div class="row-fluid-15">
                          <label for="amc_con"><?php echo lang('event_side_complimentary'); ?></label>   
                          <div class="checkbox_wrapper">
                            <?php echo form_input($complementary, 1, $complementary_checked); ?>
                            <label for="complementary<?php echo $appendId ?>">&nbsp;</label>
                          </div>
                        </div>
                        <!--end of row-fluid-->
                        
                        <div class="<?php echo $complementaryClass ?>" id="complementarydiv<?php echo $appendId; ?>">
                          <div class="row-fluid-15">
                            <label for="total_price_comp"><?php echo lang('event_side_compli_total_price'); ?> <span class="astrik">*</span>
                              <span class="info_btn"></span>
                            </label>
                            <?php echo form_input($total_price_comp); ?>
                          </div>
                          <!--end of row-fluid-->

                          <div class="row-fluid-15">
                            <label for="gst_included_total_price_comp"><?php echo lang('event_side_compli_gst_include'); ?></label>                            <?php echo form_input($gst_included_total_price_comp); ?>
                          </div>
                          <!--end of row-fluid-->

                          <div class="row-fluid-15">&nbsp;</div>
                          <div class="row-fluid-15">&nbsp;</div>
                        </div>      

                        <!-- First Complementary :end -->               
                      </div>
                      <!-- collapseSubOne //end of panel content-->

                    </div>
                    <!--end of panel--> 
                  </div>
                </div>
                <!--end of panel-group-->
              <?php
              }//end for each
            } // end loop
          } //end loop
        } // end if

      ?>

      



      <div class="btn_wrapper ">
        <a href="#collapseFive<?php echo $extraId; ?>" class="pull-left scroll_top"><?php echo lang('comm_top'); ?></a>
        <?php
        echo form_hidden('eventId', $eventId);
        echo form_hidden('extraId', $extraId);

        //button show of save and reset
        $formButton['saveButtonId'] = 'id="' . $extraId . '"'; // click button id
        $formButton['showbutton'] = array('save', 'reset');
        $formButton['labletext'] = array('save' => lang('comm_save_changes'), 'reset' => lang('comm_reset'));
        $formButton['buttonclass'] = array('save' => 'default_btn btn pull-right medium', 'reset' => 'default_btn btn pull-right medium reset_form');
        $this->load->view('common_save_reset_button', $formButton);
        ?>

      </div>
    </div>  
<?php echo form_close(); ?>
 
  </div>
</div>


<script type="text/javascript">
$(document).ready(function(){    
    //side event dtails save type details save
    ajaxdatasave('formExtra<?php echo $extraId; ?>', '<?php echo $this->uri->uri_string(); ?>',true,true,false,true,true,'#showhideformdiv<?php echo $extraId; ?>', '#showhideformdiv<?php echo $nextRecordId; ?>',false);
});
</script>
