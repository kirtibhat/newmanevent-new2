<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//--------add type of registration fields---------//  
$formCustomTheme = array(
    'name'      => 'formCustomTheme',
    'id'        => 'formCustomTheme',
    'method'    => 'post',
    'class'     => 'wpcf7-form formInvitationType',
    'data-parsley-validate' => '',
  );
  

$theme_name = !empty($colorThemedata['theme_name']) ? $colorThemedata['theme_name'] : ''; 
$theme_type = !empty($colorThemedata['theme_type']) ? $colorThemedata['theme_type'] : ''; 
$placeholder='';
if($theme_type==2) {
    
    $placeholder = $theme_name;
    $theme_name  = '';
}

$customThemeName = array(
    'name'          => 'customThemeName',
    'value'         => $theme_name,
    'id'            => 'customThemeName',
    'type'          => 'text',
    'class'         => 'xxlarge_input themenamecustom',
    'placeholder'   => 'Name', //$placeholder
    'required'      => '',
    'autocomplete'  => 'off',
    'data-parsley-error-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
);

$event_theme_screen_bg = !empty($colorThemedata['event_theme_screen_bg']) ? $colorThemedata['event_theme_screen_bg'] : ''; 
$event_theme_header_bg = !empty($colorThemedata['event_theme_header_bg']) ? $colorThemedata['event_theme_header_bg'] : ''; 
$event_theme_box_bg = !empty($colorThemedata['event_theme_box_bg']) ? $colorThemedata['event_theme_box_bg'] : ''; 
$event_theme_text_color = !empty($colorThemedata['event_theme_text_color']) ? $colorThemedata['event_theme_text_color'] : ''; 
$event_theme_link_color = !empty($colorThemedata['event_theme_link_color']) ? $colorThemedata['event_theme_link_color'] : ''; 
$is_theme_used = !empty($colorThemedata['is_theme_used']) ? $colorThemedata['is_theme_used'] : ''; 
$event_theme_link_highlighted_color = !empty($colorThemedata['event_theme_link_highlighted_color']) ? $colorThemedata['event_theme_link_highlighted_color'] : ''; 
$font_package = !empty($colorThemedata['font_package']) ? $colorThemedata['font_package'] : ''; 
$themeid = !empty($themeid) ? $themeid : ''; 

$image_position_top = !empty($colorThemedata['image_position_top']) ? $colorThemedata['image_position_top'] : '1'; 
$image_position_left = !empty($colorThemedata['image_position_left']) ? $colorThemedata['image_position_left'] : '1'; 
$image_width = !empty($colorThemedata['image_width']) ? $colorThemedata['image_width'] : '80'; 
$image_height = !empty($colorThemedata['image_height']) ? $colorThemedata['image_height'] : '80'; 

$themeTypeId = array(
    'name'      => 'themeTypeId',
    'value'     => $themeid,
    'id'        => 'themeTypeId',
    'type'      => 'hidden',
  );  

?>

<!-- Modal -->
<div id="customize_theme_popup" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" >
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header delegates">
        <h4 class="medium dt-large modal-title"><span id="registrantTypeTitle"><?php echo lang('customize_theme'); ?></span><?php //echo " ".lang('event_invitation_addtype'); ?></h4>
      </div>
      
      <div class="infobar dn" id="notsaved">
         <p id="thememessage">You havn't make any changes in default theme, do you want to keep default theme with new name? </p>
         <span class="info_btn"></span>
      </div>
        
	 <div class="modal-body">
        <div class="">
         <?php  echo form_open(base_url('event/addediteventtheme'),$formCustomTheme); ?>
            <div class="modal-body-content eventdashboard_popup small">
              <div class="row">
				<div class="labelDiv pull-left">
				  <label class="form-label text-right" for="customThemeName"><?php echo lang('event_regis_name'); ?><span aria-required="true" class="required">*</span></label>
				  <span class="info_btn"><span class="field_info xsmall">The Access ID#.</span></span>
				</div>
				 <?php echo form_input($customThemeName); ?>
				  <?php //echo form_error('ctype'); ?>
				<span id="error-customThemeName"></span>
			  </div>
			  </div>
              
              
              <div class="modal-footer">
			  <div class="pull-right">	
				  
				  
				  <?php 
                    echo form_input($themeTypeId); 
                    echo form_hidden('eventId', $eventId);
                    //Send color field data
                    echo form_hidden('event_theme_screen_bg', $event_theme_screen_bg);
                    echo form_hidden('event_theme_header_bg', $event_theme_header_bg);
                    echo form_hidden('event_theme_box_bg', $event_theme_box_bg);
                    echo form_hidden('event_theme_text_color', $event_theme_text_color);
                    echo form_hidden('event_theme_link_color', $event_theme_link_color);
                    echo form_hidden('is_theme_used', $is_theme_used);
                    echo form_hidden('event_theme_link_highlighted_color', $event_theme_link_highlighted_color);
                    echo form_hidden('font_package', $font_package);
                    
                    echo form_hidden('image_position_top', $image_position_top);
                    echo form_hidden('image_position_left', $image_position_left);
                    echo form_hidden('image_width', $image_width);
                    echo form_hidden('image_height', $image_height);
                    
                    $extraCancel  = 'class="popup_cancel submitbtn pull-right medium" data-dismiss="modal" ';
                    $extraSave    = 'class="submitbtn pull-right medium" ';
                   
                  ?>
                  <input type="hidden" value="<?php echo $is_theme_used; ?>" id="themeused">
                  
                  <button class="default_btn popup_cancel submitbtn btn-normal btn" data-dismiss="modal" type="button" name="form_cancel"><?php echo lang('comm_cancle'); ?></button>
                  <button class="default_btn popup_cancel submitbtn btn-normal btn" id="themeeventsaved" data-dismiss="modal" type="button" name="form_cancel"><?php echo lang('comm_save'); ?></button>
				  	                 
			  </div>
			 </div>
             
          <?php echo form_close(); ?>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
    $(document).ready(function (e){
        $('#themeeventsaved').on("click",function(){
			$('#formCustomTheme').parsley().validate();
			 if (true === $('#formCustomTheme').parsley().isValid()) {
				var themename = $('.themenamecustom').val();
				$('#theme_name').val(themename);
				$(".eventtheme").submit();
                loaderopen();
			} else {
                $('.parsley-errors-list').remove();
                var errorMsg ='<label id="mediatypename-error" class="error" for="customThemeName">This field required.</label>';
                $("#error-customThemeName").html(errorMsg);
                $("#customThemeName").addClass('error');
				return false;
			}
            
         });
    });  
    $('#customThemeName').keyup(function(){
        $(this).removeClass('error');
    });
    $('#customThemeName').keypress(function(){
        $("#mediatypename-error").hide();
        if (event.which == 13) { 
            event.preventDefault();
            var themename = $('.themenamecustom').val();
            $('#theme_name').val(themename);
            $('.eventtheme').submit();
        }
    });
</script>

