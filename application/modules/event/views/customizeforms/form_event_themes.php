<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
if (!empty($customizeformsdata)) { $customizeformsdata = $customizeformsdata[0]; }
/* Theme details */
$data_id                = (!empty($customizeformsdata->id)) ? $customizeformsdata->id : ''; 
$event_theme_name       = (!empty($customizeformsdata->event_theme_name)) ? $customizeformsdata->event_theme_name : ''; 
$theme_type             = (!empty($customizeformsdata->theme_type)) ? $customizeformsdata->theme_type : ''; 
$event_theme_used       = (!empty($customizeformsdata->is_theme_used)) ? $customizeformsdata->is_theme_used : ''; 
$logo_image_status      = (isset($customizeformsdata->logo_image_status)) ? $customizeformsdata->logo_image_status : 0; 
/* Theme colors & fonts property */
$event_theme_screen_bg  = (!empty($customizeformsdata->event_theme_screen_bg)) ? $customizeformsdata->event_theme_screen_bg : ''; 
$event_theme_header_bg  = (!empty($customizeformsdata->event_theme_header_bg)) ? $customizeformsdata->event_theme_header_bg : ''; 
$event_theme_box_bg     = (!empty($customizeformsdata->event_theme_box_bg)) ? $customizeformsdata->event_theme_box_bg : ''; 
$event_theme_text_color = (!empty($customizeformsdata->event_theme_text_color)) ? $customizeformsdata->event_theme_text_color : ''; 
$event_theme_link_color = (!empty($customizeformsdata->event_theme_link_color)) ? $customizeformsdata->event_theme_link_color : ''; 
$event_theme_link_highlighted_color = (!empty($customizeformsdata->event_theme_link_highlighted_color)) ? $customizeformsdata->event_theme_link_highlighted_color : ''; 
$primary_color          = (!empty($customizeformsdata->primary_color)) ? $customizeformsdata->primary_color : ''; 
$secondary_color        = (!empty($customizeformsdata->secondary_color)) ? $customizeformsdata->secondary_color : '';
/* fonts packeg */
$fontPackageVal         = (!empty($customizeformsdata->font_package)) ? $customizeformsdata->font_package : '';
$fontName               = getFontFamily($fontPackageVal);
$fontName               = (!empty($fontName->package_name)) ? $fontName->package_name : 'proxima_nova_altsemibold';
$fontPackagesData       = getDataFromTabel('font_packages', '*', '', '', 'package_name', 'ASC');
$fontPackagesOptions[""] = lang('event_customize_select_font_package');
if (!empty($fontPackagesData)) {
    foreach ($fontPackagesData as $fontPackage) {
        $fontPackagesOptions[$fontPackage->id] = $fontPackage->package_name;
    } 
}
$fontPackagesOtherOptions   = ' id="font_package" class="custom-select medium_select hasCustomSelect theme font_package"  cssAction="font_family" ';
/* canvas image object */
$sponsor_image_object       = (!empty($customizeformsdata->sponsor_image_object)) ? $customizeformsdata->sponsor_image_object : ''; 
/* Theme Images property */
$header_image           = (!empty($customizeformsdata->header_image)) ? $customizeformsdata->header_image : ''; 
$header_image_path      = (!empty($customizeformsdata->header_image_path)) ? $customizeformsdata->header_image_path : ''; 
$logo_image             = (!empty($customizeformsdata->logo_image)) ? $customizeformsdata->logo_image : ''; 
$logo_image_path        = (!empty($customizeformsdata->logo_image_path)) ? $customizeformsdata->logo_image_path : ''; 
$logo_size              = (!empty($customizeformsdata->logo_size)) ? $customizeformsdata->logo_size : ''; 
$logo_size_and_position = json_decode($logo_size);
$image_position_top     = (isset($logo_size_and_position->image_position_top)) ? $logo_size_and_position->image_position_top : '19';
$image_position_left    = (isset($logo_size_and_position->image_position_left)) ? $logo_size_and_position->image_position_left : '23' ;
$image_width            = (!empty($logo_size_and_position->image_width)) ? $logo_size_and_position->image_width : '125';
$image_height           = (!empty($logo_size_and_position->image_height)) ? $logo_size_and_position->image_height : '110';


$header_image_size      = (!empty($customizeformsdata->header_image_size)) ? $customizeformsdata->header_image_size : ''; 
$header_image_position  = json_decode($header_image_size);
$x_axis                 = (isset($header_image_position->x_axis)) ? $header_image_position->x_axis : '111';
$y_axis                 = (isset($header_image_position->y_axis)) ? $header_image_position->y_axis : '0' ;
$scale                  = (isset($header_image_position->scale)) ? $header_image_position->scale : '0.464';
/* Assign Theme Saved data */
/* Set header image path */
$is_header_or_bgcolor = (!empty($customizeformsdata->is_header_or_bgcolor)) ? $customizeformsdata->is_header_or_bgcolor : ''; 
$event_theme_header_bg = (!empty($customizeformsdata->event_theme_header_bg)) ? $customizeformsdata->event_theme_header_bg : ''; 
/* Set dynamic color from theme data table */
$textColorCss           = "color:#".$event_theme_text_color." ; font-family : '".$fontName."'";
$headerColorCss         = "background:#".$event_theme_screen_bg." ; font-family : '".$fontName."'";
$primaryColorCss        = "background:#".$primary_color." ; font-family : '".$fontName."'";
$secondaryColorCss      = "background:#".$secondary_color." ; font-family : '".$fontName."'";
$highlightBgCss         = "background:#".$event_theme_link_highlighted_color." ; font-family : '".$fontName."'";
$highlightColorCss      = "color:#".$event_theme_link_highlighted_color." ; font-family : '".$fontName."'";
$headerBgColorCss       = "background-color:#".$event_theme_header_bg;
$highlightBgColorCss    = "background-color:#".$event_theme_link_highlighted_color." ; font-family : '".$fontName."'";
$navigationLinkColor    = "color:#A5A5A8";
/* get header path */
$headerStatus = 1;
if($is_header_or_bgcolor=='header' || $is_header_or_bgcolor=='') {
    if(!empty($header_image_path) && !empty($header_image)) {
        $headerPath     = base_url().$header_image_path.$header_image;
    }else {
        $headerStatus   = 0;
        $headerPath     = base_url().$this->config->item('themes_path').'images/header_banner_dummy.png';
    }
    $headerBgColorCss   = '';
}else {
    $headerPath         = base_url().$this->config->item('themes_path').'images/blank.png';
}

/* Set logo image path */
if(!empty($logo_image_path) && !empty($logo_image)) {
    $logoPath           = base_url().$logo_image_path.$logo_image;
}else {
    $logoPath           = base_url().$this->config->item('themes_path').'images/logo_all_theme.png';
}
//display default logo
$logoDefault            = base_url().$this->config->item('themes_path').'images/logo_all_theme.png';
/* User id */
$userId                 = isLoginUser();
/* form fields */
$formAddEventTheme = array(
            'name'      => 'formAddEventTheme',
            'id'        => 'formAddEventTheme',
            'method'    => 'post',
            'class'     => 'form-horizontal mt10 eventtheme',
            'enctype'   => "multipart/form-data",
);
$themeTypeId            = $this->uri->segment(3);
$openForm               = !empty($themeTypeId) ? 'display:block' : '';
$opened                 = !empty($themeTypeId) ? 'open' : '';
$collapsed              = !empty($themeTypeId) ? '' : 'collapsed';
$themestatusaftersave   = ($this->session->flashdata('themestatus') !='') ? 'in' : '';
?>

<div class="sabPanel overflow_visible mB30 <?php echo $opened; ?>" id="sabPanel0">
   <div class="sabPanel_header customize_header " style="<?php echo $openForm; ?>"> 
       <?php 
        if (strlen($event_theme_name) > 45){
            echo $eventthemename = substr($event_theme_name, 0, 44) . '...';
        }else{
            echo $event_theme_name;
        } 
       ?>
   </div>
   <div class="sabPanel_contentWrapper" style="<?php echo $openForm; ?>">
        <?php echo form_open($this->uri->uri_string(),$formAddEventTheme); ?>
        <div id="theme_contener">
            <div class="theme_wrapper">
                
                <div class="as_theme_box theme_box" id="as_theme_box" style="<?php echo $headerColorCss;?>">
                    <div class="as_theme_head theme_head" id="as_theme_head">
                        <ul class="as_small_nav">
                            <li id="link_home" class="as_small_nav hometxt" style="<?php echo $highlightColorCss;?>"><?php echo lang('link_home'); ?></li>
                            <li class="as_small_nav" style="<?php echo $navigationLinkColor;?>"><?php echo lang('link_manage_registration'); ?></li>
                            <li class="as_small_nav" style="<?php echo $navigationLinkColor;?>"><?php echo lang('link_term_condition'); ?></li>
                            <li class="as_small_nav" style="<?php echo $navigationLinkColor;?>"><?php echo lang('link_client_web'); ?></li>
                            <li class="as_small_nav" style="<?php echo $navigationLinkColor;?>"><?php echo lang('link_contact_us'); ?></li>
                        </ul>
                    </div>
                    <div class="theme_header" style="<?php echo $headerBgColorCss;?>"><!--as_theme_banner theme_banner_Q-->
                        <div class="as_themeBannerImg themebannerimg_wrap_Q  droppable_header">
                            <?php 
                                if($logo_image_status==1){
                                    $image_position_top ='19';
                                    $image_position_left='23';
                                    $image_width ='125';
                                    $image_height='110';
                                    $logoPath = $logoDefault;
                                }
                            ?>
                            <div class="draggableParent">
                                <div id="draggable" class="ui-widget-content makeSizeEvent" style="position:absolute;top:<?php echo $image_position_top; ?>px;left:<?php echo $image_position_left; ?>px;width:<?php echo $image_width; ?>px;height:<?php echo $image_height; ?>px;">
                                    <img src="<?php echo $logoPath; ?>" style="width:100%;height:100%;" id="overlay_img" img_val="0" class="preview_image">
                                    <div style="cursor:pointer;color:#fff;" id="resetlogo">x</div>
                                </div>
                            </div>
                            <div class="boxSep content" >
                                <div class="imgLiquidNoFill imgLiquid frame" >
                                <img id="themeHeaderImage" class="as_themeBannerImg_ themebannerimg_Q header_bg" src="<?php echo $headerPath; ?>" imgname="<?php echo $headerPath; ?>">
                                <!-- Droppable div area D1 -->    
                                </div>
                                <div id='controls'>
                                  <button id='zoom_out'     type='button' title='Zoom out'> - </button>
                                  <!--<button id='fit'          type='button' title='Fit image'> [ ]  </button>-->
                                  <button id='zoom_in'      type='button' title='Zoom in'> + </button>                                  
                                </div>
                            </div>
                            <!-- End D1 -->
                        </div>
                    </div>
                    <div class="beyond_wrapper" style="<?php echo $primaryColorCss;?>">
                        <div class="pull-left bw_left">
                            <h1  style="<?php echo $highlightColorCss;?>"><?php echo $eventdetails->event_title; ?></h1>
                            <h3 style="<?php echo $textColorCss;?>" class="title_sub"><?php echo $eventdetails->subtitle; ?></h3>
                            <span class="title_sub" style="<?php echo $textColorCss;?>">
                                <?php echo $eventdetails->event_venue; ?><br/>
                                <?php echo $eventdetails->eventVenueAddress1; ?><br/>
                                <?php if(!empty($eventdetails->eventVenueZip)){ echo 'Postcode : '.$eventdetails->eventVenueZip.'<br/>';} ?>
                                <?php 
                                $dayS = date('d',strtotime($eventdetails->starttime)); 
                                $dayE = date('d',strtotime($eventdetails->endtime)); 
                                if($dayS == $dayE) {
                                    echo date('jS F Y',strtotime($eventdetails->endtime));
                                }else {
                                echo date('d',strtotime($eventdetails->starttime)); ?> - <?php echo date('jS F Y',strtotime($eventdetails->endtime)); ?>
                                <?php } ?>
                            </span>
                            <p style="<?php echo $textColorCss;?>" class="title_sub">
                                <?php if(!empty($eventdetails->description)){ $dataDescription = make_clickable($eventdetails->description,$textColorCss); echo $dataDescription; } ?> <a style="<?php echo $highlightColorCss;?>" href="javascript:void(0);">
                                <br/><?php //echo lang('newman_events_url_text'); ?><?php //echo strtolower($eventdetails->event_url); ?></a>
                            </p>
                        </div>
                        
                        <div class="pull-right bw_right">
                            <span class="bw_logintitle" style="<?php echo $highlightColorCss;?>">LOG IN</span>
                            <div class="pull-left">
                                <input type="text" placeholder="Email Address" readonly />
                                <input type="password" placeholder="Password" readonly />
                            </div>                            
                            <div class="pull-right">
                                <a href="javascript:void(0);" class="template_login" style="<?php echo $highlightBgColorCss;?>"></a>
                            </div>
                            <div class="clearfix"></div>
                            <a href="javascript:void(0);" class="pull-right" style="<?php echo $highlightColorCss;?>"><?php echo lang('forgot_link');?></a>
                            <div class="clearfix"></div>
                            <p class="loginarea" style="<?php echo $textColorCss;?>">
                                <span class="pull-left"><?php echo lang('invitations_close'); ?></span>
                                <span class="pull-right"><?php if($eventdetails->last_reg_date!='0000-00-00 00:00:00' && !empty($eventdetails->last_reg_date)) { echo date('jS F Y',strtotime($eventdetails->last_reg_date)); }else { echo 'NA';  } ?></span>
                            </p>
                        </div>
                    </div>
                    <!-- Secondary color & Invitation type section -->
                    <?php 
                        $numberOfInvitation = count($eventinvitationstypes);
                        if($numberOfInvitation > 1){
                            $divAlignCls = '';
                        }else {
                            $divAlignCls = 'block-center';
                        }
                    ?>
                    <div class="as_theme_content block_wrapper <?php echo $divAlignCls; ?> bx-bg-color" style="<?php echo $secondaryColorCss;?>">
                        <div class="row-fluid-15 m_btmZero">
                            <?php if(count($eventinvitationstypes) > 0 && !empty($eventinvitationstypes)) { ?>
                                <?php 
                                    if($eventinvitationstypes) {
                                        $countType = 0;
                                        foreach($eventinvitationstypes as $types) { 
                                            $invitation_name = !empty($types->invitation_name) ? $types->invitation_name : ''; 
                                            $invitation_limit = !empty($types->invitation_limit) ? $types->invitation_limit : ''; 
                                            $allow_waiting_list = !empty($types->allow_waiting_list) ? $types->allow_waiting_list : ''; 
                                            if($countType!=0) {
                                                if($countType%2==0){
                                                    $highlightBgCss ='background-color:#777B7A;';
                                                    $textColorCss = 'color:#ffffff';
                                                    $highlighted = '';
                                                }else {
                                                    $highlightBgCss ='background-color:#cccccc;';
                                                    $textColorCss = 'color:#ffffff';
                                                    $highlighted = '';
                                                }
                                            }else {
                                                $highlighted = 'highlighted';
                                            }
                                    ?>
                                     <div class="<?php echo $highlighted; ?> as_block hltd-colr-box"  style="<?php echo $highlightBgCss;?>">
                                        <span class="block_count"><?php if(!empty($invitation_limit)) { echo $invitation_limit; }else{ echo '0'; } ?></span>
                                        <img src="<?php echo IMAGE; ?>sm3.png">
                                        <p class="pFive bx-txt-clor"  style="<?php echo $textColorCss;?>">
                                        <?php echo strtoupper($invitation_name); ?>&nbsp;<?php //echo strtoupper(lang('register_here')); ?></p>
                                    </div>
                                <?php
                                    $countType++;
                                    } //foreach end
                                 } //end if ?>    
                            <?php } else { ?>
                            <div class="highlighted as_block hltd-colr-box"  style="<?php echo $highlightBgCss;?>">
                                <span class="block_count"><?php if(!empty($eventdetails->number_of_rego)) { echo $eventdetails->number_of_rego; }else{ echo '0'; } ?></span>
                                <img src="<?php echo IMAGE; ?>sm3.png">
                                <p class="pFive bx-txt-clor"  style="<?php echo $textColorCss;?>">
                                <?php echo strtoupper(lang('register_here')); ?></p>
                            </div>
                            <?php } ?>
                        </div>  
                        <div class="row-fluid-15 m_btmZero">
                            
                        </div>                                               
                    </div>
                     <!-- End secondary section -->
                    <div class="themeSponsorsLogos">
						
                        <div id="canvasRow" style="solid;width:596px;height:153px;margin-top:0px;">
                            <div id="canvasPopover"></div>
                            <div id="statusBar"></div>
								<div class="arrow-down tip mT30" id="dragTip">
									<h2>IMAGE GALLERY</h2>
									<p>Add as many photos or logos <br>as you need.</p>
								</div>
                            <div id="canvasContainer">
                                <canvas id="canvas" width="596" height="153"></canvas>
                            </div>
						</div>
                    </div>
                   
                    <div class="as_themefooter" style="<?php echo $headerColorCss; ?>">
                        <img src="<?php echo IMAGE; ?>footer_txt.png">
                    <ul class="as_small_social">
                        <li class="as_fb"><img src="<?php echo IMAGE; ?>fb.png"></li> 
                        <li class="as_twt"><img src="<?php echo IMAGE; ?>twt.png"></li>
                        <li class="as_lin"><img src="<?php echo IMAGE; ?>lin.png"></li>
                        <li class="like_youtube"><img src="<?php echo IMAGE; ?>youtube.png"></li>  
                        <li class="like_gplus"><img src="<?php echo IMAGE; ?>g+.png"></li>
                        <li class="like_pinterset"><img src="<?php echo IMAGE; ?>pinterest.png"></li>                                                      
                        <li class="like_instagram"><img src="<?php echo IMAGE; ?>instagram.png"></li>                                                      
                    </ul>
                    </div>
                </div>
                
                
                <div class="">
                        <div class="pull-right">
                            <input type="hidden" class="image_position_top" name="image_position_top" value="<?php echo $image_position_top; ?>">
                            <input type="hidden" class="image_position_left" name="image_position_left" value="<?php echo $image_position_left;?>">
                            <input type="hidden" class="image_width" name="image_width" value="<?php echo $image_width; ?>">
                            <input type="hidden" class="image_height" name="image_height" value="<?php echo $image_height; ?>">
                            <input type="hidden" name="gallery_action" class="gallery_action" value="">
                            <input type="hidden" name="header_gallery_image" class="header_gallery_image" value="">
                            <input type="hidden" name="logo_gallery_image" class="logo_gallery_image" value="">
                            <input type="hidden" name="action_post" value="header">
                            <input type="hidden" id="default_header" value="<?php echo $headerPath; ?>">
                            <?php echo form_hidden('eventId', $eventId); ?>
                            <input type="hidden" name="event_theme_name" id="theme_name" value="<?php echo $event_theme_name; ?>">
                            <input type="hidden" name="data_id" id="data_id" value="<?php echo $data_id; ?>">
                            <input type="hidden" name="theme_type" id="theme_type" value="<?php echo $theme_type; ?>">
                            <input type="hidden" id="sponsor_img" name="sponsor_img">
                            <input type="hidden" id="sponsor_canvas_data" name="sponsor_canvas_data">
                            <input type="hidden" id="is_header_or_bgcolor" name="is_header_or_bgcolor" value="header">                    
                            <input type="hidden" id="is_theme_used_data" name="is_theme_used_data" value="0">
                            <input type="hidden" id="logo_image_status" name="logo_image_status" value="0">
                            <input type="hidden" id="is_theme_"  value="1">
                            <input type="hidden" id="font_package_value" name="font_package_value"  value="">
                            <!-- Header image data -->
                            <input type="hidden" id="x" name="x_axis" value="<?php echo $x_axis; ?>">
                            <input type="hidden" id="y" name="y_axis" value="<?php echo $y_axis; ?>">
                            <input type="hidden" id="scale" name="scale" value="<?php echo $scale; ?>">
                            <!-- Set header plugin crop image data -->
                            <input type="hidden" id="baseimage" name="baseimage" value="">    
                                        
                                
                        </div>
                    </div> 
                </div>
                <div class="panel_footer">
                <div class="pull-right">
                    <button class="btn-normal btn customize_theme_popup ischanged" onclick="ZM.canvasSaveJSON();"  is_theme_used_data="0" type="button" name="save" eventid="<?php echo $eventId; ?>" theme_name="<?php echo $event_theme_name; ?>" theme_type="<?php echo $theme_type; ?>"><?php echo lang('comm_save_changes'); ?></button>  

                    <button class="btn-normal btn reset_theme_data " type="button" name="form_reset"><?php echo lang('reset_btn_title'); ?></button>

                    <button  onclick="ZM.canvasSaveJSON();" theme_type="<?php echo $theme_type; ?>"  theme_name="<?php echo $event_theme_name; ?>" is_theme_used_data="1" class="btn-normal btn customize_theme_popup ischanged <?php echo ($event_theme_used==1) ? 'btn-orange' : ''; ?>"  <?php echo ($event_theme_used==1) ? 'disabled="disabled"' : '' ?> type="button" name="use"  eventid="<?php echo $eventId; ?>" ><?php echo ($event_theme_used==0) ? lang('use_btn_title') : lang('used_btn_title');  ?></button>
                </div>
                </div>
                <!-- theme wrapper -->
            </div>
        </div>
         <!-- Theme pallet dev area -->
         <div class="theme_editor_panel_as" style="display:block;">                                
            <div class="optionDiv_as">
                <ul class="primaryOption_as">
                    <li><a class="trg xsmall" href="javascript:void(0)"><span class="ico">
                        <img src="<?php echo IMAGE; ?>mg1.png"></span><span><?php echo lang('header_image_title'); ?></span></a>
                        <div class="optionContent_as">
                            <h4 class="small customHead_as text-left"><?php echo lang('create_header_image_title'); ?></h4>
                            <div class="optionContentMain_as">
                                <!--<div class="mainImg"><img src="images/demo.png"></div>-->
                                <div class="centerBlock">
                                    <div class="center">
                                        <div class="custom-upload">
                                            <span class="label_text"><?php echo lang('event_term_term_select_or_drop_file'); ?></span>
                                            <input type="file" class="customIinputFileHeader" accept="image/*" id="customfileheader" name="headerImage" draggable="true">
                                            <div class="display_file">
                                                <span class="file_value"></span>
                                                <a href="javascript:void(0)" class="clear_value"></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <span class="medium or_d"><?php echo lang('or_title');?></span>
                                    <button class="centerFloat btn medium theme_popup_trg selectFormGallery"  gallery-action="header" type="button" name="form_reset"><?php echo lang('select_from_gallery');?></button>
                                    <div class="clearfix"></div>
                                    <span class="medium or_d"><?php echo lang('or_title');?></span>
                                    <div class="center mtop10tab">
                                        <input type="text" id="colour_header" name="colour_header" class="small color short_field" value="fd8204"/>
                                    </div>
                                    <div class="clearfix"></div>
                                    
                                    <span class="optioHeadr small"><?php echo lang('select_solid_color');?></span>
                                </div>
                            </div>
                            <div class="optionFooter_as">
<!--
                                <button class="default_btn btn pull-right medium customize_theme_popup invisible" is_theme_used="0" type="button" name="save" themeid="1" eventid="177" theme_name="Theme Option 1"><?php echo lang('save_title');?></button>
-->
                            </div>
                        </div>
                    </li>
                    <li>
                        <a class="trg xsmall" href="javascript:void(0)"><span class="ico">
                            <img src="<?php echo IMAGE; ?>mg2.png"></span><span><?php echo lang('overlay_image');?></span></a>
                        <div class="optionContent_as">
                            <h4 class="small customHead_as text-left"><?php echo lang('create_overlay_image');?></h4>
                            <div class="optionContentMain_as">
                                <!--<div class="mainImg"><img src="images/demo.png"></div>-->
                                <div class="centerBlock">
                                    <div class="center">
                                        <div class="custom-upload">
                                            <span class="label_text">Select or Drop File</span>
                                            <input type="file" class="customIinputFileLogo" id="custominputlogo" accept="image/*" name="logoImage"  draggable="true">
                                            <div class="display_file">
                                                <span class="file_value"></span>
                                                <a href="javascript:void(0)" class="clear_value"></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <span class="medium or_d"><?php echo lang('or_title');?></span>
                                    <button class="centerFloat btn medium  theme_popup_trg selectFormGallery" gallery-action="logo" type="button" name="form_reset"><?php echo lang('select_from_gallery');?></button>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="optionFooter_as">
<!--
                                <button class="default_btn btn pull-right medium customize_theme_popup invisible" is_theme_used="0" type="button" name="save" themeid="1" eventid="177" theme_name="Theme Option 1"><?php echo lang('save_title');?></button>
-->
                            </div>
                        </div>
                    </li>
                    <li>
                        <a class="trg xsmall" href="javascript:void(0)"><span class="ico">
                            <img src="<?php echo IMAGE; ?>mg3.png"></span><span><?php echo lang('image_gallery');?></span></a>
                        <div class="optionContent_as">
                            <h4 class="small customHead_as text-left"><?php echo lang('create_image_gallery');?></h4>
                            <div class="optionContentMain_as">
                                <!--<div class="mainImg"><img src="images/demo.png"></div>-->
                                <div class="centerBlock">
                                    <div class="center">
                                        <div class="custom-upload">
                                            <span class="label_text"><?php echo lang('event_term_term_select_or_drop_file');?></span>
                                            <input id="imgLoader" accept="image/*" multiple  type="file" name="attach_doc"  draggable="true">
                                            <div class="display_file">
                                                <span class="file_value"></span>
                                                <a href="javascript:void(0)" class="clear_value"></a>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="clearfix"></div>

                                    <span class="medium or_d"><?php echo lang('or_title');?></span>
                                    <button class="centerFloat btn medium  theme_popup_trg selectFormGallery" type="button" name="form_reset"><?php echo lang('select_from_gallery');?></button>
                                    <div class="clearfix"></div>
                                    <div class="clearfix"></div>

                                    
                                </div>
                            </div>
                            <div class="optionFooter_as">
<!--
                                <button class="default_btn btn pull-right medium customize_theme_popup invisible" is_theme_used="0" type="button" name="save" themeid="1" eventid="177" theme_name="Theme Option 1"><?php echo lang('save_title');?></button>
-->
                            </div>
                        </div>                                            
                    </li>
                    <li>
                        <a class="trg xsmall" href="javascript:void(0)"><span class="ico">
                        <img src="<?php echo IMAGE; ?>mg4.png"></span><span><?php echo lang('font_title');?></span></a>
                        <div class="optionContent_as">
                            <h4 class="small customHead_as text-left"><?php echo lang('select_font_title');?></h4>
                            <div class="optionContentMain_as">
                                
                                <div class="centerBlock">
                                    <?php echo form_dropdown('font_package', $fontPackagesOptions, $fontPackageVal, $fontPackagesOtherOptions); ?>
                                </div>
                                
                            </div>
                            <div class="optionFooter_as">
<!--
                                <button class="default_btn btn pull-right medium customize_theme_popup invisible" is_theme_used="0" type="button" name="save" themeid="1" eventid="177" theme_name="Theme Option 1"><?php echo lang('save_title');?></button>
-->
                            </div>
                        </div>                                            
                    </li>
                    <li>
                        <a class="trg xsmall" href="javascript:void(0)">
                            <span class="ico">
                                <img src="<?php echo IMAGE; ?>mg6.png"></span>
                            <span>Colours</span></a>
                        <div class="optionContent_as colorOption">                                                	
                            <div class="optionContentMain_as">
                                <!--<div class="mainImg"><img src="images/demo.png"></div>-->
                                <div class="center">
                                    <input type="text" id="event_theme_text_color" name="event_theme_text_color" class="small color short_field" value="<?php echo $event_theme_text_color;?>"/>
                                </div>
                                <div class="clearfix"></div>
                                <span class="optioHeadr xsmall"><?php echo lang('text_title');?></span>
                                
                                <div class="center">
                                    <input type="text" id="event_theme_screen_bg" name="event_theme_screen_bg" class="small color short_field" value="<?php echo $event_theme_screen_bg;?>"/>
                                </div>
                                <div class="clearfix"></div>
                                <span class="optioHeadr xsmall"><?php echo lang('event_theme_screen_bg');?></span>
                                <div class="center">
                                    <input type="text" id="primary_color" name="primary_color" class="small color short_field" value="<?php echo $primary_color;?>"/>
                                </div>
                                <div class="clearfix"></div>
                                <span class="optioHeadr xsmall"><?php echo lang('event_theme_header_bg');?></span>
                                <div class="center">
                                    <input type="text" id="secondary_color" name="secondary_color" class="small color short_field" value="<?php echo $secondary_color;?>"/>
                                </div>
                                <div class="clearfix"></div>
                                <span class="optioHeadr xsmall"><?php echo lang('event_theme_box_bg');?></span>
                                <div class="center">
                                    <input type="text" id="event_theme_link_highlighted_color" name="event_theme_link_highlighted_color" class="small color short_field" value="<?php echo $event_theme_link_highlighted_color;?>"/>
                                </div>
                                <div class="clearfix"></div>
                                <span class="optioHeadr xsmall"><?php echo lang('event_theme_link_highlight_color');?></span>
                            </div>
                        </div>                                            
                    </li>
                </ul>    
             
              </div> 
            </div>   
              <!-- End pallet area --> 
        
        
        <?php echo form_close(); ?>
        </div>
      
   
<script src="<?php echo base_url('themes/assets/js/zoom.js') ?>" type="text/javascript"></script>
<script src="<?php echo base_url('themes/assets/js/zoom.min.js') ?>" type="text/javascript"></script>
<script src="<?php echo base_url('themes/assets/js/jquery.guillotine.js') ?>" type="text/javascript"></script>

<script>
    
    setTimeout(function(e){ ZM.init();},100);
	setTimeout(function(e){ 
            var clg ='<?php echo trim($sponsor_image_object); ?>';
            ZM.doLoadJSON(clg); 
        },300);
    
	$(function() {
		$('#event_theme_text_color').on('change',function(){
			var text_color = $(this).val();
			//$('.as_small_nav li').css('color','#'+text_color);
			$('.title_sub').css('color','#'+text_color);
			$('.loginarea').css('color','#'+text_color);
			$('.pFive').css('color','#'+text_color);
		});
		$('#event_theme_screen_bg').on('change',function(){
			var screen_bg = $(this).val();
			$('#as_theme_box').css('background','#'+screen_bg);
			$('.as_themefooter').css('background','#'+screen_bg);
		});
		$('#primary_color').on('change',function(){
			var primary_color = $(this).val();
			$('.beyond_wrapper').css('background','#'+primary_color);
		});
		$('#secondary_color').on('change',function(){
			var secondary_color = $(this).val();
			$('.bx-bg-color').css('background','#'+secondary_color);
		});
		$('#event_theme_link_highlighted_color').on('change',function(){
			var event_theme_link_highlighted_color = $(this).val();
			$('.highlighted').css('background','#'+event_theme_link_highlighted_color);
			$('#link_home').css('color','#'+event_theme_link_highlighted_color);
			$('.bw_left h1').css('color','#'+event_theme_link_highlighted_color);
			$('.bw_logintitle').css('color','#'+event_theme_link_highlighted_color);
			$('.beyond_wrapper a').css('color','#'+event_theme_link_highlighted_color);
			$('.template_login').css('background-color','#'+event_theme_link_highlighted_color);
		});
	});

  $(function() {
    $( "#draggable" ).draggable({containment: "parent"});
    $( "#draggable" ).resizable({
      resize: function( event, ui ) {
          $('.image_width').val(ui.size.width);
          $('.image_height').val(ui.size.height);
          if(ui.size.width > 290 || ui.size.height > 190) {
            $(this).resizable('widget').trigger('mouseup');
          }
        }
    });
  });
  
$(function() {
$('.droppable_header').droppable({
    drop: function(event, ui)
    { 
        var pos = ui.draggable.position();
        $('.image_position_top').val(pos.top);
        $('.image_position_left').val(pos.left);
        //console.log(pos);
    }
});

//call input file on change overlay
$(".customIinputFileLogo").change(function(){
   var filename = $(this).val();
   var mysize = this.files[0].size;
   var file_status = checkFileSizeOrType(filename,mysize);
   if(!file_status) {
    return false;
   } 
   readImageURL(this,'logo');
});

function checkFileSizeOrType(filename,mysize) {
    var fileStatus = true;
    var error_file_msg = ''; 
    var ext = filename.split('.').pop();
    var allowed = ['jpg','JPG','jpeg','JPEG','png','PNG','gif','GIF'];
    if ($.inArray(ext,allowed) == -1) {
        fileStatus = false;
        error_file_msg = 'Invalid file type.  Only jpg, gif, and png files area allowed at this time.<br/>';
        
    } 
    if(mysize > 5242890)
    {
        fileStatus = false;
        error_file_msg +='File too large, please upload file upto 5MB.';
    }
    if(!fileStatus) {
        custom_popup(error_file_msg,false,false);
        return fileStatus;
    }else {
        return fileStatus;
    }
}
//header image
$(".customIinputFileHeader").change(function(){
   var filename = $(this).val();
   var mysize = this.files[0].size;
   var file_status = checkFileSizeOrType(filename,mysize);
   if(!file_status) {
    return false;
   }
   //Read Image if status is true
   readImageURL(this,'header');
   setTimeout(function(e){
    zoomimage(); 
   },2000);
});
//reset image
 $(".reset_image").click(function(){
   $('#overlay_img').attr('img_val', 0);
});
//reset image logo
$("#resetlogo").click(function(){
   $('#overlay_img').attr('img_val', 0);
   var logoImgPath= "<?php echo $logoDefault; ?>";
   //var status = confirm("Are you sure you don't want to overlay image in theme!");
   var getThemeType = '<?php echo $theme_type; ?>';
  
    $('#overlay_img').attr('src', logoImgPath);
    //empty files data
    document.getElementById("custominputlogo").value = "";
    $('.logo_gallery_image').val('');
    $('#logo_image_status').val(1);
   
});

});
  
function readImageURL(input,img_type) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        var image  = new Image();
        reader.onload = function (e) {
            image.src    = e.target.result;             
			image.onload = function() {
				var w = this.width;
				var	h = this.height;
                var dif;
                if(img_type=='logo') {
                    
                    if( w > h ) {
                       dif = parseInt(w)/parseInt(h);
                       dif = parseInt(dif);
                       
                       if(dif <= 1) new_w = '160';
                       if(dif > 1 && dif < 2) new_w = '200';
                       if(dif > 2 && dif <= 3) new_w = '220';
                       if(dif >= 3) new_w = '245';
                       $('.makeSizeEvent').css('width',new_w+'px'); 
                       $('.makeSizeEvent').css('height','110px');
                       $('.image_width').val(new_w);
                       $('.image_height').val('110');
                    }else if( h > w ) {
                       dif = parseInt(h)/parseInt(w);
                       if(dif <= 1) new_h = '140';
                       if(dif > 1 && dif < 2) new_h = '160';
                       if(dif > 2 && dif < 3) new_h = '180';
                       if(dif >= 3) new_h = '180';
                       $('.makeSizeEvent').css('width','130px'); 
                       $('.makeSizeEvent').css('height',new_h+'px');
                       $('.image_width').val('125');
                       $('.image_height').val(new_h);
                    }else {
                       $('.makeSizeEvent').css('width','125px'); 
                       $('.makeSizeEvent').css('height','110px');
                    }
                    
                }        
            };        
                    
            if(img_type==='header'){
                //empty gallery image
                $('.header_gallery_image').val('');
                //set image source
                $('.header_bg').attr('src', e.target.result);                
                //get image
                //getCorrectImage();                  
                $('#is_header_or_bgcolor').val('header');
                $(".theme_header ").removeAttr("style");
            }else{
                $('.logo_gallery_image').val('');
                $('#overlay_img').attr('src', e.target.result);
                $('#overlay_img').attr('img_val', 1);
                $('#logo_image_status').val(0);
            }
        }
        reader.readAsDataURL(input.files[0]);
    }
}

</script>
<script>
   
$("#colour_header").change(function(){
    var color_header_bg = $(this).val();
    $('.theme_header').css('background-color','#'+color_header_bg);
    $('#themeHeaderImage').attr('src', '');
    $(".imgLiquid").css("background", "");
    $('#is_header_or_bgcolor').val('color');
}); 
// Change Font Style
$("#font_package").on('change',function(){
	var text_font_style = $('#font_package option:selected').text();
	var customSelector = '.theme_wrapper h1,.theme_wrapper h2,'+
						 '.theme_wrapper h3,.theme_wrapper h4,.theme_wrapper li,.theme_wrapper ul,'+
						 '.theme_wrapper h5,.theme_wrapper h6,.theme_wrapper input,'+
						 '.theme_wrapper p,.theme_wrapper div,.theme_wrapper span,.bw_right, .bw_right a';
	
	$(customSelector).css('font-family','"'+text_font_style+'"');
})

$(".theme_popup_trg").click(function(){
    //$('.media_gallery').show();
    var galleryAction = $(this).attr('gallery-action');
    $('.gallery_action').val(galleryAction);
	$('#theme_popup').modal({ keyboard: false});
});

$("#imgInp").change(function(){
    $('.gallery_action').val('');
    $('.header_gallery_image').val('');
});
    


	// Reset Theme options
	$(".reset_theme_data").on('click',function(){
			// Text color default
			var text_color = '<?php echo $event_theme_text_color; ?>';
			$('.hometxt').css('color','#'+text_color);
			$('.title_sub').css('color','#'+text_color);
			$('.bw_logintitle').css('color','#'+text_color);
			$('.loginarea').css('color','#'+text_color);
			$('.pFive').css('color','#'+text_color);
			
			// Screen Background color
			var screen_bg = '<?php echo $event_theme_screen_bg; ?>';
			$('#as_theme_box').css('background','#'+screen_bg);
			
			// Primary Color
			var primary_color = '<?php echo $primary_color; ?>';
			$('.beyond_wrapper').css('background','#'+primary_color);
			// Secondary Color
			var secondary_color = '<?php echo $secondary_color; ?>';
			$('.bx-bg-color').css('background','#'+secondary_color);
			// Highlighted Color
			var event_theme_link_highlighted_color = '<?php echo $event_theme_link_highlighted_color; ?>';
			$('.bw_logintitle').css('color','#'+event_theme_link_highlighted_color);
			$('#link_home').css('color','#'+event_theme_link_highlighted_color);
			$('.beyond_wrapper a').css('color','#'+event_theme_link_highlighted_color);
			$('.bw_left h1').css('color','#'+event_theme_link_highlighted_color);
			var event_theme_link_highlighted_color_bg = '<?php echo $event_theme_link_highlighted_color; ?>';
			$('.highlighted').css('background','#'+event_theme_link_highlighted_color_bg);
			$('.template_login').css('background-color','#'+event_theme_link_highlighted_color_bg);
			
			var headerPath = "<?php echo $headerPath; ?>";
			$('.theme_header').css('background-color','');
			
            $('#themeHeaderImage').attr('src', headerPath);
            $('.imgLiquid').css('background-image', 'url(' + headerPath + ')');
            
            //empty files data
            document.getElementById("customfileheader").value = "";
            document.getElementById("custominputlogo").value = "";
            //file reset 
            //getCorrectImage();   
            //end file code
			var logoPath= "<?php echo $logoPath; ?>";
			$('#overlay_img').attr('src', logoPath);
			var text_font_style = "<?php echo $fontName;?>";
			var customSelector = '.theme_wrapper h1,.theme_wrapper h2,'+
								 '.theme_wrapper h3,.theme_wrapper h4,.theme_wrapper li,.theme_wrapper ul,'+
								 '.theme_wrapper h5,.theme_wrapper h6,.theme_wrapper input,'+
								 '.theme_wrapper p,.theme_wrapper div,.theme_wrapper span';
			
			$(customSelector).css('font-family','"'+text_font_style+'"');
			
			var event_theme_text_color = "#<?php echo $event_theme_text_color ?>";
			
			$("#event_theme_text_color").val(event_theme_text_color);
			$("#event_theme_text_color").css('background-color',event_theme_text_color);	
			
			var event_theme_screen_bg = "#<?php echo $event_theme_screen_bg ?>";
			$("#event_theme_screen_bg").val(event_theme_screen_bg);
			$("#event_theme_screen_bg").css('background-color',event_theme_screen_bg);	
			
			var primary_color = "#<?php echo $primary_color ?>";
			$("#primary_color").val(primary_color);
			$("#primary_color").css('background-color',primary_color);
			
			var secondary_color = "#<?php echo $secondary_color ?>";
			$("#secondary_color").val(secondary_color);
			$("#secondary_color").css('background-color',secondary_color);
			
			var event_theme_link_highlighted_color = "#<?php echo $event_theme_link_highlighted_color ?>";
			$("#event_theme_link_highlighted_color").val(event_theme_link_highlighted_color);
			$("#event_theme_link_highlighted_color").css('background-color',event_theme_link_highlighted_color);	
	})
	
    $(".selectFormGallery").on('click',function(){
	  var scrolled_val = window.pageYOffset;
	  if( $("#theme_popup").hasClass("in") ){
	  }else{  
		$("#theme_popup").css("top", scrolled_val );
	  }
	});

//check input changes
var FORM_HAS_CHANGED = false;

$("input, select").change(function(){
    FORM_HAS_CHANGED = true;
});

$('.ischanged').click(function() {
    if (FORM_HAS_CHANGED) {
        setTimeout(function(e){ $('#thememessage').hide();},500);  
    } else {
        var themeType = '<?php echo $theme_type; ?>';
		 if(themeType=='2'){
            setTimeout(function(e){ $('#thememessage').show();},1000);  
        }
	}
});



</script>
<script type='text/javascript'>
 var themeType    = '<?php echo $theme_type; ?>';   
 var headerStatus = '<?php echo $headerStatus; ?>';   
 if( ( themeType == 1 ) && ( headerStatus == 1 ) ){
     setTimeout(function(e){
        zoomimage();   
     },4000);     
 }
 function zoomimage() {
      var picture = $('#themeHeaderImage');
      var ext = $('#themeHeaderImage').attr('imgname');
      var extension = ext.substring(ext.lastIndexOf('.')+1);
       picture.guillotine({
          width: 605,
          height: 205,
          init: { angle: 0, x: <?php echo $x_axis; ?>, y: <?php echo $y_axis; ?>,scale: <?php echo $scale; ?> }
        });  
      // Make sure the image is completely loaded before calling the plugin
      picture.one('load', function(){
        // Initialize plugin (with custom event)
        picture.guillotine({eventOnChange: 'guillotinechange'});

        // Display inital data
        var data = picture.guillotine('getData');
        for(var key in data) { $('#'+key).html(data[key]); }
        getImageData(extension); 
        // Bind button actions
        $('#rotate_left').click(function(){ picture.guillotine('rotateLeft'); getImageData(extension); });
        $('#rotate_right').click(function(){ picture.guillotine('rotateRight'); getImageData(extension); });
        $('#fit').click(function(){ picture.guillotine('fit'); getImageData(extension); });
        $('#zoom_in').click(function(){
            var data = picture.guillotine('getData');
			for(var key in data) { $('#'+key).val(data[key]); }
            picture.guillotine('zoomIn'); 
            getImageData(extension);
        });
        $('#zoom_out').click(function(){
            var data = picture.guillotine('getData');
			for(var key in data) { $('#'+key).val(data[key]); }
            picture.guillotine('zoomOut'); 
            getImageData(extension);
        });
      });
      
      picture.on('click', function() { 
			var data = picture.guillotine('getData');
			for(var key in data) { $('#'+key).val(data[key]); }
            getImageData(extension);
		});
        
      // Make sure the 'load' event is triggered at least once (for cached images)
      if (picture.prop('complete')) picture.trigger('load');
      
    }    
</script>
<script>
<?php if($themestatusaftersave) { ?>
$("document").ready(function() {
    setTimeout(function() {    
        //window.location.hash = '#FourthForm';
   },3000);
});
<?php } ?>

/* Show & Hide popup onclick of theme pallet options */
$(".trg").click(function() {	
	if( $(this).hasClass("effect") ){
		$(".trg").removeClass("effect");	
		//$(this).addClass("effect");	
	}else{
		$(".trg").removeClass("effect");	
		$(this).addClass("effect");	
	}
});	
/* Hide popup on ESCAPE key pressed */
$(document).keydown(function(e) {
    if (e.keyCode == 27) {
       $("a.effect").removeClass("effect");
    }
});

$(document).click(function(e){
    if($(e.target).is('.page_content')) {
		$(".trg").removeClass("effect");
	}
});
<?php if($opened!='') {  ?>
$('.theme_editor_panel_as').show();
<?php }else { ?>
$('.theme_editor_panel_as').hide();
<?php } ?>    
        

$('.customize_header').click(function(){
    $('.theme_editor_panel_as').toggle('slow');
});

$("#font_package").change(function() { 
    $('#font_package_value').val($(this).val());
});

function getImageData(extension) {
    var imgtype  = extension;
    if(imgtype!=""){
        var left     =  $(".guillotine-canvas").position().left;
        var top      =  $(".guillotine-canvas").position().top;
        var width    =  $(".guillotine-canvas").width();
        var height   =  $(".guillotine-canvas").height();
        var canvas, canvasContext;
        canvas = $("<canvas />").attr({
        width: '605',
        height: '205'
        }).get(0);
        var img      = document.getElementById("themeHeaderImage");
        canvasContext = canvas.getContext("2d");
        canvasContext.drawImage(img,left,top,width,height);
        imageData =  canvas.toDataURL(imgtype, 1.0);
        $('#baseimage').val(imageData);
    }
}

</script>
