<?php if (!defined('BASEPATH')) { exit('No direct script access allowed'); }
$show_th ='';
if(!empty($_GET['t']) && $_GET['t']==1){ $show_th ='in'; }            
/* define current and next form div id */
$FormDivId = 'FourthForm';
$nextFormDivId = 'FourthForm';
?> 

<div class="panel overflow_visible" id="panel0">
<!-- Panel header -->
 <div class="panel_header rounded20alt">
   <span class="pull-left">Create Your Invitation</span> 
   <select class="selectevent custom-select medium_select pull-right" id="loadselectedtheme">
      <option selected="selected" value="0"><?php echo lang('select_form'); ?></option>
				<?php 
                $openTheme = !empty($singleThemeData[0]->id) ? $singleThemeData[0]->id : '';
                $themetype = $this->uri->segment(4);
				if(!empty($allSavedThemes)){
					foreach($allSavedThemes as $usedtheme){
                        $parmIdEncode = encode($usedtheme->id);
                        $uri = $this->uri->segment(3);
                        if($openTheme==$usedtheme->id) {
                            $selected = 'selected';
                        }else {
                            $selected = (decode($uri)==decode($parmIdEncode) && ($themetype==1) ) ? 'selected' : '';
                        }
                        if (strlen($usedtheme->event_theme_name) > 25){
                            $usedthemename = substr($usedtheme->event_theme_name, 0, 25) . '...';
                        }else{
                            $usedthemename = $usedtheme->event_theme_name;
                        } 
						echo '<option value="'.$parmIdEncode.'" '.$selected.' theme_type="1">'.$usedthemename.'</option>';
					}
				}
				if(!empty($allDefaultThemes)){
					foreach($allDefaultThemes as $usedtheme){
                        $parmIdEncode = encode($usedtheme->id);
                        $uri = $this->uri->segment(3);
                         if($openTheme==$usedtheme->id) {
                            $selected = 'selected';
                        }else {
                            $selected = (decode($uri)==decode($parmIdEncode) && ($themetype==2)) ? 'selected' : '';
                        }
                        
                        if (strlen($usedtheme->event_theme_name) > 25){
                            $usedthemename = substr($usedtheme->event_theme_name, 0, 25) . '...';
                        }else{
                            $usedthemename = $usedtheme->event_theme_name;
                        } 
                        
						echo '<option value="'.$parmIdEncode.'" '.$selected.' theme_type="2">'.$usedthemename.'</option>';
					}
				}
				?>
   </select>
</div>

 <div class="panel_contentWrapper overflow_visible ">
        <?php  
        if(!empty($singleThemeData)){
            $data1['loadSelectedTheme'] = $loadSelectedTheme;
            $data1['customizeformsdata'] = $singleThemeData;
            if(count($eventinvitations) > 0 && !empty($eventinvitations)) {
                $data1['eventinvitationstypes'] = $eventinvitations;
            }else{
                $data1['eventinvitationstypes'] = '';
            }
            $this->load->view('form_event_themes',$data1);
        }
        ?>
    </div>
</div>



<!--Sample panel-->
<!-- Theme default div -->
<?php $FormDivId    = 'FifthForm';
$nextFormDivId      = 'FifthForm'; ?>       
<div class="panel overflow_visible" id="panel1">
    <div class="panel_header rounded20alt">
       <span class="pull-left">Invitation Sample Gallery<?php //echo lang('form_sample_gallery'); ?></span>     
    </div>  
    <div class="panel_contentWrapper overflow_visible">
        <?php  
        for( $sample_theme = 1 ; $sample_theme <= 6 ; $sample_theme++ ) {
            $data['sample_theme_number'] = $sample_theme;
            $this->load->view('form_sample_event_themes',$data);
        }
        ?>
    </div>
</div>

  


<!--end of panel-->
<script>
    /* saved =1 default =2 */
	var baseUrl = '<?php echo base_url(); ?>';
    $('#loadselectedtheme').on('change',function(){
			var themeId = $(this).val();
            var theme_type = $('option:selected', this).attr('theme_type');
            
			if((themeId === 0 )  || ( themeId === '' ) || ( themeId===undefined )) {
                return false;
            }else if((theme_type === 0 ) || (theme_type === '') || (theme_type===undefined)){
                return false;
            }else {
                
                window.location.href=baseUrl+"event/customizeforms/"+themeId+"/"+theme_type;
            }
	});
    
    $('.selectevent').on('click',function(){
        event.preventDefault();
        event.stopPropagation();
    });
</script>
