<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
//define current and next form div id
$FormDivId = 'FourthForm';
$nextFormDivId = 'FourthForm';

$customizeformsdata = $customizeformsdata[0];
$colourSchemeVal = (!empty($customizeformsdata->colour_scheme)) ? $customizeformsdata->colour_scheme : 0;
$backgroundColourVal = (!empty($customizeformsdata->background_colour)) ? $customizeformsdata->background_colour : '00609c';
$mainColourVal = (!empty($customizeformsdata->main_colour)) ? $customizeformsdata->main_colour : '151d29';
$highlightColourVal = (!empty($customizeformsdata->highlight_colour)) ? $customizeformsdata->highlight_colour : 'fd8204';
$linkColourVal = (!empty($customizeformsdata->link_color)) ? $customizeformsdata->link_color : 'fd8204';
$fontPackageVal = (!empty($customizeformsdata->font_package)) ? $customizeformsdata->font_package : '';

$formColourTheme = array(
    'name' => 'formColourTheme',
    'id' => 'formColourTheme',
    'method' => 'post',
    'class' => 'form-horizontal',
    'data-parsley-validate' => '',
);

$colourSchemeDark = array(
    'name' => 'colour_scheme',
    'id' => 'colour_schemeDark',
    'type' => 'radio',
);
$colourSchemeLight = array(
    'name' => 'colour_scheme',
    'id' => 'colour_schemeLight',
    'type' => 'radio',
);

$backgroundColour = array(
    'name' => 'background_colour',
    'value' => $backgroundColourVal,
    'id' => 'background_colour',
    'type' => 'text',
    'required' => '',
    'class' => 'small color short_field',
);
$mainColour = array(
    'name' => 'main_colour',
    'value' => $mainColourVal,
    'id' => 'main_colour',
    'type' => 'text',
    'required' => '',
    'class' => 'small color short_field',
);
$highlightColour = array(
    'name' => 'highlight_colour',
    'value' => $highlightColourVal,
    'id' => 'highlight_colour',
    'type' => 'text',
    'required' => '',
    'class' => 'small color short_field',
);
$linkColour = array(
    'name' => 'link_colour',
    'value' => $linkColourVal,
    'id' => 'link_colour',
    'type' => 'text',
    'required' => '',
    'class' => 'small color short_field',
);


//get previous themes
$previousThemesoptions[""] = 'Select';
if (!empty($previousthemes)) {
    foreach ($previousthemes as $previoustheme) {
        $background_colour = ($previoustheme->background_colour ? $previoustheme->background_colour : 0);
        $main_colour = ($previoustheme->main_colour ? $previoustheme->main_colour : 0);
        $highlight_colour = ($previoustheme->highlight_colour ? $previoustheme->highlight_colour : 0);
        $link_color = ($previoustheme->link_color ? $previoustheme->link_color : 0);
        $font_package = ($previoustheme->font_package ? $previoustheme->font_package : 0);
        $colour_scheme = ($previoustheme->colour_scheme ? $previoustheme->colour_scheme : 0);
        $previousThemesoptions[$previoustheme->id . '##' . $background_colour . '##' . $main_colour . '##' . $highlight_colour . '##' . $link_color . '##' . $font_package . '##' . $colour_scheme] = $previoustheme->event_title;
    } //end loop
} // end if
$previousThemesOtheroptions = ' id="previous_theme"' . ' class="small custom-select short_field previous_theme"  ';


//get font packages
$fontPackagesData = getDataFromTabel('font_packages', '*', '', '', 'package_name', 'ASC');
$fontPackagesOptions[""] = lang('event_customize_select_font_package');
if (!empty($fontPackagesData)) {
    foreach ($fontPackagesData as $fontPackage) {
        $fontPackagesOptions[$fontPackage->id] = $fontPackage->package_name;
    } //end loop
} // end if
$fontPackagesOtherOptions = ' id="font_package"' . ' class="small custom-select short_field"  ';
?>

<div class="panel event_panel" id="<?php echo $FormDivId; ?>">
    <div class="panel-heading ">
        <h4 class="panel-title medium dt-large heading_btn ">
            <a class="" data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
<?php echo lang('event_customize_theme'); ?>
            </a>
        </h4>
    </div>
    <div style="height: auto;" id="collapseFour" class="accordion_content collapse apply_content">
<?php echo form_open($this->uri->uri_string(), $formColourTheme); ?>
        <div class="form-horizontal form_open_chk" id="showhideformdiv<?php echo $FormDivId; ?>">
            <div class="panel-body ls_back dashboard_panel small" id="showhideformdiv<?php echo $FormDivId; ?>">
                <div class="row-fluid-15">
                    <label for="info_org"><?php echo lang('event_customize_apply_previous_theme'); ?>
                        <span class="info_btn">
                            <span class="field_info xsmall"><?php echo lang('event_customize_apply_previous_theme_tooltip'); ?>
                            </span>
                        </span>
                    </label>
<?php echo form_dropdown('previous_theme', $previousThemesoptions, '', $previousThemesOtheroptions); ?>
                </div>
                <hr class="panel_inner_seprator" />
                <div class="row-fluid-15">
                    <label for="info_org"><?php echo lang('event_customize_colour_scheme'); ?> <span class="astrik">*</span>
                        <span class="info_btn">
                            <span class="field_info xsmall"><?php echo lang('event_customize_colour_scheme_tooltip'); ?>
                            </span>
                        </span>
                    </label>

                    <div class="radio_wrapper ">
<?php echo form_input($colourSchemeDark, 0, (($colourSchemeVal == 0) ? 'checked="checked"' : '')); ?>
                        <label for="colour_schemeDark"><?php echo lang('event_customize_colour_scheme_dark'); ?></label>

<?php echo form_input($colourSchemeLight, 1, (($colourSchemeVal == 1) ? 'checked="checked"' : '')); ?>
                        <label for="colour_schemeLight"><?php echo lang('event_customize_colour_scheme_light'); ?></label>
                    </div>

                    <!--end of session sub category-->

                </div>
                <div class="row-fluid-15">
                    <label for="reg_limit"><?php echo lang('event_customize_back_colour'); ?><span class="astrik">*</span>
                        <span class="info_btn">
                            <span class="field_info xsmall"><?php echo lang('event_customize_back_colour'); ?></span>
                        </span>
                    </label>
<?php echo form_input($backgroundColour); ?>
                </div>
                <div class="row-fluid-15">
                    <label for="reg_limit"><?php echo lang('event_customize_main_colour'); ?><span class="astrik">*</span>
                        <span class="info_btn">
                            <span class="field_info xsmall"><?php echo lang('event_customize_main_colour'); ?></span>
                        </span>
                    </label>
<?php echo form_input($mainColour); ?>
                </div>
                <div class="row-fluid-15">
                    <label for="reg_limit"><?php echo lang('event_customize_highlight_colour'); ?><span class="astrik">*</span>
                        <span class="info_btn">
                            <span class="field_info xsmall"><?php echo lang('event_customize_highlight_colour'); ?></span>
                        </span>
                    </label>
<?php echo form_input($highlightColour); ?>
                </div>
                <div class="row-fluid-15">
                    <label for="info_org"><?php echo lang('event_customize_font_package'); ?> <span class="astrik">*</span></label>
<?php echo form_dropdown('font_package', $fontPackagesOptions, $fontPackageVal, $fontPackagesOtherOptions); ?>
                </div>
                <div class="row-fluid-15">
                    <label for="reg_limit"><?php echo lang('event_customize_link_colour'); ?><span class="astrik">*</span>
                    </label>
<?php echo form_input($linkColour); ?>
                </div>


                <div class="btn_wrapper ">
                    <a href="#collapseFour" class="pull-left scroll_top"><?php echo lang('comm_top'); ?></a>
                    <?php
                    echo form_hidden('eventId', $eventId);
                    //button show of save and reset
                    $formButton['showbutton'] = array('save', 'reset');
                    $formButton['labletext'] = array('save' => lang('comm_save_changes'), 'reset' => lang('comm_reset'));
                    $formButton['buttonclass'] = array('save' => 'default_btn btn pull-right medium', 'reset' => 'default_btn btn pull-right medium reset_form');
                    $this->load->view('common_save_reset_button', $formButton);
                    ?>
                </div>


            </div>
<?php echo form_close(); ?>
        </div>
    </div>
</div>
<!--end of panel-->

<script src="<?php echo base_url('themes/assets/js/jscolor.js') ?>" type="text/javascript"></script>
<script type="text/javascript">
    //submit choose theme data value
    ajaxdatasave('formColourTheme', '<?php echo $this->uri->uri_string(); ?>',true,true,false,true,true, '#showhideformdiv<?php echo $FormDivId; ?>', '#showhideformdiv<?php echo $nextFormDivId; ?>',false);

    $('.previous_theme').change(function() {
        $this = $(this);
        var theme_val = $this.val();
        var theme_val_arr = theme_val.split('##');
        //console.log(hexToRgb(theme_val_arr[1]));
        $('#background_colour').css('backgroundColor', "rgb(" + hexToRgb(theme_val_arr[1]) + ")");
        $('#main_colour').css('backgroundColor', "rgb(" + hexToRgb(theme_val_arr[2]) + ")");
        $('#highlight_colour').css('backgroundColor', "rgb(" + hexToRgb(theme_val_arr[3]) + ")");
        $('#link_colour').css('backgroundColor', "rgb(" + hexToRgb(theme_val_arr[4]) + ")");
        $('#background_colour').val(theme_val_arr[1]);
        $('#main_colour').val(theme_val_arr[2]);
        $('#highlight_colour').val(theme_val_arr[3]);
        $('#link_colour').val(theme_val_arr[4]);

        var font_package_val = $('#font_package').val();
        $('select[id^="font_package"] option[value="' + font_package_val + '"]').removeAttr("selected", "selected");
        $('select[id^="font_package"] option[value="' + theme_val_arr[5] + '"]').attr("selected", "selected");
        //console.log($('select[id^="font_package"] option[value="'+theme_val_arr[5]+'"]').text());
        $(".holder").html($('select[id^="font_package"] option[value="' + theme_val_arr[5] + '"]').text());
        $('input:radio[name="colour_scheme"]').filter('[value="' + theme_val_arr[6] + '"]').attr('checked', true);

    });
</script>
