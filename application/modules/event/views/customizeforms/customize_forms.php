<?php if (!defined('BASEPATH')) { exit('No direct script access allowed'); } ?>
<?php $eventId = $this->uri->segment(3); ?>
<?php $themeId = $this->uri->segment(4); ?>
<?php if(!empty($eventId) && !empty($themeId)) { $params = '/'.$eventId.'/'.$themeId; }else { $params =''; } ?>
<?php $this->load->view('event_menus'.$this->config->item('fileSuffix')); ?>
<div class="page_content">
    <div class="container">
        <div class="row">
            <div class="col-9">
                <div class="col-5"></div>
                <?php $this->load->view('form_customize_color_theme_free');?>
                <?php $this->load->view('form_media_gallery');?>
            </div>
            <div class="page_btnwrapper">
                <?php
                $buttonData['viewbutton']   = array('back', 'next');
                $buttonData['linkurl']      = array('back' => base_url('event/invitations'),
                    'next' => base_url('event/confirmdetails'), 'preview' => base_url('event/customizeformpreview'.$params));
                $this->load->view('common_back_next_buttons', $buttonData);
                ?>
            </div>
        </div>
    </div>
</div>
<!--start custom tooltip-->
<div id="tooltip_wrapper"></div>
<!--end custom tooltip-->

<!--Customize Form-->
<script src="<?php echo base_url('themes/assets/js/jscolor.js') ?>" type="text/javascript"></script>
<script type="text/javascript">
	var IMAGE = "<?php echo IMAGE; ?>";


 $('.imgcls').on("click",function(){
	 $('.imgcls').removeClass('selected');
     $('.imgcls').css('border','none');
     $(this).css('border-style','dashed');
     $(this).css('border-color','green');
     $(this).addClass('selected');
 });

 
 $('.select_img').on("click",function(){
	var select_img_url = $('img.selected').attr('src');
	var select_img_name = $('img.selected').attr('imgname');
    var gallery_action =  $('.gallery_action').val();
    if(gallery_action==='header') {
        //empty files data
        document.getElementById("customfileheader").value = "";
		$('.header_gallery_image').val(select_img_name);
		$('#themeHeaderImage').attr('src',select_img_url);
        $('#colour_header').val('');
        $(".theme_header ").removeAttr("style");
        $('#is_header_or_bgcolor').val('header');
        //getCorrectImage(); 
        setTimeout(function(e){
            zoomimage(); 
           },2000);
	}else {
        //empty files data
        document.getElementById("custominputlogo").value = "";
        $('#logo_image_status').val(0);
		$('.logo_gallery_image').val(select_img_name);
		$('.preview_image').attr('src',select_img_url);
	}
	
	$('.imgcls').removeClass('selected');
    $('.imgcls').css('border','none');
 });
 
 $('.select_img_close').on("click",function(){
	$('.imgcls').removeClass('selected');
    $('.imgcls').css('border','none');
 });
 
 $('.collapsed').click(function(){
    $('#collapse').css('height','auto');
 });
 

</script>
