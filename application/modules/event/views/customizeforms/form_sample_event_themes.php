<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php $default_sponsor = IMAGE.'sponsors-logos.jpg'; //default sponsor image ?>
<?php if($sample_theme_number==1) { $sampleImage = IMAGE.'SampleConference.jpg'; 
      $themeTitle = lang('sample_form').' : &nbsp; '. lang('national_conference'); $default_header = IMAGE.'trp.png'; } ?>
<?php if($sample_theme_number==2) { $sampleImage = IMAGE.'SampleBoardMeeting.jpg'; 
      $themeTitle = lang('sample_form').' : &nbsp; '. lang('board_meeting'); $default_header = IMAGE.'trp.png'; } ?>
<?php if($sample_theme_number==3) { $sampleImage = IMAGE.'SampleParty.jpg'; 
      $themeTitle = lang('sample_form').' : &nbsp; '. lang('party_time'); $default_header = IMAGE.'trp.png'; } ?>
<?php if($sample_theme_number==4) { $sampleImage = IMAGE.'SampleWedding.jpg'; 
    $themeTitle = lang('sample_form').' : &nbsp; '. lang('wedding_invitation'); $default_header = IMAGE.'trp.png'; } ?>
<?php if($sample_theme_number==5) { $sampleImage = IMAGE.'SampleExpo.jpg'; 
      $themeTitle = lang('sample_form').' : &nbsp; '. lang('exhibition'); $default_header = IMAGE.'trp.png'; } ?>
<?php if($sample_theme_number==6) { $sampleImage = IMAGE.'SampleCharity.jpg';
      $themeTitle = lang('sample_form').' : &nbsp; '. lang('charity_walk'); $default_header = IMAGE.'trp.png'; } ?>

<div class="sabPanel overflow_visible" id="sabPanel0" id="invitationsid_<?php echo $sample_theme_number; ?>">
   <div class="sabPanel_header customize_header" style="display:block;"> <?php echo $themeTitle; ?></div>
    <div class="sabPanel_contentWrapper">
        <div class="span12"></div>
        <img src="<?php echo $sampleImage;?>">
    </div>
</div>
