<?php  if (!defined('BASEPATH')) {  exit('No direct script access allowed'); 
}

//define current and next form div id
$FormDivId = 'SecondForm';
$nextFormDivId = 'ThirdForm';

//set value in variable
if (!empty($customizeformsdata)) {
    $customizeformsdata = $customizeformsdata[0];
}

$LogoFileId = (!empty($customizeformsdata->id)) ? $customizeformsdata->id : '';
$LogoFileName = (!empty($customizeformsdata->logo_image)) ? $customizeformsdata->logo_image : '';
$LogoFileNamePath = (!empty($customizeformsdata->logo_image_path)) ? $customizeformsdata->logo_image_path : '';
if(!empty($LogoFileName)){
	$logo_url = base_url().$LogoFileNamePath.$LogoFileName;
	$default_logo = base_url().$LogoFileNamePath.$LogoFileName;
	$img_show = "display:block;";
}else {
	$logo_url = '#';
	$default_logo = '';
	$img_show = "display:none;";
}
$LogoPosition = (!empty($customizeformsdata->logo_position)) ? $customizeformsdata->logo_position : '';
?>
<script>
$(document).ready(function(){
	
	$("#SecondForm .panel-title a").click(function(e){
		window.setTimeout(function(e){
			var l_pos = $('#logo_position').val();
			drag1 = $("#droppable1").position().left,
			drag2 = $("#droppable2").position().left,
			drag3 = $("#droppable3").position().left;
			if(l_pos=='3'){
				$('#preview_image').css('left', ( drag3 + ( ($("#droppable3").innerWidth() / 2 ) - $('#preview_image').innerWidth()/2 ) )  );
			}else if(l_pos=='2'){
				$('#preview_image').css('left', ( drag2 + ( ($("#droppable2").innerWidth() / 2 ) - $('#preview_image').innerWidth()/2 ) )  );
			}else if(l_pos=='1' || !l_pos){
				$('#preview_image').css('left', ( drag1 + ( ($("#droppable1").innerWidth() / 2 ) - $('#preview_image').innerWidth()/2 ) )  );
			}
		},35);
		
	});
});
</script>
<?php



//add exhibitor floor plan form 
$formEventLogo = array(
    'name' => 'formEventLogo',
    'id' => 'formEventLogo',
    'method' => 'post',
    'class' => 'form-horizontal',
);
?>

<div class="panel event_panel co_cat" id="<?php echo $FormDivId; ?>">
  <div class="panel-heading ">
    <h4 class="panel-title medium dt-large heading_btn ">
      <a class="" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
<?php echo lang('event_customize_logo') ?>
      </a>
    </h4>
  </div>
  <div style="height: auto;" id="collapseOne" class="accordion_content collapse apply_content has_subaccordion">


<?php echo form_open($this->uri->uri_string(), $formEventLogo); ?>
  <div class="form-horizontal form_open_chk" id="showhideformdiv<?php echo $FormDivId; ?>">
    <div class="panel-body ls_back dashboard_panel small" id="attch_FileContainer_logo">
    
   
   <div class="panel-content_as top_padded">
        <div class="row-fluid-15">
            <div class="custom_input_file">                      	
                <input type="file" id="theme_logo_file" name="logoImage" class="customIinputFile">
                <input type="hidden" name="action_post" value="logo">
                <a href="#" class="default_btn triggerInputFile btn medium ml_zero reset_form">Upload Image</a>
            </div>
            <span class="medium or_d">Or</span>
            <button id="gallery_upload_img" class="default_btn pull-left btn medium reset_form theme_popup_trg" type="button" gallery-action="logo" name="form_reset">Select from Gallery</button>
        </div>
        <div class="header_image_div_as mtop15">
			<div class="dragndrop_row_Q">
					<img class="ui-draggable" id="preview_image" src="<?php echo $logo_url;?>" alt=""  style="width:65px;height:65px;<?php echo $img_show;?>"/>

				<div id="droppable1" class="droppable logo_image_div_as active logo_img_div_Q">
					<div class=" centeral_text medium">Image</div>
				</div>
				<div id="droppable2" class="droppable logo_image_div_as logo_image2 active logo_img_div_Q">
					<div class=" centeral_text medium">Image</div>
				</div>
				<div id="droppable3" class="droppable logo_image_div_as logo_image3 active logo_img_div_Q">
					<div class=" centeral_text medium">Image</div>
				</div>
            </div>
            <input type="hidden" name="logo_position" id="logo_position" value="<?php echo $LogoPosition;?>">
            <input type="hidden" name="old_logo_position" value="<?php echo $LogoPosition;?>">		
            <input type="hidden" id="default_logo" value="<?php echo $default_logo;?>">		
            <p class="central_image_text medium">Header Image Here</p>
        </div>
        <div class="sponsor_logo_wraper_as">
            <div class="sponsor_logo_block_as">
                <div class="sponsor_logo_main_as">
                    <div class="centeral_text medium">Sponsor Logo</div>
                </div>
            </div>
            <div class="sponsor_logo_block_as">
                <div class="sponsor_logo_main_as">
                    <div class="centeral_text medium">Sponsor Logo</div>
                </div>
            </div>
            <div class="sponsor_logo_block_as">
                <div class="sponsor_logo_main_as">
                    <div class="centeral_text medium">Sponsor Logo</div>
                </div>
            </div>
            <div class="sponsor_logo_block_as">
                <div class="sponsor_logo_main_as">
                    <div class="centeral_text medium">Sponsor Logo</div>
                </div>
            </div>
            <div class="sponsor_logo_block_as">
                <div class="sponsor_logo_main_as">
                    <div class="centeral_text medium">Sponsor Logo</div>
                </div>
            </div>
            <div class="sponsor_logo_block_as">
                <div class="sponsor_logo_main_as">
                    <div class="centeral_text medium">Sponsor Logo</div>
                </div>
            </div>
            <div class="sponsor_logo_block_as">
                <div class="sponsor_logo_main_as">
                    <div class="centeral_text medium">Sponsor Logo</div>
                </div>
            </div>
            <div class="sponsor_logo_block_as">
                <div class="sponsor_logo_main_as">
                    <div class="centeral_text medium">Sponsor Logo</div>
                </div>
            </div>
        </div>
    </div>
    
    
      <div class="btn_wrapper ">
        <a href="#collapseOne" class="pull-left scroll_top"><?php echo lang('comm_top'); ?></a>
        <?php
        
        echo form_hidden('eventId', $eventId);
        //button show of save and reset
        $formButton['showbutton'] = array('save', 'reset');
        $formButton['labletext'] = array('save' => lang('comm_save_changes'), 'reset' => lang('comm_reset'));
        $formButton['buttonclass'] = array('save' => 'default_btn btn pull-right medium submitbtn_logo', 'reset' => 'reset_logo default_btn btn pull-right medium reset_form');
        $this->load->view('common_save_reset_button', $formButton);
        ?>
        <input type="hidden" name="gallery_action" class="gallery_action" value="">
        <input type="hidden" name="logo_gallery_image" class="logo_gallery_image" value="">
      </div>

    </div>

    </div>
  </div>
    <?php echo form_close(); ?>
</div>                     



<script type="text/javascript" src="<?php  echo $system_js_path.'upload_js/plupload.js'; ?>"></script>
<script type="text/javascript" src="<?php  echo $system_js_path.'upload_js/plupload.gears.js'; ?>"></script>
<script type="text/javascript" src="<?php  echo $system_js_path.'upload_js/plupload.silverlight.js'; ?>"></script>
<script type="text/javascript" src="<?php  echo $system_js_path.'upload_js/plupload.flash.js'; ?>"></script>
<script type="text/javascript" src="<?php  echo $system_js_path.'upload_js/plupload.browserplus.js'; ?>"></script>
<script type="text/javascript" src="<?php  echo $system_js_path.'upload_js/plupload.html4.js'; ?>"></script>
<script type="text/javascript" src="<?php  echo $system_js_path.'upload_js/plupload.html5.js'; ?>"></script>
<script type="text/javascript" src="<?php  echo $system_js_path.'upload_common.js'; ?>"></script>



<script type="text/javascript" language="javascript">
	$(document).ready(function (e){
		$(".reset_logo").on('click',function(e){
			var default_logo = $('#default_logo').val();
			if(default_logo){
				$('#preview_image').attr('src',default_logo);
			} else{
				$('#preview_image').hide();
			}
		});
	  $("#formEventLogo").on('submit',(function(e){
			e.preventDefault();
			$.ajax({
			url: baseUrl + 'event/customizeformsmediafree',
			type: "POST",
			data:  new FormData(this),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				//console.log(data);
				if(data==='null') {              
                $('#showhideformdiv<?php echo $FormDivId; ?>').parent().parent().removeClass('in');
                $("#ThirdForm .panel-title a").click();
				}else{
					var dataArr = JSON.parse(data);
					var filename = dataArr.filename;
					custom_popup('Event Logo uploaded successfully.', true);
					$('#showhideformdiv<?php echo $FormDivId; ?>').parent().parent().removeClass('in');
					$("#ThirdForm .panel-title a").click();
					var logo_position = $('#logo_position').val();
					if(dataArr.imagepath && dataArr.filename){
						$('#default_logo').val(baseUrl+dataArr.imagepath+dataArr.filename);
						if(logo_position==1){
							$('.theme_logo2').hide();
							$('.theme_logo3').hide();
							$('.theme_logo1').show();
							$('.theme_logo1 img').attr('src',baseUrl+dataArr.imagepath+dataArr.filename);
						} else if(logo_position==2){
							$('.theme_logo1').hide();
							$('.theme_logo3').hide();
							$('.theme_logo2').show();
							$('.theme_logo2 img').attr('src',baseUrl+dataArr.imagepath+dataArr.filename);
						}else {
							$('.theme_logo1').hide();
							$('.theme_logo2').hide();
							$('.theme_logo3').show();
							$('.theme_logo3 img').attr('src',baseUrl+dataArr.imagepath+dataArr.filename);
						}
					}else{
						if(logo_position==1){
							$('.theme_logo2').hide();
							$('.theme_logo3').hide();
							$('.theme_logo1').show();
						} else if(logo_position==2){
							$('.theme_logo1').hide();
							$('.theme_logo3').hide();
							$('.theme_logo2').show();
						}else {
							$('.theme_logo1').hide();
							$('.theme_logo2').hide();
							$('.theme_logo3').show();
						}
					}
				}
			},
			error: function(){} 	        
			});
	   }));
	});
	
	function readURL(input, target) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();
			var image_target = $(target);
			reader.onload = function (e) {
			var l_pos = $('#logo_position').val();
			if(!l_pos){
				$('#logo_position').val('1');
				drag1 = $("#droppable1").position().left;
				$('#preview_image').css('left', ( drag1 + ( ($("#droppable1").innerWidth() / 2 ) - $('#preview_image').innerWidth()/2 ) )  );
			}	
			image_target.attr('src', e.target.result).show();
			};
			image_target.show();
			
			reader.readAsDataURL(input.files[0]);
		 }
	 }
	 
	$(function() {
		$(".theme_popup_trg").click(function(){
			var galleryAction = $(this).attr('gallery-action');
			$('.gallery_action').val(galleryAction);
			$('#theme_popup').modal({ keyboard: false});
		});
		
		$("#theme_logo_file").on("change",function(){
		$('.gallery_action').val('');
		$('.logo_gallery_image').val('');
		readURL(this, "#preview_image");
	 });
	 
	 
    $('#preview_image').draggable(
    { revert: "invalid",axis: "x"}
    );
    $('.droppable').droppable({
		drop: function(event, ui)
		{ 
			var pos = ui.draggable.position(),
			drag1 = $("#droppable1").position().left,
			drag2 = $("#droppable2").position().left,
			drag3 = $("#droppable3").position().left;
			
			if( parseInt(pos.left) >= (drag1) && parseInt(pos.left) <= (drag1 + ( $("#droppable1").innerWidth() )  ) ){
				
				$('#logo_position').val('1');
				ui.draggable.draggable('option','revert',false);
				
				$('#preview_image').css('left', ( drag1 + ( ($("#droppable1").innerWidth() / 2 ) - $('#preview_image').innerWidth()/2 ) )  );
				
				setTimeout(function(){ 
				ui.draggable.draggable('option','revert',true);}, 500);
			}else if( parseInt(pos.left) >= (drag2) && parseInt(pos.left) <= (drag2 + ( $("#droppable2").innerWidth() )  ) ){
				
				$('#logo_position').val('2');
				ui.draggable.draggable('option','revert',false);
				
				$('#preview_image').css('left', ( drag2 + ( ($("#droppable2").innerWidth() / 2 ) - $('#preview_image').innerWidth()/2 ) )  );
				
				setTimeout(function(){ 
				ui.draggable.draggable('option','revert',true);}, 500);
			}else if( parseInt(pos.left) >= (drag3) && parseInt(pos.left) <= (drag3 + ( $("#droppable3").innerWidth() )  ) ){
				
				$('#logo_position').val('3');
				ui.draggable.draggable('option','revert',false);
				
				$('#preview_image').css('left', ( drag3 + ( ($("#droppable3").innerWidth() / 2 ) - $('#preview_image').innerWidth()/2 ) )  );
				
				setTimeout(function(){ 
				ui.draggable.draggable('option','revert',true);}, 500);
			}else{
				ui.draggable.draggable('option','revert',true);
			} 
		}
	});
  });
</script>
