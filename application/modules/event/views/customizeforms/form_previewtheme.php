<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
if(!empty($singleThemeData)){
    $customizeformsdata = $singleThemeData;
}
if (!empty($customizeformsdata)) {
    $customizeformsdata = $customizeformsdata[0];
}
if(count($eventinvitations) > 0 && !empty($eventinvitations)) {
    $eventinvitationstypes = $eventinvitations;
}else{
    $eventinvitationstypes = '';
}
$eventId = $this->uri->segment(3);
$eventIdDecoded = decode($eventId);  

$eventSocialMedia   = $eventSocialMedia[0]; 
$event_website      = (!empty($eventSocialMedia->website)) ? $eventSocialMedia->website : '';
$linkedIn           = (!empty($eventSocialMedia->linkedIn)) ? $eventSocialMedia->linkedIn : '';
$facebook           = (!empty($eventSocialMedia->facebook)) ? $eventSocialMedia->facebook : '';
$twitter            = (!empty($eventSocialMedia->twitter)) ? $eventSocialMedia->twitter : '';
$youtube            = (!empty($eventSocialMedia->youtube)) ? $eventSocialMedia->youtube : '';
$pintrest           = (!empty($eventSocialMedia->pintrest)) ? $eventSocialMedia->pintrest : '';
$instagram          = (!empty($eventSocialMedia->instagram)) ? $eventSocialMedia->instagram : '';

/* Theme details */
$data_id                = (!empty($customizeformsdata->id)) ? $customizeformsdata->id : ''; 
$event_theme_name       = (!empty($customizeformsdata->event_theme_name)) ? $customizeformsdata->event_theme_name : ''; 
$theme_type             = (!empty($customizeformsdata->theme_type)) ? $customizeformsdata->theme_type : ''; 
$event_theme_used       = (!empty($customizeformsdata->is_theme_used)) ? $customizeformsdata->is_theme_used : ''; 
$logo_image_status      = (isset($customizeformsdata->logo_image_status)) ? $customizeformsdata->logo_image_status : 0; 
/* Theme colors & fonts property */
$event_theme_screen_bg  = (!empty($customizeformsdata->event_theme_screen_bg)) ? $customizeformsdata->event_theme_screen_bg : ''; 
$event_theme_header_bg  = (!empty($customizeformsdata->event_theme_header_bg)) ? $customizeformsdata->event_theme_header_bg : ''; 
$event_theme_box_bg     = (!empty($customizeformsdata->event_theme_box_bg)) ? $customizeformsdata->event_theme_box_bg : ''; 
$event_theme_text_color = (!empty($customizeformsdata->event_theme_text_color)) ? $customizeformsdata->event_theme_text_color : ''; 
$event_theme_link_color = (!empty($customizeformsdata->event_theme_link_color)) ? $customizeformsdata->event_theme_link_color : ''; 
$event_theme_link_highlighted_color = (!empty($customizeformsdata->event_theme_link_highlighted_color)) ? $customizeformsdata->event_theme_link_highlighted_color : ''; 
$primary_color          = (!empty($customizeformsdata->primary_color)) ? $customizeformsdata->primary_color : ''; 
$secondary_color        = (!empty($customizeformsdata->secondary_color)) ? $customizeformsdata->secondary_color : '';
/* fonts packeg */
$fontPackageVal         = (!empty($customizeformsdata->font_package)) ? $customizeformsdata->font_package : '';
$fontName               = getFontFamily($fontPackageVal);
$fontName               = (!empty($fontName->package_name)) ? $fontName->package_name : 'proxima_nova_altsemibold';
$fontPackagesData       = getDataFromTabel('font_packages', '*', '', '', 'package_name', 'ASC');
$fontPackagesOptions[""] = lang('event_customize_select_font_package');
if (!empty($fontPackagesData)) {
    foreach ($fontPackagesData as $fontPackage) {
        $fontPackagesOptions[$fontPackage->id] = $fontPackage->package_name;
    } 
}
$fontPackagesOtherOptions   = ' id="font_package" class="custom-select medium_select hasCustomSelect theme font_package"  cssAction="font_family" ';
/* canvas image object */
$sponsor_image_object       = (!empty($customizeformsdata->sponsor_image_object)) ? $customizeformsdata->sponsor_image_object : ''; 
/* Theme Images property */
$header_image           = (!empty($customizeformsdata->header_image)) ? $customizeformsdata->header_image : ''; 
$header_image_path      = (!empty($customizeformsdata->header_image_path)) ? $customizeformsdata->header_image_path : ''; 
$logo_image             = (!empty($customizeformsdata->logo_image)) ? $customizeformsdata->logo_image : ''; 
$logo_image_path        = (!empty($customizeformsdata->logo_image_path)) ? $customizeformsdata->logo_image_path : ''; 
$logo_size              = (!empty($customizeformsdata->logo_size)) ? $customizeformsdata->logo_size : ''; 
$logo_size_and_position = json_decode($logo_size);
$image_position_top     = (isset($logo_size_and_position->image_position_top)) ? $logo_size_and_position->image_position_top : '19';
$image_position_left    = (isset($logo_size_and_position->image_position_left)) ? $logo_size_and_position->image_position_left : '23' ;
$image_width            = (!empty($logo_size_and_position->image_width)) ? $logo_size_and_position->image_width : '125';
$image_height           = (!empty($logo_size_and_position->image_height)) ? $logo_size_and_position->image_height : '110';


$header_image_size      = (!empty($customizeformsdata->header_image_size)) ? $customizeformsdata->header_image_size : ''; 
$header_image_position  = json_decode($header_image_size);
$x_axis                 = (isset($header_image_position->x_axis)) ? $header_image_position->x_axis : '111';
$y_axis                 = (isset($header_image_position->y_axis)) ? $header_image_position->y_axis : '0' ;
$scale                  = (isset($header_image_position->scale)) ? $header_image_position->scale : '0.464';
/* Assign Theme Saved data */
/* Set header image path */
$is_header_or_bgcolor = (!empty($customizeformsdata->is_header_or_bgcolor)) ? $customizeformsdata->is_header_or_bgcolor : ''; 
$event_theme_header_bg = (!empty($customizeformsdata->event_theme_header_bg)) ? $customizeformsdata->event_theme_header_bg : ''; 
/* Set dynamic color from theme data table */
$textColorCss           = "color:#".$event_theme_text_color." ; font-family : '".$fontName."'";
$headerColorCss         = "background:#".$event_theme_screen_bg." ; font-family : '".$fontName."'";
$primaryColorCss        = "background:#".$primary_color." ; font-family : '".$fontName."'";
$secondaryColorCss      = "background:#".$secondary_color." ; font-family : '".$fontName."'";
$highlightBgCss         = "background:#".$event_theme_link_highlighted_color." ; font-family : '".$fontName."'";
$highlightColorCss      = "color:#".$event_theme_link_highlighted_color." ; font-family : '".$fontName."'";
$headerBgColorCss       = "background-color:#".$event_theme_header_bg;
$highlightBgColorCss    = "background-color:#".$event_theme_link_highlighted_color." ; font-family : '".$fontName."'";
$navigationLinkColor    = "color:#A5A5A8";
/* get header path */
$headerStatus = 1;
if($is_header_or_bgcolor=='header' || $is_header_or_bgcolor=='') {
    if(!empty($header_image_path) && !empty($header_image)) {
        $headerPath     = base_url().$header_image_path.$header_image;
    }else {
        $headerStatus   = 0;
        $headerPath     = base_url().$this->config->item('themes_path').'images/header_banner_dummy.png';
    }
    $headerBgColorCss   = '';
}else {
    $headerPath         = base_url().$this->config->item('themes_path').'images/blank.png';
}

/* Set logo image path */
if(!empty($logo_image_path) && !empty($logo_image)) {
    $statusOverlay      = 1;
    $logoPath           = base_url().$logo_image_path.$logo_image;
}else {
    $statusOverlay      = 0; 
    $logoPath           = base_url().$this->config->item('themes_path').'images/logo_all_theme.png';
}
//display default logo
$logoDefault            = base_url().$this->config->item('themes_path').'images/logo_all_theme.png';
/* User id */
$userId                 = isLoginUser();
/* form fields */
$formAddEventTheme = array(
            'name'      => 'formAddEventTheme',
            'id'        => 'formAddEventTheme',
            'method'    => 'post',
            'class'     => 'form-horizontal mt10 eventtheme',
            'enctype'   => "multipart/form-data",
);
$themeTypeId            = $this->uri->segment(3);
$openForm               = !empty($themeTypeId) ? 'display:block' : '';
$opened                 = !empty($themeTypeId) ? 'open' : '';
$collapsed              = !empty($themeTypeId) ? '' : 'collapsed';
$themestatusaftersave   = ($this->session->flashdata('themestatus') !='') ? 'in' : '';

$userid = $eventdetails->user_id;
$eventid = $eventdetails->event_id;
$eventid = (!empty($customizeformsdata->event_id)) ? $customizeformsdata->event_id : $eventid;
$sponsorimage = base_url().'media/events_sponsors_logo/user_'.$userid.'/'.'event_'.$eventid.'/sponsors_'.$data_id.'.png';


?>

<div class="container">
   <div class="sabPanel_contentWrapper" style="<?php echo $openForm; ?>">
        <?php //echo form_open($this->uri->uri_string(),$formAddEventTheme); ?>
        <div id="theme_contener">
            <div class="theme_wrapper preview_theme">
                
                <div class="as_theme_box theme_box" id="as_theme_box" style="<?php echo $headerColorCss;?>">

                    <div class="theme_header" style="<?php echo $headerBgColorCss;?>"><!--as_theme_banner theme_banner_Q-->
                        <div class="as_themeBannerImg themebannerimg_wrap_Q  droppable_header">
                            <?php 
                                if($logo_image_status==1){
                                    $image_position_top ='19';
                                    $image_position_left='23';
                                    $image_width ='125';
                                    $image_height='110';
                                    $logoPath = $logoDefault;
                                }
                            ?>
                            <div class="draggableParent">
                                <div id="draggable_" class="ui-widget-content" style="position:absolute;top:<?php echo $image_position_top*1.7; ?>px;left:<?php echo $image_position_left*1.7; ?>px;width:<?php echo $image_width*1.7; ?>px;height:<?php echo $image_height*1.7; ?>px;z-index: 1;">
                                  <?php if($statusOverlay==1){ ?>
                                  <img src="<?php echo $logoPath; ?>" style="width:100%;height:100%;" id="overlay_img" img_val="0" class="preview_image">
                                  <?php } ?>
                            </div>
                            </div>
                            <?php 
                            $isHeaderPath = base_url().'media/event_header/'.$customizeformsdata->header_image_name;
                            $imgstatus = (@fopen($isHeaderPath,"r")==true) ? 'true' : 'false';
                            if($imgstatus=='true') { 
                                $headerImage = $isHeaderPath;
                            }else {
                                $headerImage = base_url().$this->config->item('themes_path').'images/header_banner_dummy.png';
                            }
                            ?>
                            <div class="boxSep content" >
                                <div class="imgLiquidNoFill imgLiquid frame" >
                                <img id="themeHeaderImage" class="as_themeBannerImg_ themebannerimg_Q header_bg" src="<?php echo $headerImage; ?>">
                                <!-- Droppable div area D1 -->    
                                </div>
                                
                            </div>
                            <!-- End D1 -->
                        </div>
                    </div>
                    <div class="beyond_wrapper" style="<?php echo $primaryColorCss;?>">
                        <div class="pull-left bw_left">
                            <h1  style="<?php echo $highlightColorCss;?>"><?php echo $eventdetails->event_title; ?></h1>
                            <h3 style="<?php echo $textColorCss;?>" class="title_sub"><?php echo $eventdetails->subtitle; ?></h3>
                            <span class="title_sub" style="<?php echo $textColorCss;?>">
                                <?php echo $eventdetails->event_venue; ?><br/>
                                <?php echo $eventdetails->eventVenueAddress1; ?><br/>
                                <?php if(!empty($eventdetails->eventVenueZip)){ echo 'Postcode : '.$eventdetails->eventVenueZip.'<br/>';} ?>
                                <?php 
                                $dayS = date('d',strtotime($eventdetails->starttime)); 
                                $dayE = date('d',strtotime($eventdetails->endtime)); 
                                if($dayS == $dayE) {
                                    echo date('jS F Y',strtotime($eventdetails->endtime));
                                }else {
                                echo date('d',strtotime($eventdetails->starttime)); ?> - <?php echo date('jS F Y',strtotime($eventdetails->endtime)); ?>
                                <?php } ?>
                            </span>
                            <p style="<?php echo $textColorCss;?>" class="title_sub">
                                <?php if(!empty($eventdetails->description)){ $dataDescription = make_clickable($eventdetails->description,$textColorCss); echo $dataDescription; } ?> <a style="<?php echo $highlightColorCss;?>" href="javascript:void(0);"><br/><!--http://newmanevents.com/--><?php //echo strtolower($eventdetails->event_url); ?></a>
                            </p>
                        </div>
                        
                        <!-- Login form -->
                        <form action="<?php echo base_url(); ?>events/loginPost" method="post" id="formDelegateLogin" name="formDelegateLogin">
                        
                        <div class="pull-right bw_right">
                            <?php if($this->input->get('q')=='false') { ?>
                            <span id="error-loginUsername">
                                <label id="loginUsername-error" class="error font-small" for="loginUsername">Invalid login or password!</label>
                            </span>
                            <?php } ?>
                            <?php if($this->input->get('q')=='1') { ?>
                            <span class="font-xsmall" style="<?php echo $textColorCss;?>">We have sent you an email with a event link and your password.</span>
                            <?php } ?>
                            <span class="bw_logintitle" style="<?php echo $highlightColorCss;?>">LOG IN</span>
                            <div class="pull-left">
                                <input type="text" placeholder="Email Address"  name="loginUsername" class="large_input" id="delegateUser"/>
                                <input type="password" placeholder="Password"  name="loginPassword" class="large_input" id="delegatePass"/>
                                <input type="hidden" placeholder="Password"  name="eventid" value="<?php echo $eventid; ?>"/>
                            </div>                            
                            <div class="pull-right">
                                <input type="hidden" value="1" name="is_refrer_url">
                                <button type="button" class="template_login delegateLoginAction" style="<?php echo $highlightBgColorCss;?>"></button> 
                            </div>
                            <div class="clearfix"></div>
                            <a href="javascript:void(0);" class="pull-right pr_38" style="<?php echo $highlightColorCss;?>" data-toggle="modal" data-target="">Forgot?</a>
                            <div class="clearfix"></div>
                            <p class="loginarea" style="<?php echo $textColorCss;?>">
                                <span class="pull-left">INVITATIONS CLOSE</span>
                                <span class="pull-right pr_38"><?php if($eventdetails->last_reg_date!='0000-00-00 00:00:00' && !empty($eventdetails->last_reg_date)) { echo date('jS F Y',strtotime($eventdetails->last_reg_date)); }else { echo 'NA';  } ?></span>
                            </p>
                        </div>
                        
                        </form>
                        
                    </div>
                    <!-- Secondary color & Invitation type section -->
                    <?php 
                        $numberOfInvitation = count($eventinvitationstypes);
                        if($numberOfInvitation > 1){
                            $divAlignCls = '';
                        }else {
                            $divAlignCls = 'block-center';
                        }
                    ?>
                    <div class="as_theme_content block_wrapper <?php echo $divAlignCls; ?> bx-bg-color" style="<?php echo $secondaryColorCss;?>">
                        <div class="row-fluid-15 m_btmZero">
                            <?php if(count($eventinvitationstypes) > 0 && !empty($eventinvitationstypes)) { ?>
                                <?php 
                                    if($eventinvitationstypes) {
                                        $countType  = 0;
                                        $number     = 0;
                                        foreach($eventinvitationstypes as $types) {
                                            
                                            $whereCondition = array('invitation_type_id'=>$types->id,'isWaitList'=> 0);
                                            $Qty = countColumnValue('invitation_booker_details',$whereCondition,'quantity');
                                            
                                            $invitation_name    = !empty($types->invitation_name) ? $types->invitation_name : ''; 
                                            $invitation_limit   = !empty($types->invitation_limit) ? $types->invitation_limit : ''; 
                                            $allow_waiting_list = !empty($types->allow_waiting_list) ? $types->allow_waiting_list : ''; 
                                            $password           = !empty($types->password) ? $types->password : ''; 
                                            //echo $Qty->quantity.' - l '.$invitation_limit;
                                            $limitQty = intval($invitation_limit) - intval($Qty->quantity);
                                            if($limitQty <= 0) { $limitQty = 0; }
                                            if($countType!=0) {
                                                if($countType%2==0){
                                                    $highlightBgCss ='background-color:#777B7A;';
                                                    $textColorCss = 'color:#ffffff';
                                                    $highlighted = '';
                                                }else {
                                                    $highlightBgCss ='background-color:#cccccc;';
                                                    $textColorCss = 'color:#ffffff';
                                                    $highlighted = '';
                                                }
                                            }else {
                                                $highlighted = 'highlighted';
                                                $highlightBgCss ='background-color:#FF8A03;';
                                                
                                            }
                                            
                                            if($number==0){
                                                $bgInType = $highlightBgColorCss;
                                            }else{
                                                $bgInType = $highlightBgCss;
                                            }
                                    ?>
                                    <?php $userId = isLoginUser(); ?>
                                    <?php 
                                    //if(isset($userId) && $userId!=false) { 
                                        if(!empty($invitation_limit)) { ?>
                                        <?php if(!empty($password)) { $passAllow = 'true'; }else { $passAllow='false'; } ?>    
                                        <a href="javascript:void(0);" class="setActiveLink">
                                         <div class="<?php echo $highlighted; ?> as_block hltd-colr-box"  style="<?php echo $bgInType;?>">
                                            <span class="block_count"><?php if(!empty($limitQty)) { echo $limitQty; }else{ echo '0'; } ?></span>
                                            <img src="<?php echo IMAGE; ?>sm3.png">
                                            <p class="pFive bx-txt-clor"  style="<?php echo $textColorCss;?>">
                                            <?php echo strtoupper($invitation_name); ?>&nbsp;<?php if(empty($limitQty) && $allow_waiting_list) { echo '<br/>JOIN THE WAITLIST'; } ?></p>
                                        </div>
                                        </a>
                                        
                                    <?php $number++; } 
                                    //} 
                                    /* else { if(!empty($invitation_limit)) { ?>
                                    <a href="javascript:void(0);" class="setActiveLink add_refrer_url_param" data-toggle="modal" data-target=".login_popup">
                                     <div class="<?php echo $highlighted; ?> as_block hltd-colr-box"  style="<?php echo $highlightBgCss;?>">
                                        <span class="block_count"><?php if(!empty($limitQty)) { echo $limitQty; }else{ echo '0'; } ?></span>
                                        <img src="<?php echo IMAGE; ?>sm3.png">
                                        <p class="pFive bx-txt-clor"  style="<?php echo $textColorCss;?>">
                                        <?php echo strtoupper($invitation_name); ?>&nbsp;<?php if(empty($limitQty)) { echo '<br/>JOIN THE WAITLIST'; } ?></p>
                                    </div>
                                    </a>
                                    <?php } } */ ?>    
                                    
                                <?php
                                    $countType++;
                                    
                                    } //foreach end
                                 } //end if ?>    
                            <?php } else { ?>
                            <div class="highlighted as_block hltd-colr-box"  style="<?php echo $highlightBgCss;?>">
                                <span class="block_count"><?php if(!empty($eventdetails->number_of_rego)) { echo $eventdetails->number_of_rego; }else{ echo '0'; } ?></span>
                                <img src="<?php echo IMAGE; ?>sm3.png">
                                <p class="pFive bx-txt-clor font-small"  style="<?php echo $textColorCss;?>">REGISTER HERE</p>
                            </div>
                            <?php } ?>
                        </div>  
                        <div class="row-fluid-15 m_btmZero">
                            
                        </div>                                               
                    </div>
                     <!-- End secondary section -->
                    <div class="themeSponsorsLogos">
<!--
                        <div id="canvasRow" style="solid;width:596;height:153px;">
                            <div id="canvasPopover"></div>
                            <div id="statusBar"></div>
								<div style="top:40px;left:10px;z-index:9;  padding-top: 30px;" class="arrow-down tip" id="dragTip">
									<h2 style="text-align:center;line-height: 35px;font-size:22px;">IMAGE GALLERY</h2>
									<p>Add as many photos or logos <br>as you need.</p>
								</div>
                            <div id="canvasContainer">
                                <canvas id="canvas" width="596" height="153">
                                
                                </canvas>
                            </div>
						</div>
-->
                        <img src="<?php echo $sponsorimage; ?>" style="width:100%;max-height:220px;">
                    </div>
                   
                
                <div class="as_themefooter" style="<?php echo $headerColorCss; ?>">
                <img src="<?php echo IMAGE; ?>footer_txt.png">
                <ul class="as_small_social">
                <li class="as_fb"><img src="<?php echo base_url(); ?>themes/assets/images/fb.png"></li> 
                <li class="as_twt"><img src="<?php echo base_url(); ?>themes/assets/images/twt.png"></li>
                <li class="as_lin"><img src="<?php echo base_url(); ?>themes/assets/images/lin.png"></li>
                <li class="like_youtube"><img src="<?php echo base_url(); ?>themes/assets/images/youtube.png"></li>  
                <li class="like_gplus"><img src="<?php echo base_url(); ?>themes/assets/images/g+.png"></li>
                <li class="like_pinterset"><img src="<?php echo base_url(); ?>themes/assets/images/pinterest.png"></li>                                                      
                <li class="like_instagram"><img src="<?php echo base_url(); ?>themes/assets/images/instagram.png"></li>                                                      
                    </ul>
                </div>
           
                
                </div>
                
                
                <div class="">
                        <div class="pull-right">
                            <input type="hidden" class="image_position_top" name="image_position_top" value="<?php echo $image_position_top; ?>">
                            <input type="hidden" class="image_position_left" name="image_position_left" value="<?php echo $image_position_left;?>">
                            <input type="hidden" class="image_width" name="image_width" value="<?php echo $image_width; ?>">
                            <input type="hidden" class="image_height" name="image_height" value="<?php echo $image_height; ?>">
                            <input type="hidden" name="gallery_action" class="gallery_action" value="">
                            <input type="hidden" name="header_gallery_image" class="header_gallery_image" value="">
                            <input type="hidden" name="logo_gallery_image" class="logo_gallery_image" value="">
                            <input type="hidden" name="action_post" value="header">
                            <input type="hidden" id="default_header" value="<?php echo $headerPath; ?>">
                            <?php echo form_hidden('eventId', $eventId); ?>
                            <input type="hidden" name="event_theme_name" id="theme_name" value="<?php echo $event_theme_name; ?>">
                            <input type="hidden" name="data_id" id="data_id" value="<?php echo $data_id; ?>">
                            <input type="hidden" name="theme_type" id="theme_type" value="<?php echo $theme_type; ?>">
                            <input type="hidden" id="sponsor_img" name="sponsor_img">
                            <input type="hidden" id="sponsor_canvas_data" name="sponsor_canvas_data">
                            <input type="hidden" id="is_header_or_bgcolor" name="is_header_or_bgcolor" value="header">                    
                            <input type="hidden" id="is_theme_used_data" name="is_theme_used_data" value="0">
                            <input type="hidden" id="logo_image_status" name="logo_image_status" value="0">
                            <input type="hidden" id="is_theme_"  value="1">
                            <!-- Header image data -->
                            <input type="hidden" id="x" name="x_axis" value="<?php echo $x_axis; ?>">
                            <input type="hidden" id="y" name="y_axis" value="<?php echo $y_axis; ?>">
                            <input type="hidden" id="scale" name="scale" value="<?php echo $scale; ?>">
                            
                            <?php $this->session->set_userdata('referrer_event_id', $eventId); ?>
                                        
                                
                        </div>
                    </div> 
                </div>
                <!-- theme wrapper -->
            </div>
        </div>
        <?php //echo form_close(); ?>
</div>
<style>
.as_themefooter {
  width:100% !important;
  margin: 0px auto;
  min-height: 40px;
  display: inline-block;
  background: #ffffff;
    padding: 8px 6px;
      padding-bottom: 10px !important;
}

.as_themefooter .as_small_social {
  padding: 0px;
  margin: 0px;
  margin-top: 20px;
  list-style: outside none none;
  float: left;
  clear: both;
}
.as_themefooter .as_small_social {
  margin-top: -35px;
  float: right;
}
.as_themefooter .as_small_social li {
  width: 38px;
  height: 38px;
  float: left;
  margin-right: 5px;
}
.as_themefooter img {
  max-width: 100%;
  float: left;
  margin-top: 3px;
}
</style>
<script> 
    var bgcolor= '#<?php echo $event_theme_screen_bg; ?>';

    $('.mainNavigation').remove(); 
    $('.afterLogin').remove(); 
    $(document).ready(function(){
        $('body').css('background-color',bgcolor);
    });
</script>
