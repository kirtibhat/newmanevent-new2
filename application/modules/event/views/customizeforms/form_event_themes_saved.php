<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php
/*
 * Get logo details
 */ 
//echo '<pre>'; print_r($themes); 
if (!empty($customizeformsdata)) {
    $customizeformsdata = $customizeformsdata[0];
}
$LogoFileId = (!empty($customizeformsdata->id)) ? $customizeformsdata->id : '';
$LogoFileName = (!empty($customizeformsdata->logo_image)) ? $customizeformsdata->logo_image : '';
if(!empty($LogoFileName)) {
    $logoPath = base_url().$customizeformsdata->logo_image_path.$LogoFileName;
}else {
    $logoPath = IMAGE.'New-Top-Image-2.jpg';
}
//end
$event_theme_name = (!empty($themes->event_theme_name)) ? $themes->event_theme_name : '';
$themeId = (!empty($themes->id)) ? $themes->id : $counter;
//$themeFormData = getSavedThemeDetails($themeId,$eventId);
$themeFormData = $themes;

$event_theme_screen_bg_val  = (!empty($themeFormData->event_theme_screen_bg)) ? $themeFormData->event_theme_screen_bg : '';
$event_theme_header_bg_val  = (!empty($themeFormData->event_theme_header_bg)) ? $themeFormData->event_theme_header_bg : '';
$event_theme_box_bg_val     = (!empty($themeFormData->event_theme_box_bg)) ? $themeFormData->event_theme_box_bg : '';
$event_theme_text_color_val = (!empty($themeFormData->event_theme_text_color)) ? $themeFormData->event_theme_text_color : '';
$event_theme_link_color_val = (!empty($themeFormData->event_theme_link_color)) ? $themeFormData->event_theme_link_color : '';
$event_theme_link_highlighted_color_val = (!empty($themeFormData->event_theme_link_highlighted_color)) ? $themeFormData->event_theme_link_highlighted_color : '';
$fontPackageVal = (!empty($themeFormData->font_package)) ? $themeFormData->font_package : '';
//get font family name
$fontName = getFontFamily($fontPackageVal);
$fontName = (!empty($fontName->package_name)) ? $fontName->package_name : '';

$event_theme_tab_name  = (!empty($themeFormData->event_theme_name)) ? $themeFormData->event_theme_name : '';


$formAddEventTheme = array(
    'name'   => 'formAddEventTheme'.$themeId,
    'id'     => 'formAddEventTheme'.$themeId,
    'method' => 'post',
    'class'  => 'form-horizontal mt10',
    'data-parsley-validate' => '',
);
$event_theme_screen_bg = array(
    'name'  => 'event_theme_screen_bg11'.$themeId,
    'value' => $event_theme_screen_bg_val,
    'id'    => 'event_theme_screen_bg'.$themeId,
    'type'  => 'text',
    'themeId'=> $themes->id,
    'cssAction' => 'screen_bg',
    'placeholder' => '',
    'autocomplete' => 'off',
    //'required'  => '',
    'class' => "color  {styleElement:'event_theme_screen_bg".$themeId."'} custom_input small theme event_theme_screen_bg".$themeId."",
    'data-parsley-error-message' => lang('dash_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
);
$event_theme_header_bg = array(
    'name'  => 'event_theme_header_bg'.$themeId,
    'value' => $event_theme_header_bg_val,
    'id'    => 'event_theme_header_bg'.$themeId,
    'type'  => 'text',
    'themeId'=> $themes->id,
    'cssAction' => 'header_bg',
    'placeholder' => '',
    'autocomplete' => 'off',
    //'required'  => '',
    'class' => "color  {styleElement:'event_theme_header_bg".$themeId."'} custom_input small theme event_theme_header_bg".$themeId."",
    'data-parsley-error-message' => lang('dash_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
);
$event_theme_box_bg = array(
    'name'  => 'event_theme_box_bg'.$themeId,
    'value' => $event_theme_box_bg_val,
    'id'    => 'event_theme_box_bg'.$themeId,
    'type'  => 'text',
    'themeId'=> $themes->id,
    'cssAction' => 'box_bg',
    'placeholder' => '',
    'autocomplete' => 'off',
    //'required'  => '',
    'class' => "color  {styleElement:'event_theme_box_bg".$themeId."'} custom_input small theme event_theme_box_bg".$themeId."",
    'data-parsley-error-message' => lang('dash_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
);
$event_theme_text_color = array(
    'name'  => 'event_theme_text_color'.$themeId,
    'value' => $event_theme_text_color_val,
    'id'    => 'event_theme_text_color'.$themeId,
    'type'  => 'text',
    'themeId'=> $themes->id,
    'cssAction' => 'text_color',
    'placeholder' => '',
    'autocomplete' => 'off',
    //'required'  => '',
    'class' => "color  {styleElement:'event_theme_text_color".$themeId."'} custom_input small theme event_theme_text_color".$themeId."",
    'data-parsley-error-message' => lang('dash_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
);
$event_theme_link_color = array(
    'name'  => 'event_theme_link_color'.$themeId,
    'value' => $event_theme_link_color_val,
    'id'    => 'event_theme_link_color'.$themeId,
    'type'  => 'text',
    'themeId'=> $themes->id,
    'cssAction' => 'link_color',
    'placeholder' => '',
    'autocomplete' => 'off',
    //'required'  => '',
    'class' => "color  {styleElement:'event_theme_link_color".$themeId."'} custom_input small theme event_theme_link_color".$themeId."",
    'data-parsley-error-message' => lang('dash_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
);
$event_theme_link_highlighted_color = array(
    'name'  => 'event_theme_link_highlighted_color'.$themeId,
    'value' => $event_theme_link_highlighted_color_val,
    'id'    => 'event_theme_link_highlighted_color'.$themeId,
    'type'  => 'text',
    'themeId'=> $themes->id,
    'cssAction' => 'link_highlighted_color',
    'placeholder' => '',
    'autocomplete' => 'off',
    //'required'  => '',
    'class' => "color  {styleElement:'event_theme_link_highlighted_color".$themeId."'} custom_input small theme event_theme_link_highlighted_color".$themeId."",
    'data-parsley-error-message' => lang('dash_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
);

//get font packages

$fontPackagesData = getDataFromTabel('font_packages', '*', '', '', 'package_name', 'ASC');
$fontPackagesOptions[""] = lang('event_customize_select_font_package');
if (!empty($fontPackagesData)) {
    foreach ($fontPackagesData as $fontPackage) {
        $fontPackagesOptions[$fontPackage->id] = $fontPackage->package_name;
    } //end loop
} // end if
$fontPackagesOtherOptions = ' id="font_package'.$themeId.'"' . ' class="small select_poz custom-select short_field pull-right theme font_package'.$themeId.'"  cssAction="font_family" themeId= '.$themes->id.'';

?>
<div class="panel event_panel sub_panel mtop15<?php echo $themes->event_theme_name; ?>" id="invitationsid_<?php echo $themeId; ?>">
    <div class="panel-heading ">
        <h4 class="panel-title medium dt-large filter_invitations  <?php echo strtolower($themes->event_theme_name); ?>" id="invitation_<?php echo $themes->id; ?>">
            <a class="collapsed" data-toggle="collapse" data-parent="#accordion_sub" href="#collapseInternal<?php echo $themes->id; ?>">
              <?php echo $themes->event_theme_name;//echo lang('themeoption')." ".$counter;?>              
            </a>
            
        </h4>
    </div>

    
  <div style="height: auto;" id="collapseInternal<?php echo $themeId; ?>" class="accordion_content collapse apply_content ">
    <div class="span12">
    </div>
    <?php echo form_open($this->uri->uri_string(),$formAddEventTheme); ?>
        <div id="theme_contener">
            <div class="theme_wrapper">
                <div class="theme_color_value">
                    <div class="row-fluid-15">
                        <div class="w50 pd_right10">
                            <div class="cate_name">
                                <p class="small"><?php echo lang('event_theme_screen_bg'); ?></p>
                            </div>
                            <div class="color_picker">
                                <span id="event_theme_screen_bg<?php echo $themeId; ?>"></span>
                                <?php echo form_input($event_theme_screen_bg); ?>
                            </div>
                        </div>
                        <div class="w50">
                            <div class="cate_name">
                                <p class="small"><?php echo lang('event_theme_header_bg'); ?></p>
                            </div>
                            <div class="color_picker">
                                <span id="event_theme_header_bg<?php echo $themeId; ?>"></span>
                                <?php echo form_input($event_theme_header_bg); ?>
                            </div>
                        </div>
                    </div>
                    <div class="row-fluid-15">
                        <div class="w50 pd_right10">
                            <div class="cate_name">
                                <p class="small"><?php echo lang('event_theme_box_bg'); ?></p>
                            </div>
                            <div class="color_picker">
                                <span id="event_theme_box_bg<?php echo $themeId; ?>"></span>
                                <?php echo form_input($event_theme_box_bg); ?>
                            </div>
                        </div>
                        <div class="w50">
                            <div class="cate_name">
                                <p class="small"><?php echo lang('event_theme_text_color'); ?></p>
                            </div>
                            <div class="color_picker">
                                <span id="event_theme_text_color<?php echo $themeId; ?>"></span>
                                <?php echo form_input($event_theme_text_color); ?>
                            </div>
                        </div>
                    </div>
                    <div class="row-fluid-15">
                        <div class="w50 pd_right10">
                            <div class="cate_name">
                                <p class="small"><?php echo lang('event_theme_link_color'); ?></p>
                            </div>
                            <div class="color_picker">
                                <span id="event_theme_link_color<?php echo $themeId; ?>"></span>
                                <?php echo form_input($event_theme_link_color); ?>
                            </div>
                        </div>
                        <div class="w50">
                            <div class="cate_name">
                                <p class="small"><?php echo lang('event_theme_link_highlighted_color'); ?></p>
                            </div>
                            <div class="color_picker">
                                <span id="event_theme_link_highlighted_color<?php echo $themeId; ?>"></span>
                                <?php echo form_input($event_theme_link_highlighted_color); ?>
                            </div>
                        </div>
                    </div>                                            
                    <div class="row-fluid-15 m_btmZero">
                        <div class="w_100">
                            <div class="cate_name w25">
                                <p class="small"><?php echo lang('event_theme_font'); ?></p>
                            </div>
                            <div class="w75 full_width_select as_pa_left5">
                                <?php echo form_dropdown('font_package'.$themeId, $fontPackagesOptions, $fontPackageVal, $fontPackagesOtherOptions); ?>
                            </div>
                        </div>                                                
                    </div>
                </div>
                <?php
                    $linkColorCss = "color:#'".$event_theme_link_color_val."' ; font-family : '".$fontName."'";
                    $textColorCss = "color:#'".$event_theme_text_color_val."' ; font-family : '".$fontName."'";
                    $linkHighlightedColorCss = "color:#'".$event_theme_link_highlighted_color_val."' ; font-family : '".$fontName."'";
                    $bxBgColorCss = "background:#'".$event_theme_box_bg_val."' ; font-family : '".$fontName."'";
                ?>
                
                <div class="as_theme_box theme_box<?php echo $counter; ?>" id="as_theme_box<?php echo $counter; ?>" style="background:#<?php echo $event_theme_screen_bg_val; ?>;">
                    <div class="as_theme_head theme_head<?php echo $counter; ?>" id="as_theme_head<?php echo $counter; ?>" style="background:#<?php echo $event_theme_screen_bg_val; ?>;">
                        <ul class="as_small_nav">
                            <li class="as_small_nav<?php echo $counter; ?>" style="<?php echo $linkColorCss; ?>">Home</li>
                            <li class="as_small_nav<?php echo $counter; ?>" style="<?php echo $linkColorCss; ?>">Manage Existing Registration</li>
                            <li class="as_small_nav<?php echo $counter; ?>" style="<?php echo $linkColorCss; ?>">Terms and Conditions</li>
                            <li class="as_small_nav<?php echo $counter; ?>" style="<?php echo $linkColorCss; ?>">Client Website</li>
                            <li class="as_small_nav<?php echo $counter; ?>" style="<?php echo $linkColorCss; ?>">Contact Us</li>
                        </ul>
                        <ul class="as_small_social">
                            <li class="as_twt"></li>
                            <li class="as_fb"></li>   
                            <li class="as_lin"></li>                                                  
                        </ul>
                    </div>
                    <div class="as_theme_banner theme_popup_zoom"  eventid="<?php echo $eventId; ?>" themeid="<?php echo $themeId; ?>">
                        <div class="as_themeBannerImg">
                            <img class="as_themeBannerImg_" src="<?php echo $logoPath; ?>">
                        </div>
                        <div class="as_themeBannerInfo">
                            <div class="as_themeLeft">
                                <div class="as_themeHeading themeHeading<?php echo $counter; ?>" id="as_themeHeading<?php echo $counter; ?>">
                                    <h2 id="eventTitle<?php echo $counter; ?>" class="eventTitle<?php echo $counter; ?>" style="<?php echo $linkHighlightedColorCss ?>"><?php echo $eventdetails->event_title; ?></h2>
                                    <p class="pOne eventSubTitle<?php echo $counter; ?>" id="eventSubTitle<?php echo $counter; ?>" style="<?php echo $textColorCss; ?>"><?php echo $eventdetails->subtitle; ?></p>
                                    <p class="pTwo eventlocation<?php echo $counter; ?>" id="eventlocation<?php echo $counter; ?>" style="<?php echo $textColorCss; ?>"><?php echo $eventdetails->event_venue; ?> , <?php echo $eventdetails->event_location; ?>  <?php echo $eventdetails->starttime; ?>, <?php echo $eventdetails->endtime; ?></p>
                                </div>
                                <p class="pThree" id="eventDescription<?php echo $counter; ?>"><?php echo $eventdetails->description; ?></p>
                            </div>
                            <div class="as_themeRight themeRight<?php echo $counter; ?>" id="as_themeRight<?php echo $counter; ?>">
                                <div class="as_inputDummy">
                                    <input type="text" class="as_dummy_input"><input type="button" class="btnDummy">
                                </div>  
                                <p class="pFour hltd-colr<?php echo $counter; ?>">ACCESS ID #<span class="hltd-colr<?php echo $counter; ?>">(what's this?)</span></p>
                                <p class="pTwo bx-txt-clor<?php echo $counter; ?>" style="<?php echo $textColorCss; ?>">REGISTRATION OPENS  01 February 2015 <br>EARLY-BIRD REG CLOSES 15 April 2015</p>
                                <div class="as_priceVal">
                                    <p class="hltd-colr<?php echo $counter; ?>" style="<?php echo $linkHighlightedColorCss ?>">A$120 <span class="hltd-colr<?php echo $counter; ?>" style="color:#<?php echo $event_theme_link_highlighted_color_val; ?>;">Standard</span></p>
                                    <p class="hltd-colr<?php echo $counter; ?>" style="<?php echo $linkHighlightedColorCss ?>">A$80 <span class="hltd-colr<?php echo $counter; ?>" style="<?php echo $linkHighlightedColorCss ?>">Early-Bird</span></p>
                                </div>
                                <button type="button" class="dummyWhiteBtn pull-right"></button>
                            </div>
                        </div>
                    <!-- end -->
                    <div class="as_themeBannerTag">
                        <p>EVENT PROGRAM</p>
                    </div>
                    </div>
                    <div class="as_theme_content">
                        <div class="row-fluid-15 m_btmZero">
                            <div class="as_block bx-bg-color<?php echo $counter; ?>" style="<?php echo $bxBgColorCss; ?>">
                                <img src="<?php echo IMAGE; ?>sm1.png">
                                <p class="pFive bx-txt-clor<?php echo $counter; ?>" style="<?php echo $textColorCss; ?>">Delegate Registration</p>
                            </div>
                            <div class="as_block bx-bg-color<?php echo $counter; ?>" style="<?php echo $bxBgColorCss; ?>">
                                <img src="<?php echo IMAGE; ?>sm2.png">
                                <p class="pFive bx-txt-clor<?php echo $counter; ?>" style="<?php echo $textColorCss; ?>">Sponsor Registration</p>
                            </div>
                            <div class="as_block bx-bg-color<?php echo $counter; ?>" style="<?php echo $bxBgColorCss; ?>">
                                <img src="<?php echo IMAGE; ?>sm3.png">
                                <p class="pFive bx-txt-clor<?php echo $counter; ?>" style="<?php echo $textColorCss; ?>">Exhibitor Registration</p>
                            </div>
                            <div class="as_block bx-bg-color<?php echo $counter; ?>" style="<?php echo $bxBgColorCss; ?>">
                                <img src="<?php echo IMAGE; ?>sm4.png">
                                <p class="pFive bx-txt-clor<?php echo $counter; ?>" style="<?php echo $textColorCss; ?>">Speaker Registration</p>
                            </div>
                            <div class="as_block bx-bg-color<?php echo $counter; ?>" style="<?php echo $bxBgColorCss; ?>">
                                <img src="<?php echo IMAGE; ?>sm5.png">
                                <p class="pFive bx-txt-clor<?php echo $counter; ?>" style="<?php echo $textColorCss; ?>">Custom Category 1</p>
                            </div>
                        </div>  
                        <div class="row-fluid-15 m_btmZero">
                            <div class="as_block bx-bg-color<?php echo $counter; ?>" style="<?php echo $bxBgColorCss; ?>">
                                <img src="<?php echo IMAGE; ?>sm5.png">
                                <p class="pFive bx-txt-clor<?php echo $counter; ?>" style="<?php echo $textColorCss; ?>">Custom Category 2</p>
                            </div>
                            <div class="as_block bx-bg-color<?php echo $counter; ?>" style="<?php echo $bxBgColorCss; ?>">
                                <img src="<?php echo IMAGE; ?>sm5.png">
                                <p class="pFive bx-txt-clor<?php echo $counter; ?>" style="<?php echo $textColorCss; ?>">Custom Category 3</p>
                            </div>
                            <div class="as_block bx-bg-color<?php echo $counter; ?>" style="<?php echo $bxBgColorCss; ?>">
                            </div>
                            <div class="as_block bx-bg-color<?php echo $counter; ?>" style="<?php echo $bxBgColorCss; ?>">
                            </div>
                            <div class="as_block bx-bg-color<?php echo $counter; ?>" style="<?php echo $bxBgColorCss; ?>">
                            </div>
                        </div>                                               
                    </div>
                    <div class="as_themeSponsorsLogos">
                        <img src="<?php echo IMAGE; ?>sponsors-logos.jpg">
                    </div>
                    <div class="as_themefooter">
                        <img src="<?php echo IMAGE; ?>footer_txt.png">
                    </div>
                </div>
                
                
                <div class="btn_wrapper mtop15">
                    <?php
                        echo form_hidden('eventId', $eventId);
                        echo form_hidden('themeId', $themeId);
                        echo form_hidden('event_theme_name', $event_theme_name);
                    ?>
                    <input type="submit" class="default_btn btn pull-right medium" value="<?php echo lang('comm_save_changes'); ?>" name="save">
                    <button class="default_btn btn pull-right medium reset_form" type="button" name="form_reset"><?php echo lang('reset_btn_title'); ?></button>
                    <button class="default_btn btn pull-right medium customize_theme_popup" type="button" name="use" themeid="<?php echo $themeId; ?>" eventid="<?php echo $eventId; ?>"><?php echo lang('use_btn_title'); ?></button>     
             
                </div>
            </div>
        </div>
        <?php echo form_close(); ?>
    </div>
     
</div>     
