<?php if (! defined('BASEPATH')) { exit('No direct script access allowed'); 
}
//define current and next form div id
$FormDivId = 'FirstForm';
$nextFormDivId = 'SecondForm';

if (!empty($customizeformsdata)) {
    $customizeformsdata = $customizeformsdata[0];
}

$HeaderFileId = (!empty($customizeformsdata->id)) ? $customizeformsdata->id : '';
$HeaderFileName = (!empty($customizeformsdata->header_image)) ? $customizeformsdata->header_image : 'trp.png';

$headerPath='';
if(!empty($customizeformsdata->header_image_path)) {
$headerPath = getcwd().$customizeformsdata->header_image_path.$HeaderFileName;
}
if(file_exists($headerPath)){
	$headerPath = base_url().$customizeformsdata->header_image_path.$HeaderFileName;
    $default_header = base_url().$customizeformsdata->header_image_path.$HeaderFileName;
}else {
    $headerPath = IMAGE.'trp.png';
    $default_header = IMAGE.'trp.png';
}

//add exhibitor floor plan form 
$formEventHeader = array(
    'name' => 'formEventHeader',
    'id' => 'formEventHeader',
    'method' => 'post',
    'class' => 'form-horizontal',
    'enctype'=>"multipart/form-data",
);
?>


<div class="panel event_panel co_cat" id="<?php echo $FormDivId; ?>">
  <div class="panel-heading ">
    <h4 class="panel-title medium dt-large heading_btn ">
      <a class="" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
        <?php echo lang('event_customize_header') ?>
      </a>
    </h4>
  </div>
  <div style="height: auto;" id="collapseTwo" class="accordion_content collapse apply_content has_subaccordion">
    <?php echo form_open($this->uri->uri_string(), $formEventHeader); ?>
    <div class="form-horizontal form_open_chk" id="showhideformdiv<?php echo $FormDivId; ?>">

    <div class="panel-body ls_back dashboard_panel small" id="attch_FileContainer">
    
    <div class="panel-content_as top_padded">
            <div class="row-fluid-15">
                <div class="custom_input_file">                      	
                    <input type="file" class="customIinputFile" id="imgInp" name="headerImage">
                    <input type="hidden" name="action_post" value="header">
                    <a href="#" class="default_btn triggerInputFile btn medium ml_zero reset_form">Upload Image</a>
                </div>
                <span class="medium or_d">Or</span>
                <button class="default_btn pull-left btn medium reset_form theme_popup_trg" type="button" gallery-action="header" name="form_reset">Select from Gallery</button>
            </div>
            
            <div class="header_image_div_as header_image_Q mtop15 active">
                <div class="logo_image_div_as ">
                    <div class="centeral_text medium"><?php echo lang('image'); ?></div>
                </div>
                <p class="central_image_text medium"><?php echo lang('header_image_text'); ?></p>
                <img id="themeHeaderImage" src="<?php echo $headerPath; ?>" alt="image" style="height:100%;width:100%;" />
            </div>
            
            
            <div class="sponsor_logo_wraper_as">
                <div class="sponsor_logo_block_as">
                    <div class="sponsor_logo_main_as">
                        <div class="centeral_text medium"><?php echo lang('event_event_sponsor'); ?></div>
                    </div>
                </div>
                <div class="sponsor_logo_block_as">
                    <div class="sponsor_logo_main_as">
                        <div class="centeral_text medium"><?php echo lang('event_event_sponsor'); ?></div>
                    </div>
                </div>
                <div class="sponsor_logo_block_as">
                    <div class="sponsor_logo_main_as">
                        <div class="centeral_text medium"><?php echo lang('event_event_sponsor'); ?></div>
                    </div>
                </div>
                <div class="sponsor_logo_block_as">
                    <div class="sponsor_logo_main_as">
                        <div class="centeral_text medium"><?php echo lang('event_event_sponsor'); ?></div>
                    </div>
                </div>
                <div class="sponsor_logo_block_as">
                    <div class="sponsor_logo_main_as">
                        <div class="centeral_text medium"><?php echo lang('event_event_sponsor'); ?></div>
                    </div>
                </div>
                <div class="sponsor_logo_block_as">
                    <div class="sponsor_logo_main_as">
                        <div class="centeral_text medium"><?php echo lang('event_event_sponsor'); ?></div>
                    </div>
                </div>
                <div class="sponsor_logo_block_as">
                    <div class="sponsor_logo_main_as">
                        <div class="centeral_text medium"><?php echo lang('event_event_sponsor'); ?></div>
                    </div>
                </div>
                <div class="sponsor_logo_block_as">
                    <div class="sponsor_logo_main_as">
                        <div class="centeral_text medium"><?php echo lang('event_event_sponsor'); ?></div>
                    </div>
                </div>
            </div>
        </div>


      <div class="btn_wrapper ">
        <a href="#collapseTwo" class="pull-left scroll_top"><?php echo lang('comm_top'); ?></a>
        <?php
        echo form_hidden('eventId', $eventId);
        
        //button show of save and reset
        $formButton['showbutton'] = array('save', 'reset');
        $formButton['labletext'] = array('save' => lang('comm_save_changes'), 'reset' => lang('comm_reset'));
        $formButton['buttonclass'] = array('save' => 'default_btn btn pull-right medium submitbtn_header', 'reset' => 'reset_header default_btn btn pull-right medium reset_form');
        $this->load->view('common_save_reset_button', $formButton);
        ?>
        <input type="hidden" id="default_header" value="<?php echo $default_header; ?>">
        <input type="hidden" name="gallery_action" class="gallery_action" value="">
        <input type="hidden" name="header_gallery_image" class="header_gallery_image" value="">
      </div>

    </div>

  </div>
</div>
<?php echo form_close(); ?>
</div>


<script type="text/javascript" language="javascript">

$(document).ready(function (e){
	$(".reset_header").on('click',function(e){
			var default_header = $('#default_header').val();
			$('#themeHeaderImage').attr('src',default_header);
		});
  $("#formEventHeader").on('submit',(function(e){
        e.preventDefault();
        $.ajax({
        url: baseUrl + 'event/customizeformsmediafree',
        type: "POST",
        data:  new FormData(this),
        contentType: false,
        cache: false,
        processData:false,
        success: function(data){
            if(data==='null') {               
                $('#showhideformdiv<?php echo $FormDivId; ?>').parent().parent().removeClass('in');
                $("#SecondForm .panel-title a").click();
            }else{
                var dataArr = JSON.parse(data);
                var filename = dataArr.filename;
                var imgpath  = dataArr.imagepath; 
                $('#default_header').val(baseUrl+imgpath+filename); 
                setTimeout(function(){ 
                    $('.as_themeBannerImg_').attr('src', baseUrl+imgpath+filename); 
                }, 4000);
                
                custom_popup('Event header uploaded successfully.', true);                
                $('#showhideformdiv<?php echo $FormDivId; ?>').parent().parent().removeClass('in');
                $("#SecondForm .panel-title a").click();
            }
        },
        error: function(){} 	        
        });
   }));
});

function readHeaderURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#themeHeaderImage').attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    }
    
    $(".customIinputFile").change(function(){
        readHeaderURL(this);
    });


$(".theme_popup_trg").click(function(){
    //$('.media_gallery').show();
    var galleryAction = $(this).attr('gallery-action');
    $('.gallery_action').val(galleryAction);
	$('#theme_popup').modal({ keyboard: false});
});

$("#imgInp").change(function(){
    $('.gallery_action').val('');
    $('.header_gallery_image').val('');
});

</script>
