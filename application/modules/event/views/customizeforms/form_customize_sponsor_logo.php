<script src="<?php echo base_url('themes/assets/js/zoom.js') ?>" type="text/javascript"></script>
<script src="<?php echo base_url('themes/assets/js/zoom.min.js') ?>" type="text/javascript"></script>
<?php if (! defined('BASEPATH')) { exit('No direct script access allowed'); 
}


if (!empty($customizeformsdata)) {
    $customizeformsdata = $customizeformsdata[0];
}



$sponsor_image_object = (!empty($customizeformsdata->sponsor_image_object))?$customizeformsdata->sponsor_image_object:'';
if(!empty($sponsor_image_object)){
	$sponsor_reset = '';
}else {
$sponsor_reset = 'sponsor_reset';
}

//define current and next form div id
$FormDivId = 'ThirdForm';
$nextFormDivId = 'FourthForm';

//add exhibitor floor plan form 
$formEventSponsorLogo = array(
    'name' => 'formEventSponsorLogo',
    'id' => 'formEventSponsorLogo',
    'method' => 'post',
    'class' => 'form-horizontal',
);
?>

<div class="panel event_panel" id="<?php echo $FormDivId; ?>">
          <div class="panel-heading ">
            <h4 class="panel-title medium dt-large heading_btn ">
            	<a class="sponsorLogo" data-toggle="collapse" data-parent="#accordion" href="#collapseEight"> Sponsors or Image Gallery </a>
            </h4>
          </div>
          <div id="collapseEight" class="accordion_content collapse apply_content">
            <form>
              <div class="panel-body ls_back dashboard_panel small" id="showhideformdiv<?php echo $FormDivId; ?>"> 
                <div class="panel-content_as top_padded">
                	<div class="row-fluid-15">
                    	<div class="custom_input_file">                      	
							<input id="imgLoader" accept="image/*" multiple type="file" class="customIinputFile">
                            <a href="#" class="default_btn triggerInputFile btn medium ml_zero reset_form">Upload Image</a>
                        </div>
                    </div>
                    
                	<div class="header_image_div_as mtop15">
                    	<div class="logo_image_div_as">
                        	<div class="centeral_text medium">Image</div>
                        </div>
                        <p class="central_image_text medium">Header Image Here</p>
                    </div>
                    <div class="sponsor_logo_wraper_as active sponsor_Q">
						
						<div id="canvasRow">
                            <div id="canvasPopover"></div>
                            <div id="statusBar"></div>
                          
                            <div id="canvasContainer">
                                <canvas id="canvas" width="554" height="150"></canvas>
                            </div><!--/#canvasContainer-->
						</div>
						

                    	<div class="sponsor_logo_block_as">
                        	<div class="sponsor_logo_main_as">
                            	<div class="centeral_text medium">Sponsor Logo</div>
                            </div>
                        </div>
                        <div class="sponsor_logo_block_as">
                        	<div class="sponsor_logo_main_as">
                            	<div class="centeral_text medium">Sponsor Logo</div>
                            </div>
                        </div>
                        <div class="sponsor_logo_block_as">
                        	<div class="sponsor_logo_main_as">
                            	<div class="centeral_text medium">Sponsor Logo</div>
                            </div>
                        </div>
                        <div class="sponsor_logo_block_as">
                        	<div class="sponsor_logo_main_as">
                            	<div class="centeral_text medium">Sponsor Logo</div>
                            </div>
                        </div>
                        <div class="sponsor_logo_block_as">
                        	<div class="sponsor_logo_main_as">
                            	<div class="centeral_text medium">Sponsor Logo</div>
                            </div>
                        </div>
                        <div class="sponsor_logo_block_as">
                        	<div class="sponsor_logo_main_as">
                            	<div class="centeral_text medium">Sponsor Logo</div>
                            </div>
                        </div>
                        <div class="sponsor_logo_block_as">
                        	<div class="sponsor_logo_main_as">
                            	<div class="centeral_text medium">Sponsor Logo</div>
                            </div>
                        </div>
                        <div class="sponsor_logo_block_as">
                        	<div class="sponsor_logo_main_as">
                            	<div class="centeral_text medium">Sponsor Logo</div>
                            </div>
                        </div>
                    
                    </div>
                </div>
                <div class="btn_wrapper "> <a class="pull-left scroll_top" href="#collapseEight">Top</a>                 
                  <input type="submit" onclick="save_canvas_img();return false;" class="default_btn btn pull-right medium term_condi_submit" value="Save" name="save">
                  <button class="default_btn btn pull-right medium resetInput sponsor_reset" type="button" name="form_reset">Clear</button>
                </div>
              </div>
            </form>
          </div>
        </div>
        <!--end of panel--> 
        

<script type="text/javascript">
	function save_canvas_img(){
		ZM.canvasSaveJSON();
		
		custom_popup('Event sponsors logo uploaded successfully.', true);
		//~ setTimeout(function(e){
			//~ window.location.href= '<?php echo base_url();?>event/customizeforms?t=1';
        //~ },3000);  
	}

	$('.sponsor_reset').click(function(){
		   	ZM.canvasClear();
        	setTimeout(function(e){
				var clg ='<?php echo trim($sponsor_image_object); ?>';
				ZM.doLoadJSON(clg); 
			},100);
    });
	
	$('.sponsorLogo').click(function(){
        setTimeout(function(e){ ZM.init();},50);
        
        setTimeout(function(e){
            var clg ='<?php echo trim($sponsor_image_object); ?>';
            ZM.doLoadJSON(clg); 
        },70);	
    });
</script>

<script type="text/javascript" language="javascript">
	
	function readURL2(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();
			var image_target = $(target);
			reader.onload = function (e) {
				image_target.attr('src', e.target.result).show();
			};
			image_target.show();
			$('#logo_position').val('1');
			reader.readAsDataURL(input.files[0]);
		 }
	}
	 $(function() {
		 $("#theme_sponsor_file").on("change",function(){
			readURL2(this);
		 });
	 });
</script>
