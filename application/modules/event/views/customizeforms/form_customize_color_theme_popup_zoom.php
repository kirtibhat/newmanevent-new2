<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//--------add type of registration fields---------//  
$formCustomTheme = array(
    'name'      => 'formCustomTheme',
    'id'        => 'formCustomTheme',
    'method'    => 'post',
    'class'     => 'wpcf7-form formInvitationType',
    'data-parsley-validate' => '',
  );
$theme_name = (!empty($colorThemedata['theme_name'])) ? $colorThemedata['theme_name'] : 'Custom Theme';  
$customThemeName = array(
    'name'  => 'customThemeName',
    'value' => $theme_name,
    'id'  => 'customThemeName',
    'type'  => 'text',
    'class' => 'small',
    //'placeholder' => 'Name',
    'required'  => '',
    'autocomplete' => 'off',
    'data-parsley-error-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
);


    
$event_theme_used_val  = (!empty($colorThemedata['is_theme_used'])) ? $colorThemedata['is_theme_used'] : 0;
$event_theme_screen_bg_val = !empty($colorThemedata['event_theme_screen_bg']) ? '#'.$colorThemedata['event_theme_screen_bg'] : ''; 
$event_theme_header_bg_val = !empty($colorThemedata['event_theme_header_bg']) ? '#'.$colorThemedata['event_theme_header_bg'] : ''; 
$event_theme_box_bg_val = !empty($colorThemedata['event_theme_box_bg']) ? '#'.$colorThemedata['event_theme_box_bg'] : ''; 
$event_theme_text_color_val = !empty($colorThemedata['event_theme_text_color']) ? '#'.$colorThemedata['event_theme_text_color'] : ''; 
$event_theme_link_color_val = !empty($colorThemedata['event_theme_link_color']) ? '#'.$colorThemedata['event_theme_link_color'] : ''; 
$IMAGE = !empty($colorThemedata['IMAGE']) ? $colorThemedata['IMAGE'] : ''; 
$event_theme_link_highlighted_color_val = !empty($colorThemedata['event_theme_link_highlighted_color']) ? '#'.$colorThemedata['event_theme_link_highlighted_color'] : ''; 
$fontPackageVal = !empty($colorThemedata['font_package']) ? $colorThemedata['font_package'] : ''; 
//get font family name
$fontName = getFontFamily($fontPackageVal);
$fontName = (!empty($fontName->package_name)) ? $fontName->package_name : '';
$themeid = !empty($themeid) ? $themeid : ''; 
$themeId = $themeid;



$formAddEventTheme = array(
    'name'   => 'formAddEventTheme'.$themeId,
    'id'     => 'formAddEventTheme'.$themeId,
    'method' => 'post',
    'class'  => 'form-horizontal mt10',
    'data-parsley-validate' => '',
);

$themeTypeId = array(
    'name'  => 'themeTypeId',
    'value' => $themeid,
    'id'  => 'themeTypeId',
    'type'  => 'hidden',
  );  
  
$event_theme_screen_bg = array(
    'name'  => 'event_theme_screen_bg'.$themeId,
    'value' => $event_theme_screen_bg_val,
    'id'    => 'event_theme_screen_bg'.$themeId,
    'type'  => 'text',
    'themeId'=> $themeId,
    'cssAction' => 'screen_bg',
    'placeholder' => '',
    'autocomplete' => 'off',
    //'required'  => '',
    'class' => "color  {styleElement:'event_theme_screen_bg".$themeId."'} custom_input small theme event_theme_screen_bg".$themeId."",
    'data-parsley-error-message' => lang('dash_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
);
$event_theme_header_bg = array(
    'name'  => 'event_theme_header_bg'.$themeId,
    'value' => $event_theme_header_bg_val,
    'id'    => 'event_theme_header_bg'.$themeId,
    'type'  => 'text',
    'themeId'=> $themeId,
    'cssAction' => 'header_bg',
    'placeholder' => '',
    'autocomplete' => 'off',
    //'required'  => '',
    'class' => "color  {styleElement:'event_theme_header_bg".$themeId."'} custom_input small theme event_theme_header_bg".$themeId."",
    'data-parsley-error-message' => lang('dash_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
);
$event_theme_box_bg = array(
    'name'  => 'event_theme_box_bg'.$themeId,
    'value' => $event_theme_box_bg_val,
    'id'    => 'event_theme_box_bg'.$themeId,
    'type'  => 'text',
    'themeId'=> $themeId,
    'cssAction' => 'box_bg',
    'placeholder' => '',
    'autocomplete' => 'off',
    //'required'  => '',
    'class' => "color  {styleElement:'event_theme_box_bg".$themeId."'} custom_input small theme event_theme_box_bg".$themeId."",
    'data-parsley-error-message' => lang('dash_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
);
$event_theme_text_color = array(
    'name'  => 'event_theme_text_color'.$themeId,
    'value' => $event_theme_text_color_val,
    //'id'    => 'event_theme_text_color'.$themeId,
    'type'  => 'text',
    'themeId'=> $themeId,
    'cssAction' => 'text_color',
    'placeholder' => '',
    'autocomplete' => 'off',
    //'required'  => '',
    'class' => "color  {styleElement:'event_theme_text_color".$themeId."'} custom_input small theme event_theme_text_color".$themeId."",
    'data-parsley-error-message' => lang('dash_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
);
$event_theme_link_color = array(
    'name'  => 'event_theme_link_color'.$themeId,
    'value' => $event_theme_link_color_val,
    'id'    => 'event_theme_link_color'.$themeId,
    'type'  => 'text',
    'themeId'=> $themeId,
    'cssAction' => 'link_color',
    'placeholder' => '',
    'autocomplete' => 'off',
    //'required'  => '',
    'class' => "color  {styleElement:'event_theme_link_color".$themeId."'} custom_input small theme event_theme_link_color".$themeId."",
    'data-parsley-error-message' => lang('dash_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
);
$event_theme_link_highlighted_color = array(
    'name'  => 'event_theme_link_highlighted_color'.$themeId,
    'value' => $event_theme_link_highlighted_color_val,
    'id'    => 'event_theme_link_highlighted_color'.$themeId,
    'type'  => 'text',
    'themeId'=> $themeId,
    'cssAction' => 'link_highlighted_color',
    'placeholder' => '',
    'autocomplete' => 'off',
    //'required'  => '',
    'class' => "color  {styleElement:'event_theme_link_highlighted_color".$themeId."'} custom_input small theme event_theme_link_highlighted_color".$themeId."",
    'data-parsley-error-message' => lang('dash_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
);

//get font packages
$fontPackagesData = getDataFromTabel('font_packages', '*', '', '', 'package_name', 'ASC');
$fontPackagesOptions[""] = lang('event_customize_select_font_package');
if (!empty($fontPackagesData)) {
    $fontPackagesOptions[""] = lang('event_customize_select_font_package');
    foreach ($fontPackagesData as $fontPackage) {
        $fontPackagesOptions[$fontPackage->id] = $fontPackage->package_name;
    } //end loop
} // end if
$fontPackagesOtherOptions = ' id="font_package'.$themeId.'"' . ' class="font_package'.$themeId.' temp_cls small select_poz custom-select short_field pull-right theme"  cssAction="font_family" themeId= '.$themeId.'';


if (!empty($customizeformsdata)) {
    $customizeformsdata = $customizeformsdata[0];
}
$LogoFileId = (!empty($customizeformsdata->id)) ? $customizeformsdata->id : '';
$LogoFileName = (!empty($customizeformsdata->logo_image)) ? $customizeformsdata->logo_image : '';
$LogoPosition = (!empty($customizeformsdata->logo_position)) ? $customizeformsdata->logo_position : '0';
if(!empty($LogoFileName)) {
    $logoPath = base_url().$customizeformsdata->logo_image_path.$LogoFileName;
}else {
    //$logoPath = base_url().$this->config->item('themes_path').'images/New-Top-Image-2.jpg';
    $logoPath = '';
}
$HeaderFileName = (!empty($customizeformsdata->header_image)) ? $customizeformsdata->header_image : '';
if(!empty($HeaderFileName)) {
    $headerPath = base_url().$customizeformsdata->header_image_path.$HeaderFileName;
}else {
    $headerPath = base_url().$this->config->item('themes_path').'images/header_theme1.jpg';
}

$userId = isLoginUser();
$sponsor_logo = (!empty($customizeformsdata->sponsor_image_object)) ? base_url().'media/events_sponsors_logo/user_'.$userId.'/event_'.$customizeformsdata->event_id.'/sponsors.png'  : $IMAGE.'sponsors-logos.jpg';

?>
<div id="form_customize_color_theme_popup_zoom" class="modal as_modal-large fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" >
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header theme_customizer">
        <h4 class="medium dt-large modal-title"><?php echo $theme_name ?></h4>
      </div>

      <div class="modal-body small">
        <div class="modelinner ">
			<?php echo form_open($this->uri->uri_string(),$formAddEventTheme); ?>
          <div id="theme_contener">
            <div class="theme_wrapper">
                <div class="theme_color_value">
                    <div class="row-fluid-15">
                        
                        <div class="w33">
                            <div class="cate_name">
                                <p class="small"><?php echo lang('event_theme_screen_bg'); ?></p>
                            </div>
                            <div class="color_picker">
                                <span id="event_theme_screen_bg<?php echo $themeId; ?>"></span>
                                <?php echo form_input($event_theme_screen_bg); ?>
                            </div>
                        </div>
                        
                        <div class="w33 mtop10tab">
                            <div class="cate_name">
                                <p class="small"><?php echo lang('event_theme_header_bg'); ?></p>
                            </div>
                            <div class="color_picker">
                                <span id="event_theme_header_bg<?php echo $themeId; ?>"></span>
                                <?php echo form_input($event_theme_header_bg); ?>
                            </div>
                        </div>
                        
                        <div class="w33 mtop10tab">
                            <div class="cate_name">
                                <p class="small"><?php echo lang('event_theme_box_bg'); ?></p>
                            </div>
                            <div class="color_picker">
                                <span id="event_theme_box_bg<?php echo $themeId; ?>"></span>
                                <?php echo form_input($event_theme_box_bg); ?>
                            </div>
                        </div>
                    </div>
                    <div class="row-fluid-15">                        
                        <div class="w33 mtop10tab">
                            <div class="cate_name">
                                <p class="small"><?php echo lang('event_theme_text_color'); ?></p>
                            </div>
                            <div class="color_picker">
                                <span id="event_theme_text_color<?php echo $themeId; ?>"></span>
                                <?php echo form_input($event_theme_text_color); ?>
                            </div>
                        </div>
                        <div class="w33 mtop10tab">
                            <div class="cate_name">
                                <p class="small"><?php echo lang('event_theme_link_color'); ?></p>
                            </div>
                            <div class="color_picker">
                                <span id="event_theme_link_color<?php echo $themeId; ?>"></span>
                                <?php echo form_input($event_theme_link_color); ?>
                            </div>
                        </div>
                        <div class="w33 mtop10tab">
                            <div class="cate_name">
                                <p class="small"><?php echo lang('event_theme_link_highlighted_color'); ?></p>
                            </div>
                            <div class="color_picker">
                                <span id="event_theme_link_highlighted_color<?php echo $themeId; ?>"></span>
                                <?php echo form_input($event_theme_link_highlighted_color); ?>
                            </div>
                        </div>
                    </div>                                         
                    <div class="row-fluid-15 m_btmZero">
                        <div class="w66">
                            <div class="cate_name w28">
                                <p class="small"><?php echo lang('event_theme_font'); ?></p>
                            </div>
                            <div class="w72 full_width_select as_pa_left10">
								<span class="select-wrapper"><?php echo form_dropdown('font_package'.$themeId, $fontPackagesOptions, $fontPackageVal, $fontPackagesOtherOptions); ?>
                                <span class="holder"><?php echo lang('event_customize_select_font_package'); ?></span>
                                </span>
                            </div>
                            
                        </div>                                                
                    </div>
                    <div class="row-fluid-15 m_btmZero">
                        <div class="w_100"> 
                            <?php
                                $extraCancel  = 'class="default_btn btn pull-right medium reset_form" data-dismiss="modal" ';
                                //$extraSave    = 'class="default_btn btn pull-right medium" ';

                                //echo form_submit('save',lang('comm_save'),$extraSave);
                                //echo form_submit('cancel',lang('comm_cancle'),$extraCancel);
                            ?>
                    		      <?php
							echo form_hidden('eventId', $eventId);
							echo form_hidden('themeId', $themeId);
							//echo form_hidden('event_theme_name', $event_theme_name);
						?>

                            <button class="default_btn btn pull-right medium customize_theme_popup" is_theme_used="<?php echo $event_theme_used_val ?>"  type="button" name="save" themeid="<?php echo $themeId; ?>" eventid="<?php echo $eventId; ?>"  theme_name="<?php echo $theme_name;?>"><?php echo lang('comm_save_changes'); ?></button>
                            <button class="default_btn popup_cancel submitbtn pull-right medium" data-dismiss="modal" type="button" name="form_cancel"><?php echo lang('comm_cancle'); ?></button>					 
                        </div>                                                
                    </div>
                </div>
                 <?php
                    $linkColorCss = "color:".$event_theme_link_color_val." ; font-family : '".$fontName."'";
                    $textColorCss = "color:".$event_theme_text_color_val." ; font-family : '".$fontName."'";
                    $linkHighlightedColorCss = "color:".$event_theme_link_highlighted_color_val." ; font-family : '".$fontName."'";
                    $boxHighlightedColorCss = "background:".$event_theme_link_highlighted_color_val." ; font-family : '".$fontName."'";
                    $bxBgColorCss = "background:".$event_theme_box_bg_val." ; font-family : '".$fontName."'";
                    $headerBgColorCss = "background:".$event_theme_header_bg_val." ; font-family : '".$fontName."'";
                ?>
                <div class="as_theme_box p_theme_box<?php echo $themeId; ?>" id="as_theme_box<?php echo $themeId; ?>" style="background:<?php echo $event_theme_screen_bg_val; ?>;">
                    <div class="as_theme_head p_theme_head<?php echo $themeId; ?>" id="as_theme_head<?php echo $themeId; ?>" style="background:<?php echo $event_theme_screen_bg_val; ?>;">
                        <ul class="as_small_nav">
                            <li class="p_as_small_nav<?php echo $themeId; ?>" style="<?php echo $linkColorCss; ?>">Home</li>
                            <li class="p_as_small_nav<?php echo $themeId; ?>" style="<?php echo $linkColorCss; ?>">Manage Existing Registration</li>
                            <li class="p_as_small_nav<?php echo $themeId; ?>" style="<?php echo $linkColorCss; ?>">Terms and Conditions</li>
                            <li class="p_as_small_nav<?php echo $themeId; ?>" style="<?php echo $linkColorCss; ?>">Client Website</li>
                            <li class="p_as_small_nav<?php echo $themeId; ?>" style="<?php echo $linkColorCss; ?>">Contact Us</li>
                        </ul>
                        <ul class="as_small_social">
                            <li class="as_twt"></li>
                            <li class="as_fb"></li>   
                            <li class="as_lin"></li>                                                  
                        </ul>
                    </div>
                    <div class="as_theme_banner theme_banner_zoom_Q"  eventid="<?php echo $eventId; ?>" themeid="<?php echo $themeId; ?>">
                        <div class="as_themeBannerImg themebannerimg_wrap_Q">
							<div class="dragndrop_row_Q">
								<div class="theme_logo1 logo_image_div_as  logo_img_div_Q pull-left" style="<?php if($LogoPosition=='1'){ echo 'display:inline-block;';}else{ echo 'display:none;';}?>">
									<img src="<?php echo $logoPath;?>" alt=""  style="width:65px;height:65px;"/>
								</div>
								<div class="theme_logo2 logo_image_div_as logo_image2  logo_img_div_Q" style="<?php if($LogoPosition=='2'){ echo 'display:inline-block;';}else{ echo 'display:none;';}?>">
									<img src="<?php echo $logoPath;?>" alt=""  style="width:65px;height:65px;"/>
								</div>
								<div class="theme_logo3 logo_image_div_as logo_image3  logo_img_div_Q pull-right" style="<?php if($LogoPosition=='3'){ echo 'display:inline-block;';}else{ echo 'display:none;';}?>">
									<img src="<?php echo $logoPath;?>" alt=""  style="width:65px;height:65px;"/>
								</div>
							</div>
							<img class="as_themeBannerImg_ themebannerimg_Q" src="<?php echo $headerPath; ?>">
                        </div>
                        
<!--
                        <div class="as_themeBannerImg">
                            <img class="as_themeBannerImg_" src="<?php echo $headerPath; ?>">
                        </div>
-->
                        <div class="as_themeBannerInfo p_themeBannerInfo<?php echo $themeId; ?>" style="<?php echo $headerBgColorCss; ?>">
                            <div class="as_themeLeft">
                                <div class="as_themeHeading themeHeading<?php echo $themeId; ?>" id="as_themeHeading<?php echo $themeId; ?>" >
                                    <h2 class="p_eventTitle<?php echo $themeId; ?>" id="eventTitle<?php echo $themeId; ?>"><?php echo $eventdetails->event_title; ?></h2>
                                    <p class="pOne p_eventSubTitle<?php echo $themeId; ?>" id="eventSubTitle<?php echo $themeId; ?>" ><?php echo $eventdetails->subtitle; ?></p>
                                    <p class="pTwo p_eventlocation<?php echo $themeId; ?>" id="eventlocation<?php echo $themeId; ?>" ><?php echo $eventdetails->event_venue; ?> , <?php echo $eventdetails->event_location; ?>  <?php echo $eventdetails->starttime; ?>, <?php echo $eventdetails->endtime; ?></p>
                                </div>
                                <p class="pThree p_eventDescription<?php echo $themeId; ?>" id="eventDescription<?php echo $themeId; ?>" style="<?php echo $headerBgColorCss; ?>"><?php echo $eventdetails->description; ?></p>
                            </div>
                            <div class="as_themeRight themeRight<?php echo $themeId; ?>" id="as_themeRight<?php echo $themeId; ?>" >
                                <div class="as_inputDummy">
                                    <input type="text" class="as_dummy_input"><input type="button" class="btnDummy">
                                </div>  
                                <p class="pFour p_hltd-colr<?php echo $themeId; ?>"><?php echo lang('access_id'); ?> #<span class="p_hltd-colr<?php echo $themeId; ?>">(<?php echo lang('whatsthis'); ?>)</span></p>
                                <p class="pTwo p_bx-txt-clor<?php echo $themeId; ?>" style="<?php echo $textColorCss; ?>">
                                <?php if(!empty($eventdetails->endtime)) { ?>
                                <?php echo lang('reservation_close')." "; ?><?php echo date('jS F Y',strtotime($eventdetails->endtime)); ?> </p>
                                <?php } ?>
                                <div class="as_priceVal">
                                    <p class="p_hltd-colr<?php echo $themeId; ?>" style="<?php echo $linkHighlightedColorCss ?>">&nbsp; <span class="p_hltd-colr<?php echo $themeId; ?>" style="color:<?php echo $event_theme_link_highlighted_color_val; ?>;">&nbsp;</span></p>
                                    <p class="p_hltd-colr<?php echo $themeId; ?>" style="<?php echo $linkHighlightedColorCss ?>">&nbsp; <span class="p_hltd-colr<?php echo $themeId; ?>" style="<?php echo $linkHighlightedColorCss ?>">&nbsp;</span></p>
                                </div>
                                <!--<button type="button" class="dummyWhiteBtn pull-right"></button>-->
                            </div>
                        </div>
                    <!-- end -->
                        <!--<div class="as_themeBannerTag">
                        	<p>EVENT PROGRAM</p>
                        </div>-->
                    </div>
                    <div class="as_theme_content">
                        <div class="row-fluid-15 m_btmZero">
                            <!--<div class="as_block bx-bg-color<?php echo $themeId; ?>" style="background:<?php echo $event_theme_box_bg_val; ?>;">
                                <img src="<?php echo $IMAGE; ?>sm1.png">
                                <p class="pFive bx-txt-clor<?php echo $themeId; ?>">Delegate Registration</p>
                            </div>
                            <div class="as_block bx-bg-color<?php echo $themeId; ?>" style="background:<?php echo $event_theme_box_bg_val; ?>;">
                                <img src="<?php echo $IMAGE; ?>sm2.png">
                                <p class="pFive bx-txt-clor<?php echo $themeId; ?>">Sponsor Registration</p>
                            </div>
                            <div class="as_block bx-bg-color<?php echo $themeId; ?>" style="background:<?php echo $event_theme_box_bg_val; ?>;">
                                <img src="<?php echo $IMAGE; ?>sm3.png">
                                <p class="pFive bx-txt-clor<?php echo $themeId; ?>">Exhibitor Registration</p>
                            </div>
                            <div class="as_block bx-bg-color<?php echo $themeId; ?>" style="background:<?php echo $event_theme_box_bg_val; ?>;">
                                <img src="<?php echo $IMAGE; ?>sm4.png">
                                <p class="pFive bx-txt-clor<?php echo $themeId; ?>">Speaker Registration</p>
                            </div>
                            <div class="as_block bx-bg-color<?php echo $themeId; ?>" style="background:<?php echo $event_theme_box_bg_val; ?>;">
                                <img src="<?php echo $IMAGE; ?>sm5.png">
                                <p class="pFive bx-txt-clor<?php echo $themeId; ?>">Custom Category 1</p>
                            </div>-->
                            <div class="as_block p_bx-bg-color<?php echo $themeId; ?>" style="<?php echo $bxBgColorCss; ?>"></div>
                            <div class="as_block p_bx-bg-color<?php echo $themeId; ?>" style="<?php echo $bxBgColorCss; ?>"></div>
                            <div class="highlighted as_block hltd-colr-box<?php echo $themeId; ?>" style="<?php echo $boxHighlightedColorCss; ?>">
                                <img src="<?php echo $IMAGE; ?>sm3.png">
                                <p class="pFive p_bx-txt-clor<?php echo $themeId; ?>" style="<?php echo $textColorCss; ?>">
                                <?php echo lang('register_here'); ?></p>
                            </div>
                            <div class="as_block p_bx-bg-color<?php echo $themeId; ?>" style="<?php echo $bxBgColorCss; ?>"></div>
                            <div class="as_block p_bx-bg-color<?php echo $themeId; ?>" style="<?php echo $bxBgColorCss; ?>"></div>
                            
                        </div>  
                        <div class="row-fluid-15 m_btmZero">
                            <!--<div class="as_block bx-bg-color<?php echo $themeId; ?>" style="background:<?php echo $event_theme_box_bg_val; ?>;">
                                <img src="<?php echo $IMAGE; ?>sm5.png">
                                <p class="pFive bx-txt-clor<?php echo $themeId; ?>">Custom Category 2</p>
                            </div>
                            <div class="as_block bx-bg-color<?php echo $themeId; ?>" style="background:<?php echo $event_theme_box_bg_val; ?>;">
                                <img src="<?php echo $IMAGE; ?>sm5.png">
                                <p class="pFive bx-txt-clor<?php echo $themeId; ?>">Custom Category 3</p>
                            </div>
                            <div class="as_block bx-bg-color<?php echo $themeId; ?>" style="background:<?php echo $event_theme_box_bg_val; ?>;"></div>
                            <div class="as_block bx-bg-color<?php echo $themeId; ?>" style="background:<?php echo $event_theme_box_bg_val; ?>;"></div>
                            <div class="as_block bx-bg-color<?php echo $themeId; ?>" style="background:<?php echo $event_theme_box_bg_val; ?>;"></div>
                            -->
                        </div>                                               
                    </div>
                    <div class="as_themeSponsorsLogos">
                        <img style="width:100%;" src="<?php echo $sponsor_logo; ?>">
                    </div>
                    <div class="as_themefooter">
                        <img src="<?php echo $IMAGE; ?>footer_txt.png">
                    </div>
                </div>              
            </div>
          </div>
			<?php echo form_close(); ?>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
jscolor.init();
$(document).on("change", ".temp_cls", function(e) {
	$(this).next('.holder').text($(this).find(":selected").text());
	var fonts = $(this).find(":selected").text();
	$this = $(this);
	var attr_val = $this.attr('themeId');
	$('.p_eventSubTitle'+attr_val).css("font-family", fonts);
	$('.p_eventlocation'+attr_val).css('font-family',fonts);
	$('.p_bx-txt-clor'+attr_val).css('font-family',fonts);
	$('.p_as_small_nav'+attr_val).css('font-family',fonts);
	$('.p_eventTitle'+attr_val).css('font-family',fonts);
	$('.p_hltd-colr'+attr_val).css('font-family',fonts);
});
$('.theme').change(function() {
       
       $this = $(this);
       var theme_val = $this.val();
       var attr_val = $this.attr('themeId');
       var cssAction = $this.attr('cssAction');

       if(cssAction=='screen_bg'){
           $('.p_theme_box'+attr_val).css('background', "#"+theme_val);
           $('.p_theme_head'+attr_val).css('background', "#"+theme_val);
       }
       if(cssAction=='header_bg'){
            $('.p_themeBannerInfo'+attr_val).css('background', "#"+theme_val);
            $('.p_eventDescription'+attr_val).css('background', "#"+theme_val);
       }
       if(cssAction=='box_bg'){           
            $('.p_bx-bg-color'+attr_val).css('background', "#"+theme_val);
       }
       if(cssAction=='link_color'){
           $('.p_as_small_nav'+attr_val).css('color', "#"+theme_val);
           $('.p_bx-txt-clor'+attr_val).css('color', "#"+theme_val);
       }
       if(cssAction=='link_highlighted_color'){
           $('.p_eventTitle'+attr_val).css('color', "#"+theme_val);
           $('.p_hltd-colr'+attr_val).css('color', "#"+theme_val);
       }
      
       if(cssAction=='text_color') {
            $('.p_eventSubTitle'+attr_val).css('color', "#"+theme_val);
            $('.p_eventlocation'+attr_val).css('color', "#"+theme_val);            
       }      
        //~ if(cssAction=='font_family'){
            //~ //var fonts = $(".font_package"+attr_val+" option:selected").text();
            //~ var fonts = $(".font_package"+attr_val).next('.holder').text();
            //~ //alert(fonts);
            //~ $('.eventSubTitle'+attr_val).css("font-family", fonts);
            //~ $('.eventlocation'+attr_val).css('font-family',fonts);
            //~ $('.bx-txt-clor'+attr_val).css('font-family',fonts);
            //~ $('.as_small_nav'+attr_val).css('font-family',fonts);
            //~ $('.eventTitle'+attr_val).css('font-family',fonts);
            //~ $('.hltd-colr'+attr_val).css('font-family',fonts);
        //~ }

    });
    
     $(".customize_theme_popup").click(function(){
		  var themeid     = parseInt($(this).attr('themeid'));
		  var eventid     = parseInt($(this).attr('eventid'));
		  var is_theme_used     = parseInt($(this).attr('is_theme_used'));
          var theme_name     = $(this).attr('theme_name');
		  var event_theme_screen_bg = $('.event_theme_screen_bg'+themeid).val();
		  var event_theme_header_bg = $('.event_theme_header_bg'+themeid).val();
		  var event_theme_box_bg = $('.event_theme_box_bg'+themeid).val();
		  var event_theme_text_color = $('.event_theme_text_color'+themeid).val();
		  var event_theme_link_color = $('.event_theme_link_color'+themeid).val();
		  var event_theme_link_highlighted_color = $('.event_theme_link_highlighted_color'+themeid).val();
		  var font_package = $('#font_package'+themeid).val();
		  
		  var sendData = {"eventId":eventid,"themeid":themeid,"event_theme_screen_bg":event_theme_screen_bg,"event_theme_header_bg":event_theme_header_bg,"event_theme_box_bg":event_theme_box_bg,"event_theme_text_color":event_theme_text_color,"event_theme_link_color":event_theme_link_color,"event_theme_link_highlighted_color":event_theme_link_highlighted_color,"font_package":font_package,"is_theme_used":is_theme_used,"theme_name":theme_name};
		  
		  //open_model_popup('customize_theme_popup');
		  ajaxpopupopen('customize_theme_popup','event/use_custom_theme_apply',sendData,'customize_theme_popup','');
		});
        
        $('.holder').html('<?php echo $fontName; ?>');
		
</script>
