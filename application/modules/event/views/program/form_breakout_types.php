<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//set breakout form data
$breakoutId       = $formdata->id;
$breakoutName       = (!empty($formdata->breakout_name))?$formdata->breakout_name:'';
$breakoutType       = (isset($formdata->block_type)) ? $formdata->block_type : "";
$breakoutDate         = (!empty($formdata->date))?dateFormate($formdata->date, "d F"):'';
$breakoutDateNew         = (!empty($formdata->date))?dateFormate($formdata->date, "d F y"):'';
$breakoutStartTimeValue = (!empty($formdata->start_time))?timeFormate($formdata->start_time,'h:i A'):''; 
$breakoutEndTimeValue   = (!empty($formdata->end_time))?timeFormate($formdata->end_time,'h:i A'):''; 
$numberOfBlockVal     = (!empty($formdata->number_of_blocks))?$formdata->number_of_blocks:'0';
$numberOfStreamsVal   = (!empty($formdata->number_of_streams))?$formdata->number_of_streams:'0';
$numberOfSessionVal     = (!empty($formdata->number_of_session))?$formdata->number_of_session:'0';
$moveRegistrantVal      = (!empty($formdata->registrant_move_in_stream))?'1':'0';

//get value by breakout id 
$where = array('breakout_id'=>$breakoutId);
//$breakoutBlockData = getDataFromTabel('breakout_block','*',$where,'','block','ASC');
$breakoutStreamData = getDataFromTabel('breakout_stream','*',$where,'','stream','ASC');

$breakoutSessionData = getDataFromTabel('breakout_session','*',$where,'','session','ASC');



//set value in variable
if($moveRegistrantVal){
  $moveRegistrantYesVal = true;
  $moveRegistrantNoVal = false; 
}else{
  $moveRegistrantYesVal = false;
  $moveRegistrantNoVal = true;  
}

//form create variable
$formBreakoutSetup = array(
    'name'   => 'formBreakoutSetup'.$breakoutId,
    'id'   => 'formBreakoutSetup'.$breakoutId,
    'method' => 'post',
    'class'  => 'form-horizontal',
    'data-parsley-validate' => '',
);
  
$breakoutNumberOfBlocks = array(
    'name'  => 'breakoutNumberOfBlocks'.$breakoutId,
    'value' => $numberOfBlockVal,
    'id'  => 'breakoutNumberOfBlocks'.$breakoutId,
    'type'  => 'text',
    'class' => 'width_50px clr_425968 spinner_selectbox breakoutNumberOfBlocks',
    'blockbreakoutid' => $breakoutId,
    'readonly' => '',
    'min' => '0',
);  
    
$breakoutNumberOfStreams = array(
    'name'  => 'breakoutNumberOfStreams'.$breakoutId,
    'value' => $numberOfStreamsVal,
    'id'  => 'breakoutNumberOfStreams'.$breakoutId,
    'type'  => 'number',
    'class' => 'small dark breakoutNumberOfStreams',
    'streambreakoutid'  => $breakoutId,
    'stepper-type'    => 'stream',
    'readonly' => ($numberOfStreamsVal>0) ? 'TRUE' : '',
    'min' => ($numberOfStreamsVal>0) ? $numberOfStreamsVal : '0',
    'max' => ($numberOfStreamsVal>0) ? $numberOfStreamsVal : '',
);

$blockBreakoutNumbHidden = array(
            'name'  => 'blockBreakoutNumbHidden'.$breakoutId,
            'value' => $numberOfBlockVal,
            'id'  => 'blockBreakoutNumbHidden'.$breakoutId,
            'type'  => 'hidden',
          );
              
  
$streamsBreakoutNumbHidden = array(
            'name'  => 'streamsBreakoutNumbHidden'.$breakoutId,
            'value' => $numberOfStreamsVal,
            'id'  => 'streamsBreakoutNumbHidden'.$breakoutId,
            'type'  => 'hidden',
          );  

  
$sessionPerStreamNumbHidden = array(
            'name'  => 'sessionPerStreamNumbHidden'.$breakoutId,
            'value' => $numberOfSessionVal,
            'id'  => 'sessionPerStreamNumbHidden'.$breakoutId,
            'type'  => 'hidden',
            );          
                
?>

<div class="panel event_panel co_cat co_cat_corporate">
                    <div class="panel-heading" id="programblockdivid_<?php echo $breakoutId; ?>">
                      <h4 class="panel-title medium dt-large heading_btn" >
              <a class="multiline" data-toggle="collapse" data-parent="#accordion" href="#collapseSix<?php echo $breakoutId; ?>" id="<?php echo $breakoutId; ?>">
                <?php echo ucwords($breakoutName).'<br>'.$breakoutDateNew;?> ( <?php echo $breakoutStartTimeValue; ?> - <?php echo $breakoutEndTimeValue; ?>)
              </a>
              <span class="heading_btn_wrapper">
                <a href="javascript:void(0)" class="delete_btn td_btn delete_breakout" deleteid="<?php echo $breakoutId; ?>"><span>&nbsp;</span></a>
                <a href="javascript:void(0)" class="eventsetup_btn td_btn add_edit_breakouts"  breakoutid="<?php echo $breakoutId; ?>"><span>&nbsp;</span></a>
                <a href="javascript:void(0)" class="copy_btn td_btn duplicate_breakout" eventid="<?php echo $eventId; ?>" breakoutid="<?php echo $breakoutId; ?>"><span>&nbsp;</span></a>
              </span>
                        </h4>
                    </div>
                    <div style="height: auto;" id="collapseSix<?php echo $breakoutId; ?>" class="accordion_content collapse apply_content has_subaccordion">
                      <?php
               echo form_open($this->uri->uri_string(),$formBreakoutSetup); 
               echo form_hidden('blockTypeHidden'.$breakoutId,$breakoutType);
            ?>
                         <div class="panel-body ls_back dashboard_panel small " id="showhideformdiv<?php echo $breakoutId; ?>">
                            
            <?php 
            if($breakoutType=='1'){ // plenary
                $pdata['numberOfSessionVal'] = $numberOfSessionVal;
                $pdata['breakoutId'] = $breakoutId;
                $pdata['breakoutSessionData'] = $breakoutSessionData;
                $this->load->view('breakout_planery',$pdata);

              
            }else{   //Non Plenary
                $npdata['breakoutId'] = $breakoutId;
                $npdata['blockBreakoutNumbHidden'] = $blockBreakoutNumbHidden;
                $npdata['streamsBreakoutNumbHidden'] = $streamsBreakoutNumbHidden;
                $npdata['sessionPerStreamNumbHidden'] = $sessionPerStreamNumbHidden;
                $npdata['breakoutNumberOfStreams'] = $breakoutNumberOfStreams;
                $npdata['numberOfSessionVal'] = $numberOfSessionVal;
                $npdata['breakoutNumberOfStreams'] = $breakoutNumberOfStreams;
                $npdata['breakoutStreamData'] = $breakoutStreamData;
                $npdata['breakoutSessionData'] = $breakoutSessionData;
                $this->load->view('breakout_non_planery',$npdata);
            } ?> 
                           
                <div class="btn_wrapper ">
                <a href="#collapseSix<?php echo $breakoutId; ?>" class="pull-left scroll_top"><?php echo lang('comm_top'); ?></a>
                <?php              
                  echo form_hidden('eventId', $eventId);
                  echo form_hidden('breakoutId', $breakoutId);
                  echo form_hidden('formActionName', 'programBlockSave');
                
                  //button show of save and reset
                  $formButton['saveButtonId'] = 'id="'.$breakoutId.'"'; // click button id
                  $formButton['showbutton']   = array('save','reset');
                  $formButton['labletext']  = array('save'=>lang('comm_save_changes'),'reset'=>lang('comm_reset'));
                  $formButton['buttonclass']  = array('save'=>'default_btn btn pull-right medium','reset'=>'default_btn btn pull-right medium');
                  $this->load->view('common_save_reset_button',$formButton);
                  
                ?>
              </div>    

                        </div>
                       <?php echo form_close();  ?>
                    </div>
                </div>
                <!--end of panel-->



<script type="text/javascript">
  
  /*----------breakout details save by common function----------*/
  //ajaxdatasave('formBreakoutSetup<?php echo $breakoutId; ?>','<?php echo $this->uri->uri_string(); ?>',true,true,false,false,false,'#showhideformdiv<?php echo $breakoutId; ?>','#showhideformdiv<?php echo $nextRecordId; ?>');
  ajaxdatasave('formBreakoutSetup<?php echo $breakoutId; ?>','<?php echo $this->uri->uri_string(); ?>',true,true,false,true,true,'','',false);


   /* $(document).on('click', '.timepicker', function() {
        $('.timepicker').timepicker({
          showOn: "both",
          buttonImage: "<?php echo base_url(); ?>themes/system/images/calender_icon.png",
          buttonText: 'Calendar',
          controlType: 'select',
          timeFormat: 'HH:mm',
          buttonImageOnly: true,
        });
    }); */
</script>
