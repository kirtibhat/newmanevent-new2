<?php 
if(!empty($presenterDetails)){
 $presenterId    = $presenterDetails->id;
 $presenterTitle = $presenterDetails->title;
 $presenterfname = $presenterDetails->firstname;
 $presenterlname = $presenterDetails->lastname;
}else{
 $presenterId    = "";
 $presenterTitle = "";
 $presenterfname = "";
 $presenterlname = "";  
}

//Default Benefit 
$formProgramPresenter = array(
    'name'   => 'formprogrampresenter',
    'id'   => 'formprogrampresenter',
    'method' => 'post',
    'class'  => 'wpcf7-form',
    'data-parsley-validate' => '',
);
 
$programPresentertitle = array(
    'name'    => 'presenter_title',
    'value'   => $presenterTitle,
    'id'    => 'presenter_title',
    'type'    => 'text',
    'required'  => '',
    'class'   =>'small',
    'autocomplete' => 'off',
    'data-parsley-error-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
);

$programPresenterfname = array(
    'name'    => 'presenter_fname',
    'value'   => $presenterfname,
    'id'    => 'presenter_fname',
    'type'    => 'text',
    'required'  => '',
    'class'   =>'small',
    'autocomplete' => 'off',
    'data-parsley-error-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
);  
$programPresenterlname = array(
    'name'    => 'presenter_lname',
    'value'   => $presenterlname,
    'id'    => 'presenter_lname',
    'type'    => 'text',
    'required'  => '',
    'class'   =>'small',
    'autocomplete' => 'off',
    'data-parsley-error-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
);  
?>
<div id="add_presenter_popup" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" >
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header delegates">
        <h4 class="medium dt-large modal-title">
      <?php if($isPresenterCopy=='1'){
        echo lang('program_presenter_copy');
      }else{
        echo (!empty($presenterId)) ? lang("program_presenter_edit") : lang("program_presenter_add"); 
      }
      ?>
    </h4>
      </div>

      <div class="modal-body small">
        <div class="modelinner ">

       <?php echo form_open($this->uri->uri_string(),$formProgramPresenter); ?>
       <input type="hidden" value="<?php echo $isPresenterCopy ?>" name="is_presenter_copy" />
            <div class="control-group mb10 eventdashboard_popup small">

              <div class="row-fluid">
                <label class="pull-left" for="presenter_title"><?php echo lang("program_presenter_title"); ?>  <span class="astrik">*</span>
                <span class="info_btn"><span class="field_info xsmall"><?php echo lang("program_presenter_title"); ?></span></span>
                </label>
                <?php echo form_input($programPresentertitle); ?>
              </div>
              
              <div class="row-fluid">
                <label class="pull-left" for="presenter_name"><?php echo lang("program_presenter_fname"); ?> <span class="astrik">*</span></label>
                <?php echo form_input($programPresenterfname); ?>
              </div>

              <div class="row-fluid">
                <label class="pull-left" for="presenter_lname"><?php echo lang("program_presenter_lname"); ?>    <span class="astrik">*</span></label>
                <?php echo form_input($programPresenterlname); ?>
              </div>
              
              <div class="btn_wrapper">
          <?php 
            echo form_hidden('formActionName', 'programAddPresenter');
            echo form_hidden('presenter_id',$presenterId);
            
            //button show of save and reset
            $formButton['showbutton']   = array('save');
            $formButton['labletext']  = array('save'=>lang('comm_save_changes'));
            $formButton['buttonclass']  = array('save'=>'submitbtn pull-right medium');
            
            $this->load->view('event/common_save_reset_button',$formButton);
                  
                  ?>
                  <input type="submit" class="popup_cancel submitbtn pull-right medium" value="Cancel" name="logincancel" data-dismiss="modal">
              </div>

            </div>
          <?php echo form_close(); ?>
        </div>
      </div>
    </div>
  </div>
</div>
<!--end of pop2-->
<script>
 $("#formprogrampresenter").parsley();
</script>
