<div class="panel event_panel co_cat">
  <div class="panel-heading ">
    <h4 class="panel-title medium dt-large heading_btn ">
    <a class="multiline" data-toggle="collapse" data-parent="#accordion" href="#collapseSix">
      Afternoon Block <br/> 28 Oct 14 (1.00 PM - 4.00 PM)
    </a>
    <span class="heading_btn_wrapper">
      <a href="javascript:void(0)" class="delete_btn td_btn" ><span>&nbsp;</span></a>
      <a href="javascript:void(0)" class="eventsetup_btn td_btn add_edit_breakouts"><span>&nbsp;</span></a>
      <a href="javascript:void(0)" class="copy_btn td_btn duplicate_breakout"><span>&nbsp;</span></a>
    </span>
    </h4>
    
  </div>

<div style="height: auto;" id="collapseSix" class="accordion_content collapse apply_content has_subaccordion">
    <form>
    <div class="panel-body ls_back dashboard_panel small ">
      <div class="row-fluid-15">
        <label for="info_org">Block Type <span class="astrik">*</span></label>
        <input type="text" value="Breakout" id="info_org" name="info_org" class=" small disabled" disabled>
      </div>
      <!--end of row-fluid-->

      <div class="row-fluid-15">
        <label for="refr_no">Streams  <span class="astrik">*</span>
          <span class="info_btn">
            <span class="field_info xsmall">This date is the  last day you will accept earlybird registrations
            </span>
          </span>
        </label>
        <input type="number" id="refr_no" name="refr_no" value="2" required class="small dark" step="2" max="20" min="2"/>
      </div>
      <!--end of row-fluid-->
      
      <div class="row-fluid-15">
        <label for="refr_no">Sessions per Stream  <span class="astrik">*</span>
          <span class="info_btn">
            <span class="field_info xsmall">This date is the  last day you will accept earlybird registrations
            </span>
          </span>
        </label>
        <input type="number" id="refr_no" name="refr_no" value="2" required class="small dark" step="2" max="20" min="2"/>
      </div>
      <!--end of row-fluid-->
      
      <div class="row-fluid-15">
        <label for="refr_no">Allow Stream Hopping <span class="astrik">*</span>
          <span class="info_btn">
            <span class="field_info xsmall">This date is the  last day you will accept earlybird registrations
            </span>
          </span>
        </label>
        <div class="radio_wrapper ">
          <input type="radio" name="info_reg" id="info_reg_yes" checked="">
          <label for="info_reg_yes">Yes</label>

          <input type="radio" name="info_reg" id="info_reg_no">
          <label for="info_reg_no">No</label>
        </div>
      </div>
      <!--end of row-fluid-->
      
      <div id="accordion_sub" class="panel-group sub-panel-group">
        <div class="panel event_panel sub_panel">
          <div class="panel-heading ">
            <h4 class="panel-title medium dt-large">
              <a class="" data-toggle="collapse" data-parent="#accordion_sub" href="#collapseSubOne">
                Stream 1
              </a>
            </h4>
          </div>
          <!--end of panel heading-->

          <div id="collapseSubOne" class="accordion_content collapse apply_content ">
            <div class="row-fluid-15">
              <label for="info_org">Name of Stream 1 <span class="astrik">*</span></label>
              <input type="text" value="Stream 1" id="info_org" name="info_org" class=" small">
            </div>
            <!--end of row-fluid-->

            <div class="row-fluid-15">
              <label for="info_org">Location </label>
              <input type="text" value="Location" id="info_org" name="info_org" class=" small">
            </div>
            <!--end of row-fluid-->
            
            <div class="row-fluid-15">
              <label for="info_org">Limit
                <span class="info_btn">
                  <span class="field_info xsmall">This date is the  last day you will accept earlybird registrations
                  </span>
                </span>
              </label>
              <input type="text" value="140" id="info_org" name="info_org" class=" small width_130">
            </div>
            <!--end of row-fluid-->
            
            <div class="row-fluid-15">
              <label for="info_org">Restrict Stream Access <span class="astrik">*</span>
                <span class="info_btn">
                  <span class="field_info xsmall">This date is the  last day you will accept earlybird registrations
                  </span>
                </span>
              </label>
              <div class="radio_wrapper ">
                <input type="radio" name="info_reg" id="info_reg_yes" checked="">
                <label for="info_reg_yes">Yes</label>

                <input type="radio" name="info_reg" id="info_reg_no">
                <label for="info_reg_no">No</label>
              </div>
              
              <div class="session_subcat">
                <div class="row-fluid-15">
                  <span class="checkbox_wrapper">
                    <input type="checkbox" checked="" id="mem_bird" name="mem_bird">
                    <label for="mem_bird">Attendees</label>
                  </span>
                  
                  <span class="limit_wrapper">
                    <label for="attendees_limit">Limit
                      <span class="info_btn">
                        <span class="field_info xsmall">This date is the  last day you will accept earlybird registrations
                        </span>
                      </span>                                            
                    </label>
                    <input type="text" value="1234" id="info_org" name="info_org" class=" small">
                  </span>
                  <!--end of limit wrapper-->
                </div>
                <!--end of row fluid-->

                <div class="row-fluid-15 sub_check">
                  <span class="checkbox_wrapper ">
                    <input type="checkbox" checked="" id="mem_bird" name="mem_bird">
                    <label for="mem_bird">Member</label>
                  </span>
                  
                  <span class="limit_wrapper">
                    <label for="attendees_limit">Limit
                    <span class="info_btn">
                      <span class="field_info xsmall">This date is the  last day you will accept earlybird registrations
                      </span>
                    </span>                                            
                    </label>
                    <input type="text" value="1234" id="info_org" name="info_org" class=" small">
                  </span>
                  <!--end of limit wrapper-->
                </div>
                <!--end of row fluid-->
                
                <div class="row-fluid-15 sub_check">
                  <span class="checkbox_wrapper ">
                    <input type="checkbox"  id="mem_bird" name="mem_bird">
                    <label for="mem_bird">Non-Member</label>
                  </span>
                </div>
                <!--end of row fluid-->

                <div class="row-fluid-15 sub_check">
                  <span class="checkbox_wrapper ">
                    <input type="checkbox"  id="mem_bird" name="mem_bird">
                    <label for="mem_bird">Student</label>
                  </span>
                </div>
                <!--end of row fluid-->
                
                <div class="row-fluid-15">
                  <span class="checkbox_wrapper">
                    <input type="checkbox" checked="" id="mem_bird" name="mem_bird">
                    <label for="mem_bird">Sponsors</label>
                  </span>
                </div>
                <!--end of row fluid-->

                <div class="row-fluid-15 sub_check">
                  <span class="checkbox_wrapper ">
                    <input type="checkbox" checked="" id="mem_bird" name="mem_bird">
                    <label for="mem_bird">Platinum</label>
                  </span>
                  
                  <span class="limit_wrapper">
                    <label for="attendees_limit">Limit
                    <span class="info_btn">
                      <span class="field_info xsmall">This date is the  last day you will accept earlybird registrations
                      </span>
                    </span>                                            
                    </label>
                    <input type="text" value="1234" id="info_org" name="info_org" class=" small">
                  </span>
                  <!--end of limit wrapper-->
                </div>
                <!--end of row fluid-->
                
                <div class="row-fluid-15 sub_check">
                  <span class="checkbox_wrapper ">
                    <input type="checkbox"  id="mem_bird" name="mem_bird">
                    <label for="mem_bird">Gold Plus</label>
                  </span>
                </div>
                <!--end of row fluid-->

                <div class="row-fluid-15 sub_check">
                  <span class="checkbox_wrapper ">
                    <input type="checkbox"  id="mem_bird" name="mem_bird">
                    <label for="mem_bird">Gold</label>
                  </span>
                </div>
                <!--end of row fluid--> 
                
                <div class="row-fluid-15 sub_check">
                  <span class="checkbox_wrapper ">
                    <input type="checkbox"  id="mem_bird" name="mem_bird">
                    <label for="mem_bird">Silver</label>
                  </span>
                </div>
                <!--end of row fluid-->
                
                <div class="row-fluid-15">
                  <span class="checkbox_wrapper">
                    <input type="checkbox"  id="mem_bird" name="mem_bird">
                    <label for="mem_bird">Exhibitors</label>
                  </span>
                </div>
                <!--end of row fluid-->

                <div class="row-fluid-15">
                  <span class="checkbox_wrapper">
                    <input type="checkbox"  id="mem_bird" name="mem_bird">
                    <label for="mem_bird">Speakers</label>
                  </span>
                </div>
                <!--end of row fluid-->
                
              </div>
              <!--end of session sub category-->
              
            </div>
            <!--end of row-fluid-->
            
            <hr class="panel_inner_seprator">
            
            <div class="row-fluid-15">
              <label for="info_org">Session 1 Topic <span class="astrik">*</span>
                <span class="info_btn">
                  <span class="field_info xsmall">This date is the  last day you will accept earlybird registrations
                  </span>
                </span>
              </label>
              <input type="text" value="Session 1 Topic" id="info_org" name="info_org" class=" small">
            </div>
            <!--end of row-fluid-->
            
            <div class="row-fluid-15">
              <label for="refr_no">Time <span class="astrik">*</span></label>
              <div class="time_wrapper">
                <label for="start_time">Start</label>
                <select id="start_time" class="small custom-select ">
                  <option>Select</option>
                  <option selected="">9:00 AM</option>
                </select>
                
                <label for="finish_time">Finish</label>
                <select id="finish_time" class="small custom-select ">
                  <option>Select</option>
                  <option selected="">10:30 AM</option>
                </select>
              </div>
            </div>
            <!--end of row-fluid-->
            
            <div class="row-fluid-15">
              <label for="refr_no">Number of Presenters
                <span class="info_btn">
                  <span class="field_info xsmall">This date is the  last day you will accept earlybird registrations
                  </span>
                </span>
              </label>
              <input type="number" id="refr_no" name="refr_no" value="2" required class="small dark" step="1" max="10" min="1"/>
            </div>                                        
            <!--end of row-fluid-->
            
            <div class="row-fluid-15">
              <label for="info_org">Presenter</label>
              <select class="custom-select select_fwidth">
                <option>Select</option>
                <option selected>Presenter Name</option>
              </select>
            </div>
            <!--end of row-fluid-->

            <div class="row-fluid-15">
              <label for="info_org">Presenter</label>
              <select class="custom-select select_fwidth">
                <option>Select</option>
                <option selected>Presenter Name</option>
              </select>
            </div>
            <!--end of row-fluid-->
            
            <div class="row-fluid-15">
              <label for="info_org">Description
                <span class="info_btn">
                  <span class="field_info xsmall">This date is the  last day you will accept earlybird registrations
                  </span>
                </span>
              </label>
              <textarea  class="small">Presentation Description</textarea>
            </div>
            <!--end of row-fluid-->
            
            <hr class="panel_inner_seprator">
            
            <div class="row-fluid-15">
              <label for="info_org">Session 2 Topic <span class="astrik">*</span>
                <span class="info_btn">
                  <span class="field_info xsmall">This date is the  last day you will accept earlybird registrations
                  </span>
                </span>
              </label>
              <input type="text" value="Session Topic" id="info_org" name="info_org" class=" small">
            </div>
            <!--end of row-fluid-->
            
            <div class="row-fluid-15">
              <label for="refr_no">Time <span class="astrik">*</span></label>
              <div class="time_wrapper">
                <label for="start_time">Start</label>
                <select id="start_time" class="small custom-select ">
                  <option>Select</option>
                  <option selected="">9:00 AM</option>
                </select>
                
                <label for="finish_time">Finish</label>
                <select id="finish_time" class="small custom-select ">
                  <option>Select</option>
                  <option selected="">10:30 AM</option>
                </select>
              </div>
            </div>
            <!--end of row-fluid-->
            
            <div class="row-fluid-15">
              <label for="refr_no">Number of Presenters
                <span class="info_btn">
                  <span class="field_info xsmall">This date is the  last day you will accept earlybird registrations
                  </span>
                </span>
              </label>
              <input type="number" id="refr_no" name="refr_no" value="1" required class="small dark" step="1" max="10" min="1"/>
            </div>                                        
            <!--end of row-fluid-->
            
            <div class="row-fluid-15">
              <label for="info_org">Presenter</label>
              <select class="custom-select select_fwidth">
                <option>Select</option>
                <option selected>Presenter Name</option>
              </select>
            </div>
            <!--end of row-fluid-->
            
            <div class="row-fluid-15">
              <label for="info_org">Description
                <span class="info_btn">
                  <span class="field_info xsmall">This date is the  last day you will accept earlybird registrations
                  </span>
                </span>
              </label>
              <textarea  class="small">Presentation Description</textarea>
            </div>
            <!--end of row-fluid-->

          </div>  
          <!--end of panel content-->
        </div>
        <!--end of panel-->  

        <div class="panel event_panel sub_panel">
          <div class="panel-heading ">
            <h4 class="panel-title medium dt-large">
              <a class="" data-toggle="collapse" data-parent="#accordion_sub" href="#collapseSubTwo">
                Stream 2
              </a>
            </h4>
          </div>
          <!--end of panel heading-->

          <div id="collapseSubTwo" class="accordion_content collapse apply_content ">
            <div class="row-fluid-15">
              <label for="info_org">Name of Stream 1 <span class="astrik">*</span></label>
              <input type="text" value="Stream 1" id="info_org" name="info_org" class=" small">
            </div>
            <!--end of row-fluid-->

            <div class="row-fluid-15">
              <label for="info_org">Location </label>
              <input type="text" value="Location" id="info_org" name="info_org" class=" small">
            </div>
            <!--end of row-fluid-->
            
            <div class="row-fluid-15">
              <label for="info_org">Limit
                <span class="info_btn">
                  <span class="field_info xsmall">This date is the  last day you will accept earlybird registrations
                  </span>
                </span>
              </label>
              <input type="text" value="140" id="info_org" name="info_org" class=" small width_130">
            </div>
            <!--end of row-fluid-->
            
            <div class="row-fluid-15">
              <label for="info_org">Restrict Stream Access <span class="astrik">*</span>
                <span class="info_btn">
                  <span class="field_info xsmall">This date is the  last day you will accept earlybird registrations
                  </span>
                </span>
              </label>
              <div class="radio_wrapper ">
                <input type="radio" name="info_reg" id="info_reg_yes" checked="">
                <label for="info_reg_yes">Yes</label>

                <input type="radio" name="info_reg" id="info_reg_no">
                <label for="info_reg_no">No</label>
              </div>
              
              <div class="session_subcat">
                <div class="row-fluid-15">
                  <span class="checkbox_wrapper">
                    <input type="checkbox" checked="" id="mem_bird" name="mem_bird">
                    <label for="mem_bird">Attendees</label>
                  </span>
                  
                  <span class="limit_wrapper">
                    <label for="attendees_limit">Limit
                      <span class="info_btn">
                        <span class="field_info xsmall">This date is the  last day you will accept earlybird registrations
                        </span>
                      </span>                                            
                    </label>
                    <input type="text" value="1234" id="info_org" name="info_org" class=" small">
                  </span>
                  <!--end of limit wrapper-->
                </div>
                <!--end of row fluid-->

                <div class="row-fluid-15 sub_check">
                  <span class="checkbox_wrapper ">
                    <input type="checkbox" checked="" id="mem_bird" name="mem_bird">
                    <label for="mem_bird">Member</label>
                  </span>
                  
                  <span class="limit_wrapper">
                    <label for="attendees_limit">Limit
                    <span class="info_btn">
                      <span class="field_info xsmall">This date is the  last day you will accept earlybird registrations
                      </span>
                    </span>                                            
                    </label>
                    <input type="text" value="1234" id="info_org" name="info_org" class=" small">
                  </span>
                  <!--end of limit wrapper-->
                </div>
                <!--end of row fluid-->
                
                <div class="row-fluid-15 sub_check">
                  <span class="checkbox_wrapper ">
                    <input type="checkbox"  id="mem_bird" name="mem_bird">
                    <label for="mem_bird">Non-Member</label>
                  </span>
                </div>
                <!--end of row fluid-->

                <div class="row-fluid-15 sub_check">
                  <span class="checkbox_wrapper ">
                    <input type="checkbox"  id="mem_bird" name="mem_bird">
                    <label for="mem_bird">Student</label>
                  </span>
                </div>
                <!--end of row fluid-->
                
                <div class="row-fluid-15">
                  <span class="checkbox_wrapper">
                    <input type="checkbox" checked="" id="mem_bird" name="mem_bird">
                    <label for="mem_bird">Sponsors</label>
                  </span>
                </div>
                <!--end of row fluid-->

                <div class="row-fluid-15 sub_check">
                  <span class="checkbox_wrapper ">
                    <input type="checkbox" checked="" id="mem_bird" name="mem_bird">
                    <label for="mem_bird">Platinum</label>
                  </span>
                  
                  <span class="limit_wrapper">
                    <label for="attendees_limit">Limit
                    <span class="info_btn">
                      <span class="field_info xsmall">This date is the  last day you will accept earlybird registrations
                      </span>
                    </span>                                            
                    </label>
                    <input type="text" value="1234" id="info_org" name="info_org" class=" small">
                  </span>
                  <!--end of limit wrapper-->
                </div>
                <!--end of row fluid-->
                
                <div class="row-fluid-15 sub_check">
                  <span class="checkbox_wrapper ">
                    <input type="checkbox"  id="mem_bird" name="mem_bird">
                    <label for="mem_bird">Gold Plus</label>
                  </span>
                </div>
                <!--end of row fluid-->

                <div class="row-fluid-15 sub_check">
                  <span class="checkbox_wrapper ">
                    <input type="checkbox"  id="mem_bird" name="mem_bird">
                    <label for="mem_bird">Gold</label>
                  </span>
                </div>
                <!--end of row fluid--> 
                
                <div class="row-fluid-15 sub_check">
                  <span class="checkbox_wrapper ">
                    <input type="checkbox"  id="mem_bird" name="mem_bird">
                    <label for="mem_bird">Silver</label>
                  </span>
                </div>
                <!--end of row fluid-->
                
                <div class="row-fluid-15">
                  <span class="checkbox_wrapper">
                    <input type="checkbox"  id="mem_bird" name="mem_bird">
                    <label for="mem_bird">Exhibitors</label>
                  </span>
                </div>
                <!--end of row fluid-->

                <div class="row-fluid-15">
                  <span class="checkbox_wrapper">
                    <input type="checkbox"  id="mem_bird" name="mem_bird">
                    <label for="mem_bird">Speakers</label>
                  </span>
                </div>
                <!--end of row fluid-->
                
              </div>
              <!--end of session sub category-->
              
            </div>
            <!--end of row-fluid-->
            
            <hr class="panel_inner_seprator">
            
            <div class="row-fluid-15">
              <label for="info_org">Session 1 Topic <span class="astrik">*</span>
                <span class="info_btn">
                  <span class="field_info xsmall">This date is the  last day you will accept earlybird registrations
                  </span>
                </span>
              </label>
              <input type="text" value="Session 1 Topic" id="info_org" name="info_org" class=" small">
            </div>
            <!--end of row-fluid-->
            
            <div class="row-fluid-15">
              <label for="refr_no">Time <span class="astrik">*</span></label>
              <div class="time_wrapper">
                <label for="start_time">Start</label>
                <select id="start_time" class="small custom-select ">
                  <option>Select</option>
                  <option selected="">9:00 AM</option>
                </select>
                
                <label for="finish_time">Finish</label>
                <select id="finish_time" class="small custom-select ">
                  <option>Select</option>
                  <option selected="">10:30 AM</option>
                </select>
              </div>
            </div>
            <!--end of row-fluid-->
            
            <div class="row-fluid-15">
              <label for="refr_no">Number of Presenters
                <span class="info_btn">
                  <span class="field_info xsmall">This date is the  last day you will accept earlybird registrations
                  </span>
                </span>
              </label>
              <input type="number" id="refr_no" name="refr_no" value="2" required class="small dark" step="1" max="10" min="1"/>
            </div>                                        
            <!--end of row-fluid-->
            
            <div class="row-fluid-15">
              <label for="info_org">Presenter</label>
              <select class="custom-select select_fwidth">
                <option>Select</option>
                <option selected>Presenter Name</option>
              </select>
            </div>
            <!--end of row-fluid-->

            <div class="row-fluid-15">
              <label for="info_org">Presenter</label>
              <select class="custom-select select_fwidth">
                <option>Select</option>
                <option selected>Presenter Name</option>
              </select>
            </div>
            <!--end of row-fluid-->
            
            <div class="row-fluid-15">
              <label for="info_org">Description
                <span class="info_btn">
                  <span class="field_info xsmall">This date is the  last day you will accept earlybird registrations
                  </span>
                </span>
              </label>
              <textarea  class="small">Presentation Description</textarea>
            </div>
            <!--end of row-fluid-->
            
            <hr class="panel_inner_seprator">
            
            <div class="row-fluid-15">
              <label for="info_org">Session 2 Topic <span class="astrik">*</span>
                <span class="info_btn">
                  <span class="field_info xsmall">This date is the  last day you will accept earlybird registrations
                  </span>
                </span>
              </label>
              <input type="text" value="Session Topic" id="info_org" name="info_org" class=" small">
            </div>
            <!--end of row-fluid-->
            
            <div class="row-fluid-15">
              <label for="refr_no">Time <span class="astrik">*</span></label>
              <div class="time_wrapper">
                <label for="start_time">Start</label>
                <select id="start_time" class="small custom-select ">
                  <option>Select</option>
                  <option selected="">9:00 AM</option>
                </select>
                
                <label for="finish_time">Finish</label>
                <select id="finish_time" class="small custom-select ">
                  <option>Select</option>
                  <option selected="">10:30 AM</option>
                </select>
              </div>
            </div>
            <!--end of row-fluid-->
            
            <div class="row-fluid-15">
              <label for="refr_no">Number of Presenters
                <span class="info_btn">
                  <span class="field_info xsmall">This date is the  last day you will accept earlybird registrations
                  </span>
                </span>
              </label>
              <input type="number" id="refr_no" name="refr_no" value="1" required class="small dark" step="1" max="10" min="1"/>
            </div>                                        
            <!--end of row-fluid-->
            
            <div class="row-fluid-15">
              <label for="info_org">Presenter</label>
              <select class="custom-select select_fwidth">
                <option>Select</option>
                <option selected>Presenter Name</option>
              </select>
            </div>
            <!--end of row-fluid-->
            
            <div class="row-fluid-15">
              <label for="info_org">Description
                <span class="info_btn">
                  <span class="field_info xsmall">This date is the  last day you will accept earlybird registrations
                  </span>
                </span>
              </label>
              <textarea  class="small">Presentation Description</textarea>
            </div>
            <!--end of row-fluid-->

          </div>  
          <!--end of panel content-->
        </div>
        <!--end of panel-->  
      </div>
      <!--end of panel-group-->
      
      

    </div>
    </form>
  </div>
</div>
<!--end of panel-->
