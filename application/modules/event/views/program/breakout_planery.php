<div class="row-fluid-15">
    <label for="info_org"><?php echo lang('programer_block_type'); ?> <span class="astrik">*</span></label>
    <input type="text" value="<?php echo lang('programer_block_plenary') ?>" id="info_org" name="info_org" class=" small disabled" disabled>
</div>
<input type="hidden" value="<?php echo $numberOfSessionVal; ?>" id="noSessionBreakoutNumbHidden<?php echo $breakoutId; ?>" />
    <div class="row-fluid-15 noofsession">
        <label for="refr_no">
            <?php echo lang('programer_sessions'); ?>
            <span class="astrik">*</span><span class="info_btn"><span class="field_info xsmall"><?php echo lang('programer_no_of_session'); ?></span></span>
        </label>
        <input type="number" id="noofsession<?php echo $breakoutId; ?>" sessionbreakoutid ="<?php echo $breakoutId; ?>"  name="noofsession<?php echo $breakoutId; ?>" value="<?php echo $numberOfSessionVal; ?>" required class="small dark" step="1" <?php echo ($numberOfSessionVal>0) ? 'readonly=TRUE' : ''; ?>  <?php echo ($numberOfSessionVal>0) ? 'max="'.$numberOfSessionVal.'"' : ''; ?>  min='<?php echo ($numberOfSessionVal>0) ? $numberOfSessionVal : 0; ?>'  />
    </div>
    
    
    <?php //get session data
    if(!empty($breakoutSessionData)){
      foreach($breakoutSessionData as $key=>$sessionData){
        $count=$sessionData->session;  
        $BreakoutSessionRowId   = $breakoutId.'_'.$count;
        $sessionRegistrantData  = getDataFromTabel('breakouts_session_registrants','registrant_id,session_limit ',array('session_id'=>$sessionData->id));
        
        //Convert object into array
        if(!empty($sessionRegistrantData)){
          foreach($sessionRegistrantData as $key=>$value){ ?>
            <input type="hidden" class="registrantIds" id="hiddenSelectedRegistrant<?php echo $BreakoutSessionRowId; ?>" key="<?php echo $value->registrant_id; ?>" breakoutid ="<?php echo $BreakoutSessionRowId; ?>" value="<?php echo $value->session_limit; ?>" name="selectedregistrant<?php echo $breakoutId; ?>[<?php echo $value->registrant_id; ?>]" />
          <?php }
        }
      
        // 1 means plenary ?>
    <div id="no_session_block_<?php echo $BreakoutSessionRowId; ?>">    
        <input id="breakoutSessionId<?php echo $BreakoutSessionRowId; ?>"  type="hidden" value="<?php echo $sessionData->id; ?>" name="breakoutSessionId<?php echo $breakoutId; ?>[<?php echo $count; ?>]">


        <div id="accordion_sub" class="panel-group sub-panel-group full_subpanel sub_panel">
            <div class="panel-heading" >
              <h4 class="panel-title medium ">
              <a class="no_icon"><?php echo lang('programer_session'); ?> <?php echo $count; ?></a>                  
              </h4>      
            </div>
        </div>

        <div class="row-fluid-15 pt15">
            <label for="sessionName<?php echo $BreakoutSessionRowId; ?>">Session <?php echo $count; ?> Topic <span class="astrik">*</span>
                <span class="info_btn">
                    <span class="field_info xsmall">Session <?php echo $count; ?> Topic
                    </span>
                </span>
            </label>
            <input type="text" value="<?php echo (!empty($sessionData->session_name)) ? $sessionData->session_name : 'Session '.$count.' Topic'; ?>" id="sessionName<?php echo $BreakoutSessionRowId; ?>" name="sessionName<?php echo $BreakoutSessionRowId; ?>" class=" small">
        </div>
        <!--end of row-fluid-->
        
        <div class="row-fluid-15">
                <label for="refr_no">Time <span class="astrik">*</span></label>
                <div class="time_wrapper date_wrapper d_timewrapper">
                     <span class="time1" class="date_wrapper">  
                        <label for="sessionStartTime<?php echo $BreakoutSessionRowId; ?>">Start</label>
                        <input type="text" data-format="hh:mm:ss" value="<?php echo $sessionData->start_time; ?>" class="width_100px timepicker" readonly="" required="" id="sessionStartTime<?php echo $BreakoutSessionRowId; ?>"  name="sessionStartTime<?php echo $BreakoutSessionRowId; ?>">
                        
                     </span>
                     <span class="time1" class="date_wrapper">      
                        <label for="sessionFinishTime<?php echo $BreakoutSessionRowId; ?>">Finish</label>
                        <input type="text" data-format="hh:mm:ss" value="<?php echo $sessionData->end_time; ?>" class="width_100px timepicker" readonly="" required="" id="sessionFinishTime<?php echo $BreakoutSessionRowId; ?>"  name="sessionFinishTime<?php echo $BreakoutSessionRowId; ?>">
                        
                    </span> 
                </div>
         </div>
         <!--end of row-fluid-->
        
        <!-- Session Location -->
        <div class="row-fluid-15">
            <label for="breakoutSessionVenue>">Location </label>
            <input type="text" value="<?php echo $sessionData->session_location; ?>" id="breakoutSessionVenue<?php echo $BreakoutSessionRowId; ?>" name="breakoutSessionVenue<?php echo $BreakoutSessionRowId; ?>" class=" small">
        </div>
            <!--end of row-fluid-->
                    
        <!-- Stream Limit -->
        <div class="row-fluid-15">
            <label for="info_org">Limit
                <span class="info_btn"><span class="field_info xsmall">Limit</span></span>
            </label>
            <input type="text" value="<?php echo $sessionData->session_limit; ?>" id="breakoutSessionLimit<?php echo $BreakoutSessionRowId; ?>" name="breakoutSessionLimit<?php echo $BreakoutSessionRowId; ?>" class="numValue small width_130">
        </div>
        <!--end of row-fluid-->
                    
        <!-- Restrict Stream Access -->
        <div class="row-fluid-15">
        
            <label for="info_org"><?php echo lang('programer_restrict_session_access'); ?><span class="astrik"></span>
                <span class="info_btn">
                    <span class="field_info xsmall"><?php echo lang('programer_restrict_session_access'); ?>
                    </span>
                </span>
            </label>
            <div class="radio_wrapper ">
                <input type="radio" value="1" breakoutSId="<?php echo $BreakoutSessionRowId; ?>" class="restrictStreamAccess" name="restrictSessionAccess<?php echo $BreakoutSessionRowId; ?>" value="1" id="restrictSessionAccessyes<?php echo $BreakoutSessionRowId; ?>" <?php echo ($sessionData->session_restrict_access=='1') ? 'checked=""' : ""; ?> >
                <label for="restrictSessionAccessyes<?php echo $BreakoutSessionRowId; ?>">Yes</label>

                <input type="radio" value="0" breakoutSId="<?php echo $BreakoutSessionRowId; ?>" class="restrictStreamAccess" name="restrictSessionAccess<?php echo $BreakoutSessionRowId; ?>" value="0" id="restrictSessionAccessno<?php echo $BreakoutSessionRowId; ?>" <?php echo ($sessionData->session_restrict_access=='0') ? 'checked=""' : ""; ?> >
                <label for="restrictSessionAccessno<?php echo $BreakoutSessionRowId; ?>">No</label>
            </div>
    </div>
       <!-- Category Stream Access -->
      <div class="row-fluid-15">    
        <div class="session_subcat <?php echo ($sessionData->session_restrict_access=='0') ? 'dn' : ""; ?>" id="registrantCatDiv<?php echo $BreakoutSessionRowId; ?>">
            <?php 
            
                if(!empty($catFilterList)){
                    
                    if(!empty($catFilterList)){
                        foreach($catFilterList as $key=>$list){
                            foreach($list as $cat=>$registrant){
                            ?>
                                <div class="row-fluid-15">
                                 <span class="checkbox_wrapper">
                                     <input type="checkbox" BreakoutSessionRowId="<?php echo $BreakoutSessionRowId; ?>" data-append-id="<?php echo $key; ?>_<?php echo $BreakoutSessionRowId; ?>" class="registrantCat" data-category-id="<?php echo $key; ?>" id="registrant_cat_<?php echo $key; ?>_<?php echo $BreakoutSessionRowId; ?>" name="registrant_cat_<?php echo $key; ?>_<?php echo $BreakoutSessionRowId; ?>"><label for="registrant_cat_<?php echo $key; ?>_<?php echo $BreakoutSessionRowId; ?>"><?php echo $cat; ?></label></span></div>
                                 <div id="category_list_<?php echo $key; ?>_<?php echo $BreakoutSessionRowId; ?>" data-category="<?php echo $key; ?>_<?php echo $BreakoutSessionRowId; ?>" class="dn">
                                 <?php  
                                 foreach($registrant as $registrantId => $value){
                                    //$checked = (in_array($registrantId,$sessionRegistrantData)) ? 'checked="checked"' : "";
                                  ?>
                                    <div class="row-fluid-15 sub_check"><span class="checkbox_wrapper "><input type="checkbox" value="1" id="registrantcheckbox<?php echo $BreakoutSessionRowId; ?><?php echo '_'.$registrantId; ?>" name="registrantcheckbox<?php echo $BreakoutSessionRowId; ?><?php echo '['.$registrantId.']'; ?>"><label for="registrantcheckbox<?php echo $BreakoutSessionRowId; ?><?php echo '_'.$registrantId; ?>"><?php echo $value; ?></label></span>
                                        <span class="limit_wrapper"><label for="attendees_limit">Limit</label>
                                            <input type="text" value="" id="registrantlimit<?php echo $BreakoutSessionRowId.'_'.$registrantId; ?>" name="registrantlimit<?php echo $BreakoutSessionRowId.'_'.$registrantId; ?>" class=" small">
                                        </span>
                                    </div>
                                  <?php } //end loop ?>
                                </div>
                              <?php } // end loop
                        } //end loop
                    } // end if
                } // end if
            ?>
        
        </div>
    </div>

    <hr class="panel_inner_seprator">

    <?php
      $presentersRecords = $sessionData->presenters_ids;
      $presentersList = (!empty($presentersRecords)) ? json_decode($presentersRecords) : array();
      ?>
      <div class="row-fluid-15">
        <label for="refr_no"><?php echo lang('programer_number_presenter'); ?>
          <span class="info_btn">
            <span class="field_info xsmall"><?php echo lang('programer_number_presenter'); ?>
            </span>
          </span>
        </label>
        
        <input type="number" data-fieldid="<?php echo $BreakoutSessionRowId; ?>" id="noOfPresenter<?php echo $BreakoutSessionRowId; ?>" name="noOfPresenter<?php echo $BreakoutSessionRowId; ?>" value="<?php echo $sessionData->number_of_presenters; ?>" required class="small dark presenter_class" step="1"  min="0" />
      </div> 
<!--end of row-fluid-->
      <div id="load_presenter_data_<?php echo $BreakoutSessionRowId; ?>">
      <?php 
       if(!empty($presentersRecords)){ 
         foreach($presentersList as $key=>$list){ ?>  
        <div class="row-fluid-15" id="presenters_div_<?php echo $BreakoutSessionRowId."_".$key; ?>">
          <label for="info_org"><?php echo lang('programer_presenter'); ?></label>
           
            <select id="presenters_select_<?php echo $BreakoutSessionRowId; ?>[<?php echo $key; ?>]" class="custom-select select_fwidth" name="presenters_select_<?php echo $BreakoutSessionRowId; ?>[<?php echo $key; ?>]">
              <option value="0"><?php echo lang('programer_select_presenter'); ?></option>
              <?php
              // get presenter data
               if(!empty($presenterDetails)){
                foreach($presenterDetails as $value){ 
                    $selected = ($value->id==$list) ? 'selected="selected"' : ""; 
                 ?>
                    <option value='<?php echo $value->id; ?>' <?php echo $selected; ?> ><?php echo ucfirst($value->title)." ".ucfirst($value->firstname)." ".ucfirst($value->lastname); ?></option>
                <?php } //end loop
              } // end if ?>
            </select>
          
          </div>
          <?php } // end loop ?>
      <?php } ?>
      </div>
      
      <div id="load_presenter_description_<?php echo $BreakoutSessionRowId; ?>">
        <?php 
          if(!empty($presentersRecords)){ ?>
        <div class="row-fluid-15">
          <label for="presenter_description_<?php echo $BreakoutSessionRowId; ?>">
            <?php echo lang('programer_description'); ?>
            <span class="info_btn">
              <span class="field_info xsmall"></span>
            </span>
          </label>  
          <textarea class="small" name="presenter_description<?php echo $BreakoutSessionRowId; ?>" for="presenter_description<?php echo $BreakoutSessionRowId; ?>"><?php echo $sessionData->presenters_description; ?></textarea>
        </div>
        <?php } ?>
      </div>
                 
  
    
</div>
<?php  
  }//end loop
  ?>
<?php }//end if ?>

<div class="row-fluid-15">  
       <div id="no_session_div_<?php echo $breakoutId; ?>"></div>
</div>
<!-- End no_of_session_data -->
