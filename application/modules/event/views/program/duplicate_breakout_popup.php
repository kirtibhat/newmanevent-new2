<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//--------add type fo registration fields---------//	

$breakoutNameValue 		= (!empty($breakoutdata->breakout_name))?$breakoutdata->breakout_name:''; 
$headerAction			= 		lang('event_breakout_duplicate_title'); 

$formTypeofBreakoutDuplicate = array(
		'name'	 => 'formTypeofBreakoutDuplicate',
		'id'	 => 'formTypeofBreakoutDuplicate',
		'method' => 'post',
		'class'  => 'form-horizontal mt10',
);
	
$breakoutName = array(
		'name'	=> 'breakoutName',
		'value'	=> $breakoutNameValue,
		'id'	=> 'breakoutName',
		'type'	=> 'text',
		'required'	=> '',
);
		
$breakoutId = array(
		'name'	=> 'breakoutId',
		'value'	=> $breakoutIdValue,
		'id'	=> 'breakoutId',
		'type'	=> 'hidden',
);


?>

<!-- Modal -->
<div id="duplicate_brakout_popup_div" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" >
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header delegates">
        <h4 class="medium dt-large modal-title"><?php echo $headerAction; ?></h4>
      </div>
       <div class="modal-body small">
        <div class="modelinner ">
          <?php  echo form_open(base_url('event/duplicatebreakoutsave'),$formTypeofBreakoutDuplicate); ?>
           
            <div class="control-group mb10 eventdashboard_popup small">

              <div class="row-fluid">
              	<label class="pull-left" for="block_name"><?php echo lang('event_breakout_popup_add_field'); ?>   <span class="astrik">*</span>
                <span class="info_btn"><span class="field_info xsmall"><?php echo lang('event_breakout_popup_add_field'); ?></span></span>
                </label>
				<?php echo form_input($breakoutName); ?>
              </div>
            
              <div class="btn_wrapper">
                <?php 
					echo form_hidden('eventId', $eventId);
					echo form_input($breakoutId);
					$extraCancel 	= 'class="popup_cancel submitbtn pull-right medium" data-dismiss="modal" ';
					$extraSave 	= 'class="submitbtn pull-right medium" ';
					echo form_submit('save',lang('comm_save'),$extraSave);
					echo form_submit('cancel',lang('comm_cancle'),$extraCancel);
				?>
		      </div>

            </div>
          <?php echo form_close(); ?>
        </div>
      </div>
    </div>
  </div>
</div>

