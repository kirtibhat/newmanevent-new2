<?php 
//if($limit>0){
//for($i=1;$i<=$limit;$i++){	
?>
	<div class="panel event_panel sub_panel">
	
			<div class="panel-heading ">
				<h4 class="panel-title medium dt-large">
					<a class="" data-toggle="collapse" data-parent="#accordion_sub" href="#collapseSubOne<?php echo $programBlockId.'_'.$limit; ?>">
						Stream <?php echo $limit; ?>
					</a>
				</h4>
			</div>
			<!--end of panel heading-->

			<div id="collapseSubOne<?php echo $programBlockId.'_'.$limit; ?>" class="accordion_content collapse apply_content ">
				<div class="row-fluid-15">
					<label for="info_org">Name of Stream <?php echo $limit; ?> <span class="astrik">*</span></label>
					<input type="text" value="Stream 1" id="info_org" name="info_org" class=" small">
				</div>
				<!--end of row-fluid-->

				<div class="row-fluid-15">
					<label for="info_org">Location </label>
					<input type="text" value="Location" id="info_org" name="info_org" class=" small">
				</div>
				<!--end of row-fluid-->
				
				<div class="row-fluid-15">
					<label for="info_org">Limit
						<span class="info_btn">
							<span class="field_info xsmall">This date is the  last day you will accept earlybird registrations
							</span>
						</span>
					</label>
					<input type="text" value="140" id="info_org" name="info_org" class=" small width_130">
				</div>
				<!--end of row-fluid-->
				
				<div class="row-fluid-15">
					<label for="info_org">Restrict Stream Access <span class="astrik">*</span>
						<span class="info_btn">
							<span class="field_info xsmall">This date is the  last day you will accept earlybird registrations
							</span>
						</span>
					</label>
					<div class="radio_wrapper ">
						<input type="radio" name="info_reg" id="info_reg_yes" checked="">
						<label for="info_reg_yes">Yes</label>

						<input type="radio" name="info_reg" id="info_reg_no">
						<label for="info_reg_no">No</label>
					</div>
					
					
				
					
					<div class="session_subcat">
						<div class="row-fluid-15">
							<span class="checkbox_wrapper">
								<input type="checkbox" checked="" id="mem_bird" name="mem_bird">
								<label for="mem_bird">Attendees</label>
							</span>
							
							<span class="limit_wrapper">
								<label for="attendees_limit">Limit
									<span class="info_btn">
										<span class="field_info xsmall">This date is the  last day you will accept earlybird registrations
										</span>
									</span>                                            
								</label>
								<input type="text" value="1234" id="info_org" name="info_org" class=" small">
							</span>
							<!--end of limit wrapper-->
						</div>
						<!--end of row fluid-->

						<div class="row-fluid-15 sub_check">
							<span class="checkbox_wrapper ">
								<input type="checkbox" checked="" id="mem_bird" name="mem_bird">
								<label for="mem_bird">Member</label>
							</span>
							
							<span class="limit_wrapper">
								<label for="attendees_limit">Limit
								<span class="info_btn">
									<span class="field_info xsmall">This date is the  last day you will accept earlybird registrations
									</span>
								</span>                                            
								</label>
								<input type="text" value="1234" id="info_org" name="info_org" class=" small">
							</span>
							<!--end of limit wrapper-->
						</div>
						<!--end of row fluid-->
						
						<div class="row-fluid-15 sub_check">
							<span class="checkbox_wrapper ">
								<input type="checkbox"  id="mem_bird" name="mem_bird">
								<label for="mem_bird">Non-Member</label>
							</span>
						</div>
						<!--end of row fluid-->

						<div class="row-fluid-15 sub_check">
							<span class="checkbox_wrapper ">
								<input type="checkbox"  id="mem_bird" name="mem_bird">
								<label for="mem_bird">Student</label>
							</span>
						</div>
						<!--end of row fluid-->
						
						<div class="row-fluid-15">
							<span class="checkbox_wrapper">
								<input type="checkbox" checked="" id="mem_bird" name="mem_bird">
								<label for="mem_bird">Sponsors</label>
							</span>
						</div>
						<!--end of row fluid-->

						<div class="row-fluid-15 sub_check">
							<span class="checkbox_wrapper ">
								<input type="checkbox" checked="" id="mem_bird" name="mem_bird">
								<label for="mem_bird">Platinum</label>
							</span>
							
							<span class="limit_wrapper">
								<label for="attendees_limit">Limit
								<span class="info_btn">
									<span class="field_info xsmall">This date is the  last day you will accept earlybird registrations
									</span>
								</span>                                            
								</label>
								<input type="text" value="1234" id="info_org" name="info_org" class=" small">
							</span>
							<!--end of limit wrapper-->
						</div>
						<!--end of row fluid-->
						
						<div class="row-fluid-15 sub_check">
							<span class="checkbox_wrapper ">
								<input type="checkbox"  id="mem_bird" name="mem_bird">
								<label for="mem_bird">Gold Plus</label>
							</span>
						</div>
						<!--end of row fluid-->

						<div class="row-fluid-15 sub_check">
							<span class="checkbox_wrapper ">
								<input type="checkbox"  id="mem_bird" name="mem_bird">
								<label for="mem_bird">Gold</label>
							</span>
						</div>
						<!--end of row fluid--> 
						
						<div class="row-fluid-15 sub_check">
							<span class="checkbox_wrapper ">
								<input type="checkbox"  id="mem_bird" name="mem_bird">
								<label for="mem_bird">Silver</label>
							</span>
						</div>
						<!--end of row fluid-->
						
						<div class="row-fluid-15">
							<span class="checkbox_wrapper">
								<input type="checkbox"  id="mem_bird" name="mem_bird">
								<label for="mem_bird">Exhibitors</label>
							</span>
						</div>
						<!--end of row fluid-->

						<div class="row-fluid-15">
							<span class="checkbox_wrapper">
								<input type="checkbox"  id="mem_bird" name="mem_bird">
								<label for="mem_bird">Speakers</label>
							</span>
						</div>
						<!--end of row fluid-->
						
					</div>
					<!--end of session sub category-->
				
				
					
				</div>
				<!--end of row-fluid-->
				
				<hr class="panel_inner_seprator">
				
				
					<div class="row-fluid-15">
						<label for="info_org">Session <?php echo $j; ?> Topic <span class="astrik">*</span>
							<span class="info_btn">
								<span class="field_info xsmall">This date is the  last day you will accept earlybird registrations
								</span>
							</span>
						</label>
						<input type="text" value="Session 1 Topic" id="info_org" name="info_org" class=" small">
					</div>
					<!--end of row-fluid-->
					
					<div class="row-fluid-15">
						<label for="refr_no">Time <span class="astrik">*</span></label>
						<div class="time_wrapper">
							 <span class="time1" class="date_wrapper">	
								<label for="start_time">Start</label>
								<input type="text" data-format="HH:mm:ss PP" class="width_100px timepicker" readonly="" required="" id="blockStartTime" value="" name="blockStartTime">
								<span class="add-on"><a href="javascript:void(0)" class="datepicker_btn timepicker" data-date-icon="icon-time">start</a></span>
							 </span>
							 <span class="time2" class="date_wrapper">		
								<label for="finish_time">Finish</label>
								<input type="text" data-format="HH:mm:ss PP" class="width_100px timepicker" readonly="" required="" id="blockStartTime" value="" name="blockStartTime">
								<span class="add-on"><a href="javascript:void(0)" class="datepicker_btn timepicker icon-calendar" data-date-icon="icon-time">end </a></span>
							</span>	
						</div>
					</div>
					<!--end of row-fluid-->
					
					<div class="row-fluid-15">
						<label for="refr_no">Number of Presenters
							<span class="info_btn">
								<span class="field_info xsmall">This date is the  last day you will accept earlybird registrations
								</span>
							</span>
						</label>
						<input type="number" id="refr_no" name="refr_no" value="2" required class="small dark" step="1" max="10" min="1"/>
					</div>                                        
					<!--end of row-fluid-->
					<div class="row-fluid-15">
						<label for="info_org">Presenter</label>
						<select class="custom-select select_fwidth">
							<option>Select</option>
							<option selected>Presenter Name</option>
						</select>
					</div>
					<!--end of row-fluid-->

					<div class="row-fluid-15">
						<label for="info_org">Presenter</label>
						<select class="custom-select select_fwidth">
							<option>Select</option>
							<option selected>Presenter Name</option>
						</select>
					</div>
					<!--end of row-fluid-->
                                        
					<div class="row-fluid-15">
						<label for="info_org">Description
							<span class="info_btn">
								<span class="field_info xsmall">This date is the  last day you will accept earlybird registrations
								</span>
							</span>
						</label>
						<textarea  class="small">Presentation Description</textarea>
					</div>
					<!--end of row-fluid-->
									
					<hr class="panel_inner_seprator">
				
			
				

			</div>  
			<!--end of panel content-->
		</div>
<?php // }  // end if
// } 
?>
<!--end of panel-->  

<script>
	
	$(function() {
		$('.time1').datetimepicker({
		  pickDate: false,
		  pickTime: true
		});
    });
	
	$(function() {
		$('.time2').datetimepicker({
		  pickDate: false,		
		  pickTime: true
		});
    });
