<div class="panel event_panel co_cat">
    <div class="panel-heading ">
        <h4 class="panel-title medium dt-large ">
            <a class=" " data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><?php echo lang("program_sort_presenter"); ?></a>
        </h4>
    </div>
    <div style="height: auto;" id="collapseOne" class="accordion_content collapse apply_content">
        <form>
            <div class="panel-body ls_back dashboard_panel small co_category" id="showhideformdivshowpresenterlist">
                <ul class="medium category_ul">
                    <?php if(!empty($presenterDetails)){ 
                        foreach($presenterDetails as $value){
                        ?>
                            <li><?php echo $value->title." ".$value->firstname." ".$value->lastname; ?></li>
                        <?php } // end loop 
                    }// end if ?>
                </ul>
            </div>                          
        </form>
    </div>
</div>
