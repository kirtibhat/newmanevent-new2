<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed'); 
}
/*
  |--------------------------------------------------------------------------|
  | This Field define for temparay open popup by add section id        |
  | and edit record id                           |
  |--------------------------------------------------------------------------|
 */

$tempHidden = array(
    'type' => 'hidden',
    'name' => 'tempHidden',
    'id' => 'tempHidden',
    'value' => '',
);

echo form_input($tempHidden);

$this->load->view('event/event_menus');


$catFilterList = array();
if (!empty($registrantCategory)) {
    foreach ($registrantCategory as $value) {
        $catFilterList[$value->category_id][$value->category][$value->registrant_id] = $value->registrant_name;
    }//end loop
}
?>

<div class="row-fluid-15">
    <div id="page_content" class="span9">

        <div id="accordion" class="panel-group">

            <p>
                <a href="javascript:void(0)" class="btn medium add_presenter add_big_btn event_btn_margin add_presenter_btn"  isCopyPresenter="0" presenterId="0"  ><?php echo lang('add_presenter_button'); ?></a>
                <a href="javascript:void(0)" class="btn medium add_reg_type add_big_btn"><?php echo lang('import_presenter_button'); ?></a>
            </p>

            <?php
            // load presenter details list
            /*$this->load->view('program_presenter_list');

            if (!empty($presenterDetails)) {
                foreach ($presenterDetails as $value) {
                    // load presenter details list
                    $data['presenterDetails'] = $value;
                    $this->load->view('form_program_presenter_details', $data);
                }
            }*/

            $this->load->view('program_presenters');
            ?>

            <p class="small">
                <?php echo lang('program_block_title'); ?>
                <span class="clearfix"></span>
                <a href="javascript:void(0)" class="btn medium add_program_block add_big_btn event_btn_margin add_edit_breakouts" breakoutid="0"><?php echo lang('program_block_button'); ?></a>
            </p>  


            <?php
            if (!empty($eventbreakouts)) {

                //prepare array data for current and next form hide/show  
                foreach ($eventbreakouts as $breakouts) {
                    $recordId[] = $breakouts->id;
                }

                //show breakout forms 
                $rowCount = 0;
                foreach ($eventbreakouts as $breakouts) {
                    $nextId = $rowCount + 1;
                    $sendData['formdata'] = $breakouts;
                    $sendData['formdata'] = $breakouts;
                    $sendData['catFilterList'] = $catFilterList;
                    $sendData['presenterDetails'] = $presenterDetails;
                    $sendData['nextRecordId'] = 0;
                    // daynamic load all breakouts forms
                    $this->load->view('program/form_breakout_types', $sendData);
                    $rowCount++;
                }
            }
            ?>

            <input type="hidden" value="<?php echo $eventId; ?>" id="event_id" />
        </div>
        <?php
        //next and previous button
        $buttonData['viewbutton'] = array('next', 'preview', 'back');
        $buttonData['linkurl'] = array('back' => base_url('event/setupregistrant'), 'next' => base_url('event/setupsideevents'), 'preview' => '');
        $this->load->view('common_back_next_buttons', $buttonData);
        ?>

        <!--end of page btn wrapper-->  
    </div>
    <!--end of page content-->
</div>
<!--end of row-->


<div id="opentModelBox"></div>

<!--start custom tooltip-->
<div id="tooltip_wrapper"></div>
<!--end custom tooltip-->

<script>

    ajaxdatasave('formprogrampresenter', '<?php echo $this->uri->uri_string(); ?>', true, true, false, true, false, '#showhideformdivaddpresenter', '#showhideformdivshowpresenterlist', '');

    var presenterDetails = '<div class="row-fluid-15" id="presenters_div_{Y}" ><label for="info_org">Presenter</label><select class="custom-select select_fwidth" name="presenters_select_{X}" id="presenters_select_{X}" ><option>Select</option>';

    var categoryHTML = '';

<?php
if (!empty($catFilterList)) {
    foreach ($catFilterList as $key => $list) {
        foreach ($list as $cat => $registrant) {
            ?>
                categoryHTML += '<div class="row-fluid-15"><span class="checkbox_wrapper"><input type="checkbox" data-append-id="<?php echo $key; ?>_{X}" class="registrantCat" data-category-id="<?php echo $key; ?>" id="registrant_cat_<?php echo $key; ?>_{X}" name="registrant_cat_<?php echo $key; ?>_{X}"><label for="registrant_cat_<?php echo $key; ?>_{X}"><?php echo $cat; ?></label></span></div>';
                categoryHTML += '<div id="category_list_<?php echo $key; ?>_{X}" class="dn">';
            <?php
            foreach ($registrant as $registrantId => $value) {
                ?>
                    categoryHTML += '<div class="row-fluid-15 sub_check"><span class="checkbox_wrapper "><input type="checkbox" id="registrantcheckbox{X}<?php echo '[' . $registrantId . ']'; ?>" name="registrantcheckbox{X}<?php echo '[' . $registrantId . ']'; ?>"><label for="registrantcheckbox{X}<?php echo '[' . $registrantId . ']'; ?>"><?php echo $value; ?></label></span>';
                    categoryHTML += '<span class="limit_wrapper"><label for="attendees_limit">Limit</label>';
                    categoryHTML += '<input type="text" value="" id="registrantlimit{X}<?php echo '_' . $registrantId; ?>" name="registrantlimit{X}<?php echo '_' . $registrantId; ?>" class=" small">';
                    categoryHTML += '</span></div>';
            <?php 
            } ?>
                categoryHTML += '</div>';
        <?php
        }
    }
} // end if
?>

// get presenter data
<?php if (!empty($presenterDetails)) {
    foreach ($presenterDetails as $value) {
        ?>
            presenterDetails += "<option value='<?php echo $value->id; ?>'><?php echo ucfirst($value->title) . " " . ucfirst($value->firstname) . " " . ucfirst($value->lastname); ?></option>";
    <?php 
    }
}
?>

    presenterDetails += '</select></div>';
    var registrantArray = [];
    var sreamRegistrantArray = [];
    $(document).ready(function() {


        // For Session
        $('.registrantIds').each(function(index) {
            var registrants = []; // init the array.
            registrants['key'] = $(this).attr('key');
            registrants['value'] = $(this).val();
            registrants['breakoutRowId'] = $(this).attr('breakoutid');
            registrantArray[index] = registrants;
        });

        $(".registrantCat").each(function() {
            // get assigned registrants ids //// get registrant hidden values
            var breakoutRowId = $(this).attr('BreakoutSessionRowId');

            $.each(registrantArray, function(index, registrantvalue) {
                // do your stuff here
                if (breakoutRowId == registrantvalue['breakoutRowId']) {
                    $("#registrantcheckbox" + breakoutRowId + '_' + registrantvalue['key']).attr("checked", true);
                    $("#registrantlimit" + breakoutRowId + '_' + registrantvalue['key']).val(registrantvalue['value']);
                    var registrantMainCat = $("#registrantlimit" + breakoutRowId + '_' + registrantvalue['key']).parent().parent().parent().attr('data-category');
                    $('#category_list_' + registrantMainCat).show();
                    $("#registrant_cat_" + registrantMainCat).attr('checked', true);

                }

            });

        });

        // For Stream Block
        $(".registrantSreamIds").each(function(index) {

            var registrants = []; // init the array.
            registrants['key'] = $(this).attr('key');
            registrants['value'] = $(this).val();
            registrants['breakoutRowId'] = $(this).attr('breakoutid');
            sreamRegistrantArray[index] = registrants;

        });

        $(".registrantStreamCat").each(function() {
            // get assigned registrants ids //// get registrant hidden values
            var breakoutRowId = $(this).attr('BreakoutStreamRowId');

            $.each(sreamRegistrantArray, function(index, registrantvalue) {
                // do your stuff here
                if (breakoutRowId == registrantvalue['breakoutRowId']) {
                    $("#registrantcheckbox" + breakoutRowId + '_' + registrantvalue['key']).attr("checked", true);
                    $("#registrantlimit" + breakoutRowId + '_' + registrantvalue['key']).val(registrantvalue['value']);
                    var registrantMainCat = $("#registrantlimit" + breakoutRowId + '_' + registrantvalue['key']).parent().parent().parent().attr('data-category');
                    $('#category_list_' + registrantMainCat).show();
                    $("#registrant_cat_" + registrantMainCat).attr('checked', true);

                }

            });
        });

        // Presenter Section (Add/Edit/Copy/Delete) 
        $(".add_presenter_btn").click(function() {
            var getPresenterId = parseInt($(this).attr('presenterId'));
            var getIsPresenterCopy = parseInt($(this).attr('isCopyPresenter'));

            formPostData = {'eventId': $("#event_id").val(), 'popupType': 'programPresenter', 'presenterId': getPresenterId, 'isPresenterCopy': getIsPresenterCopy};
            //call ajax popup function
            ajaxpopupopen('add_presenter_popup', 'event/programpopup', formPostData, 'add_presenter_btn');
        });

        $(".presenter_req").click(function() {
            // For set collapse hight
            $(".accordion_content").css("height", 'auto');
            var presenterId = $(this).attr('presenterId');
            if ($(this).val() == 1) {
                $("#showregistrantdiv" + presenterId).show();
            } else {
                $("#showregistrantdiv" + presenterId).hide();
            }

        });



    });

//delete type of presenter
    customconfirm('delete_presenter', 'event/deleteprogrampresenter', false, '', '', true);

///////////////////////// Program Block Section ///////////////////////////////

    $(document).on('click', '.registrantCat', function() {
        //call function to check registrant category  for session
        registrantCat(this);
    });


    $(document).on('click', '.registrantStreamCat', function() {
        //call function to check registrant category for stream
        registrantCat(this);

    });


    /*----This function is used to create duplicate copy of any breakout----*/

    $('.duplicate_breakout').click(function() {

        var breakoutId = parseInt($(this).attr('breakoutid'));
        var eventId = '<?php echo $eventId ?>';
        var sendData = {"breakoutId": breakoutId, "eventId": eventId};

        //call ajax popup breakout add and edit 
        ajaxpopupopen('duplicate_brakout_popup_div', 'event/duplicatebreakoutview', sendData, 'duplicate_breakout');

    });

    /*---This function is used to delete breakout -----*/
    customconfirm('delete_breakout', 'event/deletebreakout', '', '', '', true, true);

    /*---This function is used to delete breakout -----*/
    customconfirm('delete_breakout_session', 'event/deletebreakoutsession', '', '', '', true, true);

    /*-----Open add and edit breakout popup-------*/
    $(document).on('click', '.add_edit_breakouts', function() {

        if ($(this).attr('breakoutid') !== undefined) {
            var breakoutId = parseInt($(this).attr('breakoutid'));
            //set value in assing
            $("#tempHidden").val(breakoutId);
        } else {
            //get value form temp hidden and set
            var breakoutId = parseInt($("#tempHidden").val());
        }
        var eventId = '<?php echo $eventId ?>';
        var sendData = {"breakoutId": breakoutId, "eventId": eventId};

        //call ajax popup breakout add and edit 
        ajaxpopupopen('add_edit_brakout_popup_div', 'event/addeditbreakoutview', sendData, 'add_edit_breakouts');

    });

//save breakout add and edit data
    ajaxdatasave('formTypeofBreakout', 'event/addeditbreakoutsave', false, true, true, true, false, false, false, false);

//save breakout copy
    ajaxdatasave('formTypeofBreakoutDuplicate', 'event/duplicatebreakoutsave', false, true, true, true, false, false, false, false);

// initialize stepper 
//$(".presenter_class").stepper({customClass:"presenteradd"});  

    /*-------add daynamic bearkout number of stream--------*/

// add presenter in Session section
    $(document).on('click', '.presenteradd .stepper-step', function() {

        // For set collapse hight
        $(".accordion_content").css("height", 'auto');

        //data-fieldid

        var presenterposition = $(this).parent().find('input').attr('data-fieldid');
        var presenterVal = $(this).parent().find('input').val();
        if ($(this).hasClass('up')) {
            var presenterVal = presenterVal * 1 + 1;
            $(this).parent().find('input').val(presenterVal);

            // replace X with its ids var 
            var replaceString = /{X}/g;
            var replaceString1 = /{Y}/g;
            var replaceBy = presenterposition + '[' + presenterVal + ']';
            var replaceBy1 = presenterposition + '_' + presenterVal;
            var newpresenterDetails = presenterDetails.replace(replaceString, replaceBy);
            var newpresenterDetails = newpresenterDetails.replace(replaceString1, replaceBy1);

            $("#load_presenter_data_" + presenterposition).append(newpresenterDetails);
        } else {

            if (presenterVal >= 1) {

                // remove last div of presenter
                $("#presenters_div_" + presenterposition + "_" + presenterVal).remove();

                var presenterVal = presenterVal * 1 - 1;

            } else {
                presenterVal = 0;
            }
            $(this).parent().find('input').val(presenterVal);
        }
        if (presenterVal == 1) {
            presenterDescription = '<div class="row-fluid-15"><label for="presenter_description_' + presenterposition + '">Description<span class="info_btn"><span class="field_info xsmall"></span></span></label><textarea name="presenter_description' + presenterposition + '"  class="small"></textarea>';
            $("#load_presenter_description_" + presenterposition).html(presenterDescription);
        } else if (presenterVal == 0) {
            $("#load_presenter_description_" + presenterposition).html("");
        }

    });

// add presenter in Session section

    $(document).on('click', '.stepperCustomClass .stepper-step', function() {

        // For set collapse hight
        $(".accordion_content").css("height", 'auto');

        //data-fieldid

        var presenterposition = $(this).parent().find('input').attr('data-fieldid');
        var presenterVal = $(this).parent().find('input').val();
        if ($(this).hasClass('up')) {

            // replace X with its ids var 
            var replaceString = /{X}/g;
            var replaceString1 = /{Y}/g;
            var replaceBy = presenterposition + '[' + presenterVal + ']';
            var replaceBy1 = presenterposition + '_' + presenterVal;
            var newpresenterDetails = presenterDetails.replace(replaceString, replaceBy);
            var newpresenterDetails = newpresenterDetails.replace(replaceString1, replaceBy1);

            $("#load_presenter_data_" + presenterposition).append(newpresenterDetails);
        } else {

            // remove last div of presenter
            $("#presenters_div_" + presenterposition + "_" + (presenterVal * 1 + 1 * 1)).remove();

        }
        if (presenterVal == 1) {
            presenterDescription = '<div class="row-fluid-15"><label for="presenter_description_' + presenterposition + '">Description<span class="info_btn"><span class="field_info xsmall"></span></span></label><textarea name="presenter_description' + presenterposition + '"  class="small"></textarea>';
            $("#load_presenter_description_" + presenterposition).html(presenterDescription);
        } else if (presenterVal == 0) {
            $("#load_presenter_description_" + presenterposition).html("");
        }

    });



    var globalStreamNumber = 1;
    var globalSessionNumber = 1;


    //Click Event for number of session
    $(document).on('click', '.noofsession .stepper-step', function() {

        // For set collapse hight
        $(".accordion_content").css("height", 'auto');
        var getId = $(this).parent().find('input').attr('sessionbreakoutid');
        var getValueSession = parseInt($("#noofsession" + getId).val());
        var noSessionBreakoutNumber = parseInt($("#noSessionBreakoutNumbHidden" + getId).val());
        var breakoutId = $(this).parent().find('input').attr('breakoutId');
        //alert(getId + "===" + getValueSession + "===" + noSessionBreakoutNumber + "===" + breakoutId);
        
        var session_html = '';



        if ($(this).hasClass('up')) {
            if (getValueSession >= noSessionBreakoutNumber) {
    
                for (i = noSessionBreakoutNumber; i < getValueSession; i++) {
 
                    var noSessionPosition = i + 1;
                    var sessionFieldId = getId + '_' + noSessionPosition;

                    session_html += '<div id="no_session_block_' + sessionFieldId + '">';

                    // seprator line
                    //session_html += '<hr class="panel_inner_seprator">';


                    session_html += '<input type="hidden" name="breakoutSessionId' + getId + '[' + noSessionPosition + ']" value="0" id="breakoutSessionId' + sessionFieldId + '">';

                    session_html += '<div id="accordion_sub" class="panel-group sub-panel-group full_subpanel sub_panel"> <div class="panel-heading" > <h4 class="panel-title medium "><a  class="no_icon">Session ' + noSessionPosition +'</a></h4></div></div>';

                    
                    session_html += '<div class="row-fluid-15 pt15"><label for="sessionName' + sessionFieldId + '">Session ' + noSessionPosition + ' Topic <span class="astrik">*</span><span class="info_btn"><span class="field_info xsmall">Session per stream</span></span></label>';
                    session_html += '<input type="text" value="Session ' + noSessionPosition + ' Topic" id="info_org" name="sessionName' + sessionFieldId + '" class=" small"></div>';

                    // time 1 start time
                    session_html += '<div class="row-fluid-15"><label for="refr_no">Time <span class="astrik">*</span></label>';
                    session_html += '<div class="ime_wrapper date_wrapper d_timewrapper"><span class="time1" class="date_wrapper"><label for="start_time' + sessionFieldId + '">Start</label><input type="text" data-format="hh:mm:ss" class="width_100px timepicker" readonly="" required="" id="sessionStartTime' + sessionFieldId + '" value="" name="sessionStartTime' + sessionFieldId + '" data-parsley-error-message="Please fill out this field" data-parsley-error-class="custom_li" data-parsley-trigger="keyup"> </span>';

                    // time 2 finish time
                    session_html += '<span class="time1" class="date_wrapper"><label for="finish_time">Finish</label><input type="text" data-format="hh:mm:ss" class="width_100px timepicker" readonly="" required="" id="sessionFinishTime' + sessionFieldId + '" value="" name="sessionFinishTime' + sessionFieldId + '" data-parsley-error-message="Please fill out this field" data-parsley-error-class="custom_li" data-parsley-trigger="keyup">  </span>';
                    session_html += '</div></div>';

                    // Stream Location
                    session_html += '<div class="row-fluid-15" ><label for="breakoutSessionVenue' + sessionFieldId + '">Location <span class="astrik"></span></label><input type="text" class="small" required="" id="breakoutSessionVenue' + sessionFieldId + '" value="" name="breakoutSessionVenue' + sessionFieldId + '" data-parsley-error-message="Please fill out this field" data-parsley-error-class="custom_li" data-parsley-trigger="keyup"></div>';
                    // Steam Limit
                    session_html += '<div class="row-fluid-15" ><label for="breakoutSessionLimit' + sessionFieldId + '">Limit <span class="astrik"></span><span class="info_btn"><span class="field_info xsmall">Limit</span></span></label><input type="text" class="numValue small width_130" required="" id="breakoutSessionLimit' + sessionFieldId + '" value="" name="breakoutSessionLimit' + sessionFieldId + '"  data-parsley-type="number" data-parsley-type-message="Please enter numeric value only." data-parsley-error-class="custom_li" data-parsley-trigger="keyup"></div>';

                    // Restrict Stream Access 
                    session_html += '<div class="row-fluid-15" ><label for="info_org">Restrict Event Access  <span class="astrik"></span><span class="info_btn"><span class="field_info xsmall">Restrict Event Access</span></span></label>';
                    session_html += '<div class="radio_wrapper "><input type="radio" class="restrictStreamAccess" breakoutSId="' + sessionFieldId + '" name="restrictSessionAccess' + sessionFieldId + '" value="1" id="restrictSessionAccessyes' + sessionFieldId + '"><label for="restrictSessionAccessyes' + sessionFieldId + '">Yes</label>';
                    session_html += '<input type="radio" class="restrictStreamAccess" breakoutSId="' + sessionFieldId + '" name="restrictSessionAccess' + sessionFieldId + '" value="0" id="restrictSessionAccessno' + sessionFieldId + '" checked=""><label for="restrictSessionAccessno' + sessionFieldId + '">No</label></div>';//radio_wrapper end

                    // replace X with its ids var 
                    var replaceString = /{X}/g;
                    var newcategoryHTML = categoryHTML.replace(replaceString, sessionFieldId);
                    session_html += '<div class="row-fluid-15"><div class="session_subcat dn" id="registrantCatDiv' + sessionFieldId + '">' + newcategoryHTML + '</div></div>';
                    
                    session_html +='<hr class="panel_inner_seprator">';
                    session_html +='<div class="row-fluid-15"><label for="noOfPresenter'+sessionFieldId+'">Number of Presenters<span class="info_btn"><span class="field_info xsmall"></span></span></label>';
                    session_html +='<div class="stepper presenteradd">';
                    session_html +='<input type="number" id="noOfPresenter'+sessionFieldId+'" name="noOfPresenter'+sessionFieldId+'" value="0" required class="small dark" step="1" min="0" data-fieldid="'+sessionFieldId+'" />';
                    session_html +='<span class="stepper-step up"> </span><span class="stepper-step down"> </span>';
                    session_html +='</div></div>';
                    

                    session_html += '<div id="load_presenter_data_' + sessionFieldId + '"></div>';
                    session_html += '<div id="load_presenter_description_' + sessionFieldId + '"></div>';

                    session_html += '</div>';
                
                } 

                    
                   
                $("#no_session_div_" + getId).append(session_html);

            }
        } else {
            var getValueSession = (getValueSession * 1) + 1;
            $("#no_session_block_" + getId + "_" + getValueSession).remove();
            if (getValueSession === 1) {
                getValueSession = 0;
            }
        }
        $("#noSessionBreakoutNumbHidden" + getId).val(getValueSession);

        $('.timepicker').timepicker({
          showOn: "both",
          buttonImage: "<?php echo base_url(); ?>themes/system/images/calender_icon.png",
          buttonText: 'Calendar',
          controlType: 'select',
          timeFormat: 'HH:mm',
          buttonImageOnly: true,
        });
        
    });

// Add Stream block
    $(document).on('click', '.addstream .stepper-step', function() {

        /*
         alert("hii1131");
         var fewSeconds = 50; 
         $(this).attr('disabled', true);    
         setTimeout(function(){
         //$(".stepper-step").prop('disabled', false);     
         }, fewSeconds*1000); 
         
         */

        // For set collapse hight
        $(".accordion_content").css("height", 'auto');

        var getId = $(this).parent().find('input').attr('streambreakoutid');

        var streamLimit = parseInt($("#breakoutNumberOfStreams" + getId).val());
        var getValueSession = parseInt($("#breakoutNumberOfSession" + getId).val());
        var streamsBreakoutNumber = parseInt($("#streamsBreakoutNumbHidden" + getId).val());
        var sessionPerStreamNumbHidden = parseInt($("#sessionPerStreamNumbHidden" + getId).val());

        var stream_html = '';
        if ($(this).hasClass('up')) {
            if (streamLimit > streamsBreakoutNumber) {

                for (i = streamsBreakoutNumber; i < streamLimit; i++) {

                    var streamPosition = i + 1;
                    var streamFieldId = getId + '_' + streamPosition;

                    stream_html += '<input type="hidden" name="breakoutStreamId' + getId + '[' + streamPosition + ']" value="0" id="breakoutStreamId' + streamFieldId + '">';
                    stream_html += '<div class="panel event_panel sub_panel" id="stream_position_' + streamPosition + '">';
                    stream_html += '<div class="panel-heading darkcolor"><h4 class="panel-title medium dt-large"><a class="" data-toggle="collapse" data-parent="#accordion_sub" href="#collapseSubOne' + getId + '_' + streamPosition + '"> Stream ' + streamPosition + ' </a></h4></div>';

                    stream_html += '<div id="collapseSubOne' + getId + '_' + streamPosition + '" class="accordion_content collapse apply_content ">';

                    // Name of stream
                    stream_html += '<div class="row-fluid-15"><label for="breakoutStreamName' + streamFieldId + '">Name of Stream ' + streamPosition + ' <span class="astrik">*</span></label><input type="text" class="streamtitlechange small" breakoutid="' + getId + '" streamrowid="' + streamPosition + '" required="" id="breakoutStreamName' + streamFieldId + '" value="" name="breakoutStreamName' + streamFieldId + '"  data-parsley-error-message="Please fill out this field" data-parsley-error-class="custom_li" data-parsley-trigger="keyup"></div>';
                    // Stream Location
                    stream_html += '<div class="row-fluid-15" ><label for="breakoutStreamName' + streamFieldId + '">Location <span class="astrik"></span></label><input type="text" class="small" required="" id="breakoutStreamVenue' + streamFieldId + '" value="" name="breakoutStreamVenue' + streamFieldId + '"  data-parsley-error-message="Please fill out this field" data-parsley-error-class="custom_li" data-parsley-trigger="keyup"></div>';

                    stream_html += '<div class="row-fluid-15 addsessionstream"><label for="breakoutStreamSession">Sessions <span class="astrik">*</span></label><div class="stepper "><input type="number" id="breakoutNumberOfSession'  + streamFieldId + '" streambreakoutid ="' + getId + '" streamFieldId="'+ streamFieldId+'"  name="breakoutNumberOfSession' + streamFieldId + '" value="0"  class="small dark stepper-input" step="1" max="" min="0" data-parsley-error-message="Please fill out this field" data-parsley-error-class="custom_li" data-parsley-trigger="keyup"  /><span class="stepper-step up"></span> <span class="stepper-step down"></span> </div></div>';

                    
                    
                    // Steam Limit
                    stream_html += '<div class="row-fluid-15" ><label for="breakoutStreamName' + streamFieldId + '">Limit <span class="astrik"></span><span class="info_btn"><span class="field_info xsmall">Limit</span></span></label><input type="text" class="numValue small width_130" required="" id="breakoutLimit' + streamFieldId + '" value="" name="breakoutLimit' + streamFieldId + '"   data-parsley-type="number" data-parsley-type-message="Please enter numeric value only." data-parsley-error-class="custom_li" data-parsley-trigger="keyup"></div>';

                    // Restrict Stream Access 
                    stream_html += '<div class="row-fluid-15" ><label for="info_org">Restrict Event Access  <span class="astrik"></span><span class="info_btn"><span class="field_info xsmall">Restrict Event Access</span></span></label>';
                    stream_html += '<div class="radio_wrapper "><input type="radio" value="1" class="restrictStreamAccess" breakoutSId="' + streamFieldId + '" name="restrictStreamAccess' + streamFieldId + '" value="1" id="restrictStreamAccessyes' + streamFieldId + '"><label for="restrictStreamAccessyes' + streamFieldId + '">Yes</label>';
                    stream_html += '<input type="radio" value="0" class="restrictStreamAccess" breakoutSId="' + streamFieldId + '" name="restrictStreamAccess' + streamFieldId + '" value="0" id="restrictStreamAccessno' + streamFieldId + '"  checked=""><label for="restrictStreamAccessno' + streamFieldId + '">No</label></div>';//radio_wrapper end

                    stream_html += '</div>'; // Restrict Stream Access  end

                    // replace X with its ids var 
                    var replaceString = /{X}/g;
                    var newcategoryHTML = categoryHTML.replace(replaceString, streamFieldId);
                    stream_html += '<div class="row-fluid-15"><div class="session_subcat dn" id="registrantCatDiv' + streamFieldId + '">' + newcategoryHTML + '</div></div>';

                    // seprator line
                    //stream_html += '<hr class="panel_inner_seprator">';
                    stream_html += '<div id="session_div_' + streamFieldId + '">';

                    if (getValueSession > 0) {
                        for (j = 1; j <= getValueSession; j++) {


                            var streamSessionFieldId = streamFieldId + '_' + j;

                            stream_html += '<div id="session_block_' + streamSessionFieldId + '">';

                            stream_html += '<div id="accordion_sub" class="panel-group sub-panel-group full_subpanel sub_panel"> <div class="panel-heading" > <h4 class="panel-title medium "><a  class="no_icon">Session ' + j +'</a></h4></div></div>';
                            
                            stream_html += '<div class="row-fluid-15 pt15"><label for="sessionName' + streamSessionFieldId + '">Session ' + j + ' Topic <span class="astrik">*</span><span class="info_btn"><span class="field_info xsmall">Session per stream</span></span></label>';
                            stream_html += '<input type="text" value="Session ' + j + ' Topic" id="info_org" name="sessionName' + streamSessionFieldId + '" class=" small"></div>';

                            // time 1 start time
                            stream_html += '<div class="row-fluid-15"><label for="refr_no">Time <span class="astrik">*</span></label>';
                            stream_html += '<div class="time_wrapper date_wrapper d_timewrapper"><span class="time1" class="date_wrapper"><label for="start_time' + streamSessionFieldId + '">Start</label><input type="text" data-format="hh:mm:ss" class="width_100px timepicker" readonly="" required="" id="sessionStartTime' + streamSessionFieldId + '" value="" name="sessionStartTime' + streamSessionFieldId + '"  data-parsley-error-message="Please fill out this field" data-parsley-error-class="custom_li" data-parsley-trigger="keyup">  </span>';

                            stream_html += '<input type="hidden" name="breakoutSessionId' + getId + '_' + streamPosition + '[' + j + ']" value="0" id="breakoutSessionId' + streamSessionFieldId + '">';


                            // time 2 finish time
                            stream_html += '<span class="time1" class="date_wrapper"><label for="finish_time">Finish</label><input type="text" data-format="hh:mm:ss" class="width_100px timepicker" readonly="" required="" id="sessionFinishTime' + streamSessionFieldId + '" value="" name="sessionFinishTime' + streamSessionFieldId + '"  data-parsley-error-message="Please fill out this field" data-parsley-error-class="custom_li" data-parsley-trigger="keyup"> </span>';
                            stream_html += '</div></div>';
                            //stream_html +='<span id="presenter_div_'+streamSessionFieldId+'">';
                            stream_html += '<div class="row-fluid-15"><label for="noOfPresenter' + streamSessionFieldId + '">Number of Presenters<span class="info_btn"><span class="field_info xsmall"></span></span></label>';
                            stream_html += '<div class="stepper presenteradd">';
                            stream_html += '<input type="number" id="noOfPresenter' + streamSessionFieldId + '" name="noOfPresenter' + streamSessionFieldId + '" value="0" required class="small dark" step="1" min="0" data-fieldid="' + streamSessionFieldId + '" />';
                            stream_html += '<span class="stepper-step up"> </span><span class="stepper-step down"> </span>';
                            stream_html += '</div></div>';

                            stream_html += '<div id="load_presenter_data_' + streamSessionFieldId + '"></div>';
                            stream_html += '<div id="load_presenter_description_' + streamSessionFieldId + '"></div>';
                            //stream_html +='</span>';

                            //stream_html += '<hr class="panel_inner_seprator">';
                            stream_html += '</div>';
                        }

                        globalSessionNumber++;
                    }
                    stream_html += '</div>';


                    stream_html += '</div>'; // accordion_content close div
                    stream_html += '</div>'; // event_panel close div


                    globalStreamNumber = streamPosition;
                }


                if (streamsBreakoutNumber == 0) {
                    $('#stream_data' + getId).append(stream_html);
                } else {
                    if (streamsBreakoutNumber < streamPosition) {
                        $('#stream_data' + getId).append(stream_html);
                    }
                }

            }


        } else { // down  

            $("#stream_position_" + (streamLimit + 1)).remove();
            var streamLimit = (streamLimit * 1) + 1;
            $("#breakoutStreamId" + getId + "_" + streamLimit).remove();
            if (streamLimit === 1) {
                streamLimit = 0;
            }
        }

        $("#streamsBreakoutNumbHidden" + getId).val(streamLimit);
        $("#sessionPerStreamNumbHidden" + getId).val(getValueSession);

    });

    $(document).on('click', '.restrictStreamAccess', function() {
        // For set collapse hight
        $(".accordion_content").css("height", 'auto');
        var breakoutsid = $(this).attr('breakoutsid');
        if ($(this).val() == "1") {
            $('#registrantCatDiv' + breakoutsid).show();
        } else {
            $('#registrantCatDiv' + breakoutsid).hide();
        }
    });


    $(document).on('click', '.restrictSessionAccess', function() {
        // For set collapse hight
        $(".accordion_content").css("height", 'auto');
        var breakoutsid = $(this).attr('breakoutsid');
        if ($(this).val() == "1") {
            $('#registrantCatDiv' + breakoutsid).show();
        } else {
            $('#registrantCatDiv' + breakoutsid).hide();
        }
    });

// Add Stream block
    /*$(document).on('click', '.timepicker', function() {
        $('.timepicker').timepicker({
          showOn: "both",
          buttonImage: "<?php echo base_url(); ?>themes/system/images/calender_icon.png",
          buttonText: 'Calendar',
          controlType: 'select',
          timeFormat: 'HH:mm',
          buttonImageOnly: true,
        });
    });*/

// Add Session block
        $(document).on('click', '.addsessionstream .stepper-step', function() {
       
        // For set collapse hight
        $(".accordion_content").css("height", 'auto');

        var getId = $(this).parent().find('input').attr('streambreakoutid');
        var streamLimit = parseInt($("#breakoutNumberOfStreams" + getId).val());
        var streamFieldId = $(this).parent().find('input').attr('streamFieldId');
        
        var breakoutNumberOfSession = parseInt($("#breakoutNumberOfSession" + streamFieldId).val());
        //alert("Here" + getValueSession);
        if ($(this).hasClass('up')) { // increment
            $("#breakoutNumberOfSession" + streamFieldId).val(breakoutNumberOfSession + 1);
        } else {
            $("#breakoutNumberOfSession" + streamFieldId).val(breakoutNumberOfSession - 1);
        }                                                                                   
        var getValueSession = parseInt($("#breakoutNumberOfSession" + streamFieldId).val());
        var streamsBreakoutNumber = parseInt($("#streamsBreakoutNumbHidden" + getId).val());
        var sessionPerStreamNumbHidden = parseInt($("#sessionPerStreamNumbHidden" + getId).val());
        if(!sessionPerStreamNumbHidden){
            sessionPerStreamNumbHidden = 0;
        }
        //alert("Here" + getValueSession + sessionPerStreamNumbHidden);

        var stream_html = '';
        if ($(this).hasClass('up')) {

            // For session 
            if (getValueSession > sessionPerStreamNumbHidden) {
                //to add session fields per stream


                //alert(sessionPerStreamNumbHidden);
                //k = streamLimit;

                //for (k = 1; k <= streamLimit; k++) {
                    var session_html = '';
                    //for (j = sessionPerStreamNumbHidden; j < getValueSession; j++) {
                        
                        //alert("here");
                        var sessionPosition = getValueSession;//j + 1;
                        var sessionFieldId = getId + '_' + sessionPosition;


                        

                        session_html += '<div id="session_block_' + sessionFieldId + '" class="pt15">';

                        session_html += '<div id="accordion_sub" class="panel-group sub-panel-group full_subpanel sub_panel "> <div class="panel-heading" >     <h4 class="panel-title medium "> <a>Session ' + sessionPosition + '</a>  </h4>   </div> </div>';
                        
                        session_html += '<div class="pt15 sessionName' + sessionFieldId + '"><label for="info_org">Session ' + sessionPosition + ' Topic <span class="astrik">*</span><span class="info_btn"><span class="field_info xsmall">Session per stream</span></span></label></div>';
                        session_html += '<input type="text" value="Session ' + sessionPosition + ' Topic" id="info_org" name="sessionName' + sessionFieldId + '" class=" small">';

                        // time 1 start time
                        session_html += '<div class="row-fluid-15"><label for="refr_no">Time <span class="astrik">*</span></label>';
                        session_html += '<div class="time_wrapper date_wrapper d_timewrapper"><span class="time1" class="date_wrapper"><label for="start_time' + sessionFieldId + '">Start</label><input type="text" data-format="hh:mm:ss" class="width_100px timepicker" readonly="" required="" id="sessionStartTime' + sessionFieldId + '" value="" name="sessionStartTime' + sessionFieldId + '"  data-parsley-error-message="Please fill out this field" data-parsley-error-class="custom_li" data-parsley-trigger="keyup"> </span>';

                        session_html += '<input type="hidden" name="breakoutSessionId' + getId + '_' + '[' + sessionPosition + ']" value="0" id="breakoutSessionId' + getId + '_' + sessionPosition + '">';

                        // time 2 finish time
                        session_html += '<span class="time1" class="date_wrapper"><label for="finish_time">Finish</label><input type="text" data-format="hh:mm:ss" class="width_100px timepicker" readonly="" required="" id="sessionFinishTime' + sessionFieldId + '" value="" name="sessionFinishTime' + sessionFieldId + '"  data-parsley-error-message="Please fill out this field" data-parsley-error-class="custom_li" data-parsley-trigger="keyup">  </span>';
                        session_html += '</div></div>';

                        //session_html +='<span id="presenter_div_'+sessionFieldId+'">';
                        session_html += '<div class="row-fluid-15"><label for="noOfPresenter' + sessionFieldId + '">Number of Presenters<span class="info_btn"><span class="field_info xsmall"></span></span></label>';
                        session_html += '<div class="stepper presenteradd">';
                        session_html += '<input type="number" id="noOfPresenter' + sessionFieldId + '" name="noOfPresenter' + sessionFieldId + '" value="0" required class="small dark" step="1" min="0" data-fieldid="' + sessionFieldId + '" />';
                        session_html += '<span class="stepper-step up"> </span><span class="stepper-step down"> </span>';
                        session_html += '</div></div>';

                        session_html += '<div id="load_presenter_data_' + sessionFieldId + '"></div>';
                        session_html += '<div id="load_presenter_description_' + sessionFieldId + '"></div>';
                        //session_html +='</span>';

                        //session_html += '<hr class="panel_inner_seprator">';

                        session_html += '</div>';

                        
                    //}// end loop

                    $('#session_div_' + streamFieldId ).append(session_html);

                    $('.timepicker').timepicker({
                      showOn: "both",
                      buttonImage: "<?php echo base_url(); ?>themes/system/images/calender_icon.png",
                      buttonText: 'Calendar',
                      controlType: 'select',
                      timeFormat: 'HH:mm',
                      buttonImageOnly: true,
                    });

                //} // end loop

            }


        } else { // down
            var getValueSession = parseInt($("#breakoutNumberOfSession" + streamFieldId).val());
            var removeBlockID = getId + '_'+(getValueSession+1);
            $("#session_block_" + removeBlockID).remove();
            //alert(getValueSession+1);    
            /*for (k = 1; k <= streamLimit; k++) {
                var sessionPosition = sessionPerStreamNumbHidden;
               //var sessionFieldId = getId + '_' + k + '_' + sessionPosition;
                //$("#session_block_" + sessionFieldId).remove();
            } // end loop
            */
            if (getValueSession == 1) {
                getValueSession = 0;
            }
        }

        $("#sessionPerStreamNumbHidden" + streamFieldId).val(getValueSession);

    });

    function registrantCat(obj) {
        // For set collapse hight
        $(".accordion_content").css("height", 'auto');

        var divId = $(obj).attr('data-append-id');
        if ($(obj).prop("checked")) {
            $("#category_list_" + divId).show();
        } else {
            $("#category_list_" + divId).hide();
        }
    }

    $(document).ready(function() {
        $('.timepicker').timepicker({
          showOn: "both",
          buttonImage: "<?php echo base_url(); ?>themes/system/images/calender_icon.png",
          buttonText: 'Calendar',
          controlType: 'select',
          timeFormat: 'HH:mm',
          buttonImageOnly: true,
        });
    });
</script>

