<?php 
//Default Benefit 
$formProgramPresenterDetails = array(
    'name'   => 'formProgramPresenterDetails'.$presenterDetails->id,
    'id'   => 'formProgramPresenterDetails'.$presenterDetails->id,
    'method' => 'post',
    'class'  => 'wpcf7-form',
    'data-parsley-validate' => '',
);

 
$programPresenterorganisation = array(
    'name'    => 'presenterOrganisation'.$presenterDetails->id,
    'value'   => $presenterDetails->organisation,
    'id'    => 'presenterOrganisation'.$presenterDetails->id,
    'type'    => 'text',
    //'required'  => '',
    'class'   =>'small',
    'autocomplete' => 'off',
    'data-parsley-error-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
);

$programPresenteremail = array(
    'name'    => 'presenterEmail'.$presenterDetails->id,
    'value'   => $presenterDetails->email,
    'id'    => 'presenterEmail'.$presenterDetails->id,
    'type'    => 'email',
    //'required'  => '',
    'class'   =>'small presenterEmail',
    'autocomplete' => 'off',
    'data-parsley-error-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
);  

$programPresenteremailConfirm = array(
    'name'    => 'presenterEmailConfirm'.$presenterDetails->id,
    'value'   => $presenterDetails->email,
    'id'    => 'presenterEmailConfirm'.$presenterDetails->id,
    //'required'  => '',
    'class'=>'small',
    'autocomplete' => 'off',
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-equalto' => '#presenterEmail'.$presenterDetails->id,
    'data-parsley-equalto-message' => lang('common_confirm_email_equal'),
    'data-parsley-trigger' => 'keyup',
);  
      
$programPresenterphone = array(
    'name'      => 'presenterPhone'.$presenterDetails->id,
    'value'     => $presenterDetails->mobile,
    'id'      => 'presenterPhone'.$presenterDetails->id,
    'type'      => 'text',
    //'required'    => '',
    'placeholder' =>'123456789',
    'class'     =>'small  contact_input phoneValue',
    'autocomplete'  => 'off',
    'data-parsley-error-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'change',
);  

$programPresentertopic = array(
    'name'    => 'presenterTopic'.$presenterDetails->id,
    'value'   => $presenterDetails->topic,
    'id'    => 'presenterTopic'.$presenterDetails->id,
    'type'    => 'text',
    //'required'  => '',
    'class'   =>'small',
    'autocomplete' => 'off',
    'data-parsley-error-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
);

$programPresenterdes = array(
    'name'    => 'presenterDescription'.$presenterDetails->id,
    'value'   => $presenterDetails->presentation_description,
    'id'    => 'presenterDescription'.$presenterDetails->id,
    'type'    => 'text',
    'class'   =>'small',
    'rows'    =>'3',
    'cols'    =>'40'
);

$programPresenterresrequiredyes = array(
    'name'    => 'presenterRegRequired'.$presenterDetails->id,
    'value'   => '1',
    'id'    => 'presenterRegRequiredYes'.$presenterDetails->id,
    'type'    => 'radio',
    'required'  => '',
    'class'   =>'small presenter_req',
    'autocomplete' => 'off',
    'data-parsley-error-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
    'checked'        => '',
    'presenterId'     => $presenterDetails->id  
);
$dn = '';
if($presenterDetails->is_registration_required=='1'){
  $programPresenterresrequiredyes['checked'] = true;
}else{
  unset($programPresenterresrequiredyes['checked']);
}

$programPresenterresrequiredno = array(
    'name'    => 'presenterRegRequired'.$presenterDetails->id,
    'value'   => '0',
    'id'    => 'presenterRegRequiredNo'.$presenterDetails->id,
    'type'    => 'radio',
    'required'  => '',
    'class'   =>'small presenter_req',
    'autocomplete' => 'off',
    'data-parsley-error-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
    'presenterId'      => $presenterDetails->id 
);

if($presenterDetails->is_registration_required=='0'){
  $dn = 'dn';
  $programPresenterresrequiredno['checked'] = true;
}else{
  unset($programPresenterresrequiredno['checked']);
}


$registrantTypeOptions[""] = "Select Registration Type";
if(!empty($registrantType)){
  foreach($registrantType as $type){
    $registrantTypeOptions[$type->id] = $type->registrant_name;
  } //end loop
} // end if
?> 
 <div class="panel event_panel sub_panel <?php echo $presenterDetails->firstname." ".$presenterDetails->lastname; ?>" id="corporatepackage_<?php echo $presenterDetails->id; ?>">
    <div class="panel-heading" >
      <h4 class="panel-title medium dt-large filter_presenters <?php echo strtolower($presenterDetails->firstname." ".$presenterDetails->lastname); ?>" id="program_presenter_div_<?php echo $presenterDetails->id; ?>">
      <a class="collapsed" data-toggle="collapse" data-parent="#accordion_sub" href="#collapseTwo<?php echo $presenterDetails->id; ?>"><?php echo $presenterDetails->firstname." ".$presenterDetails->lastname; ?></a>
      
      <span class="heading_btn_wrapper pull-right">        
        <a href="javascript:void(0)" class="delete_btn td_btn delete_presenter" id="delete_<?php echo $presenterDetails->id; ?>" deleteid="<?php echo $presenterDetails->id; ?>"><span>&nbsp;</span></a>
        <a href="javascript:void(0)" class="eventsetup_btn td_btn add_presenter_btn" id="edit_<?php echo $presenterDetails->id; ?>" isCopyPresenter="0" presenterId="<?php echo $presenterDetails->id; ?>"><span>&nbsp;</span></a>
        <a href="javascript:void(0)" class="copy_btn td_btn add_presenter_btn" id="duplicate_<?php echo $presenterDetails->id; ?>" isCopyPresenter="1" presenterId="<?php echo $presenterDetails->id; ?>" ><span>&nbsp;</span></a>
        
      </span>
      </h4>      
    </div>
    
    <div style="height: auto;" id="collapseTwo<?php echo $presenterDetails->id; ?>" class="accordion_content collapse apply_content ">
      <?php echo form_open($this->uri->uri_string(),$formProgramPresenterDetails); ?>
     
      
        <div class="row-fluid-15">
          <label for="info_org"><?php echo lang('program_organisation'); ?><span class="astrik"></span></label>
          <?php echo form_input($programPresenterorganisation); ?>
        </div>

        <div class="row-fluid-15">
          <label for="info_email"><?php echo lang('program_email'); ?><span class="astrik"></span></label>
          <?php echo form_input($programPresenteremail); ?>
        </div>

        <div class="row-fluid-15">
          <label for="info_email"><?php echo lang('program_confirm_email'); ?><span class="astrik"></span></label>
          <?php echo form_input($programPresenteremailConfirm); ?>
        </div>
        
        <div class="row-fluid-15">
          <label for="info_phone"><?php echo lang('program_phone'); ?><span class="astrik"></span></label>   
          
          <input id="input_hidden_mobile<?php echo $presenterDetails->id; ?>" type="hidden" value="<?php echo  $presenterDetails->mobile; ?>" name="input_hidden_mobile<?php echo $presenterDetails->id; ?>">
          <input id="input_hidden_landline<?php echo $presenterDetails->id; ?>" type="hidden" value="<?php echo  $presenterDetails->phone; ?>" name="input_hidden_landline<?php echo $presenterDetails->id; ?>">
          <div class="phone_input ">
          <?php echo form_input($programPresenterphone); ?>
          
             <!--<a class="phone_field_selector" href="#"></a>
             <ul id="change_input3" class="phone_field_opt change_input_class">
              <li id="mobile<?php echo $presenterDetails->id; ?>" class="selected" data-select="mobile<?php echo $presenterDetails->id; ?>" data-id="<?php echo $presenterDetails->id; ?>">Mobile</li>
              <li id="landline<?php echo $presenterDetails->id; ?>" class="not-selected" data-select="landline<?php echo $presenterDetails->id; ?>" data-id="<?php echo $presenterDetails->id; ?>">Landline</li>
             </ul> -->
          </div> 
          
        </div>
        
        <div class="row-fluid-15">
          <label for="info_topic"><?php echo lang('program_topic'); ?><span class="astrik"></span></label>
          <?php echo form_input($programPresentertopic); ?>
        </div>
        
        <div class="row-fluid-15">
          <label for="info_desc"><?php echo lang('program_presentation_description'); ?></label>
          <?php echo form_textarea($programPresenterdes); ?>
        </div>
        <!--end of row-fluid-->

        <div class="row-fluid-15">
          <label for="info_reg"><?php echo lang('program_registration_required'); ?>
            <span class="info_btn"><span class="field_info xsmall"><?php echo lang('program_registration_required'); ?></span></span>
          </label>
          <div class="radio_wrapper ">
            
            <?php echo form_input($programPresenterresrequiredyes); ?>
            <label for="<?php echo 'presenterRegRequiredYes'.$presenterDetails->id; ?>"><?php echo lang('program_registration_req_yes'); ?></label>

            <?php echo form_input($programPresenterresrequiredno); ?>
            <label for="<?php echo 'presenterRegRequiredNo'.$presenterDetails->id; ?>"><?php echo lang('program_registration_req_no'); ?></label>
          </div>
        </div>
        
        <div class="row-fluid-15 <?php echo $dn; ?>" id="showregistrantdiv<?php echo $presenterDetails->id; ?>">
          <label for="info_reg_type"><?php echo lang('program_registration_type'); ?>  <span class="astrik">*</span></label>
          
          <?php 
          $other = ' id="registrantType"'.$presenterDetails->id.' class="small custom-select short_field"  ';
          echo form_dropdown('registrantType'.$presenterDetails->id,$registrantTypeOptions,$presenterDetails->registration_type,$other);
          echo form_error('registrantType'.$presenterDetails->id);
          ?>
          
          <span class="field_desc small pull-left">
            Haven&rsquo;t set up your presenter registration type?<br/>
            <a href="<?php echo base_url('event/setupregistrant'); ?>">Click here</a> to be redirected.
          </span>
        </div>
        <!--end of row-fluid-->

        <div class="btn_wrapper ">
          <a href="#collapseTwo<?php echo $presenterDetails->id; ?>" class="pull-left scroll_top"><?php echo lang('comm_top'); ?></a>

          <?php 
            
            echo form_hidden('eventId', $presenterDetails->event_id);
            echo form_hidden('formActionName', 'programPresenterDetailSave');
            echo form_hidden('presenterId', $presenterDetails->id);
          
            //button show of save and reset
            $formButton['saveButtonId'] = 'id="'.$presenterDetails->id.'"'; // save button id
            $formButton['showbutton']   = array('save','reset');
            $formButton['labletext']  = array('save'=>lang('comm_save_changes'),'reset'=>lang('comm_reset'));
            $formButton['buttonclass']  = array('save'=>'default_btn btn pull-right medium','reset'=>'default_btn btn pull-right medium');
            $this->load->view('common_save_reset_button',$formButton);
            
          ?>
          
          <!--<input type="submit" class="default_btn btn pull-right medium" value="Save" name="save"/>
          <button type="button" class="default_btn btn pull-right medium reset_form" name="form_reset">Clear</button>
           -->  
        </div>

      <?php echo form_close(); ?>
    </div>
  </div>
  <!--end of panel-->

  
         
<script type="text/javascript">
$(document).on("change", ".presenterEmail", function(){
    if($("#presenterEmail<?php echo $presenterDetails->id; ?>").val()!=""){
        $("#presenterEmailConfirm<?php echo $presenterDetails->id; ?>").attr('required','');       
    } else {
        $("#presenterEmailConfirm<?php echo $presenterDetails->id; ?>").val('');
        $("#presenterEmailConfirm<?php echo $presenterDetails->id; ?>").attr('data-parsley-required', 'false');
        $("#presenterEmailConfirm<?php echo $presenterDetails->id; ?>").removeAttr('required');
    }
});

  
$( "#formProgramPresenterDetails<?php echo $presenterDetails->id; ?>" ).submit(function( event ){
    var selectedId  = $(this).find(".phone_field_opt").find(".selected").attr('id');
    $("#input_hidden_"+selectedId).val($("#presenterPhone<?php echo $presenterDetails->id; ?>").val()); 
    //program details type details save
    ajaxdatasave('formProgramPresenterDetails<?php echo $presenterDetails->id; ?>','<?php echo $this->uri->uri_string(); ?>',true,true,false,true,true,'#showhideformdiv<?php echo $presenterDetails->id; ?>','#showhideformdiv',false);
});  
</script> 
