<div class="panel event_panel co_cat co_cat_corporate">
    <div class="panel-heading ">
        <h4 class="panel-title medium dt-large p_rel">
            <a class="collapsed " data-toggle="collapse" data-parent="#accordion" href="#collapse"><?php echo "Presenters"; ?></a>
            <input type="text" name="filterpresenter" value="" id="filterpresenter" placeholder="" class="small short_field filterpresenter" data-key="">
        </h4>
    </div>
    <div style="height: auto;" id="collapse" class="accordion_content collapse apply_content <?php echo getIsCollapseOpen("collapseFive"); ?>">
        <div class="panel-body ls_back dashboard_panel small co_category">
            <div id="accordion_sub" class="panel-group sub-panel-group">
            <?php
            //print_r($presenterDetails);
            if (!empty($presenterDetails)) {
                foreach ($presenterDetails as $value) {
                    // load presenter details list
                    $data['presenterDetails'] = $value;
                    $this->load->view('form_program_presenter_details', $data);
                }
            } 
            ?>
            </div>
        </div>
    </div>
</div>



<script>
$(document).ready(function() {
    $(document).on('keyup', '.filterpresenter', function(){
        $(".accordion_content").css("height", 'auto');
        
        //var filter_key = $(this).data('key');
        var filter_text = $(this).val();
        //var filter_text = filter_text.toLowerCase();
        //console.log(filter_text);
        if(filter_text){             
            $('.filter_presenters').hide();
            $(".accordion_content").css("height", 'auto');
            $("h4[class*="+filter_text+"]").show();
            $(".accordion_content").css("height", 'auto');    
        } else {
           $('.filter_presenters').show();
           $(".accordion_content").css("height", 'auto');
           $(".accordion_content").css("height", 'auto');
        }
    });    
});
</script> 
