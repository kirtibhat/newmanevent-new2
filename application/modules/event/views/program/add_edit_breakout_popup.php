<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//--------add type fo registration fields---------//  

$breakoutNameValue = (!empty($breakoutdata->breakout_name))?$breakoutdata->breakout_name:''; 
$breakoutTypeValue = (isset($breakoutdata->block_type)) ? $breakoutdata->block_type : '';
$breakoutDateValue = (!empty($breakoutdata->date))?dateFormate($breakoutdata->date, "d M Y"):''; 
$breakoutStartTimeValue = (!empty($breakoutdata->start_time))?timeFormate($breakoutdata->start_time,'h:i A'):''; 
$breakoutEndTimeValue = (!empty($breakoutdata->end_time))?timeFormate($breakoutdata->end_time,'h:i A'):''; 
$headerAction = ($breakoutIdValue > 0)?lang('event_breakout_edit_title'):lang('event_breakout_add_title'); 


$formTypeofBreakout = array(
    'name'   => 'formTypeofBreakout',
    'id'   => 'formTypeofBreakout',
    'method' => 'post',
    'class'  => 'wpcf7-form',
    'data-parsley-validate' => ''
);
  
$breakoutName = array(
    'name'  => 'breakoutName',
    'value' => $breakoutNameValue,
    'id'  => 'breakoutName',
    'type'  => 'text',
    'class' => 'small',
  //  'placeholder' => 'Breakout Name',
    'required'  => '',
    'autocomplete' => 'off',
    'data-parsley-error-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
);

$breakoutDate = array(
    'name'  => 'breakoutDate',
    'value' =>  $breakoutDateValue,
    'id'  => 'breakoutDate',
    'type'  => 'text',
    'required'  => '',
    //'readonly' => '',
    'class' => 'date_input datepicker',
    'data-format' => 'yyyy-MM-dd',
    'data-parsley-error-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'change',
);
  
  
$blockStartTime = array(
    'name'  => 'blockStartTime',
    'value' =>  $breakoutStartTimeValue,
    'id'  => 'blockStartTime',
    'type'  => 'text',
    'required'  => '',
    //'readonly' => '',
    'class' => 'width_100px timepicker',
    'data-format' => 'hh:mm:ss',
    'data-parsley-error-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'change',
);

$blockEndTime = array(
    'name'  => 'blockEndTime',
    'value' =>  $breakoutEndTimeValue,
    'id'  => 'blockEndTime',
    'type'  => 'text',
    'required'  => '',
    //'readonly' => '',
    'class' => 'width_100px timepicker',
    'data-format' => 'hh:mm:ss',
    'data-parsley-error-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'change',
);    
    
$breakoutId = array(
    'name'  => 'breakoutId',
    'value' => $breakoutIdValue,
    'id'  => 'breakoutId',
    'type'  => 'hidden',
);


$blockValue[""]  = lang('programer_select_block_type'); 
$blockValue["1"] = lang('programer_block_plenary'); 
$blockValue["0"] = lang('programer_block_nonplenary'); 

?>

<!-- Modal -->
<div id="add_edit_brakout_popup_div" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" >
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header delegates">
        <h4 class="medium dt-large modal-title"><?php echo $headerAction; ?></h4>
      </div>
       <div class="modal-body small">
        <div class="modelinner ">
          <?php  echo form_open(base_url('event/addbreakouttype'),$formTypeofBreakout); ?> 
           
            <div class="control-group mb10 eventdashboard_popup small">

              <div class="row-fluid">
                <label class="pull-left" for="block_name"><?php echo lang('event_breakout_popup_add_field'); ?>   <span class="astrik">*</span>
                <span class="info_btn"><span class="field_info xsmall"><?php echo lang('tooltip_breakout_popup_add_field'); ?></span></span>
                </label>
               <?php echo form_input($breakoutName); ?>
              </div>

              <div class="row-fluid">
                <label class="pull-left" for="block_type"><?php echo lang('programer_block_type'); ?><span class="astrik">*</span>
          <span class="info_btn"><span class="field_info xsmall"><?php echo lang('programer_block_type'); ?></span></span>
                </label>
                
                <?php 
          $other = ' id="block_type" required ="" class="small custom-select-block-type custom-select full_select" data-parsley-error-message="'.lang('common_field_required').'" data-parsley-error-class="custom_li" ';
          echo form_dropdown('blockType',$blockValue,$breakoutTypeValue,$other);
          echo form_error('blockType');
                ?>
                
              </div>
              
              <div class="row-fluid">
                <label class="pull-left" for="block_date"><?php echo lang('programer_block_date'); ?> <span class="astrik">*</span></label>
                  <div class="date_wrapper program_date" id="datepicker1">
                  <?php echo form_input($breakoutDate); ?>
          
                </div>
              </div>
              
              <div class="row-fluid">
                <label class="pull-left" for="block_time"><?php echo lang('programer_block_time'); ?> <span class="astrik">*</span></label>
                <div class="popup_time_wrapper" >
                  <span  id="time1" class="date_wrapper time1" >
                    <label >Start</label>
                    <?php echo form_input($blockStartTime); ?>            
                  </span>
             
                  <span id="time2" class=" time1">
                    <label >Finish</label>
                    <?php echo form_input($blockEndTime); ?>                      
                  </span>
                   
                </div>
              </div>

              <div class="btn_wrapper">
                  <!--  <input type="submit" name="loginsubmit" value="Save" class=" submitbtn pull-right medium">
            <input type="submit" class="popup_cancel submitbtn pull-right medium" value="Cancel" name="logincancel" data-dismiss="modal">
          -->
        <?php 
          echo form_hidden('eventId', $eventId);
          echo form_input($breakoutId);
          $extraCancel  = 'class="popup_cancel submitbtn pull-right medium" data-dismiss="modal" ';
          $extraSave  = 'class="submitbtn pull-right medium" ';
          echo form_submit('save',lang('comm_save'),$extraSave);
          echo form_submit('cancel',lang('comm_cancle'),$extraCancel);
          
        ?>
        
              </div>

            </div>
          <?php echo form_close(); ?>
        </div>
      </div>
    </div>
  </div>
</div>



<script>
var eventStartDate = '<?php echo (!empty($eventdetails->starttime)) ? date('d M Y', strtotime($eventdetails->starttime)) : ''; ?>';
var eventEndDate = '<?php echo (!empty($eventdetails->endtime)) ? date('d M Y', strtotime($eventdetails->endtime)) : ''; ?>';
$(function(){ 
    ini_custom_dropdown('custom-select-block-type');
    // Validate form
    $("#formTypeofBreakout").parsley(); 
});
   
$(function() {
    //load datepicker 
    $( ".datepicker" ).datepicker({
      //startDate: new Date(),
      minDate: eventStartDate,
      maxDate: eventEndDate,
      changeMonth: true,
      changeYear: true,
      showOn: "both",
      buttonImage: baseUrl+"themes/system/images/calender_icon.png",
      dateFormat: 'dd M yy',
      buttonText: 'Calendar',
      buttonImageOnly: true,
    });
    
    //load timepicker
    $('.timepicker').timepicker({
      showOn: "both",
      buttonImage: baseUrl+"themes/system/images/calender_icon.png",
      buttonText: 'Calendar',
      controlType: 'select',
      timeFormat: 'HH:mm',
      buttonImageOnly: true,
    });
}); 
</script>
