<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 |--------------------------------------------------------------------------|
 | This Field define for temparay open popup by add section id 				|
 | and edit record id														|	
 |--------------------------------------------------------------------------|
*/

$tempHidden = array(
	'type' => 'hidden',
	'name' => 'tempHidden',
	'id' => 'tempHidden',
	'value' => '',
);

echo form_input($tempHidden);

$this->load->view('event/event_menus'); 


?>

<div class="row-fluid-15">
        <div id="page_content" class="span9">
			
			<div id="accordion" class="panel-group">
				
				 <p>
                	<a href="javascript:void(0)" class="btn medium add_presenter add_big_btn event_btn_margin add_presenter_btn"  isCopyPresenter="0" presenterId="0"  ><?php echo lang('add_presenter_button'); ?></a>
                    <a href="#" class="btn medium add_reg_type add_big_btn"><?php echo lang('import_presenter_button'); ?></a>
                </p>
				
				<?php 
					// load presenter details list
					$this->load->view('program_presenter_list');
					
					if(!empty($presenterDetails)){ 
						foreach($presenterDetails as $value){
							// load presenter details list
							$data['presenterDetails'] = $value;
							$this->load->view('form_program_presenter_details',$data);
						}
					  }		
					
				?>
					
                <p class="small">
                	<?php echo lang('program_block_title'); ?>
                    <span class="clearfix"></span>
                    <a href="#" class="btn medium add_program_block add_big_btn event_btn_margin add_edit_breakouts" breakoutid="0"><?php echo lang('program_block_button'); ?></a>
                </p>	
				
			
			<?php 
				
					if(!empty($eventbreakouts)){
						
						//prepare array data for current and next form hide/show  
						foreach($eventbreakouts as $breakouts){
							$recordId[] =  $breakouts->id;
						}
						
						//show breakout forms	
						$rowCount = 0;
						foreach($eventbreakouts as $breakouts){
							$nextId = $rowCount+1;
							$sendData['formdata'] = $breakouts; 
							$sendData['nextRecordId']	= (isset($recordId[$nextId]))?$recordId[$nextId]:'0';
							// daynamic load all breakouts forms
							$this->load->view('program/form_breakout_types',$sendData);
							$rowCount++;
						}
					}
				?>
		
			<input type="hidden" value="<?php echo $eventId; ?>" id="event_id" />
			</div>
			<?php 
			    
			    
				//next and previous button
				$buttonData['viewbutton'] = array('next','preview','back');
				$buttonData['linkurl'] 	= array('back'=>base_url('event/setupregistrant'),'next'=>base_url('event/programdetails'),'preview'=>'');
				$this->load->view('common_back_next_buttons',$buttonData);
			?>
			
            <!--end of page btn wrapper-->	
		</div>
        <!--end of page content-->
</div>
<!--end of row-->


<div id="opentModelBox"></div>

<!--start custom tooltip-->
<div id="tooltip_wrapper"></div>
<!--end custom tooltip-->
<?php print_r($presenterDetails); ?>
<script>

ajaxdatasave('formprogrampresenter','<?php echo $this->uri->uri_string(); ?>',true,true,false,true,false,'#showhideformdivaddpresenter','#showhideformdivshowpresenterlist','');

var presenterDetails = '<div class="row-fluid-15"><label for="info_org">Presenter</label><select class="custom-select select_fwidth"><option>Select</option>';
	// get presenter data
	<?php if(!empty($presenterDetails)){
		foreach($presenterDetails as $value){ ?>
			alert("come");
			presenterDetails +="<option value='<?php echo $value->id; ?>'><?php echo $value->title." ".$value->firstname." ".$value->lastname; ?></option>";
		<?php }
	} ?>
	
presenterDetails +='</select></div>'; 
alert(presenterDetails);	
$(document).ready(function() {
	
	
	
	$(".add_presenter_btn").click(function(){
		var getPresenterId 	  	   = parseInt($(this).attr('presenterId'));
		var getIsPresenterCopy 	   = parseInt($(this).attr('isCopyPresenter'));
		
		formPostData = {'eventId':$("#event_id").val(),'popupType':'programPresenter','presenterId':getPresenterId,'isPresenterCopy':getIsPresenterCopy};
		//call ajax popup function
		ajaxpopupopen('add_presenter_popup','event/programpopup',formPostData,'add_presenter_btn');
	});
	
	$(".presenter_req").click(function(){
		// For set collapse hight
		$(".accordion_content").css("height",'auto');
		var presenterId = $(this).attr('presenterId');
		if($(this).val()==1){		
			$("#showregistrantdiv"+presenterId).show();
		}else{
		   $("#showregistrantdiv"+presenterId).hide();
		}	
	
	});
	
});

//delete type of presenter
customconfirm('delete_presenter','event/deleteprogrampresenter',false,'','',true);

/*-----Open add and edit breakout popup-------*/
$(document).on('click','.add_edit_breakouts',function(){
	
	if($(this).attr('breakoutid')!==undefined){
		var breakoutId = parseInt($(this).attr('breakoutid'));
		//set value in assing
		$("#tempHidden").val(breakoutId);
	}else{
		//get value form temp hidden and set
		var breakoutId = parseInt($("#tempHidden").val());
	}
	var eventId = '<?php echo $eventId ?>';
	var sendData = {"breakoutId":breakoutId, "eventId":eventId};
	
	//call ajax popup breakout add and edit 
	ajaxpopupopen('add_edit_brakout_popup_div','event/addeditbreakoutview',sendData,'add_edit_breakouts');
	
});
	
//save breakout add and edit data
ajaxdatasave('formTypeofBreakout','event/addeditbreakoutsave',false,true,true,true,false,false,false,false);
	
/*-------add daynamic bearkout number of stream--------*/

// add presenter
$(document).on('click','.presenteradd .stepper-step',function(){
	//data-fieldid
	
	//var presenterposition = 'load_presenter_data'+$(this).parent().find('input').attr('data-fieldid');
	
	var presenterVal =  $(this).parent().find('input').val();
	 if($(this).hasClass('up')){
		var presenterVal = presenterVal*1+1; 
		$(this).parent().find('input').val(presenterVal);
		$(this).parent().parent().parent().find(".load_presenter_data").append(presenterDetails);
	 }else{
		if(presenterVal>=1){
			var presenterVal = presenterVal*1-1;
		}else{
			presenterVal = 0; 
		}
		$(this).parent().find('input').val(presenterVal);
	
	 }
	
});

// Add Stream block
$(document).on('click','.addstream .stepper-step',function(){
		
		// For set collapse hight
		$(".accordion_content").css("height",'auto');
		
		var getId=$(this).parent().find('input').attr('streambreakoutid');
		
		var streamLimit					= parseInt($("#breakoutNumberOfStreams"+getId).val());
		var getValueSession				= parseInt($("#breakoutNumberOfSession"+getId).val());
		var streamsBreakoutNumber 		= parseInt($("#streamsBreakoutNumbHidden"+getId).val());
		var sessionPerStreamNumbHidden 	= parseInt($("#sessionPerStreamNumbHidden"+getId).val());
		
		var stream_html = '';
	    if($(this).hasClass('up')){ 	
				if(streamLimit > streamsBreakoutNumber){
					
					for(i=streamsBreakoutNumber;i<streamLimit;i++){
					   
						var streamPosition = i+1;
						var streamFieldId = getId+'_'+streamPosition;
						
						
						stream_html +='<div class="panel event_panel sub_panel">'
						stream_html +='<div class="panel-heading"><h4 class="panel-title medium dt-large"><a class="" data-toggle="collapse" data-parent="#accordion_sub" href="#collapseSubOne'+getId+'_'+streamPosition+'"> Stream '+streamPosition+' </a></h4></div>';
						
						stream_html +='<div id="collapseSubOne'+getId+'_'+streamPosition+'" class="accordion_content collapse apply_content ">';
						
						// Name of stream
						stream_html +='<div class="breakoutStreamName'+streamFieldId+'"><label for="info_org">Name of Stream '+ streamPosition  +' <span class="astrik">*</span></label><input type="text" class="streamtitlechange small" breakoutid="'+getId+'" streamrowid="'+streamPosition+'" required="" id="breakoutStreamName'+streamFieldId+'" value="" name="breakoutStreamName'+streamFieldId+'"></div>';
						// Stream Location
						stream_html +='<div class="breakoutStreamVenue'+streamFieldId+'" ><label for="info_org">Location <span class="astrik">*</span></label><input type="text" class="small" required="" id="breakoutStreamVenue'+streamFieldId+'" value="" name="breakoutStreamVenue'+streamFieldId+'"></div>';
						// Steam Limit
						stream_html +='<div class="breakoutLimit'+streamFieldId+'" ><label for="info_org">Limit <span class="astrik">*</span></label><input type="text" class="small" required="" id="breakoutLimit'+streamFieldId+'" value="" name="breakoutLimit'+streamFieldId+'"></div>';
						
						// Restrict Stream Access 
						stream_html +='<div class="row-fluid-15" ><label for="info_org">Restrict Stream Access  <span class="astrik">*</span><span class="info_btn"><span class="field_info xsmall">Restrict Stream Access</span></span></label>';
						stream_html +='<div class="radio_wrapper "><input type="radio" name="restrictStreamAccess'+streamFieldId+'" id="restrictStreamAccessyes'+streamFieldId+'" checked=""><label for="restrictStreamAccessyes'+streamFieldId+'">Yes</label>';
						stream_html +='<input type="radio" name="restrictStreamAccess'+streamFieldId+'" id="restrictStreamAccessno'+streamFieldId+'"><label for="id="restrictStreamAccessno'+streamFieldId+'"">No</label></div>';//radio_wrapper end
						
						stream_html +='</div>'; // Restrict Stream Access  end
						
						// seprator line
						stream_html +='<hr class="panel_inner_seprator">';
						stream_html +='<div id="session_div_'+streamFieldId+'">';
						
						if(getValueSession>0){
							for(j=1;j<=getValueSession;j++){
								
								stream_html +='<div class="sessionName'+streamFieldId+'"><label for="info_org">Session '+j+' Topic <span class="astrik">*</span><span class="info_btn"><span class="field_info xsmall">Session per stream</span></span></label></div>';
								stream_html +='<input type="text" value="Session 1 Topic" id="info_org" name="sessionName'+streamFieldId+'" class=" small">';
								
								// time 1 start time
								stream_html +='<div class="row-fluid-15"><label for="refr_no">Time <span class="astrik">*</span></label>';
								stream_html +='<div class="time_wrapper date_wrapper"><span class="time1" class="date_wrapper"><label for="start_time'+streamFieldId+'"></label><input type="text" data-format="hh:mm:ss" class="width_100px timepicker" readonly="" required="" id="sessionStartTime'+streamFieldId+'" value="" name="sessionStartTime'+streamFieldId+'"><span class="add-on"><a href="javascript:void(0)" class="datepicker_btn timepicker" data-date-icon="icon-time"></a></span>	</span>';
								
								// time 2 finish time
								stream_html +='<span class="time1" class="date_wrapper"><label for="finish_time">Finish</label><input type="text" data-format="hh:mm:ss" class="width_100px timepicker" readonly="" required="" id="sessionFinishTime'+streamFieldId+'" value="" name="sessionFinishTime'+streamFieldId+'"><span class="add-on"><a href="javascript:void(0)" class="datepicker_btn timepicker icon-calendar" data-date-icon="icon-time"></a></span>	</span>';
								stream_html +='</div></div>'; 
								stream_html +='<span id="presenter_div_'+streamFieldId+'">';
								stream_html +='<div class="row-fluid-15"><label for="noOfPresenter'+streamFieldId+'">Number of Presenters<span class="info_btn"><span class="field_info xsmall"></span></span></label>';
								stream_html +='<div class="stepper presenteradd">';
								stream_html +='<input type="number" id="noOfPresenter'+streamFieldId+'" name="noOfPresenter'+streamFieldId+'" value="0" required class="small dark" step="1" min="0" data-fieldid="'+streamFieldId+'" />';
								stream_html +='<span class="stepper-step up"> </span><span class="stepper-step down"> </span>';
								stream_html +='</div></div>';
								
								stream_html +='<div class="load_presenter_data"></div>';
								stream_html +='</span>';
								
								stream_html +='<hr class="panel_inner_seprator">';
								
							}
						}
						stream_html +='</div>';
									
							
						stream_html +='</div>'; // accordion_content close div
						stream_html +='</div>'; // event_panel close div
						
					
					
					}
					
					  
						if(streamsBreakoutNumber==0){
							$('#stream_data'+getId).append(stream_html);
						}else{
							if(streamsBreakoutNumber < streamPosition){
								$('#stream_data'+getId).append(stream_html);
							}
						}
						
				}
				
				
	    }else{ // down	
				
	    }	    
	    
	    $("#streamsBreakoutNumbHidden"+getId).val(streamLimit);
	    $("#sessionPerStreamNumbHidden"+getId).val(getValueSession);
	    		
});

// Add Stream block
$(document).on('click','.timepicker',function(){
	 $(".time1").datetimepicker({
		pickDate: false,
		pickTime: true,
	}); 
});	
	 


// Add Session block
$(document).on('click','.addsessionstream .stepper-step',function(){
		
		// For set collapse hight
		$(".accordion_content").css("height",'auto');
		
		var getId=$(this).parent().find('input').attr('streambreakoutid');
			
		var streamLimit					= parseInt($("#breakoutNumberOfStreams"+getId).val());
		var getValueSession				= parseInt($("#breakoutNumberOfSession"+getId).val());
		var streamsBreakoutNumber 		= parseInt($("#streamsBreakoutNumbHidden"+getId).val());
		var sessionPerStreamNumbHidden 	= parseInt($("#sessionPerStreamNumbHidden"+getId).val());
		
		var stream_html = '';
	    if($(this).hasClass('up')){ 	
			
				// For session 
				if(getValueSession>sessionPerStreamNumbHidden){
					//to add session fields per stream
					var session_html ='';
					
						for(j=sessionPerStreamNumbHidden;j<getValueSession;j++){
						
							var sessionPosition = j+1;
							var sessionFieldId = getId+'_'+sessionPosition;
							
							session_html +='<div class="sessionName'+sessionFieldId+'"><label for="info_org">Session '+sessionPosition+' Topic <span class="astrik">*</span><span class="info_btn"><span class="field_info xsmall">Session per stream</span></span></label></div>';
							session_html +='<input type="text" value="Session '+sessionPosition+' Topic" id="info_org" name="sessionName'+sessionFieldId+'" class=" small">';
							
							// time 1 start time
							session_html +='<div class="row-fluid-15"><label for="refr_no">Time <span class="astrik">*</span></label>';
							session_html +='<div class="time_wrapper date_wrapper"><span class="time1" class="date_wrapper"><label for="start_time'+sessionFieldId+'">Start</label><input type="text" data-format="hh:mm:ss" class="width_100px timepicker" readonly="" required="" id="sessionStartTime'+sessionFieldId+'" value="" name="sessionStartTime'+sessionFieldId+'"><span class="add-on"><a href="javascript:void(0)" class="datepicker_btn timepicker" data-date-icon="icon-time"></a></span>	</span>';
							
							// time 2 finish time
							session_html +='<span class="time1" class="date_wrapper"><label for="finish_time">Finish</label><input type="text" data-format="hh:mm:ss" class="width_100px timepicker" readonly="" required="" id="sessionFinishTime'+sessionFieldId+'" value="" name="sessionFinishTime'+sessionFieldId+'"><span class="add-on"><a href="javascript:void(0)" class="datepicker_btn timepicker icon-calendar" data-date-icon="icon-time"></a></span>	</span>';
							session_html +='</div></div>'; 
						
							session_html +='<span id="presenter_div_'+sessionFieldId+'">';
							session_html +='<div class="row-fluid-15"><label for="noOfPresenter'+sessionFieldId+'">Number of Presenters<span class="info_btn"><span class="field_info xsmall"></span></span></label>';
							session_html +='<div class="stepper presenteradd">';
							session_html +='<input type="number" id="noOfPresenter'+sessionFieldId+'" name="noOfPresenter'+sessionFieldId+'" value="0" required class="small dark" step="1" min="0" data-fieldid="'+sessionFieldId+'" />';
							session_html +='<span class="stepper-step up"> </span><span class="stepper-step down"> </span>';
							session_html +='</div></div>';
							
							session_html +='<div class="load_presenter_data"></div>';
							session_html +='</span>';
							
							session_html +='<hr class="panel_inner_seprator">';
						}// end loop
							
						for(k=1;k<=streamLimit;k++){
							$('#session_div_'+getId+'_'+k).append(session_html);
						} // end loop
				
				}
				
				
	    }else{ // down	
				
	    }
	   
	    $("#sessionPerStreamNumbHidden"+getId).val(getValueSession);
	    
});

</script>

