<?php
echo form_input($blockBreakoutNumbHidden); //set hidden field for get value for block 
echo form_input($streamsBreakoutNumbHidden); //set hidden field for get value for stream 
echo form_input($sessionPerStreamNumbHidden); //set hidden field for get value for session per stream
?>
<div class="row-fluid-15">
    <label for="info_org"><?php echo lang('programer_block_type'); ?> <span class="astrik">*</span></label>
    <input type="text" value="<?php echo lang('programer_block_nonplenary') ?>" id="info_org" name="info_org" class=" small disabled" disabled>
</div>

<div class="row-fluid-15 addstream">
    <label for="refr_no"><?php echo lang('programer_stream'); ?><span class="astrik">*</span>
      <span class="info_btn">
          <span class="field_info xsmall"><?php echo lang('programer_stream'); ?></span>
        </span>
    </label>
    <?php 
    echo form_input($breakoutNumberOfStreams);
    echo form_error('breakoutNumberOfStreams'.$breakoutId); 
    ?>
    </div>
    <!--end of row-fluid-->
            <!--<div class="row-fluid-15 addsessionstream"  >
                <label for="refr_no"><?php echo lang('programer_session_stream'); ?> <span class="astrik">*</span>
                  
                </label>
                <input type="number" id="breakoutNumberOfSession<?php echo $breakoutId; ?>" streambreakoutid ="<?php echo $breakoutId; ?>"  name="breakoutNumberOfSession<?php echo $breakoutId; ?>" value="<?php echo $numberOfSessionVal; ?>" required class="small dark" step="1" <?php echo ($numberOfSessionVal>0) ? 'readonly=TRUE' : ''; ?>  <?php echo ($numberOfSessionVal>0) ? 'max="'.$numberOfSessionVal.'"' : ''; ?> <?php echo ($numberOfSessionVal>0) ? 'min="'.$numberOfSessionVal.'"' : '2'; ?>  />
            </div>
            end of row-fluid-->
            
            <div class="row-fluid-15">
                <label for="refr_no"><?php echo lang('programer_allow_stream_hopping'); ?> <span class="astrik">*</span>
                  <span class="info_btn">
                      <span class="field_info xsmall"><?php echo lang('programer_allow_stream_hopping'); ?>
                        </span>
                    </span>
                </label>
                <div class="radio_wrapper ">
                  <input type="radio" value="1" name="allowStreamHopping<?php echo $breakoutId; ?>" id="allowStreamHoppingyes<?php echo $breakoutId; ?>" <?php echo ($formdata->allow_stream_hopping=='1') ? 'checked="checked"' : '' ?> > 
                    <label for="allowStreamHoppingyes<?php echo $breakoutId; ?>"><?php echo lang('programer_access_yes'); ?></label>

                  <input type="radio" value="0" name="allowStreamHopping<?php echo $breakoutId; ?>" id="allowStreamHoppingno<?php echo $breakoutId; ?>" <?php echo ($formdata->allow_stream_hopping=='0') ? 'checked="checked"' : '' ?>>
                    <label for="allowStreamHoppingno<?php echo $breakoutId; ?>"><?php echo lang('programer_access_no'); ?></label>
                </div>
            </div>
            <!--end of row-fluid-->
            
            <div id="accordion_sub" class="panel-group sub-panel-group sub_panel full_subpanel">
               <div id="stream_data<?php echo $breakoutId; ?>">
                <?php
                //print_r($breakoutStreamData);
                if(!empty($breakoutStreamData)){
                  foreach($breakoutStreamData as $streamData){ 
                    $streamSerial = $streamData->stream;
                    $breakoutStreamId = $streamData->id;
                    $BreakoutStreamRowId = $breakoutId.'_'.$streamSerial;
                    //to get session by stream id
                    
                    $streamRegistrantData  = getDataFromTabel('breakouts_stream_registrants','registrant_id,registrant_limit ',array('stream_id'=>$breakoutStreamId));
                    
                    //Convert object into array
                    if(!empty($streamRegistrantData)){
                      foreach($streamRegistrantData as $key=>$value){ ?>
                        <input type="hidden" class="registrantSreamIds" id="hiddenSelectedRegistrant<?php echo $BreakoutStreamRowId."_".$key; ?>" key="<?php echo $value->registrant_id; ?>" breakoutid ="<?php echo $BreakoutStreamRowId; ?>" value="<?php echo $value->registrant_limit; ?>" name="selectedregistrant<?php echo $breakoutId; ?>[<?php echo $value->registrant_id; ?>]" />
                      <?php }
                    }

                            $numberOfSessionStreamVal = $streamData->number_of_session;
                            //echo '<pre>';print_r($streamData);
                            $breakoutStreamName = array(
                                'name'  => 'breakoutStreamName'.$BreakoutStreamRowId,
                                'value' => $streamData->stream_name,
                                'id'  => 'breakoutStreamName'.$BreakoutStreamRowId,
                                'type'  => 'text',
                                'class' => 'small',
                                'required'=>'',
                                'autocomplete' => 'off',
                                'data-parsley-error-message' => lang('common_field_required'),
                                'data-parsley-error-class' => 'custom_li',
                                'data-parsley-trigger' => 'keyup',
                            );
                            $breakoutStreamVenue = array(
                                'name'  => 'breakoutStreamVenue'.$BreakoutStreamRowId,
                                'value' => $streamData->stream_venue,
                                'id'  => 'breakoutStreamVenue'.$BreakoutStreamRowId,
                                'type'  => 'text',
                                'class' => 'small',
                                'required'=>'',
                                'autocomplete' => 'off',
                                'data-parsley-error-message' => lang('common_field_required'),
                                'data-parsley-error-class' => 'custom_li',
                                'data-parsley-trigger' => 'keyup',
                            );
                            $breakoutLimit = array(
                                'name'  => 'breakoutLimit'.$BreakoutStreamRowId,
                                'value' => $streamData->block_limit,
                                'id'  => 'breakoutLimit'.$BreakoutStreamRowId,
                                'type'  => 'text',
                                'class' => 'numValue small width_130',
                                'required'=>'',
                                'autocomplete' => 'off',
                                'data-parsley-error-message' => lang('common_field_required'),
                                'data-parsley-error-class' => 'custom_li',
                                'data-parsley-trigger' => 'keyup',
                            );
                            $restrictStreamAccessyes = array(
                                'name'  => 'restrictStreamAccess'.$BreakoutStreamRowId,
                                'value' => $streamData->block_limit,
                                'id'  => 'restrictStreamAccessyes',
                                'type'  => 'radio',
                                'value' => '1',
                                'class' => 'restrictStreamAccess',
                                'checked'=> ($streamData->restrict_stream_access=='1') ? 'checked="checked"' : '',
                                'breakoutSId' => $BreakoutStreamRowId,
                            );
                            $restrictStreamAccessno = array(
                                'name'  => 'restrictStreamAccess'.$BreakoutStreamRowId,
                                'value' => $streamData->block_limit,
                                'id'  => 'restrictStreamAccessno',
                                'type'  => 'radio',
                                'value' => '0',
                                'class' => 'restrictStreamAccess',
                                'checked'=> ($streamData->restrict_stream_access=='0') ? 'checked="checked"' : '',
                                'breakoutSId' => $BreakoutStreamRowId,
                            );

     
    ?>
                        
     <input id="breakoutStreamId<?php echo $BreakoutStreamRowId; ?>"  type="hidden" value="<?php echo $breakoutStreamId; ?>" name="breakoutStreamId<?php echo $breakoutId; ?>[<?php echo $streamSerial; ?>]">
        <div id="stream_position_<?php echo $streamSerial; ?>" class="panel event_panel sub_panel">
      <div class="panel-heading darkcolor">
        <h4 class="panel-title medium dt-large">
          <a class="" href="#collapseSubOne<?php echo $BreakoutStreamRowId; ?>" data-parent="#accordion_sub" data-toggle="collapse"> <?php echo lang('programer_stream_serial'); ?> <?php echo $streamSerial; ?> </a>
        </h4>
       </div>
       <div id="collapseSubOne<?php echo $BreakoutStreamRowId; ?>" class="accordion_content collapse apply_content ">
        
        <!-- Stream Name -->
        <div class="row-fluid-15">
          <label for="breakoutStreamName<?php echo $BreakoutStreamRowId; ?>"><?php echo lang('programer_stream_name'); ?> <?php echo $streamSerial; ?> <span class="astrik">*</span></label>
          <?php echo form_input($breakoutStreamName); ?>
        </div>
        <!--end of row-fluid-->
        
        <!-- Stream Location -->
        <div class="row-fluid-15">
          <label for="breakoutStreamVenue<?php echo $BreakoutStreamRowId; ?>"><?php echo lang('programer_stream_location'); ?> <span class="astrik"></span></label>
          <?php echo form_input($breakoutStreamVenue); ?>
          </div>
        <!--end of row-fluid-->

        <div class="row-fluid-15 addsessionstream"  >   
            <label for="refr_no"><?php echo lang('programer_sessions'); ?> <span class="astrik">*</span>
              
            </label>
            <input type="number" id="breakoutNumberOfSession<?php echo $BreakoutStreamRowId; ?>" streambreakoutid ="<?php echo $breakoutId; ?>" BreakoutStreamRowId="<?php echo $BreakoutStreamRowId ?>"  name="breakoutNumberOfSession<?php echo $BreakoutStreamRowId; ?>" value="<?php echo $numberOfSessionStreamVal; ?>" required class="small dark" step="1" <?php echo ($numberOfSessionStreamVal>0) ? 'readonly=TRUE' : ''; ?>  <?php echo ($numberOfSessionStreamVal>0) ? 'max="'.$numberOfSessionStreamVal.'"' : ''; ?> <?php echo ($numberOfSessionStreamVal>0) ? 'min="'.$numberOfSessionStreamVal.'"' : '2'; ?>  />
        </div>
        <!--end of row-fluid-->
        
        <!-- Stream Limit -->
        <div class="row-fluid-15">
          <label for="info_org"><?php echo lang('programer_limit'); ?> <span class="astrik"></span>
            <span class="info_btn">
              <span class="field_info xsmall"><?php echo lang('programer_limit'); ?>
              </span>
            </span>
          </label>
          <?php echo form_input($breakoutLimit); ?>
        </div>
        <!--end of row-fluid-->
        
        <!-- Restrict Stream Access -->
        <div class="row-fluid-15">
          
          <label for="info_org"><?php echo lang('programer_restrict_event_access'); ?><span class="astrik"></span>
            <span class="info_btn">
              <span class="field_info xsmall"><?php echo lang('programer_restrict_event_access'); ?></span>
            </span>
          </label>
          <div class="radio_wrapper ">
            <?php echo form_radio($restrictStreamAccessyes); ?>
            <label for="restrictStreamAccessyes"><?php echo lang('programer_access_yes'); ?></label>

            <?php echo form_radio($restrictStreamAccessno); ?>
            <label for="restrictStreamAccessno"><?php echo lang('programer_access_no'); ?></label>
          </div>
          <div class="session_subcat <?php echo ($streamData->restrict_stream_access=='0') ? 'dn' : ""; ?>" id="registrantCatDiv<?php echo $BreakoutStreamRowId; ?>">
            <?php 
              if(!empty($catFilterList)){
                
                if(!empty($catFilterList)){
                  foreach($catFilterList as $key=>$list){
                    foreach($list as $cat=>$registrant){
                    ?>
                      <div class="row-fluid-15"><span class="checkbox_wrapper"><input type="checkbox" data-append-id="<?php echo $key; ?>_<?php echo $BreakoutStreamRowId; ?>" BreakoutStreamRowId="<?php echo $BreakoutStreamRowId; ?>" class="registrantStreamCat" data-category-id="<?php echo $key; ?>" id="registrant_cat_<?php echo $key; ?>_<?php echo $BreakoutStreamRowId; ?>" name="registrant_cat_<?php echo $key; ?>_<?php echo $BreakoutStreamRowId; ?>"><label for="registrant_cat_<?php echo $key; ?>_<?php echo $BreakoutStreamRowId; ?>"><?php echo $cat; ?></label></span></div>
                       <div id="category_list_<?php echo $key; ?>_<?php echo $BreakoutStreamRowId; ?>" data-category="<?php echo $key; ?>_<?php echo $BreakoutStreamRowId; ?>" class="dn">
                       <?php  
                       foreach($registrant as $registrantId => $value){
                       ?>
                        <div class="row-fluid-15 sub_check"><span class="checkbox_wrapper "><input type="checkbox" id="registrantcheckbox<?php echo $BreakoutStreamRowId; ?><?php echo '_'.$registrantId; ?>" name="registrantcheckbox<?php echo $BreakoutStreamRowId; ?><?php echo '['.$registrantId.']'; ?>"><label for="registrantcheckbox<?php echo $BreakoutStreamRowId; ?><?php echo '_'.$registrantId; ?>"><?php echo $value; ?></label></span>
                          <span class="limit_wrapper"><label for="attendees_limit"><?php echo lang('programer_limit'); ?></label>
                            <input type="text" value="" id="registrantlimit<?php echo $BreakoutStreamRowId.'_'.$registrantId; ?>" name="registrantlimit<?php echo $BreakoutStreamRowId.'_'.$registrantId; ?>" class=" small">
                          </span>
                        </div>
                        <?php } //end loop ?>
                      </div>
                      <?php } // end loop
                  } //end loop
                } // end if
              } // end if
            ?>
          
          </div>  
          <!-- Close session subcat -->
        </div>  

        
        <div id="session_div_<?php echo $BreakoutStreamRowId; ?>">
          
          <?php 
             $where=array('stream_id'=>$streamData->id); 
             $breakoutSessionData = getDataFromTabel('breakout_session','*',$where,'','session','ASC');
             $count='0';
             if(!empty($breakoutSessionData))
             {  
              foreach($breakoutSessionData as $key=>$sessionRecord){
              $count+=1;  
                  $sessonBrkId=$breakoutId.'_'.$count; 
              ?>
              <div id="accordion_sub" class="panel-group sub-panel-group full_subpanel sub_panel">
                <div class="panel-heading" >
                  <h4 class="panel-title medium ">
                  <a class="no_icon"><?php echo lang('programer_session'); ?> <?php echo $count; ?></a>                  
                  </h4>      
                </div>
              </div>

              
                <div id="session_block_<?php echo $BreakoutStreamRowId."_".$count; ?>" class="pt15">
                  <input id="breakoutSessionId<?php echo $BreakoutStreamRowId."_".$count; ?>"  type="hidden" value="<?php echo $sessionRecord->id; ?>" name="breakoutSessionId<?php echo $BreakoutStreamRowId; ?>[<?php echo $sessionRecord->session; ?>]">
                    <div class="row-fluid-15">
                    <label for="info_org"><?php echo lang('programer_session'); ?> <?php echo $count; ?> <?php echo lang('programer_topic'); ?> <span class="astrik">*</span>
                      <span class="info_btn">
                        <span class="field_info xsmall"><?php echo lang('programer_session'); ?> <?php echo $count; ?> <?php echo lang('programer_topic'); ?>
                        </span>
                      </span>
                    </label>
                    <input type="text" value="<?php echo $sessionRecord->session_name; ?>" id="sessionName<?php echo $BreakoutStreamRowId."_".$count; ?>" name="sessionName<?php echo $BreakoutStreamRowId."_".$count; ?>" class=" small">
                  </div>
                  <!--end of row-fluid-->
                  
                  <div class="row-fluid-15">
                      <label for="refr_no"><?php echo lang('programer_time'); ?> <span class="astrik">*</span></label>
                      <div class="time_wrapper date_wrapper d_timewrapper">
                         <span class="time1" class="date_wrapper">  
                          <label for="sessionStartTime<?php echo $BreakoutStreamRowId."_".$count; ?>"><?php echo lang('programer_time_start'); ?></label>
                          <input type="text" data-format="hh:mm:ss" value="<?php echo $sessionRecord->start_time; ?>" class="width_100px timepicker"  required="" id="sessionStartTime<?php echo $BreakoutStreamRowId."_".$count; ?>"  name="sessionStartTime<?php echo $BreakoutStreamRowId."_".$count; ?>"  data-parsley-error-message="<?php echo lang('common_field_required'); ?>" data-parsley-error-class="custom_li" data-parsley-trigger="keyup">
                          
                         </span>
                         <span class="time1" class="date_wrapper">    
                          <label for="sessionFinishTime<?php echo $BreakoutStreamRowId."_".$count; ?>"><?php echo lang('programer_time_finish'); ?></label>
                          <input type="text" data-format="hh:mm:ss" value="<?php echo $sessionRecord->end_time; ?>" class="width_100px timepicker" required="" id="sessionFinishTime<?php echo $BreakoutStreamRowId."_".$count; ?>"  name="sessionFinishTime<?php echo $BreakoutStreamRowId."_".$count; ?>"  data-parsley-error-message="<?php echo lang('common_field_required'); ?>" data-parsley-error-class="custom_li" data-parsley-trigger="keyup">
                          
                        </span> 
                      </div>
                   </div>
                   <!--end of row-fluid-->
                  
                  <?php
                  $presentersRecords = $sessionRecord->presenters_ids;
                  $presentersList = (!empty($presentersRecords)) ? json_decode($presentersRecords) : array();
                  ?>
                  <div class="row-fluid-15">
                    <label for="refr_no"><?php echo lang('programer_number_presenter'); ?>
                      <span class="info_btn">
                        <span class="field_info xsmall"><?php echo lang('programer_number_presenter'); ?>
                        </span>
                      </span>
                    </label>
                    
                    <input type="number" data-fieldid="<?php echo $BreakoutStreamRowId."_".$count; ?>" id="noOfPresenter<?php echo $BreakoutStreamRowId."_".$count; ?>" name="noOfPresenter<?php echo $BreakoutStreamRowId."_".$count; ?>" value="<?php echo $sessionRecord->number_of_presenters; ?>" required class="small dark presenter_class" step="1"  min="0" />
                  </div>                                        
                 
                  
                  <!--end of row-fluid-->
                  <div id="load_presenter_data_<?php echo $BreakoutStreamRowId."_".$count; ?>">
                  <?php 
                   if(!empty($presentersRecords)){ 
                     foreach($presentersList as $key=>$list){ ?>  
                    <div class="row-fluid-15" id="presenters_div_<?php echo $BreakoutStreamRowId."_".$count."_".$key; ?>">
                      <label for="info_org"><?php echo lang('programer_presenter'); ?></label>
                       
                        <select id="presenters_select_<?php echo $BreakoutStreamRowId."_".$count; ?>[<?php echo $key; ?>]" class="custom-select select_fwidth" name="presenters_select_<?php echo $BreakoutStreamRowId."_".$count; ?>[<?php echo $key; ?>]">
                          <option value="0"><?php echo lang('programer_select_presenter'); ?></option>
                          <?php
                          // get presenter data
                           if(!empty($presenterDetails)){
                            foreach($presenterDetails as $value){ 
                                $selected = ($value->id==$list) ? 'selected="selected"' : ""; 
                             ?>
                                <option value='<?php echo $value->id; ?>' <?php echo $selected; ?> ><?php echo ucfirst($value->title)." ".ucfirst($value->firstname)." ".ucfirst($value->lastname); ?></option>
                            <?php } //end loop
                          } // end if ?>
                        </select>
                      
                      </div>
                      <?php } // end loop ?>
                  <?php } ?>
                  </div>
                  
                  <div id="load_presenter_description_<?php echo $BreakoutStreamRowId."_".$count; ?>">
                    <?php 
                      if(!empty($presentersRecords)){ ?>
                    <div class="row-fluid-15">
                      <label for="presenter_description_<?php echo $BreakoutStreamRowId."_".$count; ?>">
                        <?php echo lang('programer_description'); ?>
                        <span class="info_btn">
                          <span class="field_info xsmall"></span>
                        </span>
                      </label>  
                      <textarea class="small" name="presenter_description<?php echo $BreakoutStreamRowId."_".$count; ?>" for="presenter_description<?php echo $BreakoutStreamRowId."_".$count; ?>"><?php echo $sessionRecord->presenters_description; ?></textarea>
                    </div>
                    <?php } ?>
                  </div>
                

                </div>  
                  
                  
             <?php } // end loop
            }
          ?>
          
            
        </div>  
        
       </div>
        
    </div>    
    <?php }  } ?>
               
               </div> 
            </div>
            <!--end of panel-group-->
