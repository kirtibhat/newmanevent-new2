<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//reset button
if(array_search('reset',$showbutton)!==false){ 
	$extraSave 	= 'class="'.$buttonclass['reset'].'"';
	echo form_button('form_reset',$labletext['reset'],$extraSave);
}

//save button
if(array_search('save',$showbutton)!==false){ 
	
	$getSaveButtonId = (!empty($saveButtonId))?$saveButtonId:'';
	$extraSave 	= 'class="'.$buttonclass['save'].'"'.$getSaveButtonId;
	//echo form_button('save',$labletext['save'],$extraSave);
	echo form_submit('save',$labletext['save'],$extraSave);
}

//use button
if(array_search('use',$showbutton)!==false){ 
	$extraSave 	= 'class="'.$buttonclass['use'].'"'.$buttonAttr['use'];
	echo form_button('use',$labletext['use'],$extraSave);
}
?>

