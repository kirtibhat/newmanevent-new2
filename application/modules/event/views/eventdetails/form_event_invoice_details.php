<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

//set field value in variable
$iheader = (!empty($eventdetails->event_header))? $eventdetails->event_header: $eventdetails->event_title;
$iname = (!empty($eventdetails->invoice_name))?$eventdetails->invoice_name:'';
$iabn = (!empty($eventdetails->abn))?$eventdetails->abn:'';
$iaddress1 = (!empty($eventdetails->iaddress1))?$eventdetails->iaddress1:'';
$iaddress2 = (!empty($eventdetails->iaddress2))?$eventdetails->iaddress2:'';
$icity = (!empty($eventdetails->icity))?$eventdetails->icity:'';
$istate = (!empty($eventdetails->istate))?$eventdetails->istate:'';
$ipostcode = (!empty($eventdetails->ipostcode))?$eventdetails->ipostcode:'';
$icountry = (!empty($eventdetails->icountry))?$eventdetails->icountry:'';
$isendtoemail = (!empty($eventdetails->sent_to_email))?$eventdetails->sent_to_email:'';
$isendfromemail = (!empty($eventdetails->sent_from_email))?$eventdetails->sent_from_email:'';
$iintro = (!empty($eventdetails->invoice_intro))?$eventdetails->invoice_intro:'';
$iiaboveval = (!empty($eventdetails->iabovechk))?$eventdetails->iabovechk:'';
$iiIsABN = (!empty($eventdetails->is_abn))?$eventdetails->is_abn:'';
$iiisCollectGst = (!empty($eventdetails->is_collect_gst))?$eventdetails->is_collect_gst:'';
$iigstRate  = (!empty($eventdetails->gst_rate))?$eventdetails->gst_rate:'10';



$abnRegisteredChk  = true;
$abnNotRegisteredChk = false;
$isABNDivShow = '';
if($iiIsABN==1){
  $abnRegisteredChk  = true;
  $abnNotRegisteredChk = false;
}else{
  $abnRegisteredChk  = false;
  $abnNotRegisteredChk = true;
  $isABNDivShow = 'dn';
}

//gst collect section
$GSTCollectYesChk  = true;
$GSTCollectNoChk = false;
$isregisteredGSTShow = '';
if($iiisCollectGst=='1'){
  $GSTCollectYesChk  = true;
  $GSTCollectNoChk = false;
}else{
  $GSTCollectYesChk  = false;
  $GSTCollectNoChk = true;
  $isregisteredGSTShow = 'dn';
}

$formEventInvoiceDetails = array(
    'name'   => 'formEventInvoiceDetails',
    'id'   => 'formEventInvoiceDetails',
    'method' => 'post',
    'class'  => '',
    'data-parsley-validate' => '',
  );  

$eventInvoiceHeader = array(
    'name'  => 'eventInvoiceHeader',
    'value' => $iheader,
    'id'  => 'eventInvoiceHeader',
    'type'  => 'text',
    'required'  => '',
    'class' =>'alphaNumValue small',
    'data-parsley-error-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
     
  );
  
$eventInvoiceName = array(
    'name'  => 'eventInvoiceName',
    'value' => $iname,
    'id'  => 'eventInvoiceName',
    'type'  => 'text',
    'required'  => '',
    'class' =>'alphaNumValue small',
    'data-parsley-error-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
  );  
  
$abnRegistered = array(
    'name'  => 'isABN',
    'value' => '1',
    'id'  => 'abnRegistered',
    'type'  => 'radio',
    'class' => 'abnRegistered',
    'checked' => $abnRegisteredChk,
  );  

$abnNotRegistered = array(
    'name'  => 'isABN',
    'value' => '0',
    'id'  => 'abnNotRegistered',
    'type'  => 'radio',
    'class' => 'abnRegistered',
    'checked' => $abnNotRegisteredChk,
  );  
  
$eventInvoiceABN = array(
    'name'  => 'eventInvoiceABN',
    'value' => $iabn,
    'id'  => 'eventInvoiceABN',
    'type'  => 'text',
    //'required'  => '',
    'class' => 'width_130 small'
  );

$registeredGSTYes = array(
    'name'  => 'isCollectGst',
    'value' => '1',
    'id'  => 'registeredGSTYes',
    'type'  => 'radio',
    'class' => 'registeredGST',
    'checked' => $GSTCollectYesChk,
  );  

$registeredGSTNo = array(
    'name'  => 'isCollectGst',
    'value' => '0',
    'id'  => 'registeredGSTNo',
    'type'  => 'radio',
    'class' => 'registeredGST',
    'checked' => $GSTCollectNoChk,
  );  
  
$eventGSTRate = array(
    'name'  => 'eventGSTRate',
    'value' => $iigstRate,
    'id'  => 'eventGSTRate',
    'type'  => 'text',
  //  'required'  => '',
    'readonly'  => '',
    'class' => 'small disable_input',
  );  
  
$aboveAddressApply = array(
    'name'  => 'aboveAddressApply',
    'value' => '1',
    'id'  => 'aboveAddressApply',
    'type'  => 'checkbox',
    'class' => 'pull-left ml10 mr15 checkbox',
  );  
  

if(!empty($iiaboveval)){
  $aboveAddressApply['checked']=true;
} 
  
$eventInvoiceAddress1 = array(
    'name'  => 'eventInvoiceAddress1',
    'value' => $iaddress1,
    'id'  => 'eventInvoiceAddress1',
    'type'  => 'text',
    'required'  => '',
    'class'=>'small',
    'data-parsley-error-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
  );  
  
$eventInvoiceAddress2 = array(
    'name'  => 'eventInvoiceAddress2',
    'value' => $iaddress2,
    'id'  => 'eventInvoiceAddress2',
    'type'  => 'text',
    'class'=> 'small',
  );  

$eventInvoicePostcode = array(
    'name'  => 'eventInvoicePostcode',
    'value' => $ipostcode,
    'id'  => 'eventInvoicePostcode',
    'type'  => 'text',
    'class' => 'width50per width_130 small',
    'required'  => '',
    'data-parsley-error-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
  );
  
$eventInvoiceEmailSentto = array(
    'name'  => 'eventInvoiceEmailSentto',
    'value' => $isendtoemail,
    'id'  => 'eventInvoiceEmailSentto',
    'type'  => 'email',
    'data-parsley-error-class' => 'custom_li',
    'class' => 'small'
  );
  
$eventInvoiceEmailSenttoConfirm = array(
    'name'  => 'eventInvoiceEmailSenttoConfirm',
    'value' => $isendtoemail,
    'id'  => 'eventInvoiceEmailSenttoConfirm',    
    'class'=>'small',
    'autocomplete' => 'off',    
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-equalto' => '#eventInvoiceEmailSentto',
    'data-parsley-equalto-message' => lang('common_confirm_email_equal'),
    'data-parsley-trigger' => 'keyup',
  );
  
$eventInvoiceEmailSentfrom = array(
    'name'  => 'eventInvoiceEmailSentfrom',
    'value' => $isendfromemail,
    'id'  => 'eventInvoiceEmailSentfrom',
    'type'  => 'email',
    'required'  => '',
    'data-parsley-error-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
    'class' => 'small'
  );  
  
$eventInvoiceEmailSentfromConfirm = array(
    'name'  => 'eventInvoiceEmailSentfromConfirm',
    'value' => $isendfromemail,
    'id'  => 'eventInvoiceEmailSentfromConfirm',
    'class'=>'small',
    'required'  => '',
    'autocomplete' => 'off',
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-equalto' => '#eventInvoiceEmailSentfrom',
    'data-parsley-equalto-message' => lang('common_confirm_email_equal'),
    'data-parsley-trigger' => 'keyup',
  );
  
  $eventInvoiceInstruction = array(
    'name'  => 'eventInvoiceInstruction',
    'value' => $iintro,
    'id'  => 'eventInvoiceInstruction',
    'type'  => 'text',
    'class' => 'small'
  );  
  $eventInvoiceCountry = array(
    'name'  => 'eventInvoiceCountry',
    'value' => $icountry,
    'id'  => 'eventInvoiceCountry',
    'type'  => 'text',
    'class' => 'small',
    'required'  => '',
    'data-parsley-error-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
    'class' => 'small'
  );
  $eventInvoiceState = array(
    'name'  => 'eventInvoiceState',
    'value' => $istate,
    'id'  => 'eventInvoiceState',
    'type'  => 'text',
    'class' => 'width_130 small',
    'required'  => '',
    'data-parsley-error-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
  );
  $eventInvoiceCity = array(
    'name'  => 'eventInvoiceCity',
    'value' => $icity,
    'id'  => 'eventInvoiceCity',
    'type'  => 'text',
    'required'  => '',
    'data-parsley-error-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
    'class' => 'small'
  );
 
?>
  
  
   <div class="panel event_panel">
                    <div class="panel-heading ">
                        <h4 class="panel-title medium dt-large ">
                        <a class=" " data-toggle="collapse" data-parent="#accordion" href="#collapseFour"><?php echo lang('event_event_invoice_details'); ?></a>
                        </h4>
                    </div>
                    <div style="height: auto;" id="collapseFour" class="accordion_content collapse apply_content">
                      <?php echo form_open($this->uri->uri_string(),$formEventInvoiceDetails); ?>
                        <div class="panel-body ls_back dashboard_panel small" id="showhideformdiveventInvoice">
                            <div class="row-fluid-15">
                                <label for="amc_con"><?php echo lang('event_invoice_name'); ?> <span class="astrik">*</span>
                  <span class="info_btn"><span class="field_info xsmall"><?php echo lang('tooltip_invoice_event_header'); ?></span></span>
                                </label>
                                <?php echo form_input($eventInvoiceHeader); ?>
                <?php echo form_error('eventInvoiceHeader'); ?>
                                
                            </div>
                            <!--end of row-fluid-->
          
                            <div class="row-fluid-15">
                                <label for="invoice_entity"><?php echo lang('event_invioce_eventing_entity'); ?> <span class="astrik">*</span>
                                <span class="info_btn"><span class="field_info xsmall"><?php echo lang('tooltip_invoice_eventing_entity'); ?></span></span>
                                </label>
                                <?php echo form_input($eventInvoiceName); ?>
                <?php echo form_error('eventInvoiceName'); ?>
                            </div>
                            <!--end of row-fluid-->
          
                            <div class="row-fluid-15">
                                <label for="abn"><?php echo lang('event_invoice_abn'); ?> <span class="astrik">*</span>
                                  <span class="info_btn"><span class="field_info xsmall"><?php echo lang('tooltip_invoice_abn'); ?></span></span>
                                </label>
                                <div class="radio_wrapper">
                                                                
                                  <?php echo form_radio($abnRegistered); ?>
                                  <label for="abnRegistered">Registered</label>

                                  <?php echo form_radio($abnNotRegistered);  ?>
                                    <label for="abnNotRegistered">Not Registered</label>
                                </div>
                            </div>
                            
                            <div class="isABNShow  <?php echo $isABNDivShow; ?>">
                
                <div class="row-fluid">
                  <label for="event_invoice_abn">
                  </label>
                  <?php echo form_input($eventInvoiceABN); ?>
                  <?php echo form_error('eventInvoiceABN'); ?>  
                </div>
                
                            <!--end of row-fluid-->
                <div class="row-fluid">
                  
                                <label for="collect_gst"><?php echo lang('event_invoice_regis_to_collect'); ?> <span class="astrik">*</span>
                                  <span class="info_btn"><span class="field_info xsmall"><?php echo lang('tooltip_invoice_regis_to_collect'); ?></span></span>
                                </label>
                                <div class="radio_wrapper">
                                  <?php echo form_radio($registeredGSTYes); ?>
                                  <label for="registeredGSTYes"><?php echo lang('comm_yes'); ?></label>

                                  <?php echo form_radio($registeredGSTNo);  ?>
                                    <label for="registeredGSTNo"><?php echo lang('comm_no'); ?></label>
                                </div>
                            </div>
              <div class="control-group mb10 isregisteredGSTShow <?php echo $isregisteredGSTShow; ?>">
                <!--end of row-fluid-->
                  <div class="row-fluid">
                  <label for="gst_rate">
                        <?php echo lang('event_invoice_regis_rate'); ?> <!--<span class="astrik">*</span>
                        <span class="info_btn"><span class="field_info xsmall"><?php echo lang('tooltip_invoice_regis_to_collect'); ?></span></span>-->
                  </label>
                  <div class="rate">
                    <?php echo form_input($eventGSTRate); ?>
                    <?php echo form_error('eventGSTRate'); ?>
                  </div>
                </div>
                <!--end of row-fluid-->
                 </div>
               </div> 
              
              
                            <div class="row-fluid-15">
                                <label for="address"><?php echo lang('event_invoice_address'); ?> <span class="astrik">*</span>
                                  <span class="info_btn"><span class="field_info xsmall"><?php echo lang('tooltip_invoice_above_address'); ?></span></span>
                                </label>
                                <div class="checkbox_wrapper">

                                  <?php echo form_checkbox($aboveAddressApply); ?>
                                    <label for="aboveAddressApply"><?php echo lang('event_invoice_above_address'); ?></label>
                                </div>
                                <?php echo form_input($eventInvoiceAddress1); ?>
                <?php echo form_error('eventInvoiceAddress1'); ?>
                                 
                                <?php echo form_input($eventInvoiceAddress2); ?>
                                <?php echo form_error('eventInvoiceAddress2'); ?>
                                
                            </div>
                            <!--end of row-fluid-->
          
                            <div class="row-fluid-15">
                                <label for="city"><?php echo lang('event_invoice_city'); ?> <span class="astrik">*</span></label>
                                <?php 
                  //$other = 'required id="eventInvoiceCity"';
                  //echo form_dropdown('eventInvoiceCity',$masterCityDataList,$icity,$other);
                  echo form_input($eventInvoiceCity); 
                  echo form_error('eventInvoiceCity');
                ?>
                            </div>
                            <!--end of row-fluid-->
          
                            <div class="row-fluid-15">
                                <label for="state"><?php echo lang('event_invoice_state'); ?> <span class="astrik">*</span></label>
                                <?php 
                  //$other = 'required id="eventInvoiceState"';
                  //echo form_dropdown('eventInvoiceState',$masterSateDataList,$istate,$other);
                  echo form_input($eventInvoiceState); 
                  echo form_error('eventInvoiceState');
                ?>
                               
                            </div>
                            <!--end of row-fluid-->
          
                            <div class="row-fluid-15">
                                <label for="pcode"><?php echo lang('event_invoice_postcode'); ?> <span class="astrik">*</span></label>
                                <?php echo form_input($eventInvoicePostcode); ?>
                <?php echo form_error('eventInvoicePostcode'); ?>
                            </div>
                            <!--end of row-fluid-->
          
                            <div class="row-fluid-15">
                                <label for="country"><?php echo lang('event_invoice_country'); ?> <span class="astrik">*</span></label>
                                <?php 
                  //$other = 'required id="eventInvoiceCountry"';
                  //echo form_dropdown('eventInvoiceCountry',$masterCountryDataList,$icountry,$other);
                  
                  echo form_input($eventInvoiceCountry); 
                  echo form_error('eventInvoiceCountry');
                ?>
                            </div>
                            <!--end of row-fluid-->
          
                            <div class="row-fluid-15">
                                <label for="email_invoice"><?php echo lang('event_invoice_email_copy_to'); ?> <span class="info_btn"><span class="field_info xsmall"><?php echo lang('tooltip_invoice_email_copy_to'); ?></span></span>
                                </label>
                                <?php echo form_input($eventInvoiceEmailSentto); ?>
                                <?php echo form_error('eventInvoiceEmailSentto'); ?>
                            </div>
                            <!--end of row-fluid-->
          
                            <div class="row-fluid-15">
                                <label for="email_invoice"><?php echo lang('event_invoice_email_copy_to_con'); ?> <span class="info_btn"><span class="field_info xsmall"><?php echo lang('event_invoice_email_copy_to_con'); ?></span></span>
                                </label>
                                <?php echo form_input($eventInvoiceEmailSenttoConfirm); ?>
                                <?php echo form_error('eventInvoiceEmailSenttoConfirm'); ?>
                            </div>
                            <!--end of row-fluid-->
          
                            <div class="row-fluid-15">
                                <label for="email_from"><?php echo lang('event_invoice_email_send_form'); ?> <span class="astrik">*</span>
                                <span class="info_btn"><span class="field_info xsmall"><?php echo lang('tooltip_invoice_email_send_from'); ?></span></span>
                                </label>
                                <?php echo form_input($eventInvoiceEmailSentfrom); ?>
                                <?php echo form_error('eventInvoiceEmailSentfrom'); ?>
                            </div>
                            <!--end of row-fluid-->
          
                            <div class="row-fluid-15">
                                <label for="email_from"><?php echo lang('event_invoice_email_send_form_con'); ?> <span class="astrik"></span>
                                <span class="info_btn"><span class="field_info xsmall"><?php echo lang('event_invoice_email_send_form_con'); ?></span></span>
                                </label>
                                <?php echo form_input($eventInvoiceEmailSentfromConfirm); ?>
                                <?php echo form_error('eventInvoiceEmailSentfromConfirm'); ?>
                            </div>
                            <!--end of row-fluid-->

                            <div class="row-fluid-15">
                                <label for="invoice_intro"><?php echo lang('event_invoice_intruduction'); ?> <span class="info_btn"><span class="field_info xsmall"><?php echo lang('tooltip_invoice_intruduction'); ?></span></span>
                                </label>
                                <?php echo form_input($eventInvoiceInstruction); ?>
                  <?php echo form_error('eventInvoiceInstruction'); ?>
                            </div>
                            <!--end of row-fluid-->
          
                            <div class="btn_wrapper ">
                              <a href="#collapseFour" class="pull-left scroll_top"><?php echo lang('comm_top'); ?></a>

                              <?php 
                  echo form_hidden('eventId', $eventId);
                  echo form_hidden('formActionName', 'eventInvoiceDetails');
                  
                  //button show of save and reset
                  $formButton['showbutton']   = array('save','reset');
                  $formButton['labletext']  = array('save'=>lang('comm_save_changes'),'reset'=>lang('comm_reset'));
                  $formButton['buttonclass']  = array('save'=>'default_btn btn pull-right medium','reset'=>'default_btn btn pull-right medium reset_form');
                  $this->load->view('common_save_reset_button',$formButton);
                ?>
                              
                            </div>
                        </div>
                       <?php echo form_close();?>
                    </div>
                </div>
                <!--end of panel-->

               
<!-- /commonform_bg --> 
      


<script>
  
  
  //save event invoice details
  ajaxdatasave('formEventInvoiceDetails','<?php echo $this->uri->uri_string(); ?>',true,true,false,false,false,'#showhideformdiveventInvoice','#showhideformdiveventcategory');
  
  $("#aboveAddressApply").click(function(){
    var getStatus = $(this).is(':checked');
    
    //get contact person form data
    var cpAddress1  = $('#address_line1').val();
    var cpAddress2  = $('#address_line2').val();
    var cpCity    = $('#city').val();
    var cpState   = $('#state').val();
    var cpPostcode  = $('#postcode').val();
    var cpCountry   = $('#country').val();
    
    if(getStatus){
      //set value in event invoice form
      $('#eventInvoiceAddress1').val(cpAddress1);
      $('#eventInvoiceAddress2').val(cpAddress2);
      $('#eventInvoicePostcode').val(cpPostcode);
      $('#eventInvoiceState').val(cpCity);
      $('#eventInvoiceCity').val(cpState);
      $('#eventInvoiceCountry').val(cpCountry);
    }else{
      //set default blank
      $('#eventInvoiceAddress1').val('');
      $('#eventInvoiceAddress2').val('');
      $('#eventInvoicePostcode').val('');
      
      //set blank country, state and city field
      $('#eventInvoiceState').html('<option valu="">Select State</option>');
      $('#eventInvoiceCity').html('<option valu="">Select City</option>');
      $('#eventInvoiceState').val('');
      $('#eventInvoiceCity').val('');
      $('#eventInvoiceCountry').val('');
    }
  }); 
  
  //show and hide abn entered field by condition wise
  
  
  $('.abnRegistered').click(function(){
    
    // For set collapse hight
    $(".accordion_content").css("height",'auto');
    
    var abnstatus = parseInt($(this).val());
    if(abnstatus==1){
      $(".isABNShow").slideDown('slow');
    }else{
      $(".isABNShow").slideUp('slow');
    }
  });
  
  //show and hide abn entered field by condition wise
  $('.registeredGST').click(function(){
    
    // For set collapse hight
    $(".accordion_content").css("height",'auto');
    
    var abnstatus = parseInt($(this).val());
    if(abnstatus==1){
      $(".isregisteredGSTShow").slideDown('slow');
    }else{
      $(".isregisteredGSTShow").slideUp('slow');
    }
  });
  
  //set master state list data by state selection
  $('#eventInvoiceCountry').change(function(){
    var countryId = $(this).val();
    sendData = {'countryId':countryId};
    postUrl = 'event/masterstatelist';
    doAjaxRequest(postUrl,sendData,'eventInvoiceState');
    //set blank data in field
    $('#eventInvoiceState').html('<option valu="">Select State</option>');
    $('#eventInvoiceCity').html('<option valu="">Select City</option>');
  }); 
  
  //set master city list data by state selection
  $('#eventInvoiceState').change(function(){
    var stateId = $(this).val();
    sendData = {'stateId':stateId};
    postUrl = 'event/mastercitylist';
    doAjaxRequest(postUrl,sendData,'eventInvoiceCity');
  }); 
  
</script> 
