<?php if (!defined('BASEPATH'))  exit('No direct script access allowed');
/* Get value from database */
$venue_contact_firstname_val    = (!empty($eventdetails->venue_contact_firstname)) ? $eventdetails->venue_contact_firstname : '';
$venue_contact_lastname_val     = (!empty($eventdetails->venue_contact_lastname)) ? $eventdetails->venue_contact_lastname : '';
$venue_contact_email_val        = (!empty($eventdetails->venue_contact_email)) ? $eventdetails->venue_contact_email : '';

$venue_contact_mobile        = (!empty($eventdetails->venue_contact_mobile)) ? $eventdetails->venue_contact_mobile : '';
$venue_contact_landline        = (!empty($eventdetails->venue_contact_landline)) ? $eventdetails->venue_contact_landline : '';

$phone_value='';
$selectedClssMob ='selected';
$selectedClsslandline ='';
if(!empty($eventdetails->venue_contact_mobile)) {
    $phone_value = $eventdetails->venue_contact_mobile;
    //$selectedClssMob ='selected';
}

if(!empty($eventdetails->venue_contact_landline)) {
    $phone_value = $eventdetails->venue_contact_landline;
    //$selectedClsslandline ='selected';
}    

if(!empty($eventdetails->venue_contact_mobile) && !empty($eventdetails->venue_contact_landline)) {
    $selectedClssMob ='selected';
    $phone_value = $eventdetails->venue_contact_mobile;
}

if(empty($eventdetails->venue_contact_mobile) && empty($eventdetails->venue_contact_landline)) {
    $selectedClssMob ='selected';
    //$phone_value = $eventdetails->phone1_landline;
}


/* Define form fields */
$formVenueContactPersonSetup = array(
    'name'          => 'formVenueContactPerson',
    'id'            => 'formVenueContactPerson',
    'method'        => 'post',
    'class'         => '',
    'data-parsley-validate' => ''
);
$venue_contact_firstname = array(
    'name'          => 'venue_contact_firstname',
    'value'         => $venue_contact_firstname_val,
    'id'            => 'venue_contact_firstname',
    'type'          => 'text',
    'class'         => 'xlarge_input',
    'autocomplete'  => 'off',
);
$venue_contact_lastname = array(
    'name'          => 'venue_contact_lastname',
    'value'         => $venue_contact_lastname_val,
    'id'            => 'venue_contact_lastname',
    'type'          => 'text',
    'class'         => 'xlarge_input',
    'autocomplete'  => 'off',
);
$venue_contact_email = array(
    'name'          => 'venue_contact_email',
    'value'         => $venue_contact_email_val,
    'id'            => 'venue_contact_email',
    'type'          => 'text',
    'class'         => 'xlarge_input',
    'autocomplete'  => 'off',
);

$venueContactPhone = array(
'name'          => 'venueContactPhone',
'value'         => $phone_value,
'id'            => 'venueContactPhone',
'type'          => 'text',
'class'         => 'medium_input pull-left contact_input phoneValue contacteventdata',
'placeholder'   => '123456789',
'size'          => '40',
); 
?>
<div class="panel" id="venue_contact_person_panel">
      <div class="panel_header">Venue Contact Person</div>
      <div class="panel_contentWrapper" id="showhideformdivVenueContactPerson">
      <div class="infobar">
        <p class="dn" id="venue_contact_person_info">The Access ID# is a unique identifier assigned to every registrant using this site. Use it to access, edit, cancel, pay and rebook items in your registration.</p>
        <span class="info_btn" onclick="showHideInfoBar('venue_contact_person_info');"></span>
      </div>
      	<!--sub menu one-->
        <?php echo form_open($this->uri->uri_string(), $formVenueContactPersonSetup); ?>
        <input type="hidden" name="collapseValue" value="collapseTwo" />
              <div class="panel_content">
                <div class="row">
                  <div class="col-3">
                    <div class="labelDiv">
                      <label class="form-label text-right">First Name </label>
                    </div>
                  </div>
                  <div class="col-5">
                    <?php echo form_input($venue_contact_firstname); ?>
                    <?php echo form_error('venue_contact_firstname'); ?>
                    <span id="error-dinner_dance6"></span> </div>
                </div>
                <div class="row">
                  <div class="col-3">
                  <div class="labelDiv">
                      <label class="form-label text-right">Last Name </label>
                    </div>
                  </div>
                  <div class="col-5">
                    <?php echo form_input($venue_contact_lastname); ?>
                    <?php echo form_error('venue_contact_lastname'); ?>
                    <span id="error-dinner_dance6"></span> </div>
                </div>
                
                <div class="row">
                  <div class="col-3">
                    <div class="labelDiv">
                      <label class="form-label text-right">Email</label>
                    </div>
                  </div>
                  <div class="col-5">
                    <?php echo form_input($venue_contact_email); ?>
                    <?php echo form_error('venue_contact_email'); ?>
                    <span id="error-venue_contact_email"></span> </div>
                </div>
              
                
                 <div class="row">
                  <div class="col-3">
                    <div class="labelDiv">
                      <label class="form-label text-right"><?php echo lang('event_contact_phone_free'); ?> 
                      
                      </label>
                    </div>
                  </div>
                  <div class="col-5">
                    <input type='hidden' name='input_hidden_mobile2' id='input_hidden_mobile2' value="<?php echo $venue_contact_mobile; ?>" >
                    <input type='hidden' name='input_hidden_landline2' id='input_hidden_landline2' value="<?php echo $venue_contact_landline; ?>">

                     <div class="phone_box col-3">            
                        <?php echo form_input($venueContactPhone); ?>
                        <span id="buttonid2"><img src="<?php echo IMAGE; ?>drparw.png" width="28" height="28"></span>
                        <ul id="toggle2" class="phone_field_opt change_input_class" style="display:none;">
                            <li data-select="mobile2" class="<?php echo $selectedClssMob; ?> contact_event" contactType="mobile2" id="mobile2">Mobile</li>
                            <li data-select="landline2" class="<?php echo $selectedClsslandline; ?> contact_event" contactType="landline2" id="landline2">Landline</li>
                          </ul>
                          <span id="error-contactPhone"></span> </div>
                        </div>
                </div>
                
              </div>
              <div class="panel_footer"> 
                <div class="pull-right">
                    <?php
                        echo form_hidden('eventId', $eventId);              
                        echo form_hidden('formActionName', 'eventVenueContactPerson');
                        //button show
                        $formButton['showbutton'] = array('save', 'reset');
                        $formButton['labletext'] = array('save' => lang('comm_save_changes'), 'reset' => lang('comm_reset'));
                        $formButton['buttonclass'] = array('save' => 'btn-normal btn', 'reset' => 'btn-normal btn reset_space reset_form');
                        $this->load->view('common_save_reset_button', $formButton);
                    ?>
                </div>
              </div>
            <?php echo form_close(); ?>
      </div>
</div>
