<?php if (! defined('BASEPATH')) { exit('No direct script access allowed'); }
$this->load->view('event_menus'.$this->config->item('fileSuffix'));
echo '<div class="page_content">
            <div class="container">
                <div class="row">
                    <div class="col-9">';
                    // load general_event_setup view
                    $this->load->view('form_general_event_setup'.$this->config->item('fileSuffix'));
                    // load general_event_setup venue view
                    $this->load->view('form_event_venue'.$this->config->item('fileSuffix'));
                    $this->load->view('form_event_venue_free_contact_person');
                    // load form_contact view
                    $this->load->view('form_contact_person'.$this->config->item('fileSuffix'));
                    // load social media view
                    $this->load->view('form_social_media'.$this->config->item('fileSuffix'));
                    // load form_event_bank_details view
                    $this->load->view('form_event_terms_conditions'.$this->config->item('fileSuffix'));        
                    echo '</div>';
                    //next and previous button
                    $buttonData['viewbutton'] = array('back','next','preview');
                    $buttonData['linkurl']  = array('back'=>'','next'=>base_url('event/invitations'),'preview'=>'');
                    $this->load->view('common_back_next_buttons', $buttonData);
            echo '</div>';
        echo '</div>';
    echo '</div>';
echo '</div>';
?>
<?php
/*
 * Check user agent
 * Desktop/Mobile
 */
$ua = $_SERVER["HTTP_USER_AGENT"];
/*
 * Mobile
 */ 
$android    = strpos($ua, 'Android') ? true : false;
$blackberry = strpos($ua, 'BlackBerry') ? true : false;
$iphone     = strpos($ua, 'iPhone') ? true : false;
$palm       = strpos($ua, 'Palm') ? true : false;
//$ipad     = strpos($ua, 'iPad') ? true : false;
/*
 * Desktop
 */ 
$linux      = strpos($ua, 'Linux') ? true : false;
$mac        = strpos($ua, 'Macintosh') ? true : false;
$win        = strpos($ua, 'Windows') ? true : false;
if($android == true || $blackberry == true || $iphone == true || $palm == true) {
    $divId = 'map_canvas_mob';
}else{
    $divId = 'map_canvas';
}
?>
<div id="opentModelBox"></div>
<!--start custom tooltip-->
<div id="tooltip_wrapper"></div>

<script> var divIdMapCanvas = '<?php echo $divId;  ?>'; </script>
<script type="text/javascript" src="<?php echo $js_path . 'pagejs/events_details.js'; ?>"></script>
<script src="http://maps.google.com/maps/api/js?libraries=places&region=uk&language=en&sensor=true"></script>
