<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

if(!empty($userdata)){
  $userdata = $userdata[0];
}

//set user details data

$userFirstName      = (!empty($userdata->firstname))?$userdata->firstname:'';
$userLastName       = (!empty($userdata->lastname))?$userdata->lastname:'';
$userEmail        = (!empty($userdata->email))?$userdata->email:'';
$userCompany      = (!empty($userdata->company_name))?$userdata->company_name:'';
$userPosition     = (!empty($userdata->position))?$userdata->position:'';
$userAddressLine1   = (!empty($userdata->address_line1))?$userdata->address_line1:'';
$userAddressLine2     = (!empty($userdata->address_line2))?$userdata->address_line2:'';
$userCity         = (!empty($userdata->city))?$userdata->city:'';
$userState        = (!empty($userdata->state))?$userdata->state:'';
$userCountry      = (!empty($userdata->country))?$userdata->country:'';
$userStatePostcode    = (!empty($userdata->postcode))?$userdata->postcode:'';
$userPhone        = (!empty($userdata->phone))?$userdata->phone:'';
$userFax        = (!empty($userdata->fax))?$userdata->fax:'';
$userMobile       = (!empty($userdata->mobile))?$userdata->mobile:'';
$userWebsite      = (!empty($userdata->website))?$userdata->website:'';
$userFbid      = (!empty($userdata->fbid))?$userdata->fbid:'';
$userTwitterid      = (!empty($userdata->twitterid))?$userdata->twitterid:'';
$userLinkedin      = (!empty($userdata->linkedin))?$userdata->linkedin:'';
$userGplus      = (!empty($userdata->gplus))?$userdata->gplus:'';
$userPintrest      = (!empty($userdata->pintrest))?$userdata->pintrest:'';



//set field value in variable
$contactPersionId   = (!empty($eventdetails->contact_persion_id))?$eventdetails->contact_persion_id:'0';
$cptitle    = (!empty($eventdetails->title))?$eventdetails->title:'';
$cpfirstname  = (!empty($eventdetails->first_name))?$eventdetails->first_name:$userFirstName;
$cplastname   = (!empty($eventdetails->last_name))?$eventdetails->last_name:$userLastName;
$cporganisation = (!empty($eventdetails->organisation))?$eventdetails->organisation:$userCompany;
$cpposition   = (!empty($eventdetails->position))?$eventdetails->position:$userPosition;
$cpaddress1   = (!empty($eventdetails->address))?$eventdetails->address:$userAddressLine1;
$cpaddress2   = (!empty($eventdetails->address_second))?$eventdetails->address_second:$userAddressLine2;
$cpcity       = (!empty($eventdetails->city))?$eventdetails->city:$userCity;
$cpstate      = (!empty($eventdetails->state))?$eventdetails->state:$userState;
$cppostcode   = (!empty($eventdetails->postcode))?$eventdetails->postcode:$userStatePostcode;
$cpcountry    = (!empty($eventdetails->country))?$eventdetails->country:'Australia';

$cpfax            = (!empty($eventdetails->fax))?$eventdetails->fax:$userFax;
$cpmobile         = (!empty($eventdetails->mobile))?$eventdetails->mobile:$userMobile ;
$cpemail          = (!empty($eventdetails->email))?$eventdetails->email:$userEmail;
$cpweb            = (!empty($eventdetails->web))?$eventdetails->web:$userWebsite;
$contactFb        = (!empty($eventdetails->fbid))?$eventdetails->fbid:$userFbid;
$contactTwitter   = (!empty($eventdetails->twitterid))?$eventdetails->twitterid:$userTwitterid;
$contactLinkedin  = (!empty($eventdetails->linkedin))?$eventdetails->linkedin:$userLinkedin;
$contactGplus     = (!empty($eventdetails->gplus))?$eventdetails->gplus:$userGplus;
$contactPintrest  = (!empty($eventdetails->pintrest))?$eventdetails->pintrest:$userPintrest;


$cpeventdetails   = (!empty($eventdetails->event_contact_person))?$eventdetails->event_contact_person:'';

$phone1_mobile    = (!empty($eventdetails->phone1_mobile))?$eventdetails->phone1_mobile:"";
$phone1_landline  = (!empty($eventdetails->phone1_landline))?$eventdetails->phone1_landline:$userPhone;

$phone2_mobile    = (!empty($eventdetails->phone2_mobile))?$eventdetails->phone2_mobile:"";
$phone2_landline  = (!empty($eventdetails->phone2_landline))?$eventdetails->phone2_landline:"";

$eventUserDetailsChk   = true;
$eventClientDetailsChk = false;
if(!empty($cpeventdetails)){
  if($cpeventdetails==1){
    $eventUserDetailsChk  = true;
    $eventClientDetailsChk = false;
  }else{
    $eventUserDetailsChk  = false;
    $eventClientDetailsChk = true;
  }
}
$cpfax1 ="";

if($cpfax!=""){
  $faxString =explode('-',$cpfax);
  $cpfax = (isset($faxString[0])) ? $faxString[0] : ""; 
  $cpfax1 = (isset($faxString[1])) ? $faxString[1] : ""; 
}


$formGeneralEventSetup = array(
    'name'   => 'formContactPerson',
    'id'   => 'formContactPerson',
    'method' => 'post',
    'class'  => '',
    'data-parsley-validate' => '',
);
  
$eventUserDetails = array(
    'name'  => 'eventContactPerson',
    'value' => '1',
    'id'  => 'eventUserDetails',
    'type'  => 'radio',
    'class' => 'event_contact_person',
    'checked' => $eventUserDetailsChk,
);  

$eventClientDetails = array(
    'name'  => 'eventContactPerson',
    'value' => '2',
    'id'  => 'eventClientDetails',
    'type'  => 'radio',
    'class' => 'event_contact_person',
    'checked' => $eventClientDetailsChk,
);      
    
$firstName = array(
    'name'  => 'firstName',
    'value' => $cpfirstname,
    'id'  => 'firstname',
    'type'  => 'text',
    'required'  => '',
    'autocomplete' => 'off',
    'data-parsley-error-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
    'class' => 'small'
);  

$lastName = array(
    'name'  => 'lastName',
    'value' => $cplastname,
    'id'  => 'lastname',
    'type'  => 'text',
    'required'  => '',
    'class' =>'alphaValue small dark',
    'autocomplete' => 'off',
    'data-parsley-error-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
  );  
  
$contactOrganisation = array(
    'name'  => 'contactOrganisation',
    'value' => $cporganisation,
    'id'  => 'company_name',
    'type'  => 'text',
    'required'  => '',
    'class' =>'alphaNumValue small dark',
    'autocomplete' => 'off',
    'data-parsley-error-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
  );
$contactPosition = array(
    'name'  => 'contactPosition',
    'value' => $cpposition,
    'id'  => 'position',
    'type'  => 'text',
    'class' => 'small',
    'autocomplete' => 'off',
    'data-parsley-error-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
  
  );  

$contactAddress1 = array(
    'name'  => 'contactAddress1',
    'value' => $cpaddress1,
    'id'  => 'address_line1',
    'type'  => 'text',
    'required'  => '',
    'class' =>'small',
    'autocomplete' => 'off',
    'data-parsley-error-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
  );

$contactAddress2 = array(
    'name'  => 'contactAddress2',
    'value' => $cpaddress2,
    'id'  => 'address_line2',
    'type'  => 'text',
    'class' =>'small',
  );  

$contactCity = array(
    'name'  => 'contactCity',
    'value' => $cpcity,
    'id'  => 'city',
    'type'  => 'text',
    'required'  => '',
    'class' =>'small',
    'autocomplete' => 'off',
    'data-parsley-error-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
  );
  
$contactPostcode = array(
    'name'  => 'contactPostcode',
    'value' => $cppostcode,
    'id'  => 'postcode',
    'type'  => 'text',
    'class' => 'small short_field',
    'required'  => '',
    'autocomplete' => 'off',
    'data-parsley-error-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
);
/*
$contactPhone = array(
    'name'  => 'contactPhone',
    'value' => $cpphone,
    'id'  => 'phone',
    'type'  => 'text',
    'class' => 'phonesecinput codeFlag  width50per mobileValue',
    'required'  => '',
  );  
*/  

$contactPhone = array(
    'name' => 'contactPhone',
    'value' => $phone1_mobile,
    'id' => 'mobile',
    'type' => 'text',
    'class' => 'short_field small contact_input phoneValue',
    'placeholder' => '123456789',//lang('contact_us_phone_no'),
    'required' => '',
    'size' => '40',
    'data-parsley-required-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li ',
    'data-parsley-trigger' => 'keyup',
    
); 

$contactPhone2 = array(
    'name'  => 'contactPhone2',
    'value' => $phone2_mobile,
    'id'  => 'mobile2',
    'type'  => 'text',
    'class' => 'short_field small contact_input phoneValue',
    'placeholder' => '123456789',//lang('contact_us_phone_no'),
    'size' => '40',
 ); 
              
$contactFax = array(
    'name'  => 'contactFax',
    'value' => $cpfax,
    'id'  => 'fax',
    'type'  => 'text',
    'class' => 'short_field small contact_input phoneValue',
    // 'required' => '',
);              

$contactFax1 = array(
    'name'  => 'contactFax1',
    'value' => $cpfax1,
    'id'  => 'fax1',
    'type'  => 'text',
    'class' => 'phonesecinput  numValue small width_120',
    // 'required' => '',
);  

$contactMobile = array(
    'name'  => 'contactMobile',
    'value' => $cpmobile,
    'id'  => 'mobile',
    'type'  => 'text',
    'class' => 'mobileValue small short_field',
  //  'required'  => '',
);
  
$contactEmail = array(
    'name'  => 'contactEmail',
    'value' => $cpemail,
    'id'  => 'email',
    'type'  => 'email',
    'required'  => '',
    'class'=>'small',
    'autocomplete' => 'off',
    'data-parsley-error-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
 );
 
$confirmEmail = array(
    'name'  => 'confirmEmail',
    'id'  => 'confirmEmail',
    'value' => $cpemail,
    'required'  => '',
    'class'=>'small',
    'autocomplete' => 'off',
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-equalto' => '#email',
    'data-parsley-equalto-message' => lang('common_confirm_email_equal'),
    'data-parsley-trigger' => 'keyup',
 );

$contactWeb = array(
    'name'  => 'contactWeb',
    'value' => $cpweb,
    'id'  => 'website',
    'type'  => 'url',
    'class' => 'small',
    'data-parsley-type'=>'url',
    'data-parsley-error-message' => lang('valid_url_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
);  


$contactCountry = array(
  'name'  => 'contactCountry',
  'value' => $cpcountry,
  'id'  => 'country',
  'type'  => 'text',
  'class' => 'small',
  'required'  => '',
  'autocomplete' => 'off',
  'data-parsley-error-message' => lang('common_field_required'),
  'data-parsley-error-class' => 'custom_li',
  'data-parsley-trigger' => 'keyup',
);

$contactState = array(
  'name'  => 'contactState',
  'value' => $cpstate,
  'id'  => 'state',
  'type'  => 'text',
  'class' => 'small short_field',
  'required'  => '',
  'autocomplete' => 'off',
  'data-parsley-error-message' => lang('common_field_required'),
  'data-parsley-error-class' => 'custom_li',
  'data-parsley-trigger' => 'keyup',
);
/*
$contactCity = array(
  'name'  => 'contactCity',
  'value' => $cpcity,
  'id'  => 'city',
  'type'  => 'text',
  'class' => 'small'
);
*/ 
  
?>

<!-- commonform_bg -->

<div class="panel event_panel">
                    <div class="panel-heading">
                        <h4 class="panel-title medium dt-large ">
                        <a class=" " data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"><?php echo lang('event_contact_person'); ?></a>
                        </h4>
                    </div>
                    <div style="height: auto;" id="collapseTwo" class="accordion_content collapse apply_content <?php echo getIsCollapseOpen("collapseTwo"); ?>">
                      <?php echo form_open($this->uri->uri_string(),$formGeneralEventSetup); ?>
                        <div class="panel-body ls_back dashboard_panel small" id="showhideformdivpersonalContact">
          
                            <div class="row-fluid-15">
                                <label for="contact person"><?php echo lang('event_contact_event_person'); ?>
                                  <span class="info_btn"><span class="field_info xsmall"><?php echo lang('tooltip_contact_event_person'); ?></span></span>
                                </label>
                                <div class="radio_wrapper">
                                  <?php echo form_radio($eventUserDetails); ?>
                                  <label for="eventUserDetails">Operator</label>
                  
                  <?php echo form_radio($eventClientDetails);  ?>
                                  <label for="eventClientDetails">Client</label>
                                </div>
                            </div>
                            <!--end of row-fluid-->
          
                            <div class="row-fluid-15">
                                <label for="title"><?php echo lang('event_contact_title'); ?></label>
                                <?php 
                  $other = 'id="title" class="small custom-select"';
                  $titleArray[''] = 'Select Title';
                  $titleArr = $this->config->item('title_array');
                  foreach($titleArr as $key=>$value){
                    $titleArray[$key] = $value;
                  } 
                  
                  echo form_dropdown('title',$titleArray,$cptitle,$other);
                  echo form_error('title');
                ?>  
                            </div>
                            <!--end of row-fluid-->
          
                            <div class="row-fluid-15">
                                <label for="first_name"><?php echo lang('event_contact_first_last'); ?> <span class="astrik">*</span>
                                </label>
                                <?php echo form_input($firstName); ?>
                <?php echo form_error('firstName'); ?>
                            </div>
                            <!--end of row-fluid-->
          
                            <div class="row-fluid-15">
                                <label for="last_name"><?php echo lang('event_contact_last_name'); ?> <span class="astrik">*</span>
                                </label>
                                <?php echo form_input($lastName); ?>
                <?php echo form_error('lastName'); ?>
                            </div>
                            <!--end of row-fluid-->
          
                            <div class="row-fluid-15">
                                <label for="org"><?php echo lang('event_contact_organisation'); ?> <span class="astrik">*</span></label>
                               <?php echo form_input($contactOrganisation); ?>
                <?php echo form_error('contactOrganisation'); ?>
                            </div>
                            <!--end of row-fluid-->
          
                            <div class="row-fluid-15">
                                <label for="position"><?php echo lang('event_general_event_posiztion'); ?></label>
                                <?php echo form_input($contactPosition); ?>
                <?php echo form_error('contactPosition'); ?>
                            </div>
                            <!--end of row-fluid-->
          
                            <div class="row-fluid-15">
                                <label for="address"><?php echo lang('event_contact_addess'); ?> <span class="astrik">*</span></label>
                                <?php echo form_input($contactAddress1); ?>
                                <?php echo form_error('contactAddress1'); ?>
                            </div>
                            <!--end of row-fluid-->
          
                            <div class="row-fluid-15">
                                <label for="address"></label>
                                <?php echo form_input($contactAddress2); ?>
                            </div>
                            <!--end of row-fluid-->
          
                            <div class="row-fluid-15">
                                <label for="city"><?php echo lang('event_contact_city'); ?> <span class="astrik">*</span></label>
                                <?php 
                                  //$other = 'required="" id="contactCity"';
                                  //echo form_dropdown('contactCity',$masterCityDataList,$cpcity,$other);
                                  echo form_input($contactCity);
                                  echo form_error('contactCity');
                                ?>                 
                            </div>
                            <!--end of row-fluid-->
          
                            <div class="row-fluid-15">
                                <label for="state"><?php echo lang('event_contact_state'); ?> <span class="astrik">*</span></label>
                                <?php 
                  //$other = 'required="" id="contactState"';
                  //echo form_dropdown('contactState',$masterSateDataList,$cpstate,$other);
                  echo form_input($contactState);
                  echo form_error('contactState');
                ?>
                            </div>
                            <!--end of row-fluid-->
          
                            <div class="row-fluid-15">
                                <label for="pcode"><?php echo lang('event_contact_postcode'); ?> <span class="astrik">*</span></label>
                                <?php echo form_input($contactPostcode); ?>
                <?php echo form_error('contactPostcode'); ?>
                            </div>
                            <!--end of row-fluid-->
          
                            <div class="row-fluid-15">
                                <label for="country"><?php echo lang('event_contact_country'); ?> <span class="astrik">*</span></label>
                <?php 
                  //$other = 'required="" id="contactCountry"';
                  //echo form_dropdown('contactCountry',$masterCountryDataList,$cpcountry,$other);
                  echo form_input($contactCountry);
                  echo form_error('contactCountry');
                ?>
                            </div>
                            <!--end of row-fluid-->
          
                            <div class="row-fluid-15">
                                <label for="phone1"><?php echo lang('event_contact_phone'); ?> <span class="astrik">*</span></label>
                                <input type='hidden' name='input_hidden_mobile1' id='input_hidden_mobile1' value="<?php echo $phone1_mobile; ?>" >
                <input type='hidden' name='input_hidden_landline1' id='input_hidden_landline1' value="<?php echo $phone1_landline; ?>">
                 <div class="phone_input ">
                  <?php echo form_input($contactPhone); ?>
                  <?php echo form_error('contactPhone'); ?>
                  <a href="#" class="phone_field_selector"></a>
                  <ul class="phone_field_opt change_input_class" id='change_input'>
                    <li data-select="mobile1" id='mobile1' class="selected">Mobile</li>
                    <li data-select="landline1" class="not-selected"  id='landline1' >Landline</li>
                  </ul>
                 </div>
                               
                            </div>
                            <!--end of row-fluid-->
          
                            <div class="row-fluid-15">
                                <label for="phone2"><?php echo lang('event_contact_phone2'); ?></label>
                                <input type='hidden' name='input_hidden_mobile2' id='input_hidden_mobile2' value="<?php echo $phone2_mobile; ?>" >
                <input type='hidden' name='input_hidden_landline2' id='input_hidden_landline2' value="<?php echo $phone2_landline; ?>">
                <div class="phone_input ">   
                  <?php echo form_input($contactPhone2); ?>
                  <?php echo form_error('contactPhone2'); ?>
                  <a href="#" class="phone_field_selector"></a>
                  <ul class="phone_field_opt change_input_class phone_field_opt_1" id='change_input2'>
                    <li data-select="mobile2" id='mobile2' class="selected">Mobile</li>
                    <li data-select="landline2" class="not-selected"  id='landline2' >Landline</li>
                  </ul>
                </div>
                            </div>
                            <!--end of row-fluid-->
          
                            <div class="row-fluid-15">
                                <label for="fax"><?php echo lang('event_contact_fax'); ?></label>
                                <?php  echo form_input($contactFax); ?>
                                <?php  //echo form_input($contactFax1); ?>
                                <?php  echo form_error('contactFax'); ?>
                               <!-- <input type="text" id="fax" name="fax" value=""  class="small width_55 "/>
                                <input type="text" id="fax" name="fax" value=""  class="small width_120 "/>
                               --> 
                            </div>
                            <!--end of row-fluid-->
                            
                            <div class="row-fluid-15">
                                <label for="mobile"><?php echo lang('event_contact_mobile'); ?></label>
                                <?php echo form_input($contactMobile); ?>
                            <?php echo form_error('contactMobile'); ?>
                            </div>                            
                            <!--end of row-fluid-->
          
                            <div class="row-fluid-15">
                                <label for="email"><?php echo lang('event_contact_email'); ?> <span class="astrik">*</span>
                                </label>
                                <?php echo form_input($contactEmail); ?>
                                <?php echo form_error('contactEmail'); ?>
                            </div>
                            <!--end of row-fluid-->
          
                            <div class="row-fluid-15">
                                <label for="email"><?php echo lang('event_confirm_email'); ?> <span class="astrik">*</span>
                                </label>
                                <?php echo form_input($confirmEmail); ?>
                                <?php echo form_error('confirmEmail'); ?>
                            </div>
                            <!--end of row-fluid-->
          
                            <div class="row-fluid-15">
                                <label for="web"><?php echo lang('event_contact_web'); ?></label>
                                <?php echo form_input($contactWeb); ?>
                                <?php echo form_error('contactWeb'); ?>
                            </div>
                            <!--end of row-fluid-->
                            
                       
                            
                            
                            
                            <!-- Add custom contact type -->
                            <input type="hidden" value="<?php echo $contactPersionId; ?>" id="eventContactPersionId" />
                            <div class="input_fields_wrap" id="eventcustomcontactlistId">
                            <?php echo $this->load->view('custom_contact_list',array('contactPersionId'=>$contactPersionId)); ?>
                            </div>
                            <input type="hidden" id="hidden_input_no" value="0" /> 
                            <div id="hidden_custom_contact_array"></div>
                            
                            <!--end of row-fluid-->
                            
                            <div class="row-fluid-15">
                                <a href="javascript:void(0)" class="add_btn medium add_contact_type"><?php echo lang('add_custom_contact_type'); ?></a>
                            </div>
                            <!--end of row-fluid-->
          
                            <div class="btn_wrapper ">
                              <a href="#collapseTwo" class="pull-left scroll_top"><?php echo lang('comm_top'); ?></a>

                              <?php 
                                  echo form_hidden('eventId', $eventId);
                                  echo form_hidden('formActionName', 'contactPerson');
                                  
                                  //button show of save and reset
                                  $formButton['showbutton']   = array('save','reset');
                                  $formButton['labletext']  = array('save'=>lang('comm_save_changes'),'reset'=>lang('comm_reset'));
                                  $formButton['buttonclass']  = array('save'=>'default_btn btn pull-right medium','reset'=>'default_btn btn pull-right medium reset_form');
                                  $this->load->view('common_save_reset_button',$formButton);
                              ?>
                              
                            </div>
                        </div>
                        <?php echo form_close();?>
                    </div>
                </div>
 <!--end of panel-->

<script>
  
$(document).ready(function(){
      
  var max_fields      = 10; //maximum input boxes allowed
    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
    var add_button      = $(".add_field_button"); //Add button ID
    var x = 1; //initlal text box count
    
     $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').remove(); x--;
        $("#hidden_input_no").val((($("#hidden_input_no").val()*1)-1));
    })
   
   
});

var IsEditCustomObj = null;
//edit custom contact
$(document).on("click", ".add_contact_type", function(e){
    formPostData = {contactType:'',contactDetails:''};
    //call ajax popup function
    ajaxpopupopen('custom_type','event/eventcustomcontactpopup',formPostData,'add_contact_type');
    
});

//edit custom contact
$(document).on("click", ".edit_contact_type", function(e){
    IsEditCustomObj = $(this).siblings();   
    var contactType   = $(this).attr('data-contactType') // undefined
    var contactDetails  = $(this).attr('data-contactDetails');
    contactType   = (contactType=='undefined') ? '' : contactType;
    contactDetails  = (contactDetails=='undefined') ? '' : contactDetails;
    formPostData = {contactType:contactType,contactDetails:contactDetails};
    //call ajax popup function
    ajaxpopupopen('custom_type','event/eventcustomcontactpopup',formPostData,'add_contact_type');
    
});

//edit custom contact
$(document).on("click", ".editeventcustomcontact", function(e){
    var contactId = $(this).attr('data-contact');
    formPostData = {};
    eventcustompopup('event/eventcustomcontactpopup/'+contactId,'custom_type',formPostData);
  
    $("#hidden_custom_contact_array").append('<input type="hidden" name="hidden_deleted_custom_contact[]" value="'+contactId+'" />');
    $(this).parent().parent('div').remove();
});

// delete custom contact
$(document).on("click", ".deleteeventcustomcontact", function(e){
  // For set collapse hight
  $(".accordion_content").css("height",'auto');
  var contactId = $(this).attr('data-contact');
  //$("#hidden_custom_contact_array").append('<input type="hidden" name="hidden_deleted_custom_contact[]" value="'+contactId+'" />');
  $(this).parent().parent('div').remove();
});


$(document).on("click", "#add_custom_contact_button", function(e){
  
  // For set collapse hight
  $(".accordion_content").css("height",'auto');
  
  //Validate form
  if($("#customContactType").parsley().isValid()){
    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
    var hidden_input_no = $("#hidden_input_no").val();  
    e.preventDefault();
    var contactType =    $.trim($("#ctype").val());
    var contactDetails = $("#ctype_details").val();
    var contactType = contactType.charAt(0).toUpperCase() + contactType.substring(1);
    
    if(IsEditCustomObj!=null){
      $(IsEditCustomObj).parent().siblings('.short_field').val(contactDetails);
      $(IsEditCustomObj).parent().parent().find('label').html(contactType+'<input type="hidden" name="contactType[]" value="'+contactType+'">');
      IsEditCustomObj = null;
    }else{
      $(wrapper).append('<div class="row-fluid"><label for="contactType">'+contactType+'<input type="hidden" value="'+contactType+'" name="contactType[]"  /></label><input  class="small small short_field" type="text" required="" value="'+contactDetails+'" name="contactDetails[]" ><span class="category_btn pull-right"><a class="eventsetup_btn td_btn edit_contact_type" href="javascript:void(0)" data-contactType="'+contactType+'" data-contactDetails="'+contactDetails+'"  ><span> </span></a><a class="delete_btn td_btn deleteeventcustomcontact" href="javascript:void(0)"><span> </span></a></span></div></div>');
    }
    
    $('#custom_type').modal('hide');
    $("#hidden_input_no").val((($("#hidden_input_no").val()*1)+1));
    
      $("#customContactType")[0].reset();
       
  } 
  
});




</script>

<script>
  
  
  $( "#formContactPerson" ).submit(function( event ){   
  
    var selectedId  = $(this).find(".phone_field_opt").find(".selected").attr('id');
    $("#input_hidden_"+selectedId).val($("#mobile").val()); 
    
    var selectedId  = $(this).find(".phone_field_opt_1").find(".selected").attr('id');
    $("#input_hidden_"+selectedId).val($("#mobile2").val());  
    
    //save contact person details
    ajaxdatasave('formContactPerson','<?php echo $this->uri->uri_string(); ?>',true,true,false,false,false,'#showhideformdivpersonalContact','#showhideformdivSocialMedia');
    
  });
  
    
  $(".event_contact_person").click(function(){
    var contactPerson = parseInt($(this).val());
    
    if(contactPerson==1){
      var userdata = <?php echo json_encode($userdata); ?>;
      $.each(userdata, function(index, val) {
        $("#"+index).val(val);
      });
    }else{
      //reset form
      resetSelectedFrom('formContactPerson');
    } 
  });

 /*
  //set master state list data by state selection
  $('#contactCountry').change(function(){
     
    var countryId = $(this).val();
    sendData = {'countryId':countryId};
    postUrl = 'event/masterstatelist';
    doAjaxRequest(postUrl,sendData,'contactState');
    $('#contactState').html('<option valu="">Select State</option>');
    $('#contactCity').html('<option valu="">Select City</option>');
  }); 
  
  //set master city list data by state selection
  $('#contactState').change(function(){
    var stateId = $(this).val();
    sendData = {'stateId':stateId};
    postUrl = 'event/mastercitylist';
    doAjaxRequest(postUrl,sendData,'contactCity');
  }); 
  */
</script> 
