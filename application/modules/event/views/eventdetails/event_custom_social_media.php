<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$mediatypename  = (!empty($eventcustomSocialMedia->media_name)) ? $eventcustomSocialMedia->media_name:'';
$mediatypevalue = (!empty($eventcustomSocialMedia->media_value)) ? $eventcustomSocialMedia->media_value:'';

$formcustomSocialMediaType = array(
    'name'          => 'formcustomSocialMediaType',
    'id'            => 'formcustomSocialMediaType',
    'method'        => 'post',
    'class'         => 'wpcf7-form',
);

$customFieldId = array(
    'name'          => 'customFieldId',
    'value'         => $customfieldidval,
    'id'            => 'customFieldId',
    'type'          => 'hidden',
  );
  
$eventId = array(
    'name'          => 'eventId',
    'value'         => $eventId,
    'id'            => 'eventId',
    'type'          => 'hidden',
  );
  
$mediatypename = array(
    'name'          => 'mediatypename',
    'value'         => $mediatypename,
    'id'            => 'mediatypename',
    'type'          => 'text',
    'class'         =>'xxlarge_input trim_check keypressevent',
    'autocomplete'  => 'off',
);  

$mediatypevalue = array(
    'name'          => 'mediatypevalue',
    'value'         => $mediatypevalue,
    'id'            => 'mediatypevalue',
    'type'          => 'text',
    'class'         => 'xxlarge_input trim_check keypressevent',
    'autocomplete'  => 'off',
);
?>
<!-- Modal -->
<div id="custom_social_media" class="modal fade addField_popup in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
         <div class="modal-dialog">
            <div class="modal-content">
               <div class="modal-header">
        <h4 class="medium dt-large modal-title"><?php echo ($customfieldidval!=0) ? lang('event_edit_social_media') : lang('event_add_social_media'); ?></h4>
      </div>

      <div class="modal-body">
        <div class="">
         <?php echo form_open('',$formcustomSocialMediaType) ?>
            <div class="modal-body-content eventdashboard_popup small">
               
           <div class="row">
            <div class="labelDiv pull-left">
              <label class="form-label text-right" for="ctype"><?php echo lang('event_media_type_name'); ?><span aria-required="true" class="required">*</span></label>
              <span class="info_btn"><span class="field_info xsmall">The Access ID#.</span></span>
            </div>
            <?php echo form_input($mediatypename); ?>
            <?php echo form_error('ctype'); ?>
            <span id="error-mediatypename" class="error_"></span>
          </div>
           
           <div class="row">
            <div class="labelDiv pull-left">
              <label class="form-label text-right" for="ctype_details"><?php echo lang('event_media_type_value'); ?><span aria-required="true" class="required">*</span></label>
              <span class="info_btn"><span class="field_info xsmall">The Access ID#.</span></span>
            </div>
            <?php echo form_input($mediatypevalue); ?>
            <?php echo form_error('ctype_details'); ?>
            <span id="error-mediatypevalue" class="error_"></span>
          </div>    
        </div> 
            <div class="modal-footer">
              <div class="pull-right">
                    <?php
                        echo form_input($eventId); 
                        echo form_input($customFieldId); 
                        $extraCancel = 'class="popup_cancel submitbtn btn-normal btn" data-dismiss="modal" ';
                        $extraSave   = 'id="saveCustomeSocialMediaType" class="submitbtn btn-normal btn checkdata" ';
                        
                        echo form_submit('cancel',lang('comm_cancle'),$extraCancel);  
                        echo form_submit('save',lang('comm_save'),$extraSave);
                    ?> 
                  </div>
             </div>     
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

<script>

$('#mediatypename').keyup(function(){
    $("#error-mediatypename").html('');
    $("#mediatypename").removeClass('error');
});

$('#mediatypevalue').keyup(function(){
    $("#error-mediatypevalue").html('');
    $("#mediatypevalue").removeClass('error');
});   


$(document).ready(function(){
    $('.keypressevent').keypress(function(event){
        if (event.which == 13) { 
            event.preventDefault();
           $('#saveCustomeSocialMediaType').trigger('click');
        }
    });    
});   
</script>
