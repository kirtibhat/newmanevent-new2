<!-- fifth -->
<div class="panel event_panel">
  <div class="panel-heading ">
    <h4 class="panel-title medium dt-large ">
    <a class=" " data-toggle="collapse" data-parent="#accordion" href="#collapseFive"><?php echo lang('event_setup_catagories'); ?></a>
    </h4>
  </div>
  <div style="height: auto;" id="collapseFive" class="accordion_content collapse apply_content <?php echo getIsCollapseOpen("collapseFive"); ?>">
    <form>
    <div class="panel-body ls_back dashboard_panel small" id="showhideformdiveventcategory">
      
      <div class="input_fields_category">
        <?php $this->load->view('event_category_list'); ?>
      </div>
      <!--end of row-fluid-->
      
      <div class="row-fluid-15">
        <a class="add_btn medium" href="javascript:void(0)" id="event_cat"><?php echo lang('event_setup_add_category'); ?></a>
      </div> 
      <!--end of row-fluid-->

      <div class="btn_wrapper ">
        <a href="#collapseFive" class="pull-left scroll_top"><?php echo lang('comm_top'); ?></a>
        <a href="javascript:void(0)" id="defaultCategorySaveButton" class="default_btn btn pull-right medium"><?php echo lang('comm_save_changes'); ?></a>

      </div>
    </div>
    </form>
  </div>
 </div>
 <!--end of panel-->



<script>

//save contact person details
ajaxdatasave('eventCategory','<?php echo $this->uri->uri_string(); ?>',true,true,false,true,false,'#showhideformdiveventcategory','#showhideformdiveventcategory',false,true,'custom_cat');
    
  
$(document).ready(function(){
     
     $("#defaultCategorySaveButton").click(function(){
    $("#showhideformdiveventcategory").parent().parent().removeClass('in');
    $("#showhideformdivgroupbooking").parent().parent().addClass('in');
   });
     
   $("#event_cat").click(function(){
    formPostData = {};
    //call ajax popup function
    ajaxpopupopen('custom_cat','event/eventcategorypopup',formPostData,'event_cat');
  });
  
   var wrapper  = $(".input_fields_category"); //Fields wrapper
   var x = 1; //initlal text box count
    
   $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent().parent('div').remove(); x--;
    })
  
});


//edit custom contact
$(document).on("click", ".editeventcategory", function(e){
    var categoryId = $(this).attr('data-cat');
    formPostData = {categoryId:categoryId};
    //call ajax popup function
    ajaxpopupopen('custom_cat','event/eventcategorypopup',formPostData,'editeventcategory');
});

// delete custom contact
customconfirm('deleteeventcategory','event/deleteeventcategory', '', '', '', true,'',false,'<?php echo lang('event_category_deleted_succ') ?>');


</script>
