<?php
$formEventCustomContact = array(
'name'      => 'customContactType',
'id'        => 'customContactType',
'method'    => 'post',
'class'     => 'wpcf7-form',

);

$eventcustomtype = array(
'name'      => 'ctype',
'value'     => $contact_type,
'id'        => 'ctype',
'type'      => 'text',
'class'     => 'xxlarge_input trim_check keypressEvent1',
'required'  =>'',
'autocomplete' => 'off',

);  

$eventcustomdetails = array(
'name'      => 'ctype_details',
'value'     => $details,
'id'        => 'ctype_details',
'class'     => 'xxlarge_input trim_check keypressEvent2',
'rows'      => '2',
'cols'      => '5',
//'required'=>'',
'autocomplete' => 'off',

);  

?>

<!-- Modal -->
<div class="modal fade in" id="custom_type" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false"  >
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header delegates">
        <h4 class="medium dt-large modal-title"><?php echo ($contact_type!="") ? lang('edit_custom_contact_type') : lang('add_custom_contact_type'); ?></h4>
      </div>
		
      <div class="modal-body">
   
         <?php echo form_open('',$formEventCustomContact) ?>
            <div class="modal-body-content eventdashboard_popup small">

			  <div class="row">
				<div class="labelDiv pull-left">
				  <label class="form-label text-right" for="ctype"><?php echo lang('custom_contact_type'); ?><span aria-required="true" class="required">*</span></label>
				  <span class="info_btn"><span class="field_info xsmall">The Access ID#.</span></span>
				</div>
				  <?php echo form_input($eventcustomtype); ?>
				  <?php echo form_error('ctype'); ?>
				<span id="error-ctype"></span>
			  </div>
			  
              
			  <div class="row">
				<div class="labelDiv pull-left">
				  <label class="form-label text-right" for="ctype"><?php echo lang('custom_contact_details'); ?><span aria-required="true" class="required">*</span></label>
				  <span class="info_btn"><span class="field_info xsmall">The Access ID#.</span></span>
				</div>
				 <?php echo form_input($eventcustomdetails); ?>
                 <?php echo form_error('ctype_details'); ?>
				<span id="error-ctype_details"></span>
			  </div>
		   </div> 
			<div class="modal-footer">
			  <div class="pull-right">				
            <input type="submit" class="popup_cancel submitbtn btn-normal btn" value="Cancel" name="logincancel" data-dismiss="modal">
            <input type="submit" name="customType" value="Save" class="submitbtn btn-normal btn add_custom_contact" id="add_custom_contact_button">                 
			  </div>
			 </div>
            
            <?php 
                echo form_hidden('formActionName', 'eventCustomContactSave');
				echo form_close();
            ?>
        </div>
      </div>
    </div>
  </div>
</div>
<!--end of pop1-->
<script>
    
//$("#customContactType").parsley();
$('.keypressEvent1').keyup(function(){
    $("#error-ctype").html('');
    $("#ctype").removeClass('error');
});

$('.keypressEvent2').keyup(function(){
    $("#error-ctype_details").html('');
    $("#ctype_details").removeClass('error');
});   



$(document).ready(function(){
    $('#ctype').keypress(function(event){
        if (event.which == 13) { 
            event.preventDefault();
            $('.add_custom_contact').trigger('click');
        }
    });       
    
    $('#ctype_details').keypress(function(event){
        if (event.which == 13) { 
            event.preventDefault();
           $('.add_custom_contact').trigger('click');
        }
    });    
});         
</script>


