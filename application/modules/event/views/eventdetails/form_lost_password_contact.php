<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 


//set field value in variable
$lostContactNameValue   = (!empty($eventdetails->contact_name))?$eventdetails->contact_name:'';
$lostContactMobileValue  = (!empty($eventdetails->contact_mobile))?$eventdetails->contact_mobile:'';
$lostContactPhoneValue  = (!empty($eventdetails->contact_phone))?$eventdetails->contact_phone:'';
$lostContactEmailValue  = (!empty($eventdetails->contact_email))?$eventdetails->contact_email:'';


$formLostPasswordContact = array(

    'name'   => 'formLostPasswordContact',
    'id'   => 'formLostPasswordContact',
    'method' => 'post',
    'class'  => '',
    'data-parsley-validate' => ''

);

$lostContactName = array(

    'name'  => 'lostContactName',
    'value' => $lostContactNameValue,
    'id'  => 'lostContactName',
    'type'  => 'text',
    'required'  => '',
    'class'   => 'small short_field',
    'autocomplete' => 'off',
    'data-parsley-error-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
);  

$lostContactPhone = array(

    'name'  => 'lostContactPhone',
    'value' => $lostContactMobileValue,
    'id'  => 'lostContactPhone',
    'placeholder' => '123456789',
    'type'  => 'text',
    'required'  => '',
    'class'   => 'small short_field contact_input phoneValue',
    'autocomplete' => 'off',
    'data-parsley-error-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
);

$lostContactEmail = array(
    'name'  => 'lostContactEmail',
    'value' => $lostContactEmailValue,
    'id'  => 'lostContactEmail',
    'type'  => 'email',
    'required'  => '',
    'class' =>'small dark',
    'autocomplete' => 'off',
    'data-parsley-error-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
);

$lostConfirmEmail = array(
    'name'  => 'lostConfirmEmail',
    'id'  => 'lostConfirmEmail',
    'value' => $lostContactEmailValue,    
    'required'  => '',
    'class'=>'small',
    'autocomplete' => 'off',    
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-equalto' => '#lostContactEmail',
    'data-parsley-equalto-message' => lang('common_confirm_email_equal'),
    'data-parsley-trigger' => 'keyup',
 );
?>  

<div class="panel event_panel">
  <div class="panel-heading ">
    <h4 class="panel-title medium dt-large ">
    <a class=" " data-toggle="collapse" data-parent="#accordion" href="#collapseThree"><?php echo lang('event_lost_password_contact'); ?></a>
    </h4>
  </div>
  <div style="height: auto;" id="collapseThree" class="accordion_content collapse apply_content">
    <?php echo form_open($this->uri->uri_string(),$formLostPasswordContact); ?>
    <div class="panel-body ls_back dashboard_panel small" id="showhideformdivlostPasswordSection">
      <div class="row-fluid-15">
        <label for="contact_name"><?php echo lang('event_lost_password_conact_name'); ?> <span class="astrik">*</span>
          <span class="info_btn"><span class="field_info xsmall"><?php echo lang('tooltip_lost_password_contact'); ?></span></span>
        </label>
        <?php echo form_input($lostContactName); ?>
        <?php echo form_error('lostContactName'); ?>
      </div>
      <!--end of row-fluid-->

      <div class="row-fluid-15" >
      <label for="phone_1"><?php echo lang('event_lost_password_phone'); ?> <span class="astrik">*</span></label>
       <div class="phone_input ">
        <input type='hidden' name='input_hidden_mobile3' id='input_hidden_mobile3' value="<?php echo $lostContactMobileValue; ?>" >
        <input type='hidden' name='input_hidden_landline3' id='input_hidden_landline3' value="<?php echo $lostContactPhoneValue; ?>">
        <?php echo form_input($lostContactPhone); ?>
        <?php echo form_error('lostContactPhone'); ?>
            <a href="#" class="phone_field_selector"></a>
          <ul class="phone_field_opt change_input_class" id='change_input3'>
            <li data-select="mobile3" id='mobile3' class="selected">Mobile</li>
            <li data-select="landline3" class="not-selected"  id='landline3' >Landline</li>
          </ul>
       </div>   
      </div>
      <!--end of row-fluid-->

      <div class="row-fluid-15">
        <label for="email"><?php echo lang('event_lost_password_email'); ?> <span class="astrik">*</span></label>
         <?php echo form_input($lostContactEmail); ?>
        <?php echo form_error('lostContactEmail'); ?>
      </div>
      <!--end of row-fluid-->
      
      <div class="row-fluid-15">
        <label for="email"><?php echo lang('event_lost_password_confirm_email'); ?> <span class="astrik">*</span></label>
         <?php echo form_input($lostConfirmEmail); ?>
        <?php echo form_error('lostConfirmEmail'); ?>
      </div>
      <!--end of row-fluid-->

      <div class="btn_wrapper ">
        <a href="#collapseThree" class="pull-left scroll_top"><?php echo lang('comm_top'); ?></a>

        <?php 
          echo form_hidden('eventId', $eventId);
          echo form_hidden('formActionName', 'eventLostPassword');
          
          //button show of save and reset
          $formButton['showbutton']   = array('save','reset');
          $formButton['labletext']  = array('save'=>lang('comm_save_changes'),'reset'=>lang('comm_reset'));
          $formButton['buttonclass']  = array('save'=>'default_btn btn pull-right medium','reset'=>'default_btn btn pull-right medium reset_form');
          $this->load->view('common_save_reset_button',$formButton);
        ?>
        
      </div>
    </div>
     <?php echo form_close();?>
  </div>
</div>


<!-- /commonform_bg -->

<script>
  
  
  $( "#formLostPasswordContact" ).submit(function( event ){
    
    var selectedId  = $(this).find(".phone_field_opt").find(".selected").attr('id');
    $("#input_hidden_"+selectedId).val($("#lostContactPhone").val()); 
    //save event bank details
    ajaxdatasave('formLostPasswordContact','<?php echo $this->uri->uri_string(); ?>',true,true,false,false,false,'#showhideformdivlostPasswordSection','#showhideformdiveventInvoice');

  }); 
    
  
</script> 
