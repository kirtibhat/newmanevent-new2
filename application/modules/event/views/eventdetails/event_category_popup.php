<?php
//Default Benefit 
$formCorporateBenefit = array(
		'name'	 => 'eventCategory',
		'id'	 => 'eventCategory',
		'method' => 'post',
		'class'  => 'wpcf7-form',
		'data-parsley-validate'=>'',
);
 
$cname = array(
		'name'	=> 'cname',
		'value'	=> $category,
		'id'	=> 'cname',
		'type'	=> 'text',
		'class'	=> 'small',
		'required'=>'',
		'autocomplete' => 'off',
		'data-parsley-error-message' => lang('common_field_required'),
		'data-parsley-error-class' => 'custom_li',
		'data-parsley-trigger' => 'keyup'
);	

?>
<div id="custom_cat" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" >
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header delegates">
        <h4 class="medium dt-large modal-title"><?php echo ($catId=="") ? lang('event_add_category') : lang('event_edit_category'); ?></h4>
      </div>

      <div class="modal-body small">
        <div class="modelinner ">
          <?php echo form_open($this->uri->uri_string(),$formCorporateBenefit); ?>
            <input type="hidden" value="collapseFive" name="collapseValue">
            <div class="control-group mb10 eventdashboard_popup small">
              <div class="row-fluid">
              	<label class="pull-left" for="cname"><?php echo lang('event_add_category_name'); ?>  <span class="astrik">*</span></label>
                <?php echo form_input($cname); ?>
              </div>

              <div class="row-fluid">
              	<label class="pull-left" for="ctype_details"><?php echo lang('event_add_corporate_package'); ?><span class="astrik">*</span><span class="info_btn"><span class="field_info xsmall">This date is the  last day you will accept earlybird registrations</span></span>
                </label>
                <div class="radio_wrapper">
                    <input type="radio" value="1" <?php echo ($is_corporate=='1') ? 'checked="checked"' : "";  ?>  id="c_package_yes" name="c_package" required  data-parsley-error-message = "<?php echo lang('common_field_required'); ?>" data-parsley-error-class="custom_li">
                    <label for="c_package_yes">Yes</label>
                
                    <input type="radio" value="0" <?php echo ($is_corporate=='0') ? 'checked="checked"' : "";  ?> id="c_package_no" name="c_package" required data-parsley-error-message = "<?php echo lang('common_field_required'); ?>" data-parsley-error-class="custom_li" >
                    <label for="c_package_no">No</label>
                </div>
              </div>

              <div class="btn_wrapper">
				  <input type="hidden" value="<?php echo $catId ?>" name="catId" id="catId" />
				  <?php echo form_hidden('formActionName', 'eventCategory'); ?>
                  <input type="submit" name="eventsubmit" value="Save" class=" submitbtn pull-right medium" id="add_event_category_button" />
                  <input type="submit" class="popup_cancel submitbtn pull-right medium" value="Cancel" name="logincancel" data-dismiss="modal">
              </div>

            </div>
          <?php echo form_close(); ?>
        </div>
      </div>
    </div>
  </div>
</div>

<script >
	$("#eventCategory").parsley();
</script>

<!--end of pop1-->
