<?php if (!defined('BASEPATH'))  exit('No direct script access allowed');
/*Get url param for required check for confirm details page*/
$getParam       =  $this->input->get('req');
$requiredCheck  =  (!empty($getParam)) ? $getParam : '';
$openTab        =  (empty($requiredCheck) || ($requiredCheck != 4))  ? 'open' : '';
/*set field value in variable*/
$unic_event_url 	    = (!empty($eventdetails->event_url)) ? $eventdetails->event_url : '';

$geseventtitle 	        = (!empty($eventdetails->event_title)) ? $eventdetails->event_title : '';
$geseventsubtitle       = (!empty($eventdetails->subtitle)) ? $eventdetails->subtitle : '';
$geseventdescription    = (!empty($eventdetails->description)) ? $eventdetails->description : '';
$geseventlocation       = (!empty($eventdetails->event_location)) ? $eventdetails->event_location : '';
$geseventvenue          = (!empty($eventdetails->event_venue)) ? $eventdetails->event_venue : '';
/*new added venue fields*/
$geseventvenueAddress1  = (!empty($eventdetails->eventVenueAddress1)) ? $eventdetails->eventVenueAddress1 : '';
$geseventvenueAddress2  = (!empty($eventdetails->eventVenueAddress2)) ? $eventdetails->eventVenueAddress2 : '';
$geseventvenueCity      = (!empty($eventdetails->eventVenueCity)) ? $eventdetails->eventVenueCity : '';
$geseventvenueState     = (!empty($eventdetails->eventVenueState)) ? $eventdetails->eventVenueState : '';
$geseventvenueZip       = (!empty($eventdetails->eventVenueZip)) ? $eventdetails->eventVenueZip : '';
$geseventspeaker        = (!empty($eventdetails->event_speaker)) ? $eventdetails->event_speaker : '';
$geseventlimit          = (!empty($eventdetails->event_limit)) ? $eventdetails->event_limit : '';
$geseventnumberrego     = (!empty($eventdetails->number_of_rego)) ? $eventdetails->number_of_rego : '';
$sittingStyleValue      = (!empty($eventdetails->sitting_style)) ? $eventdetails->sitting_style : '';
$timeZoneValue          = (!empty($eventdetails->time_zone)) ? $eventdetails->time_zone : '';
$destinationValue       = (!empty($eventdetails->destination)) ? $eventdetails->destination : '';
$lastRegDateValue       = (!empty($eventdetails->last_reg_date)) ? $eventdetails->last_reg_date : '';
$eventNumberValue       = (!empty($eventdetails->event_number)) ? $eventdetails->event_number : '';
$eventShrtFrmTiteValue  = (!empty($eventdetails->short_form_title)) ? $eventdetails->short_form_title : getAcronyms($geseventtitle);
$eventRefNumberValue    = (!empty($eventdetails->event_reference_number)) ? $eventdetails->event_reference_number : '';
$show_map               = (!empty($eventdetails->show_map)) ? $eventdetails->show_map : '';
$event_category_val          = (!empty($eventdetails->event_category)) ? $eventdetails->event_category : '';
/*get event number */
$eventNumberValueArray 	= str_split($eventNumberValue);
$geseventlastregdate 	= '';
if (!empty($eventdetails->last_reg_date)) {
    if ($eventdetails->last_reg_date == "0000-00-00 00:00:00") {
        $geseventlastregdate = '';
    } else {
        $geseventlastregdate = dateFormate($eventdetails->last_reg_date, "d M Y h:i:s");
    }
}
/*event early bird date*/
$geseventearlybirdregdate = '';
if (!empty($eventdetails->early_bird_reg_date)) {
    if ($eventdetails->early_bird_reg_date == "") {
        $geseventearlybirdregdate = '';
    } else {
        $geseventearlybirdregdate = dateFormate($eventdetails->early_bird_reg_date, "d M Y");
    }
}
$geseventstarttime 	= (!empty($eventdetails->starttime)) ? dateFormate($eventdetails->starttime, "d M Y h:i:s") : '';
$geseventendtime 	= (!empty($eventdetails->endtime)) ? dateFormate($eventdetails->endtime, "d M Y h:i:s") : '';
$geseventeventid 	= (!empty($eventdetails->event_id)) ? $eventdetails->event_id : '';
$registerStartDate 	= (!empty($eventdetails->reg_startdate)) ? dateFormate($eventdetails->reg_startdate, "d M Y h:i:s") : '';
$earlybirdCloseDate = (!empty($eventdetails->reg_closedate)) ? dateFormate($eventdetails->reg_closedate, "d M Y") : '';
$timeZoneOptions = array('' => 'Select Time Zone', 'AWST' => 'Australian Western Standard Time', 'ACST' => 'Australian Central Standard Time', 'AEST' => 'Australian Eastern Standard Time');
$event_is_public 	= (!empty($eventdetails->event_is_public)) ? $eventdetails->event_is_public : 0;
$eventPublicYesChk  = true;
$eventPublicNoChk 	= false;
/******************* Form Fields Setup ********************************/
if(!empty($event_is_public)){
  if($event_is_public==1){
    $eventPublicYesChk  = true;
    $eventPublicNoChk 	= false;
  }else{
    $eventPublicYesChk  = false;
    $eventPublicNoChk 	= true;
  }
}else {
    $eventPublicYesChk  = false;
    $eventPublicNoChk 	= true;
}
$formGeneralEventSetup 	= array(
    'name' 	        => 'formGeneralEventSetup',
    'id'            => 'formGeneralEventSetup',
    'method'        => 'post',
);

$eventName = array(
    'name'          => 'eventName',
    'value'         => $geseventtitle,
    'id'            => 'eventName',
    'type'          => 'text',
    'placeholder'   => 'Event Name',
    'class'         => 'xlarge_input',
    'autocomplete'  => 'off',
);
$eventSubtitle = array(
    'name'          => 'eventSubtitle',
    'value'         => $geseventsubtitle,
    'id'            => 'eventSubtitle',
    'type'          => 'text',
    'placeholder'   => 'Event Subtitle',
    'class'         => 'xlarge_input',
    'autocomplete'  => 'off',
);

$eventUrl = array(
    'name'          => 'eventUrl',
    'value'         => $unic_event_url,
    'id'            => 'eventUrl',
    'type'          => 'text',
    'placeholder'   => 'Event URL',
    'class'         => 'medium_input mLm5',
    'autocomplete'  => 'off',
);

$eventShortFormTitle = array(
        'name'      => 'eventShortFormTitle',
        'value'     => $eventShrtFrmTiteValue,
        'id'        => 'eventShortFormTitle',
        'type'      => 'text',
        'class'     => 'medium_input'
);

$eventReferenceNumber = array(
        'name'      => 'eventReferenceNumber',
        'value'     => $eventRefNumberValue,
        'id'        => 'eventReferenceNumber',
        'type'      => 'text',
        'class'     => 'xlarge_input'
);

$eventStartDate = array(
    'name'          => 'eventStartDate',
    'value'         => $geseventstarttime,
    'id'            => 'eventStartDate',
    'type'          => 'text',
    'placeholder'   => 'Event Start Date',
    'class'         => 'date_input s_datepicker medium_input',
    'data-format'   => 'yyyy-MM-dd hh:mm:ss',
    'autocomplete'  => 'off',
);
$eventEndDate = array(
    'name'          => 'eventEndDate',
    'value'         => $geseventendtime,
    'id'            => 'eventEndDate',
    'type'          => 'text',
    'placeholder'   => 'Event End Date',
    'class'         => 'date_input e_datepicker medium_input',
    'data-format'   => 'yyyy-MM-dd hh:mm:ss',
    'autocomplete'  => 'off',
);

$eventDescription = array(
    'name'          => 'eventDescription',
    'value'         => $geseventdescription,
    'id'            => 'eventDescription',
    'rows'          => '3',
    'class'         => 'form-textarea'
);
$eventMaxRegistrants = array(
    'name'          => 'eventMaxRegistrants',
    'value'         => $geseventnumberrego,
    'id'            => 'eventMaxRegistrants',
    'type'          => 'text',
    'class'         => 'medium_input',
    'autocomplete'  => 'off',
);

$registerLastDate = array(
    'name'          => 'registerLastDate',
    'value'         => $geseventlastregdate,
    'id'            => 'registerLastDate',
    'type'          => 'text',
    'class'         => 'date_input medium_input datepicker clearVl',
    'data-format'   => 'yyyy-MM-dd hh:mm:ss',
    'autocomplete'  => 'off',
);
$registerStartDate = array(
    'name'          => 'registerStartDate',
    'value'         => $registerStartDate,
    'id'            => 'registerStartDate',
    'type'          => 'text',
    'class'         => 'date_input datepicker medium_input',
    'data-format'   => 'yyyy-MM-dd hh:mm:ss',
    'autocomplete'  => 'off',
);
$eventDestination = array(
    'name'          => 'eventDestination',
    'value'         => $destinationValue,
    'id'            => 'eventDestination',
    'type'          => 'text',
    'class'         => 'xlarge_input',
);
$eventLocation = array(
    'name'          => 'eventLocation',
    'value'         => $geseventlocation,
    'id'            => 'eventLocation',
    'type'          => 'text',
    'class'         => 'small',
);
$eventVenue = array(
    'name'          => 'eventVenue',
    'value'         => $geseventvenue,
    'id'            => 'eventVenue',
    'type'          => 'text',
    'class'         => 'small mapvenue',
    'autocomplete'  => 'off',
);

/*
 * new fields 
 * for venue address
 */
 $eventVenueAddress1 = array(
    'name'          => 'eventVenueAddress1',
    'value'         => $geseventvenueAddress1,
    'id'            => 'eventVenueAddress1',
    'type'          => 'text',
    'class'         => 'small',
    'autocomplete'  => 'off',
);
 
 $eventVenueAddress2 = array(
    'name'          => 'eventVenueAddress2',
    'value'         => $geseventvenueAddress2,
    'id'            => 'eventVenueAddress2',
    'type'          => 'text',
    'class'         => 'small',
    'autocomplete'  => 'off',
);

 $eventVenueCity = array(
    'name'          => 'eventVenueCity',
    'value'         => $geseventvenueCity,
    'id'            => 'eventVenueCity',
    'type'          => 'text',
    'class'         => 'small',
    'autocomplete'  => 'off',
);

 $eventVenueState = array(
    'name'          => 'eventVenueState',
    'value'         => $geseventvenueState,
    'id'            => 'eventVenueState',
    'type'          => 'text',
    'class'         => 'small width_130',
    'autocomplete'  => 'off',
);

 $eventVenueZip = array(
    'name'          => 'eventVenueZip',
    'value'         => $geseventvenueZip,
    'id'            => 'eventVenueZip',
    'type'          => 'text',
    'class'         => 'small width_130  pull-right-imp',
    'autocomplete'  => 'off',
);

$eventSponsorsLogos = array(
    'name'          => 'eventSponsorsLogos',
    'value'         => '1',
    'id'            => 'eventSponsorsLogos',
    'type'          => 'checkbox',
);

/* add public yes/no field */
$eventPublicYes = array(
    'name'          => 'eventIsPublic',
    'value'         => '1',
    'id'            => 'eventPublicYes',
    'type'          => 'radio',
    'class'         => 'radioButton',
    'checked'       => $eventPublicYesChk,
);  

$eventPublicNo = array(
    'name'          => 'eventIsPublic',
    'value'         => '0',
    'id'            => 'eventPublicNo',
    'type'          => 'radio',
    'class'         => 'radioButton',
    'checked'       => $eventPublicNoChk,
); 

/* Add Invitation cut off yes/no field */
if(!empty($geseventlastregdate)){
    $checkedYes = 'checked';
    $checkedNo  = '';
    $style      = '';
}else{
    $style      = 'display:none;';
    $checkedYes = '';
    $checkedNo  = 'checked';
}

$registerLastDateYes = array(
    'name'      => 'isRegisterLastDateYes',
    'value'     => '1',
    'id'        => 'registerLastDateYes',
    'type'      => 'radio',
    'class'     => 'radioButton',
    'checked'   => $checkedYes,
);  

$registerLastDateNo = array(
    'name'      => 'isRegisterLastDateYes',
    'value'     => '0',
    'id'        => 'registerLastDateNo',
    'type'      => 'radio',
    'class'     => 'radioButton',
    'checked'   => $checkedNo,
); 

$logo_list_div = "dn"; 
if (!empty($eventsponsorslogos)) { $logo_list_div = ""; $eventSponsorsLogos['checked'] = true; }
?>
<div class="panel <?php if($openTab =='open') { echo $openTab; } ?>" id="general_event_setup_panel">
  <div class="panel_header"><?php echo lang('event_general_event_setup'); ?></div>
  <div class="panel_contentWrapper" id="showhideformdivgeneralSection" style="<?php if($openTab =='open') { echo 'display:block;'; } ?>">
    <div class="infobar">
        <p class="dn" id="general_event_setup_info"><?php echo lang('general_event_setup_info'); ?></p>
        <span class="info_btn" onclick="showHideInfoBar('general_event_setup_info');"></span>
    </div>
    <!--sub menu one-->
    <?php echo form_open($this->uri->uri_string(), $formGeneralEventSetup); ?>
        <input type="hidden" name="collapseValue" value="collapseTwo" />
          <div class="panel_content">
            <div class="row">
              <div class="col-3">
                <div class="labelDiv">
                  <label class="form-label text-right"><?php echo lang('event_event_name'); ?> <span class="required" aria-required="true">*</span> </label>
                </div>
              </div>
              <div class="col-5">
                <?php echo form_input($eventName); ?>
                <?php echo form_error('eventName'); ?>
                <span id="error-eventName"></span> </div>
            </div>
            <div class="row">
              <div class="col-3">
                <div class="labelDiv">
                  <label class="form-label text-right"><?php echo lang('event_event_subtitle'); ?><span class="required" aria-required="true"></span> </label><span class="info_btn"><span class="field_info xsmall"><?php echo lang('sub_title_info'); ?></span></span>
                </div>
              </div>
              <div class="col-5">
                <?php echo form_input($eventSubtitle); ?>
                <?php echo form_error('eventSubtitle'); ?>
                <span id="error-eventSubtitle"></span> </div>
            </div>
            <div class="row">
              <div class="col-3">
                <div class="labelDiv">
                  <label class="form-label text-right"><?php echo lang('event_event_short_form_title'); ?> <span class="required" aria-required="true">*</span></label><span class="info_btn"><span class="field_info xsmall"><?php echo lang('short_form_title_info'); ?></span></span>
                </div>
              </div>
              <div class="col-5">
                
                <label class="xsmall_input disabled_label"><?php
                    echo (isset($eventNumberValueArray[0])) ? $eventNumberValueArray[0] : "";
                    echo (isset($eventNumberValueArray[1])) ? $eventNumberValueArray[1] : "";
                    echo (isset($eventNumberValueArray[2])) ? $eventNumberValueArray[2] : "";
                    echo (isset($eventNumberValueArray[3])) ? $eventNumberValueArray[3] : "";
                    ?></label>    
                <input type="hidden" id="short_name_prefix" name="short_name_prefix" value="<?php
                    echo (isset($eventNumberValueArray[0])) ? $eventNumberValueArray[0] : "";
                    echo (isset($eventNumberValueArray[1])) ? $eventNumberValueArray[1] : "";
                    echo (isset($eventNumberValueArray[2])) ? $eventNumberValueArray[2] : "";
                    echo (isset($eventNumberValueArray[3])) ? $eventNumberValueArray[3] : "";
                    ?>" required class="xsmall_input" disabled/>
                  
                    <span>-</span>
                    <?php echo form_input($eventShortFormTitle); ?>
                    <?php echo form_error('eventShortFormTitle'); ?>
                <span id="error-eventShortFormTitle"></span> </div>
            </div>
            <div class="row">
              <div class="col-3">
                <div class="labelDiv">
                <label class="form-label text-right"><?php echo lang('event_reference_number'); ?> <span class="required" aria-required="true"></span></label>
                  <span class="info_btn"><span class="field_info xsmall">The Access ID#.</span></span>
                </div>
              </div>
              <div class="col-5">
                <?php echo form_input($eventReferenceNumber); ?>
                <?php echo form_error('eventReferenceNumber'); ?>
                <span id="error-dinner_dance6"></span> </div>
            </div>
            <div class="row">
              <div class="col-3">
                <div class="labelDiv">
                  <label class="form-label text-right"><?php echo lang('event_event_url'); ?> <span class="required" aria-required="true">*</span></label>
                </div>
              </div>
              <div class="col-5">
                 <label class="medium_input disabled_label"><?php echo lang('newman_events_url_text'); ?></label> 
                <input type="hidden" id="event_url_prefix" name="event_url_prefix" value="<?php echo lang('newman_events_url_text'); ?>" class="medium_input" disabled/>
                <?php echo form_input($eventUrl); ?>
                <?php echo form_error('eventUrl'); ?>
                <input type="hidden" name="eventUrlOld" value="<?php echo $unic_event_url; ?>">
                <span id="error-eventUrl"></span> </div>
            </div>
            <div class="row">
              <div class="col-3">
                <div class="labelDiv">
                  <label class="form-label text-right">Start<span class="required" aria-required="true">*</span></label>
                </div>
              </div>
              <div class="col-5">
                <?php echo form_input($eventStartDate); ?>
                <span id="error-eventStartDate"></span>
                <?php echo form_error('eventStartDate'); ?> 
                <span class="medium_icon"> <i class="icon-calender cal3"></i> </span>
                 </div>
            </div>
            <div class="row">
              <div class="col-3">
                <div class="labelDiv">
                  <label class="form-label text-right"><?php echo lang('event_event_end_data'); ?><span class="required" aria-required="true">*</span></label>
                </div>
              </div>
              <div class="col-5">
                <?php echo form_input($eventEndDate); ?>
                <span id="error-eventEndDate"></span>
                <?php echo form_error('eventEndDate'); ?>
                <span class="medium_icon"> <i class="icon-calender cal4"></i> </span>
                 </div>
            </div>
            <div class="row">
              <div class="col-3">
                <div class="labelDiv">
                  <label class="form-label text-right"><?php echo lang('event_time_zone'); ?><span class="required" aria-required="true">*</span></label>
                   <span class="info_btn"><span class="field_info xsmall"><?php echo lang('time_zone_info'); ?></span></span>
                </div>
              </div>
              <div class="col-5">
              <?php
                    $other = ' id="timeZone" class="custom-select xlarge_select" ';
                    echo form_dropdown('timeZone', $timeZoneOptions, $timeZoneValue, $other);
                    echo form_error('timeZone');
               ?>              
                <span id="error-timeZone"></span> </div>
            </div>
            
            <div class="row">
              <div class="col-3">
                <div class="labelDiv">
                  <label class="form-label text-right"><?php echo lang('event_description_details_event'); ?><span class="required" aria-required="true"></span></label>
                   <span class="info_btn"><span class="field_info xsmall"><?php echo lang('description_info'); ?></span></span>
                </div>
              </div>
              <div class="col-5">
                <?php echo form_textarea($eventDescription); ?>
                <?php echo form_error('eventDescription'); ?>           
                <span id="error-dinner_dance6"></span> </div>
            </div>
            <div class="row">
              <div class="col-3">
                <div class="labelDiv">
                  <label class="form-label text-right">Event Category<span class="required" aria-required="true"></span></label>
                   <span class="info_btn"><span class="field_info xsmall"><?php echo lang('event_category_info') ?></span></span>
                </div>
              </div>
              <div class="col-5">
              <?php
                    $event_categories = $this->config->item('event_categories');
                    $attr_event_category = ' id="event_category" class="custom-select xlarge_select"  ';
                    echo form_dropdown('event_category', $event_categories, $event_category_val, $attr_event_category);
                    echo form_error('event_category');
                ?>             
                <span id="error-dinner_dance6"></span> </div>
            </div>
            <div class="row">
              <div class="col-3">
                <div class="labelDiv">
                <label class="form-label text-right"><?php echo lang('event_total_invitation_limit'); ?><span class="required" aria-required="true">*</span></label>
                   <span class="info_btn"><span class="field_info xsmall"><?php echo lang('total_invitation_limit_info'); ?></span></span>
                </div>
              </div>
              <div class="col-5">
                <?php echo form_input($eventMaxRegistrants); ?>
                <?php echo form_error('eventMaxRegistrants'); ?>
                <span id="error-eventMaxRegistrants"></span> </div>
            </div>
            <div class="row">
              <div class="col-3">
                <div class="labelDiv">
                  <label class="form-label text-right"><?php echo lang('event_event_destination'); ?></label>
                  <span class="info_btn"><span class="field_info xsmall"><?php echo lang('destination_info'); ?></span></span>
                </div>
              </div>
              <div class="col-5">
                <?php echo form_input($eventDestination); ?>
                <?php echo form_error('eventDestination'); ?>
                <span id="error-dinner_dance6"></span> </div>
            </div>
            <div class="row">
              <div class="col-3">
                <div class="labelDiv">
                  <label class="form-label text-right"><?php echo lang('event_sitting_style'); ?></label>
                   <span class="info_btn"><span class="field_info xsmall"><?php echo lang('sitting_style_info'); ?></span></span>
                </div>
              </div>
              <div class="col-5">
              <?php
                $sittingStyleArray = $this->config->item('seating_style');
                $other = ' id="sittingStyle" class="custom-select xlarge_select"  ';
                echo form_dropdown('sittingStyle', $sittingStyleArray, $sittingStyleValue, $other);
                echo form_error('sittingStyle');
                ?>            
                <span id="error-dinner_dance6"></span> </div>
            </div>
            <div class="row">
              <div class="col-3">
                <div class="labelDiv">
                  <label class="form-label text-right"><?php echo lang('event_invitation_start_date'); ?> <span class="required" aria-required="true">*</span></label> <span class="info_btn"><span class="field_info xsmall"><?php echo lang('invitation_start_info'); ?></span></span>
                </div>
              </div>
              <div class="col-5">
				   
                <?php echo form_input($registerStartDate); ?>   
                <span id="error-registerStartDate"></span>                 
                <span class="medium_icon"> <i class="icon-calender cal5"></i> </span>
                </div>
            </div>
           
            <div class="row">
              <div class="col-3">
                <div class="labelDiv">
                  <label class="form-label text-right"><?php echo lang('event_last_date_invitation'); ?><span class="required" aria-required="true"></span></label> <span class="info_btn"><span class="field_info xsmall"><?php echo lang('invitation_cut_off_info'); ?></span></span>
                </div>
              </div>
              <div class="col-5">
                  
              <div class="radioDiv">
                  <?php echo form_radio($registerLastDateYes); ?>
                  <label class="form-label pull-left" for="registerLastDateYes"><?php echo lang('event_public_invitation_yes'); ?></label>
              </div>
              <div class="radioDiv">  
                <?php echo form_radio($registerLastDateNo);  ?>
                  <label class="form-label pull-left" for="registerLastDateNo"><?php echo lang('event_public_invitation_no'); ?></label>
               </div>
               <span id="error-dinner_dance6"></span> </div>
            </div>
            
            <div class="row reg_cutoff_invitation" style="<?php echo $style; ?>">
              <div class="col-3">
                <div class="labelDiv">
                  <label class="form-label text-right"><span class="required" aria-required="true"></span></label>
<!--
                   <span class="info_btn"><span class="field_info xsmall">The Access ID#.</span></span>
-->
                </div>
              </div>
              <div class="col-5">
                <?php echo form_input($registerLastDate); ?>
                <span class="medium_icon"> <i class="icon-calender cal6"></i> </span>
                <span id="error-dinner_dance6"></span> </div>
            </div>
            
            <div class="row">
              <div class="col-3">
                <div class="labelDiv">
                  <label class="form-label text-right"><?php echo lang('event_public_invitation'); ?><span class="required" aria-required="true"></span></label> <span class="info_btn"><span class="field_info xsmall"><?php echo lang('public_event_info'); ?></span></span>
                </div>
              </div>
              <div class="col-5">
                <div class="radioDiv">
                    <?php echo form_radio($eventPublicYes); ?>
                    <label class="form-label pull-left" for="eventPublicYes"><span><?php echo lang('event_public_invitation_yes'); ?></span></label>
                </div>
                <div class="radioDiv">
                    <?php echo form_radio($eventPublicNo);  ?>
                    <label class="form-label pull-left" for="eventPublicNo"><span><?php echo lang('event_public_invitation_no'); ?></span></label>
                </div>
                <span id="error-dinner_dance6"></span> </div>
            </div>
          </div>
          <div class="panel_footer"> <a href="javascript:void();" class="scrollTopTrg" style=""><img alt="Logo image" src="<?php echo IMAGE; ?>scroll_top.png">Top</a>
            <div class="pull-right">
                <?php
                    echo form_hidden('eventId', $eventId);              
                    echo form_hidden('formActionName', 'generalEventSetup');
                    $formButton['showbutton']   = array('save', 'reset');
                    $formButton['labletext']    = array('save' => lang('comm_save_changes'), 'reset' => lang('comm_reset'));
                    $formButton['buttonclass']  = array('save' => 'confirm_check_trigger btn-normal btn', 'reset' => 'btn-normal btn reset_space reset_form reset_form');
                    $this->load->view('common_save_reset_button', $formButton);
                ?>
            </div>
          </div>
        <?php echo form_close(); ?>
  </div>
</div>
<script>
/*
* Set js variable
* file page/event_details.js
*/
var reglastDate = '<?php echo $geseventlastregdate; ?>';
var opentab     = '<?php echo $requiredCheck;  ?>'; 
</script>
