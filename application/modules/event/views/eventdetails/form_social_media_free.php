<?php  if (!defined('BASEPATH'))  exit('No direct script access allowed');
$eventSocialMediadetails = $eventSocialMediadetails[0];
//set field value in variable
$eventWebsite   = (!empty($eventSocialMediadetails->website))   ? $eventSocialMediadetails->website : '';
$eventFacebook  = (!empty($eventSocialMediadetails->facebook))  ? $eventSocialMediadetails->facebook : '';
$eventInstagram = (!empty($eventSocialMediadetails->instagram)) ? $eventSocialMediadetails->instagram : '';
$eventTwitter   = (!empty($eventSocialMediadetails->twitter))   ? $eventSocialMediadetails->twitter : '';
$eventYoutube   = (!empty($eventSocialMediadetails->youtube))   ? $eventSocialMediadetails->youtube : '';
$eventPintrest  = (!empty($eventSocialMediadetails->pintrest))  ? $eventSocialMediadetails->pintrest : '';
$eventLinkedinVal  = (!empty($eventSocialMediadetails->linkedIn))  ? $eventSocialMediadetails->linkedIn : '';
$eventGplusVal  = (!empty($eventSocialMediadetails->gplus))  ? $eventSocialMediadetails->gplus : '';
$formSocialMediaSetup = array(
'name'          => 'formSocialMediaSetup',
'id'            => 'formSocialMediaSetup',
'method'        => 'post',
'class'         => '',
);
$eventhttp = array(
'name'          => '',
'value'         => 'http://',
'disabled'=>'disabled',
'type'          => 'text',
'class'         => 'xsmall_input trim_check pull-left',
'autocomplete'  => 'off',
);

$eventWebsite = array(
'name'          => 'eventWebsite',
'value'         => $eventWebsite,
'id'            => 'eventWebsite',
'type'          => 'text',
'placeholder'   => lang('event_website'),
'class'         => 'large_input mLm5 socialmediacheck',
'autocomplete'  => 'off',
);
$eventFacebook = array(
'name'          => 'eventFacebook',
'value'         => $eventFacebook,
'id'            => 'eventFacebook',
'type'          => 'text',
'placeholder'   => lang('event_facebook'),
'class'         => 'large_input mLm5 socialmediacheck',
'autocomplete'  => 'off',
);
$eventInstagram = array(
'name'          => 'eventInstagram',
'value'         => $eventInstagram,
'id'            => 'eventInstagram',
'type'          => 'text',
'placeholder'   => lang('event_instagram'),
'class'         => ' large_input mLm5 socialmediacheck',
'autocomplete'  => 'off',
);
$eventTwitter = array(
'name'          => 'eventTwitter',
'value'         => $eventTwitter,
'id'            => 'eventTwitter',
'type'          => 'text',
'placeholder'   => lang('event_twitter'),
'class'         => ' large_input mLm5 socialmediacheck',
'autocomplete'  => 'off',
);
$eventYoutube = array(
'name'          => 'eventYoutube',
'value'         => $eventYoutube,
'id'            => 'eventYoutube',
'type'          => 'text',
'placeholder'   => lang('event_youtube'),
'class'         => ' large_input mLm5 socialmediacheck',
'autocomplete'  => 'off',
);
$eventPintrest = array(
'name'          => 'eventPintrest',
'value'         => $eventPintrest,
'id'            => 'eventPintrest',
'type'          => 'text',
'placeholder'   => lang('event_pintrest'),
'class'         => 'large_input mLm5 socialmediacheck',
'autocomplete'  => 'off',
);

$eventGplus = array(
'name'          => 'eventGplus',
'value'         => $eventGplusVal,
'id'            => 'eventGplus',
'type'          => 'text',
'placeholder'   => 'Google+',
'class'         => 'large_input mLm5 socialmediacheck',
'autocomplete'  => 'off',
);
$eventLinkedin = array(
'name'          => 'eventLinkedin',
'value'         => $eventLinkedinVal,
'id'            => 'eventLinkedin',
'type'          => 'text',
'placeholder'   => 'Linkedin',
'class'         => 'large_input mLm5 socialmediacheck',
'autocomplete'  => 'off',
);
?>
<div class="panel" id="event_social_media_setup_panel">
      <div class="panel_header"><?php echo lang('event_social_media_setup'); ?></div>
      <div class="panel_contentWrapper" id="showhideformdivSocialMedia">
      
      <div class="infobar">
        <p class="dn" id="event_social_media_info">The Access ID# is a unique identifier assigned to every registrant using this site. Use it to access, edit, cancel, pay and rebook items in your registration.</p>
        <span class="info_btn" onclick="showHideInfoBar('event_social_media_info');"></span>
      </div>
      
      	<!--sub menu one-->
        <?php echo form_open($this->uri->uri_string(), $formSocialMediaSetup); ?>
              <div class="panel_content">
                <div class="row">
                  <div class="col-3">
                    <div class="labelDiv">
                      <label class="form-label text-right pull-left"><?php echo lang('event_website'); ?></label>
                    </div>
                  </div>
                  <div class="col-5">
                    <label class="xsmall_input disabled_label"><?php echo lang('url_prefix_social_media'); ?></label>  
                    <?php //echo form_input($eventhttp); ?>
                    <?php echo form_input($eventWebsite); ?>
                    <?php echo form_error('eventWebsite'); ?>
                    <span id="error-eventWebsite"></span> </div>
                </div>
                
                <div class="row">
                  <div class="col-3">
                    <div class="labelDiv">
                      <label class="form-label text-right pull-left"><?php echo lang('event_facebook'); ?></label>
                    </div>
                  </div>
                  <div class="col-5">
					<label class="xsmall_input disabled_label"><?php echo lang('url_prefix_social_media'); ?></label> 
                    <?php //echo form_input($eventhttp); ?>
                    <?php echo form_input($eventFacebook); ?>
                    <?php echo form_error('eventFacebook'); ?>
                    <span id="error-eventFacebook"></span> </div>
                </div>
                
                <div class="row">
                  <div class="col-3">
                    <div class="labelDiv">
                      <label class="form-label text-right pull-left"><?php echo lang('event_instagram'); ?></label>
                    </div>
                  </div>
                  <div class="col-5">
                    <label class="xsmall_input disabled_label"><?php echo lang('url_prefix_social_media'); ?></label> 
                    <?php //echo form_input($eventhttp); ?>
                    <?php echo form_input($eventInstagram); ?>
                    <?php echo form_error('eventInstagram'); ?>
                    <span id="error-eventInstagram"></span> </div>
                </div>
                
                <div class="row">
                  <div class="col-3">
                    <div class="labelDiv">
                      <label class="form-label text-right pull-left"><?php echo lang('event_twitter'); ?></label>
                    </div>
                  </div>
                  <div class="col-5">
					<label class="xsmall_input disabled_label"><?php echo lang('url_prefix_social_media'); ?></label> 
                    <?php //echo form_input($eventhttp); ?>
                    <?php echo form_input($eventTwitter); ?>
                    <?php echo form_error('eventTwitter'); ?>
                    <span id="error-eventTwitter"></span> </div>
                </div>
                
                <div class="row">
                  <div class="col-3">
                    <div class="labelDiv">
                      <label class="form-label text-right pull-left"><?php echo lang('event_youtube'); ?></label>
                    </div>
                  </div>
                  <div class="col-5">
					<label class="xsmall_input disabled_label"><?php echo lang('url_prefix_social_media'); ?></label> 
                    <?php //echo form_input($eventhttp); ?>
                    <?php echo form_input($eventYoutube); ?>
                    <?php echo form_error('eventYoutube'); ?>
                    <span id="error-eventYoutube"></span> </div>
                </div>
                
                <div class="row">
                  <div class="col-3">
                    <div class="labelDiv">
                      <label class="form-label text-right pull-left"><?php echo lang('event_pintrest'); ?></label>
                    </div>
                  </div>
                  <div class="col-5">
					<label class="xsmall_input disabled_label"><?php echo lang('url_prefix_social_media'); ?></label> 
                    <?php //echo form_input($eventhttp); ?>
                    <?php echo form_input($eventPintrest); ?>
                    <?php echo form_error('eventPintrest'); ?>
                    <span id="error-eventPintrest"></span> 
                    </div>
                </div>
                
                <div class="row">
                  <div class="col-3">
                    <div class="labelDiv">
                      <label class="form-label text-right pull-left">Google+</label>
                    </div>
                  </div>
                  <div class="col-5">
					<label class="xsmall_input disabled_label"><?php echo lang('url_prefix_social_media'); ?></label> 
                    <?php //echo form_input($eventhttp); ?>
                    <?php echo form_input($eventGplus); ?>
                    <?php echo form_error('eventGplus'); ?>
                    <span id="error-eventGplus"></span> 
                    </div>
                </div>
                <div class="row">
                  <div class="col-3">
                    <div class="labelDiv">
                      <label class="form-label text-right pull-left">Linkedin</label>
                    </div>
                  </div>
                  <div class="col-5">
					<label class="xsmall_input disabled_label"><?php echo lang('url_prefix_social_media'); ?></label> 
                    <?php //echo form_input($eventhttp); ?>
                    <?php echo form_input($eventLinkedin); ?>
                    <?php echo form_error('eventLinkedin'); ?>
                    <span id="error-eventLinkedin"></span> 
                    </div>
                </div>
                
            
                
             <div class="custom_field_div_social_media row" id="custom_field_div_social_media">
                <?php
                $where = array('event_id' => $eventId);
                $customSocialMediaTypes = getDataFromTabel('event_social_media_custom', '*', $where, '', 'id', 'ASC');
                if(!empty($customSocialMediaTypes)){
                    foreach($customSocialMediaTypes as $customSocialMediaType){
                        $customSocialMediaTypeId = $customSocialMediaType->id;
                        
                        ?>
                    <div class="row" id="masterfieldlbl_<?php echo $customSocialMediaTypeId; ?>">    
                    <div class="col-3">    
                       <div class="labelDiv">
                          <label class="form-label text-right">
                              <?php echo $customSocialMediaType->media_name; ?>
                          </label>
                        </div>
                     </div>   
                        
                    <div class="col-5">
                            <?php echo form_hidden('fieldid[]', $customSocialMediaTypeId); ?>
                            <?php //echo form_input($eventhttp); ?>
                            <label class="xsmall_input disabled_label pull-left">http://</label> 
                            <input type="text" name="mediatypevalue[]" value="<?php echo $customSocialMediaType->media_value; ?>" class="medium_input pull-left">
                            <span class="category_btn editButtonGroup pull-left">   
                                
                                <a class="eventsetup_btn event_social_media btn"  href="javascript:void(0)" formAction="bookerCustomFieldSave" customfieldid="<?php echo $customSocialMediaTypeId; ?>">
                                    <span class="medium_icon"><i class="icon-edit"></i> </span>
                                </a>
                                <a href="javascript:void(0)" class="btn red delete_custom_social_media" deleteid="<?php echo $customSocialMediaTypeId; ?>">
                                   <span class="medium_icon"><i class="icon-delete"></i> </span>
                                </a>
                            </span>
                        </div>
                       </div>
                       <!-- row end --> 
                        <?php  
                    }
                }
                ?>
                </div>
             
             <div class="input_fields_wrap_social_media" id="eventcustomcontactlistId"> </div>
                
<!--
                <div class="row mT30">
                    <div class="col-1"></div>
                    <div class="col-5">
                        <div class="">
                            <a class="btn-icon btn-icon-large btn event_social_media" href="javascript:void(0)"  customfieldid="0" ><span class="medium_icon"><i class="icon-add"></i></span><div class="font-small"><?php echo lang('add_social_media_button_text'); ?></div></a>
                        </div>
                    </div>
                </div>
-->
              
              </div>
              <div class="panel_footer"> 
                <div class="pull-right">
                    <?php
                        echo form_hidden('eventId', $eventId);
                        echo form_hidden('formActionName', 'eventSocialMedia');
                        $formButton['showbutton'] = array('save', 'reset');
                        $formButton['labletext'] = array('save' => lang('comm_save_changes'), 'reset' => lang('comm_reset'));
                        $formButton['buttonclass'] = array('save' => 'btn-normal btn', 'reset' => 'btn-normal btn reset_space reset_form');
                        $this->load->view('common_save_reset_button', $formButton);
                    ?>
                </div>
              </div>
            </form>
            </div>
      </div>
<script type="text/javascript">
var event_msg_custom_social_media_deleted = '<?php echo lang('event_msg_custom_social_media_deleted') ?>';
var eventId = '<?php echo $eventId ?>';
</script>          
