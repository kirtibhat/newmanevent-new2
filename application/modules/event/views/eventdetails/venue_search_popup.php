<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//--------add type fo registration fields---------//  

$headerAction = 'Search Venue'; 

$formVenueSearch = array(
    'name'   => 'formVenueSearch',
    'id'   => 'formVenueSearch',
    'method' => 'post',
    'class'  => 'form-horizontal',
    'data-parsley-validate' => ''
);
  
//prepare state list data
$countryDataList[""] = 'Select Country';  
if(!empty($countrydata)){
  foreach($countrydata as $key => $value){
      $countryDataList[$value->id] = $value->country_name;
  }
}

?>

<!--------Add exhibitor div start-------->
<div class="modal fade alertmodelbg dn" id="venue_search_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
      
      <div class="modal-header delegates">
        <h4 class="medium dt-large modal-title"><?php echo $headerAction; ?></h4>
      </div>
      <div class="modal-body small">
        <div class="modelinner ">
            
        <div class="newevent_setup">
        <?php  echo form_open($this->uri->uri_string(),$formVenueSearch); ?>                
                Coming Soon...
        <?php echo form_close(); ?>
              </div>
       <div class="btn_wrapper">
            <input type="submit" class="popup_cancel submitbtn pull-right medium" value="Cancel" name="logincancel" data-dismiss="modal">
        </div>
       </div>
              <!--end of modeliner-->
        
      </div>
    </div>
  </div>
</div>
<!--------Add braakout div end-------->

<script type="text/javascript">

// validate form
//$("#formVenueSearch").parsley();

// serach venue model popup
popupopen('open_venue_search','venue_search_modal','formVenueSearch');  

//set state list data by country selection
$('#selectCountry').change(function(){
  var countryId = $(this).val();
  if(countryId){
	  sendData = {'countryId':countryId};
	  postUrl = 'event/statelist';
	  doAjaxRequest(postUrl,sendData,'selectState');
  } else {
	  $('#selectState').html('<option value="" selected="selected">Select State</option>');
	  $('#selectState').parent('.select-wrapper').children('span.holder').text('Select State');
	  $('#selectCity').html('<option value="" selected="selected">Select City</option>');
	  $('#selectCity').parent('.select-wrapper').children('span.holder').text('Select City');
	  $('#selectCompanyName').html('<option value="" selected="selected">Select Company Name</option>');
	  $('#selectCompanyName').parent('.select-wrapper').children('span.holder').text('Select Company Name');
  }
})
  
//set city list data by state selection
$('#selectState').change(function(){
  var stateId = $(this).val();
  if(stateId){
	  sendData = {'stateId':stateId};
	  postUrl = 'event/citylist';
	  doAjaxRequest(postUrl,sendData,'selectCity');
  } else {
	  $('#selectCity').html('<option value="" selected="selected">Select City</option>');
	  $('#selectCity').parent('.select-wrapper').children('span.holder').text('Select City');
	  $('#selectCompanyName').html('<option value="" selected="selected">Select Company Name</option>');
	  $('#selectCompanyName').parent('.select-wrapper').children('span.holder').text('Select Company Name');
  }
})  

//set company list data by state selection
$('#selectCity').change(function(){
  var stateId = $('#selectState').val();
  var cityId = $(this).val();
  if(cityId){
	  sendData = {'stateId':stateId,'cityId':cityId};
	  postUrl = 'event/companynamelist';
	  doAjaxRequest(postUrl,sendData,'selectCompanyName');
  } else {
	  $('#selectCompanyName').html('<option value="" selected="selected">Select Company Name</option>');
	  $('#selectCompanyName').parent('.select-wrapper').children('span.holder').text('Select Company Name');
  }
})    

//selected venue name set
$("#formVenueSearch").submit(function( event ){
  
  //validate form
  if($("#formVenueSearch").parsley().isValid()){
  event.preventDefault();       
  if($('#selectCompanyName').val()!=""){
    var getSearchVenu = $('#selectCompanyName option:selected').text();
    $('#eventVenue').val('');
    $('#eventVenue').val(getSearchVenu);
    //alert(getSearchVenu);
    //hide popup
    hide_popup('venue_search_modal');
  }else{
    
    //get form data
    var formData = $(this).serializeArray();
    var errorMsg = '';
    $.each(formData, function( i, field ) {
      if(field.value==0){
        errorMsg += 'Please select '+ field.name +' field.<br>';
      }
    });
    
    var openbox = $("#ajax_open_popup_html").attr('openbox');
    //hide open model box 
    hide_popup(openbox);
    //set error message
    custom_popup(errorMsg,false);
    
    //add class in custome popup
    var clickbox = $("#ajax_open_popup_html").attr('clickbox');
    $(".cust_popup_button").addClass(clickbox);
  }

  return false;
  } //end validation      
})

</script>
