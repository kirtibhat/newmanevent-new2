<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
$tctermconditionid   = (!empty($eventdetails->id))?$eventdetails->id:'';
$tctermcondition     = (!empty($eventdetails->term_conditions))?$eventdetails->term_conditions:'';
$tcdocumentfile      = (!empty($eventdetails->document_file))?$eventdetails->document_file:'';
$formTermsCondition  = array(
    'name'          => 'formTermsCondition',
    'id'            => 'formTermsCondition',
    'method'        => 'post',
    'class'         => '',
    'enctype'       => "multipart/form-data",
);
$eventnTermsCondition = array(
    'name'          => 'eventnTermsCondition',
    'value'         => (empty($tctermcondition)) ? lang('event_term_condition_default_msg') : $tctermcondition,
    'id'            => 'eventnTermsCondition',
    'type'          => 'text',
    'rows'          => '5',
    'class'         => 'form-textarea'
);  
$termSelection = array(
    'name'          => 'termSelection',
    'value'         => '0',
    'id'            => 'termSelection',
    'type'          => 'hidden',
);  
?>
<div class="panel" id="terms_conditions_panel">
  <div class="panel_header"><?php echo lang('event_term_term_condi_title'); ?></div>
  <div class="panel_contentWrapper" id="showhideformdivtermSection">
      <div class="infobar">
        <p class="dn" id="terms_condition_info">The Access ID# is a unique identifier assigned to every registrant using this site. Use it to access, edit, cancel, pay and rebook items in your registration.</p>
        <span class="info_btn" onclick="showHideInfoBar('terms_condition_info');"></span>
      </div>
    <!--sub menu one-->
    <?php echo form_open($this->uri->uri_string(),$formTermsCondition); ?>
          <div class="panel_content">
            <div class="row">
              <div class="col-3">
                <div class="labelDiv">
                  <label class="form-label text-right"><?php echo lang('event_term_term_condition'); ?> </label>
                   <span class="info_btn"><span class="field_info xsmall">The Access ID#.</span></span>
                </div>
              </div>
              <div class="col-5">
                 <?php echo form_textarea($eventnTermsCondition); ?> 
                <span id="error-dinner_dance6"></span> </div>
            </div>
            <div class="row">
              <div class="col-3">
              <div class="labelDiv">
                  <label class="form-label text-right"><?php echo lang('add_document_text'); ?></label>
                  
                </div>
              </div>
              <div class="col-5">
                  <div class="" id="attch_FileContainerTerm">
                    <div class="" id="attch_dropAreaTerm">
                      <label for="attach_doc"></label>
                      <div class="custom-upload" id="attch_pickfilesTerm" style="margin-bottom:0">
                        <span class="label_text"><?php echo lang('comm_file_upload_drop'); ?></span>
                        <input type="file" name="attach_doc" id="attach_doc" draggable="true" />
                      </div>
                      <div id="attch_file_listTerm" class="mb10 pull-left custom-upload-file addFileEvent"></div>
                    </div>
                    </div>
                  <span id="error-dinner_dance6"></span> 
                </div>
            </div>
            
          <div class="row">
                <div class="col-3">
              <div class="labelDiv">
               
                  
                </div>
              </div>
          <div class="col-5">  
          <div class="control-group mb10 pull-left  <?php echo (empty($tcdocumentfile))?'dn':' '; ?>" id="attach_file_list_mainTerm" >
          <div class="controls " id="attch_file_listTerm" >
              <?php  if(!empty($tcdocumentfile)){ 
               $fileId= remove_extension($tcdocumentfile);  
                ?>
                 <div class="uploadedName attchadddedfileTerm uploaded_file" id="<?php echo $fileId; ?>"> 
                    <div class="imagename_cont pr mr_5px font-xsmall"><?php echo $tcdocumentfile; ?></div>
                    <span class="small_icon delete_file" deleteurl='event/deletetermattachmentterm' id="cancel_<?php echo $fileId; ?>" deleteid="<?php echo $tctermconditionid; ?>"  fileid="<?php echo $fileId; ?>"><img src="<?php echo IMAGE; ?>crox.png"></span>
                  </div> 
              <?php }  ?> 
          </div>
        </div>
        </div>
        </div>

          </div>
          <div class="panel_footer"> <a href="javascript:void();" class="scrollTopTrg" style=""><img alt="Logo image" src="<?php echo IMAGE; ?>scroll_top.png">Top</a>
            <div class="pull-right">
                <?php 
                echo form_input($termSelection);
                echo form_hidden('eventId', $eventId);
                echo form_input(array('name' => 'term_condi_id', 'type'=>'hidden', 'id' =>'term_condi_id', 'value'=>$tctermconditionid));
                echo form_hidden('formActionName', 'eventTermsCondition');
              ?> 
              <button name="form_reset" type="button" class="btn-normal btn reset_space reset_form"><div class="btn_center">Clear</div></button>
              <input id="div_ar2RA_a" type="submit" name="save" value="Save" class="btn-normal btn term_condi_submit">
            </div>
          </div>
        <?php echo form_close();?>
  </div>

<script type="text/javascript" src="<?php  echo $system_js_path.'upload_js/plupload.js'; ?>"></script>
<script type="text/javascript" src="<?php  echo $system_js_path.'upload_js/plupload.gears.js'; ?>"></script>
<script type="text/javascript" src="<?php  echo $system_js_path.'upload_js/plupload.silverlight.js'; ?>"></script>
<script type="text/javascript" src="<?php  echo $system_js_path.'upload_js/plupload.flash.js'; ?>"></script>
<script type="text/javascript" src="<?php  echo $system_js_path.'upload_js/plupload.browserplus.js'; ?>"></script>
<script type="text/javascript" src="<?php  echo $system_js_path.'upload_js/plupload.html4.js'; ?>"></script>
<script type="text/javascript" src="<?php  echo $system_js_path.'upload_js/plupload.html5.js'; ?>"></script>
<script type="text/javascript" src="<?php  echo $system_js_path.'upload_common.js'; ?>"></script>
<script type="text/javascript" >
// Custom example logic
	var postData = {event_id : eventId}
	var uploadUrl = baseUrl+'event/uploadtermattachmentterm';

	var otherObj = {
		pickfiles:"attch_pickfilesTerm", 
		dropArea:"attch_dropAreaTerm", 
		FileContainer:"attch_FileContainerTerm", 
		fileTypes:"pdf,doc", 
		deleteUrl:baseUrl+'event/deletetermattachmentterm', 
		maxfilesize:"10mb", 
		multiselection:false, 
		isFileList:true, 
		uniqueNames:true, 
		autoStart:false, 
		isDelete:true, 
		maxfileupload:1, 
		mainDivFileList:"attach_file_list_mainTerm",
		listAddFile:"attch_file_listTerm",
		adddedfile:"attchadddedfileTerm",
	}
	var popupmodelid = '';
	upload_file(uploadUrl,postData,otherObj);
	  
	//Delete common delete function
	deleteUploadFiles();
    function deleteFileRow(){
        $(".addFileEvent").removeClass('uploadedName');
    }
    
</script>
