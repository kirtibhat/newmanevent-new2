<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

$requiredCheck  =  !empty($this->input->get('req'))?$this->input->get('req'):'';
$openTab        = (!empty($requiredCheck) && ($requiredCheck == 4))  ? 'open' : '';

if(!empty($userdata)){ $userdata = $userdata[0]; }
/*set user details data*/
$userFirstName      = (!empty($userdata->firstname))?$userdata->firstname:'';
$userLastName       = (!empty($userdata->lastname))?$userdata->lastname:'';
$userEmail        	= (!empty($userdata->email))?$userdata->email:'';
$userCompany      	= (!empty($userdata->company_name))?$userdata->company_name:'';
$userPhone        	= (!empty($userdata->phone))?$userdata->phone:'';
$contactPersionId   = (!empty($eventdetails->contact_persion_id))?$eventdetails->contact_persion_id:'0';
if(!empty($eventdetails->contact_persion_id)){
	$cpfirstname    = (!empty($eventdetails->first_name))?$eventdetails->first_name:'';
	$cplastname     = (!empty($eventdetails->last_name))?$eventdetails->last_name:'';
	$cporganisation = (!empty($eventdetails->organisation))?$eventdetails->organisation:'';
	$cpemail        = (!empty($eventdetails->email))?$eventdetails->email:'';
	$phone1_mobile  = (!empty($eventdetails->phone1_mobile))?$eventdetails->phone1_mobile:"";
} else{
	$cpfirstname    = $userFirstName;
	$cplastname     = $userLastName;
	$cporganisation = $userCompany;
	$cpemail        = $userEmail;
	$phone1_mobile  = $userPhone;
}
$cptitle            = (!empty($eventdetails->title))?$eventdetails->title:'';
$phone1_landline    = (!empty($eventdetails->phone1_landline))?$eventdetails->phone1_landline:'';

$phone_value='';
$selectedClssMob ='selected';
$selectedClsslandline ='';
if(!empty($eventdetails->phone1_mobile)) {
    $phone_value = $eventdetails->phone1_mobile;
    //$selectedClssMob ='selected';
}

if(!empty($eventdetails->phone1_landline)) {
    $phone_value = $eventdetails->phone1_landline;
    //$selectedClsslandline ='selected';
}    

if(!empty($eventdetails->phone1_mobile) && !empty($eventdetails->phone1_landline)) {
    $selectedClssMob ='selected';
    $phone_value = $eventdetails->phone1_mobile;
}

if(empty($eventdetails->phone1_mobile) && empty($eventdetails->phone1_landline)) {
    $selectedClssMob ='selected';
    //$phone_value = $eventdetails->phone1_landline;
}

/* Define form fields */
$formGeneralEventSetup = array(
'name'          => 'formContactPerson',
'id'            => 'formContactPerson',
'method'        => 'post',
'class'         => '',
);
$firstName = array(
'name'          => 'firstName',
'value'         => $cpfirstname,
'id'            => 'firstname',
'type'          => 'text',
'autocomplete'  => 'off',
'class'         => 'xlarge_input',
);  
$lastName = array(
'name'          => 'lastName',
'value'         => $cplastname,
'id'            => 'lastname',
'type'          => 'text',
'class'         => 'xlarge_input',
'autocomplete'  => 'off',
);    
$contactOrganisation = array(
'name'          => 'contactOrganisation',
'value'         => $cporganisation,
'id'            => 'company_name',
'type'          => 'text',
'class'         => 'xlarge_input',
'autocomplete'  => 'off',
);
$contactPhone = array(
'name'          => 'contactPhone',
'value'         => $phone_value,
'id'            => 'mobile',
'type'          => 'text',
'class'         => 'medium_input pull-left contact_input phoneValue contacteventdata',
'placeholder'   => '123456789',
'size'          => '40',
); 
$contactEmail = array(
'name'          => 'contactEmail',
'value'         => $cpemail,
'id'            => 'email',
'type'          => 'email',
'class'         =>'xlarge_input',
'autocomplete'  => 'off',
);
$confirmEmail = array(
'name'          => 'confirmEmail',
'id'            => 'confirmEmail',
'value'         => $cpemail,
'class'         => 'xlarge_input confirm_email',
'autocomplete'  => 'off',
);
?>
<div class="panel <?php echo $openTab; ?>" id="event_contact_person_panel">
  <div class="panel_header"><?php echo lang('event_contact_person'); ?></div>
  <div class="panel_contentWrapper" id="showhideformdivpersonalContact" style="<?php if(!empty($openTab)){ echo 'display:block;'; }?>" >
  <div class="infobar">
    <p class="dn" id="event_contact_person_info">The Access ID# is a unique identifier assigned to every registrant using this site. Use it to access, edit, cancel, pay and rebook items in your registration.</p>
    <span class="info_btn" onclick="showHideInfoBar('event_contact_person_info');"></span>
  </div>
    <?php echo form_open($this->uri->uri_string(),$formGeneralEventSetup); ?>
          <div class="panel_content">
            <div class="row">
              <div class="col-3">
                <div class="labelDiv">
                  <label class="form-label text-right"><?php echo lang('event_contact_title'); ?> </label>
                </div>
              </div>
              <div class="col-5">
                 <?php 
                      $other                = 'id="title" class="custom-select medium_select"';
                      $titleArray['']       = 'Select Title';
                      $titleArr             = $this->config->item('title_array');
                      foreach($titleArr as $key=>$value){
                        $titleArray[$key]   = $value;
                      } 
                      echo form_dropdown('title',$titleArray,$cptitle,$other);
                      echo form_error('title');
                    ?>    
                <span id="error-dinner_dance6"></span> </div>
            </div>   
            <div class="row">
              <div class="col-3">
                <div class="labelDiv">
                    <label class="form-label text-right"><?php echo lang('event_contact_first_last'); ?> <span class="required">*</span></label>
                </div>
              </div>
              <div class="col-5">
                <?php echo form_input($firstName); ?>
                <?php echo form_error('firstName'); ?>
                <span id="error-firstName"></span>
              </div>
            </div>
            <div class="row">
              <div class="col-3">
                <div class="labelDiv">
                <label class="form-label text-right"><?php echo lang('event_contact_last_name'); ?> <span class="required">*</span>
                
                </label>
                  
                </div>
              </div>
              <div class="col-5">
                <?php echo form_input($lastName); ?>
                <?php echo form_error('lastName'); ?>
                <span id="error-lastName"></span> </div>
            </div>
            <div class="row">
              <div class="col-3">
                <div class="labelDiv">
                  <label class="form-label text-right"><?php echo lang('event_contact_organisation'); ?><!--<span class="required">*</span>--></label>
                </div>
              </div>
              <div class="col-5">
                <?php echo form_input($contactOrganisation); ?>
                <?php echo form_error('contactOrganisation'); ?>
                <span id="error-dinner_dance6"></span> </div>
            </div>
            <div class="row">
              <div class="col-3">
                <div class="labelDiv">
                  <label class="form-label text-right"><?php echo lang('event_contact_phone_free'); ?> <span class="required">*</span> 
                  
                  </label>
                </div>
              </div>
              <div class="col-5">
                <input type='hidden' name='input_hidden_mobile1' id='input_hidden_mobile1' value="<?php echo $phone1_mobile; ?>" >
                <input type='hidden' name='input_hidden_landline1' id='input_hidden_landline1' value="<?php echo $phone1_landline; ?>">
                
                <input type='hidden' name='hidden_mobile1' id='hidden_mobile1' value="<?php echo $phone1_mobile; ?>" >
                <input type='hidden' name='hidden_landline1' id='hidden_landline1' value="<?php echo $phone1_landline; ?>">
                <input type='hidden' name='phone_field_change_status' id='phone_field_change_status' value="landline">

                 <div class="phone_box col-3">            
                    <?php echo form_input($contactPhone); ?>
                    <span id="buttonid"><img src="<?php echo IMAGE; ?>drparw.png" width="28" height="28"></span>
                    <ul id="toggle" class="phone_field_opt change_input_class" style="display:none;">
                        <li data-select="mobile1" class="<?php echo $selectedClssMob; ?> contact_event" contactType="mobile" id="mobile1">Mobile</li>
                        <li data-select="landline1" class="<?php echo $selectedClsslandline; ?> contact_event" contactType="landline" id="landline1">Landline</li>
                      </ul>
                      <span id="error-contactPhone"></span> </div>
                    </div>
            </div>
            <div class="row">
              <div class="col-3">
                <div class="labelDiv">
                <label class="form-label text-right"><?php echo lang('event_contact_email'); ?> <span class="required">*</span>
                
                </label>
                  
                </div>
              </div>
              <div class="col-5">
                  <?php echo form_input($contactEmail); ?>
                  <?php echo form_error('contactEmail'); ?>
                <span id="error-contactEmail"></span> </div>
            </div>
            <div class="row">
              <div class="col-3">
                <div class="labelDiv">
                <label class="form-label text-right"><?php echo lang('event_confirm_email'); ?> <span class="required">*</span>
                
                </label>
                </div>
              </div>
              <div class="col-5">
                  <?php echo form_input($confirmEmail); ?>
                  <?php echo form_error('confirmEmail'); ?>
                <span id="error-confirmEmail"></span> </div>
            </div>
        
            <div class="input_fields_wrap row" id="eventcustomcontactlistId">
                
                <?php echo $this->load->view('custom_contact_list',array('contactPersionId'=>$contactPersionId)); ?>
                
            </div>
            <input type="hidden" value="<?php echo $contactPersionId; ?>" id="eventContactPersionId" />
            <input type="hidden" id="hidden_input_no" value="0" /> 
            <div id="hidden_custom_contact_array"></div>
            <div class="row mT30">
            <div class="col-1">
            </div>
            <div class="col-5">
				<a class="btn-icon btn-icon-large btn add_contact_type" href="javascript:void(0)"  customfieldid="0" >					
					 <span class="medium_icon"><i class="icon-add"></i></span>
				<div class="font-small"><?php echo lang('add_contact_button_text'); ?></div></a>
			</div>
            </div>
          </div>
          <div class="panel_footer">
            <div class="pull-right">
                <?php 
                      echo form_hidden('eventId', $eventId);
                      echo form_hidden('formActionName', 'contactPerson');
                      $formButton['showbutton']     = array('save','reset');
                      $formButton['labletext']      = array('save'=>lang('comm_save_changes'),'reset'=>lang('comm_reset'));
                      $formButton['buttonclass']    = array('save'=>'btn-normal btn','reset'=>'btn-normal btn reset_space reset_form');
                      $this->load->view('common_save_reset_button',$formButton);
                  ?>
            </div>
          </div>
        <?php echo form_close();?>
  </div>
</div>
<script> var contact_userdata = <?php echo json_encode($userdata); ?>; </script>
<script>
/*
* Set js variable
* file page/event_details.js
*/
var opentab     = '<?php echo $requiredCheck;  ?>'; 
</script>
