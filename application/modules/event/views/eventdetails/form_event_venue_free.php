<?php if (!defined('BASEPATH'))  exit('No direct script access allowed');
/*set field value in variable*/
$geseventtitle          = (!empty($eventdetails->event_title)) ? $eventdetails->event_title : '';
$geseventsubtitle       = (!empty($eventdetails->subtitle)) ? $eventdetails->subtitle : '';
$geseventdescription    = (!empty($eventdetails->description)) ? $eventdetails->description : '';
$geseventlocation       = (!empty($eventdetails->event_location)) ? $eventdetails->event_location : '';
$geseventvenue          = (!empty($eventdetails->event_venue)) ? $eventdetails->event_venue : '';
/*new added venue fields*/
$geseventvenueAddress1  = (!empty($eventdetails->eventVenueAddress1)) ? $eventdetails->eventVenueAddress1 : '';
$geseventvenueAddress2  = (!empty($eventdetails->eventVenueAddress2)) ? $eventdetails->eventVenueAddress2 : '';
$geseventvenueCity      = (!empty($eventdetails->eventVenueCity)) ? $eventdetails->eventVenueCity : '';
$geseventvenueState     = (!empty($eventdetails->eventVenueState)) ? $eventdetails->eventVenueState : '';
$geseventvenueZip       = (!empty($eventdetails->eventVenueZip)) ? $eventdetails->eventVenueZip : '';
$geseventspeaker        = (!empty($eventdetails->event_speaker)) ? $eventdetails->event_speaker : '';
$geseventlimit          = (!empty($eventdetails->event_limit)) ? $eventdetails->event_limit : '';
$geseventnumberrego     = (!empty($eventdetails->number_of_rego)) ? $eventdetails->number_of_rego : '';
$sittingStyleValue      = (!empty($eventdetails->sitting_style)) ? $eventdetails->sitting_style : '';
$timeZoneValue          = (!empty($eventdetails->time_zone)) ? $eventdetails->time_zone : '';
$destinationValue       = (!empty($eventdetails->destination)) ? $eventdetails->destination : '';
$lastRegDateValue       = (!empty($eventdetails->last_reg_date)) ? $eventdetails->last_reg_date : '';
$eventNumberValue       = (!empty($eventdetails->event_number)) ? $eventdetails->event_number : '';
$eventShrtFrmTiteValue  = (!empty($eventdetails->short_form_title)) ? $eventdetails->short_form_title : getAcronyms($geseventtitle);
$eventRefNumberValue    = (!empty($eventdetails->event_reference_number)) ? $eventdetails->event_reference_number : '';
$show_map               = (!empty($eventdetails->show_map)) ? $eventdetails->show_map : '';
$event_lat_val          = (!empty($eventdetails->event_lat)) ? $eventdetails->event_lat : '-25.2743980';
$event_long_val         = (!empty($eventdetails->event_long)) ? $eventdetails->event_long : '133.7751360';
/*get event number*/ 
$eventNumberValueArray  = str_split($eventNumberValue);
/*set value*/
$geseventlastregdate    = '';
if (!empty($eventdetails->last_reg_date)) {
    if ($eventdetails->last_reg_date == "0000-00-00 00:00:00") {
        $geseventlastregdate = '';
    } else {
        $geseventlastregdate = dateFormate($eventdetails->last_reg_date, "d M Y H:i");
    }
}
/*event early bird date*/
$geseventearlybirdregdate = '';
if (!empty($eventdetails->early_bird_reg_date)) {
    if ($eventdetails->early_bird_reg_date == "") {
        $geseventearlybirdregdate = '';
    } else {
        $geseventearlybirdregdate = dateFormate($eventdetails->early_bird_reg_date, "d M Y H:i");
    }
}
//$geseventlastregdate  = (!empty($eventdetails->last_reg_date)) ? dateFormate($eventdetails->last_reg_date, "d M Y"):'';
$geseventstarttime  = (!empty($eventdetails->starttime)) ? dateFormate($eventdetails->starttime, "d M Y H:i") : '';
$geseventendtime    = (!empty($eventdetails->endtime)) ? dateFormate($eventdetails->endtime, "d M Y H:i") : '';
$geseventeventid    = (!empty($eventdetails->event_id)) ? $eventdetails->event_id : '';
$registerStartDate  = (!empty($eventdetails->reg_startdate)) ? dateFormate($eventdetails->reg_startdate, "d M Y H:i") : '';
$earlybirdCloseDate = (!empty($eventdetails->reg_closedate)) ? dateFormate($eventdetails->reg_closedate, "d M Y H:i") : '';
$timeZoneOptions    = array('' => 'Select Time Zone', 'AWST' => 'Australian Western Standard Time', 'ACST' => 'Australian Central Standard Time', 'AEST' => 'Australian Eastern Standard Time');
/*for public */
/* Form fields */
$eventPublicYesChk  = true;
$eventPublicNoChk   = false;
$formGeneralEventSetup = array(
'name'          => 'formVenueEventSetup',
'id'            => 'formVenueEventSetup',
'method'        => 'post',
'class'         => '',
'data-parsley-validate' => ''
);
$event_lat = array(
'name'          => 'event_lat',
'value'         => $event_lat_val,
'id'            => 'event_lat',
'type'          => 'hidden',
'class'         => 'small',
);
$event_long = array(
'name'          => 'event_long',
'value'         => $event_long_val,
'id'            => 'event_long',
'type'          => 'hidden',
'class'         => 'small',
);

$eventName = array(
'name'          => 'eventName',
'value'         => $geseventtitle,
'id'            => 'eventName',
'type'          => 'text',
'placeholder'   => 'Event Name',
'required'      => '',
'class'         => 'alphaNumValue small',
'autocomplete'  => 'off',
'data-parsley-error-message'    => lang('common_field_required'),
'data-parsley-error-class'      => 'custom_li',
'data-parsley-trigger'          => 'keyup',
);
$eventSubtitle = array(
'name'          => 'eventSubtitle',
'value'         => $geseventsubtitle,
'id'            => 'eventSubtitle',
'type'          => 'text',
'placeholder'   => 'Event Subtitle',
'class'         => 'alphaNumValue small',
'autocomplete'  => 'off',
'data-parsley-error-message'    => lang('common_field_required'),
'data-parsley-error-class'      => 'custom_li',
'data-parsley-trigger'          => 'keyup',
);

$eventUrl = array(
'name'          => 'eventUrl',
'value'         => replaceSpaceWithCharacter($geseventtitle, "_"),
'id'            => 'eventUrl',
'type'          => 'text',
'placeholder'   => 'Event URL',
'required'      => '',
'class'         => 'small dark width_130',
'autocomplete'  => 'off',
'data-parsley-error-message'    => lang('common_field_required'),
'data-parsley-error-class'      => 'custom_li',
'data-parsley-trigger'          => 'keyup',
);
$eventShortFormTitle = array(
'name'          => 'eventShortFormTitle',
'value'         => $eventShrtFrmTiteValue,
'id'            => 'eventShortFormTitle',
'type'          => 'text',
'class'         => 'small dark width_214'
);
$eventReferenceNumber = array(
'name'          => 'eventReferenceNumber',
'value'         => $eventRefNumberValue,
'id'            => 'eventReferenceNumber',
'type'          => 'text',
'class'         => 'small'
);
$eventStartDate = array(
'name'          => 'eventStartDate',
'value'         => $geseventstarttime,
'id'            => 'eventStartDate',
'type'          => 'text',
'placeholder'   => 'Event Start Date',
'required'      => '',
'class'         => 'date_input small datetimepicker',
'data-format'   => 'yyyy-MM-dd hh:mm:ss',
'autocomplete'  => 'off',
'data-parsley-error-message'    => lang('common_field_required'),
'data-parsley-error-class'      => 'custom_li',
'data-parsley-trigger'          => 'change',
);
$eventEndDate = array(
'name'          => 'eventEndDate',
'value'         => $geseventendtime,
'id'            => 'eventEndDate',
'type'          => 'text',
'placeholder'   => 'Event End Date',
'required'      => '',
'class'         => 'date_input small datetimepicker',
'data-format'   => 'yyyy-MM-dd hh:mm:ss',
'autocomplete'  => 'off',
'data-parsley-error-message'    => lang('common_field_required'),
'data-parsley-error-class'      => 'custom_li',
'data-parsley-trigger'          => 'keyup',
);

$eventDescription = array(
'name'          => 'eventDescription',
'value'         => $geseventdescription,
'id'            => 'eventDescription',
'rows'          => '3',
'class'         => 'small'
);
$eventMaxRegistrants = array(
'name'          => 'eventMaxRegistrants',
'value'         => $geseventnumberrego,
'id'            => 'eventMaxRegistrants',
'type'          => 'text',
'class'         => 'small  short_field',
'required'      => '',
'autocomplete'  => 'off',
'data-parsley-type'         => 'integer',
'data-parsley-type-message' => lang('enter_numeric_value'),
'data-parsley-error-class'  => 'custom_li',
'data-parsley-trigger'      => 'keyup',
);
$registerLastDate = array(
'name'          => 'registerLastDate',
'value'         => $geseventlastregdate,
'id'            => 'registerLastDate',
'type'          => 'text',
'required'      => '',
'class'         => 'date_input small datetimepicker',
'data-format'   => 'yyyy-MM-dd hh:mm:ss',
'autocomplete'  => 'off',
'data-parsley-error-message'    => lang('common_field_required'),
'data-parsley-error-class'      => 'custom_li',
'data-parsley-trigger'          => 'change',
);
$registerStartDate = array(
'name'          => 'registerStartDate',
'value'         => $registerStartDate,
'id'            => 'registerStartDate',
'type'          => 'text',
'required'      => '',
'class'         => 'date_input small datetimepicker',
'data-format'   => 'yyyy-MM-dd hh:mm:ss',
'autocomplete'  => 'off',
'data-parsley-error-message'    => lang('common_field_required'),
'data-parsley-error-class'      => 'custom_li',
'data-parsley-trigger'          => 'change',
);
$eventDestination = array(
'name'          => 'eventDestination',
'value'         => $destinationValue,
'id'            => 'eventDestination',
'type'          => 'text',
'class'         => 'small',
);
$eventLocation = array(
'name'          => 'eventLocation',
'value'         => $geseventlocation,
'id'            => 'eventLocation',
'type'          => 'text',
'class'         => 'small',
);
$eventVenue = array(
'name'          => 'eventVenue',
'value'         => $geseventvenue,
'id'            => 'eventVenue',
'type'          => 'text',
'class'         => 'xlarge_input mapvenue',
'autocomplete'  => 'off',
);
/*
* new fields 
* for venue address
*/
$eventVenueAddress1 = array(
'name'          => 'eventVenueAddress1',
'value'         => $geseventvenueAddress1,
'id'            => 'eventVenueAddress1',
'type'          => 'text',
'class'         => 'xlarge_input',
'autocomplete'  => 'off',
'data-parsley-error-message'    => lang('common_field_required'),
'data-parsley-error-class'      => 'custom_li',
'data-parsley-trigger'          => 'keyup',
);

$eventVenueAddress2 = array(
'name'          => 'eventVenueAddress2',
'value'         => $geseventvenueAddress2,
'id'            => 'eventVenueAddress2',
'type'          => 'text',
'class'         => 'xlarge_input',
'autocomplete'  => 'off',
'data-parsley-error-message'    => lang('common_field_required'),
'data-parsley-error-class'      => 'custom_li',
'data-parsley-trigger'          => 'keyup',
);

$eventVenueCity = array(
'name'          => 'eventVenueCity',
'value'         => $geseventvenueCity,
'id'            => 'eventVenueCity',
'type'          => 'text',
'class'         => 'xlarge_input',
'autocomplete'  => 'off',
'data-parsley-error-message'    => lang('common_field_required'),
'data-parsley-error-class'      => 'custom_li',
'data-parsley-trigger'          => 'keyup',
);
$eventVenueState = array(
'name'          => 'eventVenueState',
'value'         => $geseventvenueState,
'id'            => 'eventVenueState',
'type'          => 'text',
'class'         => 'medium_input',
'autocomplete'  => 'off',
'data-parsley-error-message'    => lang('common_field_required'),
'data-parsley-error-class'      => 'custom_li',
'data-parsley-trigger'          => 'keyup',
);
$eventVenueZip = array(
'name'          => 'eventVenueZip',
'value'         => $geseventvenueZip,
'id'            => 'eventVenueZip',
'type'          => 'text',
'class'         => 'medium_input',
'autocomplete'  => 'off',
'data-parsley-error-message'    => lang('common_field_required'),
'data-parsley-error-class'      => 'custom_li',
'data-parsley-trigger'          => 'keyup',
);
$eventSponsorsLogos = array(
'name'          => 'eventSponsorsLogos',
'value'         => '1',
'id'            => 'eventSponsorsLogos',
'type'          => 'checkbox',
);
/* add public yes/no field */
$eventPublicYes = array(
'name'          => 'eventIsPublic',
'value'         => '1',
'id'            => 'eventPublicYes',
'type'          => 'radio',
'class'         => 'event_contact_person',
'checked'       => $eventPublicYesChk,
);  
$eventPublicNo = array(
'name'          => 'eventIsPublic',
'value'         => '2',
'id'            => 'eventPublicNo',
'type'          => 'radio',
'class'         => 'event_contact_person',
'checked'       => $eventPublicNoChk,
); 
$logo_list_div = "dn";
if (!empty($eventsponsorslogos)) { $logo_list_div = ""; $eventSponsorsLogos['checked'] = true; }
$ua             = $_SERVER["HTTP_USER_AGENT"];
$android        = strpos($ua, 'Android') ? true : false;
$blackberry     = strpos($ua, 'BlackBerry') ? true : false;
$iphone         = strpos($ua, 'iPhone') ? true : false;
$palm           = strpos($ua, 'Palm') ? true : false;
$linux          = strpos($ua, 'Linux') ? true : false;
$mac            = strpos($ua, 'Macintosh') ? true : false;
$win            = strpos($ua, 'Windows') ? true : false;
?>
<div class="panel" id="panel0">
      <div class="panel_header" onclick="displayMap()"><?php echo lang('event_event_venue'); ?></div>
      <div class="panel_contentWrapper" id="showhideformdivVenueSection">
      <div class="infobar">
        <p class="dn" id="event_venue_details_info">The Access ID# is a unique identifier assigned to every registrant using this site. Use it to access, edit, cancel, pay and rebook items in your registration.</p>
        <span class="info_btn" onclick="showHideInfoBar('event_venue_details_info');"></span>
      </div>
      	<!--sub menu one-->
        <?php echo form_open($this->uri->uri_string(), $formGeneralEventSetup); ?>
        <input type="hidden" name="collapseValue" value="collapseTwo" />
              <div class="panel_content">
                
                <div class="row">
                  <div class="col-3">
                    <div class="labelDiv">
                      <label class="form-label text-right"><?php echo lang('event_venue_address'); ?> </label>
                    </div>
                  </div>
                  <div class="col-5">
                        <?php echo form_input($eventVenueAddress1); ?>
                        <?php echo form_error('eventVenueAddress1'); ?>
                        <span id="error-dinner_dance6"></span> 
                  </div>
                </div>
                
                <div class="row">
                  <div class="col-3">
                  </div>
                  <div class="col-5">
                        <?php echo form_input($eventVenueAddress2); ?>
                        <?php echo form_error('eventVenueAddress2'); ?>
                        <span id="error-dinner_dance6"></span> 
                    </div>
                </div>
                
                <div class="row">
                  <div class="col-3">
                    <div class="labelDiv">
                      <label class="form-label text-right"><?php echo lang('event_side_venue'); ?> </label>
                    </div>
                  </div>
                  <div class="col-5">
                        <?php echo form_input($eventVenue); ?>
                        <?php echo form_error('eventVenue'); ?>
                        <span id="error-eventVenue"></span> 
                  </div>
                </div>
                
                <div class="row">
                  <div class="col-3">
                    <div class="labelDiv">
                      <label class="form-label text-right"><?php echo lang('event_venue_city'); ?> </label>
                    </div>
                  </div>
                    <div class="col-5">
                        <?php echo form_input($eventVenueCity); ?>
                        <?php echo form_error('eventVenueCity'); ?>   
                        <span id="error-dinner_dance6"></span> 
                    </div>
                </div>
                <div class="row">
                  <div class="col-3">
                    <div class="labelDiv">
                      <label class="form-label text-right"><?php echo lang('event_venue_state'); ?></label>
                    </div>
                  </div>
                  <div class="col-5">
                    <?php echo form_input($eventVenueState); ?>
                    <?php echo form_error('eventVenueState'); ?>
                    <span id="error-dinner_dance6"></span> </div>
                </div>
                <div class="row">
                  <div class="col-3">
                    <div class="labelDiv">
                      <label class="form-label text-right"><?php echo lang('event_venue_zip'); ?></label>
                    </div>
                  </div>
                  <div class="col-5">
                    <?php echo form_input($eventVenueZip); ?>
                    <?php echo form_error('eventVenueZip'); ?>
                    <span id="error-dinner_dance6"></span> </div>
                </div>
                
                <div class="row">
                    <div class="col-3"></div>
                    <div class="col-5">
                    <!-- Map canvas -->
                    <?php if($android==true || $blackberry==true || $iphone==true || $palm==true) { ?>
                    <!-- code mobile -->    
                    <span class="pull-right"><a href="javascript:void(0);" class="viewLocationOnMap" onclick="displayMap()">
                        <?php echo lang('view_location_on_map'); ?></a>
                    </span>
                    <?php } else { ?>
                    <!-- for desktop -->
                    <div id="map_canvas" style="height:180px;width: 350px;margin: 0;position: initial;float: right;left:-1px ;"></div>
                    <?php } ?>
                     <!-- Map canvas End -->
                     </div>
                </div>
                
                
              </div>
              <div class="panel_footer"> 
                 
                <div class="pull-right">
                    <?php
                        echo form_hidden('eventId', $eventId);              
                        echo form_hidden('formActionName', 'generalEventVenueSetup');
                        echo form_input($event_lat);
                        echo form_input($event_long);
                        $formButton['showbutton']   = array('save', 'reset');
                        $formButton['labletext']    = array('save' => lang('comm_save_changes'), 'reset' => lang('comm_reset'));
                        $formButton['buttonclass']  = array('save' => 'btn-normal btn', 'reset' => 'btn-normal btn reset_space reset_form');
                        $this->load->view('common_save_reset_button', $formButton);
                    ?>
                </div>
              </div>
            </form>
      </div>
</div>
<!--end of panel-->  
<div id="location_popup_map" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" >  
    <div id="map_canvas_mob" style="height: 400px;width: 100%;margin: 0; position:absolute;display:none; "></div>
    <input type="button" class="default_btn mob_btn_dismiss btn pull-right medium" value="close" name="logincancel" data-dismiss="modal">  
</div>
