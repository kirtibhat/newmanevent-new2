<?php  if (!defined('BASEPATH'))  exit('No direct script access allowed');

$eventSocialMediadetails = $eventSocialMediadetails[0];

//set field value in variable
$eventWebsite   = (!empty($eventSocialMediadetails->website))   ? $eventSocialMediadetails->website : '';
$eventFacebook  = (!empty($eventSocialMediadetails->facebook))  ? $eventSocialMediadetails->facebook : '';
$eventInstagram = (!empty($eventSocialMediadetails->instagram)) ? $eventSocialMediadetails->instagram : '';
$eventTwitter   = (!empty($eventSocialMediadetails->twitter))   ? $eventSocialMediadetails->twitter : '';
$eventYoutube   = (!empty($eventSocialMediadetails->youtube))   ? $eventSocialMediadetails->youtube : '';
$eventPintrest  = (!empty($eventSocialMediadetails->pintrest))  ? $eventSocialMediadetails->pintrest : '';

$formSocialMediaSetup = array(
    'name' => 'formSocialMediaSetup',
    'id' => 'formSocialMediaSetup',
    'method' => 'post',
    'class' => '',
    'data-parsley-validate' => ''
);

$eventWebsite = array(
    'name' => 'eventWebsite',
    'value' => $eventWebsite,
    'id' => 'eventWebsite',
    'type' => 'text',
    'placeholder' => lang('event_website'),
    'class' => 'small',
    'autocomplete' => 'off',
    'data-parsley-error-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
);

$eventFacebook = array(
    'name' => 'eventFacebook',
    'value' => $eventFacebook,
    'id' => 'eventFacebook',
    'type' => 'text',
    'placeholder' => lang('event_facebook'),
    'class' => 'small ',
    'autocomplete' => 'off',
    'data-parsley-error-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
);

$eventInstagram = array(
    'name' => 'eventInstagram',
    'value' => $eventInstagram,
    'id' => 'eventInstagram',
    'type' => 'text',
    'placeholder' => lang('event_instagram'),
    'class' => ' small ',
    'autocomplete' => 'off',
    'data-parsley-error-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
);

$eventTwitter = array(
    'name' => 'eventTwitter',
    'value' => $eventTwitter,
    'id' => 'eventTwitter',
    'type' => 'text',
    'placeholder' => lang('event_twitter'),
    'class' => ' small',
    'autocomplete' => 'off',
    'data-parsley-error-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
);

$eventYoutube = array(
    'name' => 'eventYoutube',
    'value' => $eventYoutube,
    'id' => 'eventYoutube',
    'type' => 'text',
    'placeholder' => lang('event_youtube'),
    'class' => ' small',
    'autocomplete' => 'off',
    'data-parsley-error-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
);

$eventPintrest = array(
    'name' => 'eventPintrest',
    'value' => $eventPintrest,
    'id' => 'eventPintrest',
    'type' => 'text',
    'placeholder' => lang('event_pintrest'),
    'class' => ' small',
    'autocomplete' => 'off',
    'data-parsley-error-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
);
?>
<div class="panel event_panel">
    <div class="panel-heading ">
        <h4 class="panel-title medium dt-large ">
            <a class=" " data-toggle="collapse" data-parent="#accordion" href="#collapseNine"><?php echo lang('event_social_media_setup'); ?></a>
        </h4>
    </div>
    <div style="height: auto;" id="collapseNine" class="accordion_content collapse apply_content ">
    <?php echo form_open($this->uri->uri_string(), $formSocialMediaSetup); ?>
    <div class="panel-body ls_back dashboard_panel small" id="showhideformdivSocialMedia">
        
        <div class="row-fluid-15">
            <label for="amc_con"><?php echo lang('event_website'); ?>
                <span class="info_btn"><span class="field_info xsmall"><?php echo lang('event_website_tooltip'); ?></span></span>
            </label>
            <?php echo form_input($eventWebsite); ?>
            <?php echo form_error('eventWebsite'); ?>
        </div>
        
        <div class="row-fluid-15">
            <label for="amc_con"><?php echo lang('event_facebook'); ?></label>
            <?php echo form_input($eventFacebook); ?>
            <?php echo form_error('eventFacebook'); ?>
        </div>
        
        <div class="row-fluid-15">
            <label for="amc_con"><?php echo lang('event_instagram'); ?></label>
            <?php echo form_input($eventInstagram); ?>
            <?php echo form_error('eventInstagram'); ?>
        </div>
        
        <div class="row-fluid-15">
            <label for="amc_con"><?php echo lang('event_twitter'); ?></label>
            <?php echo form_input($eventTwitter); ?>
            <?php echo form_error('eventTwitter'); ?>
        </div>
        
        <div class="row-fluid-15">
            <label for="amc_con"><?php echo lang('event_youtube'); ?></label>
            <?php echo form_input($eventYoutube); ?>
            <?php echo form_error('eventYoutube'); ?>
        </div>
        
        <div class="row-fluid-15">
            <label for="amc_con"><?php echo lang('event_pintrest'); ?></label>
            <?php echo form_input($eventPintrest); ?>
            <?php echo form_error('eventPintrest'); ?>
        </div>

        <div class="input_fields_wrap" id="eventcustomcontactlistId"> </div>

        <div class="custom_field_div" id="custom_field_div">
        <?php
        $where = array('event_id' => $eventId);
        $customSocialMediaTypes = getDataFromTabel('event_social_media_custom', '*', $where, '', 'id', 'ASC');
        if(!empty($customSocialMediaTypes)){
            foreach($customSocialMediaTypes as $customSocialMediaType){
                $customSocialMediaTypeId = $customSocialMediaType->id;
                echo form_hidden('fieldid[]', $customSocialMediaTypeId);
                ?>
                <div class="row-fluid-15"  id="masterfieldlbl_<?php echo $customSocialMediaTypeId; ?>"><label><?php echo $customSocialMediaType->media_name; ?></label><input type="text" name="mediatypevalue[]" value="<?php echo $customSocialMediaType->media_value; ?>" class="small short_field"><span class="pull-right select_edit">   <a class="eventsetup_btn td_btn event_social_media"  href="javascript:void(0)" formAction="bookerCustomFieldSave" customfieldid="<?php echo $customSocialMediaTypeId; ?>"><span>&nbsp;</span></a><a href="javascript:void(0)" class="delete_btn td_btn delete_custom_social_media" deleteid="<?php echo $customSocialMediaTypeId; ?>"><span>&nbsp;</span></a></span></div>
                <?php
            }
        }
        ?>
        
        </div>

        <div class="row-fluid-15">
            <a class="add_btn medium event_social_media" href="javascript:void(0)"  customfieldid="0" ><?php echo lang('event_add_social_media'); ?></a>
        </div>

        <div class="btn_wrapper ">
            <a href="#collapseNine" class="pull-left scroll_top"><?php echo lang('comm_top'); ?></a>
            <?php
            echo form_hidden('eventId', $eventId);
            echo form_hidden('formActionName', 'eventSocialMedia');

            //button show
            $formButton['showbutton'] = array('save', 'reset');
            $formButton['labletext'] = array('save' => lang('comm_save_changes'), 'reset' => lang('comm_reset'));
            $formButton['buttonclass'] = array('save' => 'default_btn btn pull-right medium', 'reset' => 'default_btn btn pull-right medium reset_form');
            $this->load->view('common_save_reset_button', $formButton);
            ?>
        </div>
        
    </div>
    <?php echo form_close(); ?>    
    </div>
</div>

<script type="text/javascript">
//call for event data save and media upload
ajaxdatasave('formSocialMediaSetup', '<?php echo $this->uri->uri_string(); ?>',true,true,false,false,false,'#showhideformdivSocialMedia','#showhideformdivlostPasswordSection');

/*---This function is used to add and edit custome field for personal field -----*/
$(document).on('click','.event_social_media',function(){                                                
    var formAction = $(this).attr('formAction');

    var eventId = '<?php echo $eventId ?>';
    var customfieldid = $(this).attr('customfieldid');
    var sendData = {"eventId":eventId, "customfieldid":customfieldid};

    //call ajax popup function
    ajaxpopupopen('custom_social_media','event/eventcustomsocialmediapopup ',sendData,'custom_social_media','');
});


//Save Extra Form Custome Field Via Ajax
$(document).on("click", "#saveCustomeSocialMediaType", function(e) {
    $(".accordion_content").css("height",'auto');
    
    $("#formcustomSocialMediaType").parsley();
    if($("#formcustomSocialMediaType").parsley().isValid()){    
        //var parentFormId = data.id;
        var fromData=$("#formcustomSocialMediaType").serialize();
        var url = baseUrl+'event/saveCustomeSocialMediaType';
        //console.log(fromData); console.log(url); //return false;
        $.ajax({
            type:'POST',
            data:fromData,
            url: url,
            dataType: 'json',
            async: false,
            cache: false,
            beforeSend: function( ) {                
            },
            success: function(data){
                //console.log(data);
                if(data.is_success=='true'){
                    var mediatypename  = $("#formcustomSocialMediaType #mediatypename").val();
                    var mediatypevalue = $("#formcustomSocialMediaType #mediatypevalue").val();
                    var fieldId = data.id; 
                    //console.log("mediatypename: "+mediatypename+"===mediatypevalue: "+mediatypevalue+"====fieldId: "+fieldId);
                    var fieldHtml = '<input type="hidden" name="fieldid[]" value="'+fieldId+'">';
                    fieldHtml += '<div class="row-fluid-15"  id="masterfieldlbl_'+fieldId+'"><label>'+capWords(mediatypename)+'</label><input type="text" name="mediatypevalue[]" value="'+mediatypevalue+'" class="small short_field"><span class="pull-right select_edit">   <a class="eventsetup_btn td_btn event_social_media"  href="javascript:void(0)" formAction="bookerCustomFieldSave" customfieldid="'+fieldId+'"><span>&nbsp;</span></a><a href="javascript:void(0)" class="delete_btn td_btn delete_custom_social_media" deleteid="'+fieldId+'"><span>&nbsp;</span></a></span></div>';
                    //console.log(fieldHtml);
                    //console.log(data.saveType);
                    if(data.saveType=='insert'){
                        $(".custom_field_div").append(fieldHtml);
                        $("#custom_social_media").modal('hide');
                    } else if(data.saveType=='update'){
                        $(".custom_field_div #masterfieldlbl_"+fieldId).html(fieldHtml);
                        $("#custom_social_media").modal('hide');
                    }
                }
                e.preventDefault();
                return false;
            }
        });        
    }     
});

/*---This function is used to delete custome field for personal field -----
$(document).on('click', '.delete_btn', function(){
    console.log("here");    
    var values = $("input[name='fieldid\\[\\]']").map(function(){return $(this).val();}).get(); */
    customconfirm('delete_custom_social_media','event/deleteCustomeSocialMediaType', '', '', '', true,'',false,'<?php echo lang('event_msg_custom_social_media_deleted') ?>');
/*});*/
</script>          
