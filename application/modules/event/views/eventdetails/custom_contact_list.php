<?php // get custom contact details  
 $customContactDetails = getDataFromTabel('event_custom_contact','*',array('contact_person_id'=>$contactPersionId));
 $contactPersonId = 0;
 if(!empty($customContactDetails)){
	foreach($customContactDetails as $key => $value){ 
		$contactPersonId = $value->contact_person_id;
        if(!empty($value->details)){
		?>
		
		<div class="row" id="div_custom_contactlist_<?php echo $key; ?>">
          <div class="col-3">
            <div class="labelDiv">
              <label class="form-label text-right" for="contactType"><?php echo ucfirst($value->contact_type); ?>
                <input type="hidden" name="contactType[]" value="<?php echo $value->contact_type;  ?>">
                <input type="hidden" name="hidden_deleted_custom_contact[]" value="<?php echo $value->custom_contact_id;  ?>">
              </label>
            </div>
          </div>
          <div class="col-5">
            <input  class="medium_input pull-left short_field" type="text" value="<?php echo $value->details;  ?>" name="contactDetails[]" >
               <div class="editButtonGroup pull-left">       
                    <a class="eventsetup_btn edit_contact_type btn" data-contactType="<?php echo $value->contact_type;  ?>" data-contactDetails="<?php echo $value->details;  ?>" href="javascript:void(0)">
                        <span class="medium_icon"><i class="icon-edit"></i> </span>
                    </a>
                    <a class="delete_btn deleteeventcustomcontact red btn" data-contact="<?php echo $value->custom_contact_id;  ?>" href="javascript:void(0)">
                        <span class="medium_icon"><i class="icon-delete"></i> </span>
                    </a>
                </div>
          </div>
        </div>
	<?php
        } 
    }
	
 }
?>
<div class="dn">
    <input type="hidden" name="contact_person_id" value="<?php echo $contactPersionId;  ?>">
</div>
