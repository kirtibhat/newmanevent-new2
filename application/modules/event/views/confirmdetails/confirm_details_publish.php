<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//check not empty the assign value
if(!empty($eventdata)){
	$eventdata = $eventdata[0];
}

$geseventtitle        = (!empty($eventdata->event_title))?$eventdata->event_title:'';
$IsApproved	          = (!empty($eventdata->is_approved))?$eventdata->is_approved:'';
$geseventstarttime 	  = (!empty($eventdata->starttime))?dateFormate($eventdata->starttime, "d M Y"):'';
$geseventendtime      = (!empty($eventdata->endtime))?dateFormate($eventdata->endtime, "d M Y"):'';

$eventUrlId = encode($eventId);
$registrationURLValue = base_url('frontend/index/'.$eventUrlId);


?>
		
	<div class="commonform_bg">
		<div class="headingbg bg_807f83">
			<?php echo lang('event_conf_confirm_details'); ?>
		</div>
			<div class="form-horizontal form_open_chk" >
				<div class="control-group mb10">
					<label for="inputEmail" class="control-label"><?php echo lang('event_conf_title'); ?></label>
					<div class="controls mt_5px">
						<?php echo $geseventtitle; ?>
					</div>
				</div>
				<div class="control-group mb10">
					<label for="inputEmail" class="control-label"><?php echo lang('event_conf_start_date'); ?></label>
					<div class="controls mt_5px">
						<?php echo $geseventstarttime; ?>
					</div>
				</div>
				<div class="control-group mb10">
					<label for="inputEmail" class="control-label"><?php echo lang('event_conf_end_date'); ?></label>
					<div class="controls mt_5px ">
						<?php echo $geseventendtime; ?>
					</div>
				</div>
				<div class="control-group mb10">
					<label for="inputEmail" class="control-label"><?php echo lang('event_conf_event_Status'); ?></label>
					<div class="controls mt_5px event_status" >
						<?php echo ($IsApproved==1)?lang('event_conf_publish'):lang('event_conf_unpublish'); ?>
					</div>
				</div>
				
				<div class="control-group mb10">
					<label for="inputEmail" class="control-label"><?php echo lang('event_conf_change_status'); ?></label>
					<div class="controls">
						<input type="submit" name="change_status"  value="<?php echo lang('event_conf_publish'); ?>" class="submitbtn_cus bg_807f83 fl change_status">
						<input type="submit" name="change_status"  value="<?php echo lang('event_conf_unpublish'); ?>" class="submitbtn_cus bg_807f83 fl change_status">
					</div>
				</div>
				
				<div class="control-group mb10">
					<label for="inputEmail" class="control-label"><?php echo lang('event_conf_url'); ?></label>
					<div class="controls">
						<input type="text" name="eventName" value="<?php echo $registrationURLValue; ?>" id="callback-paragraph" placeholder="Event Name" readonly>
					</div>
				</div>
				<div class="row-fluid">
					<input type="submit" name="save" id="copy-callbacks"  value="Copy" class="submitbtn_cus bg_807f83">
				</div>
			</div>
	</div>
		


<script src="<?php echo base_url('templates/system/js/zclip/zclip.min.js') ?>" type="text/javascript"></script>
<script src="<?php echo base_url('templates/system/js/zclip/jquery.zclip.js') ?>" type="text/javascript"></script>
<script>
$(document).ready(function(){

	$("#copy-callbacks").zclip({
		path:'<?php echo base_url('templates/system/js/zclip/zeroClipboard.swf') ?>',
		copy:$('#callback-paragraph').val(),
		beforeCopy:function(){
	  
		},
		afterCopy:function(){
		   custom_popup('Event url copied.',true)
		}
	});
});
var isFieldChk = false;
$(document).on('click','.change_status',function(){
		
		var eventId = '<?php echo $eventId ?>';
		var sendData = { "eventId":eventId};
		
		
		var url = baseUrl+'event/confirmmsg';
		$.post(url, sendData, function(data){	
			if(data!='done'){
			
				$("#ajax_open_popup_html").html(data);	
				
				open_model_popup('confirm_message_popup');	
			
				//scroll bar in modal	
				$('.modelbodyscroll').perfectScrollbar({
					wheelSpeed: 20,
					wheelPropagation: false
				});
			
				isFieldChk = false;
			}else{
				if(isFieldChk==false){
					custom_popup('Your filled data verified successfully. Now you can publish or unpublish it.',true);
				}
				isFieldChk = true;
			}
		});
		
		if(isFieldChk==false){
			return false;
		}
		
		var getvalue = $(this).val();
		var statusValue = 0;
		
		if(getvalue=='Publish'){
			statusValue = 1;
			//condition for confirm button
		}else{
			statusValue = 0;
			//condition for cancel button
		}
		
		//confirm poup code
		bootbox.confirm({
			title: 'Confirm',
			message: 'Do you really want to perform this action?',
			buttons: {
				'cancel': {
					label: 'Cancel',
					className: 'btn-default custombtn'
				},
				'confirm': {
					label: 'Ok',
					className: 'btn-danger custombtn bg_807f83'
				}
			},
			callback: function(result) {
				if (result) {
						//condition for confirm button
						var url = baseUrl+'event/confirmstatus';
						var fromData = {'statusValue':statusValue,'eventId':'<?php echo $eventId; ?>' };
						$.ajax({
						  type:'POST',
						  data:fromData,
						  url: url,
						  dataType: 'json',
						  cache: false,
						  beforeSend: function( ) {
								//open loader
								loaderopen();
							},
						  success: function(data){	
								//check data 
								if(data){
									loaderclose();
									custom_popup(data.msg,true);
									$('.event_status').html(getvalue);
								}
							},
							error: function(XMLHttpRequest, textStatus, errorThrown) {
								//hide loader
								loaderclose();
								custom_popup('Request failed.',false);
							}
						});
					
				}
			}
		}).find("div.modal-header").addClass('bg_807f83');
		
	});
</script>	
