<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>

<?php $this->load->view('event_menus');  ?>


<div class="row-fluid mt20">
	<div class="span7">
		
<?php $this->load->view('confirm_details_edit');  ?>

<?php $this->load->view('confirm_details_publish');  ?>

</div>
	
	<!----Right side advertisement view load start------>
		<?php $this->load->view('right_side_advert'); ?>
	<!----Right side advertisement view load end------>
	
</div>
