<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>
<?php $this->load->view('event_menus_free');  ?>
<div class="page_content">
    <div class="container">
      <!-- Start item row -->
      <div class="row">
        <div class="col-9">
        <!-- Start panel Event Details -->  
        <?php echo $this->load->view('confirm_event_details'); ?>
        <!-- End Event Details panel -->
        
        <!-- Start panel Invitation -->  
        <?php echo $this->load->view('confirm_event_invitations'); ?>
        <!-- End invitations panel -->
        
        <!-- Start event theme panel -->  
        <?php echo $this->load->view('confirm_event_theme'); ?>
        <!-- End theme panel -->
        
        <!-- Start panel preview link -->
        <?php echo $this->load->view('confirm_event_preview_links'); ?>
        <!-- End preview link -->
       
        <!-- Start panel terms&conditions --> 
        <?php echo $this->load->view('confirm_event_terms'); ?>
        
        <!-- End terms&conditions link -->
        
        <!-- Success & Error box panel -->
        <?php $this->load->view('confirm_message_popup'); ?>
        <!-- End panel -->
    
        <!-- Event Publish Action -->
        <div class="clearFix display_inline w_100 mT10"></div>	
        <input type="hidden" name="event_id" id="event_id" value="<?php echo $eventId; ?>">
        <input type="hidden" name="event_title" id="event_title" value="<?php echo $eventdetails->event_title; ?>">
        <div class="col-9">
            <div class="btn-group pull-right">
                <?php if($eventdetails->is_approved!=1) { ?>
                <button class="btn-nav prevI btn publish_event_check">Launch</button>
                <?php }else { ?>

                <a class="esc_btn_press btn-normal btn" href="<?php echo base_url().'events/invitation/'. $eventdetails->event_url; ?>">View Page</a>
                
                <?php if($eventdetails->is_cancelled!=1) { ?>    
                <a class="esc_btn_press btn-normal btn close_msg_popup invite_people" href="<?php echo base_url().'events/addEmailInvitation/'.$eventId; ?>">Invite People</a>    
                <a class="esc_btn_press btn-normal btn cancelEventAction" href="javascript:void(0);">Cancel Event</a>
                <?php } ?>
                <?php } ?>    
            </div>
        </div>
    </div>
  </div>
  <!-- End row class -->
</div>
</div>

<!-- Invitation popup display after publish event-->
<a class="show_invitation" data-toggle="modal" data-target=".invitation_popup"></a>
<div class="modal fade invitation_popup">	
  <div class="modal-dialog modal-sm-as">
	<div class="modal-content">
	  <div class="modal-header managers_color">        
		<h4 class="modal-title">EVENT INVITATION</h4>
	  </div>
	  <div class="modal-body">
		<div class="modal-body-content ">
			<label class="form-label success_message_show"></label>
<!--
            <center><h1 class="form-label font-xlarge publishMessage"></h1></center><br/>
            <center><h1 class="form-label font-xlarge">Invite People To Your Event!</h1></center>
            <center><p class="form-label">Sending email invitation is the best way to tell people about your event</p></center>
-->
           <div class="invitationbtn">
                <a class="esc_btn_press btn-normal btn close_msg_popup" href="<?php echo base_url().'events/invitation/'. $eventdetails->event_url; ?>">View Page</a>
                <span> OR </span>
                <a class="esc_btn_press btn-normal btn close_msg_popup" href="<?php echo base_url().'events/addEmailInvitation/'.$eventId; ?>">Invite</a>
           </div>
            
		</div>
		<div class="modal-footer">
			<div class="pull-right">
				<button class="btn-normal btn popup_cancel" data-dismiss="modal"><div class="btn_center">Close</div></button>
			</div>         	
		</div>     
	  </div>               
	</div>
  </div>
</div>


<a class="cancelledpopup" data-toggle="modal" data-target=".cancelled_popup"></a>
<div class="modal fade cancelled_popup">	
  <div class="modal-dialog modal-sm-as">
	<div class="modal-content">
	  <div class="modal-header managers_color">        
		<h4 class="modal-title">Success</h4>
	  </div>
	  <div class="modal-body">
		<div class="modal-body-content ">
			<label class="form-label success_message_show"></label>
            <center><h1 class="form-label font-xlarge cancelMessage"></h1></center><br/>
		</div>
		<div class="modal-footer">
			<div class="pull-right">
				<button class="btn-normal btn popup_cancel" data-dismiss="modal"><div class="btn_center">Close</div></button>
			</div>         	
		</div>     
	  </div>               
	</div>
  </div>
</div>

