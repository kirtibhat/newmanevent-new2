<div class="panel" id="panel0">
    <div class="panel_header">Terms & Conditions
    <?php if($eventdetails->is_approved!=1) { ?>
        <button class="btn-red btn pull-right complete incompletebtn">Incomplete</button>
        <button class="btn-normalComplete btn pull-right dn completebtn">Complete</button>
        <input type="hidden" id="termscomplete" value="0">
    <?php }else { ?>
          <input type="hidden" id="termscomplete" value="1">
         <button class="btn-normalComplete btn pull-right complete">Complete</button>
    <?php }  ?>
    
    </div>
    <div class="panel_contentWrapper">
        <div class="infobar">
        <p class="dn" id="event_terms_info">The Access ID# is a unique identifier assigned to every registrant using this site. Use it to access, edit, cancel, pay and rebook items in your registration.</p>
        <span class="info_btn" onclick="showHideInfoBar('event_terms_info');"></span>
      </div>
  
    <div class="panel_content">
        <input type="checkbox" id="confirm_terms_check" name="confirm_terms_check" class="checkBox" <?php echo ($eventdetails->is_approved==1) ? 'checked' : ''; ?>>
        <label class="form-label" for="confirm_terms_check"><span>I have read and agree with the <a class="link_terms" data-toggle="modal" data-target=".terms_popup">Terms & Conditions </a></span></label>        
                    
    </div>
  </div>
</div>
<!---Term Popup-->
<div class="modal fade terms_popup ">
	<div class="modal-dialog modal-sm-as">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Terms & Condition</h4>
			</div>
			
			<div class="infobar">
				<p class="info_Status"></p>
				<a class="info_btn" onclick="return false;">
				</a>
			</div>
			<div class="modal-body">
				<div class="modal-body-content">
				<div class="clearFix display_inline w_100 mT10"></div>
					<div class="row">	
					<?php echo lang('term_condition_text');?>
					</div>
				</div>
				<div class="modal-footer">
					<div class="pull-right">
						<button class="btn-normal btn accept_event_terms" tabindex='4' data-dismiss="modal">Accept</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
