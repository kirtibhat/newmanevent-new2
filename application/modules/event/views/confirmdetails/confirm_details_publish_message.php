<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

	<div class="modal fade alertmodelbg dn" id="confirm_message_popup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg_807f83">
				<h4 class="modal-title">Summary</h4>
			</div>
				<div class="modal-body ">
				<h4>Below remainings fields need to fill before publishing.</h4>
				<div class="control-group mb10 min_height_75">
					<?php if(!empty($error_msg)){ 
					foreach($error_msg as $msg){
					?>
					<div class="controls ft_14 red_img">
						<?php echo $msg; ?> Form must be fill mandatory fields.
					</div>
					<?php } } ?>
				</div>
			</div>
			<div class="modal-footer">
				<?php 
					$extraCancel 	= 'class="btn btn-default custombtn" data-dismiss="modal" ';
					echo form_button('cancel',lang('comm_ok'),$extraCancel);
				?>
			</div>
		</div>
	
	</div>
</div>
