<div class="panel" id="panel0">
  <div class="panel_header">
    Customise Invitation Field Check
    <?php 
        $themeUsedStatus = 0 ;
        if(empty($eventthemes)){ ?>
                <button class="btn-red btn pull-right complete">Incomplete</button>
        <?php }else { 
            if(!empty($eventthemes)) { 
                foreach($eventthemes as $themelist) {
                    if($themelist->is_theme_used!=0){
                        $themeUsedStatus = 1;
                    }
                }  
            }
         ?>
    
    <?php if(!$themeUsedStatus) { ?>
            <button class="btn-red btn pull-right complete">Incomplete</button>
    <?php }else { ?>
         <button class="btn-normalComplete btn pull-right complete">Complete</button>
    <?php } } ?>
        
  </div>
  <div class="panel_contentWrapper">
  <div class="infobar">
        <p class="dn" id="event_theme_info">The Access ID# is a unique identifier assigned to every registrant using this site. Use it to access, edit, cancel, pay and rebook items in your registration.</p>
        <span class="info_btn" onclick="showHideInfoBar('event_theme_info');"></span>
      </div>
  
    <!-- Start Sub panel 1 -->
    <div class="sabPanel" id="sabPanel0">
      <div class="withcomplete sabPanel_header"> 
      <span>Create Your Invitation</span>
        <?php 
        $themeUsedStatus = 0 ;
        if(empty($eventthemes)){ ?>
                <button class="btn-red btn pull-right complete">Incomplete</button>
                <input type="hidden" name="" id="event_theme_mandatory_status" value="0">
        <?php }else { 
            if(!empty($eventthemes)) { 
                
                foreach($eventthemes as $themelist) {
                    if($themelist->is_theme_used!=0){
                        $themeUsedStatus = 1;
                    }
                }  
            }
         ?>
        <?php if($themeUsedStatus) { ?>
        <button class="btn-normalComplete btn pull-right complete">Complete</button>
        <input type="hidden" name="" id="event_theme_mandatory_status" value="1">
        <?php }else { ?>
        <button class="btn-red btn pull-right complete">Incomplete</button>
        <input type="hidden" name="" id="event_theme_mandatory_status" value="0">
        <?php } 
        } ?>
        
      </div>
      <div class="sabPanel_contentWrapper">
      <div class="panel_content">
        <?php if(empty($eventthemes)){ ?>
            <div class="row">
              <div class="col-8">
                <div class="labelDiv">
                    <label class="form-label text-right">Create Event Theme 
                        <a class="addEmptyValue" href="<?php echo base_url('event/customizeforms'); ?>">
                            <span class="medium_icon"> <i class="icon-edit"></i> </span>
                        </a> 
                    </label>   
                </div>
              </div>
            </div>
        <?php }else { ?>    

        <?php if($themeUsedStatus) { ?>
            <div class="row">
              <div class="col-8">
                <div class="labelDiv pull-left">
                  <label class="form-label text-right">
                      All mandatory fields have been completed. 
                  </label> 
                </div>
              </div>
            </div>
        <?php }else {  ?>
           <div class="row">
              <div class="col-8">
                <div class="labelDiv pull-left">
                  <label class="form-label text-right">
                      You have completed "Customise Invitation" but you did not use theme for event.
                  </label> 
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-8">
                <div class="labelDiv">
                    <label class="form-label text-right">Edit Invitation
                        <a class="addEmptyValue" href="<?php echo base_url('event/customizeforms'); ?>">
                            <span class="medium_icon"> <i class="icon-edit"></i> </span>
                        </a> 
                    </label>   
                </div>
              </div>
            </div>
          <?php } 
          } ?>    
      </div>             
      </div>
    </div>
    <!-- End sub panel -->
  </div>
  </div>

