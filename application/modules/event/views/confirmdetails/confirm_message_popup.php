<!-- Error popup -->
<a class="show_confirm_error" data-toggle="modal" data-target=".danger_popup"></a>
<div class="modal fade danger_popup">	
  <div class="modal-dialog modal-sm-as">
	<div class="modal-content">
	  <div class="modal-header danger_color">        
		<h4 class="modal-title">Error</h4>
	  </div>
	  <div class="modal-body">
		<div class="modal-body-content ">
			<label class="form-label error_message_show"></label>
		</div>
		<div class="modal-footer">
			<div class="pull-right">
				<a class="esc_btn_press btn-normal btn close_msg_popup"  data-dismiss="modal">Close</a>  
			</div>         	
		</div>     
	  </div>               
	</div>
  </div>
</div>

<!-- Success popup -->
<a class="show_confirm_success" data-toggle="modal" data-target=".success_popup"></a>
<div class="modal fade success_popup">	
  <div class="modal-dialog modal-sm-as">
	<div class="modal-content">
	  <div class="modal-header managers_color">        
		<h4 class="modal-title">Success</h4>
	  </div>
	  <div class="modal-body">
		<div class="modal-body-content ">
			<label class="form-label success_message_show"></label>
		</div>
		<div class="modal-footer">
			<div class="pull-right">
				<a class="esc_btn_press btn-normal btn close_msg_popup"  data-dismiss="modal">Close</a>  
			</div>         	
		</div>     
	  </div>               
	</div>
  </div>
</div>
