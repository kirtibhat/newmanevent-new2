<div class="panel" id="panel0">
  <div class="panel_header">
        Invitations Field Check
        <?php if(empty($eventinvitations)){ ?>
        <button class="btn-red btn pull-right complete">Incomplete</button>
        <?php } ?> 
        <?php if(!empty($eventinvitations)) { 
        $eventinvitationfirst = $eventinvitations[0];
        if(empty($eventinvitationfirst->invitation_limit)){ ?>
        <button class="btn-red btn pull-right complete">Incomplete</button>    
        <?php }else {  ?>
            
        <button class="btn-normalComplete btn pull-right complete">Complete</button>    
        <?php } } ?>    
  </div>
  <div class="panel_contentWrapper">
  <div class="infobar">
        <p class="dn" id="event_invitation_info">The Access ID# is a unique identifier assigned to every registrant using this site. Use it to access, edit, cancel, pay and rebook items in your registration.</p>
        <span class="info_btn" onclick="showHideInfoBar('event_invitation_info');"></span>
      </div>
    <!-- Start Sub panel 1 -->
    <div class="sabPanel" id="sabPanel0">
      <div class="withcomplete sabPanel_header"> 
      <span>Invitation Type</span>
        <?php if(empty($eventinvitations)){ ?>
        <button class="btn-red btn pull-right complete">Incomplete</button>
        <input type="hidden" name="" id="event_invitations_mandatory_status" value="0">
        <?php } if(!empty($eventinvitations)) { 
        $eventinvitationfirst = $eventinvitations[0];
        if(empty($eventinvitationfirst->invitation_limit)){ ?>
        <button class="btn-red btn pull-right complete">Incomplete</button>     
        <input type="hidden" name="" id="event_invitations_mandatory_status" value="0">
        <?php } else { ?>
        <button class="btn-normalComplete btn pull-right complete">Complete</button>
        <input type="hidden" name="" id="event_invitations_mandatory_status" value="1">
        <?php } } ?>   
      </div>
      <div class="sabPanel_contentWrapper">
      <div class="panel_content">
        <?php if(empty($eventinvitations)){ ?>
            <div class="row">
              <div class="col-8">
                <div class="labelDiv">
                    <label class="form-label text-right">Create Invitation 
                        <a href="<?php echo base_url('event/invitations').'/?req=nOinV'; ?>">
                            <span class="medium_icon"> <i class="icon-edit"></i> </span>
                        </a> 
                    </label>   
                </div>
              </div>
            </div>
        <?php } if(!empty($eventinvitations)) { 
        $eventinvitationfirst = $eventinvitations[0];
        if(empty($eventinvitationfirst->invitation_limit)){ ?>
            <div class="row">
              <div class="col-8">
                <div class="labelDiv">
                    <label class="form-label text-right">Invitation limit required 
                        <a class="addEmptyValue" href="<?php echo base_url('event/invitations').'/?req=ilMt'; ?>">
                            <span class="medium_icon"> <i class="icon-edit"></i> </span> 
                        </a>
                    </label>   
                </div>
              </div>
            </div>     

        <?php } else { ?>
            <div class="row">
              <div class="col-8">
                <div class="labelDiv pull-left">
                  <label class="form-label text-right">
                      All mandatory fields have been completed. 
                  </label> 
                </div>
              </div>
            </div>
        <?php } } ?> 
      </div>             
      </div>
    </div>
    <!-- End sub panel -->
  </div>
  </div>
