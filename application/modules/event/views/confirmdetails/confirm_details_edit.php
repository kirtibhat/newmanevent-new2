<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>
	<div class="commonform_bg pb0 bg_807F83">
		<ul class="nav nav-tabs" id="myTab">
			<li class="active"><a href="#GENERAL" data-toggle="tab">GENERAL</a></li>
			<li><a href="#PRICING" data-toggle="tab">PRICING</a></li>
			<li><a href="#CORRESPONDENCE" data-toggle="tab">CORRESPONDENCE</a></li>
		</ul>
		<div class="tab-content custtabbg">
			<div class="tab-pane active" id="GENERAL">
				<div id="FastWheelSpeed" class="TablecontentHolder">
					<div class="content">
						<table class="tablecuston_scroll">
						<thead>
						<tr class="firstrow">
							<td>
								&nbsp;
							</td>
							<td>
								Payment Options
							</td>
							<td>
								Registration Limits
							</td>
							<td>
								Personal Details
							</td>
							<td>
								Registration Dates
							</td>
							<td>
								Custom Fields
							</td>
						</tr>
						</thead>
						<tbody>
						<tr>
							<td>
								Default
							</td>
							<td>
								<a href="#" data-target="#myModal2" data-toggle="modal"><img src="<?php echo IMAGE; ?>editicon_enfdetail.png"></a>
							</td>
							<td>
								<a href="#"><img src="<?php echo IMAGE; ?>editicon_enfdetail.png"></a>
							</td>
							<td>
								<a href="#"><img src="<?php echo IMAGE; ?>editicon_enfdetail.png"></a>
							</td>
							<td>
								<a href="#"><img src="<?php echo IMAGE; ?>editicon_enfdetail.png"></a>
							</td>
							<td>
								<a href="#"><img src="<?php echo IMAGE; ?>editicon_enfdetail.png"></a>
							</td>
						</tr>
						<tr>
							<td>
								Apply Default to ALL
							</td>
							<td>
								<div class="checkradiobg width_auto">
									<div class="checkbox position_R cnfdetail">
										<input type="checkbox" class="pull-left ml10 mr15 checkbox" id="check20" name="ex1">
										<label class="" for="check20">&nbsp;</label>
									</div>
								</div>
							</td>
							<td>
								<div class="checkradiobg width_auto">
									<div class="checkbox position_R cnfdetail">
										<input type="checkbox" class="pull-left ml10 mr15 checkbox" id="check21" name="ex1">
										<label class="" for="check21">&nbsp;</label>
									</div>
								</div>
							</td>
							<td>
								<div class="checkradiobg width_auto">
									<div class="checkbox position_R cnfdetail">
										<input type="checkbox" class="pull-left ml10 mr15 checkbox" id="check22" name="ex1">
										<label class="" for="check22">&nbsp;</label>
									</div>
								</div>
							</td>
							<td>
								<div class="checkradiobg width_auto">
									<div class="checkbox position_R cnfdetail">
										<input type="checkbox" class="pull-left ml10 mr15 checkbox" id="check23" name="ex1">
										<label class="" for="check23">&nbsp;</label>
									</div>
								</div>
							</td>
							<td>
								<div class="checkradiobg width_auto">
									<div class="checkbox position_R cnfdetail">
										<input type="checkbox" class="pull-left ml10 mr15 checkbox" id="check24" name="ex1">
										<label class="" for="check24">&nbsp;</label>
									</div>
								</div>
							</td>
						</tr>
						<tr class="customtow_n">
							<td colspan="6">
								Registrant Type
							</td>
						</tr>
						
						<?php
							if(!empty($eventregistrants)){
								
								//check not empty the assign value
								if(!empty($eventdata)){
									$eventdata = $eventdata[0];
								}
								
								$countRegistrant = count($eventregistrants);
								$countRow = 0;
								foreach($eventregistrants as $registrants){ 
									$countRow ++;
									$regisName  = $registrants->registrant_name;
									$regisLimit = (!empty($registrants->registrants_limit))?$registrants->registrants_limit:'0';
									$regisDate  = dateFormate($eventdata->starttime, "d/m/y");
									
					    		?>
							<tr <?php echo($countRegistrant==$countRow)?'class="lastwow"':''; ?> >
								<td>
									<?php echo $regisName; ?>
								</td>
								<td>
									<a href="#"><img src="<?php echo IMAGE; ?>editicon_enfdetail.png"></a>
								</td>
								<td>
									<?php echo $regisLimit; ?><a href="#"><img src="<?php echo IMAGE; ?>editicon_enfdetail.png"></a>
								</td>
								<td>
									<a href="#"><img src="<?php echo IMAGE; ?>editicon_enfdetail.png"></a>
								</td>
								<td>
									<?php echo $regisDate; ?><a href="#"><img src="<?php echo IMAGE; ?>editicon_enfdetail.png"></a>
								</td>
								<td>
									<a href="#"><img src="<?php echo IMAGE; ?>editicon_enfdetail.png"></a>
								</td>
							</tr>
						<?php
								}	
							}
							
						?>
					
						
						</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="tab-pane" id="PRICING">
				<div id="FastWheelSpeed_1" class="TablecontentHolder">
					<div class="content">
						<table class="tablecuston_scroll">
						<thead>
						<tr class="firstrow">
							<td>
								&nbsp;
							</td>
							<td>
								Full Registration
							</td>
							<td>
								Day  Registration
							</td>
							<td>
								Early Bird  Registration
							</td>
							<td>
								Side Events
							</td>
							<td>
								Group   Bookings
							</td>
						</tr>
						</thead>
						<tbody>
						<tr>
							<td>
								Default
							</td>
							<td>
								<a href="#" data-target="#myModal2" data-toggle="modal"><img src="<?php echo IMAGE; ?>editicon_enfdetail.png"></a>
							</td>
							<td>
								<a href="#"><img src="<?php echo IMAGE; ?>editicon_enfdetail.png"></a>
							</td>
							<td>
								<a href="#"><img src="<?php echo IMAGE; ?>editicon_enfdetail.png"></a>
							</td>
							<td>
								<a href="#"><img src="<?php echo IMAGE; ?>editicon_enfdetail.png"></a>
							</td>
							<td>
								<a href="#"><img src="<?php echo IMAGE; ?>editicon_enfdetail.png"></a>
							</td>
						</tr>
						<tr>
							<td>
								Apply Default to ALL
							</td>
							<td>
								<div class="checkradiobg width_auto">
									<div class="checkbox position_R cnfdetail">
										<input type="checkbox" class="pull-left ml10 mr15 checkbox" id="check20" name="ex1">
										<label class="" for="check20">&nbsp;</label>
									</div>
								</div>
							</td>
							<td>
								<div class="checkradiobg width_auto">
									<div class="checkbox position_R cnfdetail">
										<input type="checkbox" class="pull-left ml10 mr15 checkbox" id="check21" name="ex1">
										<label class="" for="check21">&nbsp;</label>
									</div>
								</div>
							</td>
							<td>
								<div class="checkradiobg width_auto">
									<div class="checkbox position_R cnfdetail">
										<input type="checkbox" class="pull-left ml10 mr15 checkbox" id="check22" name="ex1">
										<label class="" for="check22">&nbsp;</label>
									</div>
								</div>
							</td>
							<td>
								<div class="checkradiobg width_auto">
									<div class="checkbox position_R cnfdetail">
										<input type="checkbox" class="pull-left ml10 mr15 checkbox" id="check23" name="ex1">
										<label class="" for="check23">&nbsp;</label>
									</div>
								</div>
							</td>
							<td>
								<div class="checkradiobg width_auto">
									<div class="checkbox position_R cnfdetail">
										<input type="checkbox" class="pull-left ml10 mr15 checkbox" id="check24" name="ex1">
										<label class="" for="check24">&nbsp;</label>
									</div>
								</div>
							</td>
						</tr>
						<tr class="customtow_n">
							<td colspan="6">
								Registrant Type
							</td>
							
							<?php
							if(!empty($eventregistrants)){
								
								$countRegistrant = count($eventregistrants);
								$countRow = 0;
								foreach($eventregistrants as $registrants){ 
									$countRow++;
									$regisName  = $registrants->registrant_name;
									$regisPrice = (!empty($registrants->total_price))?$registrants->total_price:'0';
									$regisEarlyPrice = (!empty($registrants->total_price_early_bird))?$registrants->total_price_early_bird:'0';
									
					    		?>
							<tr <?php echo($countRegistrant==$countRow)?'class="lastwow"':''; ?>>
								<td>
									<?php echo $regisName; ?>
								</td>
								<td>
									$ <?php echo number_format($regisPrice,2); ?><a href="#"><img src="<?php echo IMAGE; ?>editicon_enfdetail.png"></a>
								</td>
								<td>
									<a href="#"><img src="<?php echo IMAGE; ?>editicon_enfdetail.png"></a>
								</td>
								<td>
									$ <?php echo number_format($regisEarlyPrice,2); ?><a href="#"><img src="<?php echo IMAGE; ?>editicon_enfdetail.png"></a>
								</td>
								<td>
									<a href="#"><img src="<?php echo IMAGE; ?>editicon_enfdetail.png"></a>
								</td>
								<td>
									<a href="#"><img src="<?php echo IMAGE; ?>editicon_enfdetail.png"></a>
								</td>
							</tr>
						<?php
								}	
							}
							
						?>
							
						
						</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="tab-pane" id="CORRESPONDENCE">
				<div id="FastWheelSpeed_2" class="TablecontentHolder">
					<div class="content">
						<table class="tablecuston_scroll">
						<thead>
						<tr class="firstrow">
							<td>
								&nbsp;
							</td>
							<td>
								Form Banners
							</td>
							<td>
								Home <br> Information
							</td>
							<td>
								Invoice
							</td>
							<td>
								Event Terms <br> & Conditions
							</td>
							<td>
								Booking Confirmation
							</td>
						</tr>
						</thead>
						<tbody>
						<tr>
							<td>
								Default
							</td>
							<td>
								<a href="#" data-target="#myModal2" data-toggle="modal"><img src="<?php echo IMAGE; ?>editicon_enfdetail.png"></a>
							</td>
							<td>
								<a href="#"><img src="<?php echo IMAGE; ?>editicon_enfdetail.png"></a>
							</td>
							<td>
								<a href="#"><img src="<?php echo IMAGE; ?>editicon_enfdetail.png"></a>
							</td>
							<td>
								<a href="#"><img src="<?php echo IMAGE; ?>editicon_enfdetail.png"></a>
							</td>
							<td>
								<a href="#"><img src="<?php echo IMAGE; ?>editicon_enfdetail.png"></a>
							</td>
						</tr>
						<tr>
							<td>
								Apply Default to ALL
							</td>
							<td>
								<div class="checkradiobg width_auto">
									<div class="checkbox position_R cnfdetail">
										<input type="checkbox" class="pull-left ml10 mr15 checkbox" id="check20" name="ex1">
										<label class="" for="check20">&nbsp;</label>
									</div>
								</div>
							</td>
							<td>
								<div class="checkradiobg width_auto">
									<div class="checkbox position_R cnfdetail">
										<input type="checkbox" class="pull-left ml10 mr15 checkbox" id="check21" name="ex1">
										<label class="" for="check21">&nbsp;</label>
									</div>
								</div>
							</td>
							<td>
								<div class="checkradiobg width_auto">
									<div class="checkbox position_R cnfdetail">
										<input type="checkbox" class="pull-left ml10 mr15 checkbox" id="check22" name="ex1">
										<label class="" for="check22">&nbsp;</label>
									</div>
								</div>
							</td>
							<td>
								<div class="checkradiobg width_auto">
									<div class="checkbox position_R cnfdetail">
										<input type="checkbox" class="pull-left ml10 mr15 checkbox" id="check23" name="ex1">
										<label class="" for="check23">&nbsp;</label>
									</div>
								</div>
							</td>
							<td>
								<div class="checkradiobg width_auto">
									<div class="checkbox position_R cnfdetail">
										<input type="checkbox" class="pull-left ml10 mr15 checkbox" id="check24" name="ex1">
										<label class="" for="check24">&nbsp;</label>
									</div>
								</div>
							</td>
						</tr>
						<tr class="customtow_n">
							<td colspan="6">
								Registrant Type
							</td>
						</tr>
							<?php
								if(!empty($eventregistrants)){
									
									$countRegistrant = count($eventregistrants);
									$countRow = 0;
									foreach($eventregistrants as $registrants){ 
										$countRow++;
										$regisName  = $registrants->registrant_name;
									?>
								<tr <?php echo($countRegistrant==$countRow)?'class="lastwow"':''; ?>>
									<td>
										<?php echo $regisName; ?>
									</td>
									<td>
										<a href="#"><img src="<?php echo IMAGE; ?>editicon_enfdetail.png"></a>
									</td>
									<td>
										<a href="#"><img src="<?php echo IMAGE; ?>editicon_enfdetail.png"></a>
									</td>
									<td>
										<a href="#"><img src="<?php echo IMAGE; ?>editicon_enfdetail.png"></a>
									</td>
									<td>
										<a href="#"><img src="<?php echo IMAGE; ?>editicon_enfdetail.png"></a>
									</td>
									<td>
										<a href="#"><img src="<?php echo IMAGE; ?>editicon_enfdetail.png"></a>
									</td>
								</tr>
							<?php
									}	
								}
								
							?>	
						</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- /commonform_bg -->
