<div class="panel" id="panel0">
<div class="panel_header">
    Event Details Field Check
    <?php if(!empty($requiredFields) || !empty($requiredFieldsContact)){ ?>
        <button class="btn-red btn pull-right complete">Incomplete</button>
    <?php }else { ?>
        <button class="btn-normalComplete btn pull-right complete">Complete</button>
     <?php } ?>   
    </div>
    <div class="panel_contentWrapper">
      <div class="infobar">
        <p class="dn" id="confirm_event_details_info">The Access ID# is a unique identifier assigned to every registrant using this site. Use it to access, edit, cancel, pay and rebook items in your registration.</p>
        <span class="info_btn" onclick="showHideInfoBar('confirm_event_details_info');"></span>
      </div>
        <!-- Start Sub panel 1 -->
        <div class="sabPanel" id="sabPanel0">
          <div class="withcomplete sabPanel_header"> 
          <span>General Event Setup</span>
          <?php if(!empty($requiredFields)){ ?>
                <button class="btn-red btn pull-right complete">Incomplete</button>
                <input type="hidden" name="" id="event_details_mandatory_status" value="0">
          <?php }else { ?>
                <button class="btn-normalComplete btn pull-right complete">Complete</button>
                <input type="hidden" name="" id="event_details_mandatory_status" value="1">
          <?php } ?>    
          </div>
          <div class="sabPanel_contentWrapper">
          <div class="panel_content">
            
            <?php
            if($requiredFields) { 
            foreach($requiredFields as $key => $fieldname){ ?>
            <div class="row">
              <div class="col-8">
                <div class="labelDiv">
                  <label class="form-label text-right"><?php echo $fieldname.' is required field '; ?> 
                    <a class="addEmptyValue" href="<?php echo base_url('event/eventdetails').'/?req=1'; ?>">
                        <span class="medium_icon"> <i class="icon-edit"></i> </span> 
                    </a>    
                  </label> 
                </div>
              </div>
            </div>
            <?php } 
            } ?> 
            
            <?php if(empty($requiredFields)){ ?>
            <div class="row">
              <div class="col-8">
                <div class="labelDiv pull-left">
                  <label class="form-label text-right">
                      All mandatory fields have been completed. 
                  </label> 
                </div>
              </div>
            </div>
            <?php } ?>     
          </div>             
          </div>
        </div>
        <!-- End sub panel -->
        
        
         <!-- Start Sub panel 1 -->
        <div class="sabPanel" id="sabPanel1">
          <div class="withcomplete sabPanel_header"> 
          <span>Event Contact Person</span>
          <?php if(!empty($requiredFieldsContact)){ ?>
                <button class="btn-red btn pull-right complete">Incomplete</button>
                <input type="hidden" name="" id="event_details_mandatory_status" value="0">
          <?php }else { ?>
                <button class="btn-normalComplete btn pull-right complete">Complete</button>
                <input type="hidden" name="" id="event_details_mandatory_status" value="1">
          <?php } ?>    
          </div>
          <div class="sabPanel_contentWrapper">
          <div class="panel_content">
            
            <?php
            if($requiredFieldsContact) { 
            foreach($requiredFieldsContact as $key => $fieldname){ ?>
            <div class="row">
              <div class="col-8">
                <div class="labelDiv">
                  <label class="form-label text-right"><?php echo $fieldname.' is required field '; ?> 
                    <a class="addEmptyValue" href="<?php echo base_url('event/eventdetails').'/?req=4'; ?>">
                        <span class="medium_icon"> <i class="icon-edit"></i> </span> 
                    </a>    
                  </label> 
                </div>
              </div>
            </div>
            <?php } 
            } ?> 
            
            <?php if(empty($requiredFieldsContact)){ ?>
            <div class="row">
              <div class="col-8">
                <div class="labelDiv pull-left">
                  <label class="form-label text-right">
                      All mandatory fields have been completed. 
                  </label> 
                </div>
              </div>
            </div>
            <?php } ?>     
          </div>             
          </div>
        </div>
        <!-- End sub panel -->
        
      </div>
</div>
