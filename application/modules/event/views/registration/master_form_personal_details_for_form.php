<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$registrantId       = '0';
$registrantPerFrm   = 'perosnal_regisDetails';


//--------------personal form details---------------//

$formPersonalDetails = array(
        'name'   => 'formPersonalDetailsregisDetails1',
        'id'     => 'formPersonalDetailsregisDetails',
        'method' => 'post',
        'class'  => '',
        
);
    
//get master field values array 
$fieldsmastervalue = fieldsmastervalue();

$emailfield = array(
            //  'name'  => 'emailfield',
                'value' => '',
                'id'    => 'emailfield',
                'type'  => 'email',
                'placeholder'   => 'Displayed & Mandatory',
                'readonly' => '',
                'class' => 'small short_field',
);  

?>

<div class="panel event_panel">
            <div class="panel-heading " id="<?php echo $registrantPerFrm; ?>">
                <h4 class="panel-title medium dt-large ">
                    <a class=" " data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                        <?php echo lang('event_default_registrant_details_master'); ?>
                     </a>
                </h4>
            </div>
            <div style="height: auto;" id="collapseTwo" class="accordion_content collapse apply_content ">
                <?php echo form_open($this->uri->uri_string(),$formPersonalDetails); ?>
                    <div class="panel-body ls_back dashboard_panel small" id="showhideformdiv<?php echo $registrantPerFrm; ?>">
                    <?php 
                                    
                        if(!empty($masterformfielddata)){
                        foreach($masterformfielddata as $formfield){
                            
                        $fieldCreateBy = $formfield->field_create_by;

                        if($fieldCreateBy=="1"){        
                        ?>
                                    <?php
                                        echo form_hidden('fieldid[]', $formfield->id);
                                        if($formfield->field_name=="email"){ 
                                    ?>
                                        <div class="row-fluid-15">
                                            <label for="dft_email">Email
                                                <span class="info_btn"><span class="field_info xsmall">This email will be used to inform this registrant of the conference details</span></span>
                                            </label>    
                                            
                                                <?php 
                                                    $fieldName= 'field_'.$formfield->id;    
                                                    echo form_input($emailfield);
                                                    $fieldName= 'field_'.$formfield->id;    
                                                    echo form_hidden($fieldName, $formfield->field_status);
                                                ?>
                                        
                                        </div>
                                        
                                    <?php }else {  ?>
                                
                                    <div class="row-fluid-15">
                                        <label for="dft_fname">
                                        <?php echo ucwords($formfield->field_name);
                                        if($formfield->field_name=='gender') {
                                            echo '<span class="info_btn"><span class="field_info xsmall">'.ucwords($formfield->field_name).'</span></span>';
                                        }
                                        ?>
                                        </label>
                                            <?php 
                                            $other = 'id="field_'.$formfield->id.'"  class="small custom-select short_field" ';
                                            $fieldName= 'field_'.$formfield->id;    
                                            echo form_dropdown($fieldName, $fieldsmastervalue, $formfield->field_status,$other);     ?>
                                            <?php  if($formfield->is_editable=="1"){ ?> 
                                                <!--<span class="pull-right select_edit "><a class="eventsetup_btn td_btn add_personal_form_field" href="javascript:void(0)" formAction="registrantCustomFieldSave" formId="1" registrantid="<?php echo $registrantId; ?>" customfieldid="<?php echo $formfield->id; ?>"><span>&nbsp;</span></a></span>-->

                                                <span class="add_personal_form_field pull-right select_edit" formAction="registrantCustomFieldSave" formId="1" registrantid="<?php echo $registrantId; ?>" customfieldid="<?php echo $formfield->id; ?>" data-parent-form-id="formPersonalDetailsregisDetails"><a class="eventsetup_btn td_btn" href="javascript:void(0)" ><span>&nbsp;</span></a></span>
                                            <?php   } ?>    
                                    </div>
                                            
                                            
                                    <?php } // end else ?>
                                    
                            
                    <?php } } } ?>
                    
                    
                    
                    
                    <!--end of row-fluid-->
                    
                    <?php  if(!empty($masterformfielddata)){
                        foreach($masterformfielddata as $key=> $formfield){
                        
                        $fieldCreateBy = (isset($formfield->field_create_by) && $formfield->field_create_by=="0")?$formfield->field_create_by:NULL;
                        if($fieldCreateBy=="0"){ ?>
                            
                            <div class="row-fluid-15" id="masterformregistrant_<?php echo $key; ?>">
                                <label for="dft_gender"><?php echo ucwords($formfield->field_name); ?></label>
                                <?php
                                    echo form_hidden('fieldid[]', $formfield->id);
                                    $other      = 'id="field_'.$formfield->id.'" class="small custom-select short_field" ';
                                    $fieldName  = 'field_'.$formfield->id;
                                    echo form_dropdown($fieldName, $fieldsmastervalue, $formfield->field_status,$other);    
                                ?>
                                <span class="pull-right select_edit">
                                    <a class="eventsetup_btn td_btn add_personal_form_field" href="javascript:void(0)" registrantid="<?php echo $registrantId; ?>" formAction="registrantCustomFieldSave" formId="1" customfieldid="<?php echo $formfield->id; ?>"  data-parent-form-id="formPersonalDetailsregisDetails"><span>&nbsp;</span></a>
                                    <a href="javascript:void(0)" class="delete_btn td_btn delete_personal_form_field" deleteid="<?php echo $formfield->id; ?>"><span>&nbsp;</span></a>
                                </span>
                            </div>  
                    
                        <?php } } } ?>

                        <div class="custom_field_div" id="custom_field_div"></div>
                    
                    
              <div class="add_personal_form_field row-fluid-15" registrantid="<?php echo $registrantId; ?>" formAction="registrantCustomFieldSave" formId="1" customfieldid="0" data-parent-form-id="formPersonalDetailsregisDetails"><a class="add_btn medium" href="javascript:void(0)" id="add_field"><?php echo lang('event_registration_custom_field'); ?></a></div>
                    <!--end of row-fluid-->
                    
                    
                    
                    <div class="btn_wrapper ">
            <a href="#collapseTwo" class="pull-left scroll_top"><?php echo lang('comm_top'); ?></a>

                        <?php 
                            echo form_hidden('eventId', $eventId);
                            echo form_hidden('registrantId', $registrantId);
                            echo form_hidden('formActionName', 'masterPersonalDetails');
                        
                            //button show of save and reset
                            $formButton['showbutton']   = array('save','reset');
                            $formButton['labletext']    = array('save'=>lang('comm_save_changes'),'reset'=>lang('comm_reset'));
                            $formButton['buttonclass']  = array('save'=>'default_btn btn pull-right medium','reset'=>'default_btn btn pull-right medium reset_form');
                            $this->load->view('common_save_reset_button',$formButton);
                        ?>
                        
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
        <!--end of panel-->


<!-- /commonform_bg -->

<script type="text/javascript">
$(document).ready(function(){
    //personal details save
    ajaxdatasave('formPersonalDetailsregisDetails','<?php echo $this->uri->uri_string(); ?>',true,true,false,false,false,'#showhideformdiv<?php echo $registrantPerFrm; ?>','#showhideformdivdieteryrequirement');

    /*
    *  This section is used select option fields
    */
        
    $(document).on('click','.stepper-step',function(){
        $(".default_value_list").html('');
        var getval = parseInt($("#custom_field_qty").val());    
        var addField = '';
        for(i=1;i<=getval;i++){
            addField+='<label class="pull-left" for="field_1">Field '+i+'</label>';
            addField+='<input type="text" value="" id="defaultValue_'+i+'" name="defaultValue[]" class="small" required="" data-parsley-error-class = "custom_li"  data-parsley-trigger="keyup" >';
            
            $(".default_value_list").append(addField);
            addField = '';
        }
    });
    
    /*
     *  This section is used to select custome field type select
    */
        
    $(document).on('click','.fieldtype',function(){
        var getval = $(this).val(); 
        var customFieldId = parseInt($("#customFieldId").val());
        //if use add field then remove old data
        if(customFieldId==0){
            $("#selectedField").val('1');
            $(".default_value_list").html('<label class="pull-left" for="field_1">Field 1</label><input type="text" value="" id="defaultValue_1" name="defaultValue[]" class="small" required="" data-parsley-error-class = "custom_li"  data-parsley-trigger="keyup" >');
        }
        if(getval=="text" || getval=="file"){
            $("#show_dropdown_feature").hide();
        }else{
            $("#show_dropdown_feature").show();
        }
    });
});    
</script>
