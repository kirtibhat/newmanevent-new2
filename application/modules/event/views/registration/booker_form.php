<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$registrantId     = '0';
$bookerDetailsFrm   = 'booker_details';


//--------------Booker form details---------------//

$formBookerDetails = array(
    'name'   => 'formbookerdetails',
    'id'   => 'formbookerdetails',
    'method' => 'post',
    'class'  => '',
    
);
  
//get master field values array 
$fieldsmastervalue = fieldsmastervalue();

$emailfield = array(
      //  'name'  => 'emailfield',
        'value' => '',
        'id'  => 'emailbookerfield',
        'type'  => 'email',
        'placeholder' => 'Displayed & Mandatory',
        'readonly' => '',
        'class' => 'small short_field',
);  

?>


<div class="panel event_panel">
  <div class="panel-heading" id="<?php echo $bookerDetailsFrm; ?>">
    <h4 class="panel-title medium dt-large ">
      <a class="" data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><?php echo lang('booker_details'); ?></a>
    </h4>
  </div>
  <div style="height: auto;" id="collapseOne" class="accordion_content collapse apply_content">
    <?php echo form_open($this->uri->uri_string(),$formBookerDetails); ?>
    
    <div class="panel-body ls_back dashboard_panel small" id="showhideformdiv<?php echo $bookerDetailsFrm; ?>">
      
      <?php 
        if(!empty($bookerformfielddata)){
            foreach($bookerformfielddata as $formfield){
              
            $fieldCreateBy = $formfield->field_create_by;
            
            if($fieldCreateBy=="1"){    
            ?>
                  <?php
                    echo form_hidden('fieldid[]', $formfield->id);
                    if($formfield->field_name=="email"){ 
                  ?>
                    <div class="row-fluid-15">
                      <label for="dft_email">Email
                        <span class="info_btn"><span class="field_info xsmall">This email will be used to inform this registrant of the conference details</span></span>
                      </label>  
                      
                        <?php 
                          $fieldName= 'field_'.$formfield->id;  
                          echo form_input($emailfield);
                          $fieldName= 'field_'.$formfield->id;  
                          echo form_hidden($fieldName, $formfield->field_status);
                        ?>
                    
                    </div>
                    
                  <?php }else {  ?>
                
                  <div class="row-fluid-15">
                    <label for="dft_fname"><?php echo ucwords($formfield->field_name); ?></label>
                      <?php 
                      $other = 'id="field_'.$formfield->id.'"  class="small custom-select short_field" ';
                      $fieldName= 'field_'.$formfield->id;  
                      echo form_dropdown($fieldName, $fieldsmastervalue, $formfield->field_status,$other);   ?>
                      <?php  if($formfield->is_editable=="1"){ ?> 
                          <!--<span class="pull-right select_edit "><a class="eventsetup_btn td_btn add_personal_form_field" href="javascript:void(0)" registrantid="0" formAction="bookerCustomFieldSave" formId="3" customfieldid="<?php echo $formfield->id; ?>"><span>&nbsp;</span></a></span>-->

                          <span class="add_personal_form_field pull-right select_edit" registrantid="0" formAction="bookerCustomFieldSave" formId="3" customfieldid="<?php echo $formfield->id; ?>" data-parent-form-id="formbookerdetails"><a class="eventsetup_btn td_btn" href="javascript:void(0)" id="add_field" ><span>&nbsp;</span></a></span>
                      <?php } ?>  
                  </div>
                      
                      
                  <?php } // end else ?>
                  
              
          <?php } } } ?>
          
          
          
          
          <!--end of row-fluid-->
          
          <?php  if(!empty($bookerformfielddata)){
            foreach($bookerformfielddata as $key=> $formfield){
            
            $fieldCreateBy = (isset($formfield->field_create_by) && $formfield->field_create_by=="0")?$formfield->field_create_by:NULL;
            if($fieldCreateBy=="0"){ ?>
              
              <div class="row-fluid-15" id="masterformregistrant_<?php echo $key; ?>">
                <label for="dft_gender" id="masterfieldlbl_<?php echo $formfield->id; ?>"><?php echo ucwords($formfield->field_name); ?></label>
                <?php
                  echo form_hidden('fieldid[]', $formfield->id);
                  $other    = 'id="field_'.$formfield->id.'" class="small custom-select short_field" ';
                  $fieldName  = 'field_'.$formfield->id;
                  echo form_dropdown($fieldName, $fieldsmastervalue, $formfield->field_status,$other);  
                ?>
                <span class="pull-right select_edit">
                  <a class="eventsetup_btn td_btn add_personal_form_field" href="javascript:void(0)" registrantid="0" formAction="bookerCustomFieldSave" formId="3" customfieldid="<?php echo $formfield->id; ?>" data-parent-form-id="formbookerdetails"><span>&nbsp;</span></a>
                  <a href="javascript:void(0)" class="delete_btn td_btn delete_personal_form_field" deleteid="<?php echo $formfield->id; ?>"><span>&nbsp;</span></a>
                </span>
              </div>  
          
              <?php } } } ?>

            <div class="custom_field_div" id="custom_field_div"></div>
               
            
            <div class="add_personal_form_field row-fluid-15" registrantid="0" formAction="bookerCustomFieldSave" formId="3" customfieldid="0" data-parent-form-id="formbookerdetails"><a class="add_btn medium" href="javascript:void(0)" id="add_field" ><?php echo lang('event_registration_custom_field'); ?></a></div>

            <div class="btn_wrapper ">
                <a href="#collapseOne" class="pull-left scroll_top"><?php echo lang('comm_top'); ?></a>

                <?php 
                  echo form_hidden('eventId', $eventId);
                  echo form_hidden('registrantId', $registrantId);
                  echo form_hidden('formActionName', 'bookerDetails');
                
                  //button show of save and reset
                  $formButton['showbutton']   = array('save','reset');
                  $formButton['labletext']  = array('save'=>lang('comm_save_changes'),'reset'=>lang('comm_reset'));
                  $formButton['buttonclass']  = array('save'=>'default_btn btn pull-right medium','reset'=>'default_btn btn pull-right medium reset_form');
                  $this->load->view('common_save_reset_button',$formButton);
                ?>
            </div>
    </div>
    <?php echo form_close(); ?>
  </div>
</div>
<!--end of panel-->
