<?php 
if(!empty($registrantCategorydata)){
    foreach($registrantCategorydata as $registrantCategory){
    //print_r($registrantCategory);
    $registrantCategoryId   = $registrantCategory['category_id'];
    $registrantCategoryName = $registrantCategory['category'];
?>

<div class="panel event_panel co_cat co_cat_corporate">
    <div class="panel-heading ">
        <h4 class="panel-title medium dt-large p_rel">
            <a class="collapsed " data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $registrantCategoryId; ?>"><?php echo $registrantCategoryName; ?></a>
            <input type="text" name="filterregistrant" value="" id="filterregistrant" placeholder="" class="small short_field filterregistrant" data-key="">
        </h4>
    </div>
    <div style="height: auto;" id="collapse<?php echo $registrantCategoryId; ?>" class="accordion_content collapse apply_content <?php echo getIsCollapseOpen("collapseFive"); ?>">
        <div class="panel-body ls_back dashboard_panel small co_category">
            <div id="accordion_sub" class="panel-group sub-panel-group">
               <?php
                $where = array('event_id'=>$eventId, 'category_id'=>$registrantCategoryId, 'is_corporate_package'=>'0');
                $eventregistrantsdata = getDataFromTabel('event_registrants','*', $where,'','registrants_order, id','ASC');
                if(!empty($eventregistrantsdata)){
                  //prepare array data for current and next form hide/show 
                  unset($recordId);
                  foreach($eventregistrantsdata as $registrants){
                    $recordId[] =  $registrants->id;
                  }
                  
                  //show registrant type details forms
                  $recordCount = 0;
                  $where = array('event_id'=>$eventId, 'category_id'=>$registrantCategoryId, 'is_corporate_package'=>'0');
                  $eventregistrants = getDataFromTabel('event_registrants','*', $where,'','registrants_order, id','ASC');
                  foreach($eventregistrants as $registrants){
                    $nextId = $recordCount+1;
                    $sendData['registrantCategoryId']     = $registrantCategoryId;
                    $sendData['registrantCategoryName']     = $registrantCategoryName;
                    $sendData['is_corporate_package']     = 0; 
                    $sendData['formdata']     = $registrants; 
                    $sendData['nextRecordId'] =   (isset($recordId[$nextId]))?$recordId[$nextId]:'0';
                    $sendData['recordCount']  = $recordCount; 
                    // dynamic load all registrant forms
                    $this->load->view('form_registrant_types',$sendData);
                    $recordCount++;
                  }
                } // end if
              ?>
            </div>
        </div>
    </div>
</div>
<?php
    } //end for each
} // end if
?>

<?php
$where = array('event_id'=>$eventId, 'is_corporate_package'=>'1');
$eventCorpRegistrantsdata = getDataFromTabel('event_registrants','*', $where,'','registrants_order, id','ASC');
//print_r($eventCorpRegistrantsdata);
if(!empty($eventCorpRegistrantsdata)){
    foreach($eventCorpRegistrantsdata as $corpRegistrantCategory){         
        $registrantCategoryId = $corpRegistrantCategory->category_id; 
        $corpWhere = array('event_id'=>$eventId, 'category_id'=>$registrantCategoryId);
        $eventCatdata = getDataFromTabel('event_categories','*', $corpWhere,'','category_id','ASC');
        //print_r($eventCorpdata);          
        $registrantCategoryName = $eventCatdata[0]->category;
?> 
<div class="panel event_panel co_cat co_cat_corporate">
    <div class="panel-heading ">
        <h4 class="panel-title medium dt-large p_rel">
            <a class="collapsed " data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $registrantCategoryId; ?>"><?php echo $registrantCategoryName; ?></a>
            <input type="text" name="filterregistrant" value="" id="filterregistrant" placeholder="" class="small short_field filterregistrant" data-key="">
        </h4>
    </div>
    <div style="height: auto;" id="collapse<?php echo $registrantCategoryId; ?>" class="accordion_content collapse apply_content <?php echo getIsCollapseOpen("collapseFive"); ?>">
        <div class="panel-body ls_back dashboard_panel small co_category">
            <div id="accordion_sub" class="panel-group sub-panel-group">
               <?php
                $where = array('event_id'=>$eventId, 'category_id'=>$registrantCategoryId, 'is_corporate_package'=>'1');
                $eventregistrantsdata = getDataFromTabel('event_registrants','*', $where,'','registrants_order, id','ASC');
                if(!empty($eventregistrantsdata)){
                  //prepare array data for current and next form hide/show 
                  unset($recordId);
                  foreach($eventregistrantsdata as $registrants){
                    $recordId[] =  $registrants->id;
                  }
                  
                  //show registrant type details forms
                  $recordCount = 0;
                  $where = array('event_id'=>$eventId, 'category_id'=>$registrantCategoryId, 'is_corporate_package'=>'1');
                  $eventregistrants = getDataFromTabel('event_registrants','*', $where,'','registrants_order, id','ASC');
                  foreach($eventregistrantsCorp as $registrants){
                    $nextId = $recordCount+1;
                    $sendData['registrantCategoryId']     = $registrantCategoryId; 
                    $sendData['registrantCategoryName']   = $registrantCategoryName; 
                    $sendData['is_corporate_package']     = 1; 
                    $sendData['formdata']     = $registrants; 
                    $sendData['nextRecordId'] =   (isset($recordId[$nextId]))?$recordId[$nextId]:'0';
                    $sendData['recordCount']  = $recordCount; 
                    // daynamic load all registrant forms
                    $this->load->view('form_registrant_types',$sendData);
                    $recordCount++;
                  }
                } // end if
              ?>
            </div>
        </div>
    </div>
</div>
<?php        
    }
}
?>





<script>
$(document).ready(function() {
    $(document).on('keyup', '.filterregistrant', function(){
        $(".accordion_content").css("height", 'auto');         
        var filter_text = $(this).val();
        if(filter_text){                                
            $(".accordion_content").css("height", 'auto');
            $('.filter_registrants').hide();
            $("h4[class*="+filter_text+"]").show();
            $(".accordion_content").css("height", 'auto');    
        } else {           
           $(".accordion_content").css("height", 'auto');
           $('.filter_registrants').show();
        }
    });    
});
</script>
