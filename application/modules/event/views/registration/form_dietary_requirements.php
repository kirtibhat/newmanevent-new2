<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


//set field value in variable
$dietaryRequirementsValue   = (!empty($eventdetails->dietary_requirments))?$eventdetails->dietary_requirments :'0';

$formdietaryrequirements = array(
        'name'   => 'formdietaryrequirements',
        'id'     => 'formdietaryrequirements',
        'method' => 'post',
        'class'  => 'wpcf7-form',
        'data-parsley-validate' => '',
);
    
$dietaryrequirements = array(
        'name'  => 'dietaryrequirements',
        'value' => '1',
        'id'    => 'dietaryrequirements',
        'type'  => 'checkbox',
        'class' => '',
);

//check value and set value
if(!empty($dietaryRequirementsValue) && $dietaryRequirementsValue > 0){
    $dietaryrequirements['checked']=true;
}   


//--------add type fo registration fields---------//    

$formAddTypeofDietary = array(
        'name'   => 'formAddTypeofDietary',
        'id'     => 'formAddTypeofDietary',
        'method' => 'post',
        'class'  => 'wpcf7-form',
        'data-parsley-validate' => '',
    );
    
$dietaryName = array(
        'name'  => 'dietaryName',
        'value' => '',
        'id'    => 'dietaryName',
        'type'  => 'text',
        'class' => 'small',
        'required'  => '',
        'autocomplete' => 'off',
        'data-parsley-error-message' => lang('common_field_required'),
        'data-parsley-error-class' => 'custom_li',
        'data-parsley-trigger' => 'keyup',
    );

?>

 <div class="panel event_panel">
                    <div class="panel-heading ">
                        <h4 class="panel-title medium dt-large ">
                        <a class=" " data-toggle="collapse" data-parent="#accordion" href="#collapseThree"><?php echo lang('event_dietary_requirements_title'); ?></a>
                        </h4>
                    </div>
                    
                    <div style="height: auto;" id="collapseThree" class="accordion_content collapse apply_content">
                        
                        <?php echo form_open($this->uri->uri_string(),$formdietaryrequirements); ?>
                            <div class="panel-body ls_back dashboard_panel small" id="showhideformdivdieteryrequirement">
                            
                                
                                
                                <div class="row-fluid-15">
                                    <ul class="requirement_ul medium">
                                    <?php if(!empty($eventdietary)){ 
                                        foreach($eventdietary as $dietary){ ?>
                                                
                                                <li>
                                                    <a id="row_<?php echo $dietary->id; ?>">
                                                        <?php echo $dietary->dietary_name; ?>
                                                        <?php if($dietary->is_undeletable==0){ ?>
                                                            <span class="req_close crossbtn delete_dietary" id="<?php echo $dietary->id; ?>">X</span>
                                                        <?php }else{ ?>
                                                            <span class="req_close crossbtndisble" ></span>
                                                        <?php } ?>  
                                                    </a>
                                                </li>   
                                    <?php }  }  ?>
                                    </ul>
                                </div>
                                <!--end of row-fluid-->
                
                <div class="row-fluid-15 ">
                                    <a id="add_dietary" href="javascript:void(0)" class="add_btn medium addtype add_dietary_type" ><?php echo lang('event_dietary_add_dietary_req_title'); ?></a>
                                </div>          
                                <!--end of row-fluid-->
              
                                <div class="btn_wrapper ">
                  <a href="#collapseThree" class="pull-left scroll_top"><?php echo lang('comm_top'); ?></a>

                                    <?php 
                                    echo form_hidden('eventId', $eventId);
                                    echo form_hidden('formActionName', 'dietaryrequirements');
                                    $extraSave  = 'class="submitbtn_cus savechange_regd" ';
                                    ?>
                                    
                                    <a href="#" class="default_btn btn pull-right medium">Save</a>
                                    <a href="#" class="default_btn btn pull-right medium">Clear</a>
                                </div>
                            </div>
                        <?php echo form_close(); ?> 
                        
                    </div> 
 </div>                   
 <!--end of panel-->




<!--end of pop1-->
<div id="add_type_dietary_popup" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" >
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header delegates">
        <h4 class="medium dt-large modal-title"><?php echo lang('event_dietary_popup_add_field_title'); ?></h4>
      </div>

      <div class="modal-body small">
        <div class="modelinner ">
          <?php  echo form_open(base_url('event/addeventdietary'),$formAddTypeofDietary); ?>
            <div class="control-group mb10 eventdashboard_popup small">

              <div class="row-fluid">
                <label class="pull-left" for="dietary_name">Name   <span class="astrik">*</span>
                <span class="info_btn"><span class="field_info xsmall"><?php echo lang('event_dietary_field_name'); ?></span></span>
                </label>
                <?php echo form_input($dietaryName); ?>
              </div>
              
              <div class="btn_wrapper">
                  <?php 
                    echo form_hidden('eventId', $eventId);
                
                    $extraCancel    = 'class="popup_cancel submitbtn pull-right medium" data-dismiss="modal" ';
                    $extraSave      = 'class="submitbtn pull-right medium" ';
                
                    echo form_submit('save',lang('comm_save'),$extraSave);
                    echo form_submit('cancel',lang('comm_cancle'),$extraCancel);
                    
                  ?>
                 <!-- <input type="submit" name="loginsubmit" value="Save" class=" submitbtn pull-right medium">
                      <input type="submit" class="popup_cancel submitbtn pull-right medium" value="Cancel" name="logincancel" data-dismiss="modal">
                 --> 
              </div>

            </div>
         <?php echo form_close(); ?>    
        </div>
      </div>
    </div>
  </div>
</div>
<!--end of pop2-->
<!-- Modal -->

<script type="text/javascript">
$(document).ready(function(){    
    //dietary requirements save
    //ajaxdatasave('formdietaryrequirements','<?php echo $this->uri->uri_string(); ?>');
    
    // add dietary type by popup
    popupopen('add_dietary_type','add_type_dietary_popup');
        
    //form Add Type of Dietary
    ajaxdatasave('formAddTypeofDietary','event/addeventdietary',false,true,true,false,false,false,false,false);
        
    // add dietary type by popup
    $(document).on('click','.delete_dietary', function(){
        
        var deleteid = $(this).attr('id');
        //$('#row_'+deleteid).fadeOut('slow');
        jQuery('#row_'+deleteid).css('opacity', '0.4');
        //jQuery('#row_'+deleteid).css('opacity') = '0.6';
    
        var fromData={"deleteId":deleteid};
        var url = baseUrl+'event/deletedietarytype';
        $.post(url,fromData, function(data) {
              if(data){
                    if(data.is_success=='true'){
                            //custom_popup(data.msg,true);  
                            $("#row_"+deleteid).remove();
                    }
                }
            },"json");
    });
});    
</script>
