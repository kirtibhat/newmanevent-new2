<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//--------add duplicate of type registrant fields---------//  
$formDuplicateRegistration = array(
    'name'   => 'formDuplicateRegistration',
    'id'   => 'formDuplicateRegistration',
    'method' => 'post',
    'class'  => 'form-horizontal mt10',
  );
  
$registrationName = array(
    'name'  => 'registrationName',
    'value' => '',
    'id'  => 'registrationTypeName',
    'type'  => 'text',
  //  'placeholder' => 'Name',
    'required'  => '',
    'autocomplete' => 'off',
    'data-parsley-error-message' => lang('common_field_required'),
    'data-parsley-error-class' => 'custom_li',
    'data-parsley-trigger' => 'keyup',
  );

$registrationTypeId = array(
    'name'  => 'registrationTypeId',
    'value' => '0',
    'id'  => 'DuplRegistrationTypeId',
    'type'  => 'hidden',
  );  

$regisEventId = array(
    'name'  => 'eventId',
    'value' => '0',
    'id'  => 'DuplRegisEventId',
    'type'  => 'hidden',
  );    

?>



<!-- Modal -->
<div id="duplicate_regitrant_popup" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" >
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header delegates">
        <h4 class="medium dt-large modal-title"><?php echo lang('event_regis_duplicate_type'); ?></h4>
      </div>

      <div class="modal-body small">
        <div class="modelinner ">
       <?php  echo form_open(base_url('event/addediteventregistration'),$formDuplicateRegistration); ?>
            <div class="control-group mb10 eventdashboard_popup small">

              <div class="row-fluid">
                <label class="pull-left" for="ctype"><?php echo lang('event_regis_dupli_button_name'); ?>  <span class="astrik">*</span>
          </label>
                <?php echo form_input($registrationName); ?>
              </div>

              <div class="btn_wrapper">
                 <?php 
          echo form_input($registrationTypeId); 
          echo form_input($regisEventId); 
          $extraCancel  = 'class="popup_cancel submitbtn pull-right medium" data-dismiss="modal" ';
          $extraSave  = 'class="submitbtn pull-right medium" ';
          
          echo form_submit('save',lang('comm_submit'),$extraSave);
          echo form_submit('cancel',lang('comm_cancle'),$extraCancel);
        ?>
                 
              </div>

            </div>
         <?php echo form_close(); ?>
        </div>
      </div>
    </div>
  </div>
</div>
   
<script>
$(document).ready(function(){ 
    $("#formDuplicateRegistration").parsley();
});
</script>
