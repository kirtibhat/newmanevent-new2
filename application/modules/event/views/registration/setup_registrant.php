<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 |--------------------------------------------------------------------------|
 | This Field define for temparay open popup by add section id        |
 | and edit record id                           | 
 |--------------------------------------------------------------------------|
*/

$tempHidden = array(
  'type' => 'hidden',
  'name' => 'tempHidden',
  'id' => 'tempHidden',
  'value' => '',
);

echo form_input($tempHidden);

?>

<?php $this->load->view('event_menus');  ?>


 <div class="row-fluid-15">
        <div id="page_content" class="span9">
            <div id="accordion" class="panel-group">
        
        <?php 
          //add & edit registrant popup 
          $this->load->view('add_edit_registrant_type_form');
          
          //duplicate registrant popup 
          $this->load->view('duplicate_registrant_type_form');
                    
          /*
          * this view load master personal details for set for new
          * created registrant personal details form
          */ 
          
          $this->load->view('booker_form');
          
          $this->load->view('master_form_personal_details_for_form');
                
          $this->load->view('form_dietary_requirements');
          
        ?>
        <p class="small">
          <?php echo lang('event_regist_title_add'); ?> 
          <span class="clearfix"></span>
          <a href="javascript:void(0)" class="btn medium add_reg_type add_big_btn add_edit_login_type"><?php echo lang('event_dietary_add_registrant_type'); ?></a>
        </p>
        
        <?php
            /*$this->load->view('form_registrant_category');             
            if(!empty($eventregistrants)){
              
              //prepare array data for current and next form hide/show 
              unset($recordId);
              foreach($eventregistrants as $registrants){
                $recordId[] =  $registrants->id;
              }
              
              //show registrant type details forms
              $recordCount = 0;
              foreach($eventregistrants as $registrants){
                $nextId = $recordCount+1;
                $sendData['formdata']     = $registrants; 
                $sendData['nextRecordId'] =   (isset($recordId[$nextId]))?$recordId[$nextId]:'0';
                $sendData['recordCount']  = $recordCount; 
                // daynamic load all registrant forms
                $this->load->view('form_registrant_types',$sendData);
                $recordCount++;
              }
            } // end if
            */
            $this->load->view('form_registrant_types_categories');
          ?>
        
        
        <?php

          //next and previous button
          $buttonData['viewbutton'] = array('next','preview','back');
          $buttonData['linkurl']  = array('back'=>base_url('event/corporatedetails'),'next'=>base_url('event/programdetails'),'preview'=>'');
          $this->load->view('common_back_next_buttons',$buttonData);
        ?>
      
            <!--end of page btn wrapper-->  
    </div>
        <!--end of page content-->
       </div> 
</div>
<!--end of row-->


<!--start custom tooltip-->
<div id="tooltip_wrapper"></div>
<!--end custom tooltip-->


<script type="text/javascript">
$(document).ready(function(){  
     /*
      * add and editlogin type by popup
      */

    $(".add_edit_login_type").click(function(){
      $("#formTypeofRegistration").trigger('reset');//reset form
      var getRegisId     = parseInt($(this).attr('id'));
      var getRegisTitle  = $(this).attr('registitle');
      var getRegisCategory = $(this).attr('regiscategoryid');
      var getRegisPass  = $(this).attr('regispassword');
      var getRegisDescrip = $(this).attr('regisdescrip');
      
      $("#registrantTypeTitle").html("Add");
      
      if(getRegisId*1>0){
        $("#registrantTypeTitle").html("Edit");
      }
      
      //set registrant id
      $("#registrationTypeId").val(getRegisId);
      $("#registrationTypeName").val(getRegisTitle);
      
      $("#registrantType").val(getRegisCategory);
      $('#registrantType option[value="0"]').attr('selected', false );
      $('#registrantType option[value="'+$.trim(getRegisCategory)+'"]').attr('selected', true);
      
      // reset dropdown
      var selectedOption = $("#registrantType").find(":selected").text();
      $("#registrantType").parent(".select-wrapper").find(".holder").text(selectedOption);
      
      $("#registrationPassword").val(getRegisPass);
      $("#registrationDescription").val(getRegisDescrip);
      if(getRegisId > 0)
        $("#regis_action").html('Edit');
      else
        $("#regis_action").html('Add');
      //regis_action
      
      //open add registrant type form and set formdivid and clickclass
      $("#ajax_open_popup_html").attr('openbox','add_type_login_popup');  
      $("#ajax_open_popup_html").attr('clickbox','add_edit_login_type');  
      open_model_popup('add_type_login_popup');
    });

    //save type of registration
    ajaxdatasave('formTypeofRegistration','event/addediteventregistration',false,true,true,false,false,false,false,false);


    /*
     * This section is used to show and hide div of 
     * registant details and types
     */
    $(".regis_div_show").click(function(){
      var getValue = parseInt($(this).attr('regisdiv'));
      $('.regis_div_show').removeClass('active'); //remove old active
      $(this).addClass('active'); // add current selection active
      if(getValue==1){
        $(".regis_details_div").slideDown('slow');
        $(".regis_types_div").slideUp('slow');
      }else{
        $(".regis_details_div").slideUp('slow');
        $(".regis_types_div").slideDown('slow');
      }
    });


    /*
     * This section is used to show and hide div of 
     * registant details and types
     */

    $(".ispasswordaccess").click(function(){
      // For set collapse hight
      $(".accordion_content").css("height",'auto');
            
      var getregisid=$(this).attr('passwordaccessid');
      var getval=parseInt($(this).val());
      //var getid=$(this).attr('checked');
      if(getval==1 ){
        showarea("#password_access_div"+getregisid);
      }else{
        hidearea("#password_access_div"+getregisid);
      }
    });




     /*
      *  This section is used select option fields
    */

    $(document).on('click','.ui-spinner-button',function(){
      $(".default_value_list").html('');
      var getval = parseInt($("#selectedField").val()); 
      var addField = '';
      for(i=1;i<=getval;i++){
        addField+= '<div class="control-group mb10">';
        addField +='<label for="inputEmail" class="control-label">Field '+i+':</label>';
        addField +='<div class="controls">';
        addField +='<input type="text" name="defaultValue[]" value="" id="defaultValue_'+i+'" ></div></div>';
        $(".default_value_list").append(addField);
        addField = '';
      }
    });


    //allow earlybird selection in registrant type section
    $(".allowEarlybirdselection").click(function(){
      var getregisid = $(this).attr('regisid');
      var getchecked=$(this).prop('checked');
      if($(this).val()==1){
        showarea("#registrant_type_early_bird"+getregisid);
        $('.earlyBirdLimit').attr('required','');
        $('.unitPriceEarlyBird').attr('required','');
      }else if($(this).val()==0){
        hidearea("#registrant_type_early_bird"+getregisid);
        $('.earlyBirdLimit').removeAttr('required');
        $('.unitPriceEarlyBird').removeAttr('required');
      } 
    });

    //Does this registration type have the option of single day registration
    $(".issingledayregistration").click(function(){
      // For set collapse hight
      $(".accordion_content").css("height",'auto');
            
      var getregisid=$(this).attr('singledayregistration');
      var getval=parseInt($(this).val());
      //var getid=$(this).attr('checked');
      if(getval==1 ){
        showarea("#day_date_list_"+getregisid);
        //$(".disableAll_"+getregisid).removeAttr('disabled');
      }else{
        hidearea("#day_date_list_"+getregisid);
        //$(".disableAll_"+getregisid).attr('disabled','disabled'); 
      }
    });

    //registrant Day selection for registrant type
    $(".registrantDay").click(function(){        
      // For set collapse hight
      $(".accordion_content").css("height",'auto');      
      var earlybirddayid=$(this).attr('registrantdayid');
      
      var getchecked=$(this).prop('checked');
      if(getchecked==true && getchecked!=undefined ){
        showarea("#allow_day_"+earlybirddayid);
        //show earlybird section
        showarea("#allow_earlybir_button_"+earlybirddayid);
      }else{
        hidearea("#allow_day_"+earlybirddayid);
        //hide earlybird section
        $("#registrantEarlybirdDay"+earlybirddayid).prop('checked',false);
        hidearea("#allow_earlybir_button_"+earlybirddayid);
        hidearea("#allow_earlybir_"+earlybirddayid); 
      } 
      //call for hidden field active registration day 
      activeHidden(earlybirddayid);
    });

    //registrant Early bird Day Allowed
    $(".registrantEarlybirdDay").click(function(){
      var earlybirddayid=$(this).attr('earlybirddayid');
      var getchecked=$(this).prop('checked');
      if($(this).val()==1){
        showarea("#allow_earlybir_"+earlybirddayid);
       }else if($(this).val()==0) {
        hidearea("#allow_earlybir_"+earlybirddayid);
      }
      //call for hidden field active registration day 
      activeHidden(earlybirddayid);
    });

    //registrant Early bird Day Allowed
    $(".registrantComplimentary").click(function(){
      var registrantDayId=$(this).attr('registrantDayId');
      if($(this).val()==0){
        showarea("#complimentary_div_"+registrantDayId);
      }else if($(this).val()==1) {
        hidearea("#complimentary_div_"+registrantDayId);

      }
      //call for hidden field active registration day
    });

    //check registrantEarlybirdDay and registrantDay any checked the hidden field active
    function activeHidden(dayrowid){
      var getFirstVal = $("#registrantDay"+dayrowid).prop('checked');
      var getSecondVal = $("#registrantEarlybirdDay"+dayrowid).prop('checked');
      if(getFirstVal=="checked" ||  getSecondVal=="checked"){
        $(".registration_hidden_days_"+dayrowid).removeAttr('disabled');
      }else{
        $(".registration_hidden_days_"+dayrowid).attr('disabled','disabled'); 
      }
    } 


    //duplicate type of registrant popup open
    $(".duplicate_registrant").click(function(){
        var registrationTypeId = $(this).attr('id');
        var eventId = $(this).attr('eventid');
        $("#DuplRegistrationTypeId").val(registrationTypeId);
        $("#DuplRegisEventId").val(eventId);

        //open add registrant type form and set formdivid and clickclass
        $("#ajax_open_popup_html").attr('openbox','duplicate_regitrant_popup'); 
        $("#ajax_open_popup_html").attr('clickbox','duplicate_registrant'); 
        open_model_popup('duplicate_regitrant_popup');  
    })

    //create duplidate type of registration
    ajaxdatasave('formDuplicateRegistration','event/duplicateeventregistration',false,true,true,false,false,false,false,false);

    //reset duplidate type of registration
    popup_cancle_form('dup_cancel_form','formDuplicateRegistration');

    //delete type of registrant
    customconfirm('delete_registrant','event/deleteeventregistration', '', '', '', true,'',false,'<?php echo lang('event_msg_type_registration_deleted') ?>');

    /*---shortable registrant type and drag drop -----*/
    /*
    $(function() {
       $( ".sortable" ).sortable({ 
      update: function() {
      var order = $(this).sortable("serialize");
        var url = baseUrl+'event/regitrantorderupdate';
        $.post(url, order, function(data){          
          refreshPge();       
        },'json');                        
      }                 
    });
    });

    */


    /*---This function is used to add and edit custome field for personal field -----*/
    $(document).on('click','.add_personal_form_field',function(){                                                

        //set value in temparray hidden field for if again open
        if($(this).attr('registrantid') !== undefined && $(this).attr('customfieldid') !== undefined ){  
            var parentFormId = $(this).data('parent-form-id');
            var registrantid = $(this).attr('registrantid');
            //alert(parentFormId+" ----- "+registrantid);return false;
            var customfieldid = $(this).attr('customfieldid');
            var formAction = $(this).attr('formAction');
            var formId     = $(this).attr('formId');

            //concanate two value in assing
            var tempObj = registrantid+','+customfieldid;
            $("#tempHidden").val(tempObj);
        }else{
            var getTempData = $("#tempHidden").val();
            var getData = getTempData.split(',');

            //get value from hidden field and set value
            registrantid = getData[0];
            customfieldid = getData[1];
        }

        var eventId = '<?php echo $eventId ?>';
        var sendData = {"registrantid":registrantid, "customfieldid":customfieldid, "eventId":eventId,"formAction":formAction,"formId":formId, "parentFormId":parentFormId};

        //call ajax popup function
        ajaxpopupopen('add_personal_form_field_popup','event/add_edit_custom_field',sendData,'add_personal_form_field','');
    });

    

    /*---This function is used to delete custome field for personal field -----*/
    customconfirm('delete_personal_form_field','event/deleteeditcustomefield', '', '', '', true,'',false,'<?php echo lang('event_msg_custom_field_deleted') ?>');

    var gstRate = parseInt('<?php echo (!empty($eventdetails->gst_rate))?$eventdetails->gst_rate:"0"; ?>');

    /*----This function is used to unit price enter and show-----*/
    unitPriceManage('unitPriceEnter','registrantId','GSTInclude','totalPrice',gstRate, 'totalPriceIncGST');

    /*----This function is used to unit price enter and show early bird-----*/
    unitPriceManage('unitPriceEarlyBird','registrantId','earlyGSTInclude','earlyBirdPrice',gstRate, 'earlyBirdPriceIncGST');

    /*----This function is used to day wise unit price enter and show early bird-----*/
    unitPriceManage('dayUnitPriceEarlyBird','registrantDayId','registrantGSTInclude','registrantDayTotalPrice',gstRate,'registrantDayPriceIncGST');

    /*----This function is used to early bird day wise unit price enter and show early bird-----*/
    unitPriceManage('earlyDayUnitPriceEarlyBird','registrantDayId','registrantEarlybirdGSTInclude','registrantEarlybirdDayPrice',gstRate,'registrantEarlybirdDayPrice');


    /*
    *  This section is used to select custome field type select
    */
    $(document).on('click','.fieldtype',function(){
      var getval = $(this).val(); 
      var customFieldId = parseInt($("#customFieldId").val());
      //if use add field then remove old data
      if(customFieldId==0){
        $("#selectedField").val('0');
        $(".default_value_list_personal").html('');
      }
      if(getval=="text" || getval=="file"){
        $("#show_dropdown_feature").hide();
      }else{
        $("#show_dropdown_feature").show();
        $("#custom_field_qty_personal").val('1');
        $(".default_value_list_personal").html('<label class="pull-left" for="field_1">Field 1</label><input type="text" class="small" value="" required  data-parsley-error-message = "<?php echo lang('common_field_required'); ?>" data-parsley-error-class="custom_li"  data-parsley-trigger="keyup" name="defaultValue[]" id="defaultValue_0" />');
      }
    });

    /*
    *  This section is used select option fields
    */   
    $(document).on('click','.step-personal',function(){
      var qty = parseInt($("#custom_field_qty_personal").val());    
      if($(this).hasClass('up')){ // increment
        if(qty<10){
          var currentId = 'defaultValue_'+qty; 
          var currentLblId = 'defaultLblValue_'+qty; 
          $("#custom_field_qty_personal").val((qty*1)+1);    
          $(".default_value_list_personal").append('<label class="pull-left" for="field_1" id="'+currentLblId+'">Field '+((qty*1)+1)+'</label><input type="text" class="small" value="" required  data-parsley-error-message = "<?php echo lang('common_field_required'); ?>" data-parsley-error-class="custom_li"  data-parsley-trigger="keyup" name="defaultValue[]" id="'+currentId+'" />');
        }   
      }else{
        var currentId = 'defaultValue_'+((qty*1)-1); 
        var currentLblId = 'defaultLblValue_'+((qty*1)-1); 
        if(qty>1){
          console.log(((qty*1)-1));
          $("#custom_field_qty_personal").val(((qty*1)-1)); 
          $("#"+currentId).remove();
          $("#"+currentLblId).remove();
        }  
      } 
    });

    /*
    * This section is used to show and hide div of 
    * registant details and types
    */
    $(".comaplimentary").click(function(){
        // For set collapse hight
        $(".accordion_content").css("height",'auto');
        var registrantid=$(this).data('registrantid');
        if($(this).val()==1){
            hidearea("#comaplimentaryDiv"+registrantid);
            $("#totalPrice"+registrantid).removeAttr('required');
            $("#totalPrice"+registrantid).attr('data-parsley-required', 'false');               
        } else if($(this).val()==0){
            $("#totalPrice"+registrantid).attr('required','');
            showarea("#comaplimentaryDiv"+registrantid);                      
        }
    });


    //form add and edit custome field save
    //ajaxdatasave('formAddCustomeField','event/addeditcustomefieldsave',false,true,true,false,false,false,false,false);

    //Save Extra Form Custome Field Via Ajax
    $(document).on("click", ".saveCustomeField", function(e) {
        $(".accordion_content").css("height",'auto');

        if($("#formAddCustomeField").parsley().isValid()){
            //var parentFormId = data.id;
            
            var fromData=$("#formAddCustomeField").serialize();            
            var parentFormId = $("#formAddCustomeField #parentFormId").val();
            var formId = $("#formAddCustomeField #formId").val();
            //console.log(fromData); 
            //console.log("parentFormId: "+parentFormId);
            //console.log("formId: "+formId);
            var url = baseUrl+'event/addeditcustomefieldsave';
            $.ajax({
                type:'POST',
                data:fromData,
                url: url,
                dataType: 'json',
                async: false,
                cache: false,
                beforeSend: function( ) {
                },
                success: function(data){
                    if(data.is_success=='true'){
                        var fieldName = $("#formAddCustomeField #fieldName").val();
                        var fieldId = data.id;
                        var selectarr = '<select name="field_'+fieldId+'" id="field_'+fieldId+'" class="small custom-select short_field">';
                        $.each(data.fieldsmastervalue, function(index, val) {
                            var selectindex = (index=="1") ? "selected='selected'" : "";
                            selectarr+= '<option value="'+index+'" '+selectindex+'>'+val+'</option>';;
                        });                   
                        selectarr += '</select>';                        
                        var fieldHtml = '<input type="hidden" name="fieldid[]" value="'+fieldId+'">';
                        fieldHtml += '<div class="row-fluid-15" id="masterformregistrant_'+fieldId+'"><label for="dft_gender" id="masterfieldlbl_'+fieldId+'">'+capWords(fieldName)+'</label>  '+ selectarr +' <span class="pull-right select_edit">   <a class="eventsetup_btn td_btn add_personal_form_field"  href="javascript:void(0)" registrantid="0" formAction="bookerCustomFieldSave" formId="'+formId+'" customfieldid="'+fieldId+'"  data-parent-form-id="'+parentFormId+'"><span>&nbsp;</span></a><a href="javascript:void(0)" class="delete_btn td_btn delete_personal_form_field" deleteid="'+fieldId+'"><span>&nbsp;</span></a></span></div>';
                        
                        if(data.saveType=='insert'){
                            $("#"+parentFormId+" .custom_field_div").append(fieldHtml);
                            $("#add_personal_form_field_popup").modal('hide');
                        } else if(data.saveType=='update'){
                            $("#"+parentFormId+" #masterfieldlbl_"+fieldId).html(capWords(fieldName));
                            $("#add_personal_form_field_popup").modal('hide');
                        }
                        e.preventDefault();
                        return false;
                    }
                }
            });
        }
    });
});
</script>
