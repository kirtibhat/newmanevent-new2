<?php if (!empty($registrantCategorydata)) {  //echo '<pre>';print_r($registrantCategorydata);    ?>
    <?php foreach ($registrantCategorydata as $key => $value) { $registrantCatId = $key; ?>
        <div class="panel event_panel co_cat co_cat_corporate">
            <div class="panel-heading ">
                <h4 class="panel-title medium dt-large ">
                    <a class="collapsed " data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $key ?>"><?php echo $value['category'] . " Registrants"; ?></a>
                    <input type="text" name="filterpackage<?php echo $key ?>" value="" id="filterpackage<?php echo $key ?>" placeholder="Filter Package" class="small short_field filterpackage" data-key="<?php echo $key ?>">
                </h4>
            </div>
            <div style="height: auto;" id="collapse<?php echo $key ?>" class="accordion_content collapse apply_content">

                <div class="sortable" style="height:auto;">
                <?php
                $recordCount = 0;
                $where = array('event_id'=>$eventId, 'category_id'=>$value['category_id']);
                $eventregistrantsdata = getDataFromTabel('event_registrants','*', $where,'','registrants_order, id','ASC');
                if(!empty($eventregistrantsdata)){
                foreach($eventregistrantsdata as $registrants){
                    $nextId = $recordCount+1;                 
                    //echo '<pre>';print_r($registrants->registrant_name);

                    //set registrant form data
                    $registrantId = $registrants->id;
                    $registrantName = (!empty($registrants->registrant_name))?$registrants->registrant_name:'';
                    $registrantPassword = (!empty($registrants->password))?$registrants->password:'';
                    $registrantDescription = (!empty($registrants->description))?$registrants->description:'';
                    $registrantCategoryId = (!empty($registrants->category_id))?$registrants->category_id:'';
                    $additionalDetailsVal = (!empty($registrants->additional_details))?$registrants->additional_details:'';
                    $allowEarlybirdVal = (!empty($registrants->allow_earlybird))?true:false;
                    $limitVal = (isset($registrants->registrants_limit))?$registrants->registrants_limit:'';
                    $limitEarlybirdVal = (isset($registrants->registrants_limit_early_bird))?$registrants->registrants_limit_early_bird:'';
                    $passwordVal = (!empty($registrants->password))?$registrants->password:'';
                    $unitPriceVal = (!empty($registrants->unit_price))?$registrants->unit_price:'';
                    $totalPriceVal = (!empty($registrants->total_price))?$registrants->total_price:'';
                    $gstVal = (isset($registrants->gst))?$registrants->gst:'';
                    $DateEarlyBirdVal = (!empty($registrants->earlybird_date))?$registrants->earlybird_date:'';
                    $registrantsType = (isset($registrants->registrants_type))?$registrants->registrants_type:'0';
                    $registrantsCategory = (isset($registrants->category_id))?$registrants->category_id:'0';

                    //set value for early bird field
                    $isDateEarlyBirdVal = 'dn';
                    $isDateEarlyBirdFieldDisable = true;
                    if(!empty($DateEarlyBirdVal)){
                      $isDateEarlyBirdVal = '';
                      $isDateEarlyBirdFieldDisable = true;
                      $DateEarlyBirdVal = dateFormate($DateEarlyBirdVal, "d M Y");
                    }

                    $unitPriceEarlyBirdVal = (!empty($registrants->unit_price_early_bird))?$registrants->unit_price_early_bird:'';
                    $totalPriceEarlyBirdVal = (!empty($registrants->total_price_early_bird))?$registrants->total_price_early_bird:'';
                    $gstEarlyBird = (isset($registrants->gst_early_bird))?$registrants->gst_early_bird:'';
                    $complementryVal = (!empty($registrants->complementry))?true:false;
                    $optionSingleDayRegistrationVal = (!empty($registrants->option_for_single_day_registration))?$registrants->option_for_single_day_registration:'';

                    // check condition for single day registration val

                    if($optionSingleDayRegistrationVal==1){
                        $singleDayRegisYesVal = true; 
                        $singleDayRegisNoVal = false;
                        $singleDayRegisDivIsShow = "";
                    }else{
                        
                        $singleDayRegisYesVal = false;  
                        $singleDayRegisNoVal = true;
                        $singleDayRegisDivIsShow = "dn";
                        
                    }

                    //set password check and uncheck
                    $passwordAccessYesChk = false;
                    $passwordAccessNoChk  = true;
                    $isPassDivshow = 'dn';
                    if(!empty($passwordVal)){
                      $passwordAccessYesChk = true;
                      $passwordAccessNoChk  = false;
                      $isPassDivshow = '';
                    } 
                      
                    //group value set
                    $groupBookingAllowedVal = (!empty($registrants->group_booking_allowed))?true:false;
                    $minGroupBookingSizeVal = (!empty($registrants->min_group_booking_size))?$registrants->min_group_booking_size:0;
                    $percentageDiscountGroupBookingVal = (!empty($registrants->percentage_discount_for_group_booking))?$registrants->percentage_discount_for_group_booking:'0';

                    //single group value set
                    $groupBookingAllowedSingledayRegistrantVal = (!empty($registrants->group_booking_allowed_for_singleday_registrant))?true:false;
                    $minGroupBookingSingleDayVal = (!empty($registrants->min_group_booking_size_for_single_day_registrant))?$registrants->min_group_booking_size_for_single_day_registrant:' ';
                    $percentageDiscountSingle_DayVal = (!empty($registrants->percentage_discount_for_single_day_registrant))?$registrants->percentage_discount_for_single_day_registrant:'0';

                    //form create variable
                    $formRegistrantType = array(
                        'name'   => 'formRegistrantTypeSave',
                        'id'   => 'formRegistrantTypeSave',
                        'method' => 'post',
                        'class'  => 'form-horizontal',
                        'data-parsley-validate' => '',
                      );

                    $registrantCategory = array(
                        'name' => 'registrantCategory' . $registrantId,
                        'value' => $registrantCategoryId,
                        'id' => 'registrantCategory' . $registrantId,
                        'type' => 'hidden',    
                    );
  
                      
                    $additionalDetails = array(
                        'name'  => 'additionalDetails'.$registrantId,
                        'value' => $additionalDetailsVal,
                        'id'  => 'additionalDetails'.$registrantId,
                      //  'required'  => '',
                        'rows' => '3',
                      );
                      
                    $allowEarlybird = array(
                        'name'  => 'allowEarlybird'.$registrantId,
                        'value' => '1',
                        'id'  => 'allowEarlybird'.$registrantId,
                        'regisid' => $registrantId,
                        'type'  => 'checkbox',
                        'class' => 'mt10 allowEarlybirdselection',
                        'checked' => $allowEarlybirdVal,
                      );  

                    $passwordAccessNo = array(
                        'name'  => 'passwordAccess'.$registrantId,
                        'value' => '0',
                        'id'  => 'passwordAccessNo'.$registrantId,
                        'type'  => 'radio',
                        'class' => 'ispasswordaccess',
                        'passwordaccessid' => $registrantId,
                        'checked' => $passwordAccessNoChk,
                      );    
                      
                    $passwordAccessYes = array(
                        'name'  => 'passwordAccess'.$registrantId,
                        'value' => '1',
                        'id'  => 'passwordAccessYes'.$registrantId,
                        'type'  => 'radio',
                        'class' => 'ispasswordaccess',
                        'passwordaccessid' => $registrantId,
                        'checked' => $passwordAccessYesChk,
                      );  

                    $password = array(
                        'name'  => 'password'.$registrantId,
                        'value' => $passwordVal,
                        'id'  => 'password'.$registrantId,
                        'type'  => 'password',
                        'class' => 'small',
                      );  

                    $confirmpassword = array(
                        'name'  => 'confirmpassword'.$registrantId,
                        'value' => $passwordVal,
                        'id'  => 'confirmpassword'.$registrantId,
                        'type'  => 'password',
                        'class' => 'small',
                      );    
                      
                    $limit = array(
                        'name'  => 'limit'.$registrantId,
                        'value' => $limitVal,
                        'id'  => 'limit'.$registrantId,
                        'type'  => 'text',
                        'required'  => '',
                        'class' => 'short_field small numValue',
                        'autocomplete' => 'off',
                        'data-parsley-error-message' => lang('common_field_required'),
                        'data-parsley-error-class' => 'custom_li',
                        'data-parsley-trigger' => 'keyup',
                      );  
                        
                    $unitPrice = array(
                        'name'  => 'unitPrice'.$registrantId,
                        'value' => $unitPriceVal,
                        'id'  => 'unitPrice'.$registrantId,
                        'type'  => 'text',
                        'required'  => '',
                        'registrantId'  => $registrantId,
                        'class' => 'short_field small unitPriceEnter',
                      );    
                      
                    $GSTInclude = array(
                        'name'  => 'GSTInclude'.$registrantId,
                        'value' => $gstVal,
                        'id'  => 'GSTInclude'.$registrantId,
                        'type'  => 'text',
                        'readonly'  => 'true',
                        'class' => 'short_field small',
                      );  
                      
                    $totalPrice = array(
                        'name'  => 'totalPrice'.$registrantId,
                        'value' => $totalPriceVal,
                        'id'  => 'totalPrice'.$registrantId,
                        'type'  => 'text',
                        'required'=>'',
                        'autocomplete' => 'off',
                        'data-parsley-error-message' => lang('common_field_required'),
                        'data-parsley-error-class' => 'custom_li',
                        'data-parsley-trigger' => 'keyup',
                        'registrantId'  => $registrantId,
                        //'readonly'  => 'true',
                        'class' => 'short_field small unitPriceEnter numValue',
                      );    

                    $earlyBirdDate = array(
                        'name'  => 'earlyBirdDate'.$registrantId,
                        'value' => $DateEarlyBirdVal,
                        'id'  => 'earlyBirdDate'.$registrantId,
                        'type'  => 'text',
                        'readonly'  => '',
                        'disabled'  => '',
                        'class' => 'datepicker earlyBirdDaterequired',
                      );    

                    $earlyBirdLimit = array(
                        'name'  => 'earlyBirdLimit'.$registrantId,
                        'value' => $limitEarlybirdVal,
                        'id'  => 'earlyBirdLimit'.$registrantId,
                        'type'  => 'text',
                        'registrantId'  => $registrantId,
                        'class' => 'short_field small numValue earlyBirdLimit',
                        'data-parsley-error-message' => lang('common_field_required'),
                        'data-parsley-error-class' => 'custom_li',
                        'data-parsley-trigger' => 'keyup',
                      );    
                        
                    $earlyBirdUnitPrice = array(
                        'name'  => 'earlyBirdUnitPrice'.$registrantId,
                        'value' => $unitPriceEarlyBirdVal,
                        'id'  => 'earlyBirdUnitPrice'.$registrantId,
                        'type'  => 'text',
                        'disabled'  => '',
                        'registrantId'  => $registrantId,
                        'class' => 'width50per unitPriceEarlyBird earlyBirdDaterequired',     
                      );    
                        
                    $earlyGSTInclude = array(
                        'name'  => 'earlyGSTInclude'.$registrantId,
                        'value' => $gstEarlyBird,
                        'id'  => 'earlyGSTInclude'.$registrantId,
                        'type'  => 'text',
                        'disabled'  => '',
                        'readonly'  => '',
                        'class' => 'short_field small',
                      );  
                      
                    $earlyBirdPrice = array(
                        'name'  => 'earlyBirdPrice'.$registrantId,
                        'value' => $totalPriceEarlyBirdVal,
                        'id'  => 'earlyBirdPrice'.$registrantId,
                        'type'  => 'text',
                        //'required'  => '',
                        'registrantId'  => $registrantId,
                      //  'disabled'  => '',
                      //  'readonly'  => '',
                        'class' => 'short_field small unitPriceEarlyBird numValue',
                        'data-parsley-error-message' => lang('common_field_required'),
                        'data-parsley-error-class' => 'custom_li',
                        'data-parsley-trigger' => 'keyup',
                      );  
                        
                    //remove disable from early bird section field
                    if($isDateEarlyBirdFieldDisable){
                      unset($earlyBirdLimit['disabled']);
                      unset($earlyBirdDate['disabled']);
                      unset($earlyBirdUnitPrice['disabled']);
                      unset($earlyBirdPrice['disabled']);
                      unset($earlyGSTInclude['disabled']);
                    }   

                    $comaplimentary = array(
                        'name'  => 'comaplimentary'.$registrantId,
                        'value' => '1',
                        'id'  => 'comaplimentary'.$registrantId,
                        'type'  => 'checkbox',
                        'data-registrantId'=>$registrantId,
                        'class' => 'mt10 comaplimentary',
                        'checked' => $complementryVal,
                      );

                    $complementryDivClass = '';  
                    if( $complementryVal == true){
                      $complementryDivClass = 'dn';
                    }      
                      
                    $singleDayRegistrationYes = array(
                        'name'  => 'singleDayRegistration'.$registrantId,
                        'value' => '1',
                        'id'  => 'singleDayRegistrationYes'.$registrantId,
                        'type'  => 'radio',
                        'singledayregistration' => $registrantId,
                        'required'  => '',
                        'class' => 'issingledayregistration',
                        'autocomplete' => 'off',
                        'data-parsley-error-message' => lang('common_field_required'),
                        'data-parsley-error-class' => 'custom_li',
                        'data-parsley-trigger' => 'keyup',
                        
                        
                    );  
                    if($singleDayRegisYesVal){
                      $singleDayRegistrationYes['checked'] = true;
                    }

                    $singleDayRegistrationNo = array(
                        'name'  => 'singleDayRegistration'.$registrantId,
                        'value' => '0',
                        'id'  => 'singleDayRegistrationNo'.$registrantId,
                        'type'  => 'radio',
                        'singledayregistration' => $registrantId,
                        'required'  => '',
                        'class' => 'issingledayregistration mt10 pull-left'
                        
                    );    
                    if( $singleDayRegisNoVal){
                      $singleDayRegistrationNo['checked'] = true;
                    }

                      
                    $registrationDisplayedYes = array(
                        'name'  => 'registrationDisplayed'.$registrantId,
                        'value' => '1',
                        'id'  => 'registrationDisplayedYes'.$registrantId,
                        'type'  => 'radio',
                        'required'  => '',
                        'autocomplete' => 'off',
                        'data-parsley-error-message' => lang('common_field_required'),
                        'data-parsley-error-class' => 'custom_li',
                        'data-parsley-trigger' => 'keyup',
                        'checked' => true,
                    );  

                    $registrationDisplayedNo = array(
                        'name'  => 'registrationDisplayed'.$registrantId,
                        'value' => '0',
                        'id'  => 'registrationDisplayedNo'.$registrantId,
                        'type'  => 'radio',
                        'required'  => '',
                        'autocomplete' => 'off',
                        'data-parsley-error-message' => lang('common_field_required'),
                        'data-parsley-error-class' => 'custom_li',
                        'data-parsley-trigger' => 'keyup',
                      );      
                      

                    //-------------add and remove disable for group book fields section------------------//
                    //group book checkebox area   
                    $registrantGroupbookings = array(
                        'name'  => 'registrantGroupbookings'.$registrantId,
                        'value' => '1',
                        'id'  => 'registrantGroupbookings'.$registrantId,
                        'groupbookingsid' => $registrantId,
                        'type'  => 'checkbox',
                        'class' => 'registrantGroupbookings',
                        'checked' => $groupBookingAllowedVal,
                      );  
                      
                    $registrantGroupBookingsSize = array(
                        'name'  => 'registrantGroupBookingsSize'.$registrantId,
                        'value' => $minGroupBookingSizeVal,
                        'id'  => 'registrantGroupBookingsSize'.$registrantId,
                        'type'  => 'number',
                        'class' => 'small numValue',
                        //'required'  => '',
                        'autocomplete' => 'off',
                        'data-parsley-error-message' => lang('common_field_required'),
                        'data-parsley-error-class' => 'custom_li',
                        'data-parsley-trigger' => 'change',  
                        'disabled' => '',
                        'min' => '0',
                          'step'=>'1'
                    );    

                    $registrantGroupBookingsPercent = array(
                        'name'  => 'registrantGroupBookingsPercent'.$registrantId,
                        'value' => $percentageDiscountGroupBookingVal,
                        'id'  => 'registrantGroupBookingsPercent'.$registrantId,
                        'type'  => 'text',
                        'class' => 'small numValue clr_425968 spinner_selectbox',
                        'required'  => '',
                        'autocomplete' => 'off',
                        'data-parsley-error-message' => lang('common_field_required'),
                        'data-parsley-error-class' => 'custom_li',
                        'data-parsley-trigger' => 'keyup',
                        'disabled' => '',
                        'min' => '0',
                      );    

                    //check group book allow then show div otherwise hide
                    $isgroupBookingShowDiv = 'dn';
                    $isDisableGroup='disabled';
                    if($groupBookingAllowedVal){
                      $isgroupBookingShowDiv = '';
                      unset($registrantGroupBookingsSize['disabled']);
                      unset($registrantGroupBookingsPercent['disabled']);
                      $isDisableGroup='';
                    }
                      

                    //-------------add and remove disable for group book fields single day section------------------//
                    //single registraint book checkebox area 
                    $singleDayGroupbookings = array(
                        'name'  => 'singleDayGroupbookings'.$registrantId,
                        'value' => '1',
                        'id'  => 'singleDayGroupbookings'.$registrantId,
                        'type'  => 'checkbox',
                        'singledaygroupid' => $registrantId,
                        'class' => 'singleDayGroupbookings',
                        'checked' => $groupBookingAllowedSingledayRegistrantVal,
                        
                        
                      );

                    $registrantSingleDayGroupBookingsSize = array(
                        'name'  => 'registrantSingleDayGroupBookingsSize'.$registrantId,
                        'value' => $minGroupBookingSingleDayVal,
                        'id'  => 'registrantSingleDayGroupBookingsSize'.$registrantId,
                        'type'  => 'text',
                        'class' => 'width_50px clr_425968 spinner_selectbox',
                        //'required'  => '',
                        'disabled' => '',
                        'min' => '0',
                      );
                      
                    $singleDayGroupBookingsPercent = array(
                        'name'  => 'singleDayGroupBookingsPercent'.$registrantId,
                        'value' => $percentageDiscountSingle_DayVal,
                        'id'  => 'singleDayGroupBookingsPercent'.$registrantId,
                        'type'  => 'text',
                        'class' => 'width_50px clr_425968 spinner_selectbox',
                        //'required'  => '',
                        'disabled' => '',
                        'min' => '0',
                      );    

                    //check single group book allow then show div otherwise hide
                    $isSinglegroupBookingShowDiv = 'dn';
                    $isDisableSingleGroup='disabled';
                    if($groupBookingAllowedSingledayRegistrantVal){
                      $isSinglegroupBookingShowDiv = '';
                      unset($registrantSingleDayGroupBookingsSize['disabled']);
                      unset($singleDayGroupBookingsPercent['disabled']);
                      $isDisableSingleGroup='';
                    } 
                ?>
                <div style="height:auto;" class="panel event_panel sub_panel <?php echo $registrants->registrant_name ?>" id="registrantsid_<?php echo $registrants->id ?>">
                    <div class="panel-heading">
                        <h4 class="panel-title medium dt-large filter_packages<?php echo $registrantCatId; ?> <?php echo strtolower($registrants->registrant_name).$registrantCatId ?>" id="registrantsid_<?php echo $registrants->id ?>">
                        
                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion_sub" href="#collapseDefault<?php echo $registrants->id ?>"><?php echo (!empty($registrants->registrant_name)) ? $registrants->registrant_name:'' ?></a>
                            
                            <span class="heading_btn_wrapper">
                              <a href="javascript:void(0)" class="delete_btn td_btn delete_registrant" id="<?php echo $registrantId; ?>" deleteid="<?php echo $registrantId; ?>"><span>&nbsp;</span></a>
                              <a href="javascript:void(0)" class="eventsetup_btn td_btn add_edit_login_type" id="<?php echo $registrantId; ?>" registitle="<?php echo ucwords($registrantName); ?>" regispassword="<?php echo ucwords($registrantPassword); ?>" regiscategoryid="<?php echo $registrantsCategory; ?>"  regisdescrip="<?php echo ucwords($registrantDescription); ?>"><span>&nbsp;</span></a>
                              <a href="javascript:void(0)" class="copy_btn td_btn duplicate_registrant" id="<?php echo $registrantId; ?>" eventid="<?php echo $eventId; ?>"><span>&nbsp;</span></a>
                            </span>
                        </h4>
                    </div>

                    <div style="height:auto;"  id="collapseDefault<?php echo $registrants->id ?>" class="accordion_content collapse apply_content">
                    <?php echo form_open($this->uri->uri_string(),$formRegistrantType);
                    echo form_input($registrantCategory);
                    ?>
                        <div class="row-fluid-15">
                          <label for="mem_pwd_access"><?php echo lang('event_regist_regist_password_access'); ?><span class="astrik">*</span>
                            <span class="info_btn"><span class="field_info xsmall"><?php echo lang('event_regist_regist_password_access'); ?></span></span>
                          </label>
                          <div class="radio_wrapper ">
                            
                            <?php echo form_radio($passwordAccessYes); ?>
                            <label for="passwordAccessYes<?php echo $registrantId; ?>"><?php echo lang('comm_yes'); ?></label>
                            
                            <?php echo form_radio($passwordAccessNo); ?>
                            <label for="passwordAccessNo<?php echo $registrantId; ?>"><?php echo lang('comm_no'); ?></label>
                            
                          </div>
                        </div>
                        <!--end of row-fluid-->

                        <div id="password_access_div<?php echo $registrantId; ?>" class="<?php echo $isPassDivshow; ?>">
                          
                           <div class="row-fluid-15">
                            <label for="mem_pwd"><?php echo lang('event_regist_regist_password'); ?> <span class="astrik">*</span></label>
                            <?php echo form_input($password); ?>
                            <?php echo form_error('password'.$registrantId); ?>
                          </div>
                          <!--end of row-fluid-->

                          <div class="row-fluid-15">
                            <label for="mem_cpwd"><?php echo lang('event_regist_regist_confir_pass'); ?> <span class="astrik">*</span></label>
                            <?php echo form_input($confirmpassword); ?>
                            <?php echo form_error('confirmpassword'.$registrantId); ?>
                          </div>
                          <!--end of row-fluid-->
                          
                        </div>

                        <div class="row-fluid-15">
                            <label for="mem_desc"><?php echo lang('event_regist_additional'); ?>
                              <span class="info_btn"><span class="field_info xsmall"><?php echo lang('tooltip_regist_additional'); ?></span></span>
                            </label>
                             <?php echo form_textarea($additionalDetails); ?>
                            <?php echo form_error('additionalDetails'.$registrantId); ?>
                        </div>
                        <!--end of row-fluid-->

                         <div class="row-fluid-15">
                          <label for="mem_limit"><?php echo lang('event_regist_regist_limit'); ?> <span class="astrik">*</span>
                            <span class="info_btn"><span class="field_info xsmall"><?php echo lang('tooltip_regist_regist_limit'); ?></span></span>
                          </label>
                          <?php echo form_input($limit); ?>
                          <?php echo form_error('limit'.$registrantId); ?>
                        </div>
                        <!--end of row-fluid-->

                        <div class="row-fluid-15">
                          <label for="mem_compl"><?php echo lang('event_regist_complimentary'); ?>  <span class="astrik">*</span></label>
                          <span class="checkbox_wrapper">
                            <?php echo form_checkbox($comaplimentary); ?>
                            <label for="comaplimentary<?php echo $registrantId ?>" class="eventDcheck">&nbsp;</label>
                          </span>
                        </div>
                        <!--end of row-fluid-->

                        <div id="comaplimentaryDiv<?php echo $registrantId ?>" class="<?php echo $complementryDivClass; ?>">
                            <div class="row-fluid-15">
                              <label for="mem_totalprice"><?php echo lang('event_regist_total_price'); ?><span class="astrik">*</span>
                                <span class="info_btn"><span class="field_info xsmall"><?php echo lang('tooltip_regist_total_price'); ?></span></span>
                              </label>
                              <?php echo form_input($totalPrice); ?>
                              <?php echo form_error('totalPrice'.$registrantId); ?>
                            </div>
                            <!--end of row-fluid-->
                            
                             <div class="row-fluid-15">
                              <label for="mem_gst"><?php echo lang('event_regist_gst_include'); ?></label>
                              <?php echo form_input($GSTInclude); ?>
                              <?php echo form_error('GSTInclude'.$registrantId); ?>
                            </div>
                            <!--end of row-fluid-->
                        </div>
                          
                        <div class="row-fluid-15">
                          <label for="mem_bird"><?php echo lang('event_regist_allow_earlybird'); ?>
                            <span class="info_btn"><span class="field_info xsmall"><?php echo lang('tooltip_regist_allow_earlybird'); ?></span></span>
                          </label>
                          <span class="checkbox_wrapper">
                            <?php echo form_checkbox($allowEarlybird); ?>
                            <label for="allowEarlybird<?php echo $registrantId ?>" class="eventDcheck">&nbsp;</label>
                          </span>
                        </div>
                        <!--end of row-fluid-->

                        <div id="registrant_type_early_bird<?php echo $registrantId; ?>" class="<?php echo $isDateEarlyBirdVal; ?>">
                           <div class="row-fluid-15">
                            <label for="mem_birdlimit"><?php echo lang('event_regist_earlybird_limit'); ?>   <span class="astrik">*</span></label>
                            <?php echo form_input($earlyBirdLimit); ?>
                            <?php echo form_error('earlyBirdLimit'.$registrantId); ?>
                          </div>
                          <!--end of row-fluid-->
                          <div class="row-fluid-15">
                              <label for="mem_totalpricebird"><?php echo lang('event_regist_earlybird_price'); ?><span class="astrik">*</span></label>
                              <?php echo form_input($earlyBirdPrice); ?>
                              <?php echo form_error('earlyBirdPrice'.$registrantId); ?>
                          </div>
                          
                          <div class="row-fluid-15">
                            <label for="mem_gstbird"><?php echo lang('event_regist_earlybird_gst_inc'); ?></label>
                            <?php echo form_input($earlyGSTInclude); ?>
                            <?php echo form_error('earlyGSTInclude'.$registrantId); ?>
                          </div>
                          <!--end of row-fluid-->
                          
                          </div>  

                        <div class="row-fluid-15">
                          <label for="mem_bird"><?php echo lang('event_regist_grop_booking'); ?>
                            <span class="info_btn"><span class="field_info xsmall"><?php echo lang('tooltip_regist_grop_booking'); ?></span></span>
                          </label>
                          <span class="checkbox_wrapper typregisrcheck">
                            <?php echo form_checkbox($registrantGroupbookings); ?>
                            <label for="registrantGroupbookings<?php echo $registrantId ?>" class="eventDcheck">&nbsp;</label>
                          </span>
                        </div>
                        <!--end of row-fluid-->
                                
                                <div class="<?php echo $isgroupBookingShowDiv;  ?>" id="group_bookings_div_<?php echo $registrantId ?>">
                          
                          <div class="row-fluid-15">
                            <label class="control-label" for="inputEmail"><?php echo lang('event_regist_min_booking_size'); ?></label>
                             <span class="checkbox_wrapper">
                              <?php echo form_input($registrantGroupBookingsSize) ?>
                             </span>
                          </div>
                          
                          <div class="row-fluid-15">
                            <label class="control-label" for="inputEmail"><?php echo lang('event_regist_percent_diss'); ?></label>
                             <div class="rate">
                                <?php echo form_input($registrantGroupBookingsPercent) ?> 
                              </div>
                          </div>
                        </div>
                                            
                              <hr class="panel_inner_seprator" />
                              
                                 <div class="row-fluid-15">
                          <label for="mem_reg"><?php echo lang('event_regist_single_day_field'); ?>
                            <span class="info_btn"><span class="field_info xsmall"><?php echo lang('tooltip_regist_single_day_field'); ?></span></span>
                          </label>
                          <div class="radio_wrapper ">
                            
                            <?php echo form_radio($singleDayRegistrationYes); ?>
                              <label for="singleDayRegistrationYes<?php echo $registrantId ?>"><?php echo lang('comm_yes'); ?>  &nbsp; &nbsp;</label>

                            <?php echo form_radio($singleDayRegistrationNo); ?>
                            <label for="singleDayRegistrationNo<?php echo $registrantId ?>"><?php echo lang('comm_no'); ?>  &nbsp; &nbsp;</label>
                            <?php echo form_error('singleDayRegistration'.$registrantId); ?>
                          </div>
                        </div> 
                        <!------single day registrant list div start------>


                        <div id="day_date_list_<?php echo $registrantId; ?>" class="row-fluid-15 <?php echo $singleDayRegisDivIsShow; ?>">
                        <?php 
                           //get $registrant Daywise data by registraint id
                         /* $regisWhere = array('registrant_id'=>$registrantId);
                          $registrantDaywise = getDataFromTabel('event_registrant_daywise','*',$regisWhere,'','day','ASC');
                          
                          if(!empty($registrantDaywise)){
                            foreach($registrantDaywise as $daywiseData){
                              $getDayWiseArr[$daywiseData->day] = $daywiseData; 
                            }
                          }

                          $daydifference= datedaydifference($eventdetails->starttime,$eventdetails->endtime);
                          
                          if(!empty($daydifference)){
                            foreach($daydifference as $getdaydate){
                              //set array value in varibale
                              $getDay= $getdaydate['day'];  
                              $getDate= $getdaydate['date'];  
                              $regisDayId = $registrantId.'_'.$getDay;
                              
                              //set default value
                              $registrantDayVal = false;
                              $isRegisEdit = false;
                              $registrantEarlybirdDayChk    = false; 
                              $registrantDayGroupBookingChk = false; 
                              $isDayWiseDisplay = 'dn';
                              $isEarlyDayWiseDisplayButton = 'dn';
                              $isEarlyDayWiseDisplayDiv = 'dn';
                              $registrantDayLimitVal = '';
                              $registrantDayUnitPriceVal = '';
                              $registrantDayTotalPriceVal = '';
                              $registrantGSTIncludeVal = '';
                              $registrantEarlybirdDateVal = '';
                              $registrantEarlybirdDayUnitPriceVal = '';
                              $registrantEarlybirdDayPriceVal = '';
                              $registrantEarlybirdGSTIncludeVal = '';
                              $regDayWiseId = 0;
                               
                              //set value by get data
                              if(isset($getDayWiseArr[$getDay])){
                                $valueObj = $getDayWiseArr[$getDay];
                                $registrantDayVal = (empty($valueObj->total_price))?false:true;
                                $isRegisEdit = true;
                                $registrantEarlybirdDayChk = (!empty($valueObj->allow_earlybird))?true:false; 
                                $registrantDayGroupBookingChk = (!empty($valueObj->allow_day_group_booking))?true:false; 
                                $isDayWiseDisplay = (empty($valueObj->total_price))?'dn':'';
                                $isEarlyDayWiseDisplayButton = (!$registrantDayVal)?"dn":"";
                                $isEarlyDayWiseDisplayDiv = (!$registrantEarlybirdDayChk)?"dn":"";
                                $isEarlyDayGroupBookingDiv = (!$registrantDayGroupBookingChk)?"dn":"";
                                $registrantDayLimitVal = $valueObj->registrant_daywise_limit;
                                $registrantDayUnitPriceVal = $valueObj->unit_price;
                                $registrantDayTotalPriceVal = $valueObj->total_price;
                                $registrantGSTIncludeVal = $valueObj->gst;
                                $registrantEarlybirdDateVal =  $valueObj->date_early_bird;
                                if(!empty($registrantEarlybirdDateVal)){
                                  $registrantEarlybirdDateVal =  dateFormate($registrantEarlybirdDateVal, "d M Y");
                                }
                                $registrantEarlybirdDayUnitPriceVal = $valueObj->unit_price_early_bird;
                                $registrantEarlybirdDayPriceVal = $valueObj->total_price_early_bird;
                                $registrantEarlybirdGSTIncludeVal = $valueObj->gst_early_bird;
                                $regDayWiseId = $valueObj->id;
                              }
                                
                              //daynamic create day 
                              $registrantDay = array(
                                  'name'  => 'registrantDay'.$regisDayId,
                                  'value' => '1',
                                  'id'  => 'registrantDay'.$regisDayId,
                                  'type'  => 'checkbox',
                                  'registrantdayid' => $regisDayId,
                                  'class' => 'pull-left mt10  registrantDay disableAll_'.$registrantId,
                                  'checked' => $registrantDayVal,
                                );  
                              $registrantEarlybirdDay = array(
                                  'name'  => 'registrantEarlybirdDay'.$regisDayId,
                                  'value' => '1',
                                  'id'  => 'registrantEarlybirdDay'.$regisDayId,
                                  'type'  => 'checkbox',
                                  'earlybirddayid'  => $regisDayId,
                                  'class' => 'registrantEarlybirdDay pull-left ml10 mr15 checkbox disableAll_'.$registrantId,
                                  'checked' => $registrantEarlybirdDayChk,
                                );  
                              
                              $registrantDayGroupBooking = array(
                                  'name'  => 'registrantDayGroupBooking'.$regisDayId,
                                  'value' => '1',
                                  'id'  => 'registrantDayGroupBooking'.$regisDayId,
                                  'type'  => 'checkbox',
                                  'earlybirddayid'  => $regisDayId,
                                  'class' => 'pull-left ml10 mr15 checkbox disableAll_'.$registrantId,
                                  'checked' => $registrantDayGroupBookingChk,
                                );    
                              
                              
                              $registrantDayLimit = array(
                                'name'  => 'registrantDayLimit'.$regisDayId,
                                'value' => $registrantDayLimitVal,
                                'id'  => 'registrantDayLimit'.$regisDayId,
                                'type'  => 'text',
                              //  'required'  => '',
                                'disabled'  => 'disabled',
                                'class' => 'short_field small singledayclass'.$regisDayId.' disableAll_'.$registrantId,
                                'autocomplete' => 'off',
                                'data-parsley-error-message' => lang('common_field_required'),
                                'data-parsley-error-class' => 'custom_li',
                                'data-parsley-trigger' => 'keyup',
                              );  
                                
                              $registrantDayUnitPrice = array(
                                'name'  => 'registrantDayUnitPrice'.$regisDayId,
                                'value' => $registrantDayUnitPriceVal,
                                'id'  => 'registrantDayUnitPrice'.$regisDayId,
                                'type'  => 'text',
                                //  'required'  => '',
                                'registrantDayId' => $regisDayId,
                                'disabled'  => 'disabled',
                                'autocomplete' => 'off',
                                'data-parsley-error-message' => lang('common_field_required'),
                                'data-parsley-error-class' => 'custom_li',
                                'data-parsley-trigger' => 'keyup',
                                'class' => 'short_field small dayUnitPriceEarlyBird singledayclass'.$regisDayId.'  disableAll_'.$registrantId,
                              );  
                              
                              $registrantGSTInclude = array(
                                'name'  => 'registrantGSTInclude'.$regisDayId,
                                'value' => $registrantGSTIncludeVal,
                                'id'  => 'registrantGSTInclude'.$regisDayId,
                                'type'  => 'text',
                              //  'required'  => '',
                                'readonly'  => '',
                                'disabled'  => 'disabled',
                                'class' => 'short_field small  disableAll_'.$registrantId,
                              );
                              
                              $registrantDayTotalPrice = array(
                                'name'  => 'registrantDayTotalPrice'.$regisDayId,
                                'value' => $registrantDayTotalPriceVal,
                                'id'  => 'registrantDayTotalPrice'.$regisDayId,
                                'type'  => 'text',
                              //  'required'  => '',
                                'readonly'  => '',
                                'disabled'  => 'disabled',
                                'class' => 'width50per singledayclass'.$regisDayId.'  disableAll_'.$registrantId,
                              );  
                              
                              $registrantEarlybirdDate = array(
                                'name'  => 'registrantEarlybirdDate'.$regisDayId,
                                'value' => $registrantEarlybirdDateVal,
                                'id'  => 'registrantEarlybirdDate'.$regisDayId,
                                'type'  => 'text',
                                //'readonly'  => '',
                                'disabled'  => 'disabled',
                                'autocomplete' => 'off',
                                'data-parsley-error-message' => lang('common_field_required'),
                                'data-parsley-error-class' => 'custom_li',
                                'data-parsley-trigger' => 'keyup',
                                'class' => 'datetimepicker date_input singledayearlyclass'.$regisDayId.' disableAll_'.$registrantId,
                              );  
                                
                              $registrantEarlybirdDayUnitPrice = array(
                                'name'  => 'registrantEarlybirdDayUnitPrice'.$regisDayId,
                                'value' => $registrantEarlybirdDayUnitPriceVal,
                                'id'  => 'registrantEarlybirdDayUnitPrice'.$regisDayId,
                                'type'  => 'text',
                                'autocomplete' => 'off',
                                'data-parsley-error-message' => lang('common_field_required'),
                                'data-parsley-error-class' => 'custom_li',
                                'data-parsley-trigger' => 'keyup',
                              //  'required'  => '',
                                'registrantDayId' => $regisDayId,
                                'disabled'  => 'disabled',
                                'class' => 'width50per earlyDayUnitPriceEarlyBird singledayearlyclass'.$regisDayId.' disableAll_'.$registrantId,
                              );  
                              
                              $registrantEarlybirdGSTInclude = array(
                                'name'  => 'registrantEarlybirdGSTInclude'.$regisDayId,
                                'value' => $registrantEarlybirdGSTIncludeVal,
                                'id'  => 'registrantEarlybirdGSTInclude'.$regisDayId,
                                'type'  => 'text',
                              //  'required'  => '',
                                'disabled'  => 'disabled',
                                'readonly'  => '',
                                'class' => 'width50per disableAll_'.$registrantId,
                              );
                              
                              $registrantEarlybirdDayPrice = array(
                                'name'  => 'registrantEarlybirdDayPrice'.$regisDayId,
                                'value' => $registrantEarlybirdDayPriceVal,
                                'id'  => 'registrantEarlybirdDayPrice'.$regisDayId,
                                'type'  => 'text',
                              //  'required'  => '',
                                'disabled'  => 'disabled',
                                'readonly'  => '',
                                'autocomplete' => 'off',
                                'data-parsley-error-message' => lang('common_field_required'),
                                'data-parsley-error-class' => 'custom_li',
                                'data-parsley-trigger' => 'keyup',
                                'class' => 'width50per singledayearlyclass'.$regisDayId.' disableAll_'.$registrantId,
                              );  
                              
                              $hiddenDayField = array(
                                    'name' =>'days_regisid_'.$registrantId.'[]',
                                    'type'=>'hidden',
                                    'class' =>'registration_hidden_days_'.$regisDayId,
                                    'disabled'  => '',
                                    'value' => $getDay,
                                 );
                                  
                              $hiddenDateField = array(
                                  'name' =>'date_regisid_'.$registrantId.'[]',
                                  'type'=>'hidden',
                                  'class' =>'registration_hidden_days_'.$regisDayId,
                                  'disabled'  => '',
                                  'value' => dateFormate($getDate, "Y-m-d"),
                               );
                              
                              $regisDaywiseId = array(
                                    'name' =>'regis_daywiseid_'.$registrantId.'['.$getDay.']',
                                    'type'=>'hidden',
                                    'id' =>'registration_hidden_days_'.$regisDayId,
                                //    'disabled'  => '',
                                    'value' => $regDayWiseId,
                                 ); 
                              
                                //remove all disable for edit condtion
                                if($isRegisEdit){
                                  //check if true
                                  if($registrantDayVal){
                                    unset($registrantDayLimit['disabled']);
                                    unset($registrantDayUnitPrice['disabled']);
                                    unset($registrantDayTotalPrice['disabled']);
                                    unset($registrantGSTInclude['disabled']);
                                  }
                                  //check if true
                                  if($registrantEarlybirdDayChk){
                                    unset($registrantEarlybirdDate['disabled']);
                                    unset($registrantEarlybirdDayUnitPrice['disabled']);
                                    unset($registrantEarlybirdDayPrice['disabled']);
                                    unset($registrantEarlybirdGSTInclude['disabled']);
                                  }
                                  
                                  unset($hiddenDayField['disabled']);
                                  unset($hiddenDateField['disabled']);
                                //  unset($regisDaywiseId['disabled']);
                                }   
                            
                            ?> 
                            
                            <div class="reg_checkbox_wrapper">
                              <span class="checkbox_wrapper">
                                <input type="checkbox"  name="mem_reg1" id="mem_reg1">
                                <?php echo form_checkbox($registrantDay); ?>
                                <label for="registrantDay<?php echo $regisDayId; ?>"><?php echo  dateFormate($getdaydate['date'], "d M Y"); ?> - <?php echo lang('event_regist_day'); ?> <?php echo $getDay; ?></label>
                              </span>
                            </div>
                            
                            <div id="allow_day_<?php echo $regisDayId; ?>" class="<?php echo $isDayWiseDisplay; ?>">
                            
                                <div class="row-fluid-15">
                                  <label for="mem_birdlimit"><?php echo lang('event_regist_single_limit'); ?>   <span class="astrik">*</span>
                                    <span class="info_btn"><span class="field_info xsmall"><?php echo lang('tooltip_regist_single_limit'); ?></span>
                                  </label>
                                  <?php echo form_input($registrantDayLimit); ?>
                                  <?php echo form_error('registrantDayLimit'.$regisDayId); ?>
                                  
                                </div>
                                <!--end of row-fluid-->
                                
                                <div class="row-fluid-15">
                                  <label for="mem_totalpricebird"><?php echo lang('event_regist_single_unit_price'); ?> <span class="astrik">*</span></label>
                                  <?php echo form_input($registrantDayUnitPrice); ?>
                                  <?php echo form_error('registrantDayUnitPrice'.$regisDayId); ?>
                                </div>
                                <!--end of row-fluid-->
                                
                                <div class="row-fluid-15">
                                  <label for="mem_gstbird"><?php echo lang('event_regist_single_gst_include'); ?></label>
                                  <?php echo form_input($registrantGSTInclude); ?>
                                  <?php echo form_error('registrantGSTInclude'.$regisDayId); ?>
                                </div>
                                <!--end of row-fluid-->
                                
                                <div class="row-fluid-15" id="allow_day_group_button_<?php echo $regisDayId; ?>">
                                  <label for="mem_birdallow" class="ml_m8"> <?php echo lang('event_regist_allow_day_group_booking'); ?>  
                                    <span class="info_btn"><span class="field_info xsmall"><?php echo lang('tooltip_regist_single_allow'); ?></span></span>
                                  </label>
                                  <span class="checkbox_wrapper">
                                    <?php echo form_checkbox($registrantDayGroupBooking); ?>
                                    <label for="registrantDayGroupBooking<?php echo $regisDayId; ?>" class="width_auto ml0 mt10"></label> 
                                  </span>
                                </div>
                                <!--end of row-fluid-->
                                
                                <div class="row-fluid-15 <?php echo $isEarlyDayWiseDisplayButton; ?>" id="allow_earlybir_button_<?php echo $regisDayId; ?>">
                                  <label for="mem_birdallow"><?php echo lang('event_regist_allow_early_bird'); ?>
                                    <span class="info_btn"><span class="field_info xsmall"><?php echo lang('tooltip_regist_single_allow'); ?></span></span>
                                  </label>
                                  <span class="checkbox_wrapper">
                                    <?php echo form_checkbox($registrantEarlybirdDay); ?>
                                    <label for="registrantEarlybirdDay<?php echo $regisDayId; ?>" class="width_auto ml0 mt10 eventDcheck"></label> 
                                  </span>
                                </div>
                                <!--end of row-fluid-->
                                
                            </div>
                            
                            <div id="allow_earlybir_<?php echo $regisDayId; ?>" class="<?php echo $isEarlyDayWiseDisplayDiv; ?>">
                              
                               <div class="row-fluid-15">
                                <label for="mem_birdallowday"><?php echo lang('event_regist_single_early_date'); ?> 
                                  <span class="info_btn"><span class="field_info xsmall"><?php echo lang('tooltip_regist_single_early_date'); ?></span></span>
                                </label>
                                <div class="date_wrapper" id="datepicker<?php echo $regisDayId; ?>"> 
                                  <?php echo form_input($registrantEarlybirdDate); ?>
                                  <?php echo form_error('registrantEarlybirdDate'.$regisDayId); ?>
                                    <!--<span class="add-on"><a href="javascript:void(0)" class="datepicker_btn datetimepicker"  data-date-icon="icon-calendar" data-time-icon="icon-time"></a></span>-->
                                </div>
                              </div>
                              
                              <div class="row-fluid-15">
                                <label for="mem_birdallowday"><?php echo lang('event_regist_single_early_unit_price'); ?>
                                  <span class="info_btn"><span class="field_info xsmall"><?php echo lang('tooltip_regist_single_early_unit_price'); ?></span></span>
                                </label>
                                <div class="date_wrapper">
                                  <?php echo form_input($registrantEarlybirdDayUnitPrice); ?>
                                  <?php echo form_error('registrantEarlybirdDayUnitPrice'.$regisDayId); ?>
                                </div>
                              </div>
                                
                              <div class="row-fluid-15">
                                <label for="mem_birdallowday"><?php echo lang('event_regist_single_early_gst'); ?>
                                  <span class="info_btn"><span class="field_info xsmall"><?php echo lang('tooltip_regist_single_early_gst'); ?></span></span>
                                </label>
                                <div class="date_wrapper">
                                  <?php echo form_input($registrantEarlybirdGSTInclude); ?>
                                  <?php echo form_error('registrantEarlybirdGSTInclude'.$regisDayId); ?>
                                </div>
                              </div>
                              
                              <div class="row-fluid-15">
                                <label for="mem_birdallowday"><?php echo lang('event_regist_single_early_price'); ?>
                                  <span class="info_btn"><span class="field_info xsmall"><?php echo lang('tooltip_regist_single_early_price'); ?></span></span>
                                </label>
                                <div class="date_wrapper">
                                  <?php echo form_input($registrantEarlybirdDayPrice); ?>
                                  <?php echo form_error('registrantEarlybirdDayPrice'.$regisDayId); ?>
                                </div>
                              </div>       
                            </div>
                            
                              <?php 
                              echo form_input($hiddenDayField);
                              echo form_input($hiddenDateField);
                              echo form_input($regisDaywiseId);
                           }//end loop  
                           
                        }//end if
                        
                        */ 
                        ?>
                        </div>
                        <!--end of row-fluid-->
                        <!------single day registrant list div end------>

                        <hr class="panel_inner_seprator" />
                        <?php
                        //set registrant array
                        if(isset($eventregistrants[$recordCount]) && !empty($eventregistrants[$recordCount])){
                            $sendData['registrantsData']  = $eventregistrants[$recordCount];
                            $sendData['formSectionName']  = 'regisType';
                            $sendData['nextRecordId'] =   (isset($recordId[$nextId]))?$recordId[$nextId]:'0';
                            $sendData['recordCount']  = $recordCount;
                            $this->load->view('form_personal_details_for_form',$sendData);
                        }
                        ?>


                        <div class="btn_wrapper ">
                            <a href="#collapseSix_<?php echo $registrantId; ?>" class="pull-left scroll_top"><?php echo lang('comm_top'); ?></a>
                            <?php 
                            echo form_hidden('eventId', $eventId);
                            echo form_hidden('formActionName', 'registrantsDetails');
                            echo form_hidden('registrantId', $registrantId);

                            //button show of save and reset
                            $formButton['saveButtonId'] = 'id="'.$registrantId.'"'; // save button id
                            $formButton['showbutton']   = array('save','reset');
                            $formButton['labletext']  = array('save'=>lang('comm_save_changes'),'reset'=>lang('comm_reset'));
                            $formButton['buttonclass']  = array('save'=>'default_btn btn pull-right medium','reset'=>'default_btn btn pull-right medium');
                            $this->load->view('common_save_reset_button',$formButton);
                            ?> 
                            <!--<a href="#" class="default_btn btn pull-right medium">Save</a>
                            <a href="#" class="default_btn btn pull-right medium">Clear</a>-->
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
                <?php
                $recordCount++;
                }
                }
                ?>
                </div>
            </div>    
        </div>                         
        <?php
    } // end loop
} // end if
?>

<script type="text/javascript">
$(document).ready(function(){ 
  //registrant type details save
  ajaxdatasave('formRegistrantTypeSave','<?php echo $this->uri->uri_string(); ?>',true,true,false,true,true,'','',false);
});
</script>
