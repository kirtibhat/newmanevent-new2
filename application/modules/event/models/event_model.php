<?php
class Event_model extends CI_model{


  private $tablenmevent           = 'event';
  private $tablenmeventcategory       = 'event_categories';
  private $tablenmeventcustomcontact      = 'event_custom_contact';    
  private $tablenmeventregistrant       = 'event_registrants';
  private $tablenmeventcontactperson      = 'event_contact_person';
  private $tablenmeventcustomemail      = 'event_emails';
  private $tablenmeventcustomemailattach    = 'event_email_attachments';
  private $tablenmeventinvoice        = 'event_invoice';
  private $tablenmeventbankdetail       = 'event_bank_detail';
  private $tablenmeventtermconditions     = 'event_term_conditions';
  private $tablenmcity            = 'city';
  private $tablenmcompanylist         = 'company_list';
  private $tablenmpaymentregistrationfee    = 'payment_registration_fee';
  private $tablenmpaymentgateway        = 'payment_gateway';
  private $tablefloorplantype         = 'corporate_floor_plan_type';
  private $tabledefaultpackagebenefit     = 'corporate_package_benefit';
  private $tabledefaultadditionalpurchaseitem = 'corporate_purchase_item';
  private $tablecorporatepackage        = 'corporate_package';
  private $tablecorporatepackagebooths    = 'corporate_package_booths';
  private $tablecustomformfield       = 'custom_form_fields';
  private $tableeventextracustomfield       = 'event_extras_custom_fields';
  private $tablecustomizeforms       = 'customize_forms';
  private $tablenmeventsocialmedia      = 'event_social_media_custom';
  
  private $tablenmeventdefaultthemes      = 'event_default_themes';
  private $tablenmeventthemes      = 'event_themes_data';
  
  private $font_packages = 'font_packages';
  
  
    
  function __construct(){
    parent::__construct();    
       
  }
  
  /*
   * @description: This function is used to get event details
   * @param: $eventId
   * @type: array
   * @return: object
   */ 
  
  public function geteventCategories($eventId=0,$eventCategoryId=0){  
    
    if($eventCategoryId!=0){
      $this->db->where('category_id',$eventCategoryId);
    }else{
      $this->db->where('event_id',$eventId);
      $this->db->or_where('event_id','0');
    } 
    
    $query = $this->db->get($this->tablenmeventcategory);
    return $query->result();
  }
  
  /*
   * @description: This function is used to get event details
   * @param: $eventId
   * @type: array
   * @return: object
   */ 
  
  public function getEventCorporateCategory($eventId=0){  
    
    $this->db->where("is_corporate = '1' AND (event_id = '".$eventId."')");
    $query = $this->db->get($this->tablenmeventcategory);
    return $query->result();
  }
  
  
  
  
    
  /*
   * @description: This function is used to add new event category
   * @param: $eventId
   * @type: array
   * @return: object
   */ 
  
  public function addeventcategory($data){  
    $query = $this->db->insert($this->tablenmeventcategory,$data);
    return $this->db->insert_id();
  }
  
    /*
   * @description: This function is used to update event category
   * @param: $data,where
   * @type: array
   * @return: void
   */ 
  
  public function updateeventcategory($data,$where){  
    $this->db->where($where);
    $result=$this->db->update($this->tablenmeventcategory,$data);
    return ($result) ? TRUE : FALSE ;
  }
  
  
  /*
   * @description: This function is used to add new event category
   * @param: $eventId
   * @type: array
   * @return: void
   */ 
  
  public function addeventcustomcontact($data){ 
    $query = $this->db->insert_batch($this->tablenmeventcustomcontact,$data);
    return ($query) ? $this->db->insert_id() : FALSE;
  }
  

  
  /*
   * @description: This function is used to get data of event custom contact
   * @param: $eventCustomContactId
   * @type: array
   * @return: object
   */ 
  
  public function geteventcustomcontact($data){ 
    $this->db->where($data);
    $query = $this->db->get($this->tablenmeventcustomcontact);
    return $query->row();
  }
  
  
  /*
   * @description: This function is used to delete the custom contact records
   * @param: $data
   * @type: array
   * @return: void
  */ 
  
  public function deleteeventcustomcontact($contactPersonId){ 
    $this->db->where('contact_person_id',$contactPersonId);
    $query = $this->db->delete($this->tablenmeventcustomcontact);
    return ($query) ? TRUE : FALSE;
  }
  
  /*
   * @description: This function is used to get details of emails
   * @param: $data
   * @type: array
   * @return: void
  */
  public function geteventcustomemaildetails($data){
    $this->db->where($data);
    $query = $this->db->get($this->tablenmeventcustomemail);
    return ($query) ? $query->row() : FALSE;
  }
  
  /*
   * @description: This function is used to insert data to email table
   * @param: $data
   * @type: array
   * @return: void
  */
  public function addeventcustomemail($data){
    $query = $this->db->insert($this->tablenmeventcustomemail,$data);
    return $this->db->insert_id();
  }
  
  /*
   * @description: This function is used to insert data to email table
   * @param: $data
   * @type: array
   * @return: void
  */
  public function addeventemailattachment($data){
    $query = $this->db->insert($this->tablenmeventcustomemailattach,$data);
    return $this->db->insert_id();
  }
  
  
  /*
   * @description: This function is used to update data to email table
   * @param: $data
   * @type: array
   * @return: void
  */
  public function updateeventcustomemail($data,$where){
    $this->db->where($where);
    $query = $this->db->update($this->tablenmeventcustomemail,$data);
    return ($query) ? TRUE : FALSE;
  }
  
  /*
   * @description: This function is used to get event details
   * @param: $eventId
   * @type: array
   * @return: object
   */ 
  
  
  public function geteventdetails($eventId,$userId=0){  
    $this->db->select('te.*,tncp.id as contact_persion_id,tncp.*,tnbe.*,tntc.*');
    $this->db->select('tnei.event_header,tnei.invoice_name,tnei.abn,tnei.above_address as iabovechk,tnei.address as iaddress1,
    tnei.address_second as iaddress2,tnei.city as icity,tnei.state as istate,tnei.postcode as ipostcode,tnei.country as icountry,
    tnei.sent_to_email,tnei.sent_from_email,tnei.invoice_intro,tnei.is_abn,tnei.is_collect_gst,tnei.gst_rate');
    $this->db->from($this->tablenmevent.' as te');
    $this->db->join($this->tablenmeventcontactperson.' as tncp','te.id =tncp.event_id','left');
    $this->db->join($this->tablenmeventinvoice.' as tnei','te.id =tnei.event_id','left');
    $this->db->join($this->tablenmeventbankdetail.' as tnbe','te.id =tnbe.event_id','left');
    $this->db->join($this->tablenmeventtermconditions.' as tntc','te.id =tntc.event_id','left');
    $this->db->where('te.id',$eventId);
    if($userId!=0){
      $this->db->where('te.user_id',$userId);
    } 
    $query = $this->db->get();
    return $query->row();
  }
  
  
  /*
   * @description: This function is used to get event payment option details
   * @param: $eventId
   * @type: array
   * @return: object
   */ 
  
  
  public function getpaymentdetails($eventId){  
    $this->db->select('*');
    $this->db->from('payment_registration_fee');
    $this->db->where('event_id',$eventId);
    $query = $this->db->get();
    //echo $this->db->last_query();die();
    return $query->row();
  }
  
  
  
  /*
   * @description: This function is used to get company name list data
   * @param: $cityId
   * @param: $stateId
   * @return: object
   */ 
  
  
  public function getcompanylist($stateId,$cityId){ 
    $this->db->select('cmplist.id,cmplist.company_name,city.city_name');
    $this->db->from($this->tablenmcompanylist.' cmplist');
    $this->db->join($this->tablenmcity.' as city','city.id =cmplist.city_id','left');
    $this->db->where('cmplist.city_id',$cityId);
    $this->db->where('cmplist.state_id',$stateId);
    $query = $this->db->get();
    return $query->result();
  }
  
  
  
  /*
   * @description: This function is used to get registrant list 
   * @param: $eventId
   * @param: $sectionId
   * @return: object
   */ 
  
  
  public function getRegistrantList($eventId,$sectionId=''){  
    $this->db->select('*');
    $this->db->from($this->tablenmeventregistrant);
    $this->db->where('event_id',$eventId);
    $this->db->where('registrants_limit !=','');
    if(!empty($sectionId)){
      $this->db->where('(registrants_type = 0 or  registrants_type = '.$sectionId.')',NULL,FALSE);
    }else{
      $this->db->where('registrants_type ','0');
    } 
    $this->db->order_by("id", "asc");   
    $query = $this->db->get();
    
    return $query->result();
  }
  
  
  /*
   * @description: This function is used save corporate space type
   * @param: $eventId
   * @param: $sectionId
   * @return: int
   */ 
   
  public function insertCorporateSpaceType($data){
    $this->db->insert($this->tablefloorplantype,$data);
    return $this->db->insert_id();
  }
  
  /*
   * @description: This function is used Update corporate space type
   * @param: $eventId
   * @return: boolean
   */ 
   
  public function updateCorporateSpaceType($data,$id){
    $this->db->where('floor_plan_type_id',$id);
    $result=$this->db->update($this->tablefloorplantype,$data);
    return ($result) ? TRUE : FALSE;
  }
  
  /*
   * @description: This function is used get corporate space type Details
   * @param: $eventId
   * @return: object
   */ 
   
  public function getCorporateSpaceTypeDetails($data){
    $query = $this->db->get_where($this->tablefloorplantype,$data);
    return $query->row();
  }
  
  /*
   * @description: This function is used get total available booths
   * @param: $eventId
   * @return: object
   */
  public function getTotalAvailableBooths($data){
    $this->db->select('available_booth');
    $this->db->where($data);
    $query = $this->db->get($this->tablefloorplantype);
    return $query->result();
    
  }
  
  /*
   * @description: This function is used get  all corporate space type based on event Id
   * @param: $eventId
   * @return: object
   */ 
   
  public function getCorporateSpaceType($data){
    $query = $this->db->get_where($this->tablefloorplantype,$data);
    return $query->result();
  }
  
  /*
   * @description: This function is used get  all corporate space type based on event Id
   * @param: $eventId
   * @return: object
   */ 
   
  public function getCorporateUsedSpace($data){
        $this->db->select('sum(total_space_available) as used_space');
        $query = $this->db->get_where($this->tablefloorplantype,$data);
    return $query->row();
  }
  
  /*
   * @description: This function is used save default benefits
   * @param: $eventId
   * @return: int
  */ 
  public function insertcorporatedefaultbenefit($data){
     $this->db->insert_batch($this->tabledefaultpackagebenefit,$data);
     return $this->db->insert_id();
  }
  
  /*
   * @description: This function is used Update default benefits
   * @param: $updatedData,$benefitId
   * @return: int
  */ 
  public function updatecorporatedefaultbenefit($updatedData,$benefitId){
     $this->db->where('package_benefit_id',$benefitId); 
     $result =$this->db->update($this->tabledefaultpackagebenefit,$updatedData);
     return ($result) ? TRUE : FALSE;
  }
  
  
  /*
   * @description: This function is used delete default benefits
   * @param: array
   * @return: boolean
  */ 
  public function deletedefaultpackagebenefit($data){
     $query=$this->db->delete($this->tabledefaultpackagebenefit,$data);
     return ($query) ? TRUE : FALSE;
  }
  
  /*
   * @description: This function is used delete default benefits
   * @param: array
   * @return: boolean
  */ 
  public function deletecorporatespacetype($data){
     $query=$this->db->delete($this->tablefloorplantype,$data);
     return ($query) ? TRUE : FALSE;
  }
  
  
  /*
   * @description: This function is used save default benefits
   * @param: $eventId
   * @return: int
  */ 
  public function insertcorporatedefaultpackageItem($data){
     $this->db->insert($this->tabledefaultadditionalpurchaseitem,$data);
     return $this->db->insert_id();
  }
  
  /*
   * @description: This function is used to update default purchase item
   * @param: $eventId
   * @return: boolean
  */ 
  public function updatecorporatedefaultpackageItem($data,$id){
     $this->db->where('purchase_item_id',$id);
     $query = $this->db->update($this->tabledefaultadditionalpurchaseitem,$data);
   
     return ($query) ? TRUE : FALSE;
  }
  
  
  /*
   * @description: This function is used delete default purchase item
   * @param: array
   * @return: boolean
  */ 
  public function deletedefaultpurchaseitem($data){
     $query=$this->db->delete($this->tabledefaultadditionalpurchaseitem,$data);
     return ($query) ? TRUE : FALSE;
  }
  
  /*
   * @description: This function is used add corporate package
   * @param: array
   * @return: boolean
  */ 
  public function addcorporatepackage($data){
     $query=$this->db->insert($this->tablecorporatepackage,$data);
     return $this->db->insert_id();
  }
  
  /*
   * @description: This function is used to delete corporate package
   * @param: array
   * @return: boolean
  */ 
  public function deletecorporatepackage($where){
     $query=$this->db->delete($this->tablecorporatepackage,$where);
     return ($query) ? TRUE : FALSE;
  }
  
    
  /*
   * @description: This function is used to update corporate package
   * @param: array
   * @return: boolean
  */ 
  public function updatecorporatepackage($data,$where){
     $this->db->where($where);
     $query=$this->db->update($this->tablecorporatepackage,$data);
     return ($query) ? TRUE : FALSE;
  }
  
  /*
   * @description: This function is used to update corporate package
   * @param: array
   * @return: boolean
  */ 
  
  public function updatecorporateApplicationDetails($data,$where){
     $this->db->where($where);
     $this->db->update_batch($this->tablecustomformfield, $data, 'id'); 
     return TRUE;
  }
  
  /*
   * @description: This function is used to get corporate package benefit based on corporate id , 0 belongs to default package benefit
   * 
  */
  public function getCorporatePackageBenefit($data=array(),$corporateId=0){
    
    if(!empty($data)){
      $this->db->where($data);
    } 
    
    $this->db->where("(corporate_package_id='0' OR corporate_package_id='".$corporateId."')");
    $this->db->order_by('package_benefit_id','ASC');
    $result=$this->db->get($this->tabledefaultpackagebenefit);
  
    return $result->result();
  }
  
  /*
   * @description: This function is used to get corporate purchase item based on corporate id , 0 belongs to default purchase item
   * 
  */
  public function getCorporatePurchaseItem($data=array(),$corporateId=0){
    
    if(!empty($data)){
      $this->db->where($data);
    } 
    
    $this->db->where("(corporate_package_id='0' OR corporate_package_id='".$corporateId."')");
    $this->db->order_by('corporate_package_id','ASC');
    $result=$this->db->get($this->tabledefaultadditionalpurchaseitem);
    
    return $result->result();
  }
  
  /*
   * @description: get Already reserved booths for other packages for the same event id
   * 
  */
  public function getReservedBoothForOtherPackage($data=array(),$corporateId=0){
    
    if(!empty($data)){
      $this->db->where($data);
    } 
    
    $this->db->where_not_in('corporate_package_id', array($corporateId));
    $result=$this->db->get($this->tablecorporatepackagebooths);
    return $result->result();
  }
  
  /*
   * @description: This function is used to get corporate purchase booths
   * 
  */
  public function getCorporatePackageBooths($data){
    
    $this->db->select('count(id) as totalUsedSpace');
    $this->db->where($data);  
    $result=$this->db->get($this->tablecorporatepackagebooths);
    $count = $result->result();
    return $count[0]->totalUsedSpace;
  }
  /*
   * @description: This function is used to delete corporate booths
   * 
  */
  public function deleteCorporatePackageBooths($data){
    
    $result=$this->db->delete($this->tablecorporatepackagebooths,$data);
    return ($result) ? TRUE : FALSE;
  }
  
  /*
   * @description: This function is used to add corporate booths
   * 
  */
  
  public function addCorporatePackageBooths($data){
    
    $result = $this->db->insert_batch($this->tablecorporatepackagebooths,$data);
    return $this->db->insert_id();
  }
  
  /*
   * @description: This function is used to get corporate packages details
   * 
  */
  public function getExhibitionSpaceDetails($where=array()){
    
    $eventId       = (isset($where['event_id'])) ? $where['event_id'] : 0;
    $corporatePackageId  = (isset($where['corporate_package_id'])) ? $where['corporate_package_id'] : 0;
    $this->db->select('booth.floor_plan_type_id,booth.booth_position,plan.total_space_available');
    $this->db->from($this->tablecorporatepackagebooths.' as booth');
    $this->db->join($this->tablefloorplantype.' as plan','booth.floor_plan_type_id=plan.floor_plan_type_id');
    $this->db->where('booth.event_id',$eventId);
    $this->db->where('booth.corporate_package_id',$corporatePackageId);
    $query = $this->db->get();
    return (!empty($query)) ? $query->result() : FALSE;
    
  }
  
  /*
   * @description: This function is used to copy corporate package
   * @param: array
   * @return: boolean
  */ 
  
  public function copycorporatecategory($data,$id){
    
    $packageId = 0;
    
    // get corporate package details
    $query  =$this->db->get_where($this->tablecorporatepackage,array('id'=>$id));
    $result =$query->row();
    if($result){
        
      $data['description']        = $result->description;
      $data['total_price']        = $result->total_price;
      $data['gst']            = $result->gst;
      $data['is_approval_request']    = $result->is_approval_request;
      $data['request_password']       = $result->request_password;
      $data['subject_to_approval']    = $result->subject_to_approval;
      $data['password_required']      = $result->password_required;
      $data['password']           = $result->password;
      $data['price']            = $result->price;
      $data['registration_included']    = $result->registration_included;;
      $data['package_benefit_ids']    = $result->package_benefit_ids;;
      $data['purchase_Item_ids']      = $result->purchase_Item_ids;
      $data['attach_registration']    = $result->attach_registration;
      $data['limit_package_available']  = 0;
    
      
    $packageId = $this->addcorporatepackage($data);
    
    }
    return $packageId;
  }
  
  /*
   * @description: This function is used to copy benefit item and purchase item
   * @param: array
   * @return: boolean
  */ 
  
  public function copybenefitpurchaseitem($newpackageId,$packageId){
    
    $data =array();$benefitsInsertData=array();$purchaseInsertData=array();$packageBooths="";
    
    //get benefits based on corporate packageId
    $result = $this->getCorporatePackageBenefit($data,$packageId);
    if(!empty($result)){
      foreach($result as $value){
        if($value->corporate_package_id!=0){  
            $benefitsInsertData[] = array('event_id'=>$value->event_id,'corporate_package_id'=>$newpackageId,'benefit_name'=>$value->benefit_name); 
        }     
      } 
    }
    // insert benefit
    if(!empty($benefitsInsertData)){
      $this->db->insert_batch($this->tabledefaultpackagebenefit,$benefitsInsertData); 
    }
    
    //get purchased item based on corporate packageId
    $result = "";
    $result = $this->getCorporatePurchaseItem($data,$packageId);
    if(!empty($result)){
      foreach($result as $value){
        if($value->corporate_package_id!=0){  
          $purchaseInsertData[] = array('event_id'=>$value->event_id,'corporate_package_id'=>$newpackageId,'name'=>$value->name,'item_limit'=>$value->item_limit,'total_price'=>$value->total_price,'gst_include_price'=>$value->gst_include_price); 
        }     
      } 
    }
    
    // insert purchase
    if(!empty($purchaseInsertData)){
      $this->db->insert_batch($this->tabledefaultadditionalpurchaseitem,$purchaseInsertData); 
    }
    
    // get corporate_package_booths
    $query  =  $this->db->get_where($this->tablecorporatepackagebooths,array('corporate_package_id'=>$packageId));
    $result = $query->result();
    if(!empty($result)){
      foreach($result as $value){
        $packageBooths[] = array('floor_plan_type_id'=>$value->floor_plan_type_id,'corporate_package_id'=>$newpackageId,'booth_position'=>$value->booth_position,'event_id'=>$value->event_id);
      } 
    }
    
    if(!empty($packageBooths)){
      $this->db->insert_batch($this->tablecorporatepackagebooths,$packageBooths); 
    }
    
    
  } // end function
  
  /*
   * @description: This function is used to copy benefit item and purchase item
   * @param: array
   * @return: boolean
  */ 
  
  public function getRegistrantCategory($event_id=0){
      
    $this->db->select('registrant.id as registrant_id,registrant.registrants_limit,registrant.registrant_name,cat.category,cat.category_id ');    
    $this->db->from($this->tablenmeventregistrant.' as registrant');    
    $this->db->join($this->tablenmeventcategory.' as cat','cat.category_id=registrant.category_id');    
    $this->db->where('registrant.event_id',$event_id);
    $this->db->where('registrant.registrants_limit !=', 'NULL');
    
    $this->db->order_by('category_id','ASC');
    
    $query=$this->db->get();
    
    return $query->result();     
  }

  
  /*
   * @description: This function is used to update event extra item custom field
   * @param: array
   * @return: boolean
  */ 
  
  public function updateeventExtraCustomFileds($data,$where){
     $this->db->where($where);
     $this->db->update_batch($this->tableeventextracustomfield, $data, 'id'); 
     return TRUE;
  }


  /*
   * @description: This function is used to get previous colour themes
   * @param: $eventId, $userId
   * @type: array
   * @return: object
   */ 
  
  
  public function getpreviouscolourthemes($eventId,$userId){  
    $this->db->select('te.id, te.event_title,  tcf.colour_scheme,  tcf.background_colour,  tcf.main_colour,  tcf.highlight_colour,  tcf.font_package,  tcf.link_color'); 
    $this->db->from($this->tablenmevent.' as te');
    $this->db->join($this->tablecustomizeforms.' as tcf','te.id =tcf.event_id','left');
    $this->db->where('te.id !=',$eventId);        
    $this->db->where('te.user_id',$userId);       
    $query = $this->db->get();
    return $query->result();
  }


  /*
   * @description: This function is used to add new event social media
   * @param: $eventId
   * @type: array
   * @return: void
   */ 
  
  public function addeventcustomsocialmedia($data){ 
    $query = $this->db->insert_batch($this->tablenmeventsocialmedia,$data);
    return ($query) ? $this->db->insert_id() : FALSE;
  }

   /*
   * @description: This function is used to get data of event custom social media type
   * @param: $eventId
   * @type: array
   * @return: object
   */ 
  
  public function geteventcustomsocialmedia($data){ 
    $this->db->where($data);
    $query = $this->db->get($this->tablenmeventsocialmedia);
    return $query->row();
  }
  
  /* new event theme */
   public function geteventthemesdetails(){  
    $query = $this->db->get($this->tablenmeventdefaultthemes);
    return $query->result();
  }
  
  /* event used theme */
   public function geteventusedthemesdetails($event_id, $user_id){  
    $this->db->where('event_id',$event_id);
    $this->db->where('user_id',$user_id);
    $query = $this->db->get($this->tablenmeventthemes);
    return $query->result();
  }
  
  
  public function geteventthemesfonts()
  {
    $query = $this->db->get($this->font_packages);
    return $query->result();      
  }
  
    /* Get all theme event used & saved data */
   public function getThemeDataFromTable($event_id, $user_id,$themeId=''){     
      
    $this->db->where('event_id',$event_id);
	$this->db->where('user_id',$user_id);
    if($themeId!=''){ $themeId = decode($themeId); $this->db->where('id',$themeId); }
	$this->db->order_by("last_modified", "desc");
	$this->db->limit('1');
	$query = $this->db->get($this->tablenmeventthemes);
	return $query->result();
    
    
  }
  
  
}

?>
