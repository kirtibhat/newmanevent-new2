<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 * This class is used to manage for report controller logic and data
 *
 * @package   report manage
 * @author    Lokendra Meena
 * @email     lokendrameena@cdnsol.com
 * @since     Version 1.0
 * @filesource
 */
 
 
class Factory{
	 
	//set limit of show records 
	private $limit = '10'; 
	 
	
	function __construct(){
		//create instance object
		$this->ci =& get_instance();
		$this->ci->load->library(array('head','template','pagination_custom'));
		$this->ci->load->model(array('report_model'));
		$this->ci->load->helper(array('pdfexport_helper'));
	}
	
	//--------------------------------------------------------------------------------
	
	/* 
	 * @description: This function is used to show delegate reports filter view
     * @load: deletegate filter view
     * @return: void
	 */ 
	
	
	public function delegatefilter(){
		
		//current open eventId
		$eventId = currentEventId();
		
		//decode entered eventid
		$eventId = decode($eventId); 
		
		$whereEvent					= array('id'=>$eventId);
		$where 						= array('event_id'=>$eventId);
		$wherePersonalField 		= array('event_id'=>$eventId,'registrant_id !='=>'0');
		$data['eventData'] 			= 	$this->ci->common_model->getDataFromTabel('event','starttime,endtime',$whereEvent);
		$data['registrantData'] 	= 	$this->ci->common_model->getDataFromTabel('event_registrants','id, registrant_name',$where);
		$data['personalFieldData'] 	= 	$this->ci->common_model->getDataFromTabel('custom_form_fields','id, field_name',$wherePersonalField);
		$data['sideeventData'] 		= 	$this->ci->common_model->getDataFromTabel('side_event','id, side_event_name',$where);
		$data['breakoutsData'] 		= 	$this->ci->common_model->getDataFromTabel('breakouts','id, breakout_name,date',$where);
		$data['eventId'] 			= 	$eventId;
		
		
		$this->ci->template->load('template','report_delegate_filter',$data,true);
	}
	
	
	//--------------------------------------------------------------------------------
	
	/* 
	 * @access: public
	 * @descrition: This function is get registrant list data
	 * @return void
	 */ 
	
	
	public function registranttionlist(){
		
		//if user directy go on registration list page then redirect to filter page
		if(!$this->ci->input->post('submittype')){
			redirect(base_url('report/delegatefilter'));
		}
		
		$eventId = currentEventId();
		
		//decode entered eventid
		$eventId = decode($eventId); 
		
		//get record by page wise
		$registrantionlist = $this->ci->report_model->registranttionlist($eventId);
		
		//get selected field field will be show		
		$selectedField  = $this->ci->input->post('selectedField');	
	
		$typeOfReport   = $this->ci->input->post('typeofreport');	
		
		//check condition for sponsor and exhibitor
		if($typeOfReport=="sponsorshipReport" || $typeOfReport=="exhibitorReport"){
			redirect(base_url('report/delegatefilter'));
		}
		
		$delegateListDataByType = "";
		$registrationColumCount = "0";
		//set default
		$frntSideEventIdArray = "";
		if(!empty($registrantionlist)){
			
			foreach($registrantionlist as $registrantion){
				
				//convert object to array
				$registrantion = (array) $registrantion;
				
				//registrant form field data and field name
				$getRegistrantFields		 =  getRegistrantAllFields($registrantion['id']);
				
				//check and selected value will be show
				if(!empty($selectedField)){
					foreach($getRegistrantFields as $key => $value){
						if(array_search($key,$selectedField)===false){
							//remove not selected field
							unset($getRegistrantFields[$key]);
						}							
					}
				}
				
				$registrationIdArray[] = $registrantion['id'];
				
				//side event additiona
			
				if(0 < $registrantion['frnt_additional_guest']){
					$frntSideEventIdArray[] = $registrantion['frntsideeventid'];
				}
				
				
				
				//prepare array for csv export
				$registrationColumCount	 	 = array('personal_count'=>count($getRegistrantFields));	
				$registrationDetails	 	 = array('registration_details'=>$registrantion,'personal_details'=>$getRegistrantFields);	
				$delegateListDataByType[]    = $registrationDetails;
			}
		
		}
		
		
		
		
		//-----------side event related information show here start------------------//
		
		if($typeOfReport == "sideeventReport" || $typeOfReport == "fullReport")
		{
			//get data from event table
			$getSieeventData 	=  	$this->ci->report_model->regissideeventlist($eventId);
			
			$sideventShowData = array();
			if(!empty($getSieeventData)){
				
				foreach($getSieeventData as $sieeventData){
					
					$sideEventId =  $sieeventData['side_event_id'];
					
					$countEvent = count(search($sideventShowData, 'side_event_id', $sideEventId));
					//only one record need to insert
					if(empty($countEvent)){
						$sideventShowData[] = $sieeventData;
					}
					
				}
			}
			
			
			//registrationsideevent.
			if(!empty($frntSideEventIdArray)){
					$registrationsideevent 	=  	$this->ci->report_model->registrationsideevent($frntSideEventIdArray);
			}
			
			$countRow5=0;
			if(!empty($delegateListDataByType)){
				foreach($delegateListDataByType as $delegateListArr){
					
						
						//get side event attend id
						
						$frntsideeventid = $delegateListArr['registration_details']['frntsideeventid'];
						$frntadditionalguest = $delegateListArr['registration_details']['frnt_additional_guest'];
						
						//get side event attendins data
						if(!empty($registrationsideevent)){
							$getAttendSideData = search($registrationsideevent, 'frnt_side_event_id', $frntsideeventid);
						}
						
						//user selected side event data list
						$sideEventListShow = array();
						
						if(!empty($sideventShowData)){
							$countRow7=0;
							foreach($sideventShowData as $sidEventArr){
								
								$sideEventListShow[$countRow7]['side_event_name']   = $sidEventArr['side_event_name'];
								$sideEventListShow[$countRow7]['Number_Attending']  = (!empty($frntadditionalguest))?$frntadditionalguest:"0";
								$maxAddtionGuest			= $sidEventArr['maximum_number_of_additional_guest'];
								
								$guestFieldList = array();
								$countRow=0;
								
								for($k=1;$k<=$maxAddtionGuest;$k++){
									$guestFieldList[$countRow]['Guest '.$k.' Title'] = (!empty($getAttendSideData[$countRow]['details'])?$getAttendSideData[$countRow]['details']:"");
									$guestFieldList[$countRow]['Guest '.$k.' First Name'] =  (!empty($getAttendSideData[$countRow]['guest_firstname'])?$getAttendSideData[$countRow]['guest_firstname']:"");
									$guestFieldList[$countRow]['Guest '.$k.' Surname'] =  (!empty($getAttendSideData[$countRow]['guest_surname'])?$getAttendSideData[$countRow]['guest_surname']:"");
									$guestFieldList[$countRow]['Guest '.$k.' Dietary Requirement'] =  (!empty($getAttendSideData[$countRow]['dietary_name'])?$getAttendSideData[$countRow]['dietary_name']:"");
									$countRow++;
								}
								$sideEventListShow[$countRow7]['side_event_list_count'] = $countRow * 4 + 1; // ;lastrow + 4 colom + 1 "no. attending colom"	
								$sideEventListShow[$countRow7]['side_event_list'] = $guestFieldList;	
								unset($guestFieldList);
								
								$countRow++;
								
							}
						}
						
						$delegateListDataByType[$countRow5]['sideevent'] = $sideEventListShow;
						unset($sideEventListShow);
						
						
					$countRow5++;
				}
			}
			
		}
	
		//-----------side event related information show here end------------------//
		
		
		//-------------breakout realated information show here start-------------//
		
		if($typeOfReport == "breakoutReport" || $typeOfReport == "fullReport")
		{
		
			//get data from event table
			$whereEventIdRef	= array('event_id'=>$eventId);
			$breakoutData 	=  	$this->ci->common_model->getDataFromTabel('breakouts','id,breakout_name,number_of_streams',$whereEventIdRef);
			
			//this varible is use to show breakout data
			$breakoutShowData;
			$brRowCount = 0;
			if(!empty($breakoutData)){
				
				foreach($breakoutData  as $breakout){
					
					$breakoutId = $breakout->id;
					$breakoutName = $breakout->breakout_name;
			
					//assign breakout name
					$breakoutShowData[$brRowCount]['breakoutid'] = $breakoutId;
					$breakoutShowData[$brRowCount]['breakoutname'] = $breakoutName;
					
					$whereBreakoutStream	= 	array('breakout_id'=>$breakoutId);
					$breakoutStreamData 	=  	$this->ci->common_model->getDataFromTabel('breakout_stream','id,stream',$whereBreakoutStream);
					
					if(!empty($breakoutStreamData)){
						
						foreach($breakoutStreamData  as $breakoutStream){
							
							$breakoutStreamId 			= $breakoutStream->id;
							$breakoutStreamNumber   	= $breakoutStream->stream;
					
							$whereBreakoutSession	= 	array('stream_id'=>$breakoutStreamId);
							$breakoutSessionData 	=  	$this->ci->common_model->getDataFromTabel('breakout_session','id,session',$whereBreakoutSession);
							
							if(!empty($breakoutSessionData)){
								
								foreach($breakoutSessionData  as $breakoutSession){
								
									$breakoutSessionId 			= $breakoutSession->id;
									$breakoutSessionNumber   	= $breakoutSession->session;
									$breakoutStreamSessionId    = $breakoutStreamNumber.'_'.$breakoutSessionNumber;
									
									$breakoutShowData[$brRowCount]['streamsession'][$breakoutStreamSessionId]['stream']  =  $breakoutStreamNumber;
									$breakoutShowData[$brRowCount]['streamsession'][$breakoutStreamSessionId]['session'] = $breakoutSessionNumber;
									$breakoutShowData[$brRowCount]['streamsessionlist'][] = $breakoutStreamSessionId;
									
								}	
							}
							
						}
					}
					
					$brRowCount++;	
				}
			}
			
			
			
			//breakout registrant data
			if(!empty($registrationIdArray)){
				$registrationbreakout= $this->ci->report_model->registrationbreakout($registrationIdArray);
			}
			if(empty($registrationbreakout)){
				$registrationbreakout = "";
			}
					
			//print_r(search($registrationbreakout, 'registered_user_id', '54'));
			$registIdCount = 0;
			if(!empty($registrationIdArray)){
				
				foreach($registrationIdArray as $registrationId){
					
						$registBreakoutListData = search($registrationbreakout, 'registered_user_id', $registrationId);
						
						$rowCount = 0;
						if(!empty($breakoutShowData)){
							
							foreach($breakoutShowData as $key => $value){
								$breakAttenList = array();
								
								$streamSessionList 		= $value['streamsessionlist'];
								$breakoutId			 	= $value['breakoutid'];
								$breakoutName 			= $value['breakoutname'];
								
								if(!empty($streamSessionList)){
									foreach($streamSessionList as $streamSession){
										$streamSessionKey = $streamSession;
										
										if(!empty($registBreakoutListData)){
											$checkBrkCount = 0;
											foreach($registBreakoutListData as $registBreakoutList){
												$regisStremSessionKey = $registBreakoutList['stream'].'_'.$registBreakoutList['session'];
												$regisbreakoutId = $registBreakoutList['breakout_id'];
												if($streamSessionKey == $regisStremSessionKey && $breakoutId==$regisbreakoutId){
													$checkBrkCount = 1;
												}
											}
											
											$breakAttenList[$streamSessionKey] = $checkBrkCount;
											
										}else{
											
											$breakAttenList[$streamSessionKey]= '0';
											
										}
										
									}
								}
								
								$breakAttenListData[$rowCount]['breakoutname'] = $breakoutName;
								$breakAttenListData[$rowCount]['breakoutcount'] = count($breakAttenList);
								$breakAttenListData[$rowCount]['breakoutlist'] = $breakAttenList;
								unset($breakAttenList);
								$rowCount++;
								
							}
						}
						
						$delegateListDataByType[$registIdCount]['breakout'] = $breakAttenListData;
						unset($userAttendBreakout);
						
					$registIdCount++;	
				}
			}
		
		}
		//-------------breakout realated information show here start-------------//
		
		//get data from event table
		$whereEvent	= array('id'=>$eventId);
		$eventData 			= 	$this->ci->common_model->getDataFromTabel('event','event_title,starttime,endtime,event_location',$whereEvent);
		
		if(!empty($eventData)){
			$eventData =  $eventData['0']; 
		}
		
		//get event data registrant table check by condition
		if($this->ci->input->post('registrant')){
			$fieldName = 'id';
			$whereRegisEvent =  $this->ci->input->post('registrant');
		}else{
			$fieldName = 'event_id';
			$whereRegisEvent =  $eventId;
		}
		
		//get data from registrant table
		$registrantData 	= 	$this->ci->common_model->getDataFromTabelWhereIn('event_registrants','id,registrant_name,registrants_limit,registrants_type',$fieldName,$whereRegisEvent);
		
		if(!empty($registrantData)){
			
			$recordCount = 0;
			foreach($registrantData as $registrant){
				
				$registrationId = $registrant['id'];
				
				$countRegis = 0;
				foreach($registrantionlist as $registrantion){
					
						$registrationRegisId= $registrantion->registrant_id;
						if($registrationRegisId==$registrationId){
							$countRegis++;
						}
				}
				
				//assign total registration for particulr registrant
				$registrantData[$recordCount]['regiscount'] =  $countRegis;
				$recordCount++;
			}	
				
		}
		
		/*
		echo "<pre>";
		
		print_r($delegateListDataByType);
		
		die();
		
		*/
		
		//get form submit type
		$submitType 	 = $this->ci->input->post('submittype');
		$submitTypeValue = $submitType[0];	
		
		switch($submitTypeValue){
			
			//export registration data in html formate
			case "html":
				$data['typeOfReport'] 				= 	$typeOfReport;
				$data['eventData'] 					= 	$eventData;
				$data['delegateListDataByType'] 	= 	$delegateListDataByType;
				$data['registrationColumCount'] 	= 	$registrationColumCount;
				$this->ci->template->load('template','report_registrant_list',$data,true);
			break;
			
			//export registration data in pdf formate
			case "pdf":
				$data['typeOfReport'] 				= 	$typeOfReport;
				$data['eventData'] 					= 	$eventData;
				$data['delegateListDataByType'] 	= 	$delegateListDataByType;
				$data['registrationColumCount'] 	= 	$registrationColumCount;
				$pdfData 		= $this->ci->load->view('registrant_list_pdt_view',$data,true);    
				$reportFileName = getReportNameLable($typeOfReport);   		
				exportPDF($pdfData,$reportFileName); 
			break;
			
			//export registration data in csv formate
			case "csv":
				//call record export function
				$this->_reportCSVExport($eventData, $registrantData, $delegateListDataByType,$typeOfReport);
			break;
		}	
	}
	
	
	/*
	 *  @access: public
	 *  @description: This function is used to export record in csv
	 *  @return: void
	 */ 
	
	public function _reportCSVExport($eventData, $registrantData, $delegateListDataByType,$typeOfReport){
		
			//current open related data preparing
			$eventName		 	=   ucwords($eventData->event_title);
			$eventLoction	 	=   ucwords($eventData->event_location);
			$eventStartDate  	=   dateFormate($eventData->starttime,'D, d M Y');
			$eventEndDate 		=   dateFormate($eventData->endtime,'D, d M Y');
			$currentEndDate 	=   dateFormate(date('y-m-d H:i'),'d-M-y H:i');
			$eventDays			=   datedaydifference($eventData->starttime,$eventData->endtime);
			$numberOfDays 		=   count($eventDays); 
			$eventDate = $eventStartDate.' - '.$eventEndDate.'[ '.$numberOfDays.' days]';
			
			//load our new PHPExcel library
			$this->ci->load->library('excel');
			
			//activate worksheet number 1
			$this->ci->excel->setActiveSheetIndex(0);
			//name the worksheet
			$this->ci->excel->getActiveSheet()->setTitle('Report');
		
		//----------Event related information row start-------------//
		
			//set common width of colomn
			$this->ci->excel->getActiveSheet()->getColumnDimension('A')->setWidth(25);
			$this->ci->excel->getActiveSheet()->getColumnDimension('B')->setWidth(25);
			
			//set cell A1 content with some text
			$this->ci->excel->getActiveSheet()->setCellValue('A1', $eventName);
			$this->ci->excel->getActiveSheet()->setCellValue('A2', getReportNameLable($typeOfReport));
			$this->ci->excel->getActiveSheet()->setCellValue('A3', 'Event Date');
			$this->ci->excel->getActiveSheet()->setCellValue('B3', $eventDate);
			$this->ci->excel->getActiveSheet()->setCellValue('A4', 'Event Location');
			$this->ci->excel->getActiveSheet()->setCellValue('B4', $eventLoction);
			
			//change the font size
			$this->ci->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(14);
			$this->ci->excel->getActiveSheet()->getStyle('A2')->getFont()->setSize(14);
			
			//make the font become bold
			$this->ci->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
			$this->ci->excel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);
		
		//----------Event related information row end-------------//
		
		
		//----------Registrant type related information row start-------------//
		
			//set registrant type listing
			$this->ci->excel->getActiveSheet()->setCellValue('A6', 'Registration Type');
			$this->ci->excel->getActiveSheet()->setCellValue('B6', 'Total');
			$this->ci->excel->getActiveSheet()->setCellValue('C6', 'Limits');
			
			//set text bold
			$this->ci->excel->getActiveSheet()->getStyle('A6')->getFont()->setBold(true);
			$this->ci->excel->getActiveSheet()->getStyle('B6')->getFont()->setBold(true);
			$this->ci->excel->getActiveSheet()->getStyle('C6')->getFont()->setBold(true);
			
			//set background color in cell of header
			$this->ci->excel->getActiveSheet()->getStyle('A6')->applyFromArray( array(
					'fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,
						'color' => array('rgb' => '969696')	)	) );
			
			$this->ci->excel->getActiveSheet()->getStyle('B6')->applyFromArray( array(
					'fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,
						'color' => array('rgb' => '969696')	)	) );
						
			$this->ci->excel->getActiveSheet()->getStyle('C6')->applyFromArray( array(
					'fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,
						'color' => array('rgb' => '969696')	)	) );			
			
			
			
			//show regitrant data list cell
			if(!empty($registrantData)){
				$countRow1=7;
				foreach($registrantData as $registrant){
					
					$registrantName 	= $registrant['registrant_name'].' ('.getRegistrantSectionName($registrant['registrants_type']).' )';
					$regiscount 		= $registrant['regiscount'];
					$registrantLimit 	= $registrant['registrants_limit'];
					
					$this->ci->excel->getActiveSheet()->setCellValue('A'.$countRow1, $registrantName);
					$this->ci->excel->getActiveSheet()->setCellValue('B'.$countRow1, $regiscount);
					$this->ci->excel->getActiveSheet()->setCellValue('C'.$countRow1, $registrantLimit);
					
					$countRow1++;
				}
			}
			
			//this code after two line new row will be show
			$countRow2 = $countRow1 + 2;
			
			//merge cell A1 until D1
			$this->ci->excel->getActiveSheet()->mergeCells('A'.$countRow2.':C'.$countRow2);
			
			//set right border solide
			$this->ci->excel->getActiveSheet()->getStyle('A'.$countRow2.':C'.$countRow2)->applyFromArray(
				array('borders' => array(
											'right'		=> array('style' => PHPExcel_Style_Border::BORDER_MEDIUM)
										)
					 )
				);
			
		//----------Registrant type related information row end-------------//
		
	
		//----------Registration Header Details  related information row start-------------//
		//cell text
		$this->ci->excel->getActiveSheet()->setCellValue('A'.$countRow2, 'Registration Details');
		
		//change the font size
		$this->ci->excel->getActiveSheet()->getStyle('A'.$countRow2)->getFont()->setSize(12);
		
		//set background color in cell of registration details
		$this->ci->excel->getActiveSheet()->getStyle('A'.$countRow2)->applyFromArray( array(
				'fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,
					'color' => array('rgb' => '7F7F7F')	)	) );
		
		//set aligment to center for that merged cell (A1 to D1)
		$this->ci->excel->getActiveSheet()->getStyle('A'.$countRow2)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			
		//make the font become bold
		$this->ci->excel->getActiveSheet()->getStyle('A'.$countRow2)->getFont()->setBold(true);	
		
		
		//this code after two line new row will be show
		$countRow3 = $countRow2+1;
		
		//cell text
		$this->ci->excel->getActiveSheet()->setCellValue('A'.$countRow3, 'Booking No.');
		$this->ci->excel->getActiveSheet()->setCellValue('B'.$countRow3, 'Booking Date');
		$this->ci->excel->getActiveSheet()->setCellValue('C'.$countRow3, 'Registration Type');
		
		//set text bold
		$this->ci->excel->getActiveSheet()->getStyle('A'.$countRow3)->getFont()->setBold(true);
		$this->ci->excel->getActiveSheet()->getStyle('B'.$countRow3)->getFont()->setBold(true);
		$this->ci->excel->getActiveSheet()->getStyle('C'.$countRow3)->getFont()->setBold(true);
		
		//set right border solide
		$this->ci->excel->getActiveSheet()->getStyle('A'.$countRow3.':C'.$countRow3)->applyFromArray(
			array('borders' => array(
										'right'		=> array('style' => PHPExcel_Style_Border::BORDER_MEDIUM)
									)
				 )
			);
		
		//set background color in cell of header
		$this->ci->excel->getActiveSheet()->getStyle('A'.$countRow3)->applyFromArray( array(
				'fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,
					'color' => array('rgb' => 'c0c0c0')	)	) );
		
		$this->ci->excel->getActiveSheet()->getStyle('B'.$countRow3)->applyFromArray( array(
				'fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,
					'color' => array('rgb' => 'c0c0c0')	)	) );
					
		$this->ci->excel->getActiveSheet()->getStyle('C'.$countRow3)->applyFromArray( array(
				'fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,
					'color' => array('rgb' => 'c0c0c0')	)	) );	
		
		
		//this code after two line new row will be show data
		$countRow4 = $countRow3+1;
		
		
		//----------Registration Header Details  related information row end-------------//
		
		
		$firstCount=0; // row count variable
		if(!empty($delegateListDataByType)){
		
			foreach($delegateListDataByType  as $registrantionData){
			
			
					//---------------registration details data display start ---------------//
						//cell text
						$this->ci->excel->getActiveSheet()->setCellValue('A'.$countRow4, $registrantionData['registration_details']['registration_booking_number']);
						$this->ci->excel->getActiveSheet()->setCellValue('B'.$countRow4, dateFormate($registrantionData['registration_details']['registration_date'],'d-M-Y'));
						$this->ci->excel->getActiveSheet()->setCellValue('C'.$countRow4, $registrantionData['registration_details']['registrant_name']);
						
						//set right border solide
						$this->ci->excel->getActiveSheet()->getStyle('A'.$countRow4.':C'.$countRow4)->applyFromArray(
							array('borders' => array(
														'right'		=> array('style' => PHPExcel_Style_Border::BORDER_MEDIUM)
													)
								 )
							);
					//---------------registration details data display end ---------------//
					
					
					//---------------personal details data display start ---------------//
					
						//get personal field data
						$personalDetailsData = $registrantionData['personal_details'];
						
						//record create from column number three
						$columnNumber = "3";
						if(!empty($personalDetailsData)){
							
							//first colomn number start
							$firstColomnNumber = $columnNumber;
								
							foreach($personalDetailsData as $key => $value){
								//this condition for showing column name
								if($firstCount==0){
									//heading of colomn on time only
									$this->ci->excel->getActiveSheet()->setCellValueByColumnAndRow($columnNumber, $countRow3, ucwords($key));
									
									//data formate code below	
									$fill = array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'startcolor' => array('rgb' => 'c0c0c0') );
									$this->ci->excel->getActiveSheet()->getStyleByColumnAndRow($columnNumber,$countRow3)->getFill()->applyFromArray($fill);
									$this->ci->excel->getActiveSheet()->getStyleByColumnAndRow($columnNumber,$countRow3)->getFont()->setBold(true);
								}
								
								$showValue = $value;
								if($key=="address"){
									$showValue = "";
									$getAddressValue= (array) json_decode($value);
									if(is_array($getAddressValue)){
										if(!empty($getAddressValue['addressName'])){
											$addressValue = (array) $getAddressValue['addressName'];
											if(!empty($addressValue) && array_filter($addressValue)){
												$showValue = implode(",", $addressValue);
											}
										}	
									}
								}
								
								
								//personal field data one by one
								$this->ci->excel->getActiveSheet()->setCellValueByColumnAndRow($columnNumber, $countRow4, $showValue);
								$columnNumber++;
							}
							
							$lastRecordColumnNum 	= $columnNumber -1;
							$nextColomnNumber  		=  $columnNumber;
						}
						
						
						
						//get personal details field coloumn number
						$lastColumnName 		= PHPExcel_Cell::stringFromColumnIndex($lastRecordColumnNum);
						$firstColomnName 			= PHPExcel_Cell::stringFromColumnIndex($firstColomnNumber);

						//merge cell A1 until D1
						$this->ci->excel->getActiveSheet()->mergeCells($firstColomnName.$countRow2.':'.$lastColumnName.$countRow2);
						
						//cell text
						$this->ci->excel->getActiveSheet()->setCellValue($firstColomnName.$countRow2, 'Personal Details');
						
						//set aligment to center for that merged cell (A1 to D1)
						$this->ci->excel->getActiveSheet()->getStyle($firstColomnName.$countRow2)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
							
						//make the font become bold
						$this->ci->excel->getActiveSheet()->getStyle($firstColomnName.$countRow2)->getFont()->setBold(true);	
						
						//set background color in cell of header
						$this->ci->excel->getActiveSheet()->getStyle($firstColomnName.$countRow2)->applyFromArray( array(
								'fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,
									'color' => array('rgb' => '969696')	)	) );
								
					//---------------personal details data display end ---------------//
					
					
					//------------sideevent section report show start-------------//
					
						//get personal field data
						$getSideeventData = (!empty($registrantionData['sideevent']))?$registrantionData['sideevent']:'';
						
						//$columnNumberSideevent = $columnNumber;
						$rowCount = 0;
						if($getSideeventData){

							foreach($getSideeventData as $getSideevent){
								
								$sideEventName  	  = $getSideevent['side_event_name'];
								$sideEventCount		  = $getSideevent['side_event_list_count'];
								$sideEventList 		  = $getSideevent['side_event_list'];
								$sideEventAttedingNo  = $getSideevent['Number_Attending'];
								
								//first colomn number start
								$firstColomnNumber = $columnNumber;
								
								//number of attending show colmn and data 
								$this->ci->excel->getActiveSheet()->setCellValueByColumnAndRow($columnNumber, $countRow3, ucwords('No. Attending'));
								$this->ci->excel->getActiveSheet()->setCellValueByColumnAndRow($columnNumber, $countRow4, $sideEventAttedingNo);
								
								//data formate style add code below	
								$fill = array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'startcolor' => array('rgb' => 'c0c0c0') );
								$this->ci->excel->getActiveSheet()->getStyleByColumnAndRow($columnNumber,$countRow3)->getFill()->applyFromArray($fill);
								$this->ci->excel->getActiveSheet()->getStyleByColumnAndRow($columnNumber,$countRow3)->getFont()->setBold(true);
								
								if(!empty($sideEventList)){
									
									//add one becoz above one hardcard colomn add
									$columnNumber = $columnNumber +1;
									
									foreach($sideEventList as $sideEventListArr){
										
										foreach($sideEventListArr as $key => $value){
											
											//this condition for showing column name
											if($firstCount==0){
												$this->ci->excel->getActiveSheet()->setCellValueByColumnAndRow($columnNumber, $countRow3, ucwords($key));
												
												//data formate code below	
												$fill = array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'startcolor' => array('rgb' => 'c0c0c0') );
												$this->ci->excel->getActiveSheet()->getStyleByColumnAndRow($columnNumber,$countRow3)->getFill()->applyFromArray($fill);
												$this->ci->excel->getActiveSheet()->getStyleByColumnAndRow($columnNumber,$countRow3)->getFont()->setBold(true);
											}
											
											//personal field data one by one
											$this->ci->excel->getActiveSheet()->setCellValueByColumnAndRow($columnNumber, $countRow4, $value);
											
											$columnNumber++;
										}
									}
									
									$lastRecordColumnNum 	= $columnNumber -1;
									$nextColomnNumber  		=  $columnNumber;
									
								}

								//sideevent style code	
								$lastColumnName 			= PHPExcel_Cell::stringFromColumnIndex($lastRecordColumnNum);
								$firstColomnName 			= PHPExcel_Cell::stringFromColumnIndex($firstColomnNumber);
								
								//merge cell A1 until D1
								$this->ci->excel->getActiveSheet()->mergeCells($firstColomnName.$countRow2.':'.$lastColumnName.$countRow2);
								
								//cell text
								$this->ci->excel->getActiveSheet()->setCellValue($firstColomnName.$countRow2, $sideEventName);
								
								//set aligment to center for that merged cell (A1 to D1)
								$this->ci->excel->getActiveSheet()->getStyle($firstColomnName.$countRow2)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
									
								//make the font become bold
								$this->ci->excel->getActiveSheet()->getStyle($firstColomnName.$countRow2)->getFont()->setBold(true);	
								
								//set background color in cell of header
								$this->ci->excel->getActiveSheet()->getStyle($firstColomnName.$countRow2)->applyFromArray( array(
										'fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,
											'color' => array('rgb' => 'BFBFBF')	)	) );
								
								$rowCount++;
							}

						}
					
					//------------sideevent section report show end-------------//
					
					
					//------------breakout section report show start-------------//
					
						//get personal field data
						$getBreakoutData = (!empty($registrantionData['breakout']))?$registrantionData['breakout']:'';
						
						$rowCount = 0;
						if($getBreakoutData){

							foreach($getBreakoutData as $getBreakout){
								
								$breakoutname  = $getBreakout['breakoutname'];
								$breakoutcount = $getBreakout['breakoutcount'];
								$breakoutlist  = $getBreakout['breakoutlist'];
								
								//brakout start first colomn name
								$firstColomnNumber = $columnNumber;
								
								if(!empty($breakoutlist)){
									
									foreach($breakoutlist as $key => $value){
										
										//this condition for showing column name
										if($firstCount==0){
											
											$breakoutValue= explode("_",$key);
											$breakoutValue1 = $breakoutValue[0];
											$breakoutValue2 =  $breakoutValue[1];
											$streamBreakoutName = "Stream ".$breakoutValue1." - Session ".$breakoutValue2;
											$this->ci->excel->getActiveSheet()->setCellValueByColumnAndRow($columnNumber, $countRow3, ucwords($streamBreakoutName));
											
											//data formate code below	
											$fill = array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'startcolor' => array('rgb' => 'c0c0c0') );
											$this->ci->excel->getActiveSheet()->getStyleByColumnAndRow($columnNumber,$countRow3)->getFill()->applyFromArray($fill);
											$this->ci->excel->getActiveSheet()->getStyleByColumnAndRow($columnNumber,$countRow3)->getFont()->setBold(true);
										}
										
										
										//personal field data one by one
										$this->ci->excel->getActiveSheet()->setCellValueByColumnAndRow($columnNumber, $countRow4, $value);
										
										$columnNumber++;
									}
									
									$lastRecordColumnNum 	= $columnNumber -1;
									$nextColomnNumber  		=  $columnNumber;
									
								}

								//breakout style code	
								$lastColumnName 			= PHPExcel_Cell::stringFromColumnIndex($lastRecordColumnNum);
								$firstColomnName 			= PHPExcel_Cell::stringFromColumnIndex($firstColomnNumber);
								
								//merge cell A1 until D1
								$this->ci->excel->getActiveSheet()->mergeCells($firstColomnName.$countRow2.':'.$lastColumnName.$countRow2);
								
								//cell text
								$this->ci->excel->getActiveSheet()->setCellValue($firstColomnName.$countRow2, $breakoutname);
								
								//set aligment to center for that merged cell (A1 to D1)
								$this->ci->excel->getActiveSheet()->getStyle($firstColomnName.$countRow2)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
									
								//make the font become bold
								$this->ci->excel->getActiveSheet()->getStyle($firstColomnName.$countRow2)->getFont()->setBold(true);	
								
								//set background color in cell of header
								$this->ci->excel->getActiveSheet()->getStyle($firstColomnName.$countRow2)->applyFromArray( array(
										'fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,
											'color' => array('rgb' => 'AD9DC1')	)	) );
								
								$rowCount++;
							}

						}
					//------------breakout section report show end-------------//
				
				
					//------------payment section report show start-------------//
					
						//payment related information show data
						if($typeOfReport == "fullReport"){ 
							
							for($i=0;$i<4;$i++){
								$nextBreakoutColumnName[] = PHPExcel_Cell::stringFromColumnIndex($columnNumber);
								$columnNumber++;
							}
							
							//cell text
							
							$totalAmount		= (!empty($registrantionData['registration_details']['total_amount']))?$registrantionData['registration_details']['total_amount']:"0";
							$paymentReceived	= (!empty($registrantionData['registration_details']['received_amount']))?$registrantionData['registration_details']['received_amount']:"0";
							$paymentRemaing		= $totalAmount  - $paymentReceived;
							$paymentAmount		= $paymentReceived;
							$paymentMode		= $registrantionData['registration_details']['payment_type'];
							$paymentDate		= date("d-M-Y",strtotime($registrantionData['registration_details']['payment_date']));
							
							if($firstCount==0){
								
								//merge cell A1 until D1
								$this->ci->excel->getActiveSheet()->mergeCells($nextBreakoutColumnName[0].$countRow2.':'.$nextBreakoutColumnName[3].$countRow2);
								
								//cell text
								$this->ci->excel->getActiveSheet()->setCellValue($nextBreakoutColumnName[0].$countRow2, 'Payment');
								
								//set aligment to center for that merged cell (A1 to D1)
								$this->ci->excel->getActiveSheet()->getStyle($nextBreakoutColumnName[0].$countRow2)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
									
								//make the font become bold
								$this->ci->excel->getActiveSheet()->getStyle($nextBreakoutColumnName[0].$countRow2)->getFont()->setBold(true);	
								
								//set background color in cell of header
								$this->ci->excel->getActiveSheet()->getStyle($nextBreakoutColumnName[0].$countRow2)->applyFromArray( array(
										'fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,
											'color' => array('rgb' => '7B7B7B')	)	) );
								
								
								//cell text
								$this->ci->excel->getActiveSheet()->setCellValue($nextBreakoutColumnName[0].$countRow3, 'Amount $.');
								$this->ci->excel->getActiveSheet()->setCellValue($nextBreakoutColumnName[1].$countRow3, 'Balance Remaining');
								$this->ci->excel->getActiveSheet()->setCellValue($nextBreakoutColumnName[2].$countRow3, 'Payment Method');
								$this->ci->excel->getActiveSheet()->setCellValue($nextBreakoutColumnName[3].$countRow3, 'Payment Date');
								
								//set text bold
								$this->ci->excel->getActiveSheet()->getStyle($nextBreakoutColumnName[0].$countRow3)->getFont()->setBold(true);
								$this->ci->excel->getActiveSheet()->getStyle($nextBreakoutColumnName[1].$countRow3)->getFont()->setBold(true);
								$this->ci->excel->getActiveSheet()->getStyle($nextBreakoutColumnName[2].$countRow3)->getFont()->setBold(true);
								$this->ci->excel->getActiveSheet()->getStyle($nextBreakoutColumnName[3].$countRow3)->getFont()->setBold(true);
								
								//set background color in cell of header
								$this->ci->excel->getActiveSheet()->getStyle($nextBreakoutColumnName[0].$countRow3)->applyFromArray( array(
										'fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,
											'color' => array('rgb' => 'C0BBBB')	)	) );	
								
								$this->ci->excel->getActiveSheet()->getStyle($nextBreakoutColumnName[1].$countRow3)->applyFromArray( array(
										'fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,
											'color' => array('rgb' => 'C0BBBB')	)	) );
								
								$this->ci->excel->getActiveSheet()->getStyle($nextBreakoutColumnName[2].$countRow3)->applyFromArray( array(
										'fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,
											'color' => array('rgb' => 'C0BBBB')	)	) );							
								
								$this->ci->excel->getActiveSheet()->getStyle($nextBreakoutColumnName[3].$countRow3)->applyFromArray( array(
										'fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,
											'color' => array('rgb' => 'C0BBBB')	)	) );	
									
							}	
							
							$this->ci->excel->getActiveSheet()->setCellValue($nextBreakoutColumnName[0].$countRow4, $paymentAmount);
							$this->ci->excel->getActiveSheet()->setCellValue($nextBreakoutColumnName[1].$countRow4, $paymentRemaing);
							$this->ci->excel->getActiveSheet()->setCellValue($nextBreakoutColumnName[2].$countRow4, $paymentMode);
							$this->ci->excel->getActiveSheet()->setCellValue($nextBreakoutColumnName[3].$countRow4, $paymentDate);
							
							//set right border solide
							$this->ci->excel->getActiveSheet()->getStyle('A'.$countRow4.':C'.$countRow4)->applyFromArray(
								array('borders' => array(
															'right'		=> array('style' => PHPExcel_Style_Border::BORDER_MEDIUM)
														)
									 )
								);
						
						}
					
					//------------payment section report show start-------------//
					
				$countRow4++;	
				$firstCount++;
				
			}
			
		}
		
		//for lool end 
		
		//get last row number of registration details page
		$lastRowNumber = $countRow4 - 1;
		//set bottom border solid
		$this->ci->excel->getActiveSheet()->getStyle('A'.$lastRowNumber.':C'.$lastRowNumber)->applyFromArray(
			array('borders' => array(
										'right'		=> array('style' => PHPExcel_Style_Border::BORDER_MEDIUM),
										'bottom'		=> array('style' => PHPExcel_Style_Border::BORDER_MEDIUM)
									)
				 )
			);
			
					
					
					
		
		
		//echo $personalColumnName;die();
		$filename=getReportNameLable($typeOfReport).'.xls'; //save our workbook as this file name
		header('Content-type: text/csv'); //mime type
		header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
		header('Cache-Control: max-age=0'); //no cache

		//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
		//if you want to save it as .XLSX Excel 2007 format
		$objWriter = PHPExcel_IOFactory::createWriter($this->ci->excel, 'Excel5');  
		//force user to download the Excel file without writing it to server's HD
		$objWriter->save('php://output');
		/*echo "<pre>";
		print_r($delegateListData);die();*/
		
	}
	
	
	
	/*
	 * @access: public
     * @description: This function is used to delete event register user data
     * @return: void
     *  
     */ 
	
	public function deleteeventuser(){
		
		$deleteId = $this->ci->input->post('deleteId');
		
		//delete data form contact person
		$whereCondi1 = array('id'=>$deleteId);
		$whereCondi2 = array('registered_user_id'=>$deleteId);
		//delete record form frnt_breakouts table
		$this->ci->common_model->deleteRowFromTabel('frnt_breakouts',$whereCondi2);
		//delete record form frnt_form_field_value table
		$this->ci->common_model->deleteRowFromTabel('frnt_form_field_value',$whereCondi2);
		
		//delete record form frnt_side_event_guest table
		$getFrntSideEvent = $this->ci->common_model->getDataFromTabel('frnt_side_event','id',$whereCondi2);
		if(!empty($getFrntSideEvent)){
			foreach($getFrntSideEvent as $frntSideEvent){
				$whereCondi3 = array('frnt_side_event_id'=>$frntSideEvent->id);
				$this->ci->common_model->deleteRowFromTabel('frnt_side_event_guest',$whereCondi3);
			}
		}
		
		//delete record form frnt_side_event table
		$this->ci->common_model->deleteRowFromTabel('frnt_side_event',$whereCondi2);
		
		//delete record form nm_frnt_registrant table
		$this->ci->common_model->deleteRowFromTabel('frnt_registrant',$whereCondi1);
		
		$msg = lang('report_msg_event_record_delete');
		$returnArray=array('msg'=>$msg,'is_success'=>'true');
		set_global_messages($returnArray);
		echo json_encode($returnArray);
	}
	
	//--------------------------------------------------------------------------------
	
	/* 
	 * @access: public
	 * @descrition: This function is get payments list data
	 * @return void
	 */ 
	
	
	public function paymentslist(){
		
		//get current user id
		$userId = isLoginUser();	
		
		$paymentsListData = $this->ci->report_model->paymentslist();
		
		//call function for preparing financial data	
		$paymentsListData = $this->_paymentDataPrepared($paymentsListData);
	
		$data['geteventslist']   	= $this->ci->report_model->geteventslist($userId);
		$data['geteventsdetails']  	= $this->ci->report_model->geteventsdetails();
		$data['paymentslistdata']   = $paymentsListData;
		$data['selecteventmsg']		= ($this->ci->input->post('eventid'))?lang('report_pay_event_select_fail_msg'):lang('report_pay_event_select_msg');
		
		if($this->ci->input->is_ajax_request()){
			
			$this->ci->load->view('view_payments_list',$data);
		}else{
			$this->ci->template->load('template','report_payments_list',$data,true);
		}
	}
	
	
	/*
	 * @description: payment type data prepared 
	 * @param: array
	 * @return: array
	 */ 
	
	function _paymentDataPrepared($dataArray){
		//assing blank array
		$returnArray = array();
		if(!empty($dataArray)){
			foreach($dataArray as $dataArr){
				
				//prepared cash data array
				if($dataArr->payment_type=='cash'){
					$returnArray['cash'][] = $dataArr;
				}
				
				//prepared cheque data array
				if($dataArr->payment_type=='cheque'){
					$returnArray['cheque'][] = $dataArr;
				}
				
				//prepared credit card data array
				if($dataArr->payment_type=='credit_card'){
					$returnArray['credit_card'][] = $dataArr;
				}
					
			}
		}
		
		return $returnArray;
	}
	
	
	
	
	//-------------------------------------------------------------------------
	
	/*
    * @access: public
    * @description: This function is used to change payment confirm status
    * @return: void
    *  
    */ 
	
	public function paymentconfirmstatus(){
		
		$paymentid 	 = $this->ci->input->post('paymentid');
		$getvalue = $this->ci->input->post('getvalue');
		$updateData = array('payment_received'=>$getvalue);
		$paymentsUserDetails = $this->ci->report_model->paymentsuserdetails($paymentid);
		$where = array('id'=>$paymentid);
		$whereRegis = array('payment_id'=>$paymentid);
		$this->ci->common_model->updateDataFromTabel('frnt_payment_details',$updateData,$where);	
		if($getvalue==1):
			//payment confirm 
			$updateDataRegis = $updateData = array('is_active'=>'1');
			$this->ci->common_model->updateDataFromTabelWhereIn('frnt_registrant',$updateDataRegis,$whereRegis);	
			$this->_paymentConfirm($paymentsUserDetails);
			$msg = lang('report_msg_payment_confirm_status_change');
		else:
			//payment cancel 
			$updateDataRegis = $updateData = array('is_active'=>'0');
			$this->ci->common_model->updateDataFromTabelWhereIn('frnt_registrant',$updateDataRegis,$whereRegis);
			$this->_paymentCancel($paymentsUserDetails);
			$msg = lang('report_msg_payment_cancel_status_change');
		endif;
		$returnArray=array('msg'=>$msg,'is_success'=>'true');
		echo json_encode($returnArray);
	}
	
	
	//--------------------------------------------------------------------------------
	
	/*
	 * @access: private
	 * @description: This function is used payment confirm email 
	 * @return void
	 */ 
	
	private function _paymentConfirm($emailData){
		
		//set email data
		$userEmail = $emailData->email; 
		//$userEmail = 'lokendrameena@cdnsol.com'; 
		$data['emailData'] = $emailData; 
		$subject = 'newmanevents: payment confirmation.';
		$this->ci->load->library('email');
		$this->ci->email->from($this->ci->config->item('from_email'));
		$this->ci->email->to($userEmail);
		$this->ci->email->subject($subject);
		$message = $this->ci->load->view('email/payment_confirm',$data,true);
		$this->ci->email->message($message);
		$this->ci->email->send(); 
	}
	
	
	//--------------------------------------------------------------------------------
	
	/*
	 * @access: private
	 * @description: This function is used  payment cancel email
	 * @return void
	 */ 
	
	private function _paymentCancel($emailData){
		
		//set email data
		$cancelMessage = lang('report_msg_cancellation_email_message');
		
		//check message enter then set entered message
		if($this->ci->input->post('cancelMessage')){
			$cancelMessage = $this->ci->input->post('cancelMessage');
		}	
		
		$userEmail = $emailData->email; 
		//$userEmail = 'lokendrameena@cdnsol.com'; 
		$data['emailData'] = $emailData; 
		$data['cancelMessage'] = $cancelMessage; 
		$subject = 'newmanevents: payment cancellation.';
		$this->ci->load->library('email');
		$this->ci->email->from($this->ci->config->item('from_email'));
		$this->ci->email->to($userEmail);
		$this->ci->email->subject($subject);
		$message = $this->ci->load->view('email/payment_cancel',$data,true);
		$this->ci->email->message($message);
		$this->ci->email->send(); 
	}
	
	
	//--------------------------------------------------------------------------------
	
	/*
	 * @access: public
	 * @description: This function is used to show payment cancel message view 
	 * @return void
	 */ 
	 
	public function paymentcancel(){
		$data['paymentid'] 	 = $this->ci->input->post('paymentid');
		$data['getvalue'] = $this->ci->input->post('getvalue');
		$this->ci->load->view('view_cancel_message',$data);
	} 
	
	//--------------------------------------------------------------------------------
	
	
	
	
}


/* End of file factory.php */
/* Location: ./system/application/modules/report/libraries/factory.php */
