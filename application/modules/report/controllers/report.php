<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * @description: This controller is used to manage report
 * @create date: 4-Apr-2014
 * @auther: Lokendra Meena
 * @email: lokendrameena@cdnsol.com
 * 
 */ 

class report extends MX_Controller{

	function __construct(){
		parent::__construct();
		$this->load->library(array('factory'));
		$this->load->language(array('report'));
		$this->form_validation->set_error_delimiters('<div class="server_msg_show">', '</div>');
		$this->session_check->checkSession(); 		
	}

	
	
	
	
	//----------------------------------------------------------------------------
	
    /*
     * @default load 
     * @description: This function is used to show delegate reports filter view
     * @load: deletegate filter view
     * @return: void
     *  
     */
    
    public function delegatefilter(){
		
		//call factory method for event register user data
		$this->factory->delegatefilter();
	}


	//----------------------------------------------------------------------------
	
    /*
     * @default load 
     * @description: This function is used to event register user data
     * @load: login view
     * @return: void
     *  
     */
    
    public function delegatereport(){
		
		//call factory method for event register user data
		$this->factory->registranttionlist();
	} 
	
	//----------------------------------------------------------------------------
	
    /*
     * @access: public
     * @description: This function is used to delete event register user data
     * @return: void
     *  
     */
	
	public function deleteeventuser(){
		//call factory method for delete event user data
		$this->factory->deleteeventuser();
	}
	
	//----------------------------------------------------------------------------
	
    /*
     * @default load 
     * @description: This function is used to payments data
     * @load: report_payments_view
     * @return: void
     *  
     */
    
    public function payments(){
		
		//call factory method for payments data
		$this->factory->paymentslist();
	} 
	
	//----------------------------------------------------------------------------
	
   /*
    * @access: public
    * @description: This function is used to change sponsor status
    * @return: void
    *  
    */
	
	public function paymentconfirmstatus(){
		//call factory method for to change sponsor status
		$this->factory->paymentconfirmstatus();
	}
	
	//----------------------------------------------------------------------------
	
    /*
     * @default load 
     * @description: This function is used to show payment cancel message view 
     * @load: view_cancel_message
     * @return: void
     *  
     */
	
	public function paymentcancel(){
		$this->factory->paymentcancel();
	}
	
	//----------------------------------------------------------------------------
	
   
}

/* End of file home.php */
/* Location: ./system/application/modules/report/controllers/home.php */
