<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	//event relate info data
	$eventName		 	=   ucwords($eventData->event_title);
	$eventStartDate  	=   dateFormate($eventData->starttime,'D, d M Y');
	$eventEndDate 		=   dateFormate($eventData->endtime,'D, d M Y');
	$currentEndDate 	=   dateFormate(date('y-m-d H:i'),'d-M-y H:i');
	$eventDays			=   datedaydifference($eventData->starttime,$eventData->endtime);
	$numberOfDays 		=   count($eventDays); 

?>
<div class="spacer40">
</div>
<div class="row-fluid">
<div class="headingh1"><?php echo lang('report_admin'); ?></div>
<div class="spacer40"></div>
<div class="finan_report_main">
<div class="report_head"><?php echo $eventName; ?></div>
<ul>
	<li><div>Date:</div> <?php echo $eventStartDate; ?> - <?php echo $eventEndDate; ?> [<?php echo $numberOfDays; ?> days]</li>
	<li><div>Date Printed:</div> <?php echo $currentEndDate; ?></li>
	<li><div>Report Build Time:</div> {elapsed_time} seconds</li>
</ul>
</div>
<div class="spacer40"></div>
</div>

<div class="loadviewsection">
	<?php echo $this->load->view('view_registrant_list'); ?>
</div>
