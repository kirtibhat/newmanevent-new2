<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
	
<div class="row-fluid">
	<div class="eventcontainerbg">
		<div class="headingbg">
			<div class="currenteventH bg_none pl0">
				<?php echo getReportNameLable($typeOfReport); ?>
			</div>
		</div>
		<div class="panel-body">
			<div class="TablecontentHolderDelegate tabledatascrolling">
				<div class="content">
					<table class="admintable tablecuston_scroll registration_list" cellpadding="0" cellspacing="0" border="0">
					<tbody>
					<?php if(!empty($delegateListDataByType)){
						
					  ?>
						<thead>
						
							
							<tr class="finance_tabel_head">
								<td colspan="3">
									<?php echo lang('report_full_regis_details'); ?>					
								</td>
								
								<td colspan="<?php echo $registrationColumCount['personal_count']; ?>">
									<?php echo lang('report_full_personal_details'); ?>					
								</td>
								
								
								<!------side event list----->
								
								<?php
								//personal field show section
								$countRow=0;
								foreach($delegateListDataByType as $delegateData ){
									if(!empty($delegateData['sideevent'])){
											
										$sideeventlistall = $delegateData['sideevent'];	
											foreach($sideeventlistall as $sideeventlist){
												$sideeventname  = $sideeventlist['side_event_name'];
												$sideeventcount = $sideeventlist['side_event_list_count'];
												
												if($countRow==0){
												?>
												
												<td colspan="<?php echo $sideeventcount; ?>">
													<?php echo $sideeventname; ?>					
												</td>
												
												<?php
												}
											}
										
										$countRow++;
									}
									
								} ?>
								
								
								<!------brakout list data------->
								
								<?php
								//personal field show section
								$countRow=0;
								foreach($delegateListDataByType as $delegateData ){
									if(!empty($delegateData['breakout'])){
											
										$breakoutlistall = $delegateData['breakout'];	
											foreach($breakoutlistall as $breakoutlist){
												$breakoutname  = $breakoutlist['breakoutname'];
												$breakoutcount = $breakoutlist['breakoutcount'];
												
												if($countRow==0){
												?>
												
												<td colspan="<?php echo $breakoutcount; ?>">
													<?php echo $breakoutname; ?>					
												</td>
												
												<?php
												}
											}
										
										$countRow++;
									}
									
								} ?>
								
								<?php if($typeOfReport == "fullReport"){ ?>
									<td colspan="4">
										<?php echo lang('report_full_paymennt_details'); ?>					
									</td>
								<?php } ?>	
								
								
							</tr>
							
							<tr>
								
							<th>
								<?php echo lang('report_regis_booking_history'); ?>
							</th>
							<th>
								<?php echo lang('report_regis_date_time'); ?>
							</th>
							<th>
								<?php echo lang('report_regis_rego_type'); ?>
							</th>
							
							
							<?php
								//personal field show section
								$countRow=0;
								foreach($delegateListDataByType as $delegateData ){
								$personalDetails = $delegateData['personal_details'];
								 if(!empty($personalDetails) && $countRow==0){ 
										foreach($personalDetails as $key => $value){
								?>
									<th>
										<?php echo ucwords($key); ?>
									</th>
							<?php  } $countRow++; }  } ?>
							
							
							<!-----side event field data start------->
							<?php
								//personal field show section
								$countRow=0;
								foreach($delegateListDataByType as $delegateData ){
									if(!empty($delegateData['sideevent'])){
											
										$sideeventlistall = $delegateData['sideevent'];	
										foreach($sideeventlistall as $sideeventlist){
											$sideeventlistarray = $sideeventlist['side_event_list'];
											
											//one time show condition
											if($countRow==0){
												echo '<th> No. Attending </th>';
											}
											foreach($sideeventlistarray as $sideeventlistarrlisting){
												foreach($sideeventlistarrlisting as $key => $value){
												//one time show condition
												if($countRow==0){
												?>	
													<th>
														<?php echo ucwords($key); ?>
													</th>
												<?php
													}
												}
											}
										}
										$countRow++;
									}
									
								} ?>
							<!-----side event field data end------->
							
							<!-----breakout field data------->
							<?php
								//personal field show section
								$countRow=0;
								foreach($delegateListDataByType as $delegateData ){
									if(!empty($delegateData['breakout'])){
											
										$breakoutlistall = $delegateData['breakout'];	
										foreach($breakoutlistall as $breakoutlist){
											$breakoutlistarray = $breakoutlist['breakoutlist'];
											
											
											foreach($breakoutlistarray as $key => $value){
													
												$breakoutValue= explode("_",$key);
												$breakoutValue1 = $breakoutValue[0];
												$breakoutValue2 =  $breakoutValue[1];
												$streamBreakoutName = "Stream ".$breakoutValue1." - Session ".$breakoutValue2;
												
												if($countRow==0){
												?>
													<th>
														<?php echo ucwords($streamBreakoutName); ?>
													</th>
												<?php
												}
												
											}
											
											
										}
										
										$countRow++;
									}
									
								} ?>
							
							<?php if($typeOfReport == "fullReport"){ ?>
							<th>
								<?php echo lang('report_regis_amount'); ?>
							</th>
							<th>
								<?php echo lang('report_regis_balance_remaining'); ?>
							</th>
							<th>
								<?php echo lang('report_regis_payment_method'); ?>
							</th>
							
							<th>
								<?php echo lang('report_regis_payment_date'); ?>
							</th>
							<?php } ?>
						
						</tr>
						</thead>
					<?php
						foreach($delegateListDataByType as $delegateData ){
						
						$registrationDetails = $delegateData['registration_details'];
						$personalDetails = $delegateData['personal_details'];
						$breakoutData = (!empty($delegateData['breakout']))?$delegateData['breakout']:'';
						$sideeventData = (!empty($delegateData['sideevent']))?$delegateData['sideevent']:'';
					?>	
						<tr>
							
						<td>
								<?php echo $registrationDetails['registration_booking_number']; ?>
						</td>
						
						<td>
							<div>
								<?php echo dateFormate($registrationDetails['registration_date'],'d M Y'); ?>
							</div>
							<div>
								<?php echo timeFormate($registrationDetails['registration_date'],'H:m A'); ?>
							</div>
						</td>
						
						<td>
								<?php echo $registrationDetails['registrant_name']; ?>
						</td>
						
						<?php if(!empty($personalDetails)){ 
							foreach($personalDetails as $key => $value){
								
								$showValue = $value;
								if($key=="address"){
									$showValue = "";
									$getAddressValue= (array) json_decode($value);
									if(is_array($getAddressValue)){
										if(!empty($getAddressValue['addressName'])){
											$addressValue = (array) $getAddressValue['addressName'];
											if(!empty($addressValue) && array_filter($addressValue)){
												$showValue = implode(",", $addressValue);
											}
										}	
									}
								}
								
						?>
							<td>
									<?php echo $showValue; ?>
							</td>
						<?php  }  } ?>
						
						
						<!-----side event list data----->
						
						<?php
							if(!empty($sideeventData)){
								foreach($sideeventData as $sideeventlist){
									$sideeventlistarray = $sideeventlist['side_event_list'];
									$numberAttending 	= $sideeventlist['Number_Attending'];
									echo '<td>'.$numberAttending.'</td>' ;
									foreach($sideeventlistarray as $sideeventlistarr){
									foreach($sideeventlistarr as $key => $value){
										
										 ?>
										<td>
											<?php echo $value; ?>
										</td>
									<?php
										}
									}
								}
							}
						?>
						
						<!-----side event list data----->
						
						<?php
							if(!empty($breakoutData)){
								foreach($breakoutData as $breakoutlist){
									$breakoutlistarray = $breakoutlist['breakoutlist'];
									foreach($breakoutlistarray as $key => $value){ ?>
										<td>
											<?php echo $value; ?>
										</td>
									<?php
									}
								}
							}
						?>
						
						<?php 
						
						if($typeOfReport == "fullReport"){
							$totalAmount		= (!empty($registrationDetails['total_amount']))?$registrationDetails['total_amount']:"0";
							$paymentReceived	= (!empty($registrationDetails['received_amount']))?$registrationDetails['received_amount']:"0";
							$paymentRemaing		= $totalAmount  - $paymentReceived;
							$paymentAmount		= $paymentReceived;
							$paymentMode		= $registrationDetails['payment_type'];
							$paymentDate		= date("d-M-Y",strtotime($registrationDetails['payment_date']));
							?>
							
							<td>
									<?php echo $totalAmount; ?>
							</td>
							<td>
									<?php echo $paymentReceived; ?>
							</td>
							<td>
									<?php echo $paymentMode; ?>
							</td>
							<td>
									<?php echo $paymentDate; ?>
							</td>
						<?php } ?>
					
					</tr>
					
					<?php }  }else{ ?>
						
						<thead>
							<tr>
								<th colspan="9" style="min-width:1050px;">
									<?php //echo lang('report_regis_serach_msg'); ?> No report data.
								</th>
							</tr>
						</thead>
					<?php	
					} ?>
					</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>


<div class="spacer10"></div>

