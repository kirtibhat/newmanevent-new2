<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


//search form 
$formDelegateFilter = array(
		'name'	 => 'formDelegateFilter',
		'id'	 => 'formDelegateFilter',
		'method' => 'post',
		'class'  => 'form-horizontal mt10',
	);
	
$fullReport = array(
		'name'	=> 'typeofreport',
		'value'	=> 'fullReport',
		'id'	=> 'fullReport',
		'type'	=> 'radio',
		'class'	=> 'radio',
		'checked'	=> true,
	);
$breakoutReport = array(
		'name'	=> 'typeofreport',
		'value'	=> 'breakoutReport',
		'id'	=> 'breakoutReport',
		'type'	=> 'radio',
		'class'	=> 'radio',
	);
$registrationReport = array(
		'name'	=> 'typeofreport',
		'value'	=> 'registrationReport',
		'id'	=> 'registrationReport',
		'type'	=> 'radio',
		'class'	=> 'radio',
	);
$sponsorshipReport = array(
		'name'	=> 'typeofreport',
		'value'	=> 'sponsorshipReport',
		'id'	=> 'sponsorshipReport',
		'type'	=> 'radio',
		'class'	=> 'radio',
	);
$sideeventReport = array(
		'name'	=> 'typeofreport',
		'value'	=> 'sideeventReport',
		'id'	=> 'sideeventReport',
		'type'	=> 'radio',
		'class'	=> 'radio',
	);			
$exhibitorReport = array(
		'name'	=> 'typeofreport',
		'value'	=> 'exhibitorReport',
		'id'	=> 'exhibitorReport',
		'type'	=> 'radio',
		'class'	=> 'radio',
	);

?>

<div class="row-fluid">
	<div class="headingh1">
		<?php echo lang('delegate_filter_reports') ?>
	</div>
</div>
<div class="spacer20">
</div>
	<?php  echo form_open(base_url('report/delegatereport'),$formDelegateFilter); ?>
	
		<div class="row-fluid mt20">
	<div class="span7">
		<div class="commonform_bg">
			<div class="headingbg bg_F89728 headingbg_bord">
				<?php echo lang('delegate_filter_delegate_report') ?>
			</div>
			<div class="form-horizontal">
				<div class="control-group mb10">
					<div class="formparag mb20">
						<h4>PRE EXISTING REPORTS</h4>
						 Please select what report you would like
						<div class="spacer10">
						</div>
						<div class="checkradiobg">
							<div class="radio report_50">
								<?php echo form_input($fullReport); ?>
								<label for="fullReport">Full Report</label>
							</div>
							<div class="radio report_50">
								<?php echo form_input($breakoutReport); ?>
								<label for="breakoutReport">Breakout Report</label>
							</div>
							<div class="radio report_50">
								<?php echo form_input($registrationReport); ?>
								<label for="registrationReport">Type of Registration Report</label>
							</div>
							<div class="radio report_50">
								<?php echo form_input($sponsorshipReport); ?>
								<label for="sponsorshipReport">Sponsorship Report</label>
							</div>
							<div class="radio report_50">
								<?php echo form_input($sideeventReport); ?>
								<label for="sideeventReport">Side Event Report</label>
							</div>
							<div class="radio report_50">
								<?php echo form_input($exhibitorReport); ?>
								<label for="exhibitorReport">Exhibitor Report</label>
							</div>
						</div>
					</div>
				</div>
				<div class="formparag">
					<div class="bdrB_Dash_orng">
					</div>
					<div class="spacer10">
					</div>
				</div>
				<div class="control-group mb10">
					<div class="formparag mb20">
						<h4>CUSTOM REPORTS</h4>
						 Details below will appear on all reports:<br>
						Booking Date; Booking Number; First Name; Last Name; Email; Registration
						<div class="spacer15">
						</div>
						<span>Registration Category, Include</span>
						<div class="checkradiobg">
							<?php
								if(!empty($registrantData)){
								foreach($registrantData as $registrantData){
								$registrantId = $registrantData->id;
								$registrant = array(
											'name'	=> 'registrant[]',
											'value'	=> $registrantId,
											'id'	=> 'registrant'.$registrantId,
											'type'	=> 'checkbox',
											'class'	=> 'pull-left ml10 mr15 checkbox',
										);
							?>
								<div class="checkbox report_50">
									<?php echo form_input($registrant); ?>
									<label for="<?php echo 'registrant'.$registrantId; ?>" class="width_auto ml0 mt10 eventDcheck">&nbsp; <?php echo $registrantData->registrant_name; ?></label>
								</div>
							<?php } } ?>
							
						</div>
						<div class="clearfix">
						</div>
						<div class="spacer15">
						</div>
						<span>Attendee Personal Details, Include</span>
						<div class="checkradiobg">
							<?php if(!empty($personalFieldData)){ 
								//this blank array for hiding same filed name
								$filedArray = array();
								foreach($personalFieldData as $personalField){
									$fieldName 	 = $personalField->field_name;
									$fieldId	 = $personalField->id;
									
									//check field name in array 
									if(!array_search($fieldName,$filedArray)){
									//assing filed name in array
									$filedArray[$fieldId] =  $fieldName;		
									$selectedField = array(
											'name'	=> 'selectedField[]',
											'value'	=> $fieldName,
											'id'	=> 'selectedField'.$fieldId,
											'type'	=> 'checkbox',
											'class'	=> 'pull-left ml10 mr15 checkbox',
										);
								?>
								
								<div class="checkbox report_50">
									<?php echo form_input($selectedField); ?>
									<label for="<?php echo 'selectedField'.$fieldId; ?>" class="width_auto ml0 mt10 eventDcheck">&nbsp; <?php echo ucwords($fieldName); ?></label>
								</div>
								
								<?php } } } ?>
							
						</div>
						<!---
						<div class="clearfix">
						</div>
						<div class="spacer15">
						</div>
						
						<span>Type of Registration, Include</span>
						<div class="checkradiobg">
							<div class="checkbox report_50">
								<input type="checkbox" name="ex1" id="check6" class="pull-left ml10 mr15 checkbox">
								<label for="check6" class="width_auto ml0 mt10 eventDcheck">&nbsp; Early Bird</label>
							</div>
							<div class="checkbox report_50">
								<input type="checkbox" name="ex1" id="check6" class="pull-left ml10 mr15 checkbox">
								<label for="check6" class="width_auto ml0 mt10 eventDcheck">&nbsp; Day Registration 1</label>
							</div>
							<div class="checkbox report_50">
								<input type="checkbox" name="ex1" id="check6" class="pull-left ml10 mr15 checkbox">
								<label for="check6" class="width_auto ml0 mt10 eventDcheck">&nbsp; Partner</label>
							</div>
							<div class="checkbox report_50">
								<input type="checkbox" name="ex1" id="check6" class="pull-left ml10 mr15 checkbox">
								<label for="check6" class="width_auto ml0 mt10 eventDcheck">&nbsp; Day Registration 2</label>
							</div>
							<div class="checkbox report_50">
								<input type="checkbox" name="ex1" id="check6" class="pull-left ml10 mr15 checkbox">
								<label for="check6" class="width_auto ml0 mt10 eventDcheck">&nbsp; Student</label>
							</div>
							<div class="checkbox report_50">
								<input type="checkbox" name="ex1" id="check6" class="pull-left ml10 mr15 checkbox">
								<label for="check6" class="width_auto ml0 mt10 eventDcheck">&nbsp; Day Registration 3</label>
							</div>
							<div class="checkbox report_50">
								<input type="checkbox" name="ex1" id="check6" class="pull-left ml10 mr15 checkbox">
								<label for="check6" class="width_auto ml0 mt10 eventDcheck">&nbsp; Member</label>
							</div>
							<div class="checkbox report_50">
								<input type="checkbox" name="ex1" id="check6" class="pull-left ml10 mr15 checkbox">
								<label for="check6" class="width_auto ml0 mt10 eventDcheck">&nbsp; Group Booking</label>
							</div>
							<div class="checkbox report_50">
								<input type="checkbox" name="ex1" id="check6" class="pull-left ml10 mr15 checkbox">
								<label for="check6" class="width_auto ml0 mt10 eventDcheck">&nbsp; Non Member</label>
							</div>
						</div>
						--->
						<!---
						<div class="clearfix">
						</div>
						<div class="spacer15">
						</div>
						<span>Side Events, Include</span>
						<div class="checkradiobg">
							
							--->
							
							<?php 
							/*
							if(!empty($sideeventData)){ 
								foreach($sideeventData as $sideevent){
									$sideeventName 	 = $sideevent->side_event_name;
									$sideeventId	 = $sideevent->id;
									
									$selectedSideevent = array(
										'name'	=> 'selectedSideevent[]',
										'value'	=> $sideeventId,
										'id'	=> 'selectedSideevent'.$sideeventId,
										'type'	=> 'checkbox',
										'class'	=> 'pull-left ml10 mr15 checkbox',
									);
							?>		
								<div class="checkbox report_50">
									<?php echo form_input($selectedSideevent); ?>
									<label for="<?php echo 'selectedSideevent'.$sideeventId; ?>" class="width_auto ml0 mt10 eventDcheck">&nbsp; <?php echo $sideeventName; ?></label>
								</div>
							<?php }  } */ ?>
							
						</div>
						<div class="clearfix">
						</div>
						
						<!---
						<div class="spacer15">
						</div>
						
						<span>Breakouts, Include</span><br>
						<div class="spacer15">
						</div>
					
						<div class="clr_f89728">
							DAY 1
						</div>
						<div class="checkradiobg">
							
							<div class="checkbox report_50">
								<input type="checkbox" name="ex1" id="check6" class="pull-left ml10 mr15 checkbox">
								<label for="check6" class="width_auto ml0 mt10 eventDcheck">&nbsp; Workforce Training</label>
								<div class="checkbox ml25">
									<input type="checkbox" name="ex1" id="check6" class="pull-left ml10 mr15 checkbox">
									<label for="check6" class="width_auto ml0 mt10 eventDcheck">&nbsp; Session 1</label>
								</div>
								<div class="checkbox ml25">
									<input type="checkbox" name="ex1" id="check6" class="pull-left ml10 mr15 checkbox">
									<label for="check6" class="width_auto ml0 mt10 eventDcheck">&nbsp; Session 2</label>
								</div>
								<div class="checkbox ml25">
									<input type="checkbox" name="ex1" id="check6" class="pull-left ml10 mr15 checkbox">
									<label for="check6" class="width_auto ml0 mt10 eventDcheck">&nbsp; Session 3</label>
								</div>
							</div>
							<div class="checkbox report_50">
								<input type="checkbox" name="ex1" id="check6" class="pull-left ml10 mr15 checkbox">
								<label for="check6" class="width_auto ml0 mt10 eventDcheck">&nbsp; Primary Health</label>
								<div class="checkbox ml25">
									<input type="checkbox" name="ex1" id="check6" class="pull-left ml10 mr15 checkbox">
									<label for="check6" class="width_auto ml0 mt10 eventDcheck">&nbsp; Session 1</label>
								</div>
								<div class="checkbox ml25">
									<input type="checkbox" name="ex1" id="check6" class="pull-left ml10 mr15 checkbox">
									<label for="check6" class="width_auto ml0 mt10 eventDcheck">&nbsp; Session 2</label>
								</div>
								<div class="checkbox ml25">
									<input type="checkbox" name="ex1" id="check6" class="pull-left ml10 mr15 checkbox">
									<label for="check6" class="width_auto ml0 mt10 eventDcheck">&nbsp; Session 3</label>
								</div>
							</div>
							<div class="clearfix">
								&nbsp;
							</div>
							<div class="checkbox report_50">
								<input type="checkbox" name="ex1" id="check6" class="pull-left ml10 mr15 checkbox">
								<label for="check6" class="width_auto ml0 mt10 eventDcheck">&nbsp; Governance</label>
								<div class="checkbox ml25">
									<input type="checkbox" name="ex1" id="check6" class="pull-left ml10 mr15 checkbox">
									<label for="check6" class="width_auto ml0 mt10 eventDcheck">&nbsp; Session 1</label>
								</div>
								<div class="checkbox ml25">
									<input type="checkbox" name="ex1" id="check6" class="pull-left ml10 mr15 checkbox">
									<label for="check6" class="width_auto ml0 mt10 eventDcheck">&nbsp; Session 2</label>
								</div>
								<div class="checkbox ml25">
									<input type="checkbox" name="ex1" id="check6" class="pull-left ml10 mr15 checkbox">
									<label for="check6" class="width_auto ml0 mt10 eventDcheck">&nbsp; Session 3</label>
								</div>
							</div>
							<div class="checkbox report_50">
								<input type="checkbox" name="ex1" id="check6" class="pull-left ml10 mr15 checkbox">
								<label for="check6" class="width_auto ml0 mt10 eventDcheck">&nbsp; ED Talks 1</label>
							</div>
							<div class="clearfix">
							</div>
							<div class="spacer15">
							</div>
							<div class="checkbox report_50">
								<input type="checkbox" name="ex1" id="check6" class="pull-left ml10 mr15 checkbox">
								<label for="check6" class="width_auto ml0 mt10 eventDcheck">&nbsp; ED Talks 2</label>
							</div>
							<div class="checkbox report_50">
								<input type="checkbox" name="ex1" id="check6" class="pull-left ml10 mr15 checkbox">
								<label for="check6" class="width_auto ml0 mt10 eventDcheck">&nbsp; ED Talks 3</label>
							</div>
							<div class="checkbox report_50">
								<input type="checkbox" name="ex1" id="check6" class="pull-left ml10 mr15 checkbox">
								<label for="check6" class="width_auto ml0 mt10 eventDcheck">&nbsp; ED Talks 4</label>
							</div>
						</div>
						<div class="clearfix">
						</div>
						<div class="spacer15">
						</div>
						
						--->
						
					</div>
					<div class="spacer36">
					</div>
					<div class="formparag">
						<a href="#" class="color_f89828">Export this information as a HTML, PDF CSV (Comma Separated Value).</a>
					</div>
				</div>
				<div class="row-fluid mt20">
					<button class="submitbtn_cus" name="submittype[]" type="submit" role="button" value="csv" aria-disabled="false"><span></span> CSV</span></button>
					<button class="submitbtn_cus" name="submittype[]" type="submit" role="button" value="pdf" aria-disabled="false"><span></span> PDF</span></button>
					<button class="submitbtn_cus" name="submittype[]" type="submit" role="button" value="html" aria-disabled="false"><span></span> HTML</span></button>
				</div>
				<div class="spacer20"></div>
			</div>
	
	<?php echo form_close(); ?>
	</div>
		<!-- /commonform_bg -->
	</div>
	<!---
	<div class="span5 text-center">
		<img src="images/add.jpg" alt="add"/>
	</div>--->
</div>
