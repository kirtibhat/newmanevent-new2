<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	if(!empty($geteventsdetails)){
?>
	<div class="row-fluid">
		<div class="spacer15">
		</div>
		<div class="finan_report_main">
			<div class="report_head">
				<?php echo $geteventsdetails->event_title;?>
			</div>
			<ul>
				<li>
				<div>
					<?php echo lang('report_pay_event_date'); ?>
				</div>
				 <?php
				$startTime = $geteventsdetails->starttime;
				$endTime   = $geteventsdetails->endtime;
				echo date('D, d M Y',strtotime($startTime));?> - <?php echo date('D, d M Y',strtotime($endTime)); 
				echo ' [ '.count(datedaydifference($startTime,$endTime)).' days ]';
				?></li>
			</ul>
		</div>
		<div class="spacer40">
		</div>
	</div>

<?php } ?>
	
	
<div class="row-fluid">
	<div class="eventcontainerbg">
		<div class="headingbg">
			<div class="currenteventH bg_none pl0">
				<?php echo lang('report_payments_details'); ?>
			</div>
		</div>
		<div class="panel-body">
			<div class="TablecontentHolder tabledatascrolling">
				<div class="content">
					<table class="admintable tablecuston_scroll financ_table" cellpadding="0" cellspacing="0" border="0">
					
					<tbody>
						<?php if(!empty($paymentslistdata)){ ?>
					<thead>
					<tr>
						<th>
							<?php echo lang('report_pay_date'); ?>
						</th>
						<th>
							<?php echo lang('report_pay_booking_id'); ?>
						</th>
						<th>
							<?php echo lang('report_pay_registrant'); ?>
						</th>
						<th>
							<?php echo lang('report_pay_registrant_name'); ?>
						</th>
						
						<th>
							<?php echo lang('report_pay_cheque_transaction'); ?>
						</th>
						<th>
							<?php echo lang('report_pay_total_amount'); ?>
						</th>
					
						<th>
							<?php echo lang('report_pay_received_amt'); ?>
						</th>
					
						<th>
							<?php echo lang('report_pay_outstaning_amt'); ?>
						</th>
						
						<th>
							<?php echo lang('report_pay_payment_status'); ?>
						</th>
					</tr>
					</thead>
					
					<?php
					
					//define variable for grand total
					$grandTatolAmount = '0';
					$grandReceivedAmount = '0';
					$grandOutstandingAmount = '0';
					
					foreach($paymentslistdata as $key => $paymentsdata ){ ?>
				
					<tr class="finance_tabel_head">
						<td colspan="9">
							<?php
							 switch($key){
									case 'credit_card':
										echo lang('report_pay_event_credit_card');
									break; 
									case 'cheque':
										echo lang('report_pay_event_cheque');
									break;
									case 'cash':
										echo lang('report_pay_event_cash');
									break;
								}
							?>
						</td>
					</tr>
							
					<?php
						//define variable for sub total
						$subTatolAmount = '0';
						$subReceivedAmount = '0';
						$subOutstandingAmount = '0';
						foreach($paymentsdata as $paymentslist ){
					?>	
						<tr>
						<td>
							<div>
								<?php echo dateFormate($paymentslist->payment_date,'d M Y'); ?>
							</div>
							<div>
								<?php echo timeFormate($paymentslist->payment_date,'H:m A'); ?>
							</div>
						</td>	
						<td>
							<?php echo $paymentslist->registration_booking_number; ?>
						</td>
						<td>
							<?php echo $paymentslist->regscount; ?>
						</td>
						<td>
							<div>
								<?php echo $paymentslist->firstname; ?>
							</div>
							<div>
								<?php echo $paymentslist->email; ?>
							</div>
						</td>
						
						<td>
							<?php echo (!empty($paymentslist->transaction_id))?$paymentslist->transaction_id:'N/A'; ?>
						</td>
						
						<td>
							<?php 
									echo $paymentslist->total_amount;
									$subTatolAmount = $subTatolAmount + $paymentslist->total_amount;
							 ?>
						</td>
						
						<td>
							<?php
									echo $paymentslist->received_amount;
									$subReceivedAmount = $subReceivedAmount + $paymentslist->received_amount;
							 ?>
						</td>
						<td>
							<?php
									echo $paymentslist->outstanding_amount;
									$subOutstandingAmount = $subOutstandingAmount + $paymentslist->outstanding_amount; 
							?>
						</td>
						<td>
							<div class="checkradiobg">
								<div class="radio admin">
                                <input <?php echo ($paymentslist->payment_received=='1')?'checked disabled="true"':''; ?>   id="received<?php echo $paymentslist->id; ?>" paymentid='<?php echo $paymentslist->id; ?>' type="radio" name="payment_received<?php echo $paymentslist->id; ?>" value="1" class="radio payment_received">
                                <label for="received<?php echo $paymentslist->id; ?>" class="width_105px"><?php echo lang('report_pay_received'); ?></label>
                           		
                           		<div class="clearfix"></div>
                                <input <?php echo ($paymentslist->payment_received=='2')?'checked disabled="true"':''; ?> id="notreceived<?php echo $paymentslist->id; ?>" paymentid='<?php echo $paymentslist->id; ?>' type="radio" name="payment_received<?php echo $paymentslist->id; ?>" value="2" class="payment_received">
                                <label for="notreceived<?php echo $paymentslist->id; ?>" class="width_105px"><?php echo lang('report_pay_not_received'); ?></label>
                               </div>
                            </div>
						</td>
					</tr>
					
					<?php } ?>
						<tr class="finance_subt">
							<td colspan="5">
								<div>
									<?php echo lang('report_pay_event_sub_total'); ?>
								</div>
							</td>
							<td>
								<?php echo number_format($subTatolAmount,2); ?>
							</td>
							<td>
								<?php echo number_format($subReceivedAmount,2); ?>
							</td>
							<td >
								<?php echo number_format($subOutstandingAmount,2); ?>
							</td>
							<td >
								&nbsp;
							</td>
							<?php 
								$grandTatolAmount = $grandTatolAmount + $subTatolAmount;
								$grandReceivedAmount = $grandReceivedAmount + $subReceivedAmount;
								$grandOutstandingAmount = $grandOutstandingAmount +  $subOutstandingAmount;
							
							?>
						</tr>
					<?php  } ?>
							
							<tr class="finance_subt border_425968">
								<td colspan="5">
									<div>
										<?php echo lang('report_pay_event_grand_total'); ?>
									</div>
								</td>
								<td>
									<?php echo number_format($grandTatolAmount,2); ?>
								</td>
								<td>
									<?php echo number_format($grandReceivedAmount,2); ?>
								</td>
								<td >
									<?php echo number_format($grandOutstandingAmount,2); ?>
								</td>
								<td >
									&nbsp;
								</td>
							</tr>
					
					<?php  }else{ ?>
					
					<thead>
					<tr>
						<th colspan="9">
							<?php echo 'No Records found.'; ?>
						</th>
					</tr>
					</thead>
					
					<?php } ?>
					</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="spacer10"></div>
