<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');  ?>

<div class="spacer40">
</div>
<div class="row-fluid">
	<div class="headingh1">
		<?php echo lang('report_payments'); ?>
	</div>
	<div class="spacer40">
	</div>
	<div class="commpnpara">
		<?php echo lang('report_payments_details_title'); ?>
	</div>
</div>
<div class="spacer15">
</div>
<div class="loadviewsection">
	<?php echo $this->load->view('view_payments_list'); ?>
</div>


<script type="text/javascript">

	//record pagination
	//recordpagination('report/payments','formFinancialSearch');

	//payment confirm & cancel action 
	$(document).on('click','.payment_received',function(){
		
		var paymentid 	= $(this).attr('paymentid');
		var getvalue = parseInt($(this).val());
		
		if(getvalue==1){
			//condition for confirm button
			$('#received'+paymentid).attr('disabled',true);
			$('#notreceived'+paymentid).attr('disabled',false);
		}else{
			//condition for cancel button
			$('#notreceived'+paymentid).attr('disabled',true);
			$('#received'+paymentid).attr('disabled',false);
		}
		
		//confirm poup code
		bootbox.confirm({
			title: 'Confirm',
			message: 'Do you really want to perform this action?',
			buttons: {
				'cancel': {
					label: 'Cancel',
					className: 'btn-default custombtn'
				},
				'confirm': {
					label: 'Ok',
					className: 'btn-danger custombtn bg_f26531'
				}
			},
			callback: function(result) {
				if (result) {
					if(getvalue==1){
						//condition for confirm button
						var url = baseUrl+'report/paymentconfirmstatus';
						var fromData = {'paymentid':paymentid,'getvalue':getvalue };
						$.ajax({
						  type:'POST',
						  data:fromData,
						  url: url,
						  dataType: 'json',
						  cache: false,
						  beforeSend: function( ) {
								//open loader
								loaderopen();
							},
						  success: function(data){	
								//check data 
								if(data){
									loaderclose();
									custom_popup(data.msg,true);
								}
							},
							error: function(XMLHttpRequest, textStatus, errorThrown) {
								//hide loader
								loaderclose();
								custom_popup('Request failed.',false);
							}
						});
					}else{
						//condition for cancel button
						var fromData = {'paymentid':paymentid,'getvalue':getvalue };
						//call ajax payment cancel message view open
						ajaxpopupopen('payment_cancel_message_popup','report/paymentcancel',fromData);
					}
				}
			}
		});
	});
	
</script>
