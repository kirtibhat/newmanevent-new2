<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

//event relate info data
	$eventName		 	=   ucwords($eventData->event_title);
	$eventStartDate  	=   dateFormate($eventData->starttime,'D, d M Y');
	$eventEndDate 		=   dateFormate($eventData->endtime,'D, d M Y');
	$currentEndDate 	=   dateFormate(date('y-m-d H:i'),'d-M-y H:i');
	$eventDays			=   datedaydifference($eventData->starttime,$eventData->endtime);
	$numberOfDays 		=   count($eventDays); 
	
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Newman event</title>
<style type="text/css">
	body{font-family: 'proxima_nova_altsemibold';}
	@font-face {
    font-family: 'proxima_nova_altsemibold';
    src: url('../fonts/mark_simonson_-_proxima_nova_alt_semibold-webfont.eot');
    src: url('../fonts/mark_simonson_-_proxima_nova_alt_semibold-webfont.eot?#iefix') format('embedded-opentype'),
         url('../fonts/mark_simonson_-_proxima_nova_alt_semibold-webfont.woff') format('woff'),
         url('../fonts/mark_simonson_-_proxima_nova_alt_semibold-webfont.ttf') format('truetype'),
         url('../fonts/mark_simonson_-_proxima_nova_alt_semibold-webfont.svg#proxima_nova_altsemibold') format('svg');
    font-weight: normal;
    font-style: normal;

}
</style>
</head>
<body>
<div style="max-width:1050px; width:100%; margin:auto; display:table;">
	<div style="float:left; padding:20px 0"><img src="<?php echo base_url('templates/system/images/logobig.png'); ?>" alt="logo" /></div>
 
<div style="width:100%; float:left; color:#425968; font-size:20px;">
    	<div style="font-size:48px; margin:20px 0;">Reports</div>
        <div style="color:#f89728; font-size:30px; margin-bottom:10px;"><?php echo $eventName ?></div>
        
        <div style="height:auto; width:100%; float:left; margin-bottom:8px;">
        	<div style="width:200px; float:left;">Date:</div>
            <div  style="width:auto; float:left;"><?php echo $eventStartDate; ?> - <?php echo $eventEndDate; ?> [<?php echo $numberOfDays ?> days]</div>
        </div>
        
        <div style="height:auto; width:100%; float:left; margin-bottom:8px;">
        	<div style="width:200px; float:left;">Date Printed:</div>
            <div  style="width:auto; float:left;"><?php echo $currentEndDate; ?></div>
        </div>
        
        <div style="height:auto; width:100%; float:left; margin-bottom:8px;">
        	<div style="width:200px; float:left;">Report Build Time:</div>
            <div  style="width:auto; float:left;">0.0354 seconds</div>
        </div>
</div>    
    
<div style=" width:100%; padding-bottom:25px; border-radius:36px; float:left; margin-top:30px;">
    <div style="border-radius:36px 36px 0 0; height:84px;">
        <div style="color:#000; font-size:30px; float:left; margin-top:30px; padding-left:33px; line-height:28px;"><?php echo getReportNameLable($typeOfReport); ?></div>        
    </div>
	<table cellpadding="0" cellspacing="0" border="0"> 		
<thead >

<tr>
	
	<?php if(!empty($delegateListDataByType)) { ?>
		<td colspan="3" style="border:solid 1px #000;font-size:26px;text-align:center;">
			<?php echo lang('report_full_regis_details'); ?>					
		</td>
	<?php } ?>
	
	<?php if(!empty($delegateListDataByType)) { ?>
	<td colspan="<?php echo $registrationColumCount['personal_count']; ?>" style="border:solid 1px #000;font-size:26px;text-align:center;">
		<?php echo lang('report_full_personal_details'); ?>					
	</td>
	<?php } ?>
	
	<!------side event list----->
								
	<?php
	if(!empty($delegateListDataByType)) {
		//personal field show section
		$countRow=0;
		foreach($delegateListDataByType as $delegateData ){
			if(!empty($delegateData['sideevent'])){
					
				$sideeventlistall = $delegateData['sideevent'];	
					foreach($sideeventlistall as $sideeventlist){
						$sideeventname  = $sideeventlist['side_event_name'];
						$sideeventcount = $sideeventlist['side_event_list_count'];
						
						if($countRow==0){
						?>
						
						<td style="border:solid 1px #000;font-size:26px;text-align:center;" colspan="<?php echo $sideeventcount; ?>">
							<?php echo $sideeventname; ?>					
						</td>
						
						<?php
						}
					}
				
				$countRow++;
			}
			
		}
	}
	
	 ?>
	
	<?php
	if(!empty($delegateListDataByType)) {
		//personal field show section
		$countRow=0;
		foreach($delegateListDataByType as $delegateData ){
			if(!empty($delegateData['breakout'])){
					
				$breakoutlistall = $delegateData['breakout'];	
					foreach($breakoutlistall as $breakoutlist){
						$breakoutname  = $breakoutlist['breakoutname'];
						$breakoutcount = $breakoutlist['breakoutcount'];
						
						if($countRow==0){
						?>
						
						<td colspan="<?php echo $breakoutcount; ?>" style="border:solid 1px #000;font-size:26px;text-align:center;">
							<?php echo $breakoutname; ?>					
						</td>
						
						<?php
						}
					}
				
				$countRow++;
			}
		
		} 
	} 
	
	?>
	
	<?php if($typeOfReport == "fullReport"){ 
	if(!empty($delegateListDataByType)) {	
		?>
	<td colspan="4" style="border:solid 1px #000;font-size:26px;text-align:center;">
		<?php echo lang('report_full_paymennt_details'); ?>					
	</td>
	<?php } } ?>
	
</tr>	
	
<tr class="bpmTopnTail">
<?php if(!empty($delegateListDataByType)) { ?>	
	
<th style="border:solid 1px #000; ">Booking No</th>
<th style="border:solid 1px #000">Date & Time</th>
<th style="border:solid 1px #000">Registration Type</th>
<?php } ?>

<?php if(!empty($delegateListDataByType)){
	//personal field show section
	$countRow=0;
	foreach($delegateListDataByType as $delegateData ){
	$personalDetails = $delegateData['personal_details'];
	 if(!empty($personalDetails) && $countRow==0){ 
			foreach($personalDetails as $key => $value){
?>
<th style="border:solid 1px #000"><?php echo ucwords($key); ?> </th>

<?php } $countRow++; }  }  } ?>


<!-----side event field data start------->
	<?php
	if(!empty($delegateListDataByType)) {
		//personal field show section
		$countRow=0;
		foreach($delegateListDataByType as $delegateData ){
			if(!empty($delegateData['sideevent'])){
					
				$sideeventlistall = $delegateData['sideevent'];	
				foreach($sideeventlistall as $sideeventlist){
					$sideeventlistarray = $sideeventlist['side_event_list'];
					
					//one time show condition
					if($countRow==0){
						echo '<th style="border:solid 1px #000"> No. Attending </th>';
					}
					foreach($sideeventlistarray as $sideeventlistarrlisting){
						foreach($sideeventlistarrlisting as $key => $value){
						//one time show condition
						if($countRow==0){
						?>	
							<th style="border:solid 1px #000">
								<?php echo ucwords($key); ?>
							</th>
						<?php
							}
						}
					}
				}
				$countRow++;
			}
			
		} 
	
	}
	
	?>
	<!-----side event field data end------->


<?php
	if(!empty($delegateListDataByType)) {
		//personal field show section
		$countRow=0;
		foreach($delegateListDataByType as $delegateData ){
			if(!empty($delegateData['breakout'])){
					
				$breakoutlistall = $delegateData['breakout'];	
				foreach($breakoutlistall as $breakoutlist){
					$breakoutlistarray = $breakoutlist['breakoutlist'];
					
					
					foreach($breakoutlistarray as $key => $value){
							
						$breakoutValue= explode("_",$key);
						$breakoutValue1 = $breakoutValue[0];
						$breakoutValue2 =  $breakoutValue[1];
						$streamBreakoutName = "Stream ".$breakoutValue1." - Session ".$breakoutValue2;
						
						if($countRow==0){
						?>
							<th style="border:solid 1px #000">
								<?php echo ucwords($streamBreakoutName); ?>
							</th>
						<?php
						}
						
					}
					
					
				}
				
				$countRow++;
			}
			
		} 
	} 
	
	?>

<?php if($typeOfReport == "fullReport"){ 
if(!empty($delegateListDataByType)) {	
	?>
<th style="border:solid 1px #000">
	<?php echo lang('report_regis_amount'); ?>
</th>
<th style="border:solid 1px #000">
	<?php echo lang('report_regis_balance_remaining'); ?>
</th>
<th style="border:solid 1px #000">
	<?php echo lang('report_regis_payment_method'); ?>
</th>

<th style="border:solid 1px #000">
	<?php echo lang('report_regis_payment_date'); ?>
</th>
<?php }  } ?>

</tr>
</thead>
<tbody>
	
	<?php if(!empty($delegateListDataByType)) {
		$countRow = 1;
		
		foreach($delegateListDataByType as $delegateData ){
						
		$registrationDetails = $delegateData['registration_details'];
		$personalDetails = $delegateData['personal_details'];
		$breakoutData = (!empty($delegateData['breakout']))?$delegateData['breakout']:'';
		$sideeventData = (!empty($delegateData['sideevent']))?$delegateData['sideevent']:'';
		
		$rowColor = ($countRow % 2 )?'#f3f6f6':'#dee6e7';	
	?>	
		
	<tr >
	<td style="border:solid 1px #000"><span style="color:#f26838;"><?php echo $registrationDetails['registration_booking_number']; ?></span></td>
	<td style="border:solid 1px #000"><?php echo dateFormate($registrationDetails['registration_date'],'d M Y'); ?> <div style="border-bottom:dashed 1px #c2d1d4;"></div><?php echo dateFormate($registrationDetails['registration_date'],'H:m A'); ?></td>
	<td style="border:solid 1px #000"><?php echo $registrationDetails['registrant_name']; ?></td>
	<?php if(!empty($personalDetails)){ 
		foreach($personalDetails as $key => $value){ 
			
			$showValue = $value;
			if($key=="address"){
				$showValue = "";
				$getAddressValue= (array) json_decode($value);
				if(is_array($getAddressValue)){
					if(!empty($getAddressValue['addressName'])){
						$addressValue = (array) $getAddressValue['addressName'];
						if(!empty($addressValue) && array_filter($addressValue)){
							$showValue = implode(",", $addressValue);
						}
					}	
				}
			}
			
			?>
			<td style="border:solid 1px #000"><?php echo $showValue; ?></td>
	<?php  }  } ?>
	
	
		<!-----side event list data----->
						
			<?php
				if(!empty($sideeventData)){
					foreach($sideeventData as $sideeventlist){
						$sideeventlistarray = $sideeventlist['side_event_list'];
						$numberAttending 	= $sideeventlist['Number_Attending'];
						echo '<td style="border:solid 1px #000">'.$numberAttending.'</td>' ;
						foreach($sideeventlistarray as $sideeventlistarr){
						foreach($sideeventlistarr as $key => $value){
							
							 ?>
							<td style="border:solid 1px #000">
								<?php echo $value; ?>
							</td>
						<?php
							}
						}
					}
				}
			?>
		
		<!-----side event list data----->
	
	<?php
		if(!empty($breakoutData)){
			foreach($breakoutData as $breakoutlist){
				$breakoutlistarray = $breakoutlist['breakoutlist'];
				foreach($breakoutlistarray as $key => $value){ ?>
					<td style="border:solid 1px #000">
						<?php echo $value; ?>
					</td>
				<?php
				}
			}
		}
	?>
	
	<?php if($typeOfReport == "fullReport"){ 
						
	$totalAmount		= (!empty($registrationDetails['total_amount']))?$registrationDetails['total_amount']:"0";
	$paymentReceived	= (!empty($registrationDetails['received_amount']))?$registrationDetails['received_amount']:"0";
	$paymentRemaing		= $totalAmount  - $paymentReceived;
	$paymentAmount		= $paymentReceived;
	$paymentMode		= $registrationDetails['payment_type'];
	$paymentDate		= date("d-M-Y",strtotime($registrationDetails['payment_date']));
	?>
	
	<td style="border:solid 1px #000">
			<?php echo $totalAmount; ?>
	</td>
	<td style="border:solid 1px #000">
			<?php echo $paymentRemaing; ?>
	</td>
	<td style="border:solid 1px #000">
			<?php echo $paymentMode; ?>
	</td>
	<td style="border:solid 1px #000">
			<?php echo $paymentDate; ?>
	</td>

	</tr>
	
	<?php } ?>

	<?php  $countRow++; }  } ?>


</tbody>
</table> 
 
</div>
     
 </div>  
 
</div>
</body>
</html>



