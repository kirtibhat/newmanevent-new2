<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

$formPaymentCancel = array(
		'name'	 => 'formPaymentCancel',
		'id'	 => 'formPaymentCancel',
		'method' => 'post',
		'class'  => 'wpcf7-form',
	);
	
$cancelMessage = array(
		'name'	=> 'cancelMessage',
		'value'	=> '',
		'id'	=> 'cancelMessage',
		'type'	=> 'textarea',
		'rows'	=> '3',
		'cols'	=> '500',
		'placeholder'	=> lang('report_msg_cancellation_message_enter'),
		//'required'	=> '',
	);	
	

$paymentidField = array(
		'name'	=> 'paymentid',
		'value'	=> $paymentid,
		'id'	=> 'paymentid',
		'type'	=> 'hidden',
	);

$getvalueField = array(
		'name'	=> 'getvalue',
		'value'	=> $getvalue,
		'id'	=> 'getvalue',
		'type'	=> 'hidden',
	);			
?>

<div class="modal fade homelogin_popup dn" id="payment_cancel_message_popup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close colosebtnlogin" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"><?php echo lang('report_msg_cancellation_message'); ?></h4>
			</div>
			<div class="modal-body">
				<div class="modelinner">
					 <?php echo form_open(base_url('report/paymentcancelstatus'),$formPaymentCancel); ?>
						<div class="control-group mb10 loginbtninput_home">
							<?php echo form_textarea($cancelMessage); ?>
							<div class="mb10">
								<?php 
									$extra 	= 'class="homepopup_login submitbtn" ';
									echo form_submit('cancelSubmit',$this->lang->line('comm_submit'),$extra);
								?>
								<div class="clearfix">
								</div>
							</div>
						</div>
					<?php
						echo form_input($paymentidField); 
						echo form_input($getvalueField); 
						echo form_close(); 
					?>
				</div>
			</div>
		</div>
	</div>
</div>
