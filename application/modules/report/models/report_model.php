<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class report_model extends CI_model{


	private $tablenmuser					= 'user';
	private $tablenmfrntregistrant			= 'frnt_registrant';
	private $tablenmfrntbreakouts			= 'frnt_breakouts';
	private $tablenmevent					= 'event';
	private $tablenmfrntpaymentdetails		= 'frnt_payment_details';
	private $tablennmeventregistrants		= 'event_registrants';
	private $tablenmsideevent				= 'side_event';
	private $tablesponsexhibooths			= 'sponsor_exhibitor_booths';
	private $tablenmsponsor					= 'sponsor';
	private $tablenmexhibitor				= 'exhibitor';
    
	function __construct(){
		parent::__construct();	
			
	}
	
	
	/*
	 * @description: This function is used to get all registrant data of all event 
	 * @return: object
	 */ 
	
	
	public function registranttionlist($eventId){
		
		$registrant  = $this->input->post('registrant');
		
		$this->db->select('frtrgst.id,frtrgst.registration_booking_number,frtrgst.registrant_id,frtrgst.registration_date,event.event_title,
		frnt_payment_details.total_amount,frnt_payment_details.payment_type,frnt_payment_details.payment_date,frnt_payment_details.received_amount,
		event_registrants.registrant_name,fse.additional_guest as frnt_additional_guest, fse.side_event_id,fse.id as frntsideeventid,
		se.side_event_name');
		$this->db->from('nm_frnt_registrant as frtrgst');
		$this->db->join($this->tablenmuser, 'frtrgst.user_id = user.id');
		$this->db->join($this->tablenmevent, 'user.event_id = event.id');
		$this->db->join($this->tablenmfrntpaymentdetails, 'frtrgst.payment_id = frnt_payment_details.id','left');
		$this->db->join($this->tablennmeventregistrants, 'frtrgst.registrant_id = event_registrants.id');
		$this->db->join('frnt_side_event fse', 'fse.registered_user_id = frtrgst.id','left');
		$this->db->join('side_event se', 'fse.side_event_id = se.id','left');
		
		$this->db->where('frtrgst.event_id',$eventId); //check payment paid status
		$this->db->where('frtrgst.payment_status','1'); //check payment paid status
		$this->db->where('frtrgst.is_active ','1'); //check payment confirm status
		
		//if not empty then show selected registrant
		if(!empty($registrant)){
			$this->db->where_in('registrant_id',$registrant);
		}
		$this->db->order_by("frtrgst.id", "asc"); 
		if(!empty($limit)){
			$this->db->limit($limit,$start);
		}
		$query = $this->db->get();
		return $query->result();
	}
	
	
	/*
	 * @description: This function is used to get all field data 
	 * function use in common_helper
	 * @where: fieldsArray
	 * @return: object
	 */ 
	
	public function getformfieldid($fieldsArray){
		$this->db->select('id,field_name');
		$this->db->from('custom_form_fields');
		$this->db->where_in('id',$fieldsArray);
		$query = $this->db->get();
		return $query->result();
	}
	
	/*
	 * @description: This function is used to get all field value data 
	 * function use in common_helper
	 * @where: 	fieldsArray
	 * @where: 	registrant register_user_id
	 * @return: object
	 */ 
	
	public function getformfieldvalue($fieldsArray,$registrantUserId){
		$this->db->select('value');
		$this->db->from('frnt_form_field_value');
		$this->db->where('registered_user_id',$registrantUserId);
		$this->db->where_in('field_id',$fieldsArray);
		$query = $this->db->get();
		return $query->result();
	}
	
	/*
	 * @description: This function is used to get all field value data 
	 * function use in common_helper
	 * @where: 	fieldsArray
	 * @where: 	registrant register_user_id
	 * @return: object
	 */ 
	
	public function getformfieldallvalue($fieldsArray,$registrantUserId){
		$this->db->select('fffv.value, cff.field_name');
		$this->db->from('frnt_form_field_value as fffv');
		$this->db->join('custom_form_fields as cff', 'cff.id = fffv.field_id');
		$this->db->where('fffv.registered_user_id',$registrantUserId);
		$this->db->where_in('fffv.field_id',$fieldsArray);
		$query = $this->db->get();
		return $query->result();
	}
	
	/*
	 * @description: This function for get payment details table data 
	 * @return: object
	 */ 
	
	public function paymentslist(){	
		
		//search by below fields
		$eventId = currentEventId();
		//decode entered eventid
		$eventId = decode($eventId); 
		
		//set query
		$this->db->select('fpd.*,fu.firstname,fu.email,fe.event_title');
		$this->db->select('COUNT(fr.payment_id) as regscount,fr.event_id, fr.registration_booking_number');
		$this->db->from('frnt_payment_details as fpd');
		$this->db->join($this->tablenmuser.' as fu', 'fpd.user_id = fu.id');
		$this->db->join($this->tablenmfrntregistrant.' as fr', 'fr.payment_id = fpd.id');
		$this->db->join($this->tablenmevent.' as fe', 'fe.id = fr.event_id');
		$this->db->group_by('fr.payment_id'); 
		
		$this->db->where('fe.id',$eventId);
		//$this->db->order_by("fpd.id", "desc"); 
		$this->db->order_by("fpd.payment_type", "asc"); 
		if(!empty($limit)){
			$this->db->limit($limit,$start);
		}
		$query = $this->db->get();
		return $query->result();
	}
	
	
	/*
	 * @description: This function for get payment user details table data 
	 * @param: paymentId
	 * @return: object
	 */ 
	
	public function paymentsuserdetails($paymentId){	
		$this->db->select('fpd.*,fu.firstname,fu.email');
		$this->db->from('frnt_payment_details as fpd');
		$this->db->join($this->tablenmuser.' as fu', 'fpd.user_id = fu.id');
		$this->db->order_by('fpd.id', 'desc'); 
		$this->db->where_in('fpd.id',$paymentId);
		$query = $this->db->get();
		return $query->row();
	}
	
	
	
	//--------------------------------------------------------------------------
	
	/*
	 * @descript: This function is used to showing current evetns list
	 * @param $userId
	 * @return object
	 * 
	 */ 
	
	function geteventslist($userId){
		
		$this->db->select('id,event_title');
		$this->db->from($this->tablenmevent);
		$this->db->order_by('id','DESC');
		$this->db->where('user_id',$userId);
		$this->db->where('is_approved','1');
		$query = $this->db->get();
		return $query->result();
	}
	
	
	//--------------------------------------------------------------------------
	
	/*
	 * @descript: This function is used to showing selected evetns details
	 * @param $userId
	 * @return object
	 * 
	 */ 
	
	function geteventsdetails(){
		
		$eventId  	= $this->input->post('eventid');
		$this->db->select('id,event_title,starttime,endtime');
		$this->db->from($this->tablenmevent);
		$this->db->where('id',$eventId);
		$query = $this->db->get();
		return $query->row();
	}
	
	
	//--------------------------------------------------------------------------
	
	/*
	 * @descript: This function is used to get breakout details by registration user id
	 * @param $registrationUserId
	 * @return object
	 * 
	 */ 
	
	function registrationbreakout($registrationUserId){
		
		$this->db->select('frntb.id, frntb.registered_user_id,bstream.id as stream_id,bstream.stream_name, bstream.stream, bsession.id as session_id, bsession.session, brk.breakout_name, brk.id as breakout_id');
		$this->db->from($this->tablenmfrntbreakouts.' as frntb');
		$this->db->join('breakout_stream_session as bss', 'bss.id = frntb.stream_session_id');
		$this->db->join('breakout_stream as bstream', 'bstream.id = bss.breakout_stream_id','left');
		$this->db->join('breakout_session as bsession', 'bsession.id = bss.breakout_session_id','left');
		$this->db->join('breakouts as brk', 'brk.id = bstream.breakout_id','left');
		
		$this->db->where_in('registered_user_id',$registrationUserId);
		
		$query = $this->db->get();
		return $query->result_array();
	}
	
	
	//--------------------------------------------------------------------------
	
	/*
	 * @descript: This function is used to get side event data by event id
	 * @param $eventId
	 * @return object
	 * 
	 */ 
	
	function regissideeventlist($eventId){
		$this->db->select('se.side_event_name, se.id as side_event_id, ser.registrant_id, ser.id as side_event_regis_id,ser.additional_guest, ser.maximum_number_of_additional_guest');
		$this->db->from('side_event as se');
		$this->db->join('side_event_registrant as ser', 'ser.side_event_id = se.id');
		$this->db->where('se.event_id',$eventId);
		$this->db->where('ser.additional_guest > ','0');
		$this->db->order_by("ser.maximum_number_of_additional_guest", "desc"); 
		$query = $this->db->get();
		return $query->result_array();
	}
	
	
	
	
	//--------------------------------------------------------------------------
	
	/*
	 * @descript: This function is used to get registration side event list 
	 * @param $sideeventRegisId
	 * @return array
	 * 
	 */ 
	
	function registrationsideevent($sideeventRegisId){
		
		$this->db->select('frntseg.*, ed.dietary_name');
		$this->db->from('frnt_side_event_guest as frntseg');
		$this->db->join('event_dietary as ed', 'ed.id = frntseg.dietary_requirment_id');
		$this->db->where_in('frnt_side_event_id',$sideeventRegisId);
		$query = $this->db->get();
		return $query->result_array();
	}
	
}

/* End of file report_model.php */
/* Location: ./system/application/modules/report/model/report_model.php */
