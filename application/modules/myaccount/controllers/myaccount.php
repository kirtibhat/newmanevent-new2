<?php

/*
 * @description: This controller is used to manage myprofile, billing, multiplyusers
 * @create date: 25-june-2014
 * @auther: Lokendra Meena
 * @email: lokendrameena@cdnsol.com
 * 
 */ 

class Myaccount extends MX_Controller{

	function __construct(){
		parent::__construct();
		$this->load->library('factory',$this);
	}

	/*
    * @description: This function is used to manage user profile details
    * @load: my profile view
    */
     
	function myprofile()
	{
		$this->factory->myprofile();
	}
	
	//--------------------------------------------------------------------------
	
	/*
    * @description: This function is used to manage billing manage
    * @load: billing view
    */
	
	function billing()
	{
		$this->factory->billing();
	}
	
	//--------------------------------------------------------------------------
	 
   /*
    * @description: This function is used to manage multiply users
    * @load: multiply users view
    */ 
	  
	function multiplyusers()
	{
			$this->factory->multiplyusers();
	}
	
	
	// ------------------------------------------------------------------------ 
	
	/*
	 * @description: This function is used to set custome error without any field on sever side
	 * @return false
	 */
	
	public function custom_error_set($str)
	{
		$this->form_validation->set_message('custom_error_set', '%s');
		return FALSE;
	}
  
}

/* End of file myaccount.php */
/* Location: ./system/application/modules/myaccount/controllers/myaccount.php */
?>
