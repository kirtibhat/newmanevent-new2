<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 * This class is used to manage for my account controller logic and data
 *
 * @package   myaccount manage
 * @author    Lokendra Meena
 * @email     lokendrameena@cdnsol.com
 * @since     Version 1.0
 * @filesource
 */
 
 
class Factory{
	 
	//set limit of show records 
	private $limit = '10'; 
	private $ci = NULL;   
	
	function __construct($ci){
		//create instance object
		$this->ci = $ci;
		$this->ci->load->library(array('head','template'));
		$this->ci->load->model(array('myaccount_model'));
		$this->ci->load->language(array('myaccount'));
		$this->ci->form_validation->set_error_delimiters('<div class="server_msg_show">', '</div>');
		$this->ci->session_check->checkSession(); 
	}
	
	
	/*
    * @description: This function is used to manage user profile details
    * @load: my profile view
    */
     
	public function myprofile()
	{
		
		if($this->ci->input->is_ajax_request()){
		
			$formActionName = $this->ci->input->post('formActionName');
		
			switch($formActionName){
				
				case "contactDetails":
					$this->_updatecontactDetails();
				break;
				
				case "accountInformation":
					$this->_updateAccountInformation();
				break;
					
			}		
		}else{
			//calling view show
			$this->_mypofileView();
		}
	}
	
	
	//-------------------------------------------------------------------------
	
	/*
	 *@description: This function is used to validate my profile 
	 *@return: void 
	 */ 
	 
	 function _mypofileView(){
		 
		$userId 					= isLoginUser();
		$data['userContactDetails'] = $this->ci->myaccount_model->userContactDetails($userId);
		$data['userId'] 			= $userId;
		$this->ci->template->load('template','my_profile',$data,TRUE);
	}
	
	//-------------------------------------------------------------------------
	
	/*
	 *@description: This function is used to update contact personal detail
	 *@return: void 
	 */ 
	 
	 function _updatecontactDetails(){
		 
		 //get logged in user id
		$userId 			= isLoginUser();
		
		//This function is used to validate
		$this->_contactDetailsValidate();	
		
		if ($this->ci->form_validation->run($this->ci))
		{
			//call for save my profile data save
			$this->_contactDetailsSave($userId);
			
			$msg = lang('myaccount_msg_contact_detals');
			echo json_message('msg',$msg);
		}
		else
		{
			$errors = $this->ci->form_validation->error_array();
			echo json_message('msg',$errors,'is_success','false');
		}
	}
	
	
	//-------------------------------------------------------------------------
	
	/*
	 *@description: This function is used to validate my profile 
	 *@return: void 
	 */ 
		private function _contactDetailsValidate(){
		
		// set ruls for form filed
		$this->ci->form_validation->set_rules('contactFirstName', 'first name', 'trim|required');
		$this->ci->form_validation->set_rules('contactSurname', 'surname', 'trim|required');
		$this->ci->form_validation->set_rules('contactOrganisation', 'organisation', 'trim|required');
		$this->ci->form_validation->set_rules('contactPhone', 'phone', 'trim|required');
		$this->ci->form_validation->set_rules('contactAddress1', 'address', 'trim|required');
		$this->ci->form_validation->set_rules('contactCity', 'city', 'trim|required');
		$this->ci->form_validation->set_rules('contactState', 'state', 'trim|required');
		$this->ci->form_validation->set_rules('contactCountry', 'country', 'trim|required');
		$this->ci->form_validation->set_rules('contactPostcode', 'postcode', 'trim|required');
		$this->ci->form_validation->set_rules('contactWebsite', 'website', 'trim|required');
			
	}
	
	
	// ------------------------------------------------------------------------ 
	
	/*
	 * @description: This function is used to save contact personal details
	 * @return void
	 * 
	 */ 
	
	function _contactDetailsSave($userId){
		
		// get post data
		$updateData['user_title']		 = $this->ci->input->post('contactTitle');
		$updateData['firstname'] 		 = $this->ci->input->post('contactFirstName');
		$updateData['lastname'] 		 = $this->ci->input->post('contactSurname');
		$updateData['company_name']  	 = $this->ci->input->post('contactOrganisation');
		$updateData['phone']  			 = $this->ci->input->post('contactPhone');
		$updateData['fax'] 			  	 = $this->ci->input->post('contactFax');
		$updateData['mobile'] 			 = $this->ci->input->post('contactMobile');
		$updateData['address_line1']  	 = $this->ci->input->post('contactAddress1');
		$updateData['address_line2']  	 = $this->ci->input->post('contactAddress2');
		$updateData['city']  			 = $this->ci->input->post('contactCity');
		$updateData['state']  			 = $this->ci->input->post('contactState');
		$updateData['country'] 			 = $this->ci->input->post('contactCountry');
		$updateData['postcode']    		 = $this->ci->input->post('contactPostcode');
		$updateData['website']  		 = $this->ci->input->post('contactWebsite');
		
		$where = array('id'=>$userId);
		$this->ci->common_model->updateDataFromTabel('user',$updateData,$where);	
	}
	
	//-------------------------------------------------------------------------
	
	/*
	 *@description: This function is used to update account information detail
	 *@return: void 
	 */ 
	 
	 function _updateAccountInformation(){
		 
		//get logged in user id
		$userId 	= isLoginUser();
		
		//This function is used to validate
		$this->_accountInformationValidate($userId);	
		
		if ($this->ci->form_validation->run($this->ci))
		{
			//call for data save
			$this->_accountInformationSave($userId);
			
			$msg = lang('myaccount_msg_password');
			echo json_message('msg',$msg);
		}
		else
		{
			$errors = $this->ci->form_validation->error_array();
			echo json_message('msg',$errors,'is_success','false');
		}
	}
	
	//-------------------------------------------------------------------------
	
	/*
	 *@description: This function is used to validate account information  
	 *@return: void 
	 */ 
		private function _accountInformationValidate($userId){
		
		// set ruls for form filed
		$this->ci->form_validation->set_rules('varifyCurrentPassword', 'varify current password', 'trim|required');
		$this->ci->form_validation->set_rules('newPassword', 'new password', 'trim|required|matches[confirmNewPassword]|min_length[5]|max_length[25]');
		$this->ci->form_validation->set_rules('confirmNewPassword', 'confirm password', 'trim|required|min_length[5]|max_length[25]');
		
		//check varify password from database
		if(!empty($userId)){
			//get user data
			$where = array('id'=>$userId);
			$userdata = $this->ci->common_model->getDataFromTabel('user','id,password',$where);
			if(!empty($userdata)){
				$userdata 				= $userdata[0];
				$userOldPassword 		=   $userdata->password;
				$varifyCurrentPassword	= 	md5($this->ci->input->post('varifyCurrentPassword'));
				//check old password with new password
				if($userOldPassword != $varifyCurrentPassword){
						$this->ci->form_validation->set_rules('currentPassword', 'Varify current password is not correct.', 'callback_custom_error_set');
				}
			}
		}	
	
	}
	
	// ------------------------------------------------------------------------ 
	
	/*
	 * @description: This function is used to save account information details
	 * @return void
	 */ 
	
	function _accountInformationSave($userId){
		
		// get post data
		$updateData['password']		 = md5($this->ci->input->post('newPassword'));
		
		//update data in user uable
		$where = array('id'=>$userId);
		$this->ci->common_model->updateDataFromTabel('user',$updateData,$where);	
	}
	
	//--------------------------------------------------------------------------
	
	/*
    * @description: This function is used to manage billing manage
    * @load: billing view
    */
	
	function billing()
	{
		if($this->ci->input->is_ajax_request()){
		
			$formActionName = $this->ci->input->post('formActionName');
		
			switch($formActionName){
				
				case "billInfoDetails":
					$this->_updateBillingInformation();
				break;
				
				case "debitCardInfo":
					$this->_updateDebitCardInformation();
				break;
					
			}		
		}else{
			//calling view show
			$this->billingView();
		}
	}
	
	//--------------------------------------------------------------------------
	
	/*
    * @description: This function is used to manage billing view show
    * @load: billing view
    */
	
	function billingView()
	{
		$userId 				= isLoginUser();
		$data['userId'] 		= $userId;
		$whereUserId 			= array('user_id'=>$userId);
		$whereUser 				= array('id'=>$userId);
		
		//get user data
		$userdata				= $this->ci->common_model->getDataFromTabel('user','*',$whereUser);
		
		//get billing formation data
		$billingDetails 		= $this->ci->common_model->getDataFromTabel('user_billing_information','*',$whereUserId);
		
		//check data is exist then set data array
		if(!empty($billingDetails)){
			$billingDetails 	= $billingDetails[0];
		}
		
		//get credit card  information data
		$creditCardDetails 		= $this->ci->common_model->getDataFromTabel('user_credit_Card_information','*',$whereUserId);
		
		//check data is exist then set data array
		if(!empty($creditCardDetails)){
			$creditCardDetails 	= $creditCardDetails[0];
		}
		
		//check data is exist then set data array
		if(!empty($userdata)){
			$userdata 			= $userdata[0];
		}
		
		//set data details 
		$data['billingDetails']			 = $billingDetails;
		$data['creditCardDetails']		 = $creditCardDetails;
		$data['userDetails']			 = $userdata;
		
		$this->ci->template->load('template','billing',$data,TRUE);
	}
	
	
	//-------------------------------------------------------------------------
	
	/*
	 *@description: This function is used to update billing informatin detail
	 *@return: void 
	 */ 
	 
	 function _updateBillingInformation(){
		 
		 //get logged in user id
		$userId 			= isLoginUser();
		
		//This function is used to validate
		$this->_billingInformationValidate();	
		
		if ($this->ci->form_validation->run($this->ci))
		{
			//call for save my profile data save
			$this->_billingInformationSave($userId);
			
			$msg = lang('myaccount_msg_bill_info');
			echo json_message('msg',$msg);
		}
		else
		{
			$errors = $this->ci->form_validation->error_array();
			echo json_message('msg',$errors,'is_success','false');
		}
	}
	
	
	
	//-------------------------------------------------------------------------
	
	/*
	 *@description: This function is used to validate billing information 
	 *@return: void 
	 */ 
		private function _billingInformationValidate(){
		
		// set ruls for form filed
		$this->ci->form_validation->set_rules('billInfoFirstName', 'first name', 'trim|required');
		$this->ci->form_validation->set_rules('billInfoSurname', 'surname', 'trim|required');
		$this->ci->form_validation->set_rules('billInfoEmail', 'email', 'trim|required|email');
		$this->ci->form_validation->set_rules('billInfoOrganisation', 'organisation', 'trim|required');
		$this->ci->form_validation->set_rules('billInfoPhone', 'phone', 'trim|required');
		$this->ci->form_validation->set_rules('billInfoAddress1', 'address', 'trim|required');
		$this->ci->form_validation->set_rules('billInfoCity', 'city', 'trim|required');
		$this->ci->form_validation->set_rules('billInfoState', 'state', 'trim|required');
		$this->ci->form_validation->set_rules('billInfoCountry', 'country', 'trim|required');
		$this->ci->form_validation->set_rules('billInfoPostcode', 'postcode', 'trim|required');
			
	}
	
	// ------------------------------------------------------------------------ 
	
	/*
	 * @description: This function is used to save billing information
	 * @return void
	 * 
	 */ 
	
	function _billingInformationSave($userId){
		
		// get post data
		$updateData['user_id'] 			 	= $userId;
		$updateData['apply_user_address'] 	= $this->ci->input->post('billInfoApplyUserInfo');
		$updateData['firstname'] 		 	= $this->ci->input->post('billInfoFirstName');
		$updateData['surname'] 		 	 	= $this->ci->input->post('billInfoSurname');
		$updateData['email']  	 		 	= $this->ci->input->post('billInfoEmail');
		$updateData['organisation']  	 	= $this->ci->input->post('billInfoOrganisation');
		$updateData['phone']  			 	= $this->ci->input->post('billInfoPhone');
		$updateData['fax'] 			  	 	= $this->ci->input->post('billInfoFax');
		$updateData['mobile'] 			 	= $this->ci->input->post('billInfoMobile');
		$updateData['address1']  		 	= $this->ci->input->post('billInfoAddress1');
		$updateData['address2'] 	 	 	= $this->ci->input->post('billInfoAddress2');
		$updateData['city']  			 	= $this->ci->input->post('billInfoCity');
		$updateData['state']  			 	= $this->ci->input->post('billInfoState');
		$updateData['country'] 			 	= $this->ci->input->post('billInfoCountry');
		$updateData['postcode']  	     	= $this->ci->input->post('billInfoPostcode');
		
		$whereBilling 			= array('user_id'=>$userId);
		//get billing formation data
		$billingDetails 	= $this->ci->common_model->getDataFromTabel('user_billing_information','*',$whereBilling);
		
		//billing data update
		if(!empty($billingDetails)){
			$this->ci->common_model->updateDataFromTabel('user_billing_information',$updateData,$whereBilling);	
		}else{
			//billing data insert
			$this->ci->common_model->addDataIntoTabel('user_billing_information', $updateData);
		}
		
	}
	
	//-------------------------------------------------------------------------
	
	/*
	 *@description: This function is used to update debit card informatin detail
	 *@return: void 
	 */ 
	 
	 function _updateDebitCardInformation(){
		 
		 //get logged in user id
		$userId 			= isLoginUser();
		
		//This function is used to validate
		$this->_debitCardInformationValidate();	
		
		if ($this->ci->form_validation->run($this->ci))
		{
			//call for save my profile data save
			$this->_debitCardInformationSave($userId);
			
			$msg = lang('myaccount_msg_bill_info');
			echo json_message('msg',$msg);
		}
		else
		{
			$errors = $this->ci->form_validation->error_array();
			echo json_message('msg',$errors,'is_success','false');
		}
	}
	
	//-------------------------------------------------------------------------
	
	/*
	 *@description: This function is used to validate debit card information 
	 *@return: void 
	 */ 
		private function _debitCardInformationValidate(){
		
		// set ruls for form filed
		$this->ci->form_validation->set_rules('nameOnCard', 'name on card', 'trim|required');
		$this->ci->form_validation->set_rules('creditCardNumber', 'credit card number', 'trim|required');
		$this->ci->form_validation->set_rules('securityCode', 'security code', 'trim|required');
		$this->ci->form_validation->set_rules('debitExpiryMonth', 'expiry month', 'trim|required');
		$this->ci->form_validation->set_rules('debitExpiryYear', 'expiry year', 'trim|required');
			
	}
	
	
	// ------------------------------------------------------------------------ 
	
	/*
	 * @description: This function is used to save debit card information
	 * @return void
	 * 
	 */ 
	
	function _debitCardInformationSave($userId){
		
		// get post data
		$updateData['user_id'] 			 		= $userId;
		$updateData['name_on_card'] 		 	= $this->ci->input->post('nameOnCard');
		$updateData['credit_card_number'] 		= base64_encode($this->ci->input->post('creditCardNumber'));
		$updateData['security_code']  	 		= base64_encode($this->ci->input->post('securityCode'));
		$updateData['expiry_month']  		 	= $this->ci->input->post('debitExpiryMonth');
		$updateData['expiry_year']  	 		= $this->ci->input->post('debitExpiryYear');
		
		$whereDebitCard 			= array('user_id'=>$userId);
		//get debit card formation data
		$debitCardDetails 	= $this->ci->common_model->getDataFromTabel('user_credit_Card_information','*',$whereDebitCard);
		
		//debit card data update
		if(!empty($debitCardDetails)){
			$this->ci->common_model->updateDataFromTabel('user_credit_Card_information',$updateData,$whereDebitCard);	
		}else{
			//debit card data insert
			$this->ci->common_model->addDataIntoTabel('user_credit_Card_information', $updateData);
		}
		
	}
	
	//--------------------------------------------------------------------------
	
	/*
    * @description: This function is used to manage multiply users
    * @load: multiply_users view
    */
	
	function multiplyusers()
	{
		$userId = isLoginUser();
		$data['userId'] = $userId;
		$this->ci->template->load('template','multiply_users',$data,TRUE);
	}
	
	
}


/* End of file factory.php */
/* Location: ./system/application/modules/myaccount/libraries/factory.php */
