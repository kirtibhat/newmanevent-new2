<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>


<div class="spacer40">
</div>
<div class="row-fluid">
	<div class="headingh1">
		<?php echo lang('myaccount_my_profile'); ?>
	</div>
</div>
<div class="spacer40">
</div>
<div class="row-fluid mt20">
	<div class="span7">
		
		
		<?php $this->load->view('my_profile_contact_details_view'); ?>
		<!-- /contact details -->
	
		<?php $this->load->view('my_profile_account_information'); ?>
		<!-- /account information -->

	</div>
	
	<?php $this->load->view('event/right_side_advert'); ?>
	
</div>

