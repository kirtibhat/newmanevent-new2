<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$accFirstNameValue      = (!empty($userContactDetails->firstname))?$userContactDetails->firstname:'';
$accSurnameValue        = (!empty($userContactDetails->lastname))?$userContactDetails->lastname:'';
$accEmailValue        	= (!empty($userContactDetails->email))?$userContactDetails->email:'';
$accfullName			=  ucwords($accFirstNameValue.' '.$accSurnameValue);

$formAccountInformation = array(
		'name'	 => 'formAccountInformation',
		'id'	 => 'formAccountInformation',
		'method' => 'post',
		'class'  => 'form-horizontal',
	);
	
$varifyCurrentPassword = array(
		'name'	=> 'varifyCurrentPassword',
		'value'	=> '',
		'id'	=> 'varifyCurrentPassword',
		'type'	=> 'password',
		'required'	=> '',
	);
	
$newPassword = array(
		'name'	=> 'newPassword',
		'value'	=> '',
		'id'	=> 'newPassword',
		'type'	=> 'password',
		'required'	=> '',
	);
	
$confirmNewPassword = array(
		'name'	=> 'confirmNewPassword',
		'value'	=> '',
		'id'	=> 'confirmNewPassword',
		'type'	=> 'password',
		'required'	=> '',
	);
	
					

?>

<div class="commonform_bg">
	<div class="headingbg bg_F89728 brd_botm_0px">
		<?php echo lang('myaccount_accinfo'); ?>
	</div>
	
	<?php echo form_open($this->uri->uri_string(),$formAccountInformation); ?>
	
	<div class="form-horizontal">
		<div class="control-group mb10">
			<div class="control-group mb10">
				<label class="control-label" for="inputEmail">
					<?php echo lang('myaccount_accinfo_name'); ?>
				</label>
				<div class="controls ft_15 mt_8">
					<?php echo $accfullName; ?>
				</div>
			</div>
			<div class="control-group mb10">
				<label class="control-label" for="inputEmail">
					<?php echo lang('myaccount_accinfo_operator_id'); ?>
				</label>
				<div class="controls ft_15 mt_8">
					<?php echo $accEmailValue; ?>
				</div>
			</div>
			<div class="control-group mb10">
				<label class="control-label" for="inputEmail">
					<?php echo lang('myaccount_accinfo_varify_curr_pass'); ?>
				</label>
				<div class="controls">
					<?php echo form_input($varifyCurrentPassword); ?>
					<?php echo form_error('varifyCurrentPassword'); ?>
				</div>
			</div>
			<div class="control-group mb10">
				<label class="control-label" for="inputEmail">
					<?php echo lang('myaccount_accinfo_new_password'); ?>
				</label>
				<div class="controls">
					<?php echo form_input($newPassword); ?>
					<?php echo form_error('newPassword'); ?>
				</div>
			</div>
			<div class="control-group mb10">
				<label class="control-label" for="inputEmail">
					<?php echo lang('myaccount_accinfo_confirm_new_pass'); ?>
				</label>
				<div class="controls">
					<?php echo form_input($confirmNewPassword); ?>
					<?php echo form_error('confirmNewPassword'); ?>
				</div>
			</div>
		</div>
		<div class="row-fluid mt20">
			<?php 
					echo form_hidden('formActionName', 'accountInformation');
					//button show
					$formButton['showbutton']   = array('save','reset');
					$formButton['labletext'] 	= array('save'=>lang('comm_save_changes'),'reset'=>lang('comm_reset'));
					$formButton['buttonclass']  = array('save'=>'submitbtn_cus mr_16','reset'=>'submitbtn_cus reset_form');
					$this->load->view('event/common_save_reset_button',$formButton);
			?>
		</div>
	</div>
	
	<?php echo form_close(); ?>
</div>

<script type="text/javascript">
//call for data save
ajaxdatasave('formAccountInformation','<?php echo $this->uri->uri_string(); ?>',false,true,false,false,true,false,false);
</script>

