<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//credit card information
$nameOnCardValue 	       = (!empty($creditCardDetails->name_on_card))?$creditCardDetails->name_on_card:'';
$creditCardNumberValue     = (!empty($creditCardDetails->credit_card_number))?$creditCardDetails->credit_card_number:'';
$securityCodeValue         = (!empty($creditCardDetails->security_code))?$creditCardDetails->security_code:'';
$expiryMonthValue		   = (!empty($creditCardDetails->expiry_month))?$creditCardDetails->expiry_month:'';
$expiryYearValue 		   = (!empty($creditCardDetails->expiry_year))?$creditCardDetails->expiry_year:'';


$formDebitCardInfo = array(
		'name'	 => 'formDebitCardInfo',
		'id'	 => 'formDebitCardInfo',
		'method' => 'post',
		'class'  => 'form-horizontal',
	);
	
$nameOnCard = array(
		'name'	=> 'nameOnCard',
		'value'	=> $nameOnCardValue,
		'id'	=> 'nameOnCard',
		'type'	=> 'text',
		'required'	=> '',
	);	

$creditCardNumber = array(
		'name'	=> 'creditCardNumber',
		'value'	=> base64_decode($creditCardNumberValue),
		'id'	=> 'creditCardNumber',
		'type'	=> 'password',
		'required'	=> '',
	);	
	
$securityCode = array(
		'name'	=> 'securityCode',
		'value'	=> base64_decode($securityCodeValue),
		'id'	=> 'securityCode',
		'type'	=> 'password',
		'required'	=> '',
	);	
	

$monthArray = array(
			  ''  => 'Month',
			  '1' => 'January',
			  '2' => 'February',
			  '3' => 'March',
			  '4' => 'April',
			  '5' => 'May',
			  '6' => 'June',
			  '7' => 'July',
			  '8' => 'August',
			  '9' => 'September',
			  '10' => 'October',
			  '11' => 'November',
			  '12' => 'December',
			);
                
$yearArray['']    = 'Year';
for($i=date("Y");$i>=1910;$i--){
$yearArray[$i]   = $i;
}                
						
?>

<div class="commonform_bg">
	<div class="headingbg bg_F89728 brd_botm_0px">
		<?php echo lang('myaccount_bill_debit_card');?>
	</div>
	<?php echo form_open($this->uri->uri_string(),$formDebitCardInfo); ?>
		
		<div class="form-horizontal">
		<div class="control-group mb10">
			
			
			<div class="control-group mb10">
				<label class="control-label" for="inputEmail">
					<?php echo lang('myaccount_debitcard_name_on_card');?>
					<font class="mandatory_cls">*</font>
				</label>
				<div class="controls">
					<?php echo form_input($nameOnCard); ?>
					<?php echo form_error('nameOnCard'); ?>
				</div>
			</div>
			
			<div class="control-group mb10">
				<label class="control-label" for="inputEmail">
					<?php echo lang('myaccount_debitcard_cardit_num');?>
					<font class="mandatory_cls">*</font>
				</label>
				<div class="controls">
					<?php echo form_input($creditCardNumber); ?>
					<?php echo form_error('creditCardNumber'); ?>
				</div>
			</div>
			
			<div class="control-group mb10">
				<label class="control-label" for="inputEmail">
					<?php echo lang('myaccount_debitcard_security_code');?>
					<font class="mandatory_cls">*</font>
				</label>
				<div class="controls">
					<?php echo form_input($securityCode); ?>
					<?php echo form_error('securityCode'); ?>
				</div>
			</div>
			
			<div class="control-group mb10">
				<label class="control-label" for="debitExpiryMonth"><?php echo lang('myaccount_debitcard_expiry_month');?></label>
				<div class="controls">
					<div class="fleft">
						<div class="pull-left select-style register select_eventdetailP width90per">
							<?php 
								$other = ' id="debitExpiryMonth" required =""';
								echo form_dropdown('debitExpiryMonth',$monthArray,$expiryMonthValue,$other);
								echo form_error('debitExpiryMonth');
							?>
						</div>
					</div>

					<div class="fleft">
						<div class="pull-left select-style register select_eventdetailP width90per">
							<?php 
								$other = ' id="debitExpiryYear" required =""';
								echo form_dropdown('debitExpiryYear',$yearArray,$expiryYearValue,$other);
								echo form_error('debitExpiryYear');
							?>
						</div>
					</div>
				</div>
			</div>
				
			
		</div>
		
		<div class="row-fluid mt20">
			<?php 
				//form post action
				echo form_hidden('formActionName', 'debitCardInfo');
				
				//button show
				$formButton['showbutton']   = array('save','reset');
				$formButton['labletext'] 	= array('save'=>lang('comm_save_changes'),'reset'=>lang('comm_reset'));
				$formButton['buttonclass']  = array('save'=>'submitbtn_cus mr_16','reset'=>'submitbtn_cus reset_form');
				$this->load->view('event/common_save_reset_button',$formButton);
			?>
		</div>
	</div>
	
	<?php echo form_close(); ?>
</div>
<script type="text/javascript">
//call for data save
ajaxdatasave('formDebitCardInfo','<?php echo $this->uri->uri_string(); ?>',false,true,false,false,true,false,false);
</script>
