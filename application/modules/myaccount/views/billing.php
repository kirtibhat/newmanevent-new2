<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>


<div class="spacer40">
</div>
<div class="row-fluid">
	<div class="headingh1">
		<?php echo lang('myaccount_billing'); ?>
	</div>
</div>
<div class="spacer40">
</div>
<div class="row-fluid mt20">
	<div class="span7">
	
		<?php $this->load->view('billing_billing_paln_summary'); ?>
		<!--plan summery -->
		
		<?php $this->load->view('billing_billing_latest_bills'); ?>
		<!--latest bills -->		
		
		<?php $this->load->view('billing_billing_information_view'); ?>
		<!--Billing Information -->
	
		<?php $this->load->view('billing_billing_debit_card_view'); ?>
		<!--Direct Debit Card Information -->

	</div>
	
	<?php $this->load->view('event/right_side_advert'); ?>
	
</div>

