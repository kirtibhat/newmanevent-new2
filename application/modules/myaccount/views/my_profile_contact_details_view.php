<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$contactTitleValue     		= (!empty($userContactDetails->user_title))?$userContactDetails->user_title:'';
$contactFirstNameValue      = (!empty($userContactDetails->firstname))?$userContactDetails->firstname:'';
$contactSurnameValue        = (!empty($userContactDetails->lastname))?$userContactDetails->lastname:'';
$contactOrganisationValue   = (!empty($userContactDetails->company_name))?$userContactDetails->company_name:'';
$contactEmailValue        	= (!empty($userContactDetails->email))?$userContactDetails->email:'';
$contactPhoneValue        	= (!empty($userContactDetails->phone))?$userContactDetails->phone:'';
$contactFaxValue       	 	= (!empty($userContactDetails->fax))?$userContactDetails->fax:'';
$contactMobileValue        	= (!empty($userContactDetails->mobile))?$userContactDetails->mobile:'';
$contactAddress1Value       = (!empty($userContactDetails->address_line1))?$userContactDetails->address_line1:'';
$contactAddress2Value       = (!empty($userContactDetails->address_line2))?$userContactDetails->address_line2:'';
$contactCityValue        	= (!empty($userContactDetails->city))?$userContactDetails->city:'';
$contactStateValue        	= (!empty($userContactDetails->state))?$userContactDetails->state:'';
$contactPostcodeValue      	= (!empty($userContactDetails->postcode))?$userContactDetails->postcode:'';
$contactCountryValue      	= (!empty($userContactDetails->country))?$userContactDetails->country:'australia';
$contactWebsiteValue       	= (!empty($userContactDetails->website))?$userContactDetails->website:'';


$formContactDetails = array(
		'name'	 => 'formContactDetails',
		'id'	 => 'formContactDetails',
		'method' => 'post',
		'class'  => 'form-horizontal',
	);
	
$contactFirstName = array(
		'name'	=> 'contactFirstName',
		'value'	=> $contactFirstNameValue,
		'id'	=> 'contactFirstName',
		'type'	=> 'text',
		'required'	=> '',
	);	

$contactSurname = array(
		'name'	=> 'contactSurname',
		'value'	=> $contactSurnameValue,
		'id'	=> 'contactSurname',
		'type'	=> 'text',
		'required'	=> '',
	);	
	
$contactOrganisation = array(
		'name'	=> 'contactOrganisation',
		'value'	=> $contactOrganisationValue,
		'id'	=> 'contactOrganisation',
		'type'	=> 'text',
		'required'	=> '',
	);	
	
$contactEmail = array(
		'name'	=> 'contactEmail',
		'value'	=> $contactEmailValue,
		'id'	=> 'contactEmail',
		'type'	=> 'text',
		'readonly'	=> true,
	);		
	
$contactPhone = array(
		'name'	=> 'contactPhone',
		'value'	=> $contactPhoneValue,
		'id'	=> 'contactPhone',
		'type'	=> 'text',
		'required'	=> '',
	);

$contactFax = array(
		'name'	=> 'contactFax',
		'value'	=> $contactFaxValue,
		'id'	=> 'contactFax',
		'type'	=> 'text',
		'required'	=> '',
	);	

$contactMobile = array(
		'name'	=> 'contactMobile',
		'value'	=> $contactMobileValue,
		'id'	=> 'contactMobile',
		'type'	=> 'text',
		'required'	=> '',
	);	

$contactAddress1 = array(
		'name'	=> 'contactAddress1',
		'value'	=> $contactAddress1Value,
		'id'	=> 'contactAddress1',
		'type'	=> 'text',
		'required'	=> '',
	);	
	
$contactAddress2 = array(
		'name'	=> 'contactAddress2',
		'value'	=> $contactAddress2Value,
		'id'	=> 'contactAddress2',
		'type'	=> 'text',
	);		

$contactCity = array(
		'name'	=> 'contactCity',
		'value'	=> $contactCityValue,
		'id'	=> 'contactCity',
		'type'	=> 'text',
		'required'	=> '',
	);	
	
$contactState = array(
		'name'	=> 'contactState',
		'value'	=> $contactStateValue,
		'id'	=> 'contactState',
		'type'	=> 'text',
		'required'	=> '',
	);	
		
$contactPostcode = array(
		'name'	=> 'contactPostcode',
		'value'	=> $contactPostcodeValue,
		'id'	=> 'contactPostcode',
		'type'	=> 'text',
		'required'	=> '',
	);			
	
$contactWebsite = array(
		'name'	=> 'contactWebsite',
		'value'	=> $contactWebsiteValue,
		'id'	=> 'contactWebsite',
		'type'	=> 'text',
		'required'	=> '',
	);
	
$contactCountryList  	  = array('australia'=>'Australia');
						
?>

<div class="commonform_bg">
	<div class="headingbg bg_F89728 brd_botm_0px">
		<?php echo lang('myaccount_contact_details');?>
	</div>
	<?php echo form_open($this->uri->uri_string(),$formContactDetails); ?>
		
		<div class="form-horizontal">
		<div class="control-group mb10">
			
			<div class="control-group mb10">
				<label for="inputEmail" class="control-label">
					<?php echo lang('myaccount_contact_title');?>
				</label>
				<div class="controls pr0">
					<div class="pull-left select-style register select_eventdetailP">
					
						<?php
							$other = 'id="contactTitle"';
							$titleArray[''] = 'Select Title';
							$titleArr = $this->config->item('title_array');
							foreach($titleArr as $key=>$value){
								$titleArray[$key] = $value;
							} 
							
							echo form_dropdown('contactTitle',$titleArray,$contactTitleValue,$other);
							echo form_error('contactTitle');
						?>
						
					</div>
				</div>
			</div>
			
			<div class="control-group mb10">
				<label class="control-label" for="inputEmail">
					<?php echo lang('myaccount_contact_first_name');?>
					<font class="mandatory_cls">*</font>
				</label>
				<div class="controls">
					<?php echo form_input($contactFirstName); ?>
					<?php echo form_error('contactFirstName'); ?>
				</div>
			</div>
			<div class="control-group mb10">
				<label class="control-label" for="inputEmail">
					<?php echo lang('myaccount_contact_surname');?>
					<font class="mandatory_cls">*</font>
				</label>
				<div class="controls">
					<?php echo form_input($contactSurname); ?>
					<?php echo form_error('contactSurname'); ?>
				</div>
			</div>
			<div class="control-group mb10">
				<label class="control-label" for="inputEmail">
					<?php echo lang('myaccount_contact_organisation');?>
					<font class="mandatory_cls">*</font>
				</label>
				<div class="controls">
					<?php echo form_input($contactOrganisation); ?>
					<?php echo form_error('contactOrganisation'); ?>
				</div>
			</div>
			<div class="control-group mb10">
				<label class="control-label" for="inputEmail">
					<?php echo lang('myaccount_contact_email');?>
					<font class="mandatory_cls">*</font>
				</label>
				<div class="controls">
					<?php echo form_input($contactEmail); ?>
					<?php echo form_error('contactEmail'); ?>
				</div>
			</div>
			<div class="control-group mb10">
				<label class="control-label" for="inputEmail">
					<?php echo lang('myaccount_contact_phone');?>
					<font class="mandatory_cls">*</font>
				</label>
				<div class="controls">
					<?php echo form_input($contactPhone); ?>
					<?php echo form_error('contactPhone'); ?>
				</div>
			</div>
			<div class="control-group mb10">
				<label class="control-label" for="inputEmail">
					<?php echo lang('myaccount_contact_fax');?>
				</label>
				<div class="controls">
					<?php echo form_input($contactFax); ?>
					<?php echo form_error('contactFax'); ?>
				</div>
			</div>
			<div class="control-group mb10">
				<label class="control-label" for="inputEmail">
					<?php echo lang('myaccount_contact_mobile');?>
				</label>
				<div class="controls">
					<?php echo form_input($contactMobile); ?>
					<?php echo form_error('contactMobile'); ?>
				</div>
			</div>
			<div class="control-group mb10">
				<label class="control-label" for="inputEmail">
					<?php echo lang('myaccount_contact_address');?>
					<font class="mandatory_cls">*</font>
				</label>
				<div class="controls">
					<?php echo form_input($contactAddress1); ?>
					<?php echo form_error('contactAddress1'); ?>
				</div>
			</div>
			<div class="control-group mb10">
				<div class="controls">
					<?php echo form_input($contactAddress2); ?>
				</div>
			</div>
			<div class="control-group mb10">
				<label class="control-label" for="inputEmail">
					<?php echo lang('myaccount_contact_suburb_city');?>
					<font class="mandatory_cls">*</font>
				</label>
				<div class="controls">
					<?php echo form_input($contactCity); ?>
					<?php echo form_error('contactCity'); ?>
				</div>
			</div>
			<div class="control-group mb10">
				<label class="control-label" for="inputEmail">
					<?php echo lang('myaccount_contact_state');?>
					<font class="mandatory_cls">*</font>
				</label>
				<div class="controls">
					<?php echo form_input($contactState); ?>
					<?php echo form_error('contactState'); ?>
				</div>
			</div>
			<div class="control-group mb10">
				<label class="control-label" for="inputEmail">
					<?php echo lang('myaccount_contact_postcode');?>
					<font class="mandatory_cls">*</font>
				</label>
				<div class="controls">
					<?php echo form_input($contactPostcode); ?>
					<?php echo form_error('contactPostcode'); ?>
				</div>
			</div>
			
			<div class="control-group mb10">
				<label for="inputEmail" class="control-label">
					<?php echo lang('myaccount_contact_country');?>
					<font class="mandatory_cls">*</font>
				</label>
				<div class="controls pr0">
					<div class="pull-left select-style register select_eventdetailP">
					
						<?php 
							$other = ' id="contactCountry" required =""';
							echo form_dropdown('contactCountry',$contactCountryList,$contactCountryValue,$other);
							echo form_error('contactCountry');
						?>
						
					</div>
				</div>
			</div>
			
			<div class="control-group mb10">
				<label class="control-label" for="inputEmail">
					<?php echo lang('myaccount_contact_website');?>
					<font class="mandatory_cls">*</font>
				</label>
				<div class="controls">
					<?php echo form_input($contactWebsite); ?>
					<?php echo form_error('contactWebsite'); ?>
				</div>
			</div>
		</div>
		<div class="row-fluid mt20">
			<?php 
				
				echo form_hidden('formActionName', 'contactDetails');
				
				//button show
				$formButton['showbutton']   = array('save','reset');
				$formButton['labletext'] 	= array('save'=>lang('comm_save_changes'),'reset'=>lang('comm_reset'));
				$formButton['buttonclass']  = array('save'=>'submitbtn_cus mr_16','reset'=>'submitbtn_cus reset_form');
				$this->load->view('event/common_save_reset_button',$formButton);
			?>
		</div>
	</div>
	
	<?php echo form_close(); ?>
</div>
<script type="text/javascript">
//call for data save
ajaxdatasave('formContactDetails','<?php echo $this->uri->uri_string(); ?>',false,true,false,false,true,false,false);
</script>
