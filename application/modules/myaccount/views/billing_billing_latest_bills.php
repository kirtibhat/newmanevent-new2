<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<div class="commonform_bg">
	<div class="headingbg bg_F89728 brd_botm_0px">
		Latest Bills
	</div>
	<div class="form-horizontal">
		<div class="control-group mb10 ">
			<div class="control-group mb10">
				<label class="control-label">Amount Due</label>
				<div class="controls text_info">
					<span>$ 0.00</span>
					<div>
						Account up to date!
					</div>
					<div class="row-fluid mt10 mb20">
						<button class="submitbtn_cus" type="button" role="button" aria-disabled="false"><span class="ui-button-text">Pay</span></button>
						<button class="submitbtn_cus" type="button" role="button" aria-disabled="false"><span class="ui-button-text">Bill Details<i class="icon-eye"></i></span></button>
					</div>
				</div>
			</div>
			<!--end of field wrapper-->
			<div class="control-group mb10">
				<label class="control-label">MAY 2014</label>
				<div class="controls text_info">
					<span>$ 50.00</span>
					<div class="fright ">
						<button class="visiblity submitbtn_cus" type="button" role="button" aria-disabled="false"><span class="ui-button-text">Pay</span></button>
						<button class="submitbtn_cus ui-button" type="button" role="button" aria-disabled="false"><span class="ui-button-text">Bill Details<i class="icon-eye"></i></span></button>
					</div>
				</div>
			</div>
			<!--end of field wrapper-->
			<div class="control-group mb10">
				<label class="control-label">APRIL 2014</label>
				<div class="controls text_info">
					<span>$ 50.00</span>
					<div class="fright ">
						<button class="visiblity submitbtn_cus" type="button" role="button" aria-disabled="false"><span class="ui-button-text">Pay</span></button>
						<button class="submitbtn_cus" type="button" role="button" aria-disabled="false"><span class="ui-button-text">Bill Details<i class="icon-eye"></i></span></button>
					</div>
				</div>
			</div>
			<!--end of field wrapper-->
			<div class="control-group mb10">
				<label class="control-label">MARCH 2014</label>
				<div class="controls text_info">
					<span>$ 50.00</span>
					<div class="fright ">
						<button class="visiblity submitbtn_cus " type="button" role="button" aria-disabled="false"><span class="ui-button-text">Pay</span></button>
						<button class="submitbtn_cus" type="button" role="button" aria-disabled="false"><span class="ui-button-text">Bill Details<i class="icon-eye"></i></span></button>
					</div>
				</div>
			</div>
			<!--end of field wrapper-->
		</div>
		<!--end of control group-->
	</div>
</div>
<!-- /commonform_bg  Latest Bills-->
