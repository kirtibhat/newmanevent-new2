<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//user details
$billFirstNameValue     	 = (!empty($userDetails->firstname))?$userDetails->firstname:'';
$billSurnameValue       	 = (!empty($userDetails->lastname))?$userDetails->lastname:'';
$billEmailValue         	 = (!empty($userDetails->email))?$userDetails->email:'';
$billfullName				 =  ucwords($billFirstNameValue.' '.$billSurnameValue);

//billing information
$applyUserInfoValue      	 = (!empty($billingDetails->apply_user_address))?$billingDetails->apply_user_address:'';
$billInfoFirstNameValue      = (!empty($billingDetails->firstname))?$billingDetails->firstname:'';
$billInfoSurnameValue        = (!empty($billingDetails->surname))?$billingDetails->surname:'';
$billInfoOrganisationValue   = (!empty($billingDetails->organisation))?$billingDetails->organisation:'';
$billInfoEmailValue        	 = (!empty($billingDetails->email))?$billingDetails->email:'';
$billInfoPhoneValue        	 = (!empty($billingDetails->phone))?$billingDetails->phone:'';
$billInfoFaxValue       	 = (!empty($billingDetails->fax))?$billingDetails->fax:'';
$billInfoMobileValue         = (!empty($billingDetails->mobile))?$billingDetails->mobile:'';
$billInfoAddress1Value       = (!empty($billingDetails->address1))?$billingDetails->address1:'';
$billInfoAddress2Value       = (!empty($billingDetails->address2))?$billingDetails->address2:'';
$billInfoCityValue        	 = (!empty($billingDetails->city))?$billingDetails->city:'';
$billInfoStateValue          = (!empty($billingDetails->state))?$billingDetails->state:'';
$billInfoPostcodeValue       = (!empty($billingDetails->postcode))?$billingDetails->postcode:'';
$billInfoCountryValue      	 = (!empty($billingDetails->country))?$billingDetails->country:'australia';


$formBillinfoDetails = array(
		'name'	 => 'formBillinfoDetails',
		'id'	 => 'formBillinfoDetails',
		'method' => 'post',
		'class'  => 'form-horizontal',
	);
	
$billInfoFirstName = array(
		'name'	=> 'billInfoFirstName',
		'value'	=> $billInfoFirstNameValue,
		'id'	=> 'firstname',
		'type'	=> 'text',
		'required'	=> '',
	);	

$billInfoSurname = array(
		'name'	=> 'billInfoSurname',
		'value'	=> $billInfoSurnameValue,
		'id'	=> 'lastname',
		'type'	=> 'text',
		'required'	=> '',
	);	
	
$billInfoOrganisation = array(
		'name'	=> 'billInfoOrganisation',
		'value'	=> $billInfoOrganisationValue,
		'id'	=> 'company_name',
		'type'	=> 'text',
		'required'	=> '',
	);	
	
$billInfoEmail = array(
		'name'	=> 'billInfoEmail',
		'value'	=> $billInfoEmailValue,
		'id'	=> 'email',
		'type'	=> 'text',
	);		
	
$billInfoPhone = array(
		'name'	=> 'billInfoPhone',
		'value'	=> $billInfoPhoneValue,
		'id'	=> 'phone',
		'type'	=> 'text',
		'required'	=> '',
	);

$billInfoFax = array(
		'name'	=> 'billInfoFax',
		'value'	=> $billInfoFaxValue,
		'id'	=> 'fax',
		'type'	=> 'text',
		'required'	=> '',
	);	

$billInfoMobile = array(
		'name'	=> 'billInfoMobile',
		'value'	=> $billInfoMobileValue,
		'id'	=> 'mobile',
		'type'	=> 'text',
		'required'	=> '',
	);	

$billInfoAddress1 = array(
		'name'	=> 'billInfoAddress1',
		'value'	=> $billInfoAddress1Value,
		'id'	=> 'address_line1',
		'type'	=> 'text',
		'required'	=> '',
	);	
	
$billInfoAddress2 = array(
		'name'	=> 'billInfoAddress2',
		'value'	=> $billInfoAddress2Value,
		'id'	=> 'address_line2',
		'type'	=> 'text',
	);		

$billInfoCity = array(
		'name'	=> 'billInfoCity',
		'value'	=> $billInfoCityValue,
		'id'	=> 'city',
		'type'	=> 'text',
		'required'	=> '',
	);	
	
$billInfoState = array(
		'name'	=> 'billInfoState',
		'value'	=> $billInfoStateValue,
		'id'	=> 'state',
		'type'	=> 'text',
		'required'	=> '',
	);	
		
$billInfoPostcode = array(
		'name'	=> 'billInfoPostcode',
		'value'	=> $billInfoPostcodeValue,
		'id'	=> 'postcode',
		'type'	=> 'text',
		'required'	=> '',
	);			
	
$billInfoCountryList  	  = array('australia'=>'Australia');
$appyUserAddressList  	  = array(''=>'Select User','applyUserInfo'=>$billfullName);
						
?>

<div class="commonform_bg">
	<div class="headingbg bg_F89728 brd_botm_0px">
		<?php echo lang('myaccount_bill_billing_info');?>
	</div>
	<?php echo form_open($this->uri->uri_string(),$formBillinfoDetails); ?>
		
		<div class="form-horizontal">
		<div class="control-group mb10">
			
			<div class="control-group mb10">
				<label for="inputEmail" class="control-label">
					<?php echo lang('myaccount_apply_user_address');?>
				</label>
				<div class="controls pr0">
					<div class="pull-left select-style register select_eventdetailP">
					
						<?php
							$other = 'id="billInfoApplyUserInfo" class="applyuserinfo"';
							echo form_dropdown('billInfoApplyUserInfo',$appyUserAddressList,$applyUserInfoValue,$other);
							echo form_error('billInfoApplyUserInfo');
						?>
						
					</div>
				</div>
			</div>
			
			<div class="control-group mb10">
				<label class="control-label" for="inputEmail">
					<?php echo lang('myaccount_bill_first_name');?>
					<font class="mandatory_cls">*</font>
				</label>
				<div class="controls">
					<?php echo form_input($billInfoFirstName); ?>
					<?php echo form_error('billInfoFirstName'); ?>
				</div>
			</div>
			<div class="control-group mb10">
				<label class="control-label" for="inputEmail">
					<?php echo lang('myaccount_bill_surname');?>
					<font class="mandatory_cls">*</font>
				</label>
				<div class="controls">
					<?php echo form_input($billInfoSurname); ?>
					<?php echo form_error('billInfoSurname'); ?>
				</div>
			</div>
			<div class="control-group mb10">
				<label class="control-label" for="inputEmail">
					<?php echo lang('myaccount_bill_organisation');?>
					<font class="mandatory_cls">*</font>
				</label>
				<div class="controls">
					<?php echo form_input($billInfoOrganisation); ?>
					<?php echo form_error('billInfoOrganisation'); ?>
				</div>
			</div>
			<div class="control-group mb10">
				<label class="control-label" for="inputEmail">
					<?php echo lang('myaccount_bill_email');?>
					<font class="mandatory_cls">*</font>
				</label>
				<div class="controls">
					<?php echo form_input($billInfoEmail); ?>
					<?php echo form_error('billInfoEmail'); ?>
				</div>
			</div>
			<div class="control-group mb10">
				<label class="control-label" for="inputEmail">
					<?php echo lang('myaccount_bill_phone');?>
					<font class="mandatory_cls">*</font>
				</label>
				<div class="controls">
					<?php echo form_input($billInfoPhone); ?>
					<?php echo form_error('billInfoPhone'); ?>
				</div>
			</div>
			<div class="control-group mb10">
				<label class="control-label" for="inputEmail">
					<?php echo lang('myaccount_bill_fax');?>
				</label>
				<div class="controls">
					<?php echo form_input($billInfoFax); ?>
					<?php echo form_error('billInfoFax'); ?>
				</div>
			</div>
			<div class="control-group mb10">
				<label class="control-label" for="inputEmail">
					<?php echo lang('myaccount_bill_mobile');?>
				</label>
				<div class="controls">
					<?php echo form_input($billInfoMobile); ?>
					<?php echo form_error('billInfoMobile'); ?>
				</div>
			</div>
			<div class="control-group mb10">
				<label class="control-label" for="inputEmail">
					<?php echo lang('myaccount_bill_address');?>
					<font class="mandatory_cls">*</font>
				</label>
				<div class="controls">
					<?php echo form_input($billInfoAddress1); ?>
					<?php echo form_error('billInfoAddress1'); ?>
				</div>
			</div>
			<div class="control-group mb10">
				<div class="controls">
					<?php echo form_input($billInfoAddress2); ?>
				</div>
			</div>
			<div class="control-group mb10">
				<label class="control-label" for="inputEmail">
					<?php echo lang('myaccount_bill_suburb_city');?>
					<font class="mandatory_cls">*</font>
				</label>
				<div class="controls">
					<?php echo form_input($billInfoCity); ?>
					<?php echo form_error('billInfoCity'); ?>
				</div>
			</div>
			<div class="control-group mb10">
				<label class="control-label" for="inputEmail">
					<?php echo lang('myaccount_bill_state');?>
					<font class="mandatory_cls">*</font>
				</label>
				<div class="controls">
					<?php echo form_input($billInfoState); ?>
					<?php echo form_error('billInfoState'); ?>
				</div>
			</div>
			<div class="control-group mb10">
				<label class="control-label" for="inputEmail">
					<?php echo lang('myaccount_bill_postcode');?>
					<font class="mandatory_cls">*</font>
				</label>
				<div class="controls">
					<?php echo form_input($billInfoPostcode); ?>
					<?php echo form_error('billInfoPostcode'); ?>
				</div>
			</div>
			
			<div class="control-group mb10">
				<label for="inputEmail" class="control-label">
					<?php echo lang('myaccount_bill_country');?>
					<font class="mandatory_cls">*</font>
				</label>
				<div class="controls pr0">
					<div class="pull-left select-style register select_eventdetailP">
					
						<?php 
							$other = ' id="billInfoCountry" required =""';
							echo form_dropdown('billInfoCountry',$billInfoCountryList,$billInfoCountryValue,$other);
							echo form_error('billInfoCountry');
						?>
						
					</div>
				</div>
			</div>
			
		</div>
		
		<div class="row-fluid mt20">
			<?php 
				
				echo form_hidden('formActionName', 'billInfoDetails');
				
				//button show
				$formButton['showbutton']   = array('save','reset');
				$formButton['labletext'] 	= array('save'=>lang('comm_save_changes'),'reset'=>lang('comm_reset'));
				$formButton['buttonclass']  = array('save'=>'submitbtn_cus mr_16','reset'=>'submitbtn_cus reset_form');
				$this->load->view('event/common_save_reset_button',$formButton);
			?>
		</div>
	</div>
	
	<?php echo form_close(); ?>
</div>
<script type="text/javascript">
//call for data save
ajaxdatasave('formBillinfoDetails','<?php echo $this->uri->uri_string(); ?>',false,true,false,false,true,false,false);

//apply user details 
$(".applyuserinfo").change(function(){
	
	//get selected value
	var getValue = $(this).val();
	
	//if value blank the reset form
	if(getValue==""){
		//reset form
		resetSelectedFrom('formBillinfoDetails');
	}else{
		var userdata = <?php echo json_encode($userDetails); ?>;
		$.each(userdata, function(index, val) {
			$("#"+index).val(val);
		});
	}	
});
</script>
