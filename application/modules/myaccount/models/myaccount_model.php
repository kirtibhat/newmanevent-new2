<?php
class Myaccount_model extends CI_model{


	private $tablenmevent					= 'event';
	private $tablenmeventcontactperson		= 'event_contact_person';
	private $tablenmeventinvoice			= 'event_invoice';
	private $tablenmeventbankdetail			= 'event_bank_detail';
	private $tablenmeventtermconditions		= 'event_term_conditions';
	private $tablenmuser					= 'user';
	
	
	function __construct(){
		parent::__construct();		
       
	}
	
	//-------------------------------------------------------------------------

	/*
	 * @description: This function is used to user contact details 
	 * @param: $userId
	 * @return: object
	 */ 
	
	
	public function userContactDetails($userId){	
		$this->db->select('*');
		$this->db->from($this->tablenmuser);
		$this->db->where('id',$userId);
		$query = $this->db->get();
		return $query->row();
	}
	
	//-------------------------------------------------------------------------
	
	/*
	 * @description: This function is used to get event details
	 * @param: $eventId
	 * @type: array
	 * @return: object
	 */ 
	
	
	public function geteventdetails($eventId,$userId){	
		$this->db->select('te.*,tncp.*,tnbe.*,tntc.*');
		$this->db->select('tnei.invoice_name,tnei.abn,tnei.address as iaddress,tnei.city as icity,tnei.state as istate,tnei.postcode as ipostcode,tnei.country as icountry,tnei.sent_to_email,tnei.sent_from_email,tnei.invoice_intro');
		$this->db->from($this->tablenmevent.' as te');
		$this->db->join($this->tablenmeventcontactperson.' as tncp','te.id =tncp.event_id','left');
		$this->db->join($this->tablenmeventinvoice.' as tnei','te.id =tnei.event_id','left');
		$this->db->join($this->tablenmeventbankdetail.' as tnbe','te.id =tnbe.event_id','left');
		$this->db->join($this->tablenmeventtermconditions.' as tntc','te.id =tntc.event_id','left');
		$this->db->where('te.id',$eventId);
		$this->db->where('te.user_id',$userId);
		$query = $this->db->get();
		//	echo $this->db->last_query();die();
		return $query->row();
	}
	
	
	/*
	 * @description: This function is used to get event payment option details
	 * @param: $eventId
	 * @type: array
	 * @return: object
	 */ 
	
	
	public function getpaymentdetails($eventId){	
		$this->db->select('*');
		$this->db->from('payment_registration_fee');
		$this->db->where('event_id',$eventId);
		$query = $this->db->get();
		//echo $this->db->last_query();die();
		return $query->row();
	}
	
	
}

?>
