<?php
class Events_model extends CI_model{


  private $tablenmevent                     = 'event';
  private $tablenmeventcategory             = 'event_categories';
  private $tablenmeventcustomcontact        = 'event_custom_contact';    
  private $tablenmeventregistrant           = 'event_registrants';
  private $tablenmeventcontactperson        = 'event_contact_person';
  private $tablenmeventcustomemail          = 'event_emails';
  private $tablenmeventcustomemailattach    = 'event_email_attachments';
  private $tablenmeventinvoice              = 'event_invoice';
  private $tablenmeventbankdetail           = 'event_bank_detail';
  private $tablenmeventtermconditions       = 'event_term_conditions';
  private $tablenmcity                      = 'city';
  private $tablenmcompanylist               = 'company_list';
  private $tablenmpaymentregistrationfee    = 'payment_registration_fee';
  private $tablenmpaymentgateway            = 'payment_gateway';
  private $tablefloorplantype               = 'corporate_floor_plan_type';
  private $tabledefaultpackagebenefit       = 'corporate_package_benefit';
  private $tabledefaultadditionalpurchaseitem = 'corporate_purchase_item';
  private $tablecorporatepackage            = 'corporate_package';
  private $tablecorporatepackagebooths      = 'corporate_package_booths';
  private $tablecustomformfield             = 'custom_form_fields';
  private $tableeventextracustomfield       = 'event_extras_custom_fields';
  private $tablecustomizeforms              = 'customize_forms';
  private $tablenmeventsocialmedia          = 'event_social_media_custom';
  private $tablenmeventdefaultthemes        = 'event_default_themes';
  private $tablenmeventthemes               = 'event_themes_data';
  private $font_packages                    = 'font_packages';
  
  
    
  function __construct(){
    parent::__construct();    
       
  }
  
  public function getDataFromTable($table='',$where='',$select){
      if( !empty($table) ) {
          if( !empty($select) ) {
            $this->db->select($select);
          }
          if( !empty($where) ) {
            $this->db->where($where);
          }
          $query = $this->db->get($table);
          #echo $this->db->last_query();die;
          return $query->result(); 
      }else {
          return false;
      }
  }
  
  public function geteventdetails($eventId,$userId=0){  
    $this->db->select('te.*,tncp.id as contact_persion_id,tncp.*,tntc.*');
    $this->db->from($this->tablenmevent.' as te');
    $this->db->join($this->tablenmeventcontactperson.' as tncp','te.id =tncp.event_id','left');
    $this->db->join($this->tablenmeventtermconditions.' as tntc','te.id =tntc.event_id','left');
    $this->db->where('te.id',$eventId);
    if($userId!=0){
      $this->db->where('te.user_id',$userId);
    } 
    $query = $this->db->get();
    return $query->row();
  }
  
  public function geteventthemesfonts()
  {
    $query = $this->db->get($this->font_packages);
    return $query->result();      
  }
  
  /*
	 * @description: This function is used updateDataFromTabel
	 * 
	 */
	 
	function updateDataFromTable($table='', $data=array(), $field='', $ID=0){
		$table=$table;
		if(empty($table) || !count($data)){
			return false;
		}
		else{
			if(is_array($field)){
				
				$this->db->where($field);
			}else{
				$this->db->where($field , $ID);
			}
			return $this->db->update($table , $data);
		}
	}
    
    public function geteventdetailswiththeme($eventId,$andwhere=array()){  
        $this->db->select('te.*,tncp.id as contact_persion_id,tncp.*,tntc.*,tethm.*');
        $this->db->from($this->tablenmevent.' as te');
        $this->db->join($this->tablenmeventcontactperson.' as tncp','te.id =tncp.event_id','left');
        $this->db->join($this->tablenmeventtermconditions.' as tntc','te.id =tntc.event_id','left');
        $this->db->join($this->tablenmeventthemes.' as tethm','te.id =tethm.event_id','left');
        $this->db->where('te.id',$eventId);
        if(!empty($andwhere)){
          $this->db->where($andwhere);
        } 
        $query = $this->db->get();
        return $query->row();
      }
  
  
}

?>
