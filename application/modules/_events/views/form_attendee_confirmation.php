<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php
$eventTitle =   (!empty($eventdetails->event_title)) ? $eventdetails->event_title : '';  
$startDate  =   date('l jS F Y',strtotime($eventdetails->starttime));
$endDate    =   date('l jS F Y',strtotime($eventdetails->endtime));
$eventVenue =   (!empty($eventdetails->event_venue)) ? $eventdetails->event_venue : ''; 
/* Define form fields */
$formAttendeeSetup = array(
'name'          => 'formAttendeeSetup',
'id'            => 'formAttendeeSetup',
'method'        => 'post',
'class'         => '',
'enctype'       => 'multipart/form-data'
);
?>

<?php $this->load->view('front_header'); ?>
<div class="invitation_content_inner_sec common_prop">
  <div class="col-9 responsive-pedng">
    <div class="panel" id="panel0">
      <div class="panel_header" style="<?php //echo $primarycolorbg; ?>">Confirmation</div>
      <div class="panel_contentWrapper" style="display:block;">
        <!--sub menu one-->
        <?php echo form_open(base_url().'events/modifyAttendee',$formAttendeeSetup); ?>
          <div class="panel_content responsive_content">
            <?php 
                foreach($bookerDetails as $booker) { 
                    echo $booker->field_value.'<br/>';
                }
            ?>
            <?php 
                $counter = 1;
                foreach($attendeeDetails as $attendee) {
                    echo "<br/><br/>Attendee - ".$counter." <br/><br/>"; 
                    foreach($attendee as $attendeevalue) { 
                        echo $attendeevalue->field_value.'<br/>';
                    }
                    $counter++;
                }
            ?>    
            
            
            <div class="pull-left">
                <div class="container">
                  <div class="invitation-right-footer">
                    <div class="ftr-inner">
                      <ul>
                        <a href="javascript:void(0)" onclick="publishOnFacebook();">
                          <li><span class="large_icon "> <i class="icon-facebook"></i> </span></li>
                        </a>
                        <a href="#" class="shareOnTwitter">
                          <li><span class="large_icon"> <i class="icon-twitter"></i> </span></li>
                        </a>
                        <a href="#" class="shareOnLinkdn">
                          <li><span class="large_icon"> <i class="icon-user"></i> </span></li>
                        </a>
                        
                      </ul>
                    </div>
                  </div>
                </div>
            </div>
            
          </div>
          <!--3rdpannel end--> 
          <!--4rd--> 
          <!--4rdpannel end--> 
          <!--5rd--> 
          <!--5rdpannel end-->
          <div class="panel_footer">
            <div class="pull-right">
                <input type="hidden" name="bookerId" value="<?php echo $bookerId; ?>">
                <input type="hidden" name="quantity" value="<?php echo $quantity; ?>">
                <input type="hidden" name="eventId"  value="<?php echo $eventId; ?>">
                <input type="submit" value="Modify Responce" class="btn-normal btn">
                <input type="submit" value="Done" class="btn-normal btn">
            </div>
          </div>
        <?php echo form_close();?>
      </div>
    </div>
  </div>
  <div class="col-6 venuedetail_rightPart">
    <p><?php echo lang('who_else_txt'); ?></p>
    
    <div class="col-fluid-15 mT10">
      <div class="col-fluid-7 responsive-sec">
        <div class="center_content"> <span><img src="<?php echo IMAGE; ?>user.jpg" ></span> <span class="lrg-font">Joshua Mcfadden<span class="sml-font">Google Inc.</span></span> </div>
      </div>
      <div class="col-fluid-7 responsive-sec">
        <div class="center_content"> <span><img src="<?php echo IMAGE; ?>user.jpg" ></span> <span class="lrg-font">Joshua Mcfadden<span class="sml-font">Google Inc.</span></span> </div>
      </div>
    </div>
    <div class="col-fluid-15 mT10">
      <div class="col-fluid-7 responsive-sec">
        <div class="center_content"> <span><img src="<?php echo IMAGE; ?>user.jpg" ></span> <span class="lrg-font">Joshua Mcfadden<span class="sml-font">Google Inc.</span></span> </div>
      </div>
      <div class="col-fluid-7 mT25 responsive-sec">
        <div class="center_content"> <span><a href="#">More..</a></span> </div>
      </div>
    </div>
  </div>
  <!-- Register Attendee -->
  <form action="<?php echo base_url(); ?>events/attendeeConfirmation" method="post" id="formAttendeeRegistration">
  <div class="col-15 footer_bg">
    
  </div>
  <div class="col-15">
    <div class="container">
      <div class="invitation_content_rgeisterbox common_prop">
        <button class="btn-normal btn pull-none rigisterbtnAction">
          <div class="lok">Register</div>
        </button>
        <div id="postAttendeeData">
            <input type="hidden" id="bookerQuantity" name="bookerQuantity" value="1">
            <input type="hidden" id="confirmbookerId" name="bookerId" value="0">
            <input type="hidden" name="event_id" value="<?php echo $eventId;  ?>" id="event_id">
        </div>
      </div>
    </div>
  </div>
  </form>
 <!-- End Registartion -->
</div>

<?php $this->load->view('front_footer'); ?>   

<!--
$eventTitle $startDate $endDate  $eventVenue
-->

<script>
function publishOnFacebook() {
    FB.ui({
            method: 'feed',
            name: '<?php echo $eventTitle; ?>',
            link: 'https://developers.facebook.com/docs/dialogs/',
            href: baseUrl,
            picture: 'http://newmanevents.com/themes/assets/images/dashboard_event.png',
            caption: 'NEWMAN : Simple Secure Surpricing',
            description: 'START DATE : <?php echo $startDate; ?> END DATE : <?php echo $endDate; ?>  VANUE : <?php echo $eventVenue; ?>  If your event is free to attend, then this system is free as well. You might have a wedding, a Board meeting or a party to organise and this system lets you send out invitations and to monitor responses.'
        });
}

(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.5&appId=837973882987543";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

$(document).ready(function(){
	$('.shareOnTwitter').click(function(event) {
    var width  = 575,
        height = 400,
        left   = ($(window).width()  - width)  / 2,
        top    = ($(window).height() - height) / 2,
        url    = 'http://twitter.com/home/?status=<?php echo $eventTitle; ?>  START DATE : <?php echo $startDate; ?> END DATE : <?php echo $endDate; ?>  VANUE : <?php echo $eventVenue; ?>  If your event is free to attend, then this system is free as well. You might have a wedding, a Board meeting or a party to organise and this system lets you send out invitations and to monitor responses.',
        //text	= '',
        opts   = 'status=1' +
                 ',width='  + width  +
                 ',height=' + height +
                 ',top='    + top    +
                 ',left='   + left;
    
    window.open(url, 'twitter', opts);
 
    return false;
  });
  
  	$('.shareOnLinkdn').click(function(event) {
    var width  = 575,
        height = 400,
        left   = ($(window).width()  - width)  / 2,
        top    = ($(window).height() - height) / 2,
        url    = 'http://www.linkedin.com/shareArticle?mini=true&submitted-image-url=http://newmanevents.com/themes/assets/images/dashboard_event.png&url=http://localhost/newmanevents&title=<?php echo $eventTitle; ?>&summary=START DATE : <?php echo $startDate; ?> END DATE : <?php echo $endDate; ?>  VANUE : <?php echo $eventVenue; ?>  If your event is free to attend, then this system is free as well. You might have a wedding, a Board meeting or a party to organise and this system lets you send out invitations and to monitor responses.&source=newmanevenrs.com',
        //text	= '',
        opts   = 'status=1' +
                 ',width='  + width  +
                 ',height=' + height +
                 ',top='    + top    +
                 ',left='   + left;
    
    window.open(url, 'twitter', opts);
 
    return false;
  });
  
});
</script>

