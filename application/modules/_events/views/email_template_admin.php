<?php
$contactLink='#';
$emailHtmlForContact = '';
$emailHtmlForContact .='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>Just one more step...</title>
<style>
            body {
                font-family: sans-serif;
                font-size: 18px;
                width: 600px;
                color: #5C6F7C;
                margin: 35px;
				
            }
            
            a {
                color: #FF8300;
                text-decoration:none;
            }
            
            p {
                margin: 0 0 10px 0;
            }
            
            .btn {
                background: #FF8300;
                color: white;
                border-radius: 8px;
                border-bottom-right-radius: 0px;
                font-weight: bold;
                padding: 10px 20px;
                text-decoration: none;
                display: block;
            }
        </style>
</head>

<body style="font-family: sans-serif;font-size: 18px;width: 600px;color: #5C6F7C;margin: 35px;">

<table style="background:#d1d2d7; width:600px; padding:30px;">
<thead>
<tr>
<td><img src="http://cdnsolutionsgroup.com/newmanvenues/themes/emailImages/A_EM4-1.png" style="padding-bottom: 35px;"/></td>
</tr>
</thead>

<tr>
<td colspan="3" style="text-align:center;">
    <p style="color:#131e29; font-size:24px;">Contact Us<br>
        Topic : '.$contact_topic.'   
    </p>
</td>
</tr>

<tr style="background:#ecf0f1;">
<td style="padding:20px;" colspan="1">
	<img src="http://cdnsolutionsgroup.com/newmanvenues/themes/emailImages/user.png" alt="" style="float:left;"/>
    <br/>
    <p style="margin-left:20px;float: left;padding-left: 20px;width: 400px;margin: 10px 0px; color:#131e29;">Contact Us Details</p>
    <br/><br/><br/><br/>
    <p> Title : '.$title.'</p>
    <p> First Name : '.$firstName.'</p>
    <p> Last Name : '.$lastName.'</p>
    <p> Email : '.$contactEmail.'</p>
    <p> Phone : '.$contactPhone.'</p>
    <p> Organisation : '.$contactOrganisation.'</p>
    <p> Message : '.$contactMessage.'</p>
<br>
<br>
</td>
</tr>
<tr>
<td style="clear: both;margin: 50px auto 0px;display: table;">
<a href="#"><img src="http://cdnsolutionsgroup.com/newmanvenues/themes/emailImages/icpon.png" width="22" height="39"/></a>&nbsp;
<a href="#"><img src="http://cdnsolutionsgroup.com/newmanvenues/themes/emailImages/social_icop.png" width="98" height="29"/></a>   
</td>
</tr>
<td colspan="3" style=" text-align:center;">
  <a href="" style="font-size:12px; color:#131e29; text-decoration:none;">Term | </a>
  <a href="'.$contactLink.'" style="font-size:12px; color:#131e29; text-decoration:none;">Conatct us | </a>
  <a href="" style="font-size:12px; color:#131e29; text-decoration:none;">Unsubcribe</a>
 </td> 
</table>
</body>
</html>';

echo $emailHtmlForContact;
?>

