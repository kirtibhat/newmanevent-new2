<!-- Section - 1 -->
<?php $counter = 1; ?>
<?php for( $quantityCounter = 1 ; $quantityCounter <= $quantity ; $quantityCounter++ ) { ?>
<?php foreach($invitationTypesFormFieldsData as $listField) { if($listField->field_status!=3) { ?>        
<div class="row">
  <div class="col-3">
    <div class="labelDiv">
      <label class="form-label text-right"><?php echo ucwords($listField->field_name); ?></label>
    </div>
  </div>
  <div class="col-5">
      <?php if($quantityCounter==1) { echo form_hidden('fieldid[]', $listField->id); } ?>
      <?php if($listField->field_type=='selectbox') { ?>
        <span class="select-wrapper">
            <div class="customSelectWrapper" style="width: auto;">
                <select name="field_<?php echo $listField->id; ?>[]" id="field_<?php echo $listField->id; ?>" class="custom-select medium_select hasCustomSelect" style="-webkit-appearance: menulist-button; width: 175px; position: absolute; opacity: 0; height: 28px; font-size: 15px;" onchange="getTitleId(this.value,'<?php echo $listField->id.$quantityCounter; ?>');">
                <option value="" selected="selected"></option>
            <?php if( ( empty($listField->default_value) || ($listField->default_value == 'NULL') ) &&  strtolower($listField->field_name) == 'title' ) { ?>
                    <option value="Miss">Miss</option>
                    <option value="Ms">Ms</option>
                    <option value="Mrs">Mrs</option>
                    <option value="Mr">Mr</option>
            <?php }else {  
                    $optionsArr = json_decode($listField->default_value); 
                    if($optionsArr) {
                        foreach($optionsArr as $option) {
                ?>
                        <option value="<?php echo $option; ?>"><?php echo $option; ?></option>
                <?php   }
                    } 
                } ?>
                        
        </select>
            </div>
            <div class="customSelectWrapper" style="width: auto;">
                <span class="customSelect custom-select medium_select" style="display: inline-block;">
                    <span class="customSelectInner setTitle_<?php echo $listField->id.$quantityCounter; ?>" style="width: 125px; display: inline-block;">Select</span>
                </span>
            </div>
        </span>
        
        
        
      <?php }else if( $listField->field_type=='text' ) { 
                        $fieldtext='';
                        if($listField->field_name=='first name') { echo '<input type="hidden" name="attendee_fname" value="'.$listField->id.'">'; }
                        if($listField->field_name=='last name') { echo '<input type="hidden" name="attendee_lname" value="'.$listField->id.'">'; }
                        if($listField->field_name=='email') { echo '<input type="hidden" name="attendee_email" value="'.$listField->id.'">'; }
                            
                    ?>
                        <input type="text" name="field_<?php echo $listField->id; ?>[]" class="medium_input">
                  <?php  
            }else if( $listField->field_type=='radio' ) { 
                        $radioOptionsArr = json_decode($listField->default_value);
                         
                        foreach($radioOptionsArr as $radio) {
                        ?>
                    <div class="radioDiv">
                            <input type="radio" name="field_<?php echo $listField->id; ?>_<?php echo $quantityCounter; ?>[]" value="<?php echo $radio; ?>" id="invitation_types_radio_field_<?php echo $counter; ?>" class="radioButton">               
                            <label class="form-label pull-left" for="invitation_types_radio_field_<?php echo $counter; ?>"><span><?php echo $radio; ?></span></label>
                    </div>
                    
                <?php $counter++; } 
                
            }else if($listField->field_type=='file') { ?>
              <div class="custom-upload" style="width:52%;">
                <span class="label_text">Select or Drop File</span>
                <input type="file" class="customIinputFileLogo" id="bookerimage" accept="image/*" name="attendeeimage"  draggable="true">
                <div class="display_file">
                    <span class="file_value"></span>
                    <a href="javascript:void(0)" class="clear_value"></a>
                    <input type="hidden" name="attendee_image_<?php echo $listField->id; ?>[]" id="bookerFileValue" value="">
                </div>
            </div>
              
           <?php } ?>                          
    <span id="error-dinner_dance6"></span> </div>
</div> 
<?php } ?>
<?php } ?>
<?php } ?>  


                        

