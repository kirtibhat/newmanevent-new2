<div class="invitation-footer-mainarea common_prop">
  <div class="col-10">
    <div class="container">
      <div class="invitation-footer">
        <div class="ftr-inner common_prop"><span class="powerd-text">powered by</span><span><img src="<?php echo IMAGE; ?>ftr.png"></span></div>
      </div>
    </div>
  </div>
  <div class="col-5">
    <div class="container">
      <div class="invitation-right-footer">
        <div class="ftr-inner">
          <ul>
            <a href="#">
              <li><span class="large_icon "> <i class="icon-facebook"></i> </span></li>
            </a>
            <a href="#">
              <li><span class="large_icon"> <i class="icon-twitter"></i> </span></i></li>
            </a>
            <a href="#">
              <li><span class="large_icon"> <i class="icon-user"></i> </span></li>
            </a>
            <a href="#">
              <li><span class="large_icon"> <i class="icon-youtube"></i> </span></i></li>
            </a>
            <a href="#">
              <li><span class="large_icon"> <i class="icon-3-3"></i> </span></li>
            </a>
            <a href="#">
              <li><span class="large_icon"> <i class="icon-pidgin"></i> </span></i></li>
            </a>
            <a href="#">
              <li><span class="large_icon"> <i class="icon-camera"></i> </span></i></li>
            </a>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade terms_popup" style="display:none;">
	<div class="modal-dialog modal-sm-as">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Terms & Condition</h4>
			</div>
			
			<div class="infobar">
				<p class="info_Status"></p>
				<a class="info_btn" onclick="return false;">
				</a>
			</div>
			<div class="modal-body">
				<div class="modal-body-content">
				<div class="clearFix display_inline w_100 mT10"></div>
					<div class="row">	
					<?php echo lang('term_condition_text');?>
					</div>
				</div>
				<div class="modal-footer">
					<div class="pull-right">
						<button class="btn-normal btn accept_event" tabindex='4' data-dismiss="modal">Accept</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<script>
$('document').ready(function(){
    $('.mainNavWrapper').css('display','none');
});
</script>

