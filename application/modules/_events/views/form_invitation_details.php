<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

//echo '<pre>';
//print_r($eventdetails);
$secondary_color        = (!empty($eventdetails->secondary_color)) ? $eventdetails->secondary_color : '';
$primary_color          = (!empty($eventdetails->primary_color)) ? $eventdetails->primary_color : '';
$event_theme_screen_bg  = (!empty($eventdetails->event_theme_screen_bg)) ? $eventdetails->event_theme_screen_bg : '';

/* Set theme colors */
$primarycolor = 'color:#'.$primary_color;
$primarycolorbg = 'background:#'.$primary_color;

$secondarycolor = 'color:#'.$secondary_color;
$secondarycolorbg = 'background:#'.$secondary_color;

$eventthemescreencolor = 'color:#'.$event_theme_screen_bg;
$eventthemescreenbg = 'background:#'.$event_theme_screen_bg;

$userDetails    = $userDetails[0];
$firstname      = (!empty($userDetails->firstname))? $userDetails->firstname :'';
$lastname       = (!empty($userDetails->lastname))? $userDetails->lastname :'';
$email          = (!empty($userDetails->email))? $userDetails->email :'';
$company_name   = (!empty($userDetails->company_name))? $userDetails->company_name :'';
/*set user details data*/
$eventId        = (!empty($eventId))? $eventId :'';
$dietaryReq     = (!empty($dietaryReq))? $dietaryReq :'';
/* get param from url */
$paramTypeId    = $this->input->get('q');
$paramQty       = $this->input->get('qty');
$ispass         = $this->input->get('ispass');

$password_access = $this->session->userdata('password_access');

$passRequiredStatus = ($ispass=='true' && $password_access!=1) ? 'display:none;' : '';

/* Define form fields */
$formInvitationDetailsSetup = array(
'name'          => 'formInvitationDetailsSetup',
'id'            => 'formInvitationDetailsSetup',
'method'        => 'post',
'class'         => '',
'enctype'       => 'multipart/form-data'
);

?>

<?php $this->load->view('front_header'); ?>
<div class="invitation_content_inner_sec common_prop">
  <div class="col-9 responsive-pedng">
    <div class="panel" id="panel0">
      <div class="panel_header" style="<?php echo $primarycolorbg; ?>"><?php if($paramQty==0 && $paramQty!='') { echo lang('waitlist_txt');  }else { echo lang('invitation_txt'); } ?></div>
      <div class="panel_contentWrapper" style="display:block;">
        <!--sub menu one-->
        <?php echo form_open(base_url().'events/saveInvitationFormDetails',$formInvitationDetailsSetup); ?>
          <div class="panel_content responsive_content">
            
            <div class="row">
              <div class="col-3">
                <div class="labelDiv">
                  <label class="form-label text-right"><?php echo lang('invitation').' '.lang('type'); ?></label>
                </div>
              </div>
              <div class="col-5">
                <select class="custom-select medium_select isAllowwaitlist" id="invitation_types" name="invitation_types">
                    <option value="">Select</option>
                    <?php foreach($eventInvitations as $inv_value) { ?>
                            <option <?php if($paramTypeId==$inv_value->id) { echo 'selected'; } ?> value="<?php echo $inv_value->id; ?>" quantity="<?php echo $inv_value->limit_per_invitation; ?>"><?php echo $inv_value->invitation_name; ?>
                            </option>
                    <?php } ?>    
                </select>
                <span id="error-dinner_dance6"></span> </div>
            </div>
            <div class="row">
              <div class="col-3">
                <div class="labelDiv">
                  <label class="form-label text-right"><?php echo lang('quantity'); ?></label>
                </div>
              </div>
              <div class="col-5">
                <div class="stepper ">
                    <input id="invitation_quantity"  class="small_input dark stepper-input pull-left" type="number" readonly  required="" value="0" name="field_qty">
                    <span class="small_icon stepper-step "> 
                    <i class="icon-uparrow step-personal up"></i>
                    <i class="icon-downarrow step-personal down"></i>
                    </span>
                 </div>
                <span id="error-dinner_dance6"></span> 
              </div>
            </div>
            
          </div>
          
          <div class="sub_banner" style="<?php echo $secondarycolorbg; ?>">
            <h1><?php echo lang('booker_txt'); ?></h1>
          </div>
          <div class="infobar">
            <?php echo lang('front_booker_infobar_txt'); ?> 
          </div>
          <div class="panel_content responsive_content">
            <!-- Section - 2 Booker Details -->
            <?php if($bookerformfielddata) { ?>
            <?php foreach($bookerformfielddata as $listField) { if($listField->field_status!=3) { ?>    
            <div class="row">
              <div class="col-3">
                <div class="labelDiv">
                  <label class="form-label text-right"><?php echo ucwords($listField->field_name); ?></label>
                </div>
              </div>
              <div class="col-5">
                  <?php echo form_hidden('fieldid[]', $listField->id);  ?>
                  <?php if($listField->field_type=='selectbox') { ?>
                    <select class="custom-select medium_select" id="booker_title" name="booker_<?php echo $listField->id; ?>[]">
                    <option value="" selected="selected"></option>
                    <?php if( ( empty($listField->default_value) || ($listField->default_value == 'NULL') ) &&  strtolower($listField->field_name) == 'title' ) { ?>
                            <option value="Miss">Miss</option>
                            <option value="Ms">Ms</option>
                            <option value="Mrs">Mrs</option>
                            <option value="Mr">Mr</option>
                    <?php }else {  
                            $optionsArr = json_decode($listField->default_value); 
                            if($optionsArr) {
                                foreach($optionsArr as $option) {
                        ?>
                                <option value="<?php echo $option; ?>"><?php echo $option; ?></option>
                        <?php   }
                            } 
                        } ?>
                                    
                    </select>
                  <?php }else if( $listField->field_type=='text' ) { 
                        $value ='';
                        if($listField->field_name=='first name') { 
                            $value = $firstname;
                            echo '<input type="hidden" name="booker_fname" value="'.$listField->id.'">'; }
                        if($listField->field_name=='last name') { 
                            $value = $lastname;
                            echo '<input type="hidden" name="booker_lname" value="'.$listField->id.'">'; }
                        if($listField->field_name=='email') { 
                            $value = $email;
                            echo '<input type="hidden" name="booker_email" value="'.$listField->id.'">'; }
                        ?>
                            <input type="text" name="booker_<?php echo $listField->id; ?>[]" class="medium_input" value="<?php echo $value; ?>">
                      <?php  
                        }else if( $listField->field_type=='radio' ) { 
                            $radioOptionsArr = json_decode($listField->default_value);
                            $opn = 1; 
                            foreach($radioOptionsArr as $radio) {
                            ?>
                                <div class="radioDiv">
                                        <input type="radio" name="booker_<?php echo $listField->id; ?>[]" value="<?php echo $radio; ?>" id="booker_radio_field<?php echo $opn; ?>" class="radioButton">        
                                        <label class="form-label pull-left" for="booker_radio_field<?php echo $opn; ?>"><span><?php echo $radio; ?></span></label>
                                </div>
                        
                    <?php  $opn++; } 
                            
                        }else if($listField->field_type=='file') { ?>
                          <div class="custom-upload" style="width:52%;">
                                <span class="label_text">Select or Drop File</span>
                                <input type="file" class="customIinputFileLogo" id="bookerimage" accept="image/*" name="bookerimage"  draggable="true">
                                
                                
                                <div class="display_file">
                                    <span class="file_value"></span>
                                    <a href="javascript:void(0)" class="clear_value"></a>
                                    <input type="hidden" name="booker_image_<?php echo $listField->id; ?>[]" id="bookerFileValue" value="">
                                </div>
                            </div>
                       <?php } ?>                          
                <span id="error-dinner_dance6"></span> </div>
            </div> 
            <?php } ?>
            <?php } ?>
            <?php } ?>  
            <!-- End booker details -->
            
          </div>
          
          <div class="sub_banner" style="<?php echo $secondarycolorbg; ?>">
            <h1><?php echo lang('attendee_txt'); ?></h1>
          </div>
          <div class="infobar">
            <?php echo lang('front_attendee_infobar_txt'); ?>
          </div>
          <div class="panel_content responsive_content">
            <div class="row set_invitations">
                           
            </div>
          </div>
          <!--3rdpannel end--> 
          <!--4rd--> 
          <!--4rdpannel end--> 
          <!--5rd--> 
          <!--5rdpannel end-->
          <div class="panel_footer">
            <a href="javascript:void();" class="scrollTopTrg" style=""><img alt="Logo image" src="<?php echo IMAGE; ?>scroll_top.png">Top</a>
            <div class="pull-right">
              <button name="form_reset" type="button" class="btn-normal btn reset_space reset_form">Clear</button>
              <input type="submit" value="Save" class="btn-normal btn submitAction" style="<?php echo $passRequiredStatus; ?>">
              <div id="setHiddenField"></div>
              <input type="hidden" name="eventId" value="<?php echo $eventId;  ?>" id="eventsid">
            </div>
          </div>
        <?php echo form_close();?>
      </div>
    </div>
  </div>
  <div class="col-6 venuedetail_rightPart">
    <p><?php echo lang('who_else_txt'); ?></p>
    <div class="col-fluid-15 mT10">
      <div class="col-fluid-7 responsive-sec">
        <div class="center_content"> <span><img src="<?php echo IMAGE; ?>user.jpg" ></span> <span class="lrg-font">Joshua Mcfadden<span class="sml-font">Google Inc.</span></span> </div>
      </div>
      <div class="col-fluid-7 responsive-sec">
        <div class="center_content"> <span><img src="<?php echo IMAGE; ?>user.jpg" ></span> <span class="lrg-font">Joshua Mcfadden<span class="sml-font">Google Inc.</span></span> </div>
      </div>
    </div>
    <div class="col-fluid-15 mT10">
      <div class="col-fluid-7 responsive-sec">
        <div class="center_content"> <span><img src="<?php echo IMAGE; ?>user.jpg" ></span> <span class="lrg-font">Joshua Mcfadden<span class="sml-font">Google Inc.</span></span> </div>
      </div>
      <div class="col-fluid-7 responsive-sec">
        <div class="center_content"> <span><img src="<?php echo IMAGE; ?>user.jpg" ></span> <span class="lrg-font">Joshua Mcfadden<span class="sml-font">Google Inc.</span></span> </div>
      </div>
    </div>
    <div class="col-fluid-15 mT10">
      <div class="col-fluid-7 responsive-sec">
        <div class="center_content"> <span><img src="<?php echo IMAGE; ?>user.jpg" ></span> <span class="lrg-font">Joshua Mcfadden<span class="sml-font">Google Inc.</span></span> </div>
      </div>
      <div class="col-fluid-7 responsive-sec">
        <div class="center_content"> <span><img src="<?php echo IMAGE; ?>user.jpg" ></span> <span class="lrg-font">Joshua Mcfadden<span class="sml-font">Google Inc.</span></span> </div>
      </div>
    </div>
    <div class="col-fluid-15 mT10">
      <div class="col-fluid-7 responsive-sec">
        <div class="center_content"> <span><img src="<?php echo IMAGE; ?>user.jpg" ></span> <span class="lrg-font">Joshua Mcfadden<span class="sml-font">Google Inc.</span></span> </div>
      </div>
      <div class="col-fluid-7 responsive-sec">
        <div class="center_content"> <span><img src="<?php echo IMAGE; ?>user.jpg" ></span> <span class="lrg-font">Joshua Mcfadden<span class="sml-font">Google Inc.</span></span> </div>
      </div>
    </div>
    <div class="col-fluid-15 mT10">
      <div class="col-fluid-7 responsive-sec">
        <div class="center_content"> <span><img src="<?php echo IMAGE; ?>user.jpg" ></span> <span class="lrg-font">Joshua Mcfadden<span class="sml-font">Google Inc.</span></span> </div>
      </div>
      <div class="col-fluid-7 responsive-sec">
        <div class="center_content"> <span><img src="<?php echo IMAGE; ?>user.jpg" ></span> <span class="lrg-font">Joshua Mcfadden<span class="sml-font">Google Inc.</span></span> </div>
      </div>
    </div>
    <div class="col-fluid-15 mT10">
      <div class="col-fluid-7 responsive-sec">
        <div class="center_content"> <span><img src="<?php echo IMAGE; ?>user.jpg" ></span> <span class="lrg-font">Joshua Mcfadden<span class="sml-font">Google Inc.</span></span> </div>
      </div>
      <div class="col-fluid-7 mT25 responsive-sec">
        <div class="center_content"> <span><a href="#">More..</a></span> </div>
      </div>
    </div>
  </div>
  <!-- Register Attendee -->
  <form action="<?php echo base_url(); ?>events/attendeeConfirmation" method="post" id="formAttendeeRegistration">
  <div class="col-15 footer_bg">
    <div class="container">
      <div class="invitation_content_chkbox">
        <div class="chkbox-inner-sec mT20">
          <input id="terms_checkbox_invitation" name="terms_checkbox_invitation" class="checkBox" type="checkbox">
          <label class="form-label" for="terms_checkbox_invitation"></label>
          <label class="form-label terms" for="">
         <p class="para mL30">I have read and agree with the<br>
         <span class="link_terms" data-toggle="modal" data-target=".terms_popup">Terms &amp; Conditions &nbsp; </span> for my <br>
            Newman account.
         <span id="error-email" class="w_100 set_error dn" style="display: none;"><label id="email-error" class="error" for="email">Please select terms &amp; conditions.</label></span>   
         </p>
        
         </label> 
        </div>
      </div>
    </div>
  </div>
  <div class="col-15">
    <div class="container">
      <div class="invitation_content_rgeisterbox common_prop">
        <button class="btn-normal btn pull-none rigisterbtnAction">
          <div class="lok">Register</div>
        </button>
        <div id="postAttendeeData">
            <input type="hidden" id="bookerQuantity" name="bookerQuantity" value="1">
            <input type="hidden" id="confirmbookerId" name="bookerId" value="0">
            <input type="hidden" name="event_id" value="<?php echo $eventId;  ?>" id="event_id">
        </div>
      </div>
    </div>
  </div>
  </form>
 <!-- End Registartion -->
</div>

<?php $this->load->view('front_footer'); ?>   
<script>
$('.authentication_event').click(function(){
var invitationTypeId    = $('#invitationTypeId').val();
var invitationEventId   = $('#invitationEventId').val();
var passwordRequired   = $('#passwordRequired').val();
    $.ajax({
        type:"post",
        dataType:"html",
        url: baseUrl+'events/checkPassAuthentication',
        data: "eventId="+invitationEventId+"&invitation_types_id="+invitationTypeId+"&password="+passwordRequired,
            
        success: function(response)
        {
            console.log(response);
            if(response=='true') { 
                window.location.href = window.location.href;
            }else {
               $('#passRequired').html('Password not matched');
            } 
        },
        error: function()
        {						
            alert('Error while request..'); 
        }
    }); 
});        
    
$(document).ready(function(){
    var isPassTrue = '<?php echo $ispass; ?>';
    var password_access = '<?php echo $password_access; ?>';
    if(isPassTrue=='true' && password_access!='1') {
        $('.openPassRequiredModel').trigger('click');
    }
});
    
$('.submitAction').click(function(){
    $('#bookerQuantity').val($('#invitation_quantity').val());
});
$('.rigisterbtnAction').click(function(){
    var confirmbookerId = $('#confirmbookerId').val();
    if(confirmbookerId==0) {
        alert('You must save attendee first before attendee registration');
        return false;
    }
    if($("#terms_checkbox_invitation").is(':checked')) {
        $('#formAttendeeRegistration').submit();
    }else {
        $('.link_terms').trigger('click');
    }
});

$('.accept_event').click(function(){ 
    $('#terms_checkbox_invitation').attr('checked',true);
});

function getTitleId(data,dataid){ $('.setTitle_'+dataid).html(data); }
var dietaryReqArr = '<?php echo $dietaryReq; ?>';
dietaryReqArr = jQuery.parseJSON(dietaryReqArr);

$('.isAllowwaitlist').change(function(){
var invitation_types_id = $(this).val();
var eventId = '<?php echo $eventId; ?>';
    $.ajax({
        type:"post",
        dataType:"html",
        url: baseUrl+'events/isAllowWaitList',
        data: "eventId="+eventId+"&invitation_types_id="+invitation_types_id,
            
        success: function(response)
        {
            //console.log(response);
            if(response=='allow_waitlist') { 
                $('.submitAction').show();
                $(".panel_header").html('Waitlist Details');
                $(".circleMenueAction").html('Waitlist Details');
                $("#setHiddenField").html('<input type="hidden" name="isWaitList" value="1">');
            }else if(response=='false') {
                alert('You can not add attendee for this invitation because ticket quantity no more');
                $('.submitAction').hide();
                    return false;
                } else {
                $('.submitAction').show();
                $(".panel_header").html('Invitation Details');
                $(".circleMenueAction").html('Invitation Details');
                $("#setHiddenField").html('<input type="hidden" name="isWaitList" value="0">');
            } 
        },
        error: function()
        {						
            alert('Error while request..'); 
        }
    }); 
});             

ajaxdatasave('formInvitationDetailsSetup', 'events/saveInvitationFormDetails', false, true, false, false, true, '', '');

</script>
