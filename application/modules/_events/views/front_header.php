<?php 
$secondary_color        = (!empty($eventdetails->secondary_color)) ? $eventdetails->secondary_color : '';
$primary_color          = (!empty($eventdetails->primary_color)) ? $eventdetails->primary_color : '';
$event_theme_screen_bg  = (!empty($eventdetails->event_theme_screen_bg)) ? $eventdetails->event_theme_screen_bg : '';

/* Set theme colors */
$primarycolor = 'color:#'.$primary_color;
$primarycolorbg = 'background:#'.$primary_color;

$secondarycolor = 'color:#'.$secondary_color;
$secondarycolorbg = 'background:#'.$secondary_color;

$eventthemescreencolor = 'color:#'.$event_theme_screen_bg;
$eventthemescreenbg = 'background:#'.$event_theme_screen_bg;

?>
<div class="page_content invitation_content graylight_color">
<!-- dashboard main--> 
<div class="page_content invitation_content">
  <div class="container">
    <div class="invitation-logo-hdngsec common_prop" style="<?php echo $primarycolorbg; ?>">
      <div class="col-3"> <img src="<?php echo IMAGE; ?>logo.png"> </div>
      <div class="col-12">
        <h1>Beyond the Traditional</h1>
        <h6>CONVENTION 2015(Sub Heading)</h6>
        <p>ANZ STADIUM SYDNEY</p>
        <p>23-25 MARCH 2015</p>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="contentNavigation manager_setup">
      <ul>
        <li  > <a href="#"> <span class="xlarge_icon" style="<?php echo $primarycolorbg; ?>"> <i class="icon-user"></i>
        </span><span class="text circleMenueAction" style="<?php echo $primarycolor; ?>">Invitation Details</span> </a> </li>
        <li class="" > <a href="#"> <span class="xlarge_icon"> <i class="icon-thumb"></i></span><span class="text">Conformation</span> </a> </li>
      </ul>
    </div>
  </div>
  <div class="container"> 

