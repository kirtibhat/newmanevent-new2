<?php
/**
 * By this class you can manage event setup section
 * 
 * @author  shailendra tiwari
 * @email   shailendratiwari@cdnsol.com
 * @Hint    In this class all public method name shoule be normal case 
 * and private metho name shoule be camel case 
 * 
 */

class Events extends MX_Controller{
  /**
   * Constructor - Sets Event Class require helper and library other.
   * The constructor can be passed an array of config values
   */
    
    private $tableattendee          = 'invitation_attendee_details';
    private $tablebooker            = 'invitation_booker_details';
    private $tableinvitation        = 'event_invitations';
    private $tablecustomfields      = 'custom_form_fields';
    private $tableinvitationmaster  = 'invitation_master_details';
    private $tableinvitee           = 'event_invitee';
    
    function __construct(){
    parent::__construct();
    
    $this->load->library(array('head','template','process_upload'));
    $this->load->library(array('home_template','head'));
    $this->load->model(array('events_model'));
    $this->load->model(array('common_model'));
    $this->load->language(array('event'));
    $this->load->language(array('home'));
    $this->load->language(array('delegate'));
    $this->form_validation->set_error_delimiters('<div class="server_msg_show">', '</div>');
    }
    
    /*
    * @description: This function is used to show event home front page
    * @return     : @void
    */ 

    public function invitation(){
        /* Set gplus & FB url */
        $this->session->set_userdata('referrer_url', '1');
        /*
         * FB login url
         */ 
        $this->load->library('facebook', array(
            'appId'     => FB_APP_ID,
            'secret'    => FB_SECRET,  
        )); 
            
        $userSession = $this->session->userdata('user_data');
        if($userSession == ''){
            $data['login_url']  = $this->facebook->getLoginUrl(array(
                'display'       => 'popup',
                'redirect_uri'  => site_url('home/freeRegistration'), 
                'scope'         => array('email') 
                ));
        }
        /*
         * configration gplus
         */
        $this->load->library('google');  
        $client = new Google_Client();
        $client->setClientId(CLIENT_ID_GPLUS);
        $client->setClientSecret(CLIENT_SECRET_GPLUS);
        $client->setRedirectUri(REDIRECT_URI_GPLUS);
        $client->addScope("email");
        $client->addScope("profile");
        $service = new Google_Service_Oauth2($client);
        
        if ($this->input->get('code')) {
            $client->authenticate($this->input->get('code'));
            $this->session->set_userdata('access_token',$client->getAccessToken());
            header('Location: ' . filter_var(REDIRECT_URI_GPLUS, FILTER_SANITIZE_URL));
            exit;
        }

        if ($this->session->userdata('access_token')) {
            $client->setAccessToken($this->session->userdata('access_token'));
        } else {
            $authUrl = $client->createAuthUrl();
        }
        
        if (isset($authUrl)){ 
            $data['authUrlGplus'] = $authUrl;
        }
        
        /* End social media configration */
        
        $urlcode        = $this->uri->segment(3);
        $where          = array('event_url' => $urlcode);
        $event_details  = $this->events_model->getDataFromTable('event',$where,'id');
        if( count($event_details) > 0 && !empty($event_details) ) {
            $event_details      = $event_details[0];
            $eventId            = $event_details->id;
            if($eventId!=0) {
                $data['eventId'] = $eventId;
                /* get event details from event table */
                $data['eventdetails']       = $this->events_model->geteventdetails($eventId,$userId=0);
                /* get event theme fonts list */
                $data['fontsList']          = $this->events_model->geteventthemesfonts();
                $whereinvitation            = array( 'event_id'=> $eventId );
                $data['eventinvitations']   = $this->common_model->getDataFromTabel($this->tableinvitation,'*',$whereinvitation,'','invitations_order','ASC');
                //echo '<pre>';
                //print_r($data['eventinvitations']);die;
                
                $data['eventinvitationstypes'] = $data['eventinvitations'];
                $whereSocialMedia           = array('event_id' => $eventId);
                $data['eventSocialMedia']   = $this->common_model->getDataFromTabel('event_social_media','*',$whereSocialMedia);
                $data['eventSocialMediaCustom']   = $this->common_model->getDataFromTabel('event_social_media_custom','*',$whereSocialMedia);
                /* get event theme data from event_theme table */
                $whereEvent                 = array('is_theme_used' => '1' ,'event_id' => $eventId );
                $data['usedThemesDetails']  = $this->common_model->getDataFromTabel('event_themes_data', '*', $whereEvent);
                $this->home_template->load('home_template','form_front_home',$data,TRUE);
            }else {
                redirect('/');
            }
        }else {
            redirect('/');
        }
    } 
    
    /*
     * This function is used to send contact details to Admin
     * @param admin email
     * subjcet
     */ 
    public function contactus(){
        if($this->input->post()) {
            $postdata = $this->input->post();
            /* Send email notification to admin */
            $this->sendNotificationToAdmin($postdata);
        }else {
            $data = array();
            $this->home_template->load('home_template','form_contact',$data,TRUE);
        }
        
    } 
    /*
     * This function is used to create email HTML
     */ 
    public function sendNotificationToAdmin($postdata){
        /* Load Email Library */
        $this->load->library('email'); 
        $this->email->from( FROM_EMAIL, FROM_NAME ); // variable define in config  
        $subject_admin = 'Contact Support';
        $subject_user  = 'Contact Us';
        /* send email notification to admin */
        $this->email->to(ADMIN_EMAIL);
        $this->email->subject($subject_admin);
        $template_to_admin = $this->load->view('email_template_admin',$postdata,true);
        $this->email->message($template_to_admin);  
        $this->email->send();
    }
    
    /*
     * This function is used to view terms and condition page
     * return void
     */ 
    public function termsncondition(){
        $data = array();
        $this->home_template->load('home_template','form_termsncondition',$data,TRUE);
    } 
    /*
     * This function is used to show invitation forms for user
     * @param event_id
     */ 
    public function manageinvitation(){
        $data = array();
        $eventId = $this->uri->segment(3);
        /* Get login user data */
        $loginUserId = isLoginUser();
        $whereConditionForUser      = array('id' => $loginUserId);
        $data['userDetails']   = $this->common_model->getDataFromTabel('user','*',$whereConditionForUser);
        /*********End***********/  
        $data['eventId'] = $eventId;
        $whereConditionForDietary      = array('event_id' => $eventId);
        $data['dietaryRequirements']   = $this->common_model->getDataFromTabel('event_dietary','*',$whereConditionForDietary);
        foreach($data['dietaryRequirements'] as $value) {
            $data['dietaryReq'][] = $value->dietary_name;
        }
        $data['dietaryReq']       = json_encode($data['dietaryReq']);
        
        $bookerWhereCondition = array('event_id' => $eventId,'registrant_id'=>'0','form_id'=>'5');
        $data['bookerformfielddata'] = $this->common_model->getDataFromTabel($this->tablecustomfields,'*',$bookerWhereCondition);
        
        /* Get invitations data */
        $whereConditionInvitation          = array( 'event_id'=> $eventId );
        $data['eventInvitations'] = $this->common_model->getDataFromTabel($this->tableinvitation,'*',$whereConditionInvitation,'','invitations_order','ASC');
        
        if(!empty($data['eventInvitations'])) {
            foreach($data['eventInvitations'] as $inv_data) {
                $whereConInvitation          = array( 'event_id'=> $inv_data->event_id, 'registrant_id' => $inv_data->id, 'form_id' => '4' );
                $data['invitationTypesFormFields'][$inv_data->id]  =  $this->common_model->getDataFromTabel($this->tablecustomfields,'*',$whereConInvitation);
            }
        }
        
        $andwhere  = array('is_theme_used' => '1');
        $data['eventdetails'] = $this->events_model->geteventdetailswiththeme($eventId,$andwhere);
        $this->template->load('template','form_invitation_details',$data,TRUE);
    }
    
    public function saveInvitationFormDetails() {
        $is_ajax_request = $this->input->is_ajax_request();
        //$this->debug();
        
        $loginUserId = isLoginUser();    
        $invitation_types_id        = $this->input->post('invitation_types');
        $eventId                    = $this->input->post('eventId');
        /* Get total invitations limit & waitig list status (is yes/No) */
        $whereConditionInvitation   = array( 'event_id'=> $eventId, 'id' => $invitation_types_id );
        $eventInvitations           = $this->common_model->getDataFromTabel($this->tableinvitation,'*',$whereConditionInvitation);
        $data['eventInvitations']   = $eventInvitations;
        if( count($eventInvitations) > 0 ) {
            $eventInvitations       = $eventInvitations[0];
            $invitationLimit        = $eventInvitations->invitation_limit;
            $allow_waiting_list     = $eventInvitations->allow_waiting_list;
        }else {
            $invitationLimit        = 0 ;
        }
        /* get used invitation limit from added attendee */
        $whereCondition             = array('invitation_type_id' => $invitation_types_id);
        $usedInvitationLimit        = $this->common_model->getDataFromTabel($this->tablebooker,'*',$whereCondition);
        if( count($usedInvitationLimit) > 0 ) {
            $usedquantity = 0;
            if($usedInvitationLimit) {
                foreach($usedInvitationLimit as $ulimit) {
                    $usedquantity       += $ulimit->quantity;
                }
            }
            
        }else {
            $usedquantity = 0;
        }
        
        if($loginUserId==false || $loginUserId==0 || $loginUserId==null) {
            $loginUserId = 0;
        }
        
        if ($this->input->post())
        {
            $arrayVal     =  $this->input->post('fieldid');
            $field_qty    =  $this->input->post('field_qty');
            /* Check waitlist is start */
            $isWaitList    =  $this->input->post('isWaitList');
            
            /* Check post quantity shold not greater from total qty */
            if($isWaitList!=1) {
                $postQty      = $field_qty;
                $postUsedQty  = $postQty + $usedquantity; 
                if(!empty($invitationLimit)) {
                    if($postUsedQty > $invitationLimit) {
                        $remaining      = $postUsedQty - $invitationLimit; 
                        $remaining      = $postQty - $remaining;
                        if($remaining!=0) {
                            $this->form_validation->set_rules('at1', 'You can add only '.$remaining.' attendee for this invitation type', 'callback_custom_error_set');
                            $errors = 'You can add only '.$remaining.' attendee for this invitation type';
                        }else {
                            /* Check wait list*/
                            if($allow_waiting_list!=1) {
                                $this->form_validation->set_rules('at2', 'You need to allow waitlist. If you need to add more attendee', 'callback_custom_error_set');
                                $errors = 'You need to allow waitlist. If you need to add more attendee';
                             
                            }else {
                                $this->form_validation->set_rules('at3', 'Waitlist on', 'callback_custom_error_set');
                                $errors = 'Waitlist ON';
                            }
                        }
                    }
                }
                
                if(empty($invitationLimit)) {
                    echo 'You cant add this invitation has no limit';die;
                }
            }
            
            if ($this->form_validation->run())
            {
                if($is_ajax_request){
                    echo json_message('msg',$errors,'is_success','false');
                }
                die;
            }
            
           
            //update field value
            $arrayFieldVal  =  $this->input->post();
            /* Insert data for Booker person */
            /* End booker */
            $insertDataBooker['event_id']           = $eventId;
            $insertDataBooker['login_id']           = $loginUserId;
            $insertDataBooker['invitation_type_id'] = $arrayFieldVal['invitation_types'];
            $insertDataBooker['quantity']           = $arrayFieldVal['field_qty'];
            $insertDataBooker['unique_id']          = 'NEWMAN-'.randomnumber(4);
            $insertDataBooker['isWaitList']         = $isWaitList;
            $bookerId = $this->common_model->addDataIntoTabel($this->tablebooker,$insertDataBooker);
            $qrPath   = $this->qrCode($eventId,$bookerId);
            $barCode  = $this->getBarCode($bookerId);
            $whereCon = array('id' => $bookerId);
            $dataQr['qrPath']       = $qrPath; 
            $dataQr['barcodePath']  = $barCode; 
            $this->events_model->updateDataFromTable($this->tablebooker,$dataQr,$whereCon);
            /* Insert data for Attendee users */
           
            
            $loop = 1;
            foreach($arrayVal as $fieldid){
                $filedRowId = 'field_'.$fieldid;
                $filedBookerId = 'booker_'.$fieldid;
                $filedBookerImg = 'booker_image_'.$fieldid;
                $filedAttendeeImg = 'attendee_image_'.$fieldid;
                
                if (array_key_exists($filedRowId, $arrayFieldVal)) {
                   $field_data = $arrayFieldVal[$filedRowId];
                   $order = 1;
                   $formid = 4;
                }else if (array_key_exists($filedRowId.'_'.$loop, $arrayFieldVal)) {
                    $field_data = $arrayFieldVal[$filedRowId.'_'.$loop];
                    $loop++;
                    $order = 1;
                    $formid = 4;
                }else if (array_key_exists($filedBookerId, $arrayFieldVal)) {
                    $field_data = $arrayFieldVal[$filedBookerId];
                    $order = 0;
                    $formid = 5;
                }else if (array_key_exists($filedBookerImg, $arrayFieldVal)) {
                    
                    if(!empty($_FILES['bookerimage']['tmp_name'])) {
                        $field_data[] = $_FILES['bookerimage']['name'];
                        $filetmp_name = $_FILES['bookerimage']['tmp_name'];
                        $order = 0;
                        $formid = 5;
                        $fileData = array();
                        $fileData = $_FILES['bookerimage'];
                        $fieldName = 'bookerimage';
                        $this->uploadFileForInvitation($fileData,$fieldName);
                    }else {
                        $field_data = array();
                    }
                }else if (array_key_exists($filedAttendeeImg, $arrayFieldVal)) {
                    
                    if(!empty($_FILES['attendeeimage']['tmp_name'])) {
                        $field_data[] = $_FILES['attendeeimage']['name'];
                        $filetmp_name = $_FILES['attendeeimage']['tmp_name'];
                        $order = 1;
                        $formid = 4;
                        $fileData = array();
                        $fileData = $_FILES['attendeeimage'];
                        $fieldName = 'attendeeimage';
                        $this->uploadFileForInvitation($fileData,$fieldName);
                    }else {
                        $field_data = array();
                    }
                }else {
                    $field_data = array();
                }

                
                if(!empty($field_data)) {
                    $counter = $order;
                    foreach($field_data as $fieldValue) {
                        $insertDataAttendee['field_value']  = $fieldValue;
                        $insertDataAttendee['field_id']     = $fieldid;
                        $insertDataAttendee['booker_id']    = $bookerId;
                        $insertDataAttendee['form_id']      = $formid;
                        $insertDataAttendee['field_order']  = $counter;
                        //$insertDataAttendee['invitation_master_id']  = $masterId;
                        
                        $counter++;
                        $responce = $this->common_model->addDataIntoTabel($this->tableattendee,$insertDataAttendee);
                                    
                    }
                }
            }
            
            if($is_ajax_request){
                echo json_encode(array('msg'=> 'Saved successfully!','is_success'=>'true','url'=>'events/manageinvitation/'.$eventId,'bookerId'=>$bookerId));
              }else{
                $msgArray = array('msg'=>$msg,'is_success'=>'true');
                set_global_messages($msgArray);
                //redirect('event/eventdetails/'.encode($eventId));
              }
            
           /* $bookerId is "invitation_booker_details" table id 
            * & $field_qty is total selected quantity in form 
            **/
           //$this->attendeeConfirmation($bookerId,$field_qty);
        
        }
    }
    
    /*
     * Edit or Modify attendee details
     * @param
     * bookerId
     */  
     
    public function modifyAttendee(){
        $bookerId                       = $this->input->post('bookerId');
        $quantity                       = $this->input->post('quantity');
        $eventId                       = $this->input->post('eventId');
        $bookerWhereCondition1          = array('booker_id' => $bookerId, 'form_id' => 5);
        $fieldIdbooker = $this->common_model->getDataFromTabel($this->tableattendee,'id,field_id,field_value',$bookerWhereCondition1);
        for($counter = 1; $counter <= $quantity; $counter++) {
            $bookerWhereCondition2      = array('booker_id' => $bookerId, 'form_id' => 4, 'field_order' => $counter);
            $fieldIdsattendee[] = $this->common_model->getDataFromTabel($this->tableattendee,'id,field_id,field_value',$bookerWhereCondition2);
        }
        $data['fieldIdsbooker']         = $fieldIdbooker;
        $data['fieldIdsattendee']       = $fieldIdsattendee;
        $data['quantity']               = $quantity;
        $data['bookerId']               = $bookerId;
        $data['eventId']               = $eventId;
        //$this->debug($data);
        $this->home_template->load('home_template','form_update_attendee',$data,TRUE);
    } 
    /*
     * display resent saved attendee details
     * @param
     * bookerId, quantity
     */
    public function attendeeConfirmation($bookerId=null,$quantity=null,$eventId=null){
        
        if($bookerId==null || $quantity==null) {
            $bookerId = $this->input->post('bookerId');
            $quantity = $this->input->post('bookerQuantity');
            $eventId = $this->input->post('event_id');
        }
        
        $eventdetails  = $this->events_model->geteventdetails($eventId,$userId=0);
        
        $data['bookerId'] = $bookerId;
        $data['quantity'] = $quantity;
        $data['eventId']  = $eventId;
        $data['eventdetails']  = $eventdetails;
        $bookerWhereCondition           = array('booker_id' => $bookerId, 'form_id'=>'5');
        $data['bookerDetails']          = $this->common_model->getDataFromTabel($this->tableattendee,'*',$bookerWhereCondition);
        for($counter = 1; $counter <= $quantity; $counter++) {
            $attendeeWhereCondition     = array('booker_id' => $bookerId, 'form_id'=>'4', 'field_order' => $counter);
            $data['attendeeDetails'][]  = $this->common_model->getDataFromTabel($this->tableattendee,'*',$attendeeWhereCondition);
        }
        
        if(!empty($bookerId)) {
            $whereCon = array('id' => $bookerId);
            $upData['isRegister'] = '1'; 
            $this->events_model->updateDataFromTable($this->tablebooker,$upData,$whereCon);
        }
        $this->home_template->load('home_template','form_attendee_confirmation',$data,TRUE);
    } 
    
    /*
     * This function is used to create invitation form HTML
     * @param eventId, invitationTypeId, quantity
     * return form fields
     */ 
    public function getInvitationHtml() {
        /* get parameter from ajax call page form_invitation_details */
        $postdata = $this->input->post();
        $eventId            = $postdata['event_id'];
        $invitationTypeId   = $postdata['invitationTypeId'];
        $quantity           = $postdata['quantity'];
        $form_id            = '4';
        
        if( ( $quantity > 0 ) &&  !empty($eventId) && !empty($invitationTypeId) ) {
            $data['eventId']            = $eventId;
            $data['quantity']           = $quantity;
            $data['invitationTypeId']   = $invitationTypeId;
            
            /* Get invitations data */
            $whereConditionInvitation          = array( 'event_id'=> $eventId );
            $data['eventInvitations'] = $this->common_model->getDataFromTabel($this->tableinvitation,'*',$whereConditionInvitation,'','invitations_order','ASC');
            
            $whereConInvitation = array( 'event_id'=> $eventId, 'registrant_id' => $invitationTypeId, 'form_id' => $form_id );
            $data['invitationTypesFormFieldsData']  =  $this->common_model->getDataFromTabel($this->tablecustomfields,'*',$whereConInvitation);
            
            $dataHtml = $this->load->view('form_invitation_types_form_fields',$data,true);
            echo $dataHtml;die;
            
        }else {
            echo 'false';die;
        }
    }
    
    /*
     * Update Attendee & booker detais
     */
     public function updateAttendee(){
        $quantity = $this->input->post('quantity');
        $bookerId = $this->input->post('bookerId');
        $eventId = $this->input->post('eventId');
        
        $bookerIdWithValue = $this->input->post('bookeridvalue');
        foreach($bookerIdWithValue as $key => $fieldValue) {
            $updateData['field_value'] = $fieldValue;
            $where = array('id' => $key);
            //call common model for update data
            $this->common_model->updateDataFromTabel($this->tableattendee,$updateData,$where);
        }
        $attendeeValueWithIds = $this->input->post('attendee_value');
        foreach($attendeeValueWithIds as $key1 => $fieldValue1) {
            $updateData1['field_value'] = $fieldValue1;
            $where1 = array('id' => $key1);
            //call common model for update data
            $this->common_model->updateDataFromTabel($this->tableattendee,$updateData1,$where1);
        }
        redirect('events/attendeeConfirmation/'.$bookerId.'/'.$quantity.'/'.$eventId);
        
     }
    
    /*
     * QR Code
     * Library in library ciqrcode for QR code
     */
    public function qrCode($eventId,$bookerId){
        
        $eventdetails  = $this->events_model->geteventdetails($eventId,$userId=0);
        
        $eventTitle =   (!empty($eventdetails->event_title)) ? $eventdetails->event_title : '';  
        $startDate  =   date('l jS F Y',strtotime($eventdetails->starttime));
        $endDate    =   date('l jS F Y',strtotime($eventdetails->endtime));
        $eventVenue =   (!empty($eventdetails->event_venue)) ? $eventdetails->event_venue : '';  
        
        $this->load->library('ciqrcode');
        //Added files Ciqrcode.php  added qrcode dir
        //header("Content-Type: image/png");
        $imageName = randomnumber(5);
        $params['data']     = $eventTitle.' '.' DATE & TIME : '.$startDate.' to '.$endDate.'  LOCATION : '.$eventVenue;
        $params['level']    = 'H';
        $params['size']     = 3; 
        $params['savename'] = './media/qrcode/qr_'.$bookerId.'.png'; //Image saved directory
        $imagePath = base_url().'media/qrcode/qr_'.$bookerId.'.png';
        $this->ciqrcode->generate($params);
        return $imagePath;
    
    }
    
     /*
     * BAR Code
     * Library in library zend & Ean13 for same
     * dir under media/barcode
     */
     
     public function getBarCode($bookerId){
        
        $this->load->library('zend');
		$this->zend->load('Zend/Barcode');
		// prepare barcode from zend library
		
        //include_once('Zend/Barcode.php');
        //include_once('Zend/Barcode/Object/Ean13.php');
        $barcode = rand(100000000000,111110000000);
        
        $file = Zend_Barcode::draw('ean13', 'image', array('text' => $barcode), array());
        $barcode = $this->get_barcode_digits($barcode);
        //$file = Zend_Barcode::draw('ean13', 'image', array('text' => $barcode), array());
        //$barcode_text = Zend_Barcode::draw('ean13', 'image', array('text' => $barcode), array());
        // set barcode image path
        //define('UPLOAD_DIR', 'uploads/');
        $dir_path = './media/barcode/';
        // set barcode image name
        //$image_name = time().$barcode;
        $image_name = $barcode.'.png';
        // create image from barcode
        ImagePNG($file, $dir_path.$image_name);
        ImageDestroy($file);
        return $image_name;
     }
     
     public function get_barcode_digits($barcode='') {
		
		//first change digits to a string so that we can access individual numbers
		$digits =(string)$barcode;
		// 1. Add the values of the digits in the even-numbered positions: 2, 4, 6, etc.
		$even_sum = $digits{1} + $digits{3} + $digits{5} + $digits{7} + $digits{9} + $digits{11};
		// 2. Multiply this result by 3.
		$even_sum_three = $even_sum * 3;
		// 3. Add the values of the digits in the odd-numbered positions: 1, 3, 5, etc.
		$odd_sum = $digits{0} + $digits{2} + $digits{4} + $digits{6} + $digits{8} + $digits{10};
		// 4. Sum the results of steps 2 and 3.
		$total_sum = $even_sum_three + $odd_sum;
		// 5. The check character is the smallest number which, when added to the result in step 4,  produces a multiple of 10.
		$next_ten = (ceil($total_sum/10))*10;
		$check_digit = $next_ten - $total_sum;
		$barcode_digits = $digits.$check_digit;
		// Count of barcode text digits
		$barcode_digit_count = strlen($barcode_digits);
		if($barcode_digit_count < 13) {
			$remain_text = 13 - $barcode_digit_count;
			for($i=0;$i<$remain_text;$i++) {
				$barcode_digits = '0'.$barcode_digits;
			}
		}
		return $barcode_digits;
	}
    
     /* Method for upload file
     * invitation attendee & booker images
     * @param field name & files data
     */
    public function uploadFileForInvitation($fileData,$fieldName) {
        $config['upload_path']      = './media/invitationImages/';
		$config['allowed_types']    = 'gif|jpg|png';
		//$config['max_size']	        = '100';
		//$config['max_width']        = '1024';
		//$config['max_height']       = '768';

		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload($fieldName))
		{
			$error = array('error' => $this->upload->display_errors());
            print_r($error);die;
		}
		else
		{
			$data = $this->upload->data();
		}
    
    }  
    
    public function isAllowWaitList() {
        $eventId                = $this->input->post('eventId');
        $invitation_types_id    = $this->input->post('invitation_types_id');
        
         /* Get total invitations limit & waitig list status (is yes/No) */
        $whereConditionInvitation   = array( 'event_id'=> $eventId, 'id' => $invitation_types_id );
        $eventInvitations           = $this->common_model->getDataFromTabel($this->tableinvitation,'*',$whereConditionInvitation);
        $data['eventInvitations']   = $eventInvitations;
        if( count($eventInvitations) > 0 ) {
            $eventInvitations       = $eventInvitations[0];
            $invitationLimit        = $eventInvitations->invitation_limit;
            $allow_waiting_list     = $eventInvitations->allow_waiting_list;
        }else {
            $allow_waiting_list     = 0;
            $invitationLimit        = 0 ;
        }
        
        //print_r($eventInvitations);die;
        
        /* get used invitation limit from added attendee */
        $whereCondition             = array('invitation_type_id' => $invitation_types_id, 'isWaitList !=' => 1);
        $usedInvitationLimit        = $this->common_model->getDataFromTabel($this->tablebooker,'*',$whereCondition);
        
        if( count($usedInvitationLimit) > 0 ) {
            $usedquantity = 0;
            if($usedInvitationLimit) {
                foreach($usedInvitationLimit as $ulimit) {
                    $usedquantity       += $ulimit->quantity;
                }
            }
            
        }else {
            $usedquantity = 0;
        }
        
        /* Check post quantity shold not greater from total qty */
        if(!empty($invitationLimit)) {
            if($usedquantity == $invitationLimit) {
                if($allow_waiting_list) {
                    echo 'allow_waitlist';die;
                }else {
                    echo 'false';die;
                }
            }else {
                echo 'allow';die;
            }
        }
        
        if(empty($invitationLimit)) {
            echo 'nolimit';die;
        }
    }
    
    public function pdfExport()
    {
        $this->load->library('parser');
        $stringData = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <title>Untitled Document</title>
      <style type="text/css">
         body, h1, h2, h3, h4, p { margin: 0; padding: 0; }
         p { font-size: 12px; line-height: 18px; }
         .hadding-top { color: #609dd4; }
         .hadding-next { color: #FFF; margin: 5px 0 0 0; }
         .contant01 { color: #FFF; margin: 5px 0 0 0; }
      </style>
   </head>
   <body>
      <table width="200" border="0" cellpadding="0" cellspacing="0" background="#dcd8d6" style="padding: 20px 20px 0px; background:#dcd8d6;" >
         <tr>
            <td><img src="'.base_url().'media/back.png" /></td>
         </tr>
         <tr>
            <td width="489px" height="104px" style="background-color:#2d2837; padding:24px; float:left; border-top-left-radius:0px;
               border-top-right-radius:0px;
               border-bottom-right-radius:0px;
               border-bottom-left-radius:10px;">
               <h2  class="hadding-top">Beyond Traditional 2015 Conference</h2>
               <h4 class="hadding-next">THE NATIONAL CONFERENCE 2015</h4>
               <p class="contant01">ANZ STADIUM, SYDNEY</p>
               <p class="contant01"> 28 - 31 OCTOBER 2015 </p>
            </td>
         </tr>
         <tr  style="background-color:#FFF; padding:20px; margin:13px 0 0 0;float:left;
            border-top-left-radius:20px;
            border-top-right-radius:20px;
            border-bottom-right-radius:0px;
            border-bottom-left-radius:20px;  width: 497px; ">
            <td  width="40%" style="float:left; margin: 0 0 0 5px;">
               <h4>Bill to:</h4>
               <p  style="margin:0 0 0 20px">MR JIM MACKENZIE</p>
               <p style="margin:0 0 0 20px">GOOGLE INC.</p>
               <p style="margin:0 0 0 20px">jim@gmail.com</p>
               <p style="margin:0 0 0 20px">02 6239 3363</p>
            </td>
            <td  width="50%" style="float:left; margin:25px 0 0 0;">
               <h3 style=" text-align:center">TREAT YOUR eTICKET
                  LIKE CASH
               </h3>
            </td>
		</tr>
         <tr style="margin:16px 0 0 0; float:left;">
            <td>
               <h4 style="margin:5px 0 11px 0; float:left; ">TERMS & CONDITION</h4>
               <p style="clear:both; float:left; font-size:10px; line-height:13px;">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo.</p>
            </td>
         </tr>
         <tr  style="background-color:#FFF; padding:14px; margin:28px 0 0 0;float:left;
            border-top-left-radius:20px;
            border-top-right-radius:20px;
            border-bottom-right-radius:0px;
            border-bottom-left-radius:20px; width:497px;" >
            <td width="100%" style="float:left; margin:0 0 10px 0">
               <h3 style="padding:0px 0 0 84px;">Beyond the Traditional 2015 Conference</h3>
            </td>
            <td style="float:left;padding: 0 29px 0 0;"><img src="'.base_url().'media/barcode.png" /></td>
            <td width="49%" style="float:left;">
               <h4 style="float:left;">Date And Time</h4>
               <p style="float:left;">Wednesday, 28October 2015, 7:00am</p>
               <p style="float:left;">to Saturday, 31October 2015, 5:00pm</p>
               <h4 style="float:left; margin:10px 0 0 0;"> LOCATION: </h4>
               <p style="float:left; clear:both;">ANZ STADIUM</p>
               <p style="float:left; clear:both;">EDWIN FLACK AVE</p>
               <p style="float:left; clear:both;">Sydney Olympic park NSW 2127</p>
            </td>
            <td>
               <h4 style=" text-align:right; margin: 10px 0 0;">Mr jim Makenzil</h4>
               <p>Dietary Requirement: Gluten </p>
               <img src="'.base_url().'media/bar.png" style="float:right; margin:10px 0 0 0;" /> 
            </td>
            <td style="float: left; width:100%;">
               <p style="  font-size: 9px;
                  margin: 0 0 0 84px;">please read fullterms &amp; condtions at www.newmanevents.com/BTC</p>
            </td>
         </tr>
         <tr>
            <td  style="float:left; margin:20px 0 19px 10px;">
               <p style="float:left;margin:4px 0 0 0px;"> Powered by </p>
               <img src="'.base_url().'media/logo.png" style="float:left; margin:0 0 0 20px;"/>
            </td>
         </tr>
      </table>
   </body>
</html>

';
        //$stringData = $this->parser->parse('WH_PDF',$data,TRUE);       
        $this->load->helper('pdfexport_helper');
        $myFile 	 = "newman(".date('Y-m-d').").pdf";
        exportPDF($stringData,$myFile);   
    }
    
    public function getFbFriends() {
        /*
         * FB intregation
         */ 
        $this->load->library('facebook', array(
            'appId'     => FB_APP_ID,
            'secret'    => FB_SECRET,  
        )); 
    }
    
    public function checkPassAuthentication(){
        $eventId                = $this->input->post('eventId');
        $invitation_types_id    = $this->input->post('invitation_types_id');
        $password               = $this->input->post('password');
        $whereCondition = array('event_id' => $eventId,'password' => $password,'id' => $invitation_types_id);
        $responce   = $this->common_model->getDataFromTabel($this->tableinvitation,'*',$whereCondition);
        if(!empty($responce) && count($responce) > 0 ) {
            $this->session->set_userdata('password_access', '1');
            echo 'true';die;
        }else {
            echo 'false';
        }
        
        
    }
    
    public function debug($data=array()){
        
        $eventId = 232;
        $whereCondition = array('event_id' => $eventId);
        $eventInvitee   = $this->common_model->getDataFromTabel($this->tableinvitee,'*',$whereCondition);
        echo '<pre>';
        print_r($eventInvitee);die;
        if(!empty($eventInvitee)) {
            foreach($eventInvitee as $invitee) {
                $user_id = @$invitee->user_id;
                if($user_id!='' && $user_id!=null && $user_id!=0) {
                    $whereConditionUser = array('id',$user_id);
                    $users   = $this->common_model->getDataFromTabel($this->tableinvitee,'*',$whereCondition);
                }
            }
        }
        
        $this->load->library('facebook', array(
            'appId'     => FB_APP_ID,
            'secret'    => FB_SECRET,  
        )); 
        
        $data['user_profile'] = $this->facebook->api('/me?fields=email,first_name,last_name');
        $data['picture'] = $this->facebook->api("/me/picture","GET", array ('redirect' => false,'type' => 'large' ) );
          
        
        echo '<pre>';
        print_r($_POST);
        print_r($data);
        die;
    }
    
    
     /*
     * This function is used to send contact details to Admin
     * @param admin email
     * subjcet
     */ 
    public function addEmailInvitation($eventId) {
       
        if($this->input->post()) {
            $postdata = $this->input->post();
            if(!empty($postdata)) {
                if($postdata['isImport']=='1') {
                    $emailData      = '';
                    $inviteeemails  = $postdata['inviteeemails'];
                    if(!empty($inviteeemails)) {
                        $emailData      = implode(',',$inviteeemails);
                    }
                }else {
                    $emails = $postdata['emails'];
                    $emails = str_replace(',',';',$emails);
                    $emails = explode(';',$emails);
                    $emailData      = implode(',',$emails);
                }
                
                /* Send email notification with event link */
                /* Do code here */
            }
            
        }else {
            $data = array();
            $data['alldata'] ='';
            $data['eventId'] = $eventId;
            $this->template->load('template','form_email_invitation',$data,TRUE);
        }
        
    } 
    
    
    /*
    * @Description:this functio  handel  the call back url  data from gmail .
    * @access: private
    * @return: void
    * @author: $$
    */
      function GmailContactCallback(){
		  	
                $eventId = $this->session->userdata("eventId");
		  		$post                    = '';
                $data                    = array();
                $getGmaildata            = $this->input->get();
            	$data['auth_code']       = $this->input->get('code');
			    $data['client_id']       = $this->config->item('local_client_id');
				$data['client_secret']  = $this->config->item('local_client_secret');
				$data['redirect_uri']   = $this->config->item('local_redirect_uri');
				$data['max_results']     = $this->config->item('max_results');
                if($this->input->get('code')) {
                    $fields=array(
							'code'         =>  urlencode($data['auth_code']),
							'client_id'    =>  urlencode($data['client_id']),
							'client_secret'=>  urlencode($data['client_secret']),
							'redirect_uri' =>  urlencode($data['redirect_uri']),
							'grant_type'   =>  urlencode('authorization_code')
    						 );
                           
						foreach($fields as $key=>$value){ 
							 $post .= $key.'='.$value.'&';
						}

            			$post    = rtrim($post,'&');
			        $accesstoken = $this->curl_get_GmailToken($post);
                    if($accesstoken==false || $accesstoken=='' || $accesstoken==null) {
                        redirect('events/addEmailInvitation/'.$eventId);
                    }else {
			        $url = 'https://www.google.com/m8/feeds/contacts/default/full?max-results='.$data['max_results'].'&oauth_token='.$accesstoken;
          
                    $xmlresponse = $this->curl_file_get_contents($url);
                
                    if((strlen(stristr($xmlresponse,'Authorization required'))>0) && (strlen(stristr($xmlresponse,'Error '))>0)){
					      $data['flag']        = 0;
					      $data['message']     = 'OOPS !! Something went wrong. Please try after some time.';		
						  $data['result']      = '';
						}else{							
							$data['flag']      = 1;
					        $data['message']   = 'success';
					  		$xml               =  new SimpleXMLElement($xmlresponse);
							$xml->registerXPathNamespace('gd', 'http://schemas.google.com/g/2005');
							$result            =  $xml->xpath('//gd:email');
							$alldata           = array(); 
							foreach ($result as $title) {
							$alldata[] = (string) $title->attributes()->address;
							}							
							$data['alldata']    = $alldata;			
                            $data['eventId'] = $eventId;
                            $this->template->load('template','form_email_invitation',$data,TRUE);		
						}
                    }
            }
                        
			       //return $legalsContactPopup=$this->load->view('callback',$data,true);
			       
       }
              
       function curl_get_GmailToken($post){
                $urlGetToken ='https://accounts.google.com/o/oauth2/token';
                $curl = curl_init();
				curl_setopt($curl,CURLOPT_URL,$urlGetToken);
				curl_setopt($curl,CURLOPT_POST,5);
				curl_setopt($curl,CURLOPT_POSTFIELDS,$post);
				curl_setopt($curl, CURLOPT_RETURNTRANSFER,TRUE);
				curl_setopt($curl, CURLOPT_SSL_VERIFYPEER,0);
				curl_setopt($curl, CURLOPT_SSL_VERIFYHOST,0);
				
				$result = curl_exec($curl);
				curl_close($curl);
        		$response =  json_decode($result);
				$accesstoken = $response->access_token;
                return  $accesstoken ; 
     	}  
     	     
       function curl_file_get_contents($url){
			 
			 $curl = curl_init();
			 $userAgent = 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; .NET CLR 1.1.4322)';
			 
			 curl_setopt($curl,CURLOPT_URL,$url);	//The URL to fetch. This can also be set when initializing a session with curl_init().
			 curl_setopt($curl,CURLOPT_RETURNTRANSFER,TRUE);	//TRUE to return the transfer as a string of the return value of curl_exec() instead of outputting it out directly.
			 curl_setopt($curl,CURLOPT_CONNECTTIMEOUT,5);	//The number of seconds to wait while trying to connect.	
			 
			 curl_setopt($curl, CURLOPT_USERAGENT, $userAgent);	//The contents of the "User-Agent: " header to be used in a HTTP request.
			 curl_setopt($curl, CURLOPT_FOLLOWLOCATION, TRUE);	//To follow any "Location: " header that the server sends as part of the HTTP header.
			 curl_setopt($curl, CURLOPT_AUTOREFERER, TRUE);	//To automatically set the Referer: field in requests where it follows a Location: redirect.
			 curl_setopt($curl, CURLOPT_TIMEOUT, 10);	//The maximum number of seconds to allow cURL functions to execute.
			 curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);	//To stop cURL from verifying the peer's certificate.
			 curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
			 $contents = curl_exec($curl);
			 curl_close($curl);
			 return $contents;
			}
            
            
              /*
     * @description: This function is used to save skills & qualifications data
     * @access: public
     * @return void
     */ 
        public function typeselectoptions() {
	    $val = $this->input->post('val');
	    $eventId = $this->input->post('eventId');
        $this->session->set_userdata("eventId",$eventId);
	    $url = "";
	    //1-yahoo:2-gmail:3-hotmail:4-facebook
	    if(!empty($val)){
			// set the session for contact import	
		 $this->session->set_userdata("sstype",$val);	
        // set default redirect url
          switch ($val) {			 
			case 1:
				$url = "";
				break; 
			case 2:
				$url = 'https://accounts.google.com/o/oauth2/auth?client_id='.$this->config->item('local_client_id').'&redirect_uri='.$this->config->item('local_redirect_uri').'&scope=https://www.google.com/m8/feeds/&response_type=code';
				break; 
			case 3: 
				$url = 'https://login.live.com/oauth20_authorize.srf?client_id='.$this->config->item('Hcdn_client_id').'&scope=wl.signin%20wl.basic%20wl.emails%20wl.contacts_emails&response_type=code&redirect_uri='.$this->config->item('Hcdn_redirect_uri');
		      }
		   }
        echo json_encode(array('url'=>$url,'val'=>$val));
		}      
        
        /*
     * This function is used to create email HTML
     */ 
    public function sendEmailToInvitee($emailData){
        /* Load Email Library */
        $this->load->library('email'); 
        $this->email->from( FROM_EMAIL, FROM_NAME ); // variable define in config  
        $subject_user  = 'You have invited';
        $postdata = array();
        /* send email notification to admin */
        $this->email->to(ADMIN_EMAIL);
        $this->email->bcc($emailData);
        $this->email->subject($subject_user);
        $template_to_admin = $this->load->view('email_template_invitee',$postdata,true);
        $this->email->message($template_to_admin);  
        $this->email->send();
    } 
  
  
}



/* End of file events.php */
?>
