<?php
  $eventId=$this->uri->segment(3);

  ?>
    <div class="container mt40lesseight container_main">
		<?php echo $this->load->view('header'); ?>
    <div class="row-fluid mt10">
    	<div class="span7 signupresponsive">
        <div class="container-fluid signupbg">
        	<div class="signup">Sign Up!</div>
            <div class="row-fluid">
            	<ul class="frontendsignbg">
                    <li>
						 <a href="<?php echo base_url().'frontend/standardregister/'.$eventId; ?>"> 
                            <div class="paymentcircle">
                              	<div class="signupcirclebg signupregister"></div>
                            </div>
                           
                                <span class="signbottomfon">Register for Event</span>
                           </a>  
                    </li>
                    
                    <li>
						 <a href="<?php echo base_url().'sideevent/standardregister/'.$eventId; ?>"> 
                            <div class="paymentcircle">
                              	<div class="signupcirclebg signupregister"></div>
                            </div>
                           
                                <span class="signbottomfon">Register for SideEvent</span>
                           </a>  
                    </li> 
                   
                    <li>
						 <a href="<?php echo base_url().'sponsor/index/'.$eventId; ?>"> 
                            <div class="paymentcircle">
                              <div class="signupcirclebg signusponcer"></div>
                            </div>
                          <span class="signbottomfon">Be a Sponsor</span>
                         </a>  
                    </li>
                    <!--
                     <li>
						 <a href="<?php //echo base_url().'exhibitor/index/'.$eventId; ?>"> 
                            <div class="paymentcircle">
                              <div class="signupcirclebg signupprovide"></div>
                            </div>
                         <span class="signbottomfon">Provide an Exhibition</span>
                         </a>
                    </li> -->
                 
       		 </ul>
             <div class="clearfix"></div>
             <div class="spacer40"></div>
            </div>
            </div>
           
          <div class="signupbottomdiv">
          	<div class="span6 pt12 pb12">
            	<div class="commonS_circle">
                	<div class="paymentbooking payment"></div>
                </div>
                <div class="commoncircle_content">Payments & Tax Invoice Information</div> 
            </div>
            
            	<div class="span6 pt12 pb12 bdr_ldash">
            	<div class="commonS_circle">
                <div class="paymentbooking booking"></div>
                </div>
                <div class="commoncircle_content">Booking Changes & Cancellations</div> 
            </div>
            
            <div class="clearfix"></div>
            </div>
           
            
        </div>
        
         <div class="span5 sponcerbg pull-right spnncerresponsive">
         	<div class="sponcerHeading">Our Sponsors</div>
            <div class="logorowcontainer">
            	<div class="sponsorlogobg"><img src="<?php echo IMAGE;?>ecropromo.jpg"></div>
                <div class="sponsorlogobg"><img src="<?php echo IMAGE;?>byfriday.jpg"></div>
            </div>
            
            <div class="logorowcontainer">
                <div class="sponsorlogobg"><img src="<?php echo IMAGE;?>mailchimp.jpg"></div>
                <div class="sponsorlogobg"><img src="<?php echo IMAGE;?>europen.jpg"></div>
            </div>
            
            <div class="logorowcontainer">
                <div class="sponsorlogobg"><img src="<?php echo IMAGE;?>square.jpg"></div>
                <div class="sponsorlogobg"><img src="<?php echo IMAGE;?>icon.jpg"></div>
         	</div>
            
           
            <div class="clearfix"></div>
             <div class="seprator42"></div>
             
            <div class="bg_00aec5"></div>
            <div class="bg_7cd0de"></div>
            <div class="bg_a3dce7"></div>
         </div><!-- /sponcerbg -->
         
         
         
        
        <div class="clearfix"></div>
    </div>
    
    <div class="spacer40"></div>    
</div> <!-- /container -->



<script type="text/javascript">
  $(function() {
    $('#datetimepicker4').datetimepicker({
      pickTime: false
    });
  });
</script>

