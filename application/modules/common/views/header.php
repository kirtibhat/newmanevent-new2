<?php
 
  $header1='';
  $header2='';
  $logoPath='';
  $headerPath='';
  
   $eventId = (isset($event_id) && !empty($event_id))?$event_id:'';
   $customizeForms= (isset($customize_form) && !empty($customize_form))?$customize_form:'';
 
	//get event details data
   
	$eventName = '';
	$eventLocation = '';
	$evenStartDate = '';
	$eventEndDate = '';
   if(isset($eventDetails) && !empty($eventDetails)){
	    $eventData=$eventDetails[0];
		$eventName = (!empty($eventData->event_title))?$eventData->event_title:'';	
		$eventLocation = (!empty($eventData->event_location))?$eventData->event_location:'';	
		$evenStartDate = (!empty($eventData->starttime))?dateFormate($eventData->starttime,'d'):'';	
		$eventEndDate = (!empty($eventData->endtime))?dateFormate($eventData->endtime,'d F Y'):'';	
		
	}
	
 if(!empty($customizeForms)){
	if(strlen($customizeForms[0]->header1_color)>0)
	{
		$header1='style="color:'.$customizeForms[0]->header1_color.'"';
	}
	
	if(strlen($customizeForms[0]->header1_color)>0)
	{
		 $header2='style="color:'.$customizeForms[0]->header2_color.'"';
	}
	if(!empty($customizeForms[0]->logo_image))
	{
	    $logoPath=file_exists(FCPATH.$customizeForms[0]->logo_image_path.$customizeForms[0]->logo_image);
	}
	if(!empty($customizeForms[0]->header_image))
	{
	  $headerPath=file_exists(FCPATH.$customizeForms[0]->header_image_path.$customizeForms[0]->header_image);
    }
	
}
?>
	<header>
    	<div class="row-fluid">
		
		  <div class="pull-left">  
		      <a href="<?php echo base_url().'frontend/index/'.encode($eventId);?>" title="Home">
		      	<?php
                    	 
					if(!empty($customizeForms) && !empty($logoPath))
					{
						
						
					     ?>
						  <img  src="<?php echo base_url().$customizeForms[0]->logo_image_path.$customizeForms[0]->logo_image; ?>" >
					    
					     <?php
					
					}
					else
					{
						 
					 ?>
					   <img src="<?php echo IMAGE; ?>eventlogo.png"  alt="logo" />			 
					 <?php 
					} 
					?>
                
              </a>
           </div>
			
        </div>
    </header>
 
    <div class="row-fluid mt40">
    	<div class="span12 fronendsliderbg">
        	<ul>
            	<li>
               		 <div class="slidertitle">
                    	<span class="headingbig" <?php  echo $header1; ?> ><?php echo ucwords($eventName); ?></span>
                        	<div class="clearfix"></div>
                    	<span class="headingsmall" <?php  echo $header2; ?>><?php echo ucwords($eventLocation).' • '.$evenStartDate.' - '.$eventEndDate; ?> </span>
                    </div>
                    	<?php
                    	 
						if(!empty($customizeForms) && !empty($headerPath))
						{
							 //class header_width
					     ?>
                	           <img  src="<?php echo base_url().$customizeForms[0]->header_image_path.$customizeForms[0]->header_image; ?>"  alt="sliderimg"  class="">
                         <?php
						}
						else
						{
					     ?>
                            <img src="<?php echo IMAGE;?>slider.jpg" alt="sliderimg">				 
                         <?php 
						} 
					     ?>
                	
                </li>
            </ul>
        </div>
    </div>
    <style>
	  
	</style>
