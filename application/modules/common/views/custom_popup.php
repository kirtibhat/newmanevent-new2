<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<!-- Modal -->


<div class="modal fade signup_popup" aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="thanks_popup">
  <div class="modal-dialog modal-sm-as">
    <div class="modal-content">
      <div class="modal-header ">
        <h4 class="modal-title popu_title" id="popup_heading"></h4>
      </div>
      <div class="modal-body">
        <div class="modal-body-content">
            <p class="form-label show_custome_msg"></p>
        </div>
        <div class="modal-footer">
          <div class="pull-right">
            <button class="btn-normal btn popup_cancel"  data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


<div class="modal fade signup_popup" aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="success_popup_with_message">
  <div class="modal-dialog modal-sm-as">
    <div class="modal-content">
      <div class="modal-header ">
        <h4 class="modal-title popu_title" id="popup_heading"></h4>
      </div>
      <div class="modal-body">
        <div class="modal-body-content">
            <p class="form-label show_custome_msg"></p>
        </div>
        <div class="modal-footer">
          <div class="pull-right">
<!--
            <button class="btn-normal btn popup_cancel"  data-dismiss="modal">Close</button>
-->
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Modal -->
