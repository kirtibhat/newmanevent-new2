<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<!-----message showing custome popup------>
<div class="modal fade dn loaderbg" id="loader_popup_show" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class=""><!-- modal-content -->
        <div class=""> 
            <div class="modelinner"> 
                <div class="loader_wrapper"> 
                    <img src="<?php echo IMAGE; ?>loader_img.gif" alt="logo" id="logo_header" /><p class="loader_msg_text"></p>
                </div> 
            </div> 
        </div> 
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

