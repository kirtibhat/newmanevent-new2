<?php 
//---------cancle button-------------
if(isset($buttonArray['cancle'])){
//set default value for button
$buttonName  = (isset($buttonArray['cancle']['name']))?$buttonArray['cancle']['name']:'logincancel';
$buttonId    = (isset($buttonArray['cancle']['id']))?$buttonArray['cancle']['id']:'logincancel';
$buttonClass = (isset($buttonArray['cancle']['class']))?$buttonArray['cancle']['class']:'popup_cancel submitbtn btn-normal btn"';
$buttonValue = (isset($buttonArray['cancle']['value']))?$buttonArray['cancle']['value']:$this->lang->line('comm_cancle');
$cancleButtonTab = isset($buttonArray['cancle_tabindex']) ? $buttonArray['cancle_tabindex'] : '';
$cancelButton = array(
    'name'	=> $buttonName,
    'id'	=> $buttonName,
    'type'	=> 'button',
    'value'	=> $buttonValue,
    'class' => $buttonClass,
    'data-dismiss' => 'modal',
'tabindex' => $cancleButtonTab
);
echo form_input($cancelButton);
}
//---------submit button---------------
if(isset($buttonArray['submit'])){
//set default value for button
$buttonName  = (isset($buttonArray['submit']['name']))?$buttonArray['submit']['name']:'loginsubmit';
$buttonId    = (isset($buttonArray['submit']['id']))?$buttonArray['submit']['id']:'loginsubmit';
$buttonClass = (isset($buttonArray['submit']['class']))?$buttonArray['submit']['class']:'popup_login submitbtn btn-normal btn "';
$buttonValue = (isset($buttonArray['submit']['value']))?$buttonArray['submit']['value']:$this->lang->line('home_login_button');
$submitButtonTab = isset($buttonArray['submit_tabindex']) ? $buttonArray['submit_tabindex'] : '';  
$submitButton = array(
    'name'	=> $buttonName,
    'id'	=> $buttonId,
    'type'	=> 'submit',
    'value'	=> $buttonValue,
    'class' => $buttonClass,
    
'tabindex' => $submitButtonTab
);
echo form_input($submitButton);
}


?>
