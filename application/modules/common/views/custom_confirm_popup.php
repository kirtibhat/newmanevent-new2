<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<div class="modal fade alertmodelbg dn" id="custom_confirm_popup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<form id="confirm_form" name="confirm_form" action=""  method="post">
	  <div class="modal-dialog">
		<div class="modal-content">
		  <div class="modal-header <?php echo (isset($popupHeaderBg))?$popupHeaderBg:""; ?>">
		  <!---  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>--->
			<h4 class="modal-title popu_title" >Confirm</h4>
		  </div>
			   <div class="show_custome_msg">
      
				</div>
			<div class="modal-footer">
				<input type="hidden" name="confirm_field_vale" id="confirm_field_vale" value="">
				  
				<button name="cancel" type="button" class="btn-normal btn btn-default custombtn " data-dismiss="modal" >Cancel</button>
				<input type="button" name="ok" value="ok" class="btn-normal btn btn-primary custombtn confirmbtn" data-dismiss="modal"  >		
			</div>
		</div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
  </form>	
</div><!-- /.modal -->
