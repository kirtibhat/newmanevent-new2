<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<!-- Modal -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="custom_popup" class="modal fade homelogin_popup dn">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="medium dt-large modal-title " id="popup_heading"></h4>
      </div>

      <div class="modal-body small">
        <div class="modelinner">
            <div class="control-group mb10 loginbtninput_home">
			  <p class="mb_30 show_custome_msg"></p>

              <div class="clearfix"></div>
              
              <!-- single_btn -->
              <div class="btn_wrapper">
                  <input type="button" class="popup_cancel submitbtn pull-right  medium" value="Close" name="logincancel" data-dismiss="modal">
              </div>

            </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Modal -->
