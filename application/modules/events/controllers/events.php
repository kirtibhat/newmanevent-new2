<?php
/**
 * By this class you can manage event setup section
 * 
 * @author  shailendra tiwari
 * @email   shailendratiwari@cdnsol.com
 * @Hint    In this class all public method name shoule be normal case 
 * and private metho name shoule be camel case 
 * 
 */

class Events extends MX_Controller{
  /**
   * Constructor - Sets Event Class require helper and library other.
   * The constructor can be passed an array of config values
   */
    
    private $tableattendee          = 'invitation_attendee_details';
    private $tablebooker            = 'invitation_booker_details';
    private $tableinvitation        = 'event_invitations';
    private $tablecustomfields      = 'custom_form_fields';
    private $tableinvitationmaster  = 'invitation_master_details';
    private $tableinvitee           = 'event_invitee';
    private $tabledelegatereg       = 'invitation_registration';
    
    function __construct(){
    parent::__construct();
    $this->load->library('session');
    $this->load->library(array('head','template','process_upload'));
    $this->load->library(array('home_template','head'));
    $this->load->model(array('events_model'));
    $this->load->model(array('common_model'));
    $this->load->language(array('event'));
    $this->load->language(array('home'));
    $this->load->language(array('delegate'));
    $this->form_validation->set_error_delimiters('<div class="server_msg_show">', '</div>');
    }
    
    /*
    * @description: This function is used to show event home front page
    * @return     : @void
    */ 

    public function invitation(){
        /* Set gplus & FB url */
        $this->session->set_userdata('referrer_url', '1');
        /*
         * FB login url
         */ 
        $this->load->library('facebook', array(
            'appId'     => FB_APP_ID,
            'secret'    => FB_SECRET,  
        )); 
            
        $userSession = $this->session->userdata('user_data');
        if($userSession == ''){
            $data['login_url']  = $this->facebook->getLoginUrl(array(
                'display'       => 'popup',
                'redirect_uri'  => site_url('home/freeRegistration'), 
                'scope'         => array('email') 
                ));
        }
        /*
         * configration gplus
         */
        $this->load->library('google');  
        $client = new Google_Client();
        $client->setClientId(CLIENT_ID_GPLUS);
        $client->setClientSecret(CLIENT_SECRET_GPLUS);
        $client->setRedirectUri(REDIRECT_URI_GPLUS);
        $client->addScope("email");
        $client->addScope("profile");
        $service = new Google_Service_Oauth2($client);
        
        if ($this->input->get('code')) {
            $client->authenticate($this->input->get('code'));
            $this->session->set_userdata('access_token',$client->getAccessToken());
            header('Location: ' . filter_var(REDIRECT_URI_GPLUS, FILTER_SANITIZE_URL));
            exit;
        }

        if ($this->session->userdata('access_token')) {
            $client->setAccessToken($this->session->userdata('access_token'));
        } else {
            $authUrl = $client->createAuthUrl();
        }
        
        if (isset($authUrl)){ 
            $data['authUrlGplus'] = $authUrl;
        }
        
        /* End social media configration */
        
        $urlcode        = $this->uri->segment(3);
        $where          = array('event_url' => $urlcode);
        $event_details  = $this->events_model->getDataFromTable('event',$where,'id');
        
        if( count($event_details) > 0 && !empty($event_details) ) {
            $event_details      = $event_details[0];
            $eventId            = $event_details->id;
            if($eventId!=0) {
                $data['eventId'] = $eventId;
                $this->session->set_userdata("eventId",$eventId);
                /* get event details from event table */
                $andwhere  = array('is_theme_used' => '1');
                $data['eventdetails'] = $this->events_model->geteventdetailswiththeme($eventId,$andwhere);
                
                
                if(empty($urlcode) || empty($data['eventdetails'])) {
                    redirect(base_url());
                }
                
                if($data['eventdetails']->is_approved!=1 || $data['eventdetails']->is_theme_used!=1) {
                    $status = 2;
                    redirect(base_url().'events/error_page/'.$status.'/'.$eventId);
                }
                
                if($data['eventdetails']->is_cancelled!=1){
                    //$data['eventdetails']       = $this->events_model->geteventdetails($eventId,$userId=0);
                    /* get event theme fonts list */
                    $data['fontsList']          = $this->events_model->geteventthemesfonts();
                    $whereinvitation            = array( 'event_id'=> $eventId );
                    $data['eventinvitations']   = $this->common_model->getDataFromTabel($this->tableinvitation,'*',$whereinvitation,'','invitations_order','ASC');
                    //echo '<pre>';
                    //print_r($data['eventinvitations']);die;
                    
                    $data['eventinvitationstypes'] = $data['eventinvitations'];
                    $whereSocialMedia           = array('event_id' => $eventId);
                    $data['eventSocialMedia']   = $this->common_model->getDataFromTabel('event_social_media','*',$whereSocialMedia);
                    $data['eventSocialMediaCustom']   = $this->common_model->getDataFromTabel('event_social_media_custom','*',$whereSocialMedia);
                    /* get event theme data from event_theme table */
                    $whereEvent                 = array('is_theme_used' => '1' ,'event_id' => $eventId );
                    $data['usedThemesDetails']  = $this->common_model->getDataFromTabel('event_themes_data', '*', $whereEvent);
                    $this->home_template->load('home_template','form_front_home',$data,TRUE);
                }else{
                    $status = 1;
                    redirect(base_url().'events/error_page/'.$status.'/'.$eventId);
                }
            }else {
                redirect('/');
            }
        }else {
            redirect('/');
        }
    } 
    
    /*
     * This function is used to send contact details to Admin
     * @param admin email
     * subjcet
     */ 
    public function contactus(){
        
        $eventId = currentEventId();
        if(!is_numeric($eventId)) {
            if(!empty($eventId)) {
                $eventId = decode($eventId);
            }else {
                $eventId = $this->session->userdata("eventId");
            }
        }
    
        if($this->input->post()) {
            $postdata = $this->input->post();
            /* Send email notification to admin */
            $this->sendNotificationToAdmin($postdata);
            $msgArray = array('msg'=> 'Query sent to admin successfully','is_success'=>'true');
            set_global_messages($msgArray);
            redirect('events/contactus');
        }else {
            $data = array();
            $data['eventId'] = $eventId;
            $andwhere  = array('is_theme_used' => '1');
            $data['eventdetails'] = $this->events_model->geteventdetailswiththeme($eventId,$andwhere);
            
            $this->home_template->load('home_template','form_contact',$data,TRUE);
        }
        
    } 
    /*
     * This function is used to create email HTML
     */ 
    public function sendNotificationToAdmin($postdata){
        $contact_topic = $postdata['contact_topic'];
        if($contact_topic==1){
            $contact_topic ='Refunds';
        }
        if($contact_topic==2){
            $contact_topic ='Support';
        }
        
        $title = $postdata['title'];
        $firstName = $postdata['firstName'];
        $lastName = $postdata['lastName'];
        $contactEmail = $postdata['contactEmail'];
        $contactPhone = $postdata['contactPhone'];
        $contactOrganisation = $postdata['contactOrganisation'];
        $contactMessage = $postdata['contactMessage'];
        
        
        /* Sendgrid */
        $apiKey = ADMINNEWMAN_APIKEY; //:eventurl
        $this->load->helper('sendgrid');
        $admin_templateId = '1786ea7f-d242-4200-a21b-bb1ba533f806'; //
        
        $paramArray = array(
        'sub' => array(':title'=>array($title),':topic'=>array($contact_topic),':firstname'=>array($firstName),':lastname'=>array($lastName),':email'=>array($contactEmail),':phone'=>array($contactPhone),':organisation'=>array($contactOrganisation),':message' => array($contactMessage)),
        'filters' => array('templates' => array('settings' => array('enable' => 1, 'template_id' => $admin_templateId)))
        );
        $tempSubject = '.';
        $templateBody = '.';
        $toId = ADMIN_EMAIL;
        sendgrid_mail_template($admin_templateId,$tempSubject,$templateBody,$toId,$paramArray,$apiKey,eventFromEmail,$filename,$fpath);
        
        //~ 
        //~ 
        //~ 
        //~ /* Load Email Library */
        //~ $this->load->library('email'); 
        //~ $this->email->from( FROM_EMAIL, FROM_NAME ); // variable define in config  
        //~ $subject_admin = 'Contact Support';
        //~ $subject_user  = 'Contact Us';
        //~ /* send email notification to admin */
        //~ $this->email->to('shailendratiwari@cdnsol.com');
        //~ $this->email->subject($subject_admin);
        //~ 
        //~ $template_to_admin = $this->load->view('email_template_admin',$postdata,true);
        //~ print_r($template_to_admin);die;
        //~ $this->email->message($template_to_admin);  
        //~ $this->email->send();
    }
    
    /*
     * This function is used to view terms and condition page
     * return void
     */ 
    public function termsncondition(){
        $data = array();
        $eventId = currentEventId();

        if(!is_numeric($eventId)) {
            if(!empty($eventId)) {
                $eventId = decode($eventId);
            }else {
                $eventId = $this->session->userdata("eventId");
            }
        }
         
        $data['eventId'] = $eventId;
        $andwhere  = array('is_theme_used' => '1');
        $data['eventdetails'] = $this->events_model->geteventdetailswiththeme($eventId,$andwhere);
        $this->home_template->load('home_template','form_termsncondition',$data,TRUE);
    } 
    /*
     * This function is used to show invitation forms for user
     * @param event_id
     */ 
    public function manageinvitation(){
        $data = array();
        $eventId = $this->uri->segment(3);
        if(!is_numeric($eventId)) {
            $eventId = decode($eventId);
        } 
        /* Get login user data */
        $loginUserId = isLoginUser();
        $whereConditionForUser      = array('id' => $loginUserId);
        $data['userDetails']   = $this->common_model->getDataFromTabel('user','*',$whereConditionForUser);
        /*********End***********/  
        $data['eventId'] = $eventId;
        $whereConditionForDietary      = array('event_id' => $eventId);
        $data['dietaryRequirements']   = $this->common_model->getDataFromTabel('event_dietary','*',$whereConditionForDietary);
        foreach($data['dietaryRequirements'] as $value) {
            $data['dietaryReq'][] = $value->dietary_name;
        }
        $data['dietaryReq']       = json_encode($data['dietaryReq']);
        
        $bookerWhereCondition = array('event_id' => $eventId,'registrant_id'=>'0','form_id'=>'5');
        $data['bookerformfielddata'] = $this->common_model->getDataFromTabel($this->tablecustomfields,'*',$bookerWhereCondition);
        
        /* Get invitations data */
        $whereConditionInvitation          = array( 'event_id'=> $eventId );
        $data['eventInvitations'] = $this->common_model->getDataFromTabel($this->tableinvitation,'*',$whereConditionInvitation,'','invitations_order','ASC');
        
        if(!empty($data['eventInvitations'])) {
            foreach($data['eventInvitations'] as $inv_data) {
                $whereConInvitation          = array( 'event_id'=> $inv_data->event_id, 'registrant_id' => $inv_data->id, 'form_id' => '4' );
                $data['invitationTypesFormFields'][$inv_data->id]  =  $this->common_model->getDataFromTabel($this->tablecustomfields,'*',$whereConInvitation);
            }
        }
        
        $andwhere  = array('is_theme_used' => '1');
        $data['eventdetails'] = $this->events_model->geteventdetailswiththeme($eventId,$andwhere);
        
        $this->template->load('template','form_invitation_details',$data,TRUE);
    }
    
    public function saveInvitationFormDetails() {
        
        $is_ajax_request = $this->input->is_ajax_request();
        //$this->debug();
        
        $loginUserId = isLoginUser();    
        $invitation_types_id        = $this->input->post('invitation_types');
        $eventId                    = $this->input->post('eventId');
        /* Get total invitations limit & waitig list status (is yes/No) */
        $whereConditionInvitation   = array( 'event_id'=> $eventId, 'id' => $invitation_types_id );
        $eventInvitations           = $this->common_model->getDataFromTabel($this->tableinvitation,'*',$whereConditionInvitation);
        $data['eventInvitations']   = $eventInvitations;
        if( count($eventInvitations) > 0 ) {
            $eventInvitations       = $eventInvitations[0];
            $invitationLimit        = $eventInvitations->invitation_limit;
            $allow_waiting_list     = $eventInvitations->allow_waiting_list;
        }else {
            $invitationLimit        = 0 ;
        }
        /* get used invitation limit from added attendee */
        $whereCondition             = array('invitation_type_id' => $invitation_types_id);
        $usedInvitationLimit        = $this->common_model->getDataFromTabel($this->tablebooker,'*',$whereCondition);
        if( count($usedInvitationLimit) > 0 ) {
            $usedquantity = 0;
            if($usedInvitationLimit) {
                foreach($usedInvitationLimit as $ulimit) {
                    $usedquantity       += $ulimit->quantity;
                }
            }
            
        }else {
            $usedquantity = 0;
        }
        
        if($loginUserId==false || $loginUserId==0 || $loginUserId==null) {
            $loginUserId = 0;
        }
        
        if ($this->input->post())
        {
            $arrayVal     =  $this->input->post('fieldid');
            $field_qty    =  $this->input->post('field_qty');
            /* Check waitlist is start */
            $isWaitList    =  $this->input->post('isWaitList');
            
            /* Check post quantity shold not greater from total qty */
            if($isWaitList!=1) {
                $postQty      = $field_qty;
                $postUsedQty  = $postQty + $usedquantity; 
                if(!empty($invitationLimit)) {
                    if($postUsedQty > $invitationLimit) {
                        $remaining      = $postUsedQty - $invitationLimit; 
                        $remaining      = $postQty - $remaining;
                        if($remaining!=0) {
                            $this->form_validation->set_rules('at1', 'You can add only '.$remaining.' attendee for this invitation type', 'callback_custom_error_set');
                            $errors = 'You can add only '.$remaining.' attendee for this invitation type';
                        }else {
                            /* Check wait list*/
                            if($allow_waiting_list!=1) {
                                $this->form_validation->set_rules('at2', 'You need to allow waitlist. If you need to add more attendee', 'callback_custom_error_set');
                                $errors = 'You need to allow waitlist. If you need to add more attendee';
                             
                            }else {
                                $this->form_validation->set_rules('at3', 'You can add as Waitlist', 'callback_custom_error_set');
                                $errors = 'You can add as Waitlist';
                            }
                        }
                    }
                }
                
                if(empty($invitationLimit)) {
                    $this->form_validation->set_rules('at4', 'You can not add for this invitation because invitation not contains limit', 'callback_custom_error_set');
                    $errors = 'You can not add for this invitation because invitation not contains limit';
                }
            }
            
            if ($this->form_validation->run())
            {
                if($is_ajax_request){
                    echo json_message('msg',$errors,'is_success','false');
                }
                die;
            }
            
           
            //update field value
            $arrayfield_dietary  =  $this->input->post('field_dietary');
            
            $arrayFieldVal  =  $this->input->post();
            /* Insert data for Booker person */
            /* End booker */
            $insertDataBooker['event_id']           = $eventId;
            $insertDataBooker['login_id']           = $loginUserId;
            $insertDataBooker['invitation_type_id'] = $arrayFieldVal['invitation_types'];
            $insertDataBooker['quantity']           = $arrayFieldVal['field_qty'];
            $insertDataBooker['unique_id']          = 'NEWMAN-'.randomnumber(4);
            $insertDataBooker['isWaitList']         = $isWaitList;
            $bookerId = $this->common_model->addDataIntoTabel($this->tablebooker,$insertDataBooker);
            $qrPath   = $this->qrCode($eventId,$bookerId);
            $barCode  = $this->getBarCode($bookerId);
            $whereCon = array('id' => $bookerId);
            $dataQr['qrPath']       = $qrPath; 
            $dataQr['barcodePath']  = $barCode; 
            $this->events_model->updateDataFromTable($this->tablebooker,$dataQr,$whereCon);
            /* Insert data for Attendee users */
           
            
            $loop = 1;
            foreach($arrayVal as $fieldid){
                $filedRowId = 'field_'.$fieldid;
                $filedBookerId = 'booker_'.$fieldid;
                $filedBookerImg = 'booker_image_'.$fieldid;
                $filedAttendeeImg = 'attendee_image_'.$fieldid;
                $attendLoop =1;
                if (array_key_exists($filedRowId, $arrayFieldVal)) {
                   $field_data = $arrayFieldVal[$filedRowId];
                   $order = 1;
                   $formid = 4;
                }else if (array_key_exists($filedRowId.'_'.$loop, $arrayFieldVal)) {
                    $field_data = $arrayFieldVal[$filedRowId.'_'.$loop];
                    $loop++;
                    $order = 1;
                    $formid = 4;
                }else if (array_key_exists($filedBookerId, $arrayFieldVal)) {
                    $field_data = $arrayFieldVal[$filedBookerId];
                    $order = 0;
                    $formid = 5;
                }else if (array_key_exists($filedBookerImg, $arrayFieldVal)) {
                    
                    if(!empty($_FILES['bookerimage']['tmp_name'])) {
                        $field_data[] = $_FILES['bookerimage']['name'];
                        $filetmp_name = $_FILES['bookerimage']['tmp_name'];
                        $order = 0;
                        $formid = 5;
                        $fileData = array();
                        $fileData = $_FILES['bookerimage'];
                        $fieldName = 'bookerimage';
                        $this->uploadFileForInvitation($fileData,$fieldName);
                    }else {
                        $field_data = array();
                    }
                }else if (array_key_exists($filedAttendeeImg.'_'.$attendLoop, $arrayFieldVal)) {
                    
                    if(!empty($_FILES[$filedAttendeeImg.'_'.$attendLoop]['tmp_name'])) {    
                        for($attendLoop = 1;$attendLoop <= $field_qty;$attendLoop++){
                                $field_data[] = $_FILES[$filedAttendeeImg.'_'.$attendLoop]['name'];
                                $filetmp_name = $_FILES[$filedAttendeeImg.'_'.$attendLoop]['tmp_name'];
                                $order = 1;
                                $formid = 4;
                                $fileData = array();
                                $fileData = $_FILES[$filedAttendeeImg.'_'.$attendLoop];
                                $fieldName = $filedAttendeeImg.'_'.$attendLoop;
                                $this->uploadFileForInvitation($fileData,$fieldName);
                            }
                    }else {
                        $field_data = array();
                    }
                }else {
                    $field_data = array();
                }
                if(!empty($field_data)) {
                    $counter = $order;
                    foreach($field_data as $fieldValue) {
                        if(!empty($fieldValue)){
                            $insertDataAttendee['field_value']  = $fieldValue;
                            $insertDataAttendee['field_id']     = $fieldid;
                            $insertDataAttendee['booker_id']    = $bookerId;
                            $insertDataAttendee['form_id']      = $formid;
                            $insertDataAttendee['field_order']  = $counter;
                            if($counter!=0) {
                                $insertDataAttendee['dietary']  = $arrayfield_dietary[$counter-1];
                            }
                            $counter++;
                            $responce = $this->common_model->addDataIntoTabel($this->tableattendee,$insertDataAttendee);
                        }            
                    }
                }
            }
            
            if($is_ajax_request){
                echo json_encode(array('msg'=> 'Saved successfully!','is_success'=>'true','url'=>'events/manageinvitation/'.$eventId,'bookerId'=>$bookerId));
              }else{
                $msgArray = array('msg'=>$msg,'is_success'=>'true');
                set_global_messages($msgArray);
                //redirect('event/eventdetails/'.encode($eventId));
              }
            
           /* $bookerId is "invitation_booker_details" table id 
            * & $field_qty is total selected quantity in form 
            **/
           //$this->attendeeConfirmation($bookerId,$field_qty);
        
        }
    }
    
    /*
     * Edit or Modify attendee details
     * @param
     * bookerId
     */  
     
    public function modifyAttendee(){
        $bookerId                       = $this->input->post('bookerId');
        $quantity                       = $this->input->post('quantity');
        $eventId                        = $this->input->post('eventId');
        $registration_type              = $this->input->post('registration_type');
        $field_order                    = $this->input->post('field_order');
        
        $bookerWhereCondition1          = array('booker_id' => $bookerId, 'form_id' => 5);
        $fieldIdbooker = $this->common_model->getDataFromTabel($this->tableattendee,'id,field_id,field_value',$bookerWhereCondition1);
        for($counter = 1; $counter <= $quantity; $counter++) {
            $bookerWhereCondition2      = array('booker_id' => $bookerId, 'form_id' => 4, 'field_order' => $counter);
            $fieldIdsattendee[] = $this->common_model->getDataFromTabel($this->tableattendee,'id,field_id,field_value,dietary,field_order',$bookerWhereCondition2);
        }
        $data['fieldIdsbooker']         = $fieldIdbooker;
        $data['fieldIdsattendee']       = $fieldIdsattendee;
        $data['quantity']               = $quantity;
        $data['bookerId']               = $bookerId;
        $data['eventId']                = $eventId;
        
        $data['registration_type']      = (!empty($registration_type)) ? $registration_type : 1;
        $data['field_order']            = (!empty($field_order)) ? $field_order : 0;
        
        $andwhere  = array('is_theme_used' => '1');
        $data['eventdetails'] = $this->events_model->geteventdetailswiththeme($eventId,$andwhere);
        
         /* get used invitation limit from added attendee */
        //$whereCondition             = array('invitation_type_id' => $invitation_types_id);
        $whereCondition                 = array('id' => $bookerId);
        $bookerData                     = $this->common_model->getDataFromTabel($this->tablebooker,'*',$whereCondition);
        $bookerData                     = $bookerData[0];
        $invitation_type_id             = $bookerData->invitation_type_id;
         /* Get invitations data */
        $whereConditionInvitation          = array( 'event_id'=> $eventId, 'id' => $invitation_type_id );
        $data['eventInvitations'] = $this->common_model->getDataFromTabel($this->tableinvitation,'*',$whereConditionInvitation,'','invitations_order','ASC');
        
        $whereConditionForDietary      = array('event_id' => $eventId);
        $data['dietaryRequirements']   = $this->common_model->getDataFromTabel('event_dietary','*',$whereConditionForDietary);
        foreach($data['dietaryRequirements'] as $value) {
            $data['dietaryReq'][] = $value->dietary_name;
        }
        
        $this->template->load('template','form_update_attendee',$data,TRUE);
    } 
    /*
     * display resent saved attendee details
     * @param
     * bookerId, quantity
     */
    public function attendeeConfirmation($bookerId=null,$quantity=null,$eventId=null){
        
        $fieldStatus ='';
        
        if($bookerId==null || $quantity==null) {
            $bookerId = $this->input->post('bookerId');
            $quantity = $this->input->post('bookerQuantity');
            $eventId = $this->input->post('event_id');
        }else {
            $fieldStatus = 1;
        }
        
        /*  get checked ids array   */
        $checkedIds     = $this->input->post('checkedIds');
        $checkedIdsArr  = '';
        if(!empty($checkedIds)) {
            $checkedIdsArr = explode(',',$checkedIds);
        }
        
        $checkedIdsCount   = count($checkedIdsArr);
        $data['checkedIdsCount'] = $checkedIdsCount;
        $data['checkedIdsArr'] = $checkedIdsArr;
        
        $andwhere       = array('is_theme_used' => '1');
        $eventdetails   = $this->events_model->geteventdetailswiththeme($eventId,$andwhere);
        
        $data['bookerId'] = $bookerId;
        $data['quantity'] = $quantity;
        $data['eventId']  = $eventId;
        $data['eventdetails']  = $eventdetails;
        
        $bookerCondition           = array('id' => $bookerId);
        $data['bookerTableDetails']     = $this->common_model->getDataFromTabel($this->tablebooker,'*',$bookerCondition);
        
        $bookerWhereCondition           = array('booker_id' => $bookerId, 'form_id'=>'5');
        $data['bookerDetails']          = $this->common_model->getDataFromTabel($this->tableattendee,'*',$bookerWhereCondition);
      
        
        for($counter = 1; $counter <= $quantity; $counter++) {
            $attendeeWhereCondition     = array('booker_id' => $bookerId, 'form_id'=>'4', 'field_order' => $counter);
            $data['attendeeDetails'][]  = $this->common_model->getDataFromTabel($this->tableattendee,'*',$attendeeWhereCondition);
        }
        
        if(!empty($bookerId)) {
            $whereCon = array('id' => $bookerId);
            $upData['isRegister'] = '1'; 
            $this->events_model->updateDataFromTable($this->tablebooker,$upData,$whereCon);
        }
        
        //download pdf
        //~ echo '<pre>';
        //~ print_r($data);die;
       
        if($fieldStatus=='') {
            if($data['bookerTableDetails'][0]->isWaitList==0){
                $data['invitationStatus'] = '1';
                $this->getPdf($data);
            }
        }
        //$this->getPdf($data);
        $this->home_template->load('home_template','form_attendee_confirmation',$data,TRUE);
    } 
    
    /*
     * This function is used to create invitation form HTML
     * @param eventId, invitationTypeId, quantity
     * return form fields
     */ 
    public function getInvitationHtml() {
        /* get parameter from ajax call page form_invitation_details */
        $postdata = $this->input->post();
        $eventId            = $postdata['event_id'];
        $invitationTypeId   = $postdata['invitationTypeId'];
        $quantity           = $postdata['quantity'];
        $form_id            = '4';
        
        $whereConditionForDietary      = array('event_id' => $eventId);
        $data['dietaryRequirements']   = $this->common_model->getDataFromTabel('event_dietary','*',$whereConditionForDietary);
        foreach($data['dietaryRequirements'] as $value) {
            $data['dietaryReq'][] = $value->dietary_name;
        }
        
        if( ( $quantity > 0 ) &&  !empty($eventId) && !empty($invitationTypeId) ) {
            $data['eventId']            = $eventId;
            $data['quantity']           = $quantity;
            $data['invitationTypeId']   = $invitationTypeId;
            
            /* Get invitations data */
            $whereConditionInvitation          = array( 'event_id'=> $eventId );
            $data['eventInvitations'] = $this->common_model->getDataFromTabel($this->tableinvitation,'*',$whereConditionInvitation,'','invitations_order','ASC');
            
            $whereConInvitation = array( 'event_id'=> $eventId, 'registrant_id' => $invitationTypeId, 'form_id' => $form_id );
            $data['invitationTypesFormFieldsData']  =  $this->common_model->getDataFromTabel($this->tablecustomfields,'*',$whereConInvitation);
            
            $dataHtml = $this->load->view('form_invitation_types_form_fields',$data,true);
            echo $dataHtml;die;
            
        }else {
            echo 'false';die;
        }
    }
    
    /*
     * Update Attendee & booker detais
     */
     public function updateAttendee(){
         
        $arrayVal     =  $this->input->post('fieldid');
        
        if(!empty($arrayVal) && count($arrayVal) > 0) {
            $this->AddNewAttendee();
        }
        
        $quantity       = $this->input->post('quantity');
        $bookerId       = $this->input->post('bookerId');
        $eventId        = $this->input->post('eventId');
        $field_qty      = $this->input->post('field_qty');
        
        $registration_type      = $this->input->post('registration_type');
        $field_order            = $this->input->post('field_order');
        
        $bookerIdWithValue = $this->input->post('bookeridvalue');
        foreach($bookerIdWithValue as $key => $fieldValue) {
            $updateData['field_value'] = $fieldValue;
            $where = array('id' => $key);
            //call common model for update data
            $this->common_model->updateDataFromTabel($this->tableattendee,$updateData,$where);
        }
        $attendeeValueWithIds = $this->input->post('attendee_value');
        foreach($attendeeValueWithIds as $key1 => $fieldValue1) {
            $updateData1['field_value'] = $fieldValue1;
            $where1 = array('id' => $key1);
            //call common model for update data
            $this->common_model->updateDataFromTabel($this->tableattendee,$updateData1,$where1);
        }
        
        /* Update dietary details */        
        $arrdietary     =  $this->input->post('field_dietary');
        for($counter = 0; $counter <=count($arrdietary) ; $counter++) {
            $dietaryVal = $arrdietary[$counter];
            $where = array('booker_id' => $bookerId,'form_id' => 4,'field_order' => $counter+1);
            $updateDietary['dietary'] = $dietaryVal;
            $this->common_model->updateDataFromTabel($this->tableattendee,$updateDietary,$where);
        }
        
        $andwhere       = array('is_theme_used' => '1');
        $eventdetails   = $this->events_model->geteventdetailswiththeme($eventId,$andwhere);
        
        $bookerCondition           = array('id' => $bookerId);
        $data['bookerTableDetails']     = $this->common_model->getDataFromTabel($this->tablebooker,'*',$bookerCondition);
        
        $bookerWhereCondition           = array('booker_id' => $bookerId, 'form_id'=>'5');
        $data['bookerDetails']          = $this->common_model->getDataFromTabel($this->tableattendee,'*',$bookerWhereCondition);
        for($counter = 1; $counter <= $quantity; $counter++) {
            $attendeeWhereCondition     = array('booker_id' => $bookerId, 'form_id'=>'4', 'field_order' => $counter);
            $data['attendeeDetails'][]  = $this->common_model->getDataFromTabel($this->tableattendee,'*',$attendeeWhereCondition);
        }
        if(!empty($arrayVal) && count($arrayVal) > 0) {
            $quantity = $quantity + $field_qty;
        }
        $data['bookerId']       = $bookerId;
        $data['quantity']       = $quantity;
        $data['eventId']        = $eventId;
        $data['eventdetails']   = $eventdetails;
        
        $checkedIdsArr = array($field_order);
        $checkedIdsCount   = count($checkedIdsArr);
        $data['checkedIdsCount'] = $checkedIdsCount;
        $data['checkedIdsArr'] = $checkedIdsArr;
        $data['registration_type'] = $registration_type;
        $data['field_order'] = $field_order;
        
        $data['invitationStatus'] = '0';
        $this->getPdf($data);

        redirect('events/attendeeConfirmation/'.$bookerId.'/'.$quantity.'/'.$eventId.'?q=true&rtype='.$registration_type.'&field_order='.$field_order);
        
     }
    
    /*
     * QR Code
     * Library in library ciqrcode for QR code
     */
    public function qrCode($eventId,$bookerId){
        
        $eventdetails  = $this->events_model->geteventdetails($eventId,$userId=0);
        
        $eventTitle =   (!empty($eventdetails->event_title)) ? $eventdetails->event_title : '';  
        $startDate  =   date('l jS F Y',strtotime($eventdetails->starttime));
        $endDate    =   date('l jS F Y',strtotime($eventdetails->endtime));
        $eventVenue =   (!empty($eventdetails->event_venue)) ? $eventdetails->event_venue : '';  
        
        $this->load->library('ciqrcode');
        //Added files Ciqrcode.php  added qrcode dir
        //header("Content-Type: image/png");
        $imageName = randomnumber(5);
        $params['data']     = $eventTitle.' '.' DATE & TIME : '.$startDate.' to '.$endDate.'  LOCATION : '.$eventVenue;
        $params['level']    = 'H';
        $params['size']     = 3; 
        $params['savename'] = './media/qrcode/qr_'.$bookerId.'.png'; //Image saved directory
        $imagePath = base_url().'media/qrcode/qr_'.$bookerId.'.png';
        $this->ciqrcode->generate($params);
        return $imagePath;
    
    }
    
     /*
     * BAR Code
     * Library in library zend & Ean13 for same
     * dir under media/barcode
     */
     
     public function getBarCode($bookerId){
        
        $this->load->library('zend');
		$this->zend->load('Zend/Barcode');
		// prepare barcode from zend library
		
        //include_once('Zend/Barcode.php');
        //include_once('Zend/Barcode/Object/Ean13.php');
        $barcode = rand(100000000000,111110000000);
        
        $file = Zend_Barcode::draw('ean13', 'image', array('text' => $barcode), array());
        $barcode = $this->get_barcode_digits($barcode);
        //$file = Zend_Barcode::draw('ean13', 'image', array('text' => $barcode), array());
        //$barcode_text = Zend_Barcode::draw('ean13', 'image', array('text' => $barcode), array());
        // set barcode image path
        //define('UPLOAD_DIR', 'uploads/');
        $dir_path = './media/barcode/';
        // set barcode image name
        //$image_name = time().$barcode;
        $image_name = $barcode.'.png';
        // create image from barcode
        ImagePNG($file, $dir_path.$image_name);
        
        $degrees = -90;  //change this to be whatever degree of rotation you want
        header('Content-type: image/png');
        $filename = $dir_path.$image_name;  //this is the original file
        $source = imagecreatefrompng($filename) or notfound();
        $rotate = imagerotate($source,$degrees,0);
        imagepng($rotate,$filename); //save the new image
        imagedestroy($source); //free up the memory
        imagedestroy($rotate);  //free up the memory

        ImageDestroy($file);
        return $image_name;
     }
     
     public function get_barcode_digits($barcode='') {
		
		//first change digits to a string so that we can access individual numbers
		$digits =(string)$barcode;
		// 1. Add the values of the digits in the even-numbered positions: 2, 4, 6, etc.
		$even_sum = $digits{1} + $digits{3} + $digits{5} + $digits{7} + $digits{9} + $digits{11};
		// 2. Multiply this result by 3.
		$even_sum_three = $even_sum * 3;
		// 3. Add the values of the digits in the odd-numbered positions: 1, 3, 5, etc.
		$odd_sum = $digits{0} + $digits{2} + $digits{4} + $digits{6} + $digits{8} + $digits{10};
		// 4. Sum the results of steps 2 and 3.
		$total_sum = $even_sum_three + $odd_sum;
		// 5. The check character is the smallest number which, when added to the result in step 4,  produces a multiple of 10.
		$next_ten = (ceil($total_sum/10))*10;
		$check_digit = $next_ten - $total_sum;
		$barcode_digits = $digits.$check_digit;
		// Count of barcode text digits
		$barcode_digit_count = strlen($barcode_digits);
		if($barcode_digit_count < 13) {
			$remain_text = 13 - $barcode_digit_count;
			for($i=0;$i<$remain_text;$i++) {
				$barcode_digits = '0'.$barcode_digits;
			}
		}
		return $barcode_digits;
	}
    
     /* Method for upload file
     * invitation attendee & booker images
     * @param field name & files data
     */
    public function uploadFileForInvitation($fileData,$fieldName) {
        $config['upload_path']      = './media/invitationImages/';
		$config['allowed_types']    = 'gif|jpg|png';
		//$config['max_size']	        = '100';
		//$config['max_width']        = '1024';
		//$config['max_height']       = '768';

		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload($fieldName))
		{
			$error = array('error' => $this->upload->display_errors());
            print_r($error);die;
		}
		else
		{
			$data = $this->upload->data();
		}
    
    }  
    
    public function isAllowWaitList() {
        $eventId                = $this->input->post('eventId');
        $invitation_types_id    = $this->input->post('invitation_types_id');
        
         /* Get total invitations limit & waitig list status (is yes/No) */
        $whereConditionInvitation   = array( 'event_id'=> $eventId, 'id' => $invitation_types_id );
        $eventInvitations           = $this->common_model->getDataFromTabel($this->tableinvitation,'*',$whereConditionInvitation);
        $data['eventInvitations']   = $eventInvitations;

        if( count($eventInvitations) > 0 ) {
            $eventInvitations       = $eventInvitations[0];
            $invitationLimit        = $eventInvitations->invitation_limit;
            $allow_waiting_list     = $eventInvitations->allow_waiting_list;
            $isPassword             = $eventInvitations->password;
        }else {
            $isPassword='';
            $allow_waiting_list     = 0;
            $invitationLimit        = 0 ;
        }
        //echo $isPassword;die;
        //print_r($eventInvitations);die;
        
        /* get used invitation limit from added attendee */
        $whereCondition             = array('invitation_type_id' => $invitation_types_id, 'isWaitList !=' => 1);
        $usedInvitationLimit        = $this->common_model->getDataFromTabel($this->tablebooker,'*',$whereCondition);
        
        if( count($usedInvitationLimit) > 0 ) {
            $usedquantity = 0;
            if($usedInvitationLimit) {
                foreach($usedInvitationLimit as $ulimit) {
                    $usedquantity       += $ulimit->quantity;
                }
            }
            
        }else {
            $usedquantity = 0;
        }
        
        /* Check post quantity shold not greater from total qty */
        if(!empty($invitationLimit)) {
            if($usedquantity == $invitationLimit) {
                if($allow_waiting_list) {
                    if(!empty($isPassword)) {
                        echo json_encode(array('status'=> 'allow_waitlist','is_pass'=>'true'));
                    }else {
                        echo json_encode(array('status'=> 'allow_waitlist','is_pass'=>'false'));
                    }
                    //echo 'allow_waitlist';die;
                }else {
                    if(!empty($isPassword)) {
                        echo json_encode(array('status'=> 'false','is_pass'=>'true'));
                    }else {
                        echo json_encode(array('status'=> 'false','is_pass'=>'false'));
                    }
                    //echo 'false';die;
                }
            }else {
                 if(!empty($isPassword)) {
                        echo json_encode(array('status'=> 'allow','is_pass'=>'true'));
                    }else {
                        echo json_encode(array('status'=> 'allow','is_pass'=>'false'));
                    }
                //echo 'allow';die;
            }
        }
        
        if(empty($invitationLimit)) {
            echo json_encode(array('status'=> 'nolimit','is_pass'=>'false'));
            //echo 'nolimit';die;
        }
    }
    
    
    public function getFbFriends() {
        /*
         * FB intregation
         */ 
        $this->load->library('facebook', array(
            'appId'     => FB_APP_ID,
            'secret'    => FB_SECRET,  
        )); 
    }
    

    
    public function debug($data=array()){
        
        $eventId = 232;
        $whereCondition = array('event_id' => $eventId);
        $eventInvitee   = $this->common_model->getDataFromTabel($this->tableinvitee,'*',$whereCondition);
        echo '<pre>';
        print_r($eventInvitee);die;
        if(!empty($eventInvitee)) {
            foreach($eventInvitee as $invitee) {
                $user_id = @$invitee->user_id;
                if($user_id!='' && $user_id!=null && $user_id!=0) {
                    $whereConditionUser = array('id',$user_id);
                    $users   = $this->common_model->getDataFromTabel($this->tableinvitee,'*',$whereCondition);
                }
            }
        }
        
        $this->load->library('facebook', array(
            'appId'     => FB_APP_ID,
            'secret'    => FB_SECRET,  
        )); 
        
        $data['user_profile'] = $this->facebook->api('/me?fields=email,first_name,last_name');
        $data['picture'] = $this->facebook->api("/me/picture","GET", array ('redirect' => false,'type' => 'large' ) );
          
        
        echo '<pre>';
        print_r($_POST);
        print_r($data);
        die;
    }
    
    
     /*
     * This function is used to send contact details to Admin
     * @param admin email
     * subjcet
     */ 
    public function addEmailInvitation($eventId=null) {
        
        if($this->input->post()) {
            
            $is_ajax_request    = $this->input->is_ajax_request();
            $postdata           = $this->input->post();
            $subjectLine        = $postdata['subjectLine'];
            $isPassword         = $postdata['isPassword'];
            $selInvitationType  = $postdata['selInvitationType'];
            $nameOfSender       = $postdata['nameOfSender'];
        
           
            if(!empty($postdata)) {
                
                if(!empty($postdata['eventId'])) {
                    $eventId = $postdata['eventId'];
                }else {
                    $eventId = $this->session->userdata("eventId");
                }
                
                $eventUrl     = $this->getEventUrl($eventId);
                $emailData      = '';
                $inviteeemails  = '';
                if($postdata['isImport']=='1') {
                    
                    $inviteeemails  = $postdata['inviteeemails'];
                    if(!empty($inviteeemails)) {
                        $emailData      = implode(',',$inviteeemails);
                    }
                }else {
                    $emails = $postdata['emails'];
                    $emails = str_replace(',',';',$emails);
                    $emails = explode(';',$emails);
                    $emailData      = implode(',',$emails);
                }
                
                if(empty($emailData)) {
                $this->form_validation->set_rules('at9', 'You need to add atleast one email in your invitation', 'callback_custom_error_set');
                $errors = 'You need to add atleast on email in your invitation';
                
                 if ($this->form_validation->run())
                 {
                    echo json_message('msg',$errors,'is_success','false');
                    die;
                 }
                }
                
                $emailData = explode(',',$emailData);
                
                $andwhere  = array('is_theme_used' => '1');
                $eventdetails = $this->events_model->geteventdetailswiththeme($eventId,$andwhere);
                //$eventdetails = $this->events_model->geteventdetails($eventId,$userId=0);

                /* Send email to friends */        
                $this->sendEmailToInvitee($emailData,$subjectLine,$eventdetails,$isPassword,$selInvitationType,$nameOfSender);
                
                /* Send email notification with event link */
                /* Do code here */
            }
            
            if($is_ajax_request){
                echo json_encode(array('msg'=> 'Email sent successfully!','is_success'=>'true','url'=>''));
              }else{
                $msgArray = array('msg'=>$msg,'is_success'=>'true');
                set_global_messages($msgArray);
                //redirect('event/eventdetails/'.encode($eventId));
            }
            
        }else {
            
            $eventId = currentEventId();
            
            if(!is_numeric($eventId)) {
                if(!empty($eventId)) {
                    $eventId = decode($eventId);
                }else {
                    $eventId = $this->session->userdata("eventId");
                }
            }
            
            $data = array();
            $data['alldata'] ='';

            $data['eventId'] = $eventId;
            $andwhere  = array('is_theme_used' => '1');
            $data['eventdetails'] = $this->events_model->geteventdetailswiththeme($eventId,$andwhere);
            
            /* Get invitations data */
            $whereConditionInvitation   = array( 'event_id'=> $eventId );
            $data['eventInvitations']   = $this->common_model->getDataFromTabel($this->tableinvitation,'*',$whereConditionInvitation,'','invitations_order','ASC');
            
            $this->template->load('template','form_email_invitation',$data,TRUE);
        }
        
    } 
    
    
    /*
    * @Description:this functio  handel  the call back url  data from gmail .
    * @access: private
    * @return: void
    * @author: $$
    */
      function GmailContactCallback(){
		  	
                $eventId = $this->session->userdata("eventId");
		  		$post                    = '';
                $data                    = array();
                $getGmaildata            = $this->input->get();
            	$data['auth_code']       = $this->input->get('code');
			    $data['client_id']       = $this->config->item('local_client_id');
				$data['client_secret']  = $this->config->item('local_client_secret');
				$data['redirect_uri']   = $this->config->item('local_redirect_uri');
				$data['max_results']     = $this->config->item('max_results');
                if($this->input->get('code')) {
                    $fields=array(
							'code'         =>  urlencode($data['auth_code']),
							'client_id'    =>  urlencode($data['client_id']),
							'client_secret'=>  urlencode($data['client_secret']),
							'redirect_uri' =>  urlencode($data['redirect_uri']),
							'grant_type'   =>  urlencode('authorization_code')
    						 );
                           
						foreach($fields as $key=>$value){ 
							 $post .= $key.'='.$value.'&';
						}

            			$post    = rtrim($post,'&');
			        $accesstoken = $this->curl_get_GmailToken($post);
                    if($accesstoken==false || $accesstoken=='' || $accesstoken==null) {
                        redirect('events/addEmailInvitation/'.$eventId);
                    }else {
			        $url = 'https://www.google.com/m8/feeds/contacts/default/full?max-results='.$data['max_results'].'&oauth_token='.$accesstoken;
          
                    $xmlresponse = $this->curl_file_get_contents($url);
                
                    if((strlen(stristr($xmlresponse,'Authorization required'))>0) && (strlen(stristr($xmlresponse,'Error '))>0)){
					      $data['flag']        = 0;
					      $data['message']     = 'OOPS !! Something went wrong. Please try after some time.';		
						  $data['result']      = '';
						}else{							
							$data['flag']      = 1;
					        $data['message']   = 'success';
					  		$xml               =  new SimpleXMLElement($xmlresponse);
							$xml->registerXPathNamespace('gd', 'http://schemas.google.com/g/2005');
							$result            =  $xml->xpath('//gd:email');
							$alldata           = array(); 
							foreach ($result as $title) {
							$alldata[] = (string) $title->attributes()->address;
							}							
							$data['alldata']    = $alldata;			
                            $data['eventId']    = $eventId;
                            $andwhere  = array('is_theme_used' => '1');
                            $data['eventdetails'] = $this->events_model->geteventdetailswiththeme($eventId,$andwhere);
                            /* Get invitations data */
                            $whereConditionInvitation   = array( 'event_id'=> $eventId );
                            $data['eventInvitations']   = $this->common_model->getDataFromTabel($this->tableinvitation,'*',$whereConditionInvitation,'','invitations_order','ASC');
                            
                            $this->template->load('template','form_email_invitation',$data,TRUE);		
						}
                    }
            }
                        
			       //return $legalsContactPopup=$this->load->view('callback',$data,true);
			       
       }
              
       function curl_get_GmailToken($post){
                $urlGetToken ='https://accounts.google.com/o/oauth2/token';
                $curl = curl_init();
				curl_setopt($curl,CURLOPT_URL,$urlGetToken);
				curl_setopt($curl,CURLOPT_POST,5);
				curl_setopt($curl,CURLOPT_POSTFIELDS,$post);
				curl_setopt($curl, CURLOPT_RETURNTRANSFER,TRUE);
				curl_setopt($curl, CURLOPT_SSL_VERIFYPEER,0);
				curl_setopt($curl, CURLOPT_SSL_VERIFYHOST,0);
				
				$result = curl_exec($curl);
				curl_close($curl);
        		$response =  json_decode($result);
				$accesstoken = $response->access_token;
                return  $accesstoken ; 
     	}  
     	     
       function curl_file_get_contents($url){
			 
			 $curl = curl_init();
			 $userAgent = 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; .NET CLR 1.1.4322)';
			 
			 curl_setopt($curl,CURLOPT_URL,$url);	//The URL to fetch. This can also be set when initializing a session with curl_init().
			 curl_setopt($curl,CURLOPT_RETURNTRANSFER,TRUE);	//TRUE to return the transfer as a string of the return value of curl_exec() instead of outputting it out directly.
			 curl_setopt($curl,CURLOPT_CONNECTTIMEOUT,5);	//The number of seconds to wait while trying to connect.	
			 
			 curl_setopt($curl, CURLOPT_USERAGENT, $userAgent);	//The contents of the "User-Agent: " header to be used in a HTTP request.
			 curl_setopt($curl, CURLOPT_FOLLOWLOCATION, TRUE);	//To follow any "Location: " header that the server sends as part of the HTTP header.
			 curl_setopt($curl, CURLOPT_AUTOREFERER, TRUE);	//To automatically set the Referer: field in requests where it follows a Location: redirect.
			 curl_setopt($curl, CURLOPT_TIMEOUT, 10);	//The maximum number of seconds to allow cURL functions to execute.
			 curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);	//To stop cURL from verifying the peer's certificate.
			 curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
			 $contents = curl_exec($curl);
			 curl_close($curl);
			 return $contents;
			}
            
            
              /*
     * @description: This function is used to save skills & qualifications data
     * @access: public
     * @return void
     */ 
        public function typeselectoptions() {
	    $val = $this->input->post('val');
	    $eventId = $this->input->post('eventId');
        $this->session->set_userdata("eventId",$eventId);
	    $url = "";
	    //1-yahoo:2-gmail:3-hotmail:4-facebook
	    if(!empty($val)){
			// set the session for contact import	
		 $this->session->set_userdata("sstype",$val);	
        // set default redirect url
          switch ($val) {			 
			case 1:
				$url = "";
				break; 
			case 2:
				$url = 'https://accounts.google.com/o/oauth2/auth?client_id='.$this->config->item('local_client_id').'&redirect_uri='.$this->config->item('local_redirect_uri').'&scope=https://www.google.com/m8/feeds/&response_type=code';
				break; 
			case 3: 
				$url = 'https://login.live.com/oauth20_authorize.srf?client_id='.$this->config->item('Hcdn_client_id').'&scope=wl.signin%20wl.basic%20wl.emails%20wl.contacts_emails&response_type=code&redirect_uri='.$this->config->item('Hcdn_redirect_uri');
		      }
		   }
        echo json_encode(array('url'=>$url,'val'=>$val));
		}      
        
        /*
     * This function is used to create email HTML
     */ 
    public function sendEmailToInvitee($emailData,$subjectLine,$eventdetails,$isPassword,$selInvitationType,$nameOfSender){
        
        /*add event details in mail */
   
        $logoThemePath ='';
        if(!empty($eventdetails->logo_image)) {
        $logoThemePath          = base_url().'media/event_logo/'.$eventdetails->logo_image;
        $logo_size              = $eventdetails->logo_size;
        $logo_size_and_position = json_decode($logo_size);
        $image_position_top     = (isset($logo_size_and_position->image_position_top)) ? $logo_size_and_position->image_position_top : '19';
        $image_position_left    = (isset($logo_size_and_position->image_position_left)) ? $logo_size_and_position->image_position_left : '23' ;
        $image_width            = (!empty($logo_size_and_position->image_width)) ? $logo_size_and_position->image_width : '125';
        $image_height           = (!empty($logo_size_and_position->image_height)) ? $logo_size_and_position->image_height : '110';
        }

        $isHeaderPath   = base_url().'media/event_header/'.$eventdetails->header_image_name;
        
        $imgstatus = (@fopen($isHeaderPath,"r")==true) ? 'true' : 'false';
        if($imgstatus=='true') { 
        $headerImage = $isHeaderPath;
        }else {
        $headerImage = base_url().$this->config->item('themes_path').'images/header_banner_dummy.png';
        }
     
                
        /* Set logo image path */
        if(!empty($logoThemePath)) {
            $logoPath           = '<img src="'.$logoThemePath.'" style="width:150px; height:100px; padding: 20px 0px;" />';
        }else {
            $logoPath           = '<img src="'.base_url().'themes/assets/images/light.png" style="width:150px; height:100px; padding: 20px 0px;" />';
        }
        
     
        $event_title        = (!empty($eventdetails->event_title)) ? $eventdetails->event_title : '';
        $subtitle           = (!empty($eventdetails->subtitle)) ? $eventdetails->subtitle : '';
        $event_venue        = (!empty($eventdetails->event_venue)) ? $eventdetails->event_venue : '';
        $description        = (!empty($eventdetails->description)) ? $eventdetails->description : '';
        

        $starttime          = @date('d',strtotime($eventdetails->starttime));
        $starttime          = (!empty($starttime)) ? $starttime : '';
        $endtime            = @date('jS F Y',strtotime($eventdetails->endtime));
        $endtime            = (!empty($endtime)) ? $endtime : '';
        //date('l jS F Y h:i:s A');
        $starttime1         = @date('l jS F Y',strtotime($eventdetails->starttime));
        $starttime1         = (!empty($starttime1)) ? $starttime1 : '';
        $endtime1           = @date('l jS F Y',strtotime($eventdetails->endtime));
        $endtime1           = (!empty($endtime1)) ? $endtime1 : '';
        
        $event_lat          = (!empty($eventdetails->event_lat)) ? $eventdetails->event_lat : '';
        $event_long         = (!empty($eventdetails->event_long)) ? $eventdetails->event_long : '';
        
        $eventUrl           = (!empty($eventdetails->event_url)) ? $eventdetails->event_url : '';
        $eventUrl           = base_url().'events/invitation/'.$eventUrl;
                
        $mapImageUrl = 'https://maps.googleapis.com/maps/api/staticmap?center='.$event_lat.','.$event_long.'&zoom=13&size=200x131&path=weight:3%7Ccolor:blue%7Cenc:aofcFz_bhVJ[n@ZpAp@t@b@uA`FuAzEoCdJiDpLs@VM@y@s@oBcBkAw@cCoAuBu@eEaAiAa@iAi@w@a@o@g@g@k@e@u@uAaCc@i@w@y@eAo@i@UaBc@kAGo@@]JyKA}EC{G?q@?IGKCeGA{CAyCAyEAwEBaFAkJ?yGEyAIiLAiB?{@BcBJ}@@aBGwBEo@A@j@BjBFTHjEl@fOD`C?|@RARAJERWPL@FE^S`AI`A&key=AIzaSyB6Cr2EG6TLNBUaruRT7ky7IFMh8CHn9Hw';
        $mapHref  = 'http://maps.google.com/maps?q='.$event_lat.','.$event_long.'';
       
        
        /* Sendgrid */
        $apiKey = ADMINNEWMAN_APIKEY; //:eventurl
        $this->load->helper('sendgrid');
        //$admin_templateId = '8769c994-ddb7-4b5a-a7a1-23664c296026'; //
        $admin_templateId = '2a56af2f-8754-4f1b-85ec-5ecd3dbaca79'; //
        
        /* Send password */
        
        $loginDetailshtml ='';
        if(!empty($isPassword)){
            $loginDetailshtml .='.<br/><table>
                            <tr>
                                <td style="font-size: 24px;">Invitation Type :</td>
                                <td style="font-size: 24px;">'.$selInvitationType.'</td>
                            </tr>
                            <tr>
                                <td style="font-size: 24px;">Registration Password:</td>
                                <td style="font-size: 24px;">'.$isPassword.'</td>
                            </tr>
                        </table>';
        }else{
            $loginDetailshtml .='.';
        }             
        
        if(empty($nameOfSender)){
           $fromName    =  'Newman'; 
        }else {
            $fromName   = 'Newman : '.$nameOfSender; 
        }
        
        $dayS = date('d',strtotime($eventdetails->starttime)); 
        $dayE = date('d',strtotime($eventdetails->endtime)); 
        if($dayS == $dayE) {
            $eventdate = $endtime1;
        }else {
            $eventdate = $starttime1.'<br/> to '.$endtime1;
        }
        
        if(!empty($eventdetails->eventVenueAddress1) && empty($eventdetails->eventVenueAddress2)){
            $event_venue = $event_venue.' <br/> '.$eventdetails->eventVenueAddress1;
        }else if(!empty($eventdetails->eventVenueAddress1) && !empty($eventdetails->eventVenueAddress2)){
            $event_venue = $event_venue.' <br/> '.$eventdetails->eventVenueAddress1.' '.$eventdetails->eventVenueAddress2;
        }else if(empty($eventdetails->eventVenueAddress1) && !empty($eventdetails->eventVenueAddress2)){
            $event_venue = $event_venue.' <br/> '.$eventdetails->eventVenueAddress2;
        }else{}
       
        if(!empty($eventdetails->eventVenueZip)){
            $event_venue = $event_venue.' Postcode : '.$eventdetails->eventVenueZip;
        }
        
        $paramArray = array(
        'sub' => array(':eventDescription'=>array($description),':logoPath'=>array($logoPath),':headerImage'=>array($headerImage),':eventtitle' => array($event_title),':mapImageUrl' => array($mapImageUrl),':eventdate' => array($eventdate),':eventvenue' => array($event_venue),':mapHref' => array($mapHref),':eventurl' => array($eventUrl)),
        'filters' => array('templates' => array('settings' => array('enable' => 1, 'template_id' => $admin_templateId)))
        );
        $tempSubject    = $subjectLine;
        $templateBody   = $loginDetailshtml;
        //$toId = array('shailendratiwari@cdnsol.com');//$admin_email;
        
        foreach($emailData as $emailVal) {
            $toId = $emailVal;
            sendgrid_mail_template($admin_templateId,$tempSubject,$templateBody,$toId,$paramArray,$apiKey,eventFromEmail,'','',$fromName);
        }
        
    } 
    
    public function getEventUrl($eventId) {
        /* get event theme data from event_theme table */
        //$whereEvent = array('is_theme_used' => '1' ,'event_id' => $eventId );
        //$themes     = $this->common_model->getDataFromTabel('event_themes_data', '*', $whereEvent);
        
        /* get event details from event table */
        $eventdetails = $this->events_model->geteventdetails($eventId,$userId=0);
        $url = base_url().'events/invitation/'.$eventdetails->event_url;
        return $url;
    }
    
    public function getPdf($data='') {
        
        $eventdetails       = $data['eventdetails'];
        $bookerDetails      = $data['bookerDetails'];
        $attendeeDetails    = $data['attendeeDetails'];
        $checkedIdsCount    = $data['checkedIdsCount'];
        $checkedIdsArr      = $data['checkedIdsArr'];
        $invitationStatus   = $data['invitationStatus'];
        
        $bookerTableDetails = $data['bookerTableDetails'][0];
        $unique_id          = $bookerTableDetails->unique_id;
        
        $invitation_type_id = (!empty($bookerTableDetails->invitation_type_id)) ? $bookerTableDetails->invitation_type_id : '';
        $invitation_name = '';
        if(!empty($invitation_type_id)){
            $whereinvitations       = array('id'=>$invitation_type_id);
            $invitationsDetail      = getDataFromTabel('nm_event_invitations','invitation_name',$whereinvitations);
            $invitationsDetail      = $invitationsDetail[0];
            $invitation_name        = $invitationsDetail->invitation_name;
        }
        
        
        $bookerId           = $data['bookerId'];
        $quantity           = $data['quantity'];
        $eventId            = $data['eventId'];
        
        $qrPath = $bookerTableDetails->qrPath;
        $barcodePath        = base_url().'media/barcode/'.$bookerTableDetails->barcodePath;
        
        $logoThemePath ='';
        if(!empty($eventdetails->logo_image)) {
            $logoThemePath          = base_url().'media/event_logo/'.$eventdetails->logo_image;
            $logo_size              = $eventdetails->logo_size;
            $logo_size_and_position = json_decode($logo_size);
            $image_position_top     = (isset($logo_size_and_position->image_position_top)) ? $logo_size_and_position->image_position_top : '19';
            $image_position_left    = (isset($logo_size_and_position->image_position_left)) ? $logo_size_and_position->image_position_left : '23' ;
            $image_width            = (!empty($logo_size_and_position->image_width)) ? $logo_size_and_position->image_width : '125';
            $image_height           = (!empty($logo_size_and_position->image_height)) ? $logo_size_and_position->image_height : '110';
        }
        
        $isHeaderPath   = base_url().'media/event_header/'.$eventdetails->header_image_name;
        $imgstatus = (@fopen($isHeaderPath,"r")==true) ? 'true' : 'false';
        if($imgstatus=='true') { 
            $headerImage = $isHeaderPath;
        }else {
            $headerImage = base_url().$this->config->item('themes_path').'images/header_banner_dummy.png';
        }
        
        $event_title        = (!empty($eventdetails->event_title)) ? $eventdetails->event_title : '';
        $term_conditions        = (!empty($eventdetails->term_conditions)) ? $eventdetails->term_conditions : '';
        $subtitle           = (!empty($eventdetails->subtitle)) ? $eventdetails->subtitle : '';
        $event_venue        = (!empty($eventdetails->event_venue)) ? $eventdetails->event_venue : '';
        
        if(!empty($eventdetails->eventVenueAddress1) && empty($eventdetails->eventVenueAddress2)){
            $event_venue = $event_venue.' <br/> '.$eventdetails->eventVenueAddress1;
        }else if(!empty($eventdetails->eventVenueAddress1) && !empty($eventdetails->eventVenueAddress2)){
            $event_venue = $event_venue.' <br/> '.$eventdetails->eventVenueAddress1.' '.$eventdetails->eventVenueAddress2;
        }else if(empty($eventdetails->eventVenueAddress1) && !empty($eventdetails->eventVenueAddress2)){
            $event_venue = $event_venue.' <br/> '.$eventdetails->eventVenueAddress2;
        }else{}
       
        if(!empty($eventdetails->eventVenueZip)){
            $event_venue = $event_venue.' Postcode : '.$eventdetails->eventVenueZip;
        }
        
        $event_lat          = (!empty($eventdetails->event_lat)) ? $eventdetails->event_lat : '';
        $event_long         = (!empty($eventdetails->event_long)) ? $eventdetails->event_long : '';
        
        $eventUrl           = (!empty($eventdetails->event_url)) ? $eventdetails->event_url : '';
        $eventUrl           = base_url().'events/invitation/'.$eventUrl;
                
        $mapImageUrl = 'https://maps.googleapis.com/maps/api/staticmap?center='.$event_lat.','.$event_long.'&zoom=13&size=200x131&path=weight:3%7Ccolor:blue%7Cenc:aofcFz_bhVJ[n@ZpAp@t@b@uA`FuAzEoCdJiDpLs@VM@y@s@oBcBkAw@cCoAuBu@eEaAiAa@iAi@w@a@o@g@g@k@e@u@uAaCc@i@w@y@eAo@i@UaBc@kAGo@@]JyKA}EC{G?q@?IGKCeGA{CAyCAyEAwEBaFAkJ?yGEyAIiLAiB?{@BcBJ}@@aBGwBEo@A@j@BjBFTHjEl@fOD`C?|@RARAJERWPL@FE^S`AI`A&key=AIzaSyB6Cr2EG6TLNBUaruRT7ky7IFMh8CHn9Hw';
        $mapHref  = 'http://maps.google.com/maps?q='.$event_lat.','.$event_long.'';
        
        
        
        $starttime          = @date('d',strtotime($eventdetails->starttime));
        $starttime          = (!empty($starttime)) ? $starttime : '';
        $endtime            = @date('jS F Y',strtotime($eventdetails->endtime));
        $endtime            = (!empty($endtime)) ? $endtime : '';
        //date('l jS F Y h:i:s A');
        $starttime1         = @date('l jS F Y',strtotime($eventdetails->starttime));
        $starttime1         = (!empty($starttime1)) ? $starttime1 : '';
        $endtime1           = @date('l jS F Y',strtotime($eventdetails->endtime));
        $endtime1           = (!empty($endtime1)) ? $endtime1 : '';
        $booker_name = '';
        if(isset($bookerDetails) && !empty($bookerDetails)){
            $booker_name = $bookerDetails[1]->field_value;
            }


        $this->load->library('pdfcrowd');
        $attendeeHTMLforPdf ='';
        $dataView ='';
        try
        {  
             
            // create an API client instance
            $client     = new Pdfcrowd("youshailu", "c024159f7addcee18386340d8013ed2b");
            $htmlView   ='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
        <html xmlns="http://www.w3.org/1999/xhtml">
        <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Untitled Document</title>
        <style type="text/css">
        body, h1, h2, h3, h4, p { margin: 0; padding: 0; font-family: Arial, Helvetica, sans-serif; color:#151d29; }
        p { font-size: 12px; line-height: 18px; }
        .hadding-top { color: #609dd4; }
        .hadding-next { color: #FFF; margin: 5px 0 0 0; }
        .contant01 { color: #FFF; margin: 5px 0 0 0; }
        </style>
        </head>

        <body>
        <table width="636" border="0" cellpadding="0" cellspacing="0" background="#dcd8d6" style="padding: 20px 20px 0px; background:#dcd8d6;" >
          <tr>
            <td style="position:relative">';
            if($logoThemePath!='') {
                $htmlView .='
                <div style="
                position: relative;
                width: 100%;
                ">
                <img src="'.$logoThemePath.'" style="
                width: '.$image_width.'px;
                height: '.$image_height.'px;
                position: absolute;
                left: '.$image_position_left.'px;
                top: '.$image_position_top.'px;
                ">
                </div>';
            }
            
            $htmlView .='<img src="'.$headerImage.'" style="width: 100%;"/></td>
          <tr style="width:548px;">
            <td width="548px" height="104px" style="background-color:#2d2837; padding:24px; float:left; border-top-left-radius:0px;
        border-top-right-radius:0px;
        border-bottom-right-radius:0px;
        border-bottom-left-radius:10px;"><h2  class="hadding-top">'.$event_title.'</h2>
              <h4 class="hadding-next">'.$subtitle.'</h4>
              <p class="contant01">'.$event_venue.'</p>
              <p class="contant01"> '.$starttime.' - '.$endtime.' </p></td>
          </tr>';
          
          $attendeeHTMLforPdf .= $htmlView;
          
          $htmlView .='<tr  style="background-color:#FFF; padding:20px; margin:13px 0 0 0;float:left;
        border-top-left-radius:20px;
        border-top-right-radius:20px;
        border-bottom-right-radius:0px;
        border-bottom-left-radius:20px;  width: 556px; ">
            <td  width="40%" style="float:left; margin: 0 0 0 5px;">
            <h4>Bill to:</h4>';
            $counter = 0 ;
            $name ='';
            $nameAttendee1='';
            $nameAttendees='';
            $bookerDetailHtml = '';
            
            foreach($bookerDetails as $booker) { 
                
                if($counter<=2) {
                   $nameAttendee1 .= $booker->field_value.' ';     
                }
                
                if($counter<=3) {
                   $name .= $booker->field_value.' ';     
                   if($counter==3) {
                       $htmlView .='<p  style="margin:0 0 0 20px">'.$name.'</p>';
                       $bookerDetailHtml .='<p  style="margin:0 0 0 0;font-size:20px">'.$name.'</p>';
                   }
                }else {
                    $htmlView .='<p  style="margin:0 0 0 20px">'.$booker->field_value.'</p>';
                    $bookerDetailHtml .='<p  style="margin:0 0 0 0;font-size:20px;">'.$booker->field_value.'</p>';
                }
                $counter++;
            }
            
           $htmlView .= '
              </td>
            <td  width="50%" style="float:left; margin:25px 0 0 0;"><h3 style=" text-align:center">TREAT YOUR eTICKET
                LIKE CASH</h3></td>
            </tr>';    
          $dataView = '<tr style="margin:16px 0 0 0; float:left;">
            <td><h4 style="margin:5px 0 11px 0; float:left; ">TERMS & CONDITION</h4>
              <p style="clear:both; float:left; font-size:10px; line-height:13px;">'.$term_conditions.'</p></td>
          </tr>';
          
          $htmlView .= $dataView;
          $attendeeHTMLforPdf .= $dataView;
          
          $htmlView .= '<!-- Ticket Booker -->
          <tr style="background-color:#FFF; padding: 14px; margin:0px 0 26px 0;float:left;
            border-top-left-radius:20px;
            border-top-right-radius:20px;
            border-bottom-right-radius:0px;
            border-bottom-left-radius:20px; width: 556px;margin-top:10px;">
                <td style="float:left; margin:0 0 10px 0" width="100%"><h3 style="padding:0px 0 0 84px;">'.$event_title.'</h3></td>
                <td style="float:left;padding: 0 29px 0 0;margin-top: 40px;"><img src="'.$barcodePath.'" style="transform: rotate(90deg);"></td>
                <td style="float:left;" width="49%"><h4 style="float:left;">Date And Time</h4>
                  <p style="float:left;width: 84%;">'.$starttime1.'</p>
                  <p style="float:left;width: 84%;">to '.$endtime1.'</p>
                  <h4 style="float:left; margin:10px 0 0 0;"> LOCATION: </h4>
                  <p style="float:left; clear:both;width: 84%;"></p>
                  <p style="float:left; clear:both;width: 84%;"></p>
                  <p style="float:left; clear:both;width: 84%;">'.$event_venue.'</p></td>
                <td><h4 style=" text-align:right; margin: 10px 0 0;">'.$nameAttendee1.'</h4>
                  <p><!--Dietary Requirement: Gluten--> </p>
                  <img width="80" height="80" src="'.$qrPath.'" style="float:right; margin:10px 0 0 0;"></td>
                <td style="float: left; width:100%;"><p style="  font-size: 9px;
                margin: 0 0 0 84px;">please read fullterms &amp; condtions at www.newmanevents.com</p></td>
            </tr>';
            
            $attendetailHtml = '';
            $i = 0;
            foreach($attendeeDetails as $attendee) { 
                $counter = 0 ;
                $nameAttendees='';
                $dietary = $attendee[0]->dietary;
                $dietary = !empty($dietary)?$dietary:'None';
                foreach($attendee as $list) {
                    
                    if($counter<=2) {
                       $nameAttendees .= $list->field_value.' ';     
                        $counter++;
                    }
                   
                }//End foreach
            $i++;    
            $attendetailHtml .='<div style="font-weight:bold" >ATTENDEE -'.$i.'</div><div style="font-weight:bold">&nbsp;&nbsp;'.$nameAttendees.'</div><div>&nbsp;&nbsp;Dietary Requirements: '.$dietary.'</div><br>';
                
            $htmlView .= '
            <!-- Ticket Attendee -->  
            <tr style="background-color:#FFF; padding: 14px; margin:0px 0 26px 0;float:left;
            border-top-left-radius:20px;
            border-top-right-radius:20px;
            border-bottom-right-radius:0px;
            border-bottom-left-radius:20px; width: 556px;">
                <td style="float:left; margin:0 0 10px 0" width="100%"><h3 style="padding:0px 0 0 84px;">'.$event_title.'</h3></td>
                <td style="float:left;padding: 0 29px 0 0;margin-top: 40px;"><img src="'.$barcodePath.'" style="transform: rotate(90deg);"></td>
                <td style="float:left;" width="49%"><h4 style="float:left;">Date And Time</h4>
                  <p style="float:left;width: 84%;">'.$starttime1.'</p>
                  <p style="float:left;width: 84%;">to '.$endtime1.'</p>
                  <h4 style="float:left; margin:10px 0 0 0;"> LOCATION: </h4>
                  <p style="float:left; clear:both;width: 84%;"></p>
                  <p style="float:left; clear:both;width: 84%;"></p>
                  <p style="float:left; clear:both;width: 84%;">'.$event_venue.'</p></td>
                <td><h4 style=" text-align:right; margin: 10px 0 0;">'.$nameAttendees.'</h4>
                  <p><!--Dietary Requirement: Gluten--> </p>
                  <img width="80" height="80" src="'.$qrPath.'" style="float:right; margin:10px 0 0 0;"></td>
                <td style="float: left; width:100%;"><p style="  font-size: 9px;
                margin: 0 0 0 84px;">please read fullterms &amp; condtions at www.newmanevents.com/BTC</p></td>
            </tr>';
                
            }//End foreach
            
              $dataView = '<tr>
                <td  style="float:left; margin:20px 0 19px 10px;"><p style="float:left;margin:4px 0 0 0px;"> Powered by </p>
                  <img src="'.base_url().'themes/assets/images/pbwlogo.png" style="float:left; margin:0 0 0 20px;"/></td>
              </tr>
                </tr>
              
            </table>
            </body>
            </html>
            ';
            
            $htmlView .= $dataView; 
            
            // convert a web page and store the generated PDF into a $pdf variable
            //$pdf = $client->convertHtml($htmlView);

            //~ // set HTTP response headers
            header("Content-Type: application/pdf");
            header("Cache-Control: max-age=0");
            header("Accept-Ranges: none");
            header("Content-Disposition: attachment; filename=\"newmanevent.pdf\"");

            // send the generated PDF 
            //echo $pdf;
            //$namefile = date('YmdHi');
            $namefile = 'ticket_'.$bookerId;
            $out_file = fopen("./media/".$namefile.".pdf", "wb");
            $client->convertHtml($htmlView, $out_file);
            fclose($out_file);
            $filename = $namefile.'.pdf'; 
            $fpath ='./media/'.$namefile.'.pdf';
            /* Load Email Library */
            $toemail = (!empty($bookerDetails[3]->field_value)) ? $bookerDetails[3]->field_value : '';
            $subject = 'You are going to the '.$event_title.' : Ticket';
            if(!empty($toemail)) {
                
                $password = date('YmdHi').$bookerId;
                
                $insertRegistration['event_id']     = $eventId;
                $insertRegistration['booker_id']    = $bookerId;
                $insertRegistration['email']        = $toemail;
                $insertRegistration['password']     = $password;
                $insertRegistration['registration_type']     = '1';
                if($invitationStatus==1){
                    $this->common_model->addDataIntoTabel($this->tabledelegatereg,$insertRegistration);
                }
                
                $eventUrl = base_url().'events/invitation/'.$eventdetails->event_url;
                
                $bookerMessage = 'For more information or any modification in ticket attendee details please use following credentials : <br/><br/>
                <span style="color:#415464"><strong>Login Credentials<strong></span> :-<br/><span style="color:#415464"> Email</span> : '.$toemail.'<br/>Password : '.$password.'<br/><br/><span style="color:#415464"> Please find below your event URL link</span> :<br/> Event Link : <a href="'.$eventUrl.'">'.$eventUrl.'</a><br/><br/><span style="color:#415464">Regards,</span><br/><span style="color:#415464">Team Newman</span>';
                
                //~ $this->load->library('email'); 
                //~ $this->email->from( FROM_EMAIL, FROM_NAME ); // variable define in config  
                //~ $this->email->to($toemail);
                //~ $this->email->subject($subject);
                //~ $this->email->message($bookerMessage);  
                //~ $this->email->attach($fpath);
              
                
                /* Sendgrid */
                $apiKey = ADMINNEWMAN_APIKEY; //:eventurl
                $this->load->helper('sendgrid');
                //$admin_templateId = '68a0e728-400a-4e2b-b6bf-c08dc83651e1'; //
                $admin_templateId = 'a37a3c5e-54d0-4446-a0d3-ddbec2a68d48'; //
                
                $paramArray = array(
                'sub' => array(':invitationtype_name'=>array($invitation_name),':bookerId'=>array($unique_id),':attendetailHtml'=>array($attendetailHtml),':bookerDetailHtml'=>array($bookerDetailHtml),':booker_name'=>array($booker_name),':logoPath'=>array($logoThemePath),':headerImage'=>array($headerImage),':eventtitle' => array($event_title),':mapHref' => array($mapHref),':eventurl' => array($eventUrl),':mapImageUrl' => array($mapImageUrl),':startdate' => array($starttime1),':enddate' => array($endtime1),':eventvenue' => array($event_venue)),
                'filters' => array('templates' => array('settings' => array('enable' => 1, 'template_id' => $admin_templateId)))
                );
                $tempSubject = $event_title;
                $templateBody = $bookerMessage;
                $toId = $toemail;
                sendgrid_mail_template($admin_templateId,$tempSubject,$templateBody,$toId,$paramArray,$apiKey,eventFromEmail,$filename,$fpath);
            }
            
            $attendetailHtml1 = '';
            $i = 0;
            $loop = 0; 
            $index = 1;
            foreach($attendeeDetails as $attendee) {
                if($loop < $checkedIdsCount){
                if($attendee[0]->field_order==$checkedIdsArr[$loop]){
                
                $newAttendeeHtml ='';     
                
                /* Set attendee HTML */
                $counter = 0 ;
                $nameAttendees='';
                $dietary = $attendee[0]->dietary;
                $dietary = !empty($dietary)?$dietary:'None';
                foreach($attendee as $list) {
                    
                    if($counter<=2) {
                       $nameAttendees .= $list->field_value.' ';     
                        $counter++;
                    }
                   
                }//End foreach
                $i++;  
                
                $attendeeRow = '
                <!-- Ticket Attendee -->  
                <tr style="background-color:#FFF; padding: 14px; margin:0px 0 26px 0;float:left;
                border-top-left-radius:20px;
                border-top-right-radius:20px;
                border-bottom-right-radius:0px;
                border-bottom-left-radius:20px; width: 556px;">
                    <td style="float:left; margin:0 0 10px 0" width="100%"><h3 style="padding:0px 0 0 84px;">'.$event_title.'</h3></td>
                    <td style="float:left;padding: 0 29px 0 0;margin-top: 40px;"><img src="'.$barcodePath.'" style="transform: rotate(90deg);"></td>
                    <td style="float:left;" width="49%"><h4 style="float:left;">Date And Time</h4>
                      <p style="float:left;width: 84%;">'.$starttime1.'</p>
                      <p style="float:left;width: 84%;">to '.$endtime1.'</p>
                      <h4 style="float:left; margin:10px 0 0 0;"> LOCATION: </h4>
                      <p style="float:left; clear:both;width: 84%;"></p>
                      <p style="float:left; clear:both;width: 84%;"></p>
                      <p style="float:left; clear:both;width: 84%;">'.$event_venue.'</p></td>
                    <td><h4 style=" text-align:right; margin: 10px 0 0;">'.$nameAttendees.'</h4>
                      <p><!--Dietary Requirement: Gluten--> </p>
                      <img width="80" height="80" src="'.$qrPath.'" style="float:right; margin:10px 0 0 0;"></td>
                    <td style="float: left; width:100%;"><p style="  font-size: 9px;
                    margin: 0 0 0 84px;">please read fullterms &amp; condtions at www.newmanevents.com/BTC</p></td>
                </tr>';
                
                $newAttendeeHtml .= $attendeeHTMLforPdf;
                $newAttendeeHtml .= $attendeeRow; 
                $newAttendeeHtml .= $dataView; 
                
                $namefile = 'ticket_'.$bookerId.'_'.$index;
                $out_file = fopen("./media/".$namefile.".pdf", "wb");
                $client->convertHtml($newAttendeeHtml, $out_file);
                fclose($out_file);
                $filename = $namefile.'.pdf'; 
                $fpath ='./media/'.$namefile.'.pdf'; 
                
                  
                $attendetailHtml1 ='<div style="font-weight:bold" >ATTENDEE DETAILS</div><div style="font-weight:bold">&nbsp;&nbsp;'.$nameAttendees.'</div><div>&nbsp;&nbsp;Dietary Requirements: '.$dietary.'</div><br>';
                
 
                $toemail = (!empty($attendee[3]->field_value)) ? $attendee[3]->field_value : '';
                $attendeeId = (!empty($attendee[3]->id)) ? $attendee[3]->id : '';
                $booker_name = $attendee[1]->field_value;
                $subject = 'You are going to the '.$event_title.' : Ticket';
                $field_order = $attendee[0]->field_order;
                if(!empty($toemail)) {
                    
                    $password = date('YmdHi').$attendeeId;    
                    $insertRegistration['event_id']     = $eventId;
                    $insertRegistration['booker_id']    = $bookerId;
                    $insertRegistration['email']        = $toemail;
                    $insertRegistration['password']     = $password;
                    $insertRegistration['registration_type']     = '2';
                    $insertRegistration['field_order']  = $field_order;
                    if($invitationStatus==1){
                        $this->common_model->addDataIntoTabel($this->tabledelegatereg,$insertRegistration);
                    }
                    $eventUrl = base_url().'events/invitation/'.$eventdetails->event_url;
                    
                    $bookerMessage = 'For more information or any modification in ticket attendee details please use following credentials : <br/><br/>
                    <span style="color:#415464"><strong>Login Credentials<strong></span> :-<br/><span style="color:#415464"> Email</span> : '.$toemail.'<br/>Password : '.$password.'<br/><br/><span style="color:#415464"> Please find below your event URL link</span> :<br/> Event Link : <a href="'.$eventUrl.'">'.$eventUrl.'</a><br/><br/><span style="color:#415464">Regards,</span><br/><span style="color:#415464">Team Newman</span>';
                    
                    /* Sendgrid */
                    $apiKey = ADMINNEWMAN_APIKEY; //:eventurl
                    $this->load->helper('sendgrid');
                    //$admin_templateId = '68a0e728-400a-4e2b-b6bf-c08dc83651e1'; //
                    $admin_templateId = 'a37a3c5e-54d0-4446-a0d3-ddbec2a68d48'; //
                    
                    $attendetailHtml2 = '.';
                    
                    $paramArray = array(
                    'sub' => array(':invitationtype_name'=>array($invitation_name),':bookerId'=>array($unique_id),':attendetailHtml'=>array($attendetailHtml2),':bookerDetailHtml'=>array($attendetailHtml1),':booker_name'=>array($booker_name),':logoPath'=>array($logoThemePath),':headerImage'=>array($headerImage),':eventtitle' => array($event_title),':mapHref' => array($mapHref),':eventurl' => array($eventUrl),':mapImageUrl' => array($mapImageUrl),':startdate' => array($starttime1),':enddate' => array($endtime1),':eventvenue' => array($event_venue)),
                    'filters' => array('templates' => array('settings' => array('enable' => 1, 'template_id' => $admin_templateId)))
                    );
                    $tempSubject = $event_title;
                    $templateBody = $bookerMessage;
                    $toId = $toemail;
                    sendgrid_mail_template($admin_templateId,$tempSubject,$templateBody,$toId,$paramArray,$apiKey,eventFromEmail,$filename,$fpath);
                    
                    }
                    $index++;
                    $loop++;
                  }
                }
                
            }
            
            $registration_type = $data['registration_type'];
            $field_order = $data['field_order'];
            if($registration_type==1 || $registration_type==''){
                redirect('events/attendeeConfirmation/'.$bookerId.'/'.$quantity.'/'.$eventId); // on attendeeConfirmation/bookerid/qty/eventid
            }else{
                redirect('events/attendeeConfirmation/'.$bookerId.'/'.$quantity.'/'.$eventId.'?q=true&rtype='.$registration_type.'&field_order='.$field_order);
            }
        }
        catch(PdfcrowdException $why)
        {
            echo "Pdfcrowd Error: " . $why;
        }
    }
    
    
    public function loginPost(){
        $loginUsername  = $this->input->post('loginUsername');
        $loginPassword  = $this->input->post('loginPassword');
        $eventid        = $this->input->post('eventid');
        
        $condition      = array('email'=>$loginUsername,'password'=>$loginPassword); 
        $responce       = $this->common_model->getDataFromTabel($this->tabledelegatereg,'*',$condition);
        if((!empty($responce)) && (count($responce) > 0)) {
            $responce = $responce[0];
            $eventId  = $responce->event_id;
            $bookerId = $responce->booker_id;
            $registration_type = $responce->registration_type;
            $field_order = $responce->field_order;
            $conditionbooker = array('id'=>$bookerId);
            $bookerData = $this->common_model->getDataFromTabel($this->tablebooker,'*',$conditionbooker);
            if(!empty($bookerData)) {
                $bookerData = $bookerData[0];
                $quantity   = $bookerData->quantity;
                redirect('events/attendeeConfirmation/'.$bookerId.'/'.$quantity.'/'.$eventId.'?q=true&rtype='.$registration_type.'&field_order='.$field_order);
            }
        }else {
            $url = $this->getEventUrl($eventid);
            redirect($url.'?q=false');
        }
        
    }
    
     public function testEmail(){
        /* Load Email Library */
        $this->load->library('email'); 
        $this->email->from( FROM_EMAIL, FROM_NAME ); // variable define in config  
        
        /* send email notification to admin */
        $this->email->to('shailendratiwari@cdnsol.com');
        $this->email->subject('This is a email');
        
        $this->email->message('This is a sample email send by shailendra');  
        $this->email->send();
        echo $this->email->print_debugger();
    }
    
    function isUserExist(){
        echo '<pre>';
        print_r($_POST);die;
    }
    
    public function AddNewAttendee() {
        
        $is_ajax_request = $this->input->is_ajax_request();
        $loginUserId = isLoginUser();    
        $invitation_types_id        = $this->input->post('invitation_types');
        $eventId                    = $this->input->post('eventId');
        $bookerId                   = $this->input->post('bookerId');
        $quantity                   = $this->input->post('quantity');
        /* Get total invitations limit & waitig list status (is yes/No) */
        $whereConditionInvitation   = array( 'event_id'=> $eventId, 'id' => $invitation_types_id );
        $eventInvitations           = $this->common_model->getDataFromTabel($this->tableinvitation,'*',$whereConditionInvitation);
        $data['eventInvitations']   = $eventInvitations;
        if( count($eventInvitations) > 0 ) {
            $eventInvitations       = $eventInvitations[0];
            $invitationLimit        = $eventInvitations->invitation_limit;
            $allow_waiting_list     = $eventInvitations->allow_waiting_list;
        }else {
            $invitationLimit        = 0 ;
        }
        /* get used invitation limit from added attendee */
        $whereCondition             = array('invitation_type_id' => $invitation_types_id);
        $usedInvitationLimit        = $this->common_model->getDataFromTabel($this->tablebooker,'*',$whereCondition);
        if( count($usedInvitationLimit) > 0 ) {
            $usedquantity = 0;
            if($usedInvitationLimit) {
                foreach($usedInvitationLimit as $ulimit) {
                    $usedquantity       += $ulimit->quantity;
                }
            }
            
        }else {
            $usedquantity = 0;
        }
        
        if($loginUserId==false || $loginUserId==0 || $loginUserId==null) {
            $loginUserId = 0;
        }
        
            $arrayVal     =  $this->input->post('fieldid');
            $field_qty    =  $this->input->post('field_qty');
            /* Check waitlist is start */
            $isWaitList    =  $this->input->post('isWaitList');
            
            /* Check post quantity shold not greater from total qty */
           // if($isWaitList!=1) {
            $postQty      = $field_qty;
            $postUsedQty  = $postQty + $usedquantity + $quantity; 
            if(!empty($invitationLimit)) {
                if($postUsedQty > $invitationLimit) {
                    $remaining      = $postUsedQty - $invitationLimit; 
                    $remaining      = $postQty - $remaining;
                    if($remaining!=0) {
                        $this->form_validation->set_rules('at1', 'You can add only '.$remaining.' attendee for this invitation type', 'callback_custom_error_set');
                        $errors = 'You can add only '.$remaining.' attendee for this invitation type';
                    }else {
                        /* Check wait list*/
                        if($allow_waiting_list!=1) {
                            $this->form_validation->set_rules('at2', 'You need to allow waitlist. If you need to add more attendee', 'callback_custom_error_set');
                            $errors = 'You need to allow waitlist. If you need to add more attendee';
                         
                        }else {
                            $this->form_validation->set_rules('at3', 'You can add as Waitlist', 'callback_custom_error_set');
                            $errors = 'You can add as Waitlist';
                        }
                    }
                }
            }
            
            if(empty($invitationLimit)) {
                $this->form_validation->set_rules('at4', 'You can not add for this invitation because invitation not contains limit', 'callback_custom_error_set');
                $errors = 'You can not add for this invitation because invitation not contains limit';
            }
            //}
            
            if ($this->form_validation->run())
            {
                if($is_ajax_request){
                    echo json_message('msg',$errors,'is_success','false');
                }
                die;
            }
            
           
            //update field value
            $arrayFieldVal  =  $this->input->post();
            /* Insert data for Booker person */
            /* End booker */
            //~ $insertDataBooker['event_id']           = $eventId;
            //~ $insertDataBooker['login_id']           = $loginUserId;
            //~ $insertDataBooker['invitation_type_id'] = $arrayFieldVal['invitation_types'];
            //~ $insertDataBooker['quantity']           = $arrayFieldVal['field_qty'];
            //~ $insertDataBooker['unique_id']          = 'NEWMAN-'.randomnumber(4);
            //~ $insertDataBooker['isWaitList']         = $isWaitList;
            //~ $bookerId = $this->common_model->addDataIntoTabel($this->tablebooker,$insertDataBooker);
            //~ $qrPath   = $this->qrCode($eventId,$bookerId);
            //~ $barCode  = $this->getBarCode($bookerId);
            //~ $whereCon = array('id' => $bookerId);
            //~ $dataQr['qrPath']       = $qrPath; 
            //~ $dataQr['barcodePath']  = $barCode; 
            //~ $this->events_model->updateDataFromTable($this->tablebooker,$dataQr,$whereCon);
            /* Insert data for Attendee users */
           
           $whereCon = array('id' => $bookerId);
           $updatedata['quantity'] = $quantity + $field_qty;
           $this->events_model->updateDataFromTable($this->tablebooker,$updatedata,$whereCon);
           
            $loop = 1;
            foreach($arrayVal as $fieldid){
                $filedRowId = 'field_'.$fieldid;
                $filedBookerId = 'booker_'.$fieldid;
                $filedBookerImg = 'booker_image_'.$fieldid;
                $filedAttendeeImg = 'attendee_image_'.$fieldid;
                
                if (array_key_exists($filedRowId, $arrayFieldVal)) {
                   $field_data = $arrayFieldVal[$filedRowId];
                   $order = $quantity + 1;
                   $formid = 4;
                }else if (array_key_exists($filedRowId.'_'.$loop, $arrayFieldVal)) {
                    $field_data = $arrayFieldVal[$filedRowId.'_'.$loop];
                    $loop++;
                    $order = $quantity + 1;
                    $formid = 4;
                }else if (array_key_exists($filedBookerId, $arrayFieldVal)) {
                    $field_data = $arrayFieldVal[$filedBookerId];
                    $order = 0;
                    $formid = 5;
                }else if (array_key_exists($filedBookerImg, $arrayFieldVal)) {
                    
                    if(!empty($_FILES['bookerimage']['tmp_name'])) {
                        $field_data[] = $_FILES['bookerimage']['name'];
                        $filetmp_name = $_FILES['bookerimage']['tmp_name'];
                        $order = 0;
                        $formid = 5;
                        $fileData = array();
                        $fileData = $_FILES['bookerimage'];
                        $fieldName = 'bookerimage';
                        $this->uploadFileForInvitation($fileData,$fieldName);
                    }else {
                        $field_data = array();
                    }
                }else if (array_key_exists($filedAttendeeImg, $arrayFieldVal)) {
                    
                    if(!empty($_FILES['attendeeimage']['tmp_name'])) {
                        $field_data[] = $_FILES['attendeeimage']['name'];
                        $filetmp_name = $_FILES['attendeeimage']['tmp_name'];
                        $order = $quantity + 1;
                        $formid = 4;
                        $fileData = array();
                        $fileData = $_FILES['attendeeimage'];
                        $fieldName = 'attendeeimage';
                        $this->uploadFileForInvitation($fileData,$fieldName);
                    }else {
                        $field_data = array();
                    }
                }else {
                    $field_data = array();
                }

                
                if(!empty($field_data)) {
                    $counter = $order;
                    foreach($field_data as $fieldValue) {
                        $insertDataAttendee['field_value']  = $fieldValue;
                        $insertDataAttendee['field_id']     = $fieldid;
                        $insertDataAttendee['booker_id']    = $bookerId;
                        $insertDataAttendee['form_id']      = $formid;
                        $insertDataAttendee['field_order']  = $counter;
                        //$insertDataAttendee['invitation_master_id']  = $masterId;
                        
                        $counter++;
                        $responce = $this->common_model->addDataIntoTabel($this->tableattendee,$insertDataAttendee);
                                    
                    }
                }
            }
            
            //~ if($is_ajax_request){
            //~ echo json_encode(array('msg'=> 'Saved successfully!','is_success'=>'true','url'=>'events/manageinvitation/'.$eventId,'bookerId'=>$bookerId));
            //~ }else{
            //~ $msgArray = array('msg'=>$msg,'is_success'=>'true');
            //~ set_global_messages($msgArray);
            //~ }
        }
        
       public function forgotpassword(){
            
            $inputEmail         = $this->input->post('forgotDelegateEmail');   
            $whereCondition     = array( 'email'=> $inputEmail );
            $responce           = $this->common_model->getDataFromTabel($this->tabledelegatereg,'*',$whereCondition); 

            if(!empty($responce)) {
                $responce   = $responce[0];
                $email      = $responce->email;
                $pass       = $responce->password;
                $eventId    = $responce->event_id;
                
                $eventUrl   = $this->getEventUrl($eventId); 
                $subject    = 'Your Delegate Password';
                $messageBody = 'For any modification in ticket attendee details please use following credentials : <br/><br/>
                Login Credentials :-<br/> Email : '.$email.'<br/>Password : '.$pass.'<br/><br/> Please find below your event URL link :<br/> Event Link : <a href="'.$eventUrl.'">'.$eventUrl.'</a><br/><br/>Regards,<br/>Team Newman';
                
                /* Sendgrid */
                $apiKey = ADMINNEWMAN_APIKEY; //:eventurl
                $this->load->helper('sendgrid');
                $admin_templateId = 'a39cd84a-f99c-4c2e-a442-b12c82c387c0'; //
                
                $paramArray = array(
                'sub' => array(':email'=>array($email),':password'=>array($pass)),
                'filters' => array('templates' => array('settings' => array('enable' => 1, 'template_id' => $admin_templateId)))
                );
                $tempSubject = $subject;
                $templateBody = $messageBody;
                $toId = $email;
                sendgrid_mail_template($admin_templateId,$tempSubject,$templateBody,$toId,$paramArray,$apiKey,eventFromEmail);
                
                
                redirect($eventUrl.'?q=1');
            }else {
                $this->load->library('user_agent');
                $eventUrl =  $this->agent->referrer();
                redirect($eventUrl.'?q=1');
            }
            
        }
        
        public function addInvitationPassword(){
            $data = array();
            $data['eventId']  = $this->input->post('eventId');
            $data['invitation_types_id'] = $this->input->post('invitation_types_id');
            $data['primarybg'] = $this->input->post('primarybg');
            $eventId = $data['eventId'];
            /* get event details from event table */
            
            $andwhere  = array('is_theme_used' => '1');
            $eventdetails = $this->events_model->geteventdetailswiththeme($eventId,$andwhere);
            $contactPersonEmail = $eventdetails->email; //contact person email
            if(!empty($contactPersonEmail)) {
                $data['contactEmail'] = $contactPersonEmail;
            }else {
                $data['contactEmail'] = 'theteam@newmanevents.com';
            }
            $this->load->view('forgotpopup',$data); 
        }
        
    public function checkPassAuthentication(){
        $eventId                = $this->input->post('eventId');
        $invitation_types_id    = $this->input->post('invitation_types_id');
        $password               = $this->input->post('invitation_pass');
        
        $whereCondition = array('event_id' => $eventId,'password' => $password, 'id' => $invitation_types_id);
        $responce   = $this->common_model->getDataFromTabel($this->tableinvitation,'*',$whereCondition);
        //echo $this->db->last_query();die;
        //print_r($responce);die;
        if(!empty($responce) && count($responce) > 0 ) {
            $this->session->set_userdata('password_access', '1');
            echo 'true';die;
        }else {
            echo 'false';
        }
    }
    
    public function isEmailExist(){
        $email = $this->input->post('email');
        
        $whereCondition = array('email' => $email);
        $responce   = $this->common_model->getDataFromTabel($this->tabledelegatereg,'*',$whereCondition);
        
        if(!empty($responce)){
            //$this->form_validation->set_rules('at6', 'This email already registered with this event please use another email', 'callback_custom_error_set');
            $errors = 'This email already registered with this event please use another email';
            echo json_message('msg',$errors,'is_success','false');die;
        }else{
            echo json_message('msg','success','is_success','true');die;
        }
    }
    
    public function error_page($status,$eventId){
        $data=array();
        $data['status'] = $status;
        $data['eventId'] = $eventId;
        $this->home_template->load('home_template','form_error',$data,TRUE);
    }

}



/* End of file events.php */
?>
