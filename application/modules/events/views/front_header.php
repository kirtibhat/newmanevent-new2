<?php 

$style='';
$secondary_color        = (!empty($eventdetails->secondary_color)) ? $eventdetails->secondary_color : '';
$primary_color          = (!empty($eventdetails->primary_color)) ? $eventdetails->primary_color : '';
$event_theme_screen_bg  = (!empty($eventdetails->event_theme_screen_bg)) ? $eventdetails->event_theme_screen_bg : '';

$link_highlighted_color = (!empty($eventdetails->event_theme_link_highlighted_color)) ? $eventdetails->event_theme_link_highlighted_color : '';
$event_theme_text_color = (!empty($eventdetails->event_theme_text_color)) ? $eventdetails->event_theme_text_color : '';
/* get logo image path */
$logo_image             = (!empty($eventdetails->logo_image)) ? $eventdetails->logo_image : '';
$logo_image_path        = (!empty($eventdetails->logo_image_path)) ? $eventdetails->logo_image_path : '';

$event_title            = (!empty($eventdetails->event_title)) ? $eventdetails->event_title : '';
$subtitle               = (!empty($eventdetails->subtitle)) ? $eventdetails->subtitle : '';
$event_venue            = (!empty($eventdetails->event_venue)) ? $eventdetails->event_venue : '';
$eventVenueAddress1     = (!empty($eventdetails->eventVenueAddress1)) ? $eventdetails->eventVenueAddress1 : '';
$starttime              = @date('d',strtotime($eventdetails->starttime));
$starttime              = (!empty($starttime)) ? $starttime : '';
$endtime                = @date('jS F Y',strtotime($eventdetails->endtime));
$endtime                = (!empty($endtime)) ? $endtime : '';

$isLogoPath             = base_url().$logo_image_path.$logo_image;
$imgstatus              = ((@fopen($isLogoPath,"r")==true) && ($logo_image!='')) ? 'true' : 'false';

if($imgstatus=='true') { 
    $logoImage = $isLogoPath;
    $imageStatus = '1';
}else {
    $style='margin-left: 56px;';
    $imageStatus = '0';
}

//echo $logoImage;
/* Set theme colors */
$primarycolor           = 'color:#'.$primary_color;
$primarycolorbg         = 'background-color:#'.$primary_color;

$secondarycolor         = 'color:#'.$secondary_color;
$secondarycolorbg       = 'background-color:#'.$secondary_color;

$eventthemescreencolor  = 'color:#'.$event_theme_screen_bg;
$eventthemescreenbg     = 'background-color:#'.$event_theme_screen_bg;

?>
<div class="page_content invitation_content graylight_color">
<!-- dashboard main--> 
<div class="page_content invitation_content conformation_content" style="<?php echo $eventthemescreenbg; ?>">
  <div class="container">
    <div class="invitation-logo-hdngsec common_prop" style="<?php echo $primarycolorbg; ?>">
      <?php if($imageStatus==1){ ?>
      <div class="col-3 logoFrontHead"> <img src="<?php echo $logoImage; ?>"> </div>
      <?php } ?>
      <div class="col-12" style="<?php echo $style; ?>">
        <h1 style="color:#<?php echo $link_highlighted_color; ?>"><?php echo $event_title; ?></h1>
        <h6 style="color:#<?php echo $event_theme_text_color; ?>"><?php echo $subtitle; ?></h6>
        <p style="color:#<?php echo $event_theme_text_color; ?>">
        <?php echo $event_venue; if(!empty($eventVenueAddress1)){ echo '<br/>'.$eventVenueAddress1; } ?></p>
        <?php if($endtime!='31st December 1969') { ?>
        <p style="color:#<?php echo $event_theme_text_color; ?>">
        <?php
        $dayS = date('d',strtotime($eventdetails->starttime)); 
        $dayE = date('d',strtotime($eventdetails->endtime)); 
        if($dayS == $dayE) {
            echo $endtime;
        }else {
         echo $starttime.' - '.$endtime ; 
        } 
         ?></p>
        <?php } ?>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="contentNavigation manager_setup circleMenueFront delegateNavigationSetup">
      <?php if($this->uri->segment(2)=='attendeeConfirmation') { ?>
      <ul>
        <li  > <a href="#"> <span class="xlarge_icon"> <i class="icon-user"></i>
        </span><span class="text circleMenueAction" >Invitation Details</span> </a> </li>
        
        <li class="medium_color" > <a href="#"> <span class="xlarge_icon" style="<?php echo $primarycolorbg; ?>"> <i class="icon-thumb"></i></span><span class="text" style="<?php echo $primarycolor; ?>">Confirmation</span> </a> </li>
      </ul>
      <?php }else { ?>
       <ul>
        <li  > <a href="#"> <span class="xlarge_icon" style="<?php echo $primarycolorbg; ?>"> <i class="icon-user"></i>
        </span><span class="text circleMenueAction" style="<?php echo $primarycolor; ?>">Invitation Details</span> </a> </li>
        <li class="medium_color" > <a href="#"> <span class="xlarge_icon"> <i class="icon-thumb"></i></span><span class="text">Confirmation</span> </a> </li>
      </ul> 
      <?php } ?> 
    </div>
  </div>
  <div class="container"> 

