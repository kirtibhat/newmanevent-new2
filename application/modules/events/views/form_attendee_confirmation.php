<?php             
if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
$whereSocialMedia       = array('event_id' => @$eventId);
$eventSocialMedia       = getDataFromTabel('event_social_media','website',$whereSocialMedia);
$eventSocialMedia       = $eventSocialMedia[0];
$unique_id='';
if(!empty($bookerTableDetails)) {
$bookerTableDetails = $bookerTableDetails[0];
$unique_id = (!empty($bookerTableDetails->unique_id)) ? $bookerTableDetails->unique_id : '';
$invitation_type_id = (!empty($bookerTableDetails->invitation_type_id)) ? $bookerTableDetails->invitation_type_id : '';
    $invitation_name = '';
    if(!empty($invitation_type_id)){
        $whereinvitations = array('id'=>$invitation_type_id);
        $invitationsDetail       = getDataFromTabel('nm_event_invitations','invitation_name',$whereinvitations);
        $invitationsDetail       = $invitationsDetail[0];
        $invitation_name = $invitationsDetail->invitation_name;
    }
}

$bookerEmail = (!empty(@$bookerDetails[3]->field_value)) ? @$bookerDetails[3]->field_value : ''; 
$isWaitList = (!empty($bookerTableDetails->isWaitList)) ? $bookerTableDetails->isWaitList : ''; 

$eventTitle     =   (!empty($eventdetails->event_title)) ? $eventdetails->event_title : '';  
$startDate      =   date('l jS F Y',strtotime($eventdetails->starttime));
$endDate        =   date('l jS F Y',strtotime($eventdetails->endtime));
$eventVenue     =   (!empty($eventdetails->event_venue)) ? $eventdetails->event_venue : ''; 
/* Define form fields */
$formAttendeeSetup = array(
'name'          => 'formAttendeeSetup',
'id'            => 'formAttendeeSetup',
'method'        => 'post',
'class'         => '',
'enctype'       => 'multipart/form-data'
);


$secondary_color        = (!empty($eventdetails->secondary_color)) ? $eventdetails->secondary_color : '';
$primary_color          = (!empty($eventdetails->primary_color)) ? $eventdetails->primary_color : '';
$event_theme_screen_bg  = (!empty($eventdetails->event_theme_screen_bg)) ? $eventdetails->event_theme_screen_bg : '';
/* Set theme colors */
$primarycolor           = 'color:#'.$primary_color;
$primarycolorbg         = 'background-color:#'.$primary_color;

$secondarycolor         = 'color:#'.$secondary_color;
$secondarycolorbg       = 'background-color:#'.$secondary_color;

$eventthemescreencolor  = 'color:#'.$event_theme_screen_bg;
$eventthemescreenbg     = 'background-color:#'.$event_theme_screen_bg;

$registration_type      = $this->input->get('rtype');
$registration_type      = (!empty($registration_type)) ? $registration_type : 1;
$field_order            = $this->input->get('field_order');
$field_order            = (!empty($field_order)) ? $field_order : 0;    
?>

<?php $this->load->view('front_header'); ?>

<?php echo form_open(base_url().'events/modifyAttendee',$formAttendeeSetup); ?>
<div class="conformation_content_inner_sec common_prop">
  <div class="conformation-text">
        	<div class="container">
            	
                <?php if($isWaitList==0) { ?>
                <div class="row">
                	<div class="col-15 first-hedng common_prop">
                    	<h1>You're going to the <?php echo $eventTitle; ?>!</h1>
                    </div>
                    <div class="col-15 scnd-hedng common_prop">
                    	<div class="lft-txt-mrgin">
                    	<p>ID# <?php echo $unique_id; ?>
                        <?php if($this->input->get('q')!='true') { ?>
                        <span>Your tickets have been sent to <b><?php echo $bookerEmail; ?></b></span></p>
                        <?php } ?>
                        </div>
                    </div>
                </div>
                <?php }else { ?>
                 <div class="row">
                	<div class="col-15 first-hedng common_prop">
                    	<h1>You're details have been added to the WAITLIST!</h1>
                    </div>
                    <div class="col-15 scnd-hedng common_prop">
                    	<div class="lft-txt-mrgin">
                    	<p>ID# <?php echo $unique_id; ?>
                        <span>A confirmation email will sent to <b><?php echo $bookerEmail; ?></b> once your ticket are <br/>avaliable.
                            Please ensure you confirm your tickets otherwise they will be replaced. </span></p>
                        </div>
                    </div>
                </div>   
                <?php } ?> 
            
            </div>
        </div>  
  <div class="col-9 responsive-pedng">
    <div class="panel open" id="panel0">
      <div class="panel_header" style="<?php echo $primarycolorbg; ?>">
      <?php if($this->input->get('q')=='true') { ?>
            Update Confirmation
      <?php }else { 
                if($isWaitList==0) { echo "Confirmation"; }else { echo "Waitlist Confirmation"; } ?>
      <?php } ?>        
      </div>
      <div class="panel_contentWrapper" style="display:block;">
        <!--sub menu one-->
        
          <div class="panel_content responsive_content">
            <?php  if($registration_type!=2) { ?>
            <div class="col-5 contact-txt">
                <?php 
                $attCounter = 0;
                $nameBooker ='';
                foreach($bookerDetails as $booker) { 
                    if($attCounter<=2) { 
                           $nameBooker .= $booker->field_value.' ';     
                           if($attCounter==2) {
                               echo "<h3>".$nameBooker."</h3>";
                           }
                       }else {
                            echo "<p>".$booker->field_value.'</p>';
                        }
                    $attCounter++;
                }
                ?>
                
            </div>
            <?php } ?>
            <div class="col-3 bookng-id-txt">
                
                <p><?php echo $invitation_name ?></p>
                <div class="bookng-id common_prop">
                <P>ID# <?php echo $unique_id; ?></P></div>
              
            </div>  
              
            <?php if($registration_type==1) { ?>
            <div class="col-8 attendees-one">
            <?php 
                $counter = 1;
                $loop = 0;
                foreach($attendeeDetails as $attendee) {
                    $att = 0;
                    $name ='';
                    echo "<h5 class='mT10'>ATTENDEE - ".$counter."</h5>";
                    if($attendee) {
                    foreach($attendee as $attendeevalue) { 
                       if($att<=2) { 
                           $name .= $attendeevalue->field_value.' ';     
                           if($att==2) {
                               echo "<p>".$name."</p>";
                           }
                       }else {
                            echo "<p>".$attendeevalue->field_value.'</p>';
                        }
                        $att++;
                    }
                    echo "<p>Dietary Requirements : ".$attendeevalue->dietary.'</p>';
                    
                }
                    $counter++;
                    $loop++;
                }
            ?>    
             </div>
             
            <?php }else{ ?>
            <div class="col-8 attendees-one">
            <?php 
                $counter = 1;
                $loop = 0;
                $status = 0;
                foreach($attendeeDetails as $attendee) {
                    
                    $att = 0;
                    $name ='';
                    if($status==0){
                        echo "<h5 class='mT10'>ATTENDEE DETAILS</h5>";
                    }
                    if($attendee) {
                    foreach($attendee as $attendeevalue) {
                       if($field_order==$attendeevalue->field_order){  
                       $status = 1;
                       if($att<=2) { 
                           $name .= $attendeevalue->field_value.' ';     
                           if($att==2) {
                               echo "<p>".$name."</p>";
                           }
                       }else {
                            echo "<p>".$attendeevalue->field_value.'</p>';
                        }
                        $att++;
                      }
                    }
                    if($status==1) {
                        echo "<p>Dietary Requirements : ".$attendeevalue->dietary.'</p>';
                    }
                    $status++;
                }
                    $counter++;
                    $loop++;
                }
            ?>    
             </div>
             
            <?php } ?> 
             
             <?php  if($registration_type!=2) { ?>       
             <div class="col-8 social-share">
                        
                <p>Share with friends</p>
                <ul>
                    <a href="javascript:void(0)" onclick="publishOnFacebook();">
                      <li><span class="large_icon "> <i class="icon-facebook"></i> </span></li>
                    </a>
                    <a href="#" class="shareOnTwitter">
                      <li><span class="large_icon"> <i class="icon-twitter"></i> </span></li>
                    </a>
                    <a href="#" class="shareOnLinkdn">
                      <li><span class="large_icon"> <i class="icon-user"></i> </span></li>
                    </a>
                </ul>
                 
            </div>           	
             <?php } ?>
          </div>
          <!--3rdpannel end--> 
          <!--4rd--> 
          <!--4rdpannel end--> 
          <!--5rd--> 
          <!--5rdpannel end-->
          <div class="panel_footer">
            <div class="pull-right">
                <input type="hidden" name="bookerId" value="<?php echo $bookerId; ?>">
                <input type="hidden" name="quantity" value="<?php echo $quantity; ?>">
                <input type="hidden" name="eventId"  value="<?php echo $eventId; ?>">
                <input type="hidden" name="registration_type"  value="<?php echo $registration_type; ?>">
                <input type="hidden" name="field_order"  value="<?php echo $field_order; ?>">
                
            </div>
          </div>
        
      </div>
    </div>
  </div>
  <!--
  <div class="col-6 venuedetail_rightPart">
    <p><?php echo lang('who_else_txt'); ?></p>
    
    <div class="col-fluid-15 mT10">
      <div class="col-fluid-7 responsive-sec">
        <div class="center_content"> <span><img src="<?php echo IMAGE; ?>user.jpg" ></span> <span class="lrg-font">Joshua Mcfadden<span class="sml-font">Google Inc.</span></span> </div>
      </div>
      <div class="col-fluid-7 responsive-sec">
        <div class="center_content"> <span><img src="<?php echo IMAGE; ?>user.jpg" ></span> <span class="lrg-font">Joshua Mcfadden<span class="sml-font">Google Inc.</span></span> </div>
      </div>
    </div>
    <div class="col-fluid-15 mT10">
      <div class="col-fluid-7 responsive-sec">
        <div class="center_content"> <span><img src="<?php echo IMAGE; ?>user.jpg" ></span> <span class="lrg-font">Joshua Mcfadden<span class="sml-font">Google Inc.</span></span> </div>
      </div>
      <div class="col-fluid-7 mT25 responsive-sec">
        <div class="center_content"> <span><a href="#">More..</a></span> </div>
      </div>
    </div>
  </div>
  -->
  <!-- Register Attendee -->
  
  <div class="col-15 footer_bg">
    
  </div>
  <div class="col-15">
    <div class="container">
      <div class="invitation_content_rgeisterbox common_prop">
<!--
        <button class="btn-normal btn pull-none rigisterbtnAction">
          <div class="lok">Register</div>
        </button>
-->
        <input type="submit" value="Modify Response" class="btn-normal btn pull-none">
        <?php 
            if(empty($eventSocialMedia->website) || $eventSocialMedia->website==''){ 
                $eventUrl = base_url()."events/invitation/".$eventdetails->event_url;
            }else{
                $eventUrl = "http://".$eventSocialMedia->website;
            }
            
        ?>
        <a href="<?php echo $eventUrl; ?>"><input type="button" value="Done" class="btn-normal btn pull-none"></a>
      </div>
    </div>
  </div>
 
 <!-- End Registartion -->
</div>
<?php echo form_close();?>
<?php $this->load->view('front_footer'); ?>   

<!--
$eventTitle $startDate $endDate  $eventVenue
-->

<script>
function publishOnFacebook() {
    FB.ui({
            method: 'feed',
            name: '<?php echo $eventTitle; ?>',
            link: 'https://developers.facebook.com/docs/dialogs/',
            href: baseUrl,
            picture: 'http://newmanevents.com/themes/assets/images/dashboard_event.png',
            caption: 'NEWMAN : Simple Secure Surpricing',
            description: 'START DATE : <?php echo $startDate; ?> END DATE : <?php echo $endDate; ?>  VANUE : <?php echo $eventVenue; ?>  If your event is free to attend, then this system is free as well. You might have a wedding, a Board meeting or a party to organise and this system lets you send out invitations and to monitor responses.'
        });
}

(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.5&appId=837973882987543";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

$(document).ready(function(){
	$('.shareOnTwitter').click(function(event) {
    var width  = 575,
        height = 400,
        left   = ($(window).width()  - width)  / 2,
        top    = ($(window).height() - height) / 2,
        url    = 'http://twitter.com/home/?status=<?php echo $eventTitle; ?>  START DATE : <?php echo $startDate; ?> END DATE : <?php echo $endDate; ?>  VANUE : <?php echo $eventVenue; ?>  If your event is free to attend, then this system is free as well. You might have a wedding, a Board meeting or a party to organise and this system lets you send out invitations and to monitor responses.',
        //text	= '',
        opts   = 'status=1' +
                 ',width='  + width  +
                 ',height=' + height +
                 ',top='    + top    +
                 ',left='   + left;
    
    window.open(url, 'twitter', opts);
 
    return false;
  });
  
  	$('.shareOnLinkdn').click(function(event) {
    var width  = 575,
        height = 400,
        left   = ($(window).width()  - width)  / 2,
        top    = ($(window).height() - height) / 2,
        url    = 'http://www.linkedin.com/shareArticle?mini=true&submitted-image-url=http://newmanevents.com/themes/assets/images/dashboard_event.png&url=http://localhost/newmanevents&title=<?php echo $eventTitle; ?>&summary=START DATE : <?php echo $startDate; ?> END DATE : <?php echo $endDate; ?>  VANUE : <?php echo $eventVenue; ?>  If your event is free to attend, then this system is free as well. You might have a wedding, a Board meeting or a party to organise and this system lets you send out invitations and to monitor responses.&source=newmanevenrs.com',
        //text	= '',
        opts   = 'status=1' +
                 ',width='  + width  +
                 ',height=' + height +
                 ',top='    + top    +
                 ',left='   + left;
    
    window.open(url, 'twitter', opts);
 
    return false;
  });
  
});
</script>

