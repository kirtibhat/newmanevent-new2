<?php 

$eventid = currentEventId();            
if(!is_numeric($eventid)) {
    if(!empty($eventid)) {
        $eventId = decode($eventid);
    }else {
        $eventId = $this->session->userdata("eventId");
    }
}else {
    if(empty($eventId)) {
        $eventId = $this->session->userdata("eventId");
    }
}

$http='http://';
$whereSocialMedia   = array('event_id' => $eventId);
$eventSocialMedia   = getDataFromTabel('event_social_media','*',$whereSocialMedia);

$whereSocialMedia1   = array('event_id' => $eventId);
$customSocialMedia   = getDataFromTabel('event_social_media_custom','*',$whereSocialMedia1);

$eventSocialMedia   = $eventSocialMedia[0]; 
$event_website      = (!empty($eventSocialMedia->website)) ? $http.$eventSocialMedia->website : '';
$linkedIn           = (!empty($eventSocialMedia->linkedIn)) ? $http.$eventSocialMedia->linkedIn : '';
$facebook           = (!empty($eventSocialMedia->facebook)) ? $http.$eventSocialMedia->facebook : '';
$twitter            = (!empty($eventSocialMedia->twitter)) ? $http.$eventSocialMedia->twitter : '';
$youtube            = (!empty($eventSocialMedia->youtube)) ? $http.$eventSocialMedia->youtube : '';
$pintrest           = (!empty($eventSocialMedia->pintrest)) ? $http.$eventSocialMedia->pintrest : '';
$instagram          = (!empty($eventSocialMedia->instagram)) ? $http.$eventSocialMedia->instagram : '';

?>
<div class="invitation-footer-mainarea common_prop">
  <div class="col-10">
    <div class="container">
      <div class="invitation-footer">
        <div class="ftr-inner common_prop"><span><img src="<?php echo IMAGE; ?>footer_txt.png"></span></div>
      </div>
    </div>
  </div>
  <div class="col-5">
    <div class="container">
      <div class="invitation-right-footer">
        <div class="ftr-inner">
            
            <ul class="as_small_social">
            <?php if(!empty($facebook)) { ?>
            <li class="as_fb mediaIcons">
                <a href="<?php echo $facebook; ?>" target="_blank"><img src="<?php echo IMAGE; ?>fb.png"></a>
            </li> 
            <?php } ?>
            
            <?php if(!empty($twitter)) { ?>
            <li class="as_twt">
                <a href="<?php echo $twitter; ?>" target="_blank"><img src="<?php echo IMAGE; ?>twt.png"></a>
            </li>
            <?php } ?>
            
            <?php if(!empty($linkedIn)) { ?>
            <li class="as_lin">
                <a href="<?php echo $linkedIn; ?>" target="_blank"><img src="<?php echo IMAGE; ?>lin.png"></a>
            </li>
            <?php } ?>
            
            <?php if(!empty($youtube)) { ?>
            <li class="like_youtube">
                <a href="<?php echo $youtube; ?>" target="_blank"><img src="<?php echo IMAGE; ?>youtube.png"></a>
            </li>  
            <?php } ?>
            
            <?php if(!empty($pintrest)) { ?>
            <li class="like_pinterset">
                <a href="<?php echo $pintrest; ?>" target="_blank"><img src="<?php echo IMAGE; ?>pinterest.png"></a>
            </li>             
            <?php } ?>
             
            <?php if(!empty($instagram)) { ?>                                         
            <li class="like_instagram">
                <a href="<?php echo $instagram; ?>" target="_blank"><img src="<?php echo IMAGE; ?>instagram.png"></a>
            </li>                                                      
            <?php } ?>
            
            <?php 
            if($customSocialMedia){
            foreach($customSocialMedia as $media) { 
                if(!empty($media->media_value)) {
                $name = $media->media_name;
            ?>
            <li class="customLi">
                <a href="<?php echo $http.$media->media_value; ?>" target="_blank" class="customLia"><?php echo ucfirst($name[0]); ?></a>
            </li>
            
            <?php } } } ?>
            </ul>
          
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade terms_popup" style="display:none;">
	<div class="modal-dialog modal-sm-as">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Terms & Condition</h4>
			</div>
			
			<div class="infobar">
				<p class="info_Status"></p>
				<a class="info_btn" onclick="return false;">
				</a>
			</div>
			<div class="modal-body">
				<div class="modal-body-content">
				<div class="clearFix display_inline w_100 mT10"></div>
					<div class="row">	
					<?php echo lang('term_condition_text');?>
					</div>
				</div>
				<div class="modal-footer">
					<div class="pull-right">
						<button class="btn-normal btn accept_event" tabindex='4' data-dismiss="modal">Accept</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<style>
.as_small_social li {   width: 38px; height: 38px; }
.conformation_content img { border-top-left-radius: 0px; }
.customLi { background: #818282;border-radius: 20px 20px 20px 20px;padding: 2px 0px 0px 8px; }
.customLia { color: #fff;font-size: 32px;font-weight: bold; }
</style>

<script>
$('document').ready(function(){
    $('.mainNavWrapper').css('display','none');
});
</script>

