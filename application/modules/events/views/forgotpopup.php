<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$formInvitationPass = array(
    'name'          => 'formInvitationPass',
    'id'            => 'formInvitationPass',
    'method'        => 'post',
    'class'         => 'wpcf7-form ',
);
$invitation_pass = array(
    'name'          => 'invitation_pass',
    'id'            => 'invitation_pass',
    'type'          => 'password',
    'class'         => 'xxlarge_input trim_check',
    'autocomplete'  => 'off',
);   

  
?>

 <div id="invitation_password_popup" class="modal fade addField_popup in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
         <div class="modal-dialog">
            <div class="modal-content">
               <div class="modal-header" style="<?php echo $primarybg; ?>">
                  <h4 class="modal-title">Password Required</h4>
               </div>
               <div class="infobar">
        <p class="dn" id="general_event_setup_info">A password is required to access this invitation type. 
        Please email <?php echo $contactEmail; ?> if you met the requirements but have not received/ lost/ forgot the password. </p>
        <span class="info_btn" onclick="showHideInfoBar('general_event_setup_info');"></span>
    </div>
               <?php  echo form_open(base_url().'events/checkPassAuthentication',$formInvitationPass); ?>
                   <div class="modal-body">
                      <div class="modal-body-content">
                        
                        <div class="row">
                            <div class="labelDiv pull-left">
                               <label class="form-label text-right" for="field_name">Password
                               <span aria-required="true" class="required">*</span></label>
                            </div>
                            <?php echo form_input($invitation_pass); ?>			
                            <span id="error-invitation_pass"></span>
                            <input type="hidden" id="invitationEventId" name="eventId" value="<?php echo $eventId; ?>">
                            <input type="hidden" id="invitationTypeId" name="invitation_types_id" value="<?php echo $invitation_types_id; ?>">
                        </div>
                      
                      </div>
                      
                      <div class="modal-footer">
                         <div class="pull-right">
                             <a href="javascript:void(0);" onClick="window.location.href=window.location.href;" class="btn-normal btn">Reset</a>
                            <input type="button" value="Done" class="btn-normal btn authenticationevent">
                            <a href="#" class="popup_cancel" data-dismiss="modal"></a>
                         </div>
                      </div>
                      
                   </div>
                      
                <?php echo form_close();?>
            </div>
         </div>
      </div>
<script>     
 $(document).ready(function(){
     $('#invitation_pass').keypress(function(event){
        if (event.which == 13) { 
             event.preventDefault();
            $('.authenticationevent').trigger('click');
        }
     });    
    });   
$('.authenticationevent').click(function(){
    
var invitationTypeId    = $('#invitationTypeId').val();
var invitationEventId   = $('#invitationEventId').val();
var passwordRequired    = $('#invitation_pass').val();
    $.ajax({
        type:"post",
        dataType:"html",
        url: baseUrl+'events/checkPassAuthentication',
        data: "eventId="+invitationEventId+"&invitation_types_id="+invitationTypeId+"&invitation_pass="+passwordRequired,
            
        success: function(response)
        {
            console.log(response);
            if(response=='true') { 
                $('#passwordCheckStatus').val(1); 
                $('.popup_cancel').trigger('click');
                //window.location.href = window.location.href;
            }else {
               $('#error-invitation_pass').html('Password not matched');
               return false;
            } 
        },
        error: function()
        {						
            alert('Error while request..'); 
        }
    }); 
    return false;
});  
</script>
