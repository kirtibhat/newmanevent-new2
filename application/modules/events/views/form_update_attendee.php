<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
/* Define form fields */
$formUpdateAttendeeSetup = array(
'name'          => 'formUpdateAttendeeSetup',
'id'            => 'formUpdateAttendeeSetup',
'method'        => 'post',
'class'         => '',
'enctype'       => 'multipart/form-data'
);

$secondary_color        = (!empty($eventdetails->secondary_color)) ? $eventdetails->secondary_color : '';
$primary_color          = (!empty($eventdetails->primary_color)) ? $eventdetails->primary_color : '';
$event_theme_screen_bg  = (!empty($eventdetails->event_theme_screen_bg)) ? $eventdetails->event_theme_screen_bg : '';

/* Set theme colors */
$primarycolor           = 'color:#'.$primary_color;
$primarycolorbg         = 'background-color:#'.$primary_color;

$secondarycolor         = 'color:#'.$secondary_color;
$secondarycolorbg       = 'background-color:#'.$secondary_color;

$eventthemescreencolor  = 'color:#'.$event_theme_screen_bg;
$eventthemescreenbg     = 'background-color:#'.$event_theme_screen_bg;

//echo $registration_type; 
//echo $field_order; 

?>
<?php $this->load->view('front_header'); ?>
<div class="invitation_content_inner_sec common_prop">
  <div class="col-9 responsive-pedng">
    <div class="panel open" id="panel0">
      <div class="panel_header" style="<?php echo $primarycolorbg; ?>">Edit Response</div>
      <div class="panel_contentWrapper" style="display:block;">
        <!--sub menu one-->
        <?php echo form_open(base_url().'events/updateAttendee',$formUpdateAttendeeSetup); ?>
          <?php  if($registration_type!=2) { ?> 
          <!-- Invitation type & quantity panel -->
          <div class="panel_content responsive_content">
            <!-- Section -1 -->
            <div class="row">
              <div class="col-3">
                <div class="labelDiv">
                  <label class="form-label text-right"><?php echo lang('invitation').' '.lang('type'); ?></label>
                </div>
              </div>
              <div class="col-5">
                <?php $count = 0;
                if(!empty($fieldIdsattendee)) { $count = count($fieldIdsattendee); } ?>  
                <select class="custom-select medium_select isAllowwaitlist" id="invitation_types" name="invitation_types">
                    
                    <?php foreach($eventInvitations as $inv_value) { 
                           $limit_per_invitation = $inv_value->limit_per_invitation; 
                           $limit_per_invitation = $limit_per_invitation - $count;
                        ?>
                            <option value="<?php echo $inv_value->id; ?>" quantity="<?php echo $limit_per_invitation; ?>"><?php echo $inv_value->invitation_name; ?>
                            </option>
                    <?php } ?>    
                </select>
                <span id="error-dinner_dance6"></span> </div>
            </div>
            <div class="row">
              <div class="col-3">
                <div class="labelDiv">
                  <label class="form-label text-right"><?php echo lang('quantity'); ?></label>
                </div>
              </div>
              <div class="col-5">
                <div class="stepper ">
                    <input id="invitation_quantity"  class="xsmall_input dark stepper-input pull-left" type="number" readonly  required="" value="1" name="field_qty">
                    <span class="small_icon stepper-step "> 
                    <i class="icon-uparrow step-personal up"></i>
                    <i class="icon-downarrow step-personal down"></i>
                    </span>
                 </div>
                <span id="error-dinner_dance6"></span> 
              </div>
            </div>
            <!-- Section -1 End -->
          </div>
          
          <div class="sub_banner" style="<?php echo $secondarycolorbg; ?>">
            <h1><?php echo lang('booker_txt'); ?></h1>
          </div>
          <div class="infobar">
            <p class="dn" id="booker_form_info" style="display: none;">
                The booker is the person responding to the invitation and the invoice is addressed to.</p>
            <span class="info_btn" onclick="showHideInfoBar('booker_form_info');"></span>
          </div>
          <div class="panel_content responsive_content">
           <?php if($fieldIdsbooker) { foreach($fieldIdsbooker as $bookers) { ?>
                   <?php $fieldType     = getDataByParam('field_type',array('id'=> $bookers->field_id)); ?>        
                   <?php $defaultValue  = getDataByParam('default_value',array('id'=> $bookers->field_id)); ?>
                   <?php $field_name    = getDataByParam('field_name',array('id'=> $bookers->field_id)); ?>
                   <?php $field_value   = (!empty($bookers->field_value)) ? $bookers->field_value : ''; ?>
                   <?php $field_status  = getDataByParam('field_status',array('id'=> $bookers->field_id)); ?> 
                         
                    <div class="row">
                        <div class="col-3">
                            <div class="labelDiv">
                              <label class="form-label text-right"><?php echo ucwords($field_name->field_name); ?>
                              <?php $isMandatory='false'; 
                              if($field_status->field_status=='2' || $field_name->field_name=='email') { $isMandatory='true'; ?>
                              <span class="required" aria-required="true">*</span>
                              <?php } ?>
                              </label>
                            </div>
                        </div>
                        <div class="col-5">
                            <?php echo form_hidden('fieldidbooker[]', $bookers->id);  ?>
                            <?php if($fieldType->field_type=='selectbox') { ?>
                                <select class="custom-select medium_select" id="booker_title" name="bookeridvalue[<?php echo $bookers->id; ?>]" mandatory="<?php echo $isMandatory; ?>">
                                <?php if( empty($defaultValue->default_value) || ($defaultValue->default_value == 'NULL') )  { ?>
                                    <option value="Miss" <?php if($field_value=='Miss') { echo 'selected'; } ?>>Miss</option>
                                    <option value="Ms" <?php if($field_value=='Ms') { echo 'selected'; } ?>>Ms</option>
                                    <option value="Mrs" <?php if($field_value=='Mrs') { echo 'selected'; } ?>>Mrs</option>
                                    <option value="Mr" <?php if($field_value=='Mr') { echo 'selected'; } ?>>Mr</option>
                                <?php }else {  
                                    $optionsArr    = json_decode($defaultValue->default_value);
                                    if($optionsArr) {
                                        foreach($optionsArr as $option) { ?>
                                            <option value="<?php echo $option; ?>" <?php if($field_value==$option) { echo 'selected'; } ?>><?php echo $option; ?></option>
                                <?php   }
                                    } 
                                } ?>
                                </select>
                            <?php }else if( $fieldType->field_type=='text' ) { ?>
                                
                                <?php if($field_name->field_name=='email' || $field_name->field_name=='organisation') { $inputClass='large_input'; }else { $inputClass='medium_input'; }   ?>
                                
                                <input type="text" name="bookeridvalue[<?php echo $bookers->id; ?>]" class="<?php echo $inputClass; ?>" value="<?php echo $field_value; ?>" mandatory="<?php echo $isMandatory; ?>" lang="<?php echo $field_name->field_name; ?>">
                            
                            <?php }else if( $fieldType->field_type=='radio' ) { ?>
                                    <?php 
                                    $radioOptionsArr = json_decode($defaultValue->default_value);
                                    $opn = 1; 
                                    foreach($radioOptionsArr as $radio) {
                                    ?>
                                    <div class="radioDiv">
                                    <input type="radio" name="bookeridvalue_<?php echo $opn; ?>[<?php echo $bookers->id; ?>]" value="<?php echo $radio; ?>" id="booker_radio_field" class="radioButton" 
                                    <?php if($field_value==$radio) { echo 'checked'; } ?>>        
                                    <label class="form-label pull-left" for="booker_radio_field<?php echo $opn; ?>"><span><?php echo $radio; ?></span></label>
                                    </div>
                                    <?php  $opn++; } ?> 
                                    
                            <?php }else if($fieldType->field_type=='file') { ?> 
                                <div class="custom-upload">
                                    <span class="label_text">Select or Drop File</span>
                                    <input type="file" class="customIinputFileLogo" id="bookerimage" accept="image/*" name="booker_image"  draggable="true">
                                    <div class="display_file">
                                        <span class="file_value"></span>
                                        <a href="javascript:void(0)" class="clear_value"></a>
                                    </div>
                                </div>
                            <?php } ?>     
                            <div class="clearFix"></div>              
                            <span id="error-<?php //echo $bookers->id; ?>" class="error"></span> 
                        </div>
                    </div>
                   <?php } } ?>    
          </div>
          
          <?php } ?>
          
          <div class="sub_banner" style="<?php echo $secondarycolorbg; ?>">
            <h1><?php echo lang('attendee_txt'); ?></h1>
          </div>
          <div class="infobar">
            <p class="dn" id="attendee_form_info" style="display: none;">
                The attendee is the person attending the event.The below details will appear on the each tickets.</p>
            <span class="info_btn" onclick="showHideInfoBar('attendee_form_info');"></span>
            </div>
          <?php if($registration_type==1) { ?>
          <div class="panel_content responsive_content rowB customPadd">
            <?php $counter = 0; $loop=0; 
                    if($fieldIdsattendee) { 
                        foreach($fieldIdsattendee as $bookers) { ?>
                    
                    <div class="row setRow">
                        <div class="mB10 display_inline gap25">    
                    <?php foreach($bookers as $attendee) { ?>
                    <?php $fieldType     = getDataByParam('field_type',array('id'=> $attendee->field_id)); ?>        
                    <?php $defaultValue  = getDataByParam('default_value',array('id'=> $attendee->field_id)); ?>
                    <?php $field_name    = getDataByParam('field_name',array('id'=> $attendee->field_id)); ?>
                    <?php $field_value   = (!empty($attendee->field_value)) ? $attendee->field_value : ''; ?>
                    <?php $fieldOrder    = (!empty($attendee->field_order)) ? $attendee->field_order : ''; ?>
                         
                    <div class="row">
                        <div class="col-3">
                            <div class="labelDiv">
                              <label class="form-label text-right"><?php echo ucwords($field_name->field_name); ?></label>
                            </div>
                        </div>
                        <div class="col-5">
                            <?php echo form_hidden('fieldidattendee[]', $attendee->id);  ?> 
                            
                            <?php if($fieldType->field_type=='selectbox') { ?>
                                <select class="custom-select medium_select" id="booker_title" name="attendee_value[<?php echo $attendee->id; ?>]">
                                <option value="" selected="selected"></option>
                                <?php if( empty($defaultValue->default_value) || ($defaultValue->default_value == 'NULL') )  { ?>
                                    <option value="Miss" <?php if($field_value=='Miss') { echo 'selected'; } ?>>Miss</option>
                                    <option value="Ms" <?php if($field_value=='Ms') { echo 'selected'; } ?>>Ms</option>
                                    <option value="Mrs" <?php if($field_value=='Mrs') { echo 'selected'; } ?>>Mrs</option>
                                    <option value="Mr" <?php if($field_value=='Mr') { echo 'selected'; } ?>>Mr</option>
                                <?php }else {  
                                    $optionsArr    = json_decode($defaultValue->default_value);
                                    if($optionsArr) {
                                        foreach($optionsArr as $option) { ?>
                                            <option value="<?php echo $option; ?>" <?php if($field_value==$option) { echo 'selected'; } ?>><?php echo $option; ?></option>
                                <?php   }
                                    } 
                                } ?>
                                </select>
                            <?php }else if( $fieldType->field_type=='text' ) { ?>
                                <input type="text" name="attendee_value[<?php echo $attendee->id; ?>]" class="medium_input" value="<?php echo $field_value; ?>">
                            
                            <?php }else if( $fieldType->field_type=='radio' ) { ?>
                                    <?php 
                                    $radioOptionsArr = json_decode($defaultValue->default_value);
                                    $opn = 1; 
                                    foreach($radioOptionsArr as $radio) {
                                    ?>
                                    <div class="radioDiv">
                                    <input type="radio" name="attendee_value_<?php echo $opn; ?>[<?php echo $attendee->id; ?>]" value="<?php echo $radio; ?>" id="booker_radio_field" class="radioButton" 
                                    <?php if($field_value==$radio) { echo 'checked'; } ?>>        
                                    <label class="form-label pull-left" for="booker_radio_field<?php echo $opn; ?>"><span><?php echo $radio; ?></span></label>
                                    </div>
                                    <?php  $opn++; } ?> 
                                    
                            <?php }else if($fieldType->field_type=='file') { ?> 
                                <div class="custom-upload">
                                    <span class="label_text">Select or Drop File</span>
                                    <input type="file" class="customIinputFileLogo" id="bookerimage" accept="image/*" name="<?php echo $attendee->id; ?>"  draggable="true">
                                    <div class="display_file">
                                        <span class="file_value"></span>
                                        <a href="javascript:void(0)" class="clear_value"></a>
                                    </div>
                                </div>
                            <?php } ?>           
                            <div class="clearFix"></div>        
                             <span id="error-<?php //echo $attendee->id; ?>" class="error"></span> 
                        </div>
                    </div>
                   <?php $counter++; } ?>
                   <div class="row">
                      <div class="col-3">
                        <div class="labelDiv">
                          <label class="form-label text-right">Dietary Requirements</label>
                        </div>
                      </div>
                      <div class="col-5">     
                        <?php $dietary = $bookers[$loop]->dietary; ?>
                        <select class="custom-select medium_select" id="field_dietary1" name="field_dietary[]">
                            <option value="" selected="selected"></option>
                            <?php foreach($dietaryReq as $dietaryVal) { 
                                  if($dietaryVal!='None' && $dietaryVal!='Other') {  
                                ?>
                                <option value="<?php echo $dietaryVal; ?>" <?php if($dietary==$dietaryVal) { echo 'selected'; } ?>><?php echo $dietaryVal; ?></option>
                            <?php } } ?>
                                <option value="Other">Other</option>
                                <option value="None">None</option>    
                            </select>
                            <div class="clearFix"></div>
                         <span id="error-<?php //echo $fieldType->id; ?>" class="error"></span> 
                    </div>
                    </div>
                   </div>
                   </div>
                   <hr/>
                  <?php $loop++; } }  ?>   
          </div>
          <?php }else { ?>
           
           <div class="panel_content responsive_content rowB customPadd">
            <?php $counter = 0; $loop=0; $status=0;
                    if($fieldIdsattendee) { 
                        foreach($fieldIdsattendee as $bookers) { ?>
                    
                    <div class="row setRow">
                        <div class="mB10 display_inline gap25">    
                    <?php foreach($bookers as $attendee) { ?>
                    <?php $fieldType     = getDataByParam('field_type',array('id'=> $attendee->field_id)); ?>        
                    <?php $defaultValue  = getDataByParam('default_value',array('id'=> $attendee->field_id)); ?>
                    <?php $field_name    = getDataByParam('field_name',array('id'=> $attendee->field_id)); ?>
                    <?php $field_value   = (!empty($attendee->field_value)) ? $attendee->field_value : ''; ?>
                    <?php $fieldOrder    = (!empty($attendee->field_order)) ? $attendee->field_order : ''; ?>
                    
                    <?php if($field_order==$fieldOrder) { $status = 1; ?>     
                    <div class="row">
                        <div class="col-3">
                            <div class="labelDiv">
                              <label class="form-label text-right"><?php echo ucwords($field_name->field_name); ?></label>
                            </div>
                        </div>
                        <div class="col-5">
                            <?php echo form_hidden('fieldidattendee[]', $attendee->id);  ?> 
                            
                            <?php if($fieldType->field_type=='selectbox') { ?>
                                <select class="custom-select medium_select" id="booker_title" name="attendee_value[<?php echo $attendee->id; ?>]">
                                <option value="" selected="selected"></option>
                                <?php if( empty($defaultValue->default_value) || ($defaultValue->default_value == 'NULL') )  { ?>
                                    <option value="Miss" <?php if($field_value=='Miss') { echo 'selected'; } ?>>Miss</option>
                                    <option value="Ms" <?php if($field_value=='Ms') { echo 'selected'; } ?>>Ms</option>
                                    <option value="Mrs" <?php if($field_value=='Mrs') { echo 'selected'; } ?>>Mrs</option>
                                    <option value="Mr" <?php if($field_value=='Mr') { echo 'selected'; } ?>>Mr</option>
                                <?php }else {  
                                    $optionsArr    = json_decode($defaultValue->default_value);
                                    if($optionsArr) {
                                        foreach($optionsArr as $option) { ?>
                                            <option value="<?php echo $option; ?>" <?php if($field_value==$option) { echo 'selected'; } ?>><?php echo $option; ?></option>
                                <?php   }
                                    } 
                                } ?>
                                </select>
                            <?php }else if( $fieldType->field_type=='text' ) { ?>
                                <input type="text" name="attendee_value[<?php echo $attendee->id; ?>]" class="medium_input" value="<?php echo $field_value; ?>">
                            
                            <?php }else if( $fieldType->field_type=='radio' ) { ?>
                                    <?php 
                                    $radioOptionsArr = json_decode($defaultValue->default_value);
                                    $opn = 1; 
                                    foreach($radioOptionsArr as $radio) {
                                    ?>
                                    <div class="radioDiv">
                                    <input type="radio" name="attendee_value_<?php echo $opn; ?>[<?php echo $attendee->id; ?>]" value="<?php echo $radio; ?>" id="booker_radio_field" class="radioButton" 
                                    <?php if($field_value==$radio) { echo 'checked'; } ?>>        
                                    <label class="form-label pull-left" for="booker_radio_field<?php echo $opn; ?>"><span><?php echo $radio; ?></span></label>
                                    </div>
                                    <?php  $opn++; } ?> 
                                    
                            <?php }else if($fieldType->field_type=='file') { ?> 
                                <div class="custom-upload">
                                    <span class="label_text">Select or Drop File</span>
                                    <input type="file" class="customIinputFileLogo" id="bookerimage" accept="image/*" name="<?php echo $attendee->id; ?>"  draggable="true">
                                    <div class="display_file">
                                        <span class="file_value"></span>
                                        <a href="javascript:void(0)" class="clear_value"></a>
                                    </div>
                                </div>
                            <?php } ?>           
                            <div class="clearFix"></div>        
                             <span id="error-<?php //echo $attendee->id; ?>" class="error"></span> 
                        </div>
                    </div>
                   <?php } ?>
                   <?php $counter++; } ?>
                   <?php if($status==1) { ?>
                   <div class="row">
                      <div class="col-3">
                        <div class="labelDiv">
                          <label class="form-label text-right">Dietary Requirements</label>
                        </div>
                      </div>
                      <div class="col-5">     
                        <?php $dietary = $bookers[$loop]->dietary; ?>
                        <select class="custom-select medium_select" id="field_dietary1" name="field_dietary[]">
                            <option value="" selected="selected"></option>
                            <?php foreach($dietaryReq as $dietaryVal) { 
                                  if($dietaryVal!='None' && $dietaryVal!='Other') {  
                                ?>
                                <option value="<?php echo $dietaryVal; ?>" <?php if($dietary==$dietaryVal) { echo 'selected'; } ?>><?php echo $dietaryVal; ?></option>
                            <?php } } ?>
                                <option value="Other">Other</option>
                                <option value="None">None</option>    
                            </select>
                            <div class="clearFix"></div>
                         <span id="error-<?php //echo $fieldType->id; ?>" class="error"></span> 
                    </div>
                    </div>
                   <?php } ?>
                   </div>
                   </div>
                   <hr/>
                   <?php $status++; ?>
                  <?php $loop++; } }  ?>   
          </div> 
              
              
          <?php } ?>
          
               
           <div class="panel_content responsive_content rowB mtzero">
            <div class="row set_invitations setRow">
                           
            </div>
          </div>
          
          
          <div class="panel_footer">
            <div class="pull-right">
                <input type="hidden" name="quantity" value="<?php echo $quantity;  ?>" id="quantity">
                <input type="hidden" name="bookerId" value="<?php echo $bookerId;  ?>" id="bookerId">
                <input type="hidden" name="eventId" value="<?php echo $eventId;  ?>" id="eventsid">
                <input type="hidden" name="registration_type" value="<?php echo $registration_type;  ?>" id="registration_type">
                <input type="hidden" name="field_order" value="<?php echo $field_order;  ?>" id="field_order">
                
                <input type="submit" value="Update" class="btn-normal btn submitAction">
                
            </div>
          </div>
        <?php echo form_close();?>
      </div>
    </div>
  </div>
  <!--
  <div class="col-6 venuedetail_rightPart">
    <p><?php echo lang('who_else_txt'); ?></p>
    <div class="col-fluid-15 mT10">
      <div class="col-fluid-7 responsive-sec">
        <div class="center_content"> <span><img src="<?php echo IMAGE; ?>user.jpg" ></span> <span class="lrg-font">Joshua Mcfadden<span class="sml-font">Google Inc.</span></span> </div>
      </div>
      <div class="col-fluid-7 responsive-sec">
        <div class="center_content"> <span><img src="<?php echo IMAGE; ?>user.jpg" ></span> <span class="lrg-font">Joshua Mcfadden<span class="sml-font">Google Inc.</span></span> </div>
      </div>
    </div>
    <div class="col-fluid-15 mT10">
      <div class="col-fluid-7 responsive-sec">
        <div class="center_content"> <span><img src="<?php echo IMAGE; ?>user.jpg" ></span> <span class="lrg-font">Joshua Mcfadden<span class="sml-font">Google Inc.</span></span> </div>
      </div>
      <div class="col-fluid-7 responsive-sec">
        <div class="center_content"> <span><img src="<?php echo IMAGE; ?>user.jpg" ></span> <span class="lrg-font">Joshua Mcfadden<span class="sml-font">Google Inc.</span></span> </div>
      </div>
    </div>
    <div class="col-fluid-15 mT10">
      <div class="col-fluid-7 responsive-sec">
        <div class="center_content"> <span><img src="<?php echo IMAGE; ?>user.jpg" ></span> <span class="lrg-font">Joshua Mcfadden<span class="sml-font">Google Inc.</span></span> </div>
      </div>
      <div class="col-fluid-7 responsive-sec">
        <div class="center_content"> <span><img src="<?php echo IMAGE; ?>user.jpg" ></span> <span class="lrg-font">Joshua Mcfadden<span class="sml-font">Google Inc.</span></span> </div>
      </div>
    </div>
    <div class="col-fluid-15 mT10">
      <div class="col-fluid-7 responsive-sec">
        <div class="center_content"> <span><img src="<?php echo IMAGE; ?>user.jpg" ></span> <span class="lrg-font">Joshua Mcfadden<span class="sml-font">Google Inc.</span></span> </div>
      </div>
      <div class="col-fluid-7 responsive-sec">
        <div class="center_content"> <span><img src="<?php echo IMAGE; ?>user.jpg" ></span> <span class="lrg-font">Joshua Mcfadden<span class="sml-font">Google Inc.</span></span> </div>
      </div>
    </div>
    <div class="col-fluid-15 mT10">
      <div class="col-fluid-7 responsive-sec">
        <div class="center_content"> <span><img src="<?php echo IMAGE; ?>user.jpg" ></span> <span class="lrg-font">Joshua Mcfadden<span class="sml-font">Google Inc.</span></span> </div>
      </div>
      <div class="col-fluid-7 responsive-sec">
        <div class="center_content"> <span><img src="<?php echo IMAGE; ?>user.jpg" ></span> <span class="lrg-font">Joshua Mcfadden<span class="sml-font">Google Inc.</span></span> </div>
      </div>
    </div>
    <div class="col-fluid-15 mT10">
      <div class="col-fluid-7 responsive-sec">
        <div class="center_content"> <span><img src="<?php echo IMAGE; ?>user.jpg" ></span> <span class="lrg-font">Joshua Mcfadden<span class="sml-font">Google Inc.</span></span> </div>
      </div>
      <div class="col-fluid-7 mT25 responsive-sec">
        <div class="center_content"> <span><a href="#">More..</a></span> </div>
      </div>
    </div>
  </div>
  -->
  <!-- Register Attendee -->
  <form action="<?php echo base_url(); ?>events/attendeeConfirmation" method="post" id="formAttendeeRegistration">
  <div class="col-15 footer_bg">
    <div class="container">
      <div class="invitation_content_chkbox">
<!--
        <div class="chkbox-inner-sec mT20">
          <input id="terms_checkbox_invitation" name="terms_checkbox_invitation" class="checkBox" type="checkbox">
          <label class="form-label" for="terms_checkbox_invitation"></label>
          <label class="form-label terms" for="">
         <p class="para mL30">I have read and agree with the<br>
         <span class="link_terms" data-toggle="modal" data-target=".terms_popup">Terms &amp; Conditions &nbsp; </span> for my <br>
            Newman account.
         <span id="error-email" class="w_100 set_error dn" style="display: none;"><label id="email-error" class="error" for="email">Please select terms &amp; conditions.</label></span>   
         </p>
        
         </label> 
        </div>
-->
      </div>
    </div>
  </div>
  <div class="col-15">
    <div class="container">
      <div class="invitation_content_rgeisterbox common_prop">
<!--
        <button class="btn-normal btn pull-none rigisterbtnAction">
          <div class="lok">Register</div>
        </button>
-->
        <div id="postAttendeeData1">
            
        </div>
      </div>
    </div>
  </div>
  </form>
 <!-- End Registartion -->
</div>

<?php $this->load->view('front_footer'); ?>   
<script>
function getTitleId(data,dataid){ $('.setTitle_'+dataid).html(data); }

$('.submitAction').click(function(){
    var dataCheck = 0;
    var status;
    $("input").each(function(){
          
      var inputAttrVal  = $(this).attr('mandatory');
      var inputLang     = $(this).attr('lang');
       // This is the jquery object of the input, do what you will
      if(inputAttrVal=='true') {
        //dataCheck = 1;  
        var inputAVal = $(this).val();
        if($.trim(inputAVal)=='') {
            dataCheck = 1;
            $(this).parent().find('span.error').html('<label id="fieldName-error" class="error" for="fieldName">This field is required.</label>');
            //$(this).css('border','red 1px solid'); 
        }else {
            if(inputLang=='email') {
                status = validateEmail(inputAVal);
                if(status==1) {
                    dataCheck = 1;
                    $(this).parent().find('span.error').html('<label id="fieldName-error" class="error" for="fieldName">Please enter a valid email address.</label>');
                }
            }  
        }
      }   
    });
    
    if(dataCheck==1) {
        return false;
    }else {
        loaderopen();
    }
    //$('#bookerQuantity').val($('#invitation_quantity').val());
});
</script>
