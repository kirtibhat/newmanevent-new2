<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
/* Define form fields */
$formUpdateAttendeeSetup = array(
'name'          => 'formUpdateAttendeeSetup',
'id'            => 'formUpdateAttendeeSetup',
'method'        => 'post',
'class'         => '',
'enctype'       => 'multipart/form-data'
);
?>
<div class="page_content">
    <div class="container">
        <div class="row">
            <div class="col-9">
            <!-- Start panel -->
            <div class="panel open" id="event_contact_person_panel">
              <div class="panel_header">Edit Responce</div>
              <div class="panel_contentWrapper" id="showhideformdivpersonalContact" style="display:block">
                  <div class="infobar">
                    <p class="dn" id="event_contact_person_info">The Access ID# is a unique identifier assigned to every registrant using this site. Use it to access, edit, cancel, pay and rebook items in your registration.</p>
                    <span class="info_btn" onclick="showHideInfoBar('event_contact_person_info');"></span>
                  </div>
                  <?php echo form_open(base_url().'events/updateAttendee',$formUpdateAttendeeSetup); ?>
                  <div class="panel_content">
                   <div class="row" style="background-color: currentColor;width: 100%;">
                      <div class="col-3">
                        <div class="labelDiv">
                          <label class="form-label text-right">Booker Details</label>
                        </div>
                      </div>
                      <div class="col-5">
                        <hr/>
                        <span id="error-dinner_dance6"></span> </div>
                    </div>     
                   <?php foreach($fieldIdsbooker as $bookers) { ?>
                   <?php $fieldType     = getDataByParam('field_type',array('id'=> $bookers->field_id)); ?>        
                   <?php $defaultValue  = getDataByParam('default_value',array('id'=> $bookers->field_id)); ?>
                   <?php $field_name    = getDataByParam('field_name',array('id'=> $bookers->field_id)); ?>
                   <?php $field_value   = (!empty($bookers->field_value)) ? $bookers->field_value : ''; ?>
                         
                    <div class="row">
                        <div class="col-3">
                            <div class="labelDiv">
                              <label class="form-label text-right"><?php echo ucwords($field_name->field_name); ?></label>
                            </div>
                        </div>
                        <div class="col-5">
                            <?php echo form_hidden('fieldidbooker[]', $bookers->id);  ?>
                            <?php if($fieldType->field_type=='selectbox') { ?>
                                <select class="custom-select medium_select" id="booker_title" name="bookeridvalue[<?php echo $bookers->id; ?>]">
                                <?php if( empty($defaultValue->default_value) || ($defaultValue->default_value == 'NULL') )  { ?>
                                    <option value="Miss" <?php if($field_value=='Miss') { echo 'selected'; } ?>>Miss</option>
                                    <option value="Ms" <?php if($field_value=='Ms') { echo 'selected'; } ?>>Ms</option>
                                    <option value="Mrs" <?php if($field_value=='Mrs') { echo 'selected'; } ?>>Mrs</option>
                                    <option value="Mr" <?php if($field_value=='Mr') { echo 'selected'; } ?>>Mr</option>
                                <?php }else {  
                                    $optionsArr    = json_decode($defaultValue->default_value);
                                    if($optionsArr) {
                                        foreach($optionsArr as $option) { ?>
                                            <option value="<?php echo $option; ?>" <?php if($field_value==$option) { echo 'selected'; } ?>><?php echo $option; ?></option>
                                <?php   }
                                    } 
                                } ?>
                                </select>
                            <?php }else if( $fieldType->field_type=='text' ) { ?>
                                <input type="text" name="bookeridvalue[<?php echo $bookers->id; ?>]" class="xlarge_input" value="<?php echo $field_value; ?>">
                            
                            <?php }else if( $fieldType->field_type=='radio' ) { ?>
                                    <?php 
                                    $radioOptionsArr = json_decode($defaultValue->default_value);
                                    $opn = 1; 
                                    foreach($radioOptionsArr as $radio) {
                                    ?>
                                    <div class="radioDiv">
                                    <input type="radio" name="bookeridvalue_<?php echo $opn; ?>[<?php echo $bookers->id; ?>]" value="<?php echo $radio; ?>" id="booker_radio_field" class="radioButton" 
                                    <?php if($field_value==$radio) { echo 'checked'; } ?>>        
                                    <label class="form-label pull-left" for="booker_radio_field<?php echo $opn; ?>"><span><?php echo $radio; ?></span></label>
                                    </div>
                                    <?php  $opn++; } ?> 
                                    
                            <?php }else if($fieldType->field_type=='file') { ?> 
                                <div class="custom-upload">
                                    <span class="label_text">Select or Drop File</span>
                                    <input type="file" class="customIinputFileLogo" id="bookerimage" accept="image/*" name="booker_image"  draggable="true">
                                    <div class="display_file">
                                        <span class="file_value"></span>
                                        <a href="javascript:void(0)" class="clear_value"></a>
                                    </div>
                                </div>
                            <?php } ?>                   
                            <span id="error-dinner_dance6"></span> 
                        </div>
                    </div>
                   <?php } ?>    
                   
                   <div class="row" style="background-color: currentColor;width: 100%;">
                          <div class="col-3">
                            <div class="labelDiv">
                              <label class="form-label text-right">Booker Details</label>
                            </div>
                          </div>
                          <div class="col-5">
                            <hr/>
                            <span id="error-dinner_dance6"></span> 
                        </div>
                    </div>  
                  
                  
                    <?php $counter = 0; ?>
                    <?php foreach($fieldIdsattendee as $bookers) { ?>
                    <?php foreach($bookers as $attendee) { ?>
                    <?php $fieldType     = getDataByParam('field_type',array('id'=> $attendee->field_id)); ?>        
                    <?php $defaultValue  = getDataByParam('default_value',array('id'=> $attendee->field_id)); ?>
                    <?php $field_name    = getDataByParam('field_name',array('id'=> $attendee->field_id)); ?>
                    <?php $field_value   = (!empty($attendee->field_value)) ? $attendee->field_value : ''; ?>
                         
                    <div class="row">
                        <div class="col-3">
                            <div class="labelDiv">
                              <label class="form-label text-right"><?php echo ucwords($field_name->field_name); ?></label>
                            </div>
                        </div>
                        <div class="col-5">
                            <?php echo form_hidden('fieldidattendee[]', $attendee->id);  ?> 
                            
                            <?php if($fieldType->field_type=='selectbox') { ?>
                                <select class="custom-select medium_select" id="booker_title" name="attendee_value[<?php echo $attendee->id; ?>]">
                                <option value="" selected="selected"></option>
                                <?php if( empty($defaultValue->default_value) || ($defaultValue->default_value == 'NULL') )  { ?>
                                    <option value="Miss" <?php if($field_value=='Miss') { echo 'selected'; } ?>>Miss</option>
                                    <option value="Ms" <?php if($field_value=='Ms') { echo 'selected'; } ?>>Ms</option>
                                    <option value="Mrs" <?php if($field_value=='Mrs') { echo 'selected'; } ?>>Mrs</option>
                                    <option value="Mr" <?php if($field_value=='Mr') { echo 'selected'; } ?>>Mr</option>
                                <?php }else {  
                                    $optionsArr    = json_decode($defaultValue->default_value);
                                    if($optionsArr) {
                                        foreach($optionsArr as $option) { ?>
                                            <option value="<?php echo $option; ?>" <?php if($field_value==$option) { echo 'selected'; } ?>><?php echo $option; ?></option>
                                <?php   }
                                    } 
                                } ?>
                                </select>
                            <?php }else if( $fieldType->field_type=='text' ) { ?>
                                <input type="text" name="attendee_value[<?php echo $attendee->id; ?>]" class="xlarge_input" value="<?php echo $field_value; ?>">
                            
                            <?php }else if( $fieldType->field_type=='radio' ) { ?>
                                    <?php 
                                    $radioOptionsArr = json_decode($defaultValue->default_value);
                                    $opn = 1; 
                                    foreach($radioOptionsArr as $radio) {
                                    ?>
                                    <div class="radioDiv">
                                    <input type="radio" name="attendee_value_<?php echo $opn; ?>[<?php echo $attendee->id; ?>]" value="<?php echo $radio; ?>" id="booker_radio_field" class="radioButton" 
                                    <?php if($field_value==$radio) { echo 'checked'; } ?>>        
                                    <label class="form-label pull-left" for="booker_radio_field<?php echo $opn; ?>"><span><?php echo $radio; ?></span></label>
                                    </div>
                                    <?php  $opn++; } ?> 
                                    
                            <?php }else if($fieldType->field_type=='file') { ?> 
                                <div class="custom-upload">
                                    <span class="label_text">Select or Drop File</span>
                                    <input type="file" class="customIinputFileLogo" id="bookerimage" accept="image/*" name="<?php echo $attendee->id; ?>"  draggable="true">
                                    <div class="display_file">
                                        <span class="file_value"></span>
                                        <a href="javascript:void(0)" class="clear_value"></a>
                                    </div>
                                </div>
                            <?php } ?>                   
                            <span id="error-dinner_dance6"></span> 
                        </div>
                    </div>
                   <?php $counter++; } } ?>    
                  
                  </div>
                  <div class="panel_footer">
                    <div class="pull-right">
                        <input type="hidden" name="quantity" value="<?php echo $quantity;  ?>" id="quantity">
                        <input type="hidden" name="bookerId" value="<?php echo $bookerId;  ?>" id="bookerId">
                        <input type="hidden" name="eventId" value="<?php echo $eventId;  ?>" id="eventId">
                        
                        <input type="submit" value="Modify Responce" class="btn-normal btn">
                        <input type="submit" value="Done" class="btn-normal btn">
                    </div>
                  </div> 
                  <?php echo form_close();?>  
                   
              </div>
            </div>
             <!-- End panel -->
            </div>
        </div>
    </div>
</div>
