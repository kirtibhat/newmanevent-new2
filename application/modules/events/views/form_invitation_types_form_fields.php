<!-- Section - 1 -->
<?php $counter = 1; $lang=''; ?>
<?php for( $quantityCounter = 1 ; $quantityCounter <= $quantity ; $quantityCounter++ ) { ?>
 <div class="mB10 display_inline gap25">
<?php foreach($invitationTypesFormFieldsData as $listField) { if($listField->field_status!=3) { ?>        
<div class="row">
  <div class="col-3">
    <div class="labelDiv">
      <label class="form-label text-right"><?php echo ucwords($listField->field_name); ?>
      <?php $isMandatory='false'; 
      if($listField->field_status=='2') { $isMandatory='true'; ?>
      <span class="required" aria-required="true">*</span>
      <?php } ?>
      </label>
    </div>
  </div>
  
  <div class="col-5">
      <?php if($quantityCounter==1) { echo form_hidden('fieldid[]', $listField->id); } ?>
      <?php if($listField->field_type=='selectbox') { ?>
        <span class="select-wrapper">
            <div class="customSelectWrapper" style="width: auto;">
                <select name="field_<?php echo $listField->id; ?>[]" id="field_<?php echo $listField->id; ?>" class="custom-select medium_select hasCustomSelect rmv_error <?php echo 'title'.$quantityCounter; ?>" style="-webkit-appearance: menulist-button; width: 175px; position: absolute; opacity: 0; height: 28px; font-size: 15px;" onchange="getTitleId(this.value,'<?php echo $listField->id.$quantityCounter; ?>');" mandatory="<?php echo $isMandatory; ?>">
                <option value="" selected="selected"></option>
            <?php if( ( empty($listField->default_value) || ($listField->default_value == 'NULL') ) &&  strtolower($listField->field_name) == 'title' ) { ?>
                    <option value="Miss">Miss</option>
                    <option value="Ms">Ms</option>
                    <option value="Mrs">Mrs</option>
                    <option value="Mr">Mr</option>
            <?php }else {  
                    $optionsArr = json_decode($listField->default_value); 
                    if($optionsArr) {
                        foreach($optionsArr as $option) {
                ?>
                        <option value="<?php echo $option; ?>"><?php echo $option; ?></option>
                <?php   }
                    } 
                } ?>
                        
        </select>
            </div>
            <div class="customSelectWrapper" style="width: auto;">
                <span class="customSelect custom-select medium_select" style="display: inline-block;">
                    <span class="customSelectInner setTitle_<?php echo $listField->id.$quantityCounter; ?> title_wrapper_<?php echo $quantityCounter; ?>" style="width: 125px; display: inline-block;">Select</span>
                </span>
            </div>
        </span>
        
        
        
      <?php }else if( $listField->field_type=='text' ) { 
                        $fieldtext='';
                        $space ='';
                        $cls='';
                        if($listField->field_name=='first name') { 
                            if($isMandatory!='true'){
                                $space = ' ';
                            } 
                            $cls='fname'.$quantityCounter; echo '<input type="hidden" name="attendee_fname" value="'.$listField->id.'">'; }
                        if($listField->field_name=='last name') { 
                            if($isMandatory!='true'){
                                $space = ' ';
                            } 
                            $cls='lname'.$quantityCounter; echo '<input type="hidden" name="attendee_lname" value="'.$listField->id.'">'; }
                        if($listField->field_name=='email') { $cls='email'.$quantityCounter; echo '<input type="hidden" name="attendee_email" value="'.$listField->id.'">'; }
                            
                    ?>
                        <input <?php if($listField->field_name=='email') {?> type="email" <?php }else{ ?> type="text" <?php } ?> name="field_<?php echo $listField->id; ?>[]" class="medium_input rmv_error <?php echo $cls; ?>" mandatory="<?php echo $isMandatory; ?>" value="<?php echo $space; ?>">
                        
                        <?php if($listField->field_name=='email') { ?>
                        <div class="mt" style="margin-top:7px;">	
                            <input type="checkbox" name="is_email_send_<?php echo $listField->id; ?>_<?php echo $quantityCounter; ?>[]" value="<?php echo $quantityCounter; ?>" id="is_email_send_<?php echo $quantityCounter; ?>" class="checkBox isCheckedForEmail" > 		 
                            <label class="form-label" for="is_email_send_<?php echo $quantityCounter; ?>"><span>Send Ticket to Attendee</span></label>
                        </div>
                        <?php } ?>
                        
                        <?php //if($listField->field_name=='email') { echo '<input type="checkbox" class="isCheckedForEmail" name="is_email_send" value="'.$listField->id.'">'; } ?>
                  <?php  
            }else if( $listField->field_type=='radio' ) { 
                        $radioOptionsArr = json_decode($listField->default_value);
                         
                        foreach($radioOptionsArr as $radio) {
                        ?>
                    <div class="radioDiv">
                            <input type="radio" name="field_<?php echo $listField->id; ?>_<?php echo $quantityCounter; ?>[]" value="<?php echo $radio; ?>" id="invitation_types_radio_field_<?php echo $counter; ?>" class="radioButton">               
                            <label class="form-label pull-left" for="invitation_types_radio_field_<?php echo $counter; ?>"><span><?php echo $radio; ?></span></label>
                    </div>
                    
                <?php $counter++; } 
                
            }else if($listField->field_type=='file') { ?>
              <div class="custom-upload" style="">
                <span class="label_text">Select or Drop File</span>
                <input type="file" class="customIinputFileLogo" id="bookerimage" accept="image/*" name="attendee_image_<?php echo $listField->id; ?>_<?php echo $quantityCounter; ?>"  draggable="true">
                <div class="display_file">
                    <span class="file_value"></span>
                    <a href="javascript:void(0)" class="clear_value"></a>
                    <input type="hidden" name="attendee_image_<?php echo $listField->id; ?>_<?php echo $quantityCounter; ?>" id="bookerFileValue" value="">
                </div>
            </div>
              
           <?php } ?>                  
    <div class="clearFix"></div>               
    <span id="error-dinner_dance6" class="error"></span> </div>
</div> 
<?php } ?>
<?php } ?>
<div class="row">
  <div class="col-3">
    <div class="labelDiv">
      <label class="form-label text-right">Dietary Requirements</label>
    </div>
  </div>
  <div class="col-5">     
    <span class="select-wrapper">
        <div class="customSelectWrapper" style="width: auto;">
        <select name="field_dietary[]" id="field_dietary" class="custom-select medium_select hasCustomSelect rmv_error" style="-webkit-appearance: menulist-button; width: 175px; position: absolute; opacity: 0; height: 28px; font-size: 15px;" onchange="getTitleId(this.value,'<?php echo $quantityCounter; ?>');" mandatory="false">
        <option value="" selected="selected"></option>
        <?php foreach($dietaryReq as $dietaryVal) { 
              if($dietaryVal!='None' && $dietaryVal!='Other') {  
            ?>
            <option value="<?php echo $dietaryVal; ?>"><?php echo $dietaryVal; ?></option>
        <?php } } ?>
            <option value="Other">Other</option>
            <option value="None">None</option>    
        </select>
        </div>
        <div class="customSelectWrapper" style="width: auto;">
            <span class="customSelect custom-select medium_select" style="display: inline-block;">
                <span class="customSelectInner setTitle_<?php echo $quantityCounter; ?>" style="width: 125px; display: inline-block;">Select</span>
            </span>
        </div>
    </span>  
    <span id="error-dinner_dance6"></span> 
</div>
</div>
</div>
<hr/>
<?php } ?>  


                        

