<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
//echo '<pre>';
//print_r($eventdetails);
/* cp = contact person */
$first_name_cp             = (!empty($eventdetails->first_name)) ? $eventdetails->first_name : '';
$last_name_cp              = (!empty($eventdetails->last_name)) ? $eventdetails->last_name : '';
$email_cp                  = (!empty($eventdetails->email)) ? $eventdetails->email : '';
$phone1_mobile_cp          = (!empty($eventdetails->phone1_mobile)) ? $eventdetails->phone1_mobile : '';
$title_cp                  = (!empty($eventdetails->title)) ? $eventdetails->title : '';
$organisation_cp                  = (!empty($eventdetails->organisation)) ? $eventdetails->organisation : '';

$secondary_color            = (!empty($eventdetails->secondary_color)) ? $eventdetails->secondary_color : '';
$primary_color              = (!empty($eventdetails->primary_color)) ? $eventdetails->primary_color : '';
$event_theme_screen_bg      = (!empty($eventdetails->event_theme_screen_bg)) ? $eventdetails->event_theme_screen_bg : '';
//.mr0 { margin: 0px; }
$userid                     = (!empty($eventdetails->user_id)) ? $eventdetails->user_id : '';
$themeid                    = (!empty($eventdetails->id)) ? $eventdetails->id : '';
$eventid                    = (!empty($eventdetails->event_id)) ? $eventdetails->event_id : '';

$sponsorimage = base_url().'media/events_sponsors_logo/user_'.$userid.'/'.'event_'.$eventid.'/sponsors_'.$themeid.'.png';


/* Set theme colors */
$primarycolor = 'color:#'.$primary_color;
$primarycolorbg = 'background-color:#'.$primary_color;

$secondarycolor = 'color:#'.$secondary_color;
$secondarycolorbg = 'background-color:#'.$secondary_color;

$eventthemescreencolor = 'color:#'.$event_theme_screen_bg;
$eventthemescreenbg = 'background-color:#'.$event_theme_screen_bg;

/* Define form fields */
$formContactSetup = array(
'name'          => 'formContactSetup',
'id'            => 'formContactSetup',
'method'        => 'post',
'class'         => '',
);
$firstName = array(
'name'          => 'firstName',
'value'         => '',
'id'            => 'firstname',
'type'          => 'text',
'autocomplete'  => 'off',
'class'         => 'xlarge_input',
);  
$lastName = array(
'name'          => 'lastName',
'value'         => '',
'id'            => 'lastname',
'type'          => 'text',
'class'         => 'xlarge_input',
'autocomplete'  => 'off',
);    
$contactOrganisation = array(
'name'          => 'contactOrganisation',
'value'         => '',
'id'            => 'company_name',
'type'          => 'text',
'class'         => 'xlarge_input',
'autocomplete'  => 'off',
);
$contactPhone = array(
'name'          => 'contactPhone',
'value'         => '',
'id'            => 'mobile',
'type'          => 'text',
'class'         => 'medium_input pull-left contact_input phoneValue contacteventdata',
'placeholder'   => '123456789',
'size'          => '40',
); 
$contactEmail = array(
'name'          => 'contactEmail',
'value'         => '',
'id'            => 'email',
'type'          => 'email',
'class'         =>'xlarge_input',
'autocomplete'  => 'off',
);

$contactMessage = array(
    'name'          => 'contactMessage',
    'value'         => '',
    'id'            => 'contactMessage',
    'rows'          => '3',
    'class'         => 'form-textarea'
);
?>
<?php $this->load->view('front_header'); ?>
<div class="invitation_content_inner_sec common_prop">
  <div class="col-9 responsive-pedng">
    <div class="panel open" id="panel0">
      <div class="panel_header" style="<?php echo $primarycolorbg; ?>">Contact Form</div>
      <div class="panel_contentWrapper" style="display:block;">
        <!--sub menu one-->
        <?php echo form_open($this->uri->uri_string(),$formContactSetup); ?>
          <div class="panel_content responsive_content">
            
            <div class="row">
              <div class="col-3">
                <div class="labelDiv">
                  <label class="form-label text-right">Title</label>
                </div>
              </div>
              <div class="col-5">
                <?php 
                  $other                = 'id="title" class="custom-select small_select"';
                  $titleArray['']       = 'Select Title';
                  $titleArr             = $this->config->item('title_array');
                  foreach($titleArr as $key=>$value){
                    $titleArray[$key]   = $value;
                  } 
                  echo form_dropdown('title',$titleArray,$cptitle='',$other);
                  echo form_error('title');
                ?>    
                <span id="error-dinner_dance6"></span> </div>
            </div>
            
            <div class="row">
                          <div class="col-3">
                            <div class="labelDiv">
                                <label class="form-label text-right">First Name<span class="required" aria-required="true">*</span></label>
                            </div>
                          </div>
                          <div class="col-5">
                            <?php echo form_input($firstName); ?>
                            <?php echo form_error('firstName'); ?>
                            <span id="error-firstName"></span>
                          </div>
                        </div>
                       
                        <div class="row">
                          <div class="col-3">
                            <div class="labelDiv">
                            <label class="form-label text-right">Last Name<span class="required" aria-required="true">*</span></label>
                              
                            </div>
                          </div>
                          <div class="col-5">
                            <?php echo form_input($lastName); ?>
                            <?php echo form_error('lastName'); ?>
                            <span id="error-lastName"></span> </div>
                        </div>
                        
                        <div class="row">
                          <div class="col-3">
                            <div class="labelDiv">
                            <label class="form-label text-right">Email<span class="required" aria-required="true">*</span></label>
                            </div>
                          </div>
                          <div class="col-5">
                              <?php echo form_input($contactEmail); ?>
                              <?php echo form_error('contactEmail'); ?>
                            <span id="error-contactEmail"></span> </div>
                        </div>
                        
                        <div class="row">
                          <div class="col-3">
                            <div class="labelDiv">
                                <label class="form-label text-right">Phone</label>
                            </div>
                          </div>
                          <div class="col-5">
                            <?php echo form_input($contactPhone); ?>
                            <?php echo form_error('contactPhone'); ?>
                            <span id="error-contactPhone"></span>
                          </div>
                        </div>
                        
                        <div class="row">
                          <div class="col-3">
                            <div class="labelDiv">
                              <label class="form-label text-right">Organisation</label>
                            </div>
                          </div>
                          <div class="col-5">
                            <?php echo form_input($contactOrganisation); ?>
                            <?php echo form_error('contactOrganisation'); ?>
                            <span id="error-dinner_dance6"></span> </div>
                        </div>
                        
                        <div class="row">
                          <div class="col-3">
                            <div class="labelDiv">
                              <label class="form-label text-right">Topic</label>
                            </div>
                          </div>
                          <div class="col-5">
                            <?php
                                $contact_topic = $this->config->item('contact_topics');
                                $attr_contact_topic = ' id="contact_topic" class="custom-select medium_select"  ';
                                echo form_dropdown('contact_topic', $contact_topic, '', $attr_contact_topic);
                                echo form_error('event_category');
                            ?>  
                            <span id="error-contact_topic"></span> </div>
                        </div>
                        
                        <div class="row">
                          <div class="col-3">
                            <div class="labelDiv">
                              <label class="form-label text-right">Message<span class="required" aria-required="true">*</span></label>
                            </div>
                          </div>
                          <div class="col-5">
                            <?php echo form_textarea($contactMessage); ?>
                            <?php echo form_error('contactMessage'); ?>
                            <span id="error-contactMessage"></span> </div>
                        </div>
            
          </div>

          <div class="panel_footer">
<!--
            <a href="javascript:void();" class="scrollTopTrg" style=""><img alt="Logo image" src="<?php echo IMAGE; ?>scroll_top.png">Top</a>
-->
            <div class="pull-right">
                <input type="reset" value="Clear" class="btn-normal btn">
                <input type="submit" value="Send" class="btn-normal btn">
              
            </div>
          </div>
        <?php echo form_close();?>
      </div>
    </div>
  </div>
  <!-- Right Panel -->
  <div class="col-6 venuedetail_rightPart txtright">
    <p class="font-small">Have a comment, question or<br/>
    compliment? Please fill out the contact<br/>
    form or feel free to call <?php echo $first_name_cp.' '.$last_name_cp; ?> ,<br/>
    details below.
    <br/><br/>
    <?php echo $title_cp.' '.$first_name_cp.' '.$last_name_cp; ?><br/>
    <?php echo $organisation_cp; ?><br/>
    <?php echo $email_cp; ?><br/>
    <?php echo $phone1_mobile_cp; ?></p>
  </div>
  <!-- Register Attendee -->
  <form>
  <div class="col-15 footer_bg">
    <div class="container">
      <div class="invitation_content_chkbox">
        
      </div>
    </div>
  </div>
  
  <div class="col-15">
    <div class="container">
      <div class="invitation_content_rgeisterbox common_prop">
        
        
      </div>
    </div>
  </div>
  </form>
 <!-- End Registartion -->
</div>
<div class="other-logo-sec common_prop">
    	<div class="container">
        	<div class="themeSponsorsLogos">
            	<img src="<?php echo $sponsorimage; ?>" style="width:100%;max-height:220px;">
            </div>
        </div>
    </div>
<?php $this->load->view('front_footer'); ?> 
<script>
$('.circleMenueFront').css('display','none');
</script>
