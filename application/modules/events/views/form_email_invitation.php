<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

/*set user details data*/
$eventId            = (!empty($eventId))? $eventId :'';
$code = $this->input->get('code');
$checkedStatus = (!empty($code)) ? 'true' : 'false';
/* Define form fields */
$formEmailInvitationSetup = array(
'name'          => 'formEmailInvitationSetup',
'id'            => 'formEmailInvitationSetup',
'method'        => 'post',
'class'         => '',
);
$nameOfSender = array(
'name'          => 'nameOfSender',
'value'         => '',
'id'            => 'nameOfSender',
'type'          => 'text',
'autocomplete'  => 'off',
'class'         => 'xlarge_input',
);  
$subjectLine = array(
'name'          => 'subjectLine',
'value'         => '',
'id'            => 'subjectLine',
'type'          => 'text',
'class'         => 'xlarge_input',
'autocomplete'  => 'off',
);    
$emails = array(
    'name'          => 'emails',
    'value'         => '',
    'id'            => 'emails',
    'rows'          => '3',
    'placeholder'   => 'Enter separated by semicolon or comma..',
    'class'         => 'form-textarea'
);

$secondary_color            = (!empty($eventdetails->secondary_color)) ? $eventdetails->secondary_color : '';
$primary_color              = (!empty($eventdetails->primary_color)) ? $eventdetails->primary_color : '';
$event_theme_screen_bg      = (!empty($eventdetails->event_theme_screen_bg)) ? $eventdetails->event_theme_screen_bg : '';

/* Set theme colors */
$primarycolor = 'color:#'.$primary_color;
$primarycolorbg = 'background:#'.$primary_color;

$secondarycolor = 'color:#'.$secondary_color;
$secondarycolorbg = 'background:#'.$secondary_color;

$eventthemescreencolor = 'color:#'.$event_theme_screen_bg;
$eventthemescreenbg = 'background:#'.$event_theme_screen_bg;

?>
<?php $this->load->view('front_header'); ?>
<div class="invitation_content_inner_sec common_prop">
  <div class="col-9 responsive-pedng">
    <?php echo form_open(base_url().'events/addEmailInvitation/'.$eventId,$formEmailInvitationSetup); ?>
    <div class="panel mB30" id="panel0">
      <div class="panel_header" style="<?php echo $primarycolorbg; ?>">Create Email Invitation</div>
      <div class="panel_contentWrapper" style="display:block;">
        <!--sub menu one-->
        
        <div class="panel_content responsive_content">
            
            <div class="row">
              <div class="col-3">
                <div class="labelDiv">
                <label class="form-label text-right">Invitation Type</label>
                  
                </div>
              </div>
              <div class="col-5">
                <select class="custom-select medium_select" id="invitation_types_email" name="invitation_types_email">
                    <option value="">Select</option>
                    <?php foreach($eventInvitations as $inv_value) { ?>
                            <option value="<?php echo $inv_value->id; ?>" pass="<?php echo $inv_value->password; ?>" quantity="<?php echo $inv_value->limit_per_invitation; ?>"><?php echo $inv_value->invitation_name; ?>
                            </option>
                    <?php } ?>    
                </select>
                <span id="error-invitation_types_email"></span> 
              </div>
            </div>
            
            <!-- Add Guests -->
            <div class="row">
              <div class="col-3">
                <div class="labelDiv">
                  <label class="form-label text-right">Add Your Guests<span class="required" aria-required="true"></span></label> 
                </div>
              </div>
              <div class="col-5">
                <div class="radioDiv">
                <input type="radio" name="isImport" value="1"  id="importByMedia" class="radioButton isCheckedEvent" lang="media" 
                checked="checked">                    
                <label class="form-label pull-left" for="importByMedia"><span>Import emails from your Gmail address book</span></label>
                </div>
                <div class="radioDiv">
                    <input type="radio"  name="isImport" value="0" id="manullayEmails" class="radioButton isCheckedEvent" lang="manullay">                    
                    <label class="form-label pull-left" for="manullayEmails"><span>Manually enter email addresses</span></label>
                </div>
                <span id="error-dinner_dance6"></span> </div>
            </div>
            
            <div class="row mediaEmail" >
              <div class="col-3">
                <div class="labelDiv">
                  <label class="form-label text-right"></label>
                </div>
              </div>
              <div class="col-5">
                    <input type="button" value="GMAIL" class="btn-normal btn" onclick="contactget(2);"><br/><br/>
                    <div class="" id="" style="width: 350px;height: 150px;background: whitesmoke;overflow: auto;">
                        <?php //print_r($alldata); ?>
                        <?php if(!empty($alldata)) { foreach($alldata as $email) { ?>
                        <p><input type="checkbox" name="inviteeemails[]" value="<?php echo $email ?>"><?php echo $email ?></p>
                        <?php } }else { echo '<br/><p class="form-label">Import Gmail contacts which will list here..</p>'; } ?>
                    </div>
                </div>
            </div>
            
            <div class="row manullayEmail" style="display:none;">
              <div class="col-3">
                <div class="labelDiv">
                  <label class="form-label text-right"></label>
                </div>
              </div>
              <div class="col-5">
                <?php echo form_textarea($emails); ?>
                <?php echo form_error('emails'); ?>
                <span id="error-emails"></span> </div>
            </div>
            

             <div class="row">
              <div class="col-3">
                <div class="labelDiv">
                    <label class="form-label text-right">Name of Sender</label>
                </div>
              </div>
              <div class="col-5">
                <?php echo form_input($nameOfSender); ?>
                <?php echo form_error('nameOfSender'); ?>
                <span id="error-nameOfSender"></span>
              </div>
            </div>

           
            <div class="row">
              <div class="col-3">
                <div class="labelDiv">
                <label class="form-label text-right">Subject Line</label>
                  
                </div>
              </div>
              <div class="col-5">
                <?php echo form_input($subjectLine); ?>
                <?php echo form_error('subjectLine'); ?>
                <span id="error-subjectLine"></span> </div>
            </div>
            
          </div>
          
       </div>

      <div class="panel_footer">
        <div class="pull-right">
            <input type="hidden" name="eventId" value="<?php echo $eventId; ?>">
            <input type="hidden" name="isPassword" id="isPassword" value="">
            <input type="hidden" name="selInvitationType" id="selInvitationType" value="">
            <input type="submit" value="Send Invitation" class="btn-normal btn">
        </div>
      </div>
       
      </div>
     <?php echo form_close();?>
    </div>
  </div>
  <!-- Right Panel -->
  
  <div class="col-15 footer_bg">
    <div class="container">
      <div class="invitation_content_chkbox">
        
      </div>
    </div>
  </div>
  
  <div class="col-15">
    <div class="container">
      <div class="invitation_content_rgeisterbox common_prop">
        
        
      </div>
    </div>
  </div>
  </form>
 <!-- End Registartion -->
</div>
<div class="other-logo-sec common_prop">
    	<div class="container">
        	<div class="">
            	
            </div>
        </div>
    </div>
<?php $this->load->view('front_footer'); ?> 

<!-- Invitation popup display after publish event-->
<a class="show_invitation_success" data-toggle="modal" data-target=".invitation_success_popup"></a>
<div class="modal fade invitation_success_popup">	
  <div class="modal-dialog modal-sm-as">
	<div class="modal-content">
	  <div class="modal-header managers_color">        
		<h4 class="modal-title">Invitation Success</h4>
	  </div>
	  <div class="modal-body">
		<div class="modal-body-content ">
			<label class="form-label success_message_show"></label>
           <div class="invitationbtn">
                <a class="esc_btn_press btn-normal btn close_msg_popup" href="<?php echo base_url().'events/invitation/'. $eventdetails->event_url; ?>">View Page</a>
                <span> OR </span>
                <a class="esc_btn_press btn-normal btn close_msg_popup" href="<?php echo base_url()?>dashboard">Go Back to Event Set Up</a>
           </div>
            
		</div>
		<div class="modal-footer">
			<div class="pull-right">
				<button class="btn-normal btn popup_cancel" data-dismiss="modal"><div class="btn_center">Close</div></button>
			</div>         	
		</div>     
	  </div>               
	</div>
  </div>
</div>

<script>
$('.circleMenueFront').css('display','none');
ajaxdatasave('formEmailInvitationSetup', 'events/addEmailInvitation', false, true, false, false, true, '', '');    
function contactget(val){
        var eventId = '<?php echo $eventId; ?>';
        var fromData='val='+val+'&eventId='+eventId;
        $.post(baseUrl+'events/typeselectoptions',fromData, function(data) {
          if(data.url !="" && data.url != null){				      
                  window.location.href = data.url;
                  }
        }, "json");
  }
  
  $('#invitation_types_email').change(function(){
    var option = $('option:selected', this).attr('pass');
    var optionVal = $('option:selected', this).text();
    $('#isPassword').val(option);
    $('#selInvitationType').val(optionVal);
  });
</script>
