<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

/*set user details data*/
$eventId            = (!empty($eventId))? $eventId :'';
$code = $this->input->get('code');
$checkedStatus = (!empty($code)) ? 'true' : 'false';
/* Define form fields */
$formEmailInvitationSetup = array(
'name'          => 'formEmailInvitationSetup',
'id'            => 'formEmailInvitationSetup',
'method'        => 'post',
'class'         => '',
);
$nameOfSender = array(
'name'          => 'nameOfSender',
'value'         => '',
'id'            => 'nameOfSender',
'type'          => 'text',
'autocomplete'  => 'off',
'class'         => 'xlarge_input',
);  
$subjectLine = array(
'name'          => 'subjectLine',
'value'         => '',
'id'            => 'subjectLine',
'type'          => 'text',
'class'         => 'xlarge_input',
'autocomplete'  => 'off',
);    
$emails = array(
    'name'          => 'emails',
    'value'         => '',
    'id'            => 'emails',
    'rows'          => '3',
    'placeholder'   => 'Enter separated by semicolon or comma..',
    'class'         => 'form-textarea'
);
?>
<div class="page_content">
    <div class="container">
        <div class="row">
            <div class="col-9">
            <!-- Start panel -->
            <div class="panel open" id="event_contact_person_panel">
              <div class="panel_header">Create Email Invitation</div>
              <div class="panel_contentWrapper" id="showhideformdivpersonalContact" style="display:block">
              <div class="infobar">
                <p class="dn" id="event_contact_person_info">The Access ID# is a unique identifier assigned to every registrant using this site. Use it to access, edit, cancel, pay and rebook items in your registration.</p>
                <span class="info_btn" onclick="showHideInfoBar('event_contact_person_info');"></span>
              </div>
                <?php echo form_open(base_url().'events/addEmailInvitation/'.$eventId,$formEmailInvitationSetup); ?>
                      <div class="panel_content">
                        
                        <!-- Add Guests -->
                        <div class="row">
                          <div class="col-3">
                            <div class="labelDiv">
                              <label class="form-label text-right">Add Your Guests<span class="required" aria-required="true"></span></label> <span class="info_btn"><span class="field_info xsmall">If the event is open to anyone, list is as public and it will appear on our public event register.</span></span>
                            </div>
                          </div>
                          <div class="col-5">
                            <div class="radioDiv">
                            <input type="radio" name="isImport" value="1"  id="importByMedia" class="radioButton isCheckedEvent" lang="media" 
                            checked="checked">                    
                            <label class="form-label pull-left" for="importByMedia"><span>Import emails from your Gmail address book</span></label>
                            </div>
                            <div class="radioDiv">
                                <input type="radio"  name="isImport" value="0" id="manullayEmails" class="radioButton isCheckedEvent" lang="manullay">                    
                                <label class="form-label pull-left" for="manullayEmails"><span>Manullay enter email addresses</span></label>
                            </div>
                            <span id="error-dinner_dance6"></span> </div>
                        </div>
                        
                        <div class="row mediaEmail" >
                          <div class="col-3">
                            <div class="labelDiv">
                              <label class="form-label text-right"></label>
                            </div>
                          </div>
                          <div class="col-5">
                                <input type="button" value="GMAIL" class="btn-normal btn" onclick="contactget(2);"><br/><br/>
                                <div class="" id="" style="width: 350px;height: 150px;background: whitesmoke;overflow: auto;">
                                    <?php //print_r($alldata); ?>
                                    <?php if(!empty($alldata)) { foreach($alldata as $email) { ?>
                                    <p><input type="checkbox" name="inviteeemails[]" value="<?php echo $email ?>"><?php echo $email ?></p>
                                    <?php } }else { echo '<br/><p class="form-label">Import Gmail contacts which will list here..</p>'; } ?>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row manullayEmail" style="display:none;">
                          <div class="col-3">
                            <div class="labelDiv">
                              <label class="form-label text-right"></label>
                            </div>
                          </div>
                          <div class="col-5">
                            <?php echo form_textarea($emails); ?>
                            <?php echo form_error('emails'); ?>
                            <span id="error-emails"></span> </div>
                        </div>
                        
                         <div class="row">
                          <div class="col-3">
                            <div class="labelDiv">
                                <label class="form-label text-right">Name of Sender</label>
                            </div>
                          </div>
                          <div class="col-5">
                            <?php echo form_input($nameOfSender); ?>
                            <?php echo form_error('nameOfSender'); ?>
                            <span id="error-nameOfSender"></span>
                          </div>
                        </div>
                       
                        <div class="row">
                          <div class="col-3">
                            <div class="labelDiv">
                            <label class="form-label text-right">Subject Line</label>
                              
                            </div>
                          </div>
                          <div class="col-5">
                            <?php echo form_input($subjectLine); ?>
                            <?php echo form_error('subjectLine'); ?>
                            <span id="error-subjectLine"></span> </div>
                        </div>
                        
                      </div>
                      <div class="panel_footer">
                        <div class="pull-right">
                            <input type="hidden" name="eventId" value="<?php echo $eventId; ?>">
                            <input type="submit" value="Send Invitation" class="btn-normal btn">
                        </div>
                      </div>
                    <?php echo form_close();?>
              </div>
            </div>
             <!-- End panel -->
            </div>
        </div>
    </div>
</div>
<style>
.mainNavWrapper { display:none;}
</style>

<script>
ajaxdatasave('formEmailInvitationSetup', 'events/addEmailInvitation', false, true, false, false, true, '', '');    
function contactget(val){
            var eventId = '<?php echo $eventId; ?>';
            var fromData='val='+val+'&eventId='+eventId;
            $.post(baseUrl+'events/typeselectoptions',fromData, function(data) {
			  if(data.url !="" && data.url != null){				      
			          window.location.href = data.url;
                      }
            }, "json");
      }
</script>
 

