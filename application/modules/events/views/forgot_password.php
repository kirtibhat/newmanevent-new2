<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

$formForgot = array(
    'name'      => 'formForgot',
    'id'        => 'formForgot',
    'method'    => 'post',
    'class'     => 'wpcf7-form',
);

$forgotDelegateEmail = array(
    'name'      => 'forgotDelegateEmail',
    'value'     => '',
    'id'        => 'forgotDelegateEmail',
    'type'      => 'email',
    'class'     => 'xlarge_input input100',
    //'placeholder' => 'Email address',
    'size'      => '40',
    'autocomplete' => 'off',
);
?>

<!-- Modal -->
<div class="modal fade forgotPass" id="">
  <div class="modal-dialog">
    <!-- Login Form -->
    <div class="modal-content">
     <?php echo form_open(base_url('events/forgotpassword'), $formForgot); ?>
      <div class="modal-header">        
        <h4 class="modal-title">Forgot Password?</h4>
      </div>
      <div class="infobar">
        <p class="dn" id="forgot_pass_info">Please enter the email address you used for your registration and we'll send you a reminder.</p>
        <span class="info_btn" onclick="showHideInfoBar('forgot_pass_info');"></span>
      </div>
      <div class="modal-body">
        <div class="modal-body-content">
          <div class="row-fluid">
              <div class="col-fluid-15">
                <div class="labelDiv pull-left">
                  <label class="form-label">Email address<span class="required" aria-required="true">*</span></label>
                </div>
              </div>
            <div class="col-fluid-15">
                <?php echo form_input($forgotDelegateEmail); ?>		
                <span id="error-forgotDelegateEmail"></span>	
            </div>
          </div>
        </div>
        <div class="modal-footer">
            <div class="pull-right">
            <div class="btn_wrapper">
            <input type="button" name="logincancel" value="Cancel" id="logincancel" class="popup_cancel submitbtn btn-normal btn" data-dismiss="modal">
            <input type="submit" name="loginsubmit" value="Send" id="loginsubmit" class="popup_login submitbtn btn-normal btn" >
			</div>       
            </div>         	
        </div>     
      </div>        
    <input type="hidden" value="loginpost" name="formaction">
    <?php echo form_close(); ?>      
    </div>
    <!-- End Model Content -->
  </div>
</div>
<script>
$('#formForgot').validate({ 
		rules: {
			forgotDelegateEmail: {
				required: true,
				email: true
			}
		},
		messages: {
            email: {
				required:"Email address required.",
				checkRegx:"Enter valid email address."
			}
		},
		errorPlacement: function ($error, $element) {
			var name = $element.attr("name");
			$("#error-" + name).append($error);
			$(".parsley-errors-list").remove();
		}
});	
</script>
