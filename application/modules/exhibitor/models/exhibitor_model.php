<?php

class Exhibitor_model extends CI_model{
	
	
	function __construct(){
		parent::__construct();	
			
	}
	
	//..........................................................................................................
	/* @description  : This function is used to remove exhibitor booths
    * @return        : true/false
    * @param         : exhibitorId 
    */
    
    function removeExhibitorBooth($exhibitorId)
    {
		$response=false;
		$user_id=frntUserLogin('exhibitor');
		
		
		$post=$this->input->post();
		if(!isset($post['boothIds']))
		{
			redirect(base_url('frontend/index/'));
		}
		 $boothIds=$this->input->post('boothIds');	 
		 $selectedBooths=explode(',',$boothIds);
		
		//get user booth 
		$userbooths= $this->common_model->getDataFromTabelWhereWhereIn('frnt_exhibitor_booth','booth_id',array('exhibitor_id'=>$exhibitorId,'user_id'=>$user_id),'booth_status',array('0','2'));
		
		if(!empty($userbooths))
		{
			foreach($userbooths as $booth)
			{
				if(!in_array($booth->booth_id,$selectedBooths))
				{
					//remove booth 
					 $response=$this->common_model->deleteRowFromTabel('frnt_exhibitor_booth',array('booth_id'=>$booth->booth_id,'user_id'=>$user_id));
				    
				}
			}
		}
		return $response;			 
    }
    
    //..........................................................................................................
	/* @description  : This function is used to check sold p[ackage.
    * @return        : true/false
    * @param         :  eventId
    */
    function getSoldExhibitorPackageIds($eventId)
    {
		 //create array for purchase packages
		 $purchasePackages=array();
		 
		 $user_id=frntUserLogin('exhibitor');
		 
		 //get exhibitor package by eventId
		 $exhibitors=$this->common_model->getDataFromTabel('exhibitor','id',array('event_id'=>$eventId));
    
		if(!empty($exhibitors))
		{	
			foreach($exhibitors as  $exhibitor)
			{
				 $issoldPackage='yes';
				 $selectedBooths=$this->common_model->getDataFromTabel('frnt_exhibitor_booth','booth_id,booth_status',array('exhibitor_id'=>$exhibitor->id));
				if(!empty($selectedBooths))
				{
					foreach($selectedBooths as $booth)
					{
						
						//check approved booth
						if($booth->booth_status!='1')
						{    
							  $issoldPackage='no';
							  break;
						}	
					}
				
					if($issoldPackage=='yes') 
					{   
						 //get exhibitor package id
						 $purchasePackages[]=$exhibitor->id;
					}
			    }
				
			} // end of the outer loop
		}
	      
		  return $purchasePackages;	
		
	}
	
	//..........................................................................................................

	/* @description  : This function is used to get user select  exhibitor booths
    * @return        : select booth ids
    * @param         : exhibitorId
    */
     function getUserSelectBooth($exhibitorId)
     {
		  //to create array to take user selected booth ids
		   $boothIdArray=array();
		  //to get user select exhibitor booths
		   $user_id=frntUserLogin('exhibitor');
	       $selectedbooths= $this->common_model->getDataFromTabelWhereWhereIn('frnt_exhibitor_booth','booth_id',array('exhibitor_id '=>$exhibitorId,'user_id'=>$user_id),'booth_status',array('0','1'));
	       if(!empty($selectedbooths))
	       {
			   foreach($selectedbooths as $booth)
			   {
				   $boothIdArray[]=$booth->booth_id;
			   }
		   }
		   return $boothIdArray; 
	 }
    
	//..........................................................................................................

	/*@description  : This function is used to check personal contact form fields
    * @return        : Validation message
    * @param         :  
    */
    
    function checkContactFormFields()
    {
		
		 $eventId=$this->input->post('regis_event_id');
		 $user_id=frntUserLogin('exhibitor');
		  
		 $this->form_validation->set_rules('contact_first_name', 'contact first name', 'trim|required');
		 $this->form_validation->set_rules('contact_surname', 'contact surname', 'trim|required');
		 $this->form_validation->set_rules('contactphone', 'contact phone', 'trim|required');
		 $this->form_validation->set_rules('contact_cmp_name', 'contact Company name', 'trim|required');
		 $this->form_validation->set_rules('contact_position', 'contact position', 'trim|required');
		 if(!$user_id)
		 {
			 $this->form_validation->set_rules('contact_email', 'contact email', 'trim|required');
			 $this->form_validation->set_rules('contact_password', 'contact Password', 'trim|required');
			 $this->form_validation->set_rules('contact_confirm_password', 'contact Confirm Password', 'trim|required');
		 }

		if (!$this->form_validation->run())
		 {    
			 echo json_encode(array('msg' => validation_errors(), 'is_success'=>'false'));
			 die;
		 }

		if($this->input->post('contact_password')!=$this->input->post('contact_confirm_password')){

			 echo json_encode(array('msg' => 'Password Not Match', 'is_success'=>'false'));
			 die;
		}
		 // to check email exist for this event
		 $email = $this->input->post('contact_email');
		 $is_exist =$this->common_model->getDataFromTabel('user','id',array('email'=>$email,'event_id'=>$eventId,'user_type_id'=>'4'));
		 if ($user_id=='' && !empty($is_exist)) {
			 echo json_encode(array('msg' => 'Email Already Used Please Try Another.', 'is_success'=>'false'));
			 die;
		 }
	}
	

     
}

?>
