 <?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
//--------To show exhibitor packages booths------------------------------------------------------//	
   $exhibitor_id='';
   $exhibitorName = (isset($exhibitorname) && !empty($exhibitorname))?$exhibitorname:'';
   $exhibitorPrice = (isset($exhibitorprice) && !empty($exhibitorprice))?$exhibitorprice:'';
   $eventId = (isset($event_id) && !empty($event_id))?$event_id:'';

   $user_id=frntUserLogin('exhibitor');

?> 
<div class="container mt40lesseight container_main">
	<?php
	      //to load header
          $this->load->view('common/header'); 
         
           //to load exhibitor popup
           $this->load->view('exhibitor_login_popup'); 
          
           // to show selected booth in popup 
          $this->load->view('booth_popup'); 

	?>
	
	
	
    <div class="row-fluid mt10">
    	<div class="span7 signupresponsive" id="frontendleft">
        <div class="container-fluid signupbg">
                	<div class="accorianbg bdrbN">
                            <div class="accordion-group bg_00aec5">
                                <div class="accordion-heading">
                                    <div href="#ContactPersonDetails" data-parent="#monogram-acc" class="accordion-toggle">
                                    <?php echo $exhibitorName; ?>
                                    </div>
                                </div>
                                
                                <div class="accordion-heading exhi_Darkblue">
                                    <div href="#ContactPersonDetails" data-parent="#monogram-acc">
                                    $<?php echo $exhibitorPrice ; ?> <span class="fs14">(+GST)</span>
                                    </div>
                                </div>
                                
                                <div class="accordion-body">
                                <div class="innerbody exhilisting">
                                <ul>
                                	<li>1 x Exhibition booth 2m wide x 3.5m deep</li>
                                    <li>2 x Walls, in Velcro Fabric</li>
                                    <li>2 x Lights, 120 watt spotlights</li>
                                    <li>1 x Signage, Fascia panel with organisations name</li>
                                    <li>2 x Power outlet, 5 amp general purpose outlet</li>
                                    <li>2 x Complimentary delegate registrations</li>
                                    <li>2 x Complimentary tickets to Dinner and Drinks</li>
                                </ul>
                                
                                <ul class="linkcolor">
                              <li><a href="#">Acknoledgement of your organisation on the summit 
                              	website and a hyperlink to your organisations website</a></li>
								<li><a href="#">Your organisations name on the Expo passport</a></li>
                                <li><a href="#">Your organisations will recieve one complimentary satchel</a></li>
                                <li><a href="#">Your organisation will have access to delegate information</a></li>
                                </ul>
                                
                                <div class="formheading">Select which booth you would like</div>
                                <div class="mendotryfield">Please note if there is a number missing it is already sold.</div>
                                <?php
                                   
                                    if(isset($floorPlanPDF) && !empty($floorPlanPDF)) 
                                    {
										
                                      ?>
                                         <a href="<?php echo $floorPlanPDF;?>" target="_blank" >
											<button id="closebtn" class="submitbtn_cus mt10 mr0 bg_741372 fleft" type="button">SHOW PDF OF FLOOR PLAN</button>
										</a>
                                        
                                      <?php
							        }
							        else
							        {
										?>
											<button id="closebtn" class="submitbtn_cus mt10 mr0 bg_741372 fleft noPDFMSG" type="button">SHOW PDF OF FLOOR PLAN</button>
										<?php
									}
							      
                                 ?>
                              
                                 
                                 
                               <div class="clearfix"></div>
                                <div class="mt20"></div>
                                <?php
							    if(isset($exhibitorbooths) && !empty($exhibitorbooths))
							    {
								    foreach($exhibitorbooths as $booth)
								    {
										$title="";
										$pendingClass='';
										$purchasebooth=false;
										
										$exhibitor_id=$booth->exhibitor_id;
										//to check pending booth
										if(isset($selectBoothArray) && !empty($selectBoothArray))
										{
											 if(in_array($booth->id,$selectBoothArray))
											 {
												 $pendingClass='pendingbooth';
												 $title='Pending Request';
										     }
										     
										}
									
										//to check user selected booth
										 if(isset($userSelectedBooth) && !empty($userSelectedBooth))
										 {
											 if(in_array($booth->id,$userSelectedBooth))
											 {
												 $pendingClass='slectedbooth';
											 }
										 }
										
										//to check purchase booth
										if(isset($purchaseBooths) && !empty($purchaseBooths))
										{
											 if(in_array($booth->id,$purchaseBooths))
											 {
												 $purchasebooth=true;
										     }
										}
										if(!$purchasebooth)
										{
									     ?>
									 
									 
									     <a title="<?php echo $title; ?>" class="floreplane_btn selectbooth booth<?php echo $booth->id.' '.$pendingClass; ?>"><?php echo $booth->booth_position; ?></a>
                                      
                                         <input type="hidden" name="" class="" value="<?php echo $booth->id; ?>">	 	  
                                         <input type="hidden" name="" id="booth_position<?php echo $booth->id;?>" value="<?php echo $booth->booth_position; ?>">	 	  
                                    
									     <?php 
								   
								         }
								         
							        }
						        }
					             ?>
					            
					             <div class="btn_div">
									  <div class="clearfix"></div>
									  
									<a href="<?php echo base_url().'exhibitor/index/'.encode($eventId);?>">    <button id="closebtn" class="submitbtn_cus mt10 mr10 bg_741372 booth_back_btn" type="button">BACK</button></a>
                                    <a href="<?php echo base_url().'exhibitor/exhibitorhome/'.encode($eventId);?>"><button id="closebtn" class="submitbtn_cus mt10 mr10 bg_741372" type="button">Pay</button></a>
                                
                                    <button id="closebtn" class="submitbtn_cus mt10 mr10 bg_741372 saveExhibitorBooth" type="button">SAVE</button>
                                 
                                </div>   
                                  <input type="hidden" name="exhibitor_id" id="exhibitor_id" value="<?php echo $exhibitor_id; ?>">
                                  
                                  <input type="hidden" name="selected_booth_id" id="selected_booth_id" value="">	 
                                  <input type="hidden" name="current_booth_id" id="current_booth_id" value="">		  
                                  <input type="hidden" name="user_id" id="user_id" value="<?php echo $user_id; ?>">	 	  
                                  <div class="clearfix"></div>
                                 
                                
                                </div>
                                </div>
                            </div>
                </div> <!-- /accordionbg -->
            </div>
        </div>
        
        <div class="span5 addbgcontainer pull-right spnncerresponsive" id="frontendright">
        	<div class="addbg"> <div class="addcontent">Advertisement</div></div>
            <div class="addbg"> <div class="addcontent">Advertisement</div></div>
        </div>
         <!-- /sponcerbg -->
         
         
         
        
        <div class="clearfix"></div>
    </div>
    <div class="clearfix"></div>
    <div class="spacer40"></div>    
</div> <!-- /container -->

                          

 


<script type="text/javascript">
$(document).ready(function() {
	
	 var rightHeight= $("#frontendright").height();
	 var leftHeight= $("#frontendleft").height();
	 var getWindowWidth = $( window ).width();
	 
	 $('.back_btn').hide();
	if(getWindowWidth > 960){
		if(leftHeight > rightHeight){
			$("#frontendright").css('height',leftHeight);
		}else{
			$("#frontendleft").css('height',rightHeight);
		}
	}
	
	//$('#frontendleft').height($("#frontendright").height()); 
});
</script>
