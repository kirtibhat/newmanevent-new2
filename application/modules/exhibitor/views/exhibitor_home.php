<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//............................This form load all sponsor form...................
  
	 $user_id=frntUserLogin('exhibitor'); 
	
	 $eventId = (isset($event_id) && !empty($event_id))?$event_id:'';
	 
	 //to create form
     $formRegistratType = array(
		'name'	 => 'registrantTypeForm',
		'id'	 => 'registrantTypeForm',
		'method' => 'post',
		'class'  => 'form-vertical',
	);	
 ?>

<div class="container mt40lesseight container_main">
	
	<!-- to load header-----------  -->
    <?php $this->load->view('common/header'); ?>
    
   <div class="row-fluid mt10">
	<div class="span7 signupresponsive" id="frontendleft">
	  <div class="container-fluid signupbg">

		<div class="usercontainer">
			
			 <div class="commonS_circle ml0 mr15 mt6" id="usershow">
				  <div class="smalluser"></div>
			 </div>
			
			<div class="usertooltio">
				
				   <div class="tooltiparrow"></div>
				   <?php echo lang('registerForThisEventMsg'); ?>
				   <div class="clearfix"></div>
				   <a href="<?php echo base_url('exhibitor/index/'.encode($eventId)); ?>"><button type="button" class="submitbtn_cus mt10 mr0 bg_741372" >Back</button></a>
					
			</div> 
		</div>
		
		
		<!-- start exhibitor login form popup  -->
	

		       <?php 
		         
		         $this->load->view('exhibitor_login_popup');
		          if($user_id > 0){ 
		        ?>
                  <a class="submitbtn_cus mt10 mr0 bg_741372 add_login_form_field" data-toggle="modal" data-target="#exhibitor_booth_popup" ><?php echo lang('frntLogin'); ?></a>
	             <?php 
	              }else{ 
	             ?>
	              <a  class="submitbtn_cus mt10 mr0 bg_741372 logout" ><?php echo lang('frntLogout'); ?></a>
                 <?php } ?>
        <!--  end add_login_form_field_popup  -->
    
		<div class="clearfix"></div>
		 <div class="accorianbg">
		     <div class="accordion" id="monogram-acc">
		  
		    <?php 
		  
			 // load Contact person form view
			 $this->load->view('contact_person');
			
		    // load sponsor type view
		     echo form_open($this->uri->uri_string(),$formRegistratType);
		  	 $this->load->view('payment');
		  	 
		  	 //to show event booth in popup
		  	 // $this->load->view('common/booth_popup');
		
			 // load sponsor booth view form
			// $this->load->view('exhibitor_booth');
		
		     
			 echo form_close();
		     ?>
		   	 
	         </div>	
		   </div> <!-- close accorianbg div --> 
		
   	 </div>	
   	</div>    	
   	 	
		 <div class="span5 addbgcontainer pull-right spnncerresponsive" id="frontendright">
		    <div class="addbg"> <div class="addcontent">Advertisement</div></div>
		    <div class="addbg"> <div class="addcontent">Advertisement</div></div>
	     </div>
	      <div class="clearfix"></div>
	     <!-- /sponcerbg -->
         
   </div> <!-- close row-fluid mt10 div --> 
     <div class="spacer40"></div>          
</div> <!-- /container -->




<script type="text/javascript">
	

 /** to hide loder **/
 
 $('.logout').hide();
 $('.loginvalidate').hide();
 $('.loadershow').hide();
 
 /** code for check logout user **/
 	
var  checkSession='<?php echo  $user_id; ?>';

  
    if(checkSession>0)
    {
		$('.logout').show();
		$('.add_login_form_field').hide();
	}

</script>


	

