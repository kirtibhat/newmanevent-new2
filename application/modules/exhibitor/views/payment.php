 <div class="accordion-group step2">
		<div class="accordion-heading">
			<div class="accordion-toggle" data-toggle="collapse" data-parent="#monogram-acc" href="#Payment">
			Step 2 - Payment
			</div>
		</div>
		<div class="accordion-body collapse" id="Payment">
		<div class="innerbody">
			<div class="tableheading position_R min-height44">
			<div class="printpara">Please confirm these details are correct.</div>
			<div class="printicon pull-right"></div>
			</div>
			
			
			 <div class="pymtregdetail">
			
				<table>
				<tr class="headingrow">
					<th>Registration Details</th>
					<th>Total</th>
					<th></th>
					</tr>
					 
				<tr class="position_R">
				<td><font class="fsb">REGISTRANT 1</font></td>
				<td>&nbsp;</td>
				<td><div class="editicon"></div></td>
				</tr>
				<tr>
					<td>Booking Number #23456</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				
				<tr>
					<td>Standard Registration</td>
					<td>$100.00</td>
					<td>&nbsp;</td>
				</tr>
				
				<tr>
				<td><div class="spacer15"></div></td>
				<td><div class="spacer15"></div></td>
				<td><div class="spacer15"></div></td>
				</tr>
				
				
				 <tr>
					<td>Mr Jed Knight</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				
				 <tr>
					<td>somewhere@nowhere.com.au</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				
				 <tr>
					<td>Dietary Requirement: Other - Allergic to Peanuts</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				
				
				 <tr>
				<td><div class="spacer15"></div></td>
				<td><div class="spacer15"></div></td>
				<td><div class="spacer15"></div></td>
				</tr>
				
				<tr>
					<td>Offsite Dinner - 3rd January 2013, 5:00pm to 10:00pm</td>
					<td>$70.00</td>
					<td>&nbsp;</td>
				</tr>
				
				<tr>
					<td>2 Additional Guests</td>
					<td>$140.00</td>
					<td>&nbsp;</td>
				</tr>
				
				
				<tr>
					<td>Field Trip - 4nd Janurary 2013, 1:00pm to 5:00pm</td>
					<td>$100.00</td>
					<td>&nbsp;</td>
				</tr>
				
				<tr>
					<td>Dinner and Drinks - 5th Janurary 2013, 7:30pm to 11:00pm</td>
					<td>$100.00</td>
					<td>&nbsp;</td>
				</tr>
				
				
				 <tr>
				<td><div class="spacer15"></div></td>
				<td><div class="spacer15"></div></td>
				<td><div class="spacer15"></div></td>
				</tr>
				
				 
				<tr>
					<td>Primary Health - 3rd Janurary 2013, 10:00am to 11:00am</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				
				
				 <tr>
					<td>Buliding Blocks Workshop - 3rd Janurary 2013, 1:00pm to 3:00pm</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				
				 <tr>
					<td>Primary Health - 4nd Janurary 2013, 10:00am to 11:00am</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				
				
				 <tr>
					<td>Ed Talks - 4th Janurary 2013, 4:00pm to 6:00pm</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				
				<tr>
					<td>The Big Ideas Workshop - 5th Janurary 2013, 1:00pm to 3:00pm</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				
				 <tr>
				<td><div class="spacer15"></div></td>
				<td><div class="spacer15"></div></td>
				<td><div class="spacer15"></div></td>
				</tr>
				
				
				<tr class="headingrow">
				<th class="text-right">SUB TOTAL</th>
				<th>$510.00</th>
				<th></th>
				</tr>
				

				
			 </table>
			 
		
			 </div><!------ /payment registration detail ------>
			 
			 <div class="pymtregdetail mt20">
				<div class="tableheading_2 pt2pb2">
								<input type="checkbox" name="ex1" id="">
								<label class="paymentcond">I have read, understood and agreed to the 
								terms and conditions. <br><a href="#">Click here to read</a></label>    
				 </div>
				 
				 <div class="tableheading_2 pt2pb2 mt6">
								<input type="checkbox" name="ex1" id="">
								<label class="paymentcond">Send Invoice/Confirmation email to Registrant and Organiser.</label>    
				 </div> 
			 </div>
			 
			 <div class="finapayment sideSgestcont pl0">
				<div class="heading">Payment</div>
			   
			   <div class="tableheading_2">
								<input type="radio" name="ex7" id="ex1_a">
								<label>Credit Card</label>
					</div>
					 <div class="formoentform_container pl25">
					<div class="position_R">
									<input type="text" name="t1" placeholder="Credit Card Name">
					 </div>
					 
				  
							<div class="position_R mb10">	
								<input id="spinner" name="value" max="10" min="0" value="MasterCard" class="width100per">
							   </div>    
					  
						
						
					 <div class="position_R">
									<input type="text" name="t1" placeholder="Credit Card Number">
					 </div>
					 
					 <div class="position_R">
									<input type="text" name="t1" placeholder="CCV" class="maxW70">
					 </div>
					 
					 <div class="position_R">
					 <div class="pull-left maxW70">
					 <input id="spinner" name="value" max="10" min="0" value="02" class="width100per">
					 </div>
					 <div class="pull-left maxwidth100 ml25">
					 <input id="spinner" name="value" max="2022" min="1990" value="2013" class="width100per">
					 </div>
					<div class="expiredate"> Expiry Date</div>
					 <div class="clearfix"></div> 
					 </div>
					 
					   <div class="clearfix"></div> 
					   <div class="spacer15"></div>
					 <div class="mb20">
					
					 <input type="submit" value="SUBMIT & PAY" class="frontendsubmitbtn mr0">
					 <div class="clearfix"></div> 
					 </div>
					 </div>
			 </div>
			 
			</div>
		</div>
	</div>
