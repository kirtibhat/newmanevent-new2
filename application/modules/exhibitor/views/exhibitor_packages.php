 <?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
   //--------To show exhibitor packages------------------------------------------------------//	
   $eventId=(isset($event_id) && !empty($event_id))?$event_id:'';
   $user_id=frntUserLogin('exhibitor');
?> 
	
<div class="container mt40lesseight container_main">
	
	<?php
	     //to load header
	     echo $this->load->view('common/header'); 
	?>
    
    <div class="row-fluid mt10">
    	<div class="span7 signupresponsive">
        	<div class="container-fluid signupbg">
            <div class="spons2_heading">What type of exhibitor package would you like? 
            
                <button id="closebtn" class="submitbtn_cus mt10 mr10 bg_741372 logout"><?php echo lang('frntLogout'); ?></a>
                <a href="<?php echo base_url().'frontend/index/'.encode($eventId);?>"><button id="closebtn" class="submitbtn_cus mt10 mr10 bg_741372" type="button">Back</button></a>
               
            </div>
            	<div class="sponsorinner_scroll" id="CanScrollWithYAxis">
            	<div class="sponsorinner" >
					    <?php
							//to show exhibitor packages
							if(isset($exhibitorDetails) && !empty($exhibitorDetails))
							{
								foreach($exhibitorDetails as $exhibitor)
								{
									 $packageSold=false;
									 $total_price=$exhibitor->total_price;
									 if($total_price=='')
									 {
										 $total_price=0;
									 }
									 //to check purchase packages
								    if(isset($exhibitorSoldPackages) && !empty($exhibitorSoldPackages))
									{
										if(in_array($exhibitor->id,$exhibitorSoldPackages))
										{
											
											$packageSold=true; 
										}
									}
									 ?>
									<a href="<?php echo base_url().'exhibitor/exhibitorbooths/'.encode($exhibitor->id);?>" title="Click"> 
									 <div class="inner">
										
										<div class="topheading"><?php echo $exhibitor->exhibitor_name; ?> </div>
										<div class="priseheading"><?php echo $total_price; ?> <font class="fs14">(+GST)</font></div>
										<div class="topparagraph"><?php echo $exhibitor->additional_details; ?></div>

										 <div class="listingbg">
											<ul>
												<li>Only one available</li>
												<li>Acknoledgement of your organisation on the summit website and a hyperlink to your organisations website</li>
												<li>Your organisations name on the Expo passport</li>
												<li>Your organisations will recieve one complimentary satchel</li>
												<li>Your organisation will have access to delegate information</li>
											</ul>
										 </div>
										    <?php
										    
										    if($packageSold)
										    {
												
										         ?>
										             <div class="purchasepackage soldoutbg">Purchase Package <div class="iconbg"><img src="<?php echo IMAGE; ?>soldout.png" alt="package"></div> </div>
                        
										         <?php
										     }
										     else
										     {
												 ?>
										            <div class="purchasepackage">Purchase Package <div class="iconbg"><img src="<?php echo IMAGE; ?>circle_prise.png" alt="package"></div> </div>
												 <?php
											 }
										    ?>
									 </div>
								    </a>
								  <?php
							    }
							  
						    }
						    else
						    {
								?>
								<div class="">
								        NO exhibitor package available for this event.
								   </div> 
							  <?php	   
							}
					    
					     ?>  
                  </div>
             
                </div><!-- sponsorinner_scroll -->
                  
            </div>
           
            
        </div>
        
         <div class="span5 addbgcontainer pull-right spnncerresponsive" id="frontendright">
		    <div class="addbg"> <div class="addcontent">Advertisement</div></div>
		    <div class="addbg"> <div class="addcontent">Advertisement</div></div>
	     </div>
	      <div class="clearfix"></div>
	     <!-- /sponcerbg -->
           
        
            <div class="seprator42"></div>
            <div class="bg_00aec5"></div>
            <div class="bg_7cd0de"></div>
            <div class="bg_a3dce7"></div>
         </div><!-- /sponcerbg -->
         
         
         
        
        <div class="clearfix"></div>
    </div>
    
    <div class="spacer40"></div>    
</div> <!-- /container -->

                            

<script type="text/javascript">


var startLeft = 270;
var divCount = 0;
$(".inner").each(function(){
	 if(divCount > 0){
	 	$(this).css('left',startLeft+'px');
	 	startLeft = startLeft+ 270;
	 }
	 divCount++;
})
//js to load slider
jQuery(document).ready(function ($) {
	"use strict";
	$('#Default').perfectScrollbar();
	$('#CanScrollWithYAxis').perfectScrollbar({useBothWheelAxes: true});
	$('#CanScrollWithXAxis').perfectScrollbar({useBothWheelAxes: true});
  });
  
$('.logout').hide();

 /** code for check logout user **/
var  checkSession='<?php echo  $user_id; ?>';
if(checkSession>0)
{
	$('.logout').show();
}
</script>


