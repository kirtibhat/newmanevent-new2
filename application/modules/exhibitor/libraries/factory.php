<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * @description: This controller is used to manage login, registration
 * @create date: 21-Feb-2013
 * @auther: Rajendra aptidar
 * @email: rajendrapatidar@cdnsol.com
 * 
 */ 


class Exhibitor extends MX_Controller{

	private $ci = NULL;
	
	function __construct($ci){
		//create instance object
		$this->ci = $ci;
	}
	
    /*
     * @description: This function is used for Registration for event 
     * @return: void
     *  
    */
    
    function index()
    {
		
		 //to get event id from url
		 $eventId =decode($this->ci->uri->segment(3));
		 $user_id=frntUserLogin('exhibitor');
		
		if($user_id)
		{
			//to get login user eventId
			$eventId=getEventId('exhibitorid');
		     
			 // get user details for logged in user  
			 $whereEventId=array('event_id'=>$eventId); 
			 $data['userDetails']=$this->ci->common_model->getDataFromTabel('user','*',array('id'=>$user_id));
			 if(!empty($data['userDetails'])){
				$data['userDetails'] = $data['userDetails'][0];
			 }
			
		}
		 //to check valid event id
		 if($eventId=='')
		 {
		 	redirect(base_url('frontend/index/'));
		 }
		//get exhibitor details
		 $exhibitorDetails=$this->ci->common_model->getDataFromTabel('exhibitor','*',array('event_id'=>$eventId));
		
		 if(!$exhibitorDetails)
		 {
			 $msg='No exhibitor package available for this event.';
			 $msgArray = array('msg'=>$msg,'is_success'=>'false');
			 set_global_messages($msgArray);
			 redirect(base_url().'frontend/index/'.encode($eventId));
		 }	
		 $data['exhibitorDetails']=$exhibitorDetails;
		  //to show popup baground color
		 $data['popupHeaderBg'] ='bg_00aec5';  //set popup bg color
			 
		 $data['exhibitorSoldPackages']=$this->ci->exhibitor_model->getSoldExhibitorPackageIds($eventId);
	
		 $data['event_id']=$eventId; 
	
	     // to get customize form details for hreader section
		 $where=array('event_id'=>$eventId);
		 $data['customize_form']=$this->ci->common_model->getDataFromTabel('customize_forms','*',$where);
		 
         //to load exhibitor packages file
		 $this->ci->default_template->load('default_template','exhibitor_packages',$data,TRUE);
	}
     
    
     
    //..........................................................................................................

	/* @description  : This function is used to get exhibitor package booths
    * @return        : booths for exhibitor
    * @param         :  
    */
    
    function exhibitorbooths()
    { 
		 $eventId='';
	    //to get exhibitor id  and decode encoded url id	
	     $exhibitorid=decode($this->ci->uri->segment(3));	
	    
	     $user_id=frntUserLogin('exhibitor');
	    
	     //to check valid exhibitor id
		 if($exhibitorid=='')
		 {
			redirect(base_url('frontend/index/'));
		 }
	    
	     //to show popup baground color
		 $data['popupHeaderBg'] ='bg_00aec5';  //set popup bg color
		 
	    //to get exhibitorbooths
	    $data['exhibitorbooths']= $this->ci->common_model->getDataFromTabel('sponsor_exhibitor_booths','*',array('exhibitor_id '=>$exhibitorid));
   
         //to get exhibitorbooths
	    $exhibitorDetails= $this->ci->common_model->getDataFromTabel('exhibitor','*',array('id '=>$exhibitorid));
        
        if(!empty($exhibitorDetails))
        {
			$data['exhibitorname']=$exhibitorDetails[0]->exhibitor_name;
			$data['exhibitorprice']=$exhibitorDetails[0]->total_price;
			$eventId=$exhibitorDetails[0]->event_id;
			$data['event_id']=$eventId;
		}
        
      
        if($user_id)
        {
			 //to get login user eventId
		    $eventId=getEventId('exhibitorid');
		    
		}	
			//to get exhibitorbooths
		    $selectBoothArray=array();	
            
             //to get all purchase booth
            $purchaseBooths=array();
            
            //to get select axhibitor booth
	        $selectedbooths= $this->ci->common_model->getDataFromTabelWhereWhereIn('frnt_exhibitor_booth','booth_id,booth_status,user_id',array('exhibitor_id '=>$exhibitorid),'booth_status',array('0','1'));
	       
	        if(!empty($selectedbooths))
	        {
			    foreach($selectedbooths as $booth)
			    {
						if($booth->booth_status==1)
						{
							$purchaseBooths[]=$booth->booth_id;
						}
						$selectBoothArray[]=$booth->booth_id;
				}
		    }
		    $data['purchaseBooths']=$purchaseBooths;
	        $data['selectBoothArray']=$selectBoothArray;
	   
	    //o get Flooor Path PDF
	    $floorPlanPDF=$this->ci->common_model->getDataFromTabel('exhibitor_floor_plan','floor_plan,floor_plan_path',array('event_id'=>$eventId));
        if(!empty($floorPlanPDF))
        {
			$data['floorPlanPDF']=base_url().$floorPlanPDF[0]->floor_plan_path.$floorPlanPDF[0]->floor_plan;
		}
		//to get user selected exhibitor booth
		$data['userSelectedBooth']=$this->ci->exhibitor_model->getUserSelectBooth($exhibitorid);
		
		 // to get customize form details for hreader section
		 $where=array('event_id'=>$eventId);
		 $data['customize_form']=$this->ci->common_model->getDataFromTabel('customize_forms','*',$where);
		 
	
		//to load exhibiotor booths file
		$this->ci->default_template->load('default_template','exhibitor_booths',$data,TRUE);
  
    }
    
    
     
    
    //..........................................................................................................

	/* @description  : This function is used to save exhibitor booths
    * @return        : NULL
    * @param         : boothId
    */
    
    function saveexhibitorbooth()
    {
		 $userData='';
		 $exhibitorId='';
		 $response='';
		 $user_id=frntUserLogin('exhibitor');
		 
		//check post exhibitor booths id
		$post=$this->input->post();
		if($post && isset($post['boothIds']))
		{
			 $boothIds=$this->ci->input->post('boothIds');
			 $exhibitorId=$this->ci->input->post('exhibitor_id');
			 
			 $selectedBooths=explode(',',$boothIds);
			 
			 $isbooth=$selectedBooths[0];
			  
			 if(!empty($isbooth))
			{
				foreach($selectedBooths as $boothId)
				{  
					
					 //to get user exhibitor details
				    $userbooth= $this->ci->common_model->getDataFromTabelWhereWhereIn('frnt_exhibitor_booth','booth_id',array('booth_id'=>$boothId),'booth_status',array('0','1'));
                   
					$data['exhibitor_id']=$exhibitorId;
					
                     if(empty($userbooth))
                    {
						 $data['user_id']=$user_id;
						 $data['booth_id']=$boothId;
						 $data['booth_status']='0';
						 //to add exhibitor
						 $userData=$this->ci->common_model->addDataIntoTabel('frnt_exhibitor_booth', $data);
					}
							 
				}
				
			}
				 //remove user unselected booth
				 $response=$this->ci->exhibitor_model->removeExhibitorBooth($exhibitorId); 
				
		}	
		
		$is_success='true';	
	    $msg='Exhibitor booth request has been updated sucessfully.';
	    if($response)
	    {
			$msg='Exhibitor booth request has been removed sucessfully.';
		}
	    if($userData>0)
	    {
			$msg='Exhibitor booth request has been sent sucessfully.';
		   
		}
		$msgArray = json_encode(array('msg'=>$msg,'is_success'=>$is_success));
		echo $msgArray;
		
	}
	
	
    
	//..........................................................................................................

	/* @description  : This function is used to load exhibitor home
    * @return        : 
    * @param         : sponsorId 
    */
    
    function exhibitorhome()
    {
		
		//to get loogedin user id
		$user_id=frntUserLogin('exhibitor');
		
		 //to get exhibitor id  and decode encoded url id	
	     $eventId=decode($this->ci->uri->segment(3));	
	    
	     //to show popup baground color
		 $data['popupHeaderBg'] ='bg_00aec5';  //set popup bg color
		 
	     //to check valid exhibitor id
		 if($eventId=='')
		 {
			redirect(base_url('frontend/index/'));
		 }
		 if($user_id)
		 {
			 //to get loggedin user details
		 	 $data['userDetails']= $this->ci->common_model->getDataFromTabel('user','*',array('id'=>$user_id));
             if(!empty($data['userDetails'])){
				$data['userDetails'] = $data['userDetails'][0];
			 }
         }        
		 //get exhibitor details
		 $data['event_id']=$eventId;
		 
		 // to get customize form details for hreader section
		 $where=array('event_id'=>$eventId);
		 $data['customize_form']=$this->ci->common_model->getDataFromTabel('customize_forms','*',$where);
		 
		 
		 //to load exhibiotor booths file
		 $this->ci->default_template->load('default_template','exhibitor_home',$data,TRUE);
	 
	}
	
	
	
     

	//..........................................................................................................

	/* @description  : This function is used to save conatct person detail
     * @return       : void
     */
    
    function savecontactperson()
    {
		
        //to check post of the form data 
        $post=$this->ci->input->post(); 
		if($post && isset($post['contact_first_name']))
		{
			 $eventId=$this->ci->input->post('regis_event_id');
			 $user_id=frntUserLogin('exhibitor');
			 
			 //check contact form fields validation 
			 $this->ci->exhibitor_model->checkContactFormFields(); 
				 
			 // to get from data in array
			 $userDetail['username'] =$this->ci->input->post('contact_email');
			 $userDetail['user_title'] =$this->ci->input->post('contact_user_title');
			 $userDetail['firstname'] = $this->ci->input->post('contact_first_name');
			 $userDetail['lastname'] = $this->ci->input->post('contact_surname');
			 $userDetail['email'] =$this->ci->input->post('contact_email');
			 $userDetail['phone'] =$this->ci->input->post('contactphone');
			 $userDetail['mobile'] =$this->ci->input->post('contact_mobile');
			 $userDetail['company_name'] =$this->ci->input->post('contact_cmp_name');
			 $userDetail['position'] =$this->ci->input->post('contact_position');
			  
			if(strlen($this->ci->input->post('contact_password'))>0)
			{
			  $userDetail['password'] = md5($this->ci->input->post('contact_password'));

			}
			   
			// to check user login bu id then update form details
			if($user_id)
			{
				$whereUserId=array('id'=>$user_id);
				$this->common_model->updateDataFromTabel('user', $userDetail, $whereUserId);
				echo json_encode(array('msg' => 'Update successfully.', 'is_success'=>'1'));
				 die();
			}
			else
			{
				
				$userDetail['event_id']=$eventId;
				$userDetail['user_type_id'] ='4';
				//to add data in user table and get user id
				$user_id=$this->ci->common_model->addDataIntoTabel('user', $userDetail);
				
				//send email to registered user for email verification
				$from=$this->ci->config->item('admin_email');
				$to=$this->input->post('contact_email');
				$subject='Email varification';
				
				$siteurl=base_url().'exhibitor/emailverification/'.encode($user_id);
				
				$emailData = array(
				'emailId'=>$to,
				'password'=>$this->ci->input->post('contact_password'),
				'activationURl'=>$siteurl,
				); 
				
				//function to send mail
				sendMail($from, $to, $subject,$emailData);
				
				$msg='Add successfully. Please check your email for verification.';
				$msgArray = array('msg'=>$msg,'is_success'=>'true');
			    set_global_messages($msgArray);
				echo json_encode(array('msg' => '', 'is_success'=>'true'));
				die();
			}

				 echo json_encode(array('msg' => 'Failed , Please Try Again.', 'is_success'=>'false'));  
				 die();
					

	    }
    }
    
     //..........................................................................................................

	/*
	*   @description: This function is used to  exhibitor login
    *   @return:message
   */
    function exhibitorLogin()
    {
	  $usertype = $this->ci->config->item('exhibitor_user_type');
		
		$validuser = FALSE;
		 $this->ci->frnt_auth->login($usertype);
    }
    
    //..........................................................................................................

	/*
	 * @description: This function is used to varificatrion of sponsor email
	 * @return     : void
	 *
	 */
	function emailverification()
	{

		 $userId = decode($this->ci->uri->segment(3));
		
		 //get user details on the basis of user id
		 $whereArray=array('id'=>$userId);
		 $userdetails = $this->ci->common_model->getDataFromTabel('user','event_id,email_verify',$whereArray);
  
		if($userdetails)
		{
			//to check account already verified
			if($userdetails[0]->email_verify==1)
			{
				$msg='Your email address already verified.';
				$msgArray = array('msg'=>$msg,'is_success'=>'true');
				set_global_messages($msgArray);
				redirect(base_url().'frontend/index/'.encode($userdetails[0]->event_id));
			}
			//to update statu for verified email user
			$data['email_verify']='1'; 
			$this->ci->common_model->updateDataFromTabel('user',$data,array('id'=>$userId));
			
			
			//set message  
			$msg='Email address has been verified successfully.';
			$msgArray = array('msg'=>$msg,'is_success'=>'true');
			set_global_messages($msgArray);
			redirect(base_url().'frontend/index/'.encode($userdetails[0]->event_id));
		}
		   redirect(base_url('frontend/index/'));
	}

     //..........................................................................................................

	 
	/*
     * @description: This function is used to logout user form website 
     * @return: void
     *  
     */
     
    function logout()
    {
		
		$this->ci->session->unset_userdata('exhibitorid');
		$this->ci->session->unset_userdata('username');
		$this->ci->session->sess_destroy();
	   // $this->session->set_flashdata('globalmsg', 'successfully'); 
	
	}
	

  
}


/* End of file messages.php */
/* Location: ./system/application/controllers/messages.php */
?>
