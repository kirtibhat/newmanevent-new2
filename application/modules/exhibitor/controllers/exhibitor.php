<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * @description: This controller is used to manage login, registration
 * @create date: 21-Feb-2013
 * @auther: Rajendra aptidar
 * @email: rajendrapatidar@cdnsol.com
 * 
 */ 


class Exhibitor extends MX_Controller{

	function __construct(){
		parent::__construct();
		
		$this->load->library(array('default_template','head'));
		$this->load->library(array('frnt_auth'));
		$this->load->library('factory',$this);
		$this->load->model(array('exhibitor_model'));
		$this->form_validation->set_error_delimiters('<div class="server_msg_show">', '</div>');
		$this->load->language(array('frontend_sponsor','common'));
		$this->load->helper('frontend');
	 
	}

    /*
     * @description: This function is used for Registration for event 
     * @return: void
     *  
    */
    
    function index()
    {
		 $this->factory->index();
	}
     
    //..........................................................................................................

	/* @description  : This function is used to get exhibitor package booths
    * @return        : booths for exhibitor
    * @param         :  
    */
    
    function exhibitorbooths()
    { 
	$this->factory->exhibitorbooths();	
    }
    //..........................................................................................................

	/* @description  : This function is used to save exhibitor booths
    * @return        : NULL
    * @param         : boothId
    */
    
    function saveexhibitorbooth()
    {
	$this->factory->saveexhibitorbooth();	
		
	}
	//..........................................................................................................

	/* @description  : This function is used to load exhibitor home
    * @return        : 
    * @param         : sponsorId 
    */
    
    function exhibitorhome()
    {
	$this->factory->exhibitorhome();	
	 
	}
	//..........................................................................................................

	/* @description  : This function is used to save conatct person detail
     * @return       : void
     */
    
    function savecontactperson()
    {
	$this->factory->savecontactperson();	
        
    }
     //..........................................................................................................

	/*
	*   @description: This function is used to  exhibitor login
    *   @return:message
   */
    function exhibitorLogin()
    {
	 $this->factory->exhibitorLogin();
	   
    }
    //..........................................................................................................

	/*
	 * @description: This function is used to varificatrion of sponsor email
	 * @return     : void
	 *
	 */
	function emailverification()
	{
	$this->factory->emailverification();
	}
	//..........................................................................................................
	/*
     * @description: This function is used to logout user form website 
     * @return: void
     *  
     */
     
    function logout()
    {
	$this->factory->logout();	
	}
	

  
}


/* End of file messages.php */
/* Location: ./system/application/controllers/messages.php */
?>
