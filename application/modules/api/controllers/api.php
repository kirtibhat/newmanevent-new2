<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * @description: This controller is used to manage login, registration
 * @create date: 21-Nov-2013
 * @auther: Lokendra Meena
 * @email: lokendrameena@cdnsol.com
 * 
 */ 

class API extends MX_Controller{

	function __construct(){
		parent::__construct();
		
		//echo "123";
		//exit; 
		$this->load->library('auth',$this);
		$this->load->model('home_model');
		$this->load->language(array('home'));
		
	}

	//----------------------------------------------------------------------------
	
    /*
     * @access: public
     * @default load 
     * @description: This function is used to user login eheck user exist
     * @load: login view
     * @return: void
     *  
     */
    
    public function index(){
    	//echo "2"; exit; 
		$this->auth->index();
	} 
    //------------------------------------------------------------------------
    /*
     * @access: public
     * @description: This function is used to open login view and user id & password check
     * @load: login view
     * @return: void
     *  
     */
     
	public function login(){ 
		//call login method
		$this->auth->userlogin();
	}
	
	
     
	public function saveSocialMedia(){ 
		//call login method
		$this->auth->saveSocialMedia();
	}
	public function getSocialMedia(){ 
		//call login method
		$this->auth->getSocialMedia();
	}
	public function deleteSocialMedia(){ 
		//call login method
		$this->auth->deleteSocialMedia();
	}
	public function deleteVenueSocialMedia(){ 
		$this->auth->deleteVenueSocialMedia();
	}
	
	
	public function getUserDetail(){ 
		$this->auth->getUserDetail();
	}
	
	public function userBasicInformationSave(){ 
		$this->auth->userBasicInformationSave();
	}
	
	public function userProfileSave(){ 
		$this->auth->userProfileSave();
	}
	
	public function userSocialMediaSave(){ 
		$this->auth->userSocialMediaSave();
	}
	
	public function userNotificationSave(){ 
		$this->auth->userNotificationSave();
	}
	
	public function userPasswordSave(){ 
		$this->auth->userPasswordSave();
	}
	
	public function getAllMembers(){ 
		$this->auth->getAllMembers();
	}
	
	public function updateUser(){ 
		$this->auth->updateUser();
	}
	
	
	//----------------------------------------------------------------------------
	
	/*
	 * @access: public
     * @description: This function is used to manage register 
     * @load: register view
     * @return: void
     *  
     */
	
	public function register(){
		//call register method
		$this->auth->userregister();
	}
	
	//----------------------------------------------------------------------------
	
	/*
	 * @access: public
     * @description: This function is used to activation user account
     * @return: void
     *  
     */
	
	function accountactivation($encryptCode='')
	{
		//call for account activation method
		$this->auth->accountactivation($encryptCode);
	}
	
	//----------------------------------------------------------------------------
	
	/*
     * @description: This function is used to activation user account
     * @return: void
     *  
     */
	
	function forgotpassword()
	{
		//call for forgot password method
		$this->auth->forgotpassword();
	}
  
  function reset_password()
	{
    $this->auth->reset_password();
	}
	//----------------------------------------------------------------------------
	

  /*
  * @access: public
  * @description: This function is used for all type (Casual, solo , free etc.) of registration
  * @load: 
  * @return: true (if Data is successfully inserted in DB ) / False ( Otherwise) 
  */
  function registration()
  {
    $this->auth->registration();
  }
  
  /*
  * @access: public
  * @description: This function is used for free type of registration
  * @load: 
  * @return: true (if Data is successfully inserted in DB ) / False ( Otherwise) 
  */
  function freeRegistration()
  {
    $this->auth->freeRegistration();
  }
  
  /*
  * @access: public
  * @description: This function is used for free type of Password saved
  * @load: 
  * @return: true (if Data is successfully inserted in DB ) / False ( Otherwise) 
  */
  function freePassword()
  {
    $this->auth->freePassword();
  }
  
  /*
  * @access: public
  * @description: This function is used for free type of term saved
  * @load: 
  * @return: true (if Data is successfully inserted in DB ) / False ( Otherwise) 
  */
  function freeTerms()
  {
    $this->auth->freeTerms();
  }
   /*
  * @access: public
  * @description: This function is used for free type Congratulations
  * @load: 
  * @return: true  
  */
  function freeCongratulations()
  {
    $this->auth->freeCongratulations();
  }
  
   /*
  * @access: public
  * @description: This function is used for check Email Exist
  * @load: 
  * @return: true  
  */
  function checkEmailExist()
  {
    $this->auth->checkEmailExist();
  }
  
  
  /*
  * @access: public
  * @description: This function is used for save password
  * @load: 
  * @return: true (if Data is successfully inserted in DB ) / False ( Otherwise) 
  */
  function save_password()
  {
    $this->auth->save_password();
  }
  
  /*
  * @access: public
  * @description: This function is used for check our terms
  * @load: 
  * @return: true  / False ( Otherwise) 
  */
  function our_terms()
  {
    $this->auth->our_terms();
  }
  
  /*
  * @access: public
  * @description: This function is used to verify email after registration
  *  
  */
  function verify_email()
  {
    $this->auth->verify_email();
  }
	
	//----------------------------------------------------------------------------
	 
	/*
	 * @access: public
	 * @description: This function is used to check email availability
	 * @retrun void
	 * 
	 */ 
	
	function checkuseremail(){
		$user_data = $this->auth->checkUserEmail();
		if(!empty($user_data))
			echo json_encode(array('msg'=>'This email already exists.','is_success' => 'false', 'user_data' => $user_data));
		else
			echo json_encode(array('msg'=>'This email available.','is_success' => 'true'));
	}
	
	
	public function updateUserbyParam() {
        $user_data = $this->auth->updateUserbyParam();
    }
	
	//----------------------------------------------------------------------------
	 
	/*
	 * @access: public
     * @description: This function is used to logout user form website 
     * @return: void
     *  
     */
     
    public function logout()
    {
		//call method for logout	
		$this->auth->logout();
		redirect(base_url());
	 }
   
   
	
  
}

/* End of file home.php */
/* Location: ./system/application/modules/home/controllers/home.php */
