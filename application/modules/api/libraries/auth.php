<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * 
 * This class is used to manage for home controller, login, registration, forgot password
 * trouble login, other page like(contact us, abouts etc.)
 *
 * @package:   		auth
 * @author:    		Krishnapal Singh Dhakad
 * @email:  	   	krishnapaldhakad@cdnsol.com
 * @create date:	09-Nov-2015
 * @since:     		Version 1.0
 * @filesource
 */
class Auth {

    private $ci = NULL;

    function __construct($ci) {
        //create instance object
        $this->ci = $ci;
    }

    //----------------------------------------------------------------------------

    /*
     * @default load 
     * @description: This function is used to user login eheck user exist
     * @load: login view
     * @return: void
     *  
     */

    public function index() {
        //$this->ci->session_check->CheckUserLoginSession();
        //$this->ci->home_template->load('home_template', 'home_index');
    }

    //--------------------------------------------------------------------------------

    function getSocialMedia(){
		$venue_id       = $this->ci->input->post('venue_id');
		$where = array('venue_id'=>$venue_id);
		$social_media = $this->ci->common_model->getDataFromTabel('event_social_media','*',$where);
		$custom_social_media = $this->ci->common_model->getDataFromTabel('event_social_media_custom','*',$where);
		$result['social_media'] =$social_media[0];
		$result['custom_social_media'] = $custom_social_media;
		echo json_encode($result);
		die;
	}
	
	function deleteSocialMedia(){
		$social_id       = $this->ci->input->post('customSocialId');
		$where = array('id'=>$social_id);
		$this->ci->common_model->deleteRowFromTabel('event_social_media_custom',$where);
	}
	
	function deleteVenueSocialMedia(){
		$venue_id       = $this->ci->input->post('venue_id');
		$where = array('venue_id'=>$venue_id);
		$this->ci->common_model->deleteRowFromTabel('event_social_media',$where);
		$this->ci->common_model->deleteRowFromTabel('event_social_media_custom',$where);
	}
    
    function saveSocialMedia(){
		
		$getData['venue_id']       = $this->ci->input->post('venue_id');
		$getData['linkedIn']          = $this->ci->input->post('LinkedIn');
		$getData['facebook']          = $this->ci->input->post('Facebook');
		$getData['instagram']          = $this->ci->input->post('Instagram');
		$getData['twitter']          = $this->ci->input->post('Twitter');
		$getData['youtube']          = $this->ci->input->post('Youtube');
		$getData['pintrest']          = $this->ci->input->post('Pintrest');
		// check contact person details by event id
		$where = array('venue_id'=>$getData['venue_id']);
		$result = $this->ci->common_model->getDataFromTabel('event_social_media','id',$where);
			
		//update here
		if($result){      
		  // get event contact persion id
		  $customContactId = $result[0]->id;      
		  $this->ci->common_model->updateDataFromTabel('event_social_media',$getData,$where);
		  $msg = 'social media updated.';
		}else{
		  //insert here
		  $customContactId = $this->ci->common_model->addDataIntoTabel('event_social_media', $getData);
		  $msg = 'social media saved.';
		}
			
			
				$customSocialId          = $this->ci->input->post('customSocialId');
			  $data['venue_id']       = $this->ci->input->post('venue_id');
			  $customSocialName     = $this->ci->input->post('customSocialName');
			  $customSocialValue    = $this->ci->input->post('customSocialValue');
			  $flg = 0;
			  if(!empty($customSocialId )){
				  foreach($customSocialId as $customS_Id){
					$where = array('id'=>$customS_Id);
					$result = $this->ci->common_model->getDataFromTabel('event_social_media_custom','id',$where);
					if(!empty($result)){
						$where = array('id'=>$customS_Id);
						$data['media_name'] = $customSocialName[$flg];
						$data['media_value'] = $customSocialValue[$flg];;
						$this->ci->common_model->updateDataFromTabel('event_social_media_custom',$data,$where);
					} else{
						$data['media_name'] = $customSocialName[$flg];
						$data['media_value'] = $customSocialValue[$flg];;
						$insertId = $this->ci->common_model->addDataIntoTabel('event_social_media_custom', $data);
					}
					$flg++;
				  }
			  }
			
		echo json_encode(array('is_success' => 'true','msg'=>$msg));
  }
  	
	public function getUserDetail() {
		
        $user_id = $this->ci->input->post('user_id');
        $forgot_pass_random_string = $this->ci->input->post('forgot_pass_random_string');
        if(!empty($user_id)){
			
			$where = array('id'=>$user_id);
		}else if(!empty($forgot_pass_random_string)){
			
			$where = array('forgot_pass_random_string'=>$forgot_pass_random_string);
		}else{
			$where = '';
		}
		$userdata = $this->ci->common_model->getDataFromTabel('user','*',$where);
        $where2 = array('user_id'=>$user_id);
        $userdata2 = $this->ci->common_model->getDataFromTabel('user_details','*',$where2);
        if (!empty($userdata[0])) {
			if (!empty($userdata2[0])) {
				echo json_encode(array('msg'=>'success','response'=>$userdata[0],'response2'=>$userdata2[0]));
			} else{ 
				echo json_encode(array('msg'=>'success','response'=>$userdata[0]));
			}
		}else {
			echo json_encode(array('msg'=>'error'));
		}
    }
    
	public function userBasicInformationSave() {

        $id = $this->ci->input->post('id');
        $data['company_name'] = $this->ci->input->post('company_name');
		$userdata = $this->ci->common_model->updateDataFromTabel('user',$data,'id',$id);
        if (!empty($userdata)) {
			echo json_encode(array('msg'=>'success','response'=>$userdata));
		}else {
			echo json_encode(array('msg'=>'error'));
		}
    }
    
    public function userPasswordSave() {
		$userdata = true;
        $id = $this->ci->input->post('id');
        $data2['user_id'] = $id;
        $new_password = $this->ci->input->post('new_password');
        $show_password = $this->ci->input->post('show_password');
        
        $data2['show_password'] = (!empty($show_password)) ? $show_password : '';
        if(!empty($new_password)){
			$data['password'] = md5($new_password);
			$userdata = $this->ci->common_model->updateDataFromTabel('user',$data,'id',$id);
		}
        $where = array('user_id'=>$id);
		$result = $this->ci->common_model->getDataFromTabel('user_details','user_details_id',$where);
        if(!empty($result)){
			$userdata = $this->ci->common_model->updateDataFromTabel('user_details',$data2,'user_id',$id);
        } else{
			$userdata = $this->ci->common_model->addDataIntoTabel('user_details', $data2);
		}
        if (!empty($userdata)) {
           echo json_encode(array('msg'=>'success','response'=>$userdata));
		}else {
			echo json_encode(array('msg'=>'error'));
		}
    }
    
    public function userProfileSave() { 
		$id = $this->ci->input->post('id');
        $data2['user_id'] = $id;
        $newman_nickname = $this->ci->input->post('newman_nickname');
        $remove_user_logo = $this->ci->input->post('remove_user_logo');
        $data['newman_nickname'] = (!empty($newman_nickname)) ? $newman_nickname : '';
        $base_url = $this->ci->input->post('base_url');
        $userLogo = $this->ci->input->post('userLogo');
        
        $where = array('user_id'=>$id);
		$result = $this->ci->common_model->getDataFromTabel('user_details','*',$where);
        
        if(!empty($userLogo)){
			copy($base_url.$userLogo,'./media/user_logo/'.$userLogo);
			$data2['userLogo'] = $userLogo;
			$data['userImageStatus'] = '1';
			$pathinfo = array_filter( explode('/', BASEPATH) );
			array_pop($pathinfo);
			$base_path =  implode('/',$pathinfo);
			if(!empty($result) && !empty($result[0]->userLogo)){
				@unlink('/'.$base_path.'/media/user_logo/'.$result[0]->userLogo);
			}
        } else{
			if(!empty($remove_user_logo)){
				$data2['userLogo'] = '';
				$pathinfo = array_filter( explode('/', BASEPATH) );
				array_pop($pathinfo);
				$base_path =  implode('/',$pathinfo);
				if(!empty($result) && !empty($result[0]->userLogo)){
					@unlink('/'.$base_path.'/media/user_logo/'.$result[0]->userLogo);
				}
			}
		}
		$userdata = $this->ci->common_model->updateDataFromTabel('user',$data,'id',$id);
		
		
        if(!empty($result)){
			$userdata = $this->ci->common_model->updateDataFromTabel('user_details',$data2,'user_id',$id);
        } else{
			$userdata = $this->ci->common_model->addDataIntoTabel('user_details', $data2);
		}
        
        if (!empty($userdata)) {
			echo json_encode(array('msg'=>'success','response'=>$userdata));
		}else {
			echo json_encode(array('msg'=>'error'));
		}
    }    
    
    function userSocialMediaSave(){
		$getData['user_id']       = $this->ci->input->post('id');
		$getData['linkedIn']          = $this->ci->input->post('LinkedIn');
		$getData['facebook']          = $this->ci->input->post('Facebook');
		$getData['instagram']          = $this->ci->input->post('Instagram');
		$getData['twitter']          = $this->ci->input->post('Twitter');
		$getData['youtube']          = $this->ci->input->post('Youtube');
		$getData['pintrest']          = $this->ci->input->post('Pintrest');
		$getData['customSocial']          = $this->ci->input->post('customSocial');
		// check contact person details by event id
		$where = array('user_id'=>$getData['user_id']);
		$result = $this->ci->common_model->getDataFromTabel('user_details','user_details_id',$where);
			
		//update here
		if(!empty($result)){      
		  $this->ci->common_model->updateDataFromTabel('user_details',$getData,$where);
		  $msg = 'social media updated.';
		}else{
		  //insert here
		  $customContactId = $this->ci->common_model->addDataIntoTabel('user_details', $getData);
		  $msg = 'social media saved.';
		}
		
		echo json_encode(array('msg'=>'success'));
  }
  
    public function userNotificationSave() {
		
        $id = $this->ci->input->post('id');
        $alert_emailid = $this->ci->input->post('alert_emailid');
        $alert_mobile_no = $this->ci->input->post('alert_mobile_no');
        $data['alert_emailid'] = (!empty($alert_emailid)) ? $alert_emailid : '';
        $data['alert_mobile_no'] = (!empty($alert_mobile_no)) ? $alert_mobile_no : '';
        
		$userdata = $this->ci->common_model->updateDataFromTabel('user_details',$data,'user_id',$id);
        
        if (!empty($userdata)) {
			echo json_encode(array('msg'=>'success','response'=>$userdata));
		}else {
			echo json_encode(array('msg'=>'error'));
		}
    } 
    
    public function getAllMembers() {

        $user_id = $this->ci->input->post('user_id');
		$userdata = $this->ci->home_model->getAllMembers($user_id);
         if (!empty($userdata[0])) {
			echo json_encode(array('msg'=>'success','response'=>$userdata));
		}else {
			echo json_encode(array('msg'=>'error'));
		}
    }
    
    /*
     * @access: public
     * @descrition: This function is used  for user login
     * @return void
     */


    public function userlogin() {

        $email = $this->ci->input->post('email');
		$password = $this->ci->input->post('password');
		$passwordMd5 = md5($password);
        $where = array('email'=>$email,'password'=>$passwordMd5);
        $userdata = $this->ci->common_model->getDataFromTabel('user','*',$where);
        $return_data = '';
        if (!empty($userdata[0])) {
			$where2 = array('user_id'=>$userdata[0]->id);
			$userdata2 = $this->ci->common_model->getDataFromTabel('user_details','*',$where2);
			if (!empty($userdata2[0])) {
				$return_data =  json_encode(array('msg'=>'success','response'=>$userdata[0],'response2'=>$userdata2[0]));
                echo $return_data; 
                exit;  		
			} else{
				 $return_data = json_encode(array('msg'=>'success','response'=>$userdata[0]));
                 echo $return_data; 
                 exit;  		
			}
		}else {
			 $return_data = json_encode(array('msg'=>'error'));
             echo $return_data; 
             exit;  		
		}
        //print_r($return_data); exit; 
       
    }

    /*
     * @access: public
     * @descrition: This function is used  for user login
     * @return void
     */


    public function userregister() {
			
            if ($this->checkUserEmailExist()) {
                $msg = lang('home_error_email_already');
                $errorArray = array('msg' => $msg, 'is_success' => 'false');
                echo json_encode($errorArray);
                return true;
            }
			
			$company_name = $this->ci->input->post('company_name');
            $password = $this->ci->input->post('password');
            
            $gplus_id = $this->ci->input->post('gplus_id');
            $gplus_image = $this->ci->input->post('gplus_image');
            if(!empty($gplus_image)) {
                $fb_image = $gplus_image; 
            }else {
                $fb_image = $this->ci->input->post('fb_image');
            }
            $fb_id = $this->ci->input->post('fb_id');

            $firstname = $this->ci->input->post('firstname');
            $data['firstname'] = $firstname;
            $data['lastname'] = $this->ci->input->post('lastname');
            $data['username'] = $this->ci->input->post('email');
            $email = $this->ci->input->post('email');
            $base_url = $this->ci->input->post('base_url');
            $data['email'] = $email;
            $data['company_name'] = (!empty($company_name))?$company_name:'';
            $data['fb_id'] = (!empty($fb_id))?$fb_id:'';
            $data['gplus_id'] = (!empty($gplus_id))?$gplus_id:'';
            $data['password'] = (!empty($password))?md5($password):'';
            $data['user_type_id'] = '1'; 
            $data['email_verify'] = '0'; 
            $data['status'] = '1'; 
            $data['operand_number'] = getMasterNumber('operand');  
            $data['master_event_id'] = 'AA01';  
            
            $data['isTermsNConditionTrue'] = $this->ci->input->post('isTermsNConditionTrue');
            
            $userdata = $this->ci->home_model->registerinsert($data);

            if ($userdata > 0) {
				
				if(!empty($fb_image)){
					$img_name = time().'.jpg';
					
					$pathinfo = array_filter( explode('/', BASEPATH) );
					array_pop($pathinfo);
					$base_path =  implode('/',$pathinfo);
					copy($fb_image,'/'.$base_path.'/media/user_logo/'.$img_name);
				}else{
					$img_name = '';
				}
				$this->ci->common_model->addDataIntoTabel('user_details', array('user_id'=>$userdata,'userLogo'=>$img_name));
                //send varification email to user email
                //$this->_registerVerification($data);

				$random_string = encode($userdata);

				//send email to user via mandrill
				/*
                $this->ci->load->library('mandrill/email_template');
				
                $template_name = $this->ci->config->item('registration_template_name');
				
                $template_message = array(
					'to' => array(
						array(
							'email' => $email,
							'name' => 'Admin',
							'type' => 'to'
						)
					),
					'merge' => true,
					'global_merge_vars' => array(
						array(
							'name' => 'activation_link',
							'content' => $base_url . 'home/verify_email/' . $random_string
						),
						array(
							'name' => 'contact_link',
							'content' => $base_url . '?#contact_email'
						),
						array(
							'name' => 'contact_first_name',
							'content' => $firstname
						)
					)
				);
                */
                if(empty($fb_id) && empty($gplus_id)) {
                                  
                    /* Sendgrid */
                    $this->ci->load->helper('sendgrid');
                    $apiKey = ADMINNEWMAN_APIKEY;
                    $addTeam_templateId = ADDTeam_templateId; //
                     
                    $facebook_url    = $this->ci->config->item('facebook_url'); #$config['facebook_url']
                    $twitter_url     = $this->ci->config->item('twitter_url'); #$config['facebook_url']
                    $g_plus_url      = $this->ci->config->item('g_plus_url'); #$config['facebook_url']
                    $newmanvenue_url = $this->ci->config->item('newmanvenue_url'); #$config['facebook_url']
                    
                    $paramArray = array(
                    'sub' => array(':firstname' => array($firstname),':activation_link'=>array($base_url.'home/verify_email/'.$random_string),':contactLink'=>array($base_url.'?#contact_email'),':newmanvenue_url'=>array($newmanvenue_url),':twitter_url'=>array($twitter_url),':g_plus_url'=>array($g_plus_url),':facebook_url'=>array($facebook_url)),
                    'filters' => array('templates' => array('settings' => array('enable' => 1, 'template_id' => $addTeam_templateId)))
                    );
                    $tempSubject = '.';
                    //$templateBody = 'Thanks for joining Newman!!';
                    $templateBody = '.';
                    $toId = $email;//$admin_email;
                    sendgrid_mail_template($addTeam_templateId,$tempSubject,$templateBody,$toId,$paramArray,$apiKey,adminFromEmail);
                    $return = lang('team_member_saved');
                     
                //Stop Mendgrill
                //$this->ci->email_template->send_template_email($template_name, $template_message);
                
                }
                //send email to user via mandrill
				




                $msg = lang('home_error_register_success');
                $returnArray = array('msg' => $msg, 'is_success' => 'true','user_id'=>$userdata);
            } else {
                $msg = lang('home_error_register_fail');
                $returnArray = array('msg' => $msg, 'is_success' => 'false');
            }
            echo json_encode($returnArray);
        
    }



    public function updateUser() {
		
			$email      = $this->ci->input->post('email');
			$user_id    = $this->ci->input->post('user_id');
            $fb_id      = $this->ci->input->post('fb_id');
            $password   = $this->ci->input->post('password');
            $logo_image = $this->ci->input->post('logo_image');
            
            if(!empty($logo_image)){
					$img_name = time().'.jpg';
					
					$pathinfo = array_filter( explode('/', BASEPATH) );
					array_pop($pathinfo);
					$base_path =  implode('/',$pathinfo);
					copy($logo_image,'/'.$base_path.'/media/user_logo/'.$img_name);
					//$this->ci->common_model->addDataIntoTabel('user_details', array('user_id'=>$user_id,'userLogo'=>$img_name));
					
					$this->ci->common_model->updateDataFromTabel('user_details',array('userLogo'=>$img_name),'user_id',$user_id);
			}
			//$data['isTermsNConditionTrue'] = $this->ci->input->post('isTermsNConditionTrue');
			$email_verify = $this->ci->input->post('email_verify');
            if(!empty($email_verify)) {
                $data['email_verify'] = '1'; 
            }
            $where['email'] = (!empty($email))?$email:'';
            $data['fb_id'] = (!empty($fb_id))?$fb_id:'';
            //$data['email_verify'] = '1'; 
            $data['status'] = '1'; 
            
            if(!empty($password)) {
                $data['password'] = $password;
            }
            
            $userdata = $this->ci->home_model->updateUserData($where,$data);

            if ($userdata > 0) {

                //send varification email to user email
                //$this->_registerVerification($data);

                $msg = lang('home_error_register_success');
                $returnArray = array('msg' => $msg, 'is_success' => 'true','user_id'=>$userdata);
            } else {
                $msg = lang('home_error_register_fail');
                $returnArray = array('msg' => $msg, 'is_success' => 'false');
            }
            echo json_encode($returnArray);
        
    }
    
    
    public function updateUserbyParam() {
            $user_id = $this->ci->input->post('user_id');
            $this->ci->db->where('id',$user_id);
            $data['isTermsNConditionTrue'] = 1;
            $this->ci->db->update('nm_user', $data); 
    }
    
    
    



    /*
     * @access: private
     * @description: This function is used send account activation email
     * @return void
     */

    private function _registerVerification($emailData) {

        //set email data
        $userEmail = $emailData['email'];
        $emailData['activationUrl'] = base_url('home/accountactivation') . '/' . PHP_EOL . encode($userEmail);
        $subject = 'newman account activation.';
        $this->ci->load->library('email');
        $this->ci->email->from($this->ci->config->item('from_email'));
        $this->ci->email->to($userEmail);
        $this->ci->email->subject($subject);
        $message = $this->ci->load->view('email_view/register_email', $emailData, true);
        $this->ci->email->message($message);
        $this->ci->email->send();
    }

    //--------------------------------------------------------------------------------
    // this is detais for the day registant 
    /*
     * @access: public
     * @description: This function is used to user account activation 
     * @return void
     */

    public function accountactivation($encryptCode) {

        //condition for user enter link with code 
        if (empty($encryptCode)) {
            $returnArray = array('msg' => lang('home_error_account_active_error'), 'is_success' => 'false');
            set_global_messages($returnArray);
            redirect(base_url());
        }

        $userEmail = decode($encryptCode); // decode encrypted code into emailid
        //get user data
        $where = array('email' => $userEmail, 'user_type_id' => '1');
        $userdata = $this->ci->common_model->getDataFromTabel('user', 'id,email_verify,status,email', $where);

        //check email id exist
        if (!empty($userdata)) {

            if (!empty($userdata)) {
                $userdata = $userdata[0];
            }
            $updateWhere = array('email' => $userdata->email, 'user_type_id' => '1');
            $updateData = array('email_verify' => '1', 'status' => '1');

            if ($userdata->email_verify == "0") {
                //update user data
                $this->ci->common_model->updateDataFromTabel('user', $updateData, $updateWhere);
                //set error message
                $returnArray = array('msg' => lang('home_error_account_active'), 'is_success' => 'true');
                set_global_messages($returnArray);
                redirect(base_url());
            } else {
                //set error message
                $returnArray = array('msg' => lang('home_error_account_active_already'), 'is_success' => 'false');
                set_global_messages($returnArray);
                redirect(base_url());
            }
        } else {
            //set error message
            $returnArray = array('msg' => lang('home_error_account_active_error'), 'is_success' => 'false');
            set_global_messages($returnArray);
            redirect(base_url());
        }
    }

    //---------------------------------------------------------------------------

    /*
     * @access: public
     * @description: This methos is used to send encrypted password to user email id 
     * @return: json
     */

    public function forgotpassword() {

        //set validation
        $this->ci->form_validation->set_rules('forgotEmail', 'email address', 'trim|required|valid_email');

        //check validation is true
        if ($this->ci->form_validation->run($this->ci)) {
            //set email data
            $userEmail = $this->ci->input->post('forgotEmail');
			$base_url = $this->ci->input->post('base_url');
            //get user data
            $where = array('email' => $userEmail);
            $userdata = $this->ci->common_model->getDataFromTabel('user', 'id,firstname,email,password', $where);

            if (!empty($userdata)) {

                $userdata = $userdata[0];

                //prepare email send data
                $userName = ucfirst($userdata->firstname);
                $emailData['firstname'] = $userName;
                $random_string = base64_encode(randomnumber('4'));
                $emailData['random_string'] = $random_string;


                //Link send through email 

                $updateData['forgot_pass_random_string'] = $random_string;

                $updateCondi = array('email' => $userEmail);
                $this->ci->common_model->updateDataFromTabel('user', $updateData, $updateCondi);

                //insert old password in password summery table
                $insertData['user_id'] = $userdata->id;
                $insertData['old_password'] = $userdata->password;
                $this->ci->common_model->addDataIntoTabel('password_chagned_summery', $insertData);


                //send email to user email via mandrill
                /*
                $this->ci->load->library('mandrill/email_template');
                $template_name = $this->ci->config->item('forgotpassword_template_name');
                $template_message = array(
                    'to' => array(
                        array(
                            'email' => $userEmail,
                            'name' => $userName,
                            'type' => 'to'
                        )
                    ),
                    'merge' => true,
                    'global_merge_vars' => array(
                        array(
                            'name' => 'first_name',
                            'content' => $userName
                        ),
                        array(
                            'name' => 'email',
                            'content' => $userEmail
                        ),
                        array(
                            'name' => 'contact_link',
                            'content' => $base_url . '?#contact_email'
                        ),
                        array(
                            'name' => 'reset_link',
                            'content' => $base_url . 'home/reset_password/' . $random_string
                        )
                    )
                );
                $this->ci->email_template->send_template_email($template_name, $template_message);
                //send email to user email via mandrill
                */
                
                  /* Sendgrid */
                    $this->ci->load->helper('sendgrid');
                    $addTeam_templateId = ForGot_Pass;
                    $newmanvenue_url = $this->ci->config->item('newmanvenue_url'); #$config['facebook_url']
                    $apiKey = ADMINNEWMAN_APIKEY;
                    $paramArray = array(
                    'sub' => array(':firstname' => array($userName),':reset_link'=>array($base_url.'home/reset_password/'.$random_string),':contactLink'=>array($base_url.'?#contact_email'),'newman_url'=>array($newmanvenue_url)),
                    'filters' => array('templates' => array('settings' => array('enable' => 1, 'template_id' => $addTeam_templateId)))
                    );
                    $tempSubject = '?';
                    $templateBody = '.';
                    $toId = $userEmail;//$admin_email;
                    sendgrid_mail_template($addTeam_templateId,$tempSubject,$templateBody,$toId,$paramArray,$apiKey,adminFromEmail);
                

                $secure_email_format = changeEmailIdFormat($userEmail); // this function change the email format like (n****@g***.com)
                $userEmailId = sprintf($this->ci->lang->line('forgot_email_sent_popup_success1'), $secure_email_format);
                //set success message
                $msg = lang('home_error_newpassword_send');
                $returnArray = array('msg' => $msg, 'is_success' => 'true', 'useremailid' => $userEmailId);
                echo json_encode($returnArray);
            } else {
                //set error message
                $msg = lang('home_error_newpassword_fail');
                $returnArray = array('msg' => $msg, 'is_success' => 'false');
                echo json_encode($returnArray);
            }
        } else {

            //call for error message
            echo $this->_errormsg();
        }
    }

    //---------------------------------------------------------------------------

    /*
     * @access: public
     * @description: This methos is used to send trouble logging request to admin
     * @return: json
     */

    public function troublelogging() {

        //set validation
        $this->ci->form_validation->set_rules('troubleFirstName', 'first name', 'trim|required');
        $this->ci->form_validation->set_rules('troubleLastName', 'surname name', 'trim|required');
        $this->ci->form_validation->set_rules('troubleLoginEmail', 'email address', 'trim|required|valid_email');
        $this->ci->form_validation->set_rules('troubleLoginMessage', 'message', 'trim|required');

        //check validation is true
        if ($this->ci->form_validation->run($this->ci)) {

            $userEmail = $this->ci->input->post('troubleLoginEmail');

            //get user data
            $where = array('email' => $userEmail);
            $userdata = $this->ci->common_model->getDataFromTabel('user', 'email', $where);

            if (!empty($userdata)) {

                $userdata = $userdata[0];

                $emailData['adminName'] = 'Admin';
                $emailData['firstname'] = $this->ci->input->post('troubleFirstName');
                $emailData['surnamename'] = $this->ci->input->post('troubleLastName');
                $emailData['emailAddress'] = $userEmail;
                $emailData['message'] = $this->ci->input->post('troubleLoginMessage');

                $subject = 'newmanevents trouble logging in request';
                $this->ci->load->library('email');
                $this->ci->email->from($this->ci->config->item('from_email'));
                $this->ci->email->to($this->ci->config->item('admin_email'));
                $this->ci->email->subject($subject);
                $message = $this->ci->load->view('email_view/trouble_logging_email', $emailData, true);
                $this->ci->email->message($message);
                $this->ci->email->send();

                //set success message
                $msg = lang('home_error_trouble_success');
                $returnArray = array('msg' => $msg, 'is_success' => 'true');
                echo json_encode($returnArray);
            } else {
                //set error message
                $msg = lang('home_error_trouble_fail');
                $returnArray = array('msg' => $msg, 'is_success' => 'false');
                echo json_encode($returnArray);
            }
        } else {

            //call for error message
            echo $this->_errormsg();
        }
    }

    //---------------------------------------------------------------------------

    /*
     * @access: public
     * @description: This methos is used to send contact us request to admin
     * @return: json
     */

    public function contactus() {

        //set validation 
        $this->ci->form_validation->set_rules('contactFirstName', lang('contact_first_name'), 'trim|required');
        $this->ci->form_validation->set_rules('contactLastName', lang('contact_surname'), 'trim|required');
        $this->ci->form_validation->set_rules('contactEmail', lang('contact_email'), 'trim|required|valid_email');
        //$this->ci->form_validation->set_rules('contactPhoneCode', lang('contact_phonecode'), 'trim|required|numeric|min_length[2]|max_length[2]');
        $this->ci->form_validation->set_rules('contactPhone', lang('contact_phoneno'), 'trim|required');
        $this->ci->form_validation->set_rules('contactMessage', lang('contact_message'), 'trim|required');

        //check validation is true
        if ($this->ci->form_validation->run($this->ci)) {
            $emailData['adminName'] = 'Admin';
            $emailData['firstname'] = $this->ci->input->post('contactFirstName');
            $emailData['surnamename'] = $this->ci->input->post('contactLastName');
            
            if($this->ci->input->post('mobile')) {
               $emailData['mobile'] = $this->ci->input->post('mobile');
            }  else {
               $emailData['mobile'] = $this->ci->input->post('contactPhone'); 
            }
            $emailData['contact'] = $emailData['mobile'];
            if($this->ci->input->post('landline')) {
               $emailData['contact'] .= '/'.$this->ci->input->post('landline');
            }  else if ($this->ci->input->post('mobile')!='' && $this->ci->input->post('landline')=='') {
                $emailData['contact'] .= '/'.$this->ci->input->post('contactPhone');
            }

            
            
            $emailData['emailAddress'] = $this->ci->input->post('contactEmail');
            $emailData['message'] = $this->ci->input->post('contactMessage');

            //send email to admin via mandrill
            $this->ci->load->library('mandrill/email_template');
            $template_name = $this->ci->config->item('contactus_user_template_name');
            $template_message = array(
                'to' => array(
                    array(
                        'email' => $emailData['emailAddress'],
                        'name' => ucfirst($emailData['firstname']),
                        'type' => 'to'
                    )
                ),
                'merge' => true,
                'global_merge_vars' => array(
                    array(
                        'name' => 'first_name',
                        'content' => ucfirst($emailData['firstname'])
                    ),
                    array(
                        'name' => 'contactus_message',
                        'content' => $emailData['message']
                    )
                )
            );
            $this->ci->email_template->send_template_email($template_name, $template_message);


            $admin_template_name = $this->ci->config->item('contactus_admin_template_name');
            $admin_template_message = array(
                'to' => array(
                    array(
                        'email' => $this->ci->config->item('admin_email'),
                        'name' => 'Admin',
                        'type' => 'to'
                    )
                ),
                'merge' => true,
                'global_merge_vars' => array(
                    array(
                        'name' => 'first_name',
                        'content' => ucfirst($emailData['firstname'])
                    ),
                    array(
                        'name' => 'last_name',
                        'content' => ucfirst($emailData['surnamename'])
                    ),
                    array(
                        'name' => 'phone',
                        'content' => $emailData['contact']
                    ),
                    array(
                        'name' => 'email',
                        'content' => $emailData['emailAddress']
                    ),
                    array(
                        'name' => 'contactus_message',
                        'content' => $emailData['message']
                    )
                )
            );
            $this->ci->email_template->send_template_email($admin_template_name, $admin_template_message);
            
            //send email to admin via mandrill                     
            //set success message
            $msg = lang('home_contact_us_success');
            $returnArray = array('msg' => $msg, 'is_success' => 'true');
            echo json_encode($returnArray);
        } else {

            //call for error message
            echo $this->_errormsg();
        }
    }

    //---------------------------------------------------------------------------

    /*
     * @access: private
     * @description: This methos is used to show set error for validation
     * @return: json
     */

    private function _errorMsg() {
        $error = $this->ci->form_validation->error_array();
        return json_encode(array('msg' => $error, 'is_success' => 'false'));
    }

    //--------------------------------------------------------------------------------


    /*
     * @access: public
     * @description: This methos is used to show set error for validation
     * @return: json
     */

    public function checkUserEmail() {
        $regisEmail = $this->ci->input->post('email');
        $where = array('email' => $regisEmail);
        $userdata = $this->ci->common_model->getDataFromTabel('user','*', $where);
        if ($userdata) {
            return $userdata;
        } else {
            return false;
        }
    }

    

    public function alpha_numeric($str) {
        if (preg_match('#[0-9]#', $str) && preg_match('#[a-zA-Z]#', $str)) {
            return TRUE;
        }
        return FALSE;
    }

    function reset_password() {
        if ($this->ci->input->post('newpassword')) {
            $this->ci->form_validation->set_rules('newpassword', 'password', 'trim|required|matches[newpassword2]|min_length[6]|max_length[25]');
            $this->ci->form_validation->set_rules('newpassword2', 'confirm password', 'trim|required|min_length[6]|max_length[25]');
            if ($this->ci->form_validation->run($this->ci) && $this->alpha_numeric($this->ci->input->post('newpassword'))) {
                $new_pass = md5($this->ci->input->post('newpassword'));
                $random_string = $this->ci->input->post('random_string');

                // fetch user info from DB
                $where = array('forgot_pass_random_string' => $random_string);
                $fetchuserdata = $this->ci->common_model->getDataFromTabel('user', '*', $where);
                $emailData['firstname'] = $fetchuserdata[0]->firstname;
                $userEmail = $fetchuserdata[0]->email;
                $updateWhere = array('forgot_pass_random_string' => $random_string,);
                $updateData = array('password' => $new_pass, 'forgot_pass_random_string' => '');

                //update new password
                $this->ci->common_model->updateDataFromTabel('user', $updateData, $updateWhere);
            /*
                //send email to user via mandrill
                $this->ci->load->library('mandrill/email_template');
                $template_name = $this->ci->config->item('resetpassword_template_name');
                $template_message = array(
                    'to' => array(
                        array(
                            'email' => $userEmail,
                            'name' => $emailData['firstname'],
                            'type' => 'to'
                        )
                    ),
                    'merge' => true,
                    'global_merge_vars' => array(
                        array(
                            'name' => 'contact_link',
                            'content' => base_url() . '?#contact_email'
                        )
                    )
                );
                $this->ci->email_template->send_template_email($template_name, $template_message);
                //send email to user via mandrill  
                    
            */        
            $this->ci->load->helper('sendgrid');
            $reset_templateId = RESET_templateId;
            $apiKey = VENUES_APIKEY;
            $paramArray = array(
            'filters' => array('templates' => array('settings' => array('enable' => 1, 'template_id' => $reset_templateId)))
            );
            $tempSubject = '.';
            $templateBody = '.';
            $toId = $userEmail;//$admin_email;
            sendgrid_mail_template($reset_templateId,$tempSubject,$templateBody,$toId,$paramArray,$apiKey,adminFromEmail);


                echo json_encode(array('msg' => "Success", 'is_success' => 'true'));
            } else {
                echo json_encode(array('msg' => $this->ci->lang->line('password_reset_server_validation'), 'is_success' => 'false'));
            }
        } else {
            $view_data['msg'] = "Oops! Something went wrong. Please check the link.";
            $random_string = $this->ci->uri->segment(3);
            if ($random_string != "") {
                $where = array('forgot_pass_random_string' => $random_string);
                $userdata = $this->ci->common_model->getDataFromTabel('user', 'email', $where);
                if (!empty($userdata)) {
                    $view_data["secureEmailId"] = changeEmailIdFormat($userdata[0]->email);
                    $view_data["random_sting"] = $random_string;
                    $this->ci->home_template->load('home_template', 'reset_password', $view_data);
                } else {
                    $this->ci->home_template->load('home_template', 'error_reset_paasword', $view_data);
                }
            } else {
                $this->ci->home_template->load('home_template', 'error_reset_paasword', $view_data);
            }
        }
    }

    //--------------------------------------------------------------------------------

    /*
     * @access: public
     * @description: This methos is used to destory login user data
     * 
     */

    public function logout() {
        $this->ci->session->unset_userdata('userid');
        $this->ci->session->unset_userdata('username');
        $this->ci->session->sess_destroy();
    }

    public function registration() {

        if ($this->ci->input->post('registration_type')) {
            $this->ci->form_validation->set_rules('registerUsername', 'Name', 'trim|required');
            $this->ci->form_validation->set_rules('registerUserSurname', 'Surname', 'trim|required');
            $this->ci->form_validation->set_rules('email', 'Email', 'trim|required|valid_email'); //|is_unique[user.email]
            $this->ci->form_validation->set_rules('confirm_email', 'Confirm email', 'trim|required|matches[email]');
            //$this->ci->form_validation->set_message('is_unique', lang('is_unique_email_custom_message'));
            if ($this->ci->form_validation->run($this->ci)) {
                $registration_type = decode($this->ci->input->post('registration_type'));
                $data = array(
                    'firstname' => $this->ci->input->post('registerUsername'),
                    'lastname' => $this->ci->input->post('registerUserSurname'),
                    'username' => $this->ci->input->post('email'),
                    'email' => $this->ci->input->post('email'),
                    'company_name' => $this->ci->input->post('organisation'),
                    'registration_type' => $registration_type
                );


                $User_id = $this->ci->common_model->addDataIntoTabel('user', $data);
                if ($User_id != "") {
                    $popup_content = sprintf($this->ci->lang->line('please_check_your_email_and_click_verify'), $data['email']);
                    $returnArray = array('msg' => 'success', 'is_success' => 'true','user_id' => $User_id, 'content' => $popup_content);
					if($registration_type == 1){
						$this->ci->session->set_userdata('registration_type', $registration_type);
						$this->ci->session->set_userdata('user_id', $User_id);
						$this->ci->session->set_userdata('registration_step', 2);
					}
					if($registration_type != 1){
						// Set email body
						$random_string = encode($User_id);
                        /*
						//send email to user via mandrill
						$this->ci->load->library('mandrill/email_template');
						$template_name = $this->ci->config->item('registration_template_name');
						$template_message = array(
							'to' => array(
								array(
									'email' => $data['email'],
									'name' => 'Admin',
									'type' => 'to'
								)
							),
							'merge' => true,
							'global_merge_vars' => array(
								array(
									'name' => 'activation_link',
									'content' => base_url() . 'home/verify_email/' . $random_string
								),
								array(
									'name' => 'contact_link',
									'content' => base_url() . '?#contact_email'
								)
							)
						);
						$this->ci->email_template->send_template_email($template_name, $template_message);
						*/
                        
                         /* Sendgrid */
                    $this->ci->load->helper('sendgrid');
                    $addTeam_templateId = ADDTeam_templateId;
                    $apiKey = ADMINNEWMAN_APIKEY;
                    $facebook_url    = $this->ci->config->item('facebook_url'); #$config['facebook_url']
                    $twitter_url     = $this->ci->config->item('twitter_url'); #$config['facebook_url']
                    $g_plus_url      = $this->ci->config->item('g_plus_url'); #$config['facebook_url']
                    $newmanvenue_url = $this->ci->config->item('newmanvenue_url'); #$config['facebook_url']
                    
                    
                    $paramArray = array(
                    'sub' => array(':firstname' => array($data['firstname']),':activation_link'=>array(base_url().'home/verify_email/'.$random_string),':contactLink'=>array(base_url().'?#contact_email'),':newmanvenue_url'=>array($newmanvenue_url),':twitter_url'=>array($twitter_url),':g_plus_url'=>array($g_plus_url),':facebook_url'=>array($facebook_url)),
                    'filters' => array('templates' => array('settings' => array('enable' => 1, 'template_id' => $addTeam_templateId)))
                    );
                    $tempSubject = '.';
                    //$templateBody = 'Thanks for joining Newman!!';
                    $templateBody = '.';
                    
                    
                    $toId = $data['email'];//$admin_email;
                    sendgrid_mail_template($addTeam_templateId,$tempSubject,$templateBody,$toId,$paramArray,$apiKey,adminFromEmail);
                    $return = lang('team_member_saved');
                        
                        
                        //send email to user via mandrill
					}

                    echo json_encode($returnArray);
                } else {
                    echo json_encode(array('msg' => lang('record_insert_error'), 'is_success' => 'false'));
                }
            } else {
                echo $this->_errormsg();
            }
        }
    }

// end function registration

	public function freeRegistration() {
		$user_id = $this->ci->session->userdata('user_id');
		$registration_type = $this->ci->session->userdata('registration_type');
		$registration_step = $this->ci->session->userdata('registration_step');
		if(!empty($user_id) && !empty($registration_type) && !empty($registration_step)){
			if ($registration_step=='2'){
				redirect(base_url().'home/freePassword');
			} else if($registration_step=='3'){
				redirect(base_url().'home/freeTerms');
			}
		}
		if ($this->ci->input->post('registration_type')) {
            $this->ci->form_validation->set_rules('registerUsername', 'First Name', 'trim|required');
            $this->ci->form_validation->set_rules('registerUserSurname', 'Last Name', 'trim|required');
            $this->ci->form_validation->set_rules('email', 'Email', 'trim|required|valid_email'); //|is_unique[user.email]
            $this->ci->form_validation->set_rules('confirm_email', 'Confirm email', 'trim|required|matches[email]');
            if ($this->ci->form_validation->run($this->ci)) {
                
                $user_details = $this->ci->common_model->getDataFromTabel('user','id',array('email'=>$this->ci->input->post('email'), 'user_type_id'=>1, 'registration_type'=>1));
				if(!empty($user_details[0]->id)){
					echo json_encode(array('msg' => lang('is_unique_email_custom_message'), 'is_success' => 'false'));
				}else{
					$registration_type = decode($this->ci->input->post('registration_type'));
					$data = array(
						'firstname' => $this->ci->input->post('registerUsername'),
						'lastname' => $this->ci->input->post('registerUserSurname'),
						'username' => $this->ci->input->post('email'),
						'email' => $this->ci->input->post('email'),
						'company_name' => $this->ci->input->post('organisation'),
						'user_type_id' => 1,
						'operand_number'=>getMasterNumber('operand'),
						'master_event_id'=>'AA01', // need to be dynamic based on 16 bits 
						'registration_type' => $registration_type
					);
					$User_id = $this->ci->common_model->addDataIntoTabel('user', $data);
					if ($User_id != "") {
						$returnArray = array('msg' => 'success', 'is_success' => 'true','user_id' => $User_id,'url'=>'home/freePassword');
						$this->ci->session->set_userdata('registration_type', $registration_type);
						$this->ci->session->set_userdata('user_id', $User_id);
						$this->ci->session->set_userdata('registration_step', 2);
						echo json_encode($returnArray);
					} else {
						echo json_encode(array('msg' => lang('record_insert_error'), 'is_success' => 'false'));
					}
                }
            } else {
                echo $this->_errormsg();
            }
        } else {
			$this->ci->home_template->load('home_template', 'packages/apply_free');
		}
    }




	public function freePassword() {
		$user_id = $this->ci->session->userdata('user_id');
		$registration_type = $this->ci->session->userdata('registration_type');
		$registration_step = $this->ci->session->userdata('registration_step');
		if(!empty($user_id) && !empty($registration_type) && !empty($registration_step)){
			if($registration_step=='3'){
				redirect(base_url().'home/freeTerms');
			}
		}
		
		if (!empty($user_id) && !empty($registration_type) && !empty($registration_step) && $registration_step=='2') {
			if ($this->ci->input->post('registration_type')) {
				$this->ci->form_validation->set_rules('password', 'Password', 'trim|required');
				$this->ci->form_validation->set_rules('confirm_password', 'Confirm Password', 'trim|required');
				$this->ci->form_validation->set_rules('user_id', 'User ID', 'trim|required');
				if ($this->ci->form_validation->run($this->ci)){
					$password = $this->ci->input->post('password');
					$confirm_password = $this->ci->input->post('confirm_password');
					$user_id = $this->ci->input->post('user_id');
					if ($password == $confirm_password) {
						$data = array(
							'password' => md5($password)
						);
						$this->ci->common_model->updateDataFromTabel('user', $data, 'id',$user_id);
						$this->ci->session->set_userdata('registration_step', 3);
						$returnArray = array('msg' => 'success', 'is_success' => 'true','user_id' => $user_id,'url'=>'home/freeTerms');
						echo json_encode($returnArray);
					} else {
						echo json_encode(array('msg' => lang('record_insert_error'), 'is_success' => 'false'));
					}
				} else {
					echo $this->_errormsg();
				}
			} else {
				$this->ci->home_template->load('home_template', 'packages/user_password');
			}
		} else {
			redirect(base_url());
		}
	}

	public function freeTerms(){
		$user_id = $this->ci->session->userdata('user_id');
		$registration_type = $this->ci->session->userdata('registration_type');
		$registration_step = $this->ci->session->userdata('registration_step');
		if(!empty($user_id) && !empty($registration_type) && !empty($registration_step)){
			if($registration_step=='2'){
				redirect(base_url().'home/freePassword');
			}
		}
		
		if (!empty($user_id) && !empty($registration_type) && !empty($registration_step) && $registration_step=='3') {
			if ($this->ci->input->post('registration_type')) {
				$this->ci->form_validation->set_rules('our_terms', 'Terms & Conditions', 'trim|required');
				if ($this->ci->form_validation->run($this->ci)){
					$user_id = $this->ci->input->post('terms_user_id');
					$returnArray = array('msg' => 'success', 'is_success' => 'true','url'=>'home/freeCongratulations');
					
					$user_details = $this->ci->common_model->getDataFromTabel('user','email,firstname','id',$user_id);
					// Set email body
					$random_string = encode($user_id);

					//send email to user via mandrill
					/*
                    $this->ci->load->library('mandrill/email_template');
					$template_name = $this->ci->config->item('registration_template_name');
					$template_message = array(
						'to' => array(
							array(
								'email' => $user_details[0]->email,
								'name' => 'Admin',
								'type' => 'to'
							)
						),
						'merge' => true,
						'global_merge_vars' => array(
							array(
								'name' => 'activation_link',
								'content' => base_url() . 'home/verify_email/' . $random_string
							),
							array(
								'name' => 'contact_link',
								'content' => base_url() . '?#contact_email'
							)
						)
					);
					$this->ci->email_template->send_template_email($template_name, $template_message);
					*/
                    
                    $this->ci->load->helper('sendgrid');
                    $apiKey = ADMINNEWMAN_APIKEY;
                    $addTeam_templateId = ADDTeam_templateId; //
                    $facebook_url    = $this->ci->config->item('facebook_url'); #$config['facebook_url']
                    $twitter_url     = $this->ci->config->item('twitter_url'); #$config['facebook_url']
                    $g_plus_url      = $this->ci->config->item('g_plus_url'); #$config['facebook_url']
                    $newmanvenue_url = $this->ci->config->item('newmanvenue_url'); #$config['facebook_url']
                    
                    $paramArray = array(
                    'sub' => array(':firstname' => array($user_details[0]->firstname),':activation_link'=>array(base_url().'home/verify_email/'.$random_string),':contactLink'=>array(base_url().'?#contact_email'),':newmanvenue_url'=>array($newmanvenue_url),':twitter_url'=>array($twitter_url),':g_plus_url'=>array($g_plus_url),':facebook_url'=>array($facebook_url)),
                    'filters' => array('templates' => array('settings' => array('enable' => 1, 'template_id' => $addTeam_templateId)))
                    );
                    $tempSubject = '.';
                    //$templateBody = 'Thanks for joining Newman!!';
                    $templateBody = '.';
                    
                    $toId = $user_details[0]->email;//$admin_email;
                    sendgrid_mail_template($addTeam_templateId,$tempSubject,$templateBody,$toId,$paramArray,$apiKey,adminFromEmail);
                    $return = lang('team_member_saved');
                    
                    
                    //send email to user via mandrill
					$this->ci->session->unset_userdata('registration_step');
					echo json_encode($returnArray);
				} else {
					echo json_encode(array('msg' => lang('record_insert_error'), 'is_success' => 'false'));
				}
			} else {
				$this->ci->home_template->load('home_template', 'packages/our_terms');
			} 
		} else {
			redirect(base_url());
		}
	}
	
	public function freeCongratulations(){
		$user_id = $this->ci->session->userdata('user_id');
		$registration_type = $this->ci->session->userdata('registration_type');
		$registration_step = $this->ci->session->userdata('registration_step');
		if(!empty($user_id) && !empty($registration_type) && !empty($registration_step)){
			if ($registration_step=='2'){
				redirect(base_url().'home/freePassword');
			} else if($registration_step=='3'){
				redirect(base_url().'home/freeTerms');
			}
		}
		if (!empty($user_id) && !empty($registration_type)) {
			$this->ci->session->unset_userdata('user_id');
			$this->ci->session->unset_userdata('registration_type');
			$this->ci->home_template->load('home_template', 'packages/congratulations_page');
		} else {
			redirect(base_url());
		}
	}
	
	public function checkEmailExist(){
		$email = $this->ci->input->post('email');
		if (!empty($email)) {
			$user_details = $this->ci->common_model->getDataFromTabel('user','id',array('email'=>$email, 'user_type_id'=>1, 'registration_type'=>1));
			if(!empty($user_details[0]->id)){
				echo json_encode(array('msg' => lang('is_unique_email_custom_message'), 'is_success' => 'false'));
			} else {
				echo json_encode(array('is_success' => 'true'));
			}
		}
	}
	
	
    function verify_email() {
        $user_id = decode($this->ci->uri->segment(3));
        $where = array('id' => $user_id);
        $userdata = $this->ci->common_model->getDataFromTabel('user', 'email_verify', $where);
        $data = array();

        if (!empty($userdata)) {
            if ($userdata[0]->email_verify == 0) {
                $updateWhere = array('id' => $user_id);
                $updateData = array('email_verify' => '1');
                $this->ci->common_model->updateDataFromTabel('user', $updateData, $updateWhere);
                $data['msg'] = "Your email has been verified successfully.";
            } else {
                $data['msg'] = "Your email is already verified.";
            }
        } else {
            $data['msg'] = "!Oops, Something went wrong. Please check the URL.";
        }

        $this->ci->home_template->load('home_template', 'error_reset_paasword', $data);
    }
    
     public function checkUserEmailExist() {
        $regisEmail = $this->ci->input->post('email');
        $where = array('email' => $regisEmail);
        $userdata = $this->ci->common_model->getDataFromTabel('user','email', $where);
        if ($userdata) {
            return true;
        } else {
            return false;
        }
    }

}

// end class


/* End of file auth.php */
/* Location: ./system/application/modules/home/libraries/auth.php */
