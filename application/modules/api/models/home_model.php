<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * @description: This model is used manage home controller data
 * @create date: 21-Nov-2013
 * @auther: Lokendra Meena
 * @email: lokendrameena@cdnsol.com
 * 
 */ 

class Home_model extends CI_model{


	private $tablenmuser= 'user';
    
	function __construct(){
		parent::__construct();	
			
	}
	
	
	function getAllMembers($user_id){
        
        $this->db->select('u.*,ud.*');
        $this->db->from('user as u');
        $this->db->join('user_details as ud', 'ud.user_id = u.id','left');
        $this->db->where(array('u.id !='=> $user_id,'status'=>'1','email_verify'=>'1'));

        $query = $this->db->get();
        $result = $query->result();
        
		if(!empty($result)){
            return $result;
        }
        else{
            return FALSE;
        }
	}
	
	/*
	 * @description: This function is used to register user data
	 * @param: $insertData
	 * @type: array
	 * @return: $insertId
	 */ 
	
	
	public function registerinsert($insertData){	
		$this->db->insert($this->tablenmuser,$insertData); 
		$insertId = $this->db->insert_id();
		return $insertId; 
	}
	
	
	public function updateUserData($whereData,$Data){	
		$this->db->where($whereData);
		$this->db->update($this->tablenmuser, $Data); 
		return true; 
	}
	
	/*
	 * @description: This function is used to user id and password exist then login
	 * @param: $pastdata
	 * @return: boolean (true/false)
	 */ 
	
	public function checkuserexit($username,$password)
    { 
		$this->db->select('*');
		//$this->db->where('username',$username);
		$this->db->where('email',$username);
		$this->db->where('password',$password);
		$this->db->where('user_type_id','1');
		$query = $this->db->get($this->tablenmuser);
		if($query->row())
			return $query->row();
		else
			return false;	
	}
}
?>
