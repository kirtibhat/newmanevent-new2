<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * @description: This controller is used to manage login, registration
 * @create date: 03-08-2015
 * @author    Krishnapal Singh Dhakad
 * @email     krishnapaldhakad@cdnsol.com
 * 
 */ 

class Profile extends MX_Controller{

	function __construct(){
		parent::__construct();
		$this->session_check->checkSession();
		$this->load->library('factory',$this);
		$this->load->library(array('head','template','process_upload'));
        $this->load->language('home');
	}

    public function index($email="0",$venue_id="0"){
        //print_r($_FILES);die;
		$this->factory->index();
	} 
	
}

/* End of file home.php */
/* Location: ./system/application/modules/home/controllers/home.php */
