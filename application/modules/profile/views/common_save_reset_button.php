<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//reset button
if(array_search('reset',$showbutton)!==false){ 
	$extraSave 	= 'class="'.$buttonclass['reset'].'"';
	echo form_button('form_reset',$labletext['reset'],$extraSave);
}

//save button
if(array_search('save',$showbutton)!==false){ 
	$data = array(
		'name' => 'save',
		'class' => $buttonclass['save'],
		'value' => 'true',
		'type' => 'submit',
		'content' => $labletext['save']
	);
	echo form_button($data);
}
?>

