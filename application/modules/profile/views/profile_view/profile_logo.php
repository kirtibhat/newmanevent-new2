<?php if (!defined('BASEPATH'))  exit('No direct script access allowed');

$nick_nameVal 				= (!empty($userDetail->newman_nickname)) ? $userDetail->newman_nickname : '';
$userLogoVal 				= (!empty($userDetail2->userLogo)) ? $userDetail2->userLogo : '';

$userProfile = array(
'name' 		=> 'userProfile',
'id' 		=> 'userProfile',
'method'	=> 'post',
'class' 	=> '', 
'lang'      => 'userProfile',
);

$nick_name = array(
'name' 			=> 'nick_name',
'value' 		=> $nick_nameVal,
'id' 			=> 'nick_name',
'type' 			=> 'text',
'class' 		=> 'xlarge_input trim_check',
'placeholder'	=> lang('nick_name')
);
?>

<div class="panel">
	<div class="panel_header showHideUserProfile"> <?php echo lang('profile');?> </div>
	<div class="panel_contentWrapper" id="showHideUserLogoProfile">
		<div class="infobar">
            <p class="dn" id="userProfileInfo"><?php echo lang('information_bar');?></p>
            <span class="info_btn" onclick="showHideInfoBar('userProfileInfo');"></span>
        </div>
		<?php echo form_open_multipart($this->uri->uri_string(), $userProfile); ?>
	<div class="panel_content">
	<div class="row">
		<div class="col-3">
			<div class="labelDiv">
				<label class="form-label text-right"><?php echo lang('update_profile_photo');?></label>
			</div>
		</div>
		<div class="col-5">
			<div class="custom-upload">
				<span class="label_text">Select or Drop File</span>
				<input type="file" class="customIinputFileHeader" accept="image/*" name="user_logo" id="user_logo" class="xlarge_input" placeholder="" draggable="true">
				<div class="display_file">
					<span class="file_value"></span>
					<a href="javascript:void(0)" class="clear_value"></a>
				</div>
			</div>

			<div class="uploadedName upload_logo_preview pull-left" style="<?php if(!empty($userLogoVal)){ echo 'display:block;';}else{  echo 'display:none;';}?> margin-top:-7px;">
				<span class="upload_logo_name"><?php if(!empty($userLogoVal)){ echo $userLogoVal;}?></span>
				<span class="small_icon remove_upload_logo"><img src="<?php echo base_url().'themes/system/images/crox.png';?>"></span>
			</div>
			<input type="hidden" name="remove_user_logo" id="remove_user_logo">
		</div>
	</div>         
	<div class="row">
		<div class="col-3">
			<div class="labelDiv">
				<label class="form-label text-right"><?php echo lang('name_nickname');?></label>
			</div>
		</div>
		<div class="col-5">
			<?php echo form_input($nick_name);
				  echo form_error('nick_name');?>
				  <span id="error-nick_name"></span>
		</div>
	</div>              
</div>
<div class="panel_footer">
			<div class="pull-right">
				<?php
				echo form_hidden('userId', $userId);
				echo form_hidden('formActionName', 'userProfile');
				//button show
				$formButton['showbutton'] = array('save', 'reset');
				$formButton['labletext'] = array('save' => lang('save'), 'reset' => lang('clear'));
				$formButton['buttonclass'] = array('save' => 'btn-normal btn', 'reset' => 'btn-normal btn reset_profile_logo');
				$this->load->view('common_save_reset_button', $formButton);
				?>
			</div>         
		</div>		
	<?php echo form_close(); ?>
	</div>
</div>
<script>
$(document).ready(function(){
//onsole.log('demo');
//console.log(('#userProfile').attr('lang'));
});
</script>
