<?php if (!defined('BASEPATH'))  exit('No direct script access allowed');

$alert_emailidVal 	= (!empty($userDetail2->alert_emailid)) ? $userDetail2->alert_emailid : '';
$alert_mobile_noVal = (!empty($userDetail2->alert_mobile_no)) ? $userDetail2->alert_mobile_no : '';


$userNotification = array(
'name' 		=> 'userNotification',
'id' 		=> 'userNotification',
'method'	=> 'post',
'class' 	=> ''
);

$email_alert1 = array(
'name'        => 'email_alert',
'id'          => 'email_alert1',
'value'       => 'yes',
'class'       => 'radioButton',
'checked'     => (!empty($alert_emailidVal)) ? true : false
);
$email_alert2 = array(
'name'        => 'email_alert',
'id'          => 'email_alert2',
'value'       => 'no',
'class'       => 'radioButton',
'checked'     => (empty($alert_emailidVal)) ? true : false
);
$required1 = (!empty($alert_emailidVal))?'required trim_check':'';
$alert_emailid = array(
'name' 			=> 'alert_emailid',
'id' 			=> 'alert_emailid',
'type' 			=> 'text',
'class' 		=> 'xlarge_input '.$required1,
'value'			=> $alert_emailidVal,
'placeholder'	=> 'Email Address'
);

$sms_alert1 = array(
'name'        => 'sms_alert',
'id'          => 'sms_alert1',
'value'       => 'yes',
'class'       => 'radioButton',
'checked'     => (!empty($alert_mobile_noVal)) ? true : false
);
$sms_alert2 = array(
'name'        => 'sms_alert',
'id'          => 'sms_alert2',
'value'       => 'no',
'class'       => 'radioButton',
'checked'     => (empty($alert_mobile_noVal)) ? true : false
);
$required2 = (!empty($alert_mobile_noVal))?'required trim_check':'';
$alert_mobile_no = array(
'name' 			=> 'alert_mobile_no',
'id' 			=> 'alert_mobile_no',
'type' 			=> 'text',
'class' 		=> 'xlarge_input '.$required2,
'value'			=> $alert_mobile_noVal,
'placeholder'	=> 'Mobile Number'
);

?>

<div class="panel">
  <div class="panel_header showHideUserNotification"><?php echo lang('notifications');?></div>
  <div class="panel_contentWrapper" id="showHideNotificationForm">
	  <div class="infobar">
            <p class="dn" id="userNotificationInfo"><?php echo lang('information_bar');?></p>
            <span class="info_btn" onclick="showHideInfoBar('userNotificationInfo');"></span>
        </div>
	  <?php echo form_open_multipart($this->uri->uri_string(), $userNotification); ?>
	<div class="panel_content">
	  <div class="row">
		<div class="col-3">
			<div class="labelDiv">
				<label class="form-label text-right"><?php echo lang('email_alert');?><span class="required">*</span></label>
				<span class="info_btn"><span class="field_info group xsmall"><?php echo lang('email_alert');?></span></span>
			</div>
		</div>
		<div class="col-5">
			<div class="radioDiv">
				<?php echo form_radio($email_alert1);?>
				<label class="form-label pull-left" for="email_alert1"><span><?php echo lang('yes');?></span></label>
			</div>
			<div class="radioDiv">
				<?php echo form_radio($email_alert2);?>
				<label class="form-label pull-left" for="email_alert2"><span><?php echo lang('no');?></span></label>
			</div>
		</div>
	</div>
	  <div class="row alertEmailWrap" style="<?php echo (empty($alert_emailidVal)) ? 'display:none;': ''; ?>">
		<div class="col-3">
		  <div class="labelDiv">
			<label class="form-label text-right"><?php echo lang('email_address');?></label>
		  </div>
		</div>
		<div class="col-5">
		  <?php echo form_input($alert_emailid);
				  echo form_error('alert_emailid');?>
				  <span id="error-alert_emailid"></span>
		</div>
	  </div>
	  <div class="row">
		<div class="col-3">
			<div class="labelDiv">
				<label class="form-label text-right"><?php echo lang('sms_alert');?><span class="required">*</span></label>
				<span class="info_btn"><span class="field_info group xsmall"><?php echo lang('sms_alert');?></span></span>
			</div>
		</div>
		<div class="col-5">
			<div class="radioDiv">
				<?php echo form_radio($sms_alert1);?>
				<label class="form-label pull-left" for="sms_alert1"><span><?php echo lang('yes');?></span></label>
			</div>
			<div class="radioDiv">
				<?php echo form_radio($sms_alert2);?>
				<label class="form-label pull-left" for="sms_alert2"><span><?php echo lang('no');?></span></label>
			</div>
		</div>
	</div>              
	  <div class="row alertSMSWrap" style="<?php echo (empty($alert_mobile_noVal)) ? 'display:none;': ''; ?>">
		<div class="col-3">
		  <div class="labelDiv">
			<label class="form-label text-right"><?php echo lang('mobile_number');?></label>
		  </div>
		</div>
		<div class="col-5">
		  <?php echo form_input($alert_mobile_no);
				  echo form_error('alert_mobile_no');?>
				  <span id="error-alert_mobile_no"></span>
		</div>
	  </div>
	</div>
	<div class="panel_footer">
			<div class="pull-right">
				<?php
				echo form_hidden('userId', $userId);
				echo form_hidden('formActionName', 'userNotification');

				//button show
				$formButton['showbutton'] = array('save', 'reset');
				$formButton['labletext'] = array('save' => lang('save'), 'reset' => lang('clear'));
				$formButton['buttonclass'] = array('save' => 'btn-normal btn', 'reset' => 'btn-normal btn reset_form');
				$this->load->view('common_save_reset_button', $formButton);
				?>
			</div>         
		</div>		
	<?php echo form_close(); ?>
  </div>
</div>
