<?php if (!defined('BASEPATH'))  exit('No direct script access allowed');
$fb_id = (!empty($userDetail->fb_id)) ? $userDetail->fb_id : '';
$userFacebookVal 	= (!empty($userDetail2->facebook)) ? $userDetail2->facebook : 'https://www.facebook.com/'.$fb_id;
$userInstagramVal 	= (!empty($userDetail2->instagram)) ? $userDetail2->instagram : '';
$userTwitterVal 	= (!empty($userDetail2->twitter)) ? $userDetail2->twitter : '';
$userYoutubeVal 	= (!empty($userDetail2->youtube)) ? $userDetail2->youtube : '';
$userPintrestVal 	= (!empty($userDetail2->pintrest)) ? $userDetail2->pintrest : '';
$userLinkedInVal 	= (!empty($userDetail2->linkedIn)) ? $userDetail2->linkedIn : '';
$customSocial 		= (!empty($userDetail2->customSocial)) ? $userDetail2->customSocial : '';


$userSocialMedia = array(
'name' 		=> 'userSocialMedia',
'id' 		=> 'userSocialMedia',
'method'	=> 'post',
'class' 	=> ''
);

$userFacebook 	= array(
'name' 			=> 'userFacebook',
'id' 			=> 'userFacebook',
'value' 			=> $userFacebookVal,
'type' 			=> 'text',
'class' 		=> 'xlarge_input trim_check',
'placeholder'	=> lang('facebook')
);

$userInstagram 	= array(
'name' 			=> 'userInstagram',
'id' 			=> 'userInstagram',
'value' 			=> $userInstagramVal,
'type' 			=> 'text',
'class' 		=> 'xlarge_input trim_check',
'placeholder'	=> lang('instagram')
);

$userTwitter 	= array(
'name' 			=> 'userTwitter',
'id' 			=> 'userTwitter',
'value' 			=> $userTwitterVal,
'type' 			=> 'text',
'class' 		=> 'xlarge_input trim_check',
'placeholder'	=> lang('twitter')
);

$userYoutube 	= array(
'name' 			=> 'userYoutube',
'id' 			=> 'userYoutube',
'value' 			=> $userYoutubeVal,
'type' 			=> 'text',
'class' 		=> 'xlarge_input trim_check',
'placeholder'	=> lang('youtube')
);

$userPintrest 	= array(
'name' 			=> 'userPintrest',
'id' 			=> 'userPintrest',
'value' 			=> $userPintrestVal,
'type' 			=> 'text',
'class' 		=> 'xlarge_input trim_check',
'placeholder'	=> lang('pintrest')
);

$userLinkedIn 	= array(
'name' 			=> 'userLinkedIn',
'id' 			=> 'userLinkedIn',
'value' 			=> $userLinkedInVal,
'type' 			=> 'text',
'class' 		=> 'xlarge_input trim_check',
'placeholder'	=> lang('linkedIn')
);	
?>

<div class="panel">
	<div class="panel_header showHideSocialMedia"><?php echo lang('social_media'); ?> </div>
	<div class="panel_contentWrapper" id="showhideprofilesocialmedia">
		<div class="infobar">
            <p class="dn" id="userSocialMediaInfo"><?php echo lang('information_bar');?></p>
            <span class="info_btn" onclick="showHideInfoBar('userSocialMediaInfo');"></span>
        </div>
		<?php echo form_open($this->uri->uri_string(), $userSocialMedia); ?>
		<div class="panel_content">                            
			<div class="row">
				<div class="col-3">
					<div class="labelDiv">
						<label class="form-label text-right"><?php echo lang('facebook'); ?></label>
					</div>
				</div>
				<div class="col-5">
					<?php echo form_input($userFacebook);
						  echo form_error('userFacebook'); ?>
				</div>
			</div>
			<div class="row">
				<div class="col-3">
					<div class="labelDiv">
						<label class="form-label text-right"><?php echo lang('instagram'); ?></label>
					</div>
				</div>
				<div class="col-5">
					<?php echo form_input($userInstagram);
						  echo form_error('userInstagram'); ?>
				</div>
			</div>
			<div class="row">
				<div class="col-3">
					<div class="labelDiv">
						<label class="form-label text-right"><?php echo lang('twitter'); ?></label>
					</div>
				</div>
				<div class="col-5">
					<?php echo form_input($userTwitter);
						  echo form_error('userTwitter'); ?>
				</div>
			</div>
			<div class="row">
				<div class="col-3">
					<div class="labelDiv">
						<label class="form-label text-right"><?php echo lang('youtube'); ?></label>
					</div>
				</div>
				<div class="col-5">
					<?php echo form_input($userYoutube);
						  echo form_error('userYoutube'); ?>
				</div>
			</div>
			<div class="row">
				<div class="col-3">
					<div class="labelDiv">
						<label class="form-label text-right"><?php echo lang('pintrest'); ?></label>
					</div>
				</div>
				<div class="col-5">
					<?php echo form_input($userPintrest);
						  echo form_error('userPintrest'); ?>
				</div>
			</div>
			<div class="row">
				<div class="col-3">
					<div class="labelDiv">
						<label class="form-label text-right"><?php echo lang('linkedIn'); ?></label>
					</div>
				</div>
				<div class="col-5">
					<?php echo form_input($userLinkedIn);
						  echo form_error('userLinkedIn'); ?>
				</div>
			</div> 
			<div class="row customSocialContainer">
				<?php 
				if(!empty($customSocial)){
					$customSocial = json_decode($customSocial);
					//$flag=1;
					foreach($customSocial as $key => $val){
						?>
						<div class="row" id="customsocial<?php echo $key;?>">
							<div class="col-3">
								<div class="labelDiv">
									<label class="form-label text-right"><?php echo $val->label;?></label>
									<input type="hidden" name="customSocialName[]" value="<?php echo $val->label;?>">
								</div>
							</div>
							<div class="col-5">
								<input type="text" class="medium_input hasEditButton" name="customSocialValue[]" value="<?php echo $val->value;?>">
								<div class="editButtonGroup">
									<button type="button" data-toggle="modal" data-target=".addField_popup" class="btn editSocialCustom" customSocialMainId="<?php echo $key;?>" customSocialId="customsocial<?php echo $key;?>" linkname="<?php echo $val->label;?>" linkvalue="<?php echo $val->value;?>">
										<span class="medium_icon"> 
											<i class="icon-edit"></i> 
										</span>
									</button>
									 
									<a class="btn red delete_personal_form_field" customSocialId="customsocial<?php echo $key;?>" customSocialMainId="<?php echo $key;?>">
										<div class="btn_center">
											<span class="medium_icon">
												<i class="icon-delete"></i>
											</span>
										</div>
									</a>
									
									<!--<button data-toggle="modal" data-target=".temp_confirm_popup" type="button" customSocialId="customsocial<?php //echo $key;?>" customSocialMainId="<?php //echo $key;?>" class="btn red deleteSocialCustom">
										<span class="medium_icon">
											<i class="icon-delete"></i> 
										</span>
									</button>-->
								</div>
							</div>
						</div>
						<?php
						//$flag++;
						}
					}
				?>

			</div>                      
			<div class="row mT30">
				<div class="col-1">
				</div>
				<div class="col-5">
					<a class="btn-icon btn-icon-large btn add_social_link_label"  data-toggle="modal" data-target=".addField_popup">
						<span class="large_icon"><i class="icon-add"></i></span><?php echo lang('add_social_media'); ?>
					</a>
				</div>
			</div>
			              
		</div>                        
		<div class="panel_footer">
			
			<div class="pull-right">
				<?php
				echo form_hidden('userId', $userId);
				echo form_hidden('formActionName', 'userSocialMedia');

				//button show
				$formButton['showbutton'] = array('save', 'reset');
				$formButton['labletext'] = array('save' => lang('save'), 'reset' => lang('clear'));
				$formButton['buttonclass'] = array('save' => 'btn-normal btn', 'reset' => 'btn-normal btn reset_form');
				$this->load->view('common_save_reset_button', $formButton);
				?>
			</div>        
		</div>
		<?php echo form_close(); ?>
	</div>
</div>

<script type="text/javascript">
//call for event data save and media upload
ajaxdatasave('userSocialMedia', '<?php echo $this->uri->uri_string(); ?>',true,true,false,false,false,'#showhideprofilesocialmedia','#showhideprofilenotification');
</script>

