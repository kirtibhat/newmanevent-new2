<?php if (!defined('BASEPATH'))  exit('No direct script access allowed');

$show_passwordVal 				= (!empty($userDetail2->show_password)) ? $userDetail2->show_password : '';


$userPassword = array(
'name' 		    => 'userPassword',
'id' 		    => 'userPassword',
'method'	    => 'post',
'class' 	    => ''
);

$password = array(
'name' 			=> 'password',
'id' 			=> 'password',
'type' 			=> (!empty($show_passwordVal))? 'text' : 'password',
'class' 		=> 'xlarge_input trim_check',
'placeholder'	=> lang('current_password')
);
$new_password = array(
'name' 			=> 'new_password',
'id' 			=> 'new_password',
'type' 			=> (!empty($show_passwordVal))? 'text' : 'password',
'class' 		=> 'xlarge_input trim_check',
'placeholder'	=> lang('new_password')
);
$confirm_new_password = array(
'name' 			=> 'confirm_new_password',
'id' 			=> 'confirm_new_password',
'type' 			=> (!empty($show_passwordVal))? 'text' : 'password',
'class' 		=> 'xlarge_input trim_check',
'placeholder'	=> lang('confirm_new_password')
);
$show_password = array(
'name'        => 'show_password',
'id'          => 'show_password',
'value'       => 'yes',
'class' 		=> 'checkBox',
//'checked'     => (!empty($show_passwordVal))? true : false
);
?>

<div class="panel">
	<div class="panel_header showHideUserPassword"> <?php echo lang('password');?> </div>
	<div class="panel_contentWrapper" id="showHideUserPassword">
		<div class="infobar">
            <p class="dn" id="confirm_new_password_info"><?php echo lang('information_bar');?></p>
            <span class="info_btn" onclick="showHideInfoBar('confirm_new_password_info');"></span>
        </div>
		<?php echo form_open_multipart($this->uri->uri_string(), $userPassword); ?>
		<div class="panel_content">
			<?php if(!empty($userDetail->password)){ ?>
			<div class="row">
				<div class="col-3">
					<div class="labelDiv">
						<label class="form-label text-right"><?php echo lang('current_password');?><span class="required">*</span></label>
					</div>
				</div>
				<div class="col-5">
					<?php echo form_input($password);
						  echo form_error('password');?>
						  <span id="error-password"></span>
				</div>
			</div>
			<?php }?>
			<div class="row">
				<div class="col-3">
					<div class="labelDiv">
						<label class="form-label text-right"><?php echo lang('new_password');?></label>
					</div>
				</div>
				<div class="col-5">
					<?php echo form_input($new_password);
						  echo form_error('new_password');?>
						  <span id="error-new_password"></span>
				</div>
			</div>
			<div class="row">
				<div class="col-3">
					<div class="labelDiv">
					<label class="form-label text-right"><?php echo lang('confirm_new_password');?></label>
					</div>
				</div>
				<div class="col-5">
					<?php echo form_input($confirm_new_password);
						  echo form_error('confirm_new_password');?>
						  <span id="error-confirm_new_password"></span>
				</div>
			</div>
			<div class="row">
				<div class="col-3">
					<div class="labelDiv">
						<label class="form-label text-right"><?php echo lang('show_password');?></label>
					</div>
				</div>
				<div class="col-5">
					<?php echo form_checkbox($show_password); ?>
					<label class="form-label mT5" for="show_password"></label>                    
				</div>
			</div>
		</div>
		<div class="panel_footer">
			<div class="pull-right">
				<?php
				echo form_hidden('userId', $userId);
				echo form_hidden('formActionName', 'userPassword');

				//button show
				$formButton['showbutton'] = array('save', 'reset');
				$formButton['labletext'] = array('save' => lang('save'), 'reset' => lang('clear'));
				$formButton['buttonclass'] = array('save' => 'btn-normal btn', 'reset' => 'btn-normal btn reset_form');
				$this->load->view('common_save_reset_button', $formButton);
				?>
			</div>         
		</div>		
	<?php echo form_close(); ?>
	</div>
</div>	
