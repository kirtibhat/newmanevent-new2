<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');  ?>
<div>
<form action='<?php echo base_url('home/login');?>' method="post" >
	<?php echo get_global_messages('login_error');?>
	<input type="text" name="loginUsername"><br>
	<input type="password" name="loginPassword"><br>
	<input type="submit" name="submit" value="Login"><br>
</form>
</div>
<br>
<div>
	<label>Step 1</label>
<form action='<?php echo base_url('home/register');?>' method="post" >
	<?php echo get_global_messages('register_error');?>
	<input type="text" name="email" placeholder="Email"><?php echo form_error('email');?><br>
	<input type="text" name="confrim_email" placeholder="Confirm Email"><?php echo form_error('confrim_email');?><br>
	<input type="text" name="firstname" placeholder="First Name"><?php echo form_error('firstname');?><br>
	<input type="text" name="lastname" placeholder="Last Name"><?php echo form_error('lastname');?><br>
	<input type="text" name="organisation" placeholder="Organisation"><?php echo form_error('organisation');?><br>
	<input type="submit" name="submit" value="Next"><br>
</form>
</div><br>
<div>
	<label>Step 2</label>
<form action='<?php echo base_url('home/registerpassword');?>' method="post" >
	<?php echo get_global_messages('registerpassword_error');?>
	<input type="password" name="password" placeholder="Password"><?php echo form_error('password');?><br>
	<input type="password" name="confirm_password" placeholder="Confirm Password"><?php echo form_error('confirm_password');?><br>

	<input type="submit" name="submit" value="Next"><br>
</form>
</div><br>
<div>
	<label>Step 3</label>
<form action='<?php echo base_url('home/registerterms');?>' method="post" >
	<?php echo get_global_messages('registerterms_error');?>
	<input type="checkbox" name="our_terms" value='1'><?php echo form_error('our_terms');?><br>

	<input type="submit" name="submit" value="Next"><br>
</form>
</div><br>
