<?php if (!defined('BASEPATH'))  exit('No direct script access allowed');

$firstnameVal 		= (!empty($userDetail->firstname)) ? $userDetail->firstname : '';
$lastnameVal 		= (!empty($userDetail->lastname)) ? $userDetail->lastname : '';
$company_nameVal 	= (!empty($userDetail->company_name)) ? $userDetail->company_name : '';
$emailVal 			= (!empty($userDetail->email)) ? $userDetail->email : '';

$userBasicInformation = array(
    'name' 		    => 'userBasicInformation',
    'id' 		    => 'userBasicInformation',
    'method'	    => 'post',
    'class' 	    => ''
    );

$firstname = array(
    'name' 			=> 'firstname',
    'value' 		=> $firstnameVal,
    'id' 			=> 'firstname',
    'type' 			=> 'text',
    'class' 		=> 'xlarge_input',
    'placeholder'	=> lang('first_name'),
    'disabled'		=> 'disabled'
);

$lastname = array(
    'name' 			=> 'lastname',
    'value' 		=> $lastnameVal,
    'id' 			=> 'lastname',
    'type' 			=> 'text',
    'class' 		=> 'xlarge_input',
    'placeholder'	=> lang('last_name'),
    'disabled'		=> 'disabled'
);

$company_name = array(
    'name' 			=> 'company_name',
    'value' 		=> $company_nameVal,
    'id' 			=> 'company_name',
    'type' 			=> 'text',
    'class' 		=> 'xlarge_input trim_check',
    'placeholder'	=> 'Organisation'
);
$email = array(
    'name' 			=> 'email',
    'value' 		=> $emailVal,
    'id' 			=> 'email',
    'type' 			=> 'text',
    'class' 		=> 'xlarge_input',
    'placeholder'	=> 'marissa.m@natpromo.com.au',
    'disabled'		=> 'disabled'
);
?>

<div class="panel open">
	<div class="panel_header showHideBasicInformation"> <?php echo lang('basic_information');?> </div>
	<div class="panel_contentWrapper" style="display:block;" id="showHideBasicInfoForm">
       <div class="infobar">
        <p class="dn" id="userBasicInformationInfo"><?php echo lang('information_bar');?></p>
        <span class="info_btn" onclick="showHideInfoBar('userBasicInformationInfo');"></span>
      </div>
		<?php echo form_open_multipart($this->uri->uri_string(), $userBasicInformation); ?>
		<div class="panel_content">
			<div class="row">
				<div class="col-3">
					<div class="labelDiv">
						<label class="form-label text-right"><?php echo lang('first_name');?></label>
					</div>
				</div>
				<div class="col-5">
					<?php echo form_input($firstname);
						  echo form_error('firstname');?>
						  <span id="error-firstname"></span>
				</div>
			</div>
			<div class="row">
				<div class="col-3">
					<div class="labelDiv">
						<label class="form-label text-right"><?php echo lang('last_name');?></label>
					</div>
				</div>
				<div class="col-5">
					<?php echo form_input($lastname);
						  echo form_error('lastname');?>
						  <span id="error-lastname"></span>
				</div>
			</div>
			<div class="row">
				<div class="col-3">
					<div class="labelDiv">
						<label class="form-label text-right"><?php echo lang('organisation');?></label>
					</div>
				</div>
				<div class="col-5">
					<?php echo form_input($company_name);
						  echo form_error('company_name');?>
						  <span id="error-company_name"></span>
				</div>
			</div>
			<div class="row">
				<div class="col-3">
					<div class="labelDiv">
						<label class="form-label text-right"><?php echo lang('email_address');?></label>
					</div>
				</div>
				<div class="col-5">
					<?php echo form_input($email);
						  echo form_error('email');?>
						  <span id="error-email"></span>
				</div>
			</div>
		</div>
		<div class="panel_footer">
			<div class="pull-right">
				<?php
				echo form_hidden('userId', $userId);
				echo form_hidden('formActionName', 'userBasicInformation');

				//button show
				$formButton['showbutton'] = array('save', 'reset');
				$formButton['labletext'] = array('save' => lang('save'), 'reset' => lang('clear'));
				$formButton['buttonclass'] = array('save' => 'btn-normal btn', 'reset' => 'btn-normal btn reset_basic_info');
				$this->load->view('common_save_reset_button', $formButton);
				?>
			</div>         
		</div>		
	<?php echo form_close(); ?>
	
	</div>
</div>
		
