<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');  ?>
<div class="page_content">
<div class="container">
<div class="heading">
	  <h1 class="font-xxlarge">My Profile</h1>
</div>
<div class="row">
	<div class="col-9">
		<?php
		
		// load profile basic information view
		$this->load->view('profile_basic_information');
		
		// load profile password view
		$this->load->view('profile_password');
		
		// load user profile view
		$this->load->view('profile_logo');
		
		// load profile social media view
		$this->load->view('profile_social_media');
		
		// load profile notification view
		$this->load->view('profile_notification');
		
		// load custom form social media view
		$this->load->view('social_media_custom_form');
		
		?>
		<div class="clearFix display_inline w_100 mT10"></div>
	  </div>
	</div>
</div>
</div>
</div>
