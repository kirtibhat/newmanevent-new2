<?php if (!defined('BASEPATH'))  exit('No direct script access allowed');

$userCustomSocialMedia = array(
    'name' 		=> 'userCustomSocialMedia',
    'id' 		=> 'userCustomSocialMedia',
    'method'	=> 'post',
    'class' 	=> '',
    );
    
$customSocialName 	= array(
    'name' 			=> 'customSocialName',
    'id' 			=> 'customSocialName',
    'type' 			=> 'text',
    'class' 		=> 'xxlarge_input trim_check',
    'placeholder'	=> lang('name')
    );
	
$customSocialValue 	= array(
    'name' 			=> 'customSocialValue',
    'id' 			=> 'customSocialValue',
    'type' 			=> 'text',
    'class' 		=> 'xxlarge_input trim_check',
    'placeholder'	=> lang('value')
   );					    
?>
<div class="modal fade addField_popup">
  <div class="modal-dialog modal-sm-as">
    <div class="modal-content">
      <div class="modal-header">        
        <h4 class="modal-title social_link_label"><?php echo lang('add_custom_social_media_link');?></h4>
      </div>
      <div class="infobar">
			  <p></p>
			  <span class="info_btn"><span class="field_info xsmall"><?php echo lang('information_bar');?></span></span>
			</div>
      <div class="modal-body">
		<?php echo form_open($this->uri->uri_string(), $userCustomSocialMedia); ?>
      	<div class="modal-body-content">
			<div class="row">
				<div class="labelDiv pull-left">
				  <label class="form-label text-right"><?php echo lang('name');?><span class="required">*</span></label>
				</div>
				<?php echo form_input($customSocialName);
			  echo form_error('customSocialName'); ?>
			  <span id="error-customSocialName"></span>
			</div>
			<div class="row">
				<div class="labelDiv pull-left">
				  <label class="form-label text-right"><?php echo lang('value');?><span class="required">*</span></label>
				</div>
				<?php echo form_input($customSocialValue);
					  echo form_error('customSocialValue'); ?>
					  <span id="error-customSocialValue"></span>
					  <?php
					  echo form_hidden('userId', $userId);
					  echo form_hidden('formActionName', 'userCustomSocialMedia');
					  ?>
					  
					  <input type="hidden" id="setFormType">
					  <input type="hidden" id="customSocialMainId">
			</div>
			
		</div>
        <div class="modal-footer">
        	<div class="pull-right">
                <a class="btn-normal btn esc_btn_press" id="addCustomSocialClose"  data-dismiss="modal" class="close"><?php echo lang('close');?></a>  
                <button type="submit" class="btn-normal btn"><?php echo lang('save');?></button>          
            </div>         	
        </div>     
		<?php echo form_close(); ?>
      </div>               
    </div>
  </div>
</div>
