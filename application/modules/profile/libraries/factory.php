<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * 
 * This class is used to manage for home controller, login, registration, forgot password
 * trouble login, other page like(contact us, abouts etc.)
 *
 * @package:   		Factory
 * @author:    		Krishnapal Singh Dhakad
 * @email:  	   	krishnapaldhakad@cdnsol.com
 * @create date:	09-Nov-2015
 * @since:     		Version 1.0
 * @filesource
 */
class Factory {

    private $ci = NULL;

    function __construct($ci) {
        //create instance object
        $this->ci = $ci;
    }

	 /*
     * @description: This function is used to profile section save  
     */

    public function index(){
		//echo '<pre>'; print_r($this->ci->session); exit; 
		$userId 		= isLoginUser();
		if($userId==0){
			
		}
		//check is data post 
		if($this->ci->input->post()){
	  
			$actionName = $this->ci->input->post('formActionName');
			switch($actionName){
				//insert & update basic venue details form data
				case "userBasicInformation":
				  $this->userBasicInformation();
				break;

				//insert & update venue conatact details form data
				case "userPassword":
				  $this->userPassword();
				break;

				//insert & update venue finder details form data
				case "userProfile":
				  $this->userProfile();
				break;              

				//insert & update venue social media form data
				case "userSocialMedia":
				  $this->userSocialMedia();
				break;
				
				//insert & update venue custom social media form data
				case "userNotification":
				  $this->userNotification();
				break;

				default:
				// load venue details form view
				$this->userView();
			}
		} else{
		  // load venue details form view
		  $this->userView();
		}
	}
	
	/*
    * @access: private 
    * @description: This function is used to load user details view
    * @param1: $venueId
    * @return: void
    *  
    */
  
	private function userView(){
	  
		$userId 		= isLoginUser();
		$userDetail = curlCall($this->ci->config->item('newmanUrl').'api/getUserDetail',array('user_id'=>$userId));	    
		#echo '<pre></pre>'; print_r($userDetail); exit; 
		$userDetail = json_decode($userDetail);
		$data = array();
		if($userDetail->msg=='success'){
			$data['userId'] = $userId;
			$data['userDetail'] = $userDetail->response;
			$data['userDetail2'] = (!empty($userDetail->response2)) ? $userDetail->response2 :'';
			if(!empty($userDetail->response2))
			{
				$userData2 = $userDetail->response2;
				if(!empty($userData2->userLogo)){
					$newmanUrl = $this->ci->config->item('newmanUrl');
					$userLogo = $newmanUrl.'media/user_logo/'.$userData2->userLogo;
					$this->ci->session->set_userdata('userLogo',$userLogo);
				}
			}
		}
		#echo '<pre>'; print_r($data); exit; 
		$this->ci->template->load('template','profile_view/profile',$data,TRUE);
	}
    
	/*
	* @description: This function is used to check and save user Basic Information
	*/ 
	private function userBasicInformation(){
		$userId = isLoginUser(); 
		//get ajax request
		$is_ajax_request = $this->ci->input->is_ajax_request();
		
		$this->_userBasicInformationSave();
		$msg = lang('user_basic_information_updated');
		if($is_ajax_request){
			echo json_encode(array('msg'=>$msg,'is_success'=>'true'));
		}else{
			$msgArray = array('msg'=>$msg,'is_success'=>'true');
			set_global_messages($msgArray);
			redirect('profile');
		}
	}
     
	
	/*
	* @description: This function is used to save user Basic Information
	*/ 
	private function _userBasicInformationSave(){
		$updateData['id']	= isLoginUser();
		$updateData['company_name']		= $this->ci->input->post('company_name');
		$response = curlCall($this->ci->config->item('newmanUrl').'api/userBasicInformationSave',$updateData);
		$this->ci->session->set_userdata('company_name',$updateData['company_name']);
		echo json_encode(array('msg'=>'success','is_success'=>'true','company_name' => $updateData['company_name'])); 
		exit;
		//$response = json_decode($response);
		//echo '<pre>'; print_r($response); exit;
		
	}
   
    /*
	* @description: This function is used to check and save user password
	*/ 
	private function userPassword(){
        
		$this->_userPasswordValidate(); 
		//get ajax request
		$is_ajax_request = $this->ci->input->is_ajax_request();
		
		$checkstatus = false;
        if ($this->ci->form_validation->run($this->ci)) {
			$password		    = $this->ci->input->post('password');
			$new_password		= $this->ci->input->post('new_password');
			$userId 		    = isLoginUser();
			$userDetail         = curlCall($this->ci->config->item('newmanUrl').'api/getUserDetail',array('user_id'=>$userId));
			$userDetail         = json_decode($userDetail);
			$saved_password     = $userDetail->response->password;
			if(!empty($saved_password) && !empty($password) && md5($password) != $saved_password){
                $checkstatus = true;
				if($is_ajax_request){
					echo json_encode(array('msg'=> 'Current password not matched','is_success'=>'false'));
				}else{
					$msgArray = array('msg'=> 'Current password not matched','is_success'=>'false');
					set_global_messages($msgArray);
					redirect('profile');
				}
			}
			if(!empty($password) && !empty($new_password) && (md5($password) == md5($new_password))){
                $checkstatus = true;
				if($is_ajax_request){
					echo json_encode(array('msg'=> 'Current password and new password should not same','is_success'=>'false'));
				}else{
					$msgArray = array('msg'=> 'Current password and new password should not same','is_success'=>'false');
					set_global_messages($msgArray);
					redirect('profile');
				}
			}
			
            if(!$checkstatus) {
                $this->_userPasswordSave();
                $msg = 'Password updated successfully';
                if($is_ajax_request){
                    echo json_encode(array('msg'=>$msg,'is_success'=>'true'));
                }else{
                    $msgArray = array('msg'=>$msg,'is_success'=>'true');
                    set_global_messages($msgArray);
                    redirect('profile');
                }
            }
			
			
		} else{
			$errors = validation_errors();
			if($is_ajax_request){
				echo json_message('msg',$errors,'is_success','false');
			}else{
				// load event form view
				$this->userView();
			}
		}
	}
     
	/*
	* @description: This function is used to validate user password
	*/ 
	private function _userPasswordValidate(){    
		// set ruls for form filed
		//$this->ci->form_validation->set_rules('password', 'password', 'trim|required');
		$new_password		        = $this->ci->input->post('new_password');
		$confirm_new_password		= $this->ci->input->post('confirm_new_password');
        $this->ci->form_validation->set_rules('new_password', 'new password', 'trim|required|matches[confirm_new_password]');
        $this->ci->form_validation->set_rules('confirm_new_password', 'confirm new password', 'trim|required');
		
	}
	 
	/*
	* @description: This function is used to save user password
	*/  
	private function _userPasswordSave(){
		$updateData['id']	= isLoginUser();
		$updateData['new_password']		= $this->ci->input->post('new_password');
		//$updateData['confirm_new_password']		= $this->ci->input->post('confirm_new_password');
		$updateData['show_password']		= $this->ci->input->post('show_password');
		$response = curlCall($this->ci->config->item('newmanUrl').'api/userPasswordSave',$updateData);
		$response = json_decode($response);
	}
   
	/*
	* @description: This function is used to check and save user profile
	*/ 
	private function userProfile(){
		//get ajax request
		$is_ajax_request    = $this->ci->input->is_ajax_request();
		$return             = $this->_userProfileSave();
		$msg = lang('user_profile_updated');
		if($is_ajax_request){
			echo json_encode(array('msg'=>$msg,'is_success'=>'true','userLogo'=>$return['userLogo'], 'nick_name_user'=> $return['nick_name']));
		}else{
			$msgArray = array('msg'=>$msg,'is_success'=>'true');
			set_global_messages($msgArray);
			redirect('profile');
		}
	}
   
	/*
	* @description: This function is used to save user profile
	*/ 
	private function _userProfileSave(){

		$userLogo2  ='';
		$local_path = $this->ci->config->item('document_path')."media/user_logo/";
		$userid	    = isLoginUser();
		
		// this is new once
		$this->ci->session->set_userdata('nick_name' , ''); 
        $dataUser['newman_nickname']    = $this->ci->input->post('nick_name');
       
		if(!empty($_FILES['user_logo']['tmp_name'])){
            
            $config['upload_path']      = './media/user_logo/';
            $config['allowed_types']    = 'jpg|jpeg|png';
            $config['encrypt_name']     = true;
            $this->ci->load->library('upload', $config);
            
            if ( ! $this->ci->upload->do_upload('user_logo'))
            {
                $error = array('error' => $this->ci->upload->display_errors());
                print_r($error);die;
            }
            else
            {
                $uploadData                     = $this->ci->upload->data();   
                $dataUserDetails['userLogo']    = $uploadData['file_name'];
                
                $dataUser['userImageStatus']    = '1';
                $where = array('user_id'=>$userid);
                $result = $this->ci->common_model->getDataFromTabel('user_details','*',$where);
                
                
                if(!empty($result)){
                    $userdata = $this->ci->common_model->updateDataFromTabel('user_details',$dataUserDetails,'user_id',$userid);
                } else{
                    $dataUserDetails['user_id']     = $userid;
                    $userdata = $this->ci->common_model->addDataIntoTabel('user_details', $dataUserDetails);
                }
                
                $user_logo = base_url().'media/user_logo/'.$dataUserDetails['userLogo'];
				$this->ci->session->set_userdata('userLogo',$user_logo);
                $return_data = array('userLogo' =>$user_logo , 'nick_name' => $dataUser['newman_nickname']); 
                return  $return_data;
                
            }
        }
        
        $userdata = $this->ci->common_model->updateDataFromTabel('user',$dataUser,'id',$userid);
		$this->ci->session->set_userdata('nick_name' , $dataUser['newman_nickname']);
		$nick_name_user =  $this->ci->session->userdata('nick_name');
		
	}
  
	/*
	* @description: This function is used to check and save user social media
	*/ 
	private function userSocialMedia(){
		//get ajax request
		$is_ajax_request = $this->ci->input->is_ajax_request();
	
		$this->_userSocialMediaSave();
		$msg = lang('user_social_media_updated');
		if($is_ajax_request){
			echo json_encode(array('msg'=>$msg,'is_success'=>'true'));
		}else{
			$msgArray = array('msg'=>$msg,'is_success'=>'true');
			set_global_messages($msgArray);
			redirect('profile');
		}
	}
  
	/*
	* @description: This function is used to save user social media
	*/ 
	private function _userSocialMediaSave(){
		$updateData['id']	= isLoginUser();
		$updateData['Facebook']		= $this->ci->input->post('userFacebook');
		$updateData['Instagram']	= $this->ci->input->post('userInstagram');
		$updateData['Twitter']		= $this->ci->input->post('userTwitter');
		$updateData['Youtube']		= $this->ci->input->post('userYoutube');
		$updateData['Pintrest']		= $this->ci->input->post('userPintrest');
		$updateData['LinkedIn']		= $this->ci->input->post('userLinkedIn');
		
		$customSocialName		= $this->ci->input->post('customSocialName');
		$customSocialValue	= $this->ci->input->post('customSocialValue');
		$flag=0;
		if(!empty($customSocialName)){
			foreach($customSocialName as $label){
				$new_array[$flag+1] = array('label'=>$label,'value'=>$customSocialValue[$flag]);
				$flag++;
			}
			$updateData['customSocial'] = json_encode($new_array);
		}
		
		$response = curlCall($this->ci->config->item('newmanUrl').'api/userSocialMediaSave',$updateData);
		$response = json_decode($response);
	}
  
	/*
	* @description: This function is used to check and save user notification
	*/ 
	private function userNotification(){
		$this->_userNotificationValidate(); 
		//get ajax request
		$is_ajax_request = $this->ci->input->is_ajax_request();
		
		if ($this->ci->form_validation->run($this->ci)) {
			$this->_userNotificationSave();
			$msg = lang('user_notification_updated');
			if($is_ajax_request){
				echo json_encode(array('msg'=>$msg,'is_success'=>'true'));
			}else{
				$msgArray = array('msg'=>$msg,'is_success'=>'true');
				set_global_messages($msgArray);
				redirect('profile');
			}
		} else{
			$errors = validation_errors();
			if($is_ajax_request){
				echo json_message('msg',$errors,'is_success','false');
			}else{
				// load event form view
				$this->userView();
			}
		}
	}
     
	/*
	* @description: This function is used to check validatation of notification
	*/ 
	private function _userNotificationValidate(){    
		// set ruls for form filed
		$this->ci->form_validation->set_rules('email_alert', 'email alert', 'trim|required');
		$this->ci->form_validation->set_rules('sms_alert', 'sms alert', 'trim|required');
		$email_alert		= $this->ci->input->post('email_alert');
		if($email_alert=='yes'){
			$this->ci->form_validation->set_rules('alert_emailid', 'email', 'trim|required|valid_email');
		}
		$sms_alert		= $this->ci->input->post('sms_alert');
		if($sms_alert=='yes'){
			$this->ci->form_validation->set_rules('alert_mobile_no', 'mobile no', 'trim|required|numeric');
		}
	}
	 
	/*
	* @description: This function is used to save user notification
	*/  
	private function _userNotificationSave(){
		$updateData['id']	= isLoginUser();
		$updateData['alert_emailid']		= $this->ci->input->post('alert_emailid');
		$updateData['alert_mobile_no']		= $this->ci->input->post('alert_mobile_no');
		
		$response = curlCall($this->ci->config->item('newmanUrl').'api/userNotificationSave',$updateData);
		$response = json_decode($response);
	}
  
}

// end class


/* End of file factory.php */
/* Location: ./system/application/modules/home/libraries/factory.php */
