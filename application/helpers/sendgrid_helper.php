<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
  
  /**
   * @descrioption: All the common helper function of venue finder 
   * @auther: Krishnapal singh dhakad
   * @email: krishnapaldhakad@cndsol.com
   * @orgnisation: cdnsolution  
   * 
   **/ 
  
  /*
   * @descript: This function is used to check is user login
   * @return userid or flase
   */ 

  if ( ! function_exists('test_mail_template')){    
    function test_mail_template(){
      $CI =& get_instance();
      $userid=$CI->session->userdata('userid');
        require 'vendor/autoload.php';
        $sendgrid_apikey = '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';
        $sendgrid = new SendGrid($sendgrid_apikey);
        $url = 'https://api.sendgrid.com/';
        $pass = $sendgrid_apikey;
        $template_id = 'f825e2e9-efd1-4d80-8aa8-2cbc4c777b6d';
        $js = array(
        'sub' => array(':name' => array('Tets Account'),':customTest'=>array('Custom Check'),':twiiterLink'=>array('https://newmanvenues.com/')),
        'filters' => array('templates' => array('settings' => array('enable' => 1, 'template_id' => $template_id)))
        );

        $params = array(
        'to'        => "shailendratiwari@cdnsol.com",
        'toname'    => "Test Name ",
        'from'      => "amitneema@cdnsol.com",
        'fromname'  => "Test Mail",
        'subject'   => "Test Subject",
        'text'      => "I'm text!",
        'html'      => "<strong>I'm HTML! Local</strong>",
        'x-smtpapi' => json_encode($js),
        );

        $request =  $url.'api/mail.send.json';

        // Generate curl request
        $session = curl_init($request);
        // Tell PHP not to use SSLv3 (instead opting for TLS)
       // curl_setopt($session, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
        curl_setopt($session, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $sendgrid_apikey));
        // Tell curl to use HTTP POST
        curl_setopt ($session, CURLOPT_POST, true);
        // Tell curl that this is the body of the POST
        curl_setopt ($session, CURLOPT_POSTFIELDS, $params);
        // Tell curl not to return headers, but do return the response
        curl_setopt($session, CURLOPT_HEADER, false);
        curl_setopt($session, CURLOPT_RETURNTRANSFER, true);

        // obtain response
        $response = curl_exec($session);
        curl_close($session);
        // print everything out
        print_r($response);
    }
  }
  

/* Sendgrid Email Template 
 * @ templateId - 
 * @ Template Subject
 * @ To id
 * @ $templateBody - Required Filled ( compulsory )
 * @ Param Array {$js = array(
            'sub' => array(':name' => array('Tets Account'),':customTest'=>array('Custom Check'),':twiiterLink'=>array('https://newmanvenues.com/')),
            'filters' => array('templates' => array('settings' => array('enable' => 1, 'template_id' => $template_id)))
            );
            }
 * @ 
 * 
 * */


  if ( ! function_exists('sendgrid_mail_template')){    
    function sendgrid_mail_template($templateId= '',$tempSubject = '',$templateBody = '',$toId='',$paramArray = array(),$apiKey = '',$fromEmail = '',$fileName = '',$filePath='',$fromName=''){
      
        if(!empty($templateId) && !empty($toId)){ // 
            require 'vendor/autoload.php';
            $sendgrid_apikey = $apiKey;//'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';
            $sendgrid = new SendGrid($sendgrid_apikey);
            $url = 'https://api.sendgrid.com/';
            $pass = $sendgrid_apikey;
            $template_id = $templateId;
            $fromEmail = !empty($fromEmail)?$fromEmail:'admin@newmanevents.com';
            
            if(empty($fromName)){
                $fromName = 'Newman';
            } 
             
            if(!empty($fileName)){
                $params = array(
                'to'        => $toId,
                'from'      => $fromEmail,
                'fromname'  => $fromName,
                'html'      => $templateBody,
                'subject'   => $tempSubject,
                'x-smtpapi' => json_encode($paramArray),
                'files['.$fileName.']' => '@'.$filePath
                );
            }else{                
                $params = array(
                'to'        => $toId,
                'from'      => $fromEmail,
                'fromname'  => $fromName,
                'html'      => $templateBody,
                'subject'   => $tempSubject,
                'x-smtpapi' => json_encode($paramArray),
                );
            } 
            

            $request =  $url.'api/mail.send.json';

            // Generate curl request
            $session = curl_init($request);
            // Tell PHP not to use SSLv3 (instead opting for TLS)
           // curl_setopt($session, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
            curl_setopt($session, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $sendgrid_apikey));
            // Tell curl to use HTTP POST
            curl_setopt ($session, CURLOPT_POST, true);
            // Tell curl that this is the body of the POST
            curl_setopt ($session, CURLOPT_POSTFIELDS, $params);
            // Tell curl not to return headers, but do return the response
            curl_setopt($session, CURLOPT_HEADER, false);
            curl_setopt($session, CURLOPT_RETURNTRANSFER, true);

            // obtain response
            $response = curl_exec($session);
            //print_r($response);
            curl_close($session);
            // print everything out
            if($response){
                return $response;
            }
        }else{
            return false;
        }
        
    }
  }
  

