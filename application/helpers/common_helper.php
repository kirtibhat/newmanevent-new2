<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
  
  /**
   * @descrioption: All the common helper function of newmanevent 
   * @auther: lokendra meena
   * @email: loknedrameena@cndsol.com
   * @orgnisation: cdnsoltion  
   * 
   **/ 
  
  // ------------------------------------------------------------------------ 
  
  /*
   * @descript: This function is used to check is user login
   * @return userid or flase
   */ 

  if ( ! function_exists('isLoginUser')){    
    function isLoginUser(){
      $CI =& get_instance();
      $userid=$CI->session->userdata('userid');
      if($userid > 0){
        return $userid;
      }else{
        return false;
      }
    }
  }
  
  // ------------------------------------------------------------------------   
  
  /*
   * @descript: This function is used to open callapse form after page loading
   * @param1: $fieldname (string) 
   * @return string
   */
  
  if(!function_exists('getIsCollapseOpen')){     
      
    function getIsCollapseOpen($openCollapseValue=""){
      $CI =& get_instance();
      return ($CI->session->userdata('openCollapseValue')==$openCollapseValue) ? 'in' : '';
    } 
  }
  
  /*
   * @descript: This function is used to unset callapse value store in session
   * @param1: $fieldname (string) 
   * @return string
   */
  
  if(!function_exists('unsetCollapseValue')){    
      
    function unsetCollapseValue(){
      $CI =& get_instance();
      $CI->session->unset_userdata('openCollapseValue');
    } 
  }
  
  // ------------------------------------------------------------------------   
  
  /*
   * @descript: This function is used to get login user details
   * @param1: $fieldname (string) 
   * @return enter user details
   */
   
  
  if (!function_exists('LoginUserDetails')){     
    
    function LoginUserDetails($fieldname=''){
      $CI =& get_instance();
      
      if(!empty($fieldname)){
        $userdata = $CI->session->userdata('userdata'); 
        if(!empty($userdata->$fieldname)){
          return $userdata->$fieldname;
        }else{
          return false;
        }
        
      }else{
        return $CI->session->userdata('userdata');
      }
      
    }
  }
  
  // ------------------------------------------------------------------------   
  
  /*
   * @descript: This function is used convert date in any formate
   * @param1: $date (date)
   * @param2: $formate (string)
   * @return converted date formate
   */
   
  
  if ( ! function_exists('dateFormate')){    
    
    function dateFormate($date='', $formate="Y-m-d"){
      $date =($date=='')?date('Y-m-d'):$date;
      return date($formate,strtotime($date));
    }
  }
  
  // ------------------------------------------------------------------------   
  
  /*
   * @descript: This function is used convert date in any formate
   * @param1: $time (time)
   * @param2: $formate (string)
   * @return convert time formate
   */
   
  
  if ( ! function_exists('timeFormate')){    
    
    function timeFormate($time='', $formate="H:i:s"){
      
      $timeConvert = date($formate,strtotime($time));
      return $timeConvert;
    }
  }
  
  // ------------------------------------------------------------------------   
  
  /*
   * @descript: This function is used set single and multiple message in the session
   * @param1: $setArray (array)
   * @return void
   */
    
   
  if ( ! function_exists('set_global_messages'))
  {
    function set_global_messages($setArray)
    {
      $CI =&get_instance();
      foreach($setArray as $key => $val) {
        $CI->session->set_flashdata($key, $val);
      }
    }
  }
  
  // ------------------------------------------------------------------------   
  
  /*
   * @descript: This function is used get single message has set in the session
   * @param1: $type (string)
   * @return $msg
   */
  
  if ( ! function_exists('get_global_messages'))
  {
    function get_global_messages($type)
    {
      $str = '';
      $CI =&get_instance();
      $msg = $CI->session->flashdata($type);
      if($CI->session->flashdata($type)){
        $msg = $CI->session->flashdata($type);
      }else{
        $msg = false;
      }
      return $msg;
    }
  }
  
  // ------------------------------------------------------------------------   
  
  /*
   * @description: This function is used to convert array into json form for json message
   * @param1: $key (string)
   * @param2: $msg  (string)
   * @param3: $key1 (string)
   * @param4: $msg1  (string)
   * @return json message encoded message
   */ 
   
  function json_message($key,$value,$key1="is_success",$val1='true'){
    $getArray = array($key=>$value,$key1=>$val1);
    return json_encode($getArray);
  } 
  
  // ------------------------------------------------------------------------   
  
  /*
   * @description: This function is used to remove extension form any extension file
   * @param: $filename (string)
   * @return filename without extension
   */ 
   
  function remove_extension($filename){
    return preg_replace("/\\.[^.\\s]{3,4}$/", "", $filename);
  } 
  
  // ------------------------------------------------------------------------   
  
  /*
   * @description: This function is used to delete file from directory
   * @param: $dir (string)
   * @param: $filename (string)
   * @return void
   */ 
  
  
  if(! function_exists('findFileNDelete')){
    function findFileNDelete($dir, $filename){
       
        // $dir = $_SERVER['DOCUMENT_ROOT'].'/newmanevents'.$dir;   
        $dir = FCPATH.$dir;   
       
        if(is_dir($dir) && $filename !=''){
         
         $fpLen=strlen($dir);
        if($fpLen > 0 && substr($dir,-1) != DIRECTORY_SEPARATOR){
          $dir=$dir.DIRECTORY_SEPARATOR;
        }
         
        $ffs = scandir($dir);
        foreach($ffs as $ff){
          if($ff != '.' && $ff != '..'){
            if(is_dir($dir.$ff)){
              findFileNDelete($dir.$ff.DIRECTORY_SEPARATOR, $filename);
            }elseif(is_file($dir.$ff)){
              $fileInfo=pathinfo($filename);
              if(strstr($ff,$fileInfo['filename'])){
                unlink($dir.$ff);
              }
            }
          }
        }
      }
    }
  }
  
  // ------------------------------------------------------------------------   
  
  /*
   * @description: This function is used to get all value of master field value 
   * for personal details forms in event setup
   * @return $fieldsmastervalue
   * 
   **/ 
  
  
  if ( ! function_exists('fieldsmastervalue'))
  {
    function fieldsmastervalue()
    {
      $CI =&get_instance();
      $res = $CI->common_model->getDataFromTabel('nm_custom_fields_master_value','id,field_value_name');
      if($res){
        foreach ($res as $mastervalue) {
            $fieldsmastervalue[$mastervalue->id] = $mastervalue->field_value_name;
        }
      }
      return $fieldsmastervalue;
    }
  }
  
  // ------------------------------------------------------------------------   
  
  /*
   * @description: This function is used to get day listing between two dates
   * @param1: $startDate (date)
   * @param2: $endDate  (date)
   * @return $aryRange
   * 
   **/ 
  
  
  
  if ( ! function_exists('datedaydifference'))
  {
    function datedaydifference($startDate,$endDate)
    { 
      $aryRange=array();

      $iDateFrom=mktime(1,0,0,substr($startDate,5,2), substr($startDate,8,2),substr($startDate,0,4));
      $iDateTo=mktime(1,0,0,substr($endDate,5,2),  substr($endDate,8,2),substr($endDate,0,4));

      if ($iDateTo>=$iDateFrom)
      { 
        $day=1;
        //array_push($aryRange,date('Y-m-d',$iDateFrom)); // first entry
        $aryRange[$day]['day'] = $day; // first entry
        $aryRange[$day]['date'] = date('Y-m-d',$iDateFrom); // first entry
        while ($iDateFrom<$iDateTo)
        { 
          $day++;
          $iDateFrom+=86400; // add 24 hours
          $aryRange[$day]['day'] = $day;
          $aryRange[$day]['date'] = date('Y-m-d',$iDateFrom);
          
        }
      }
      
      return $aryRange;
    }
  }
  
  // ------------------------------------------------------------------------   
  
  /*
   * @description: This function is used to number listing array
   * @param1: $start (int)
   * @param2: $end  (int)
   * @param2: $lable  (string)
   * @return $numberArray
   * 
   **/  
   
  if ( ! function_exists('getnumber'))
  {
    function getnumber($start="0",$end="10",$lable=''){
      
      // if lable is not empty then show in return number list
      if(!empty($lable)){
        $numberArray[''] = $lable;
      }
      
      //create number array
      for($i=$start;$i<=$end;$i++){
        $numberArray[$i] = $i;
      }
      
      return $numberArray;
    }
  }
  
  // ------------------------------------------------------------------------   
  
  /*
   * @description: This function is used get data form db table by passing necessary parameter
   * @param1: $table  // table name
   * @param2: $field  // necessary field name
   * @param2: $whereField  // where field name
   * @param2: $whereValue  // where field value
   * @param2: $orderBy  // order by filed name
   * @param2: $order  // order like asc/dsec
   * @param2: $limit  // limit of record
   * @param2: $offset // offset
   * @param2: $resultInArray (boolean) // data return form array/object
   * @return $res
   * 
   **/ 
  
  if(! function_exists('getDataFromTabel')){
    function getDataFromTabel($table='', $field='*',  $whereField='', $whereValue='', $orderBy='', $order='ASC', $limit=0, $offset=0, $resultInArray=false ){
      $CI =& get_instance();
      $res =  $CI->common_model->getDataFromTabel($table, $field,  $whereField, $whereValue, $orderBy, $order, $limit, $offset, $resultInArray );
      return $res;
    }
  }
  
  
  if(! function_exists('getDataFromTabelWhereIn')){
    function getDataFromTabelWhereIn($table='', $field='*',  $whereField='', $whereValue='', $orderBy='', $order='ASC', $whereNotIn=0 ){
      $CI =& get_instance();
      $res =  $CI->common_model->getDataFromTabelWhereIn($table, $field,  $whereField, $whereValue, $orderBy, $order, $whereNotIn=0 );
      return $res;
    }
  }
    
  if(! function_exists('countRowsTable')){
    function countRowsTable($table='', $whereField=''){
      $CI =& get_instance();
      $res =  $CI->common_model->countRowsTable($table, $whereField);
      return $res;
    }
  }
  
  // ------------------------------------------------------------------------   
  
  /*
   * @description: This function is used get data form db table by passing necessary parameter
   * @param1: $table  // table name
   * @param2: $field  // necessary field name
   * @param2: $whereinField  // whereinField data
   * @param2: $whereValue  // where field value
   * @param2: $orderBy  // order by filed name
   * @param2: $whereNotIn //pass not included id
   * @return $result
   * 
   **/ 
  if(! function_exists('getDataFromTabelWhereWhereIn')){ 
  function getDataFromTabelWhereWhereIn($table='', $field='*',  $where='',  $whereinField='', $whereinValue='', $orderBy='', $whereNotIn=0){
        $CI =& get_instance();
    $result =  $CI->common_model->getDataFromTabelWhereWhereIn($table, $field,  $whereField, $whereValue, $orderBy, $order, $limit, $offset, $resultInArray );
    return $result;
      }
    }
   
  /*
   * @description: This function is used get data form db table by passing necessary parameter
   * * @param1: $table  // table name
   *   @param2: $field  // necessary field name
   *   @param2: $where  // where string
   */
   
  if(! function_exists('getDataFromTabelWhereOr')){ 
  function getDataFromTabelWhereOr($table='', $field='*',  $where='',  $orderBy='',$order=''){
        $CI =& get_instance();
    $result =  $CI->common_model->getDataFromTabelWhereOr($table, $field,  $where, $orderBy, $order);
    return $result;
      }
    }
  
  
  // ------------------------------------------------------------------------   
  
  /*
   * @description: This function is used to convert objec into array
   * $param1: $object (object)
   * return $array 
   * 
   */ 
  if(! function_exists('objecttoarray')){ 
    function objecttoarray($object){
      $returnArray = array();
      $array =  (array) $object;
      
      foreach($array as $value){
         $returnArray[] = $value; 
      }
      
      return $returnArray;
    }
  }
  
  // ------------------------------------------------------------------------   
  
  /*
   * @description: This function is used to convert single number to two number
   * @param1: $number (int) 
   * return array 
   * 
   */ 
  if(! function_exists('singlenumbertwo')){  
    function singlenumbertwo($number){
      
      if(strlen($number)==1){
        $number = '0'.$number;
      }
      return $number;
    }
  }
  
  // ------------------------------------------------------------------------   
  
  /*
   * @description: This function is used to show language content by passing key
   * @param1: $langKey (string) 
   * return array 
   * 
   */ 
  
  if(! function_exists('lang')){  
    function lang($langKey=''){
      $CI =& get_instance();
      return $CI->lang->line($langKey);
    }
  }
  
  // ------------------------------------------------------------------------   
  
  /*
   * @description: This function is used to genrate random number  by passing digit
   * @param1: $digits (number) 
   * return number 
   * 
   */ 
  
  if(! function_exists('randomnumber')){ 
    function randomnumber($digits='3'){
      return rand(pow(10, $digits-1), pow(10, $digits)-1);
    }
  }
  
  // ------------------------------------------------------------------------   
  
  /*
   * @description: This function is used to get registrant fields data
   * @param1: $registrantUserId 
   * return registrantFieldsValue 
   * 
   */ 
  
  if(! function_exists('getRegistrantFields')){
    function getRegistrantFields($registrantUserId){
      $CI =& get_instance();
      $CI->load->model('report/report_model');
      
      $where = array('registered_user_id'=>$registrantUserId);
      $getRegistrantFields = getDataFromTabel('frnt_form_field_value','field_id',$where);
      $arrayField = '';
      $FieldsId = '';
      $returnFields = '';
      
      if(!empty($getRegistrantFields)){
        foreach($getRegistrantFields as $getRegistrant){
          $arrayField[] = $getRegistrant->field_id;
        }
      }
      
      //get all fields data 
      if(!empty($arrayField)){
        $fieldResult =  $CI->report_model->getformfieldid($arrayField);
        if(!empty($fieldResult)){
          foreach($fieldResult  as $fieldValue){
            //if($fieldValue->field_name=='first name' || $fieldValue->field_name=='email' ){
              $FieldsId[$fieldValue->field_name] = $fieldValue->id;
            //}
          }
        }
      }
      
      //get all fields value data 
      if(!empty($FieldsId)){
        $fieldValueResult =  $CI->report_model->getformfieldvalue($FieldsId,$registrantUserId);
        
        if(!empty($fieldValueResult)){
          foreach($fieldValueResult as $fieldValue){
            $returnFields[] = $fieldValue->value;
          }
        }
      }
      
      return $returnFields;
    }
  }
  
  
  
  // ------------------------------------------------------------------------   
  
  /*
   * @description: This function is get and update master number for operand, event and registrant 
   * @param1: section "operand, event, registrant" 
   * @return mater number  
   * 
   */ 
  
  function getMasterNumber($section,$eventId="0"){
    
    $CI =& get_instance();
    $CI->load->library(array('booking_number'));
    
    switch($section){
      
      //manager organiser/operand number 
      case "operand":
      
        $masterNumber = NULL;
        $where = array('id'=>'1');
        $getData = getDataFromTabel('master_number_manage',$section,$where);
        if(!empty($getData)){
          $getData = $getData[0];
          $masterNumber = $getData->$section;
          
          //get next master number
          $nextMasterNumber = $CI->booking_number->bookingnumber($masterNumber);
          
          //udpate master number
          $upWhere = array('id'=>'1');
          $updateData = array($section=>$nextMasterNumber);
          $CI->common_model->updateDataFromTabel('master_number_manage',$updateData,$upWhere);
        }
      break;
      
      //manager event number from user table
      case "event":
        
        $masterNumber = NULL;
        $userId    =  isLoginUser();
        $where = array('id'=>$userId);
        $getData = getDataFromTabel('user','master_event_id',$where);
        if(!empty($getData)){
          $getData = $getData[0];
          $masterNumber = $getData->master_event_id;
          
          //get next master number
          $nextMasterNumber = $CI->booking_number->bookingnumber($masterNumber);
          
          //udpate master event number in user table
          $upWhere = array('id'=>$userId);
          $updateData = array('master_event_id'=>$nextMasterNumber);
          $CI->common_model->updateDataFromTabel('user',$updateData,$upWhere);
        }
      break;
      
      //manage registrant number from event table
      case "registrant":
      
        $masterNumber = NULL;
        $where = array('id'=>$eventId);
        $getData = getDataFromTabel('event','master_registrant_id',$where);
        if(!empty($getData)){
          $getData = $getData[0];
          $masterNumber = $getData->master_registrant_id;
          
          //get next master number
          $nextMasterNumber = $CI->booking_number->bookingnumber($masterNumber);
          
          //udpate master registrant number in event table
          $upWhere = array('id'=>$eventId);
          $updateData = array('master_registrant_id'=>$nextMasterNumber);
          $CI->common_model->updateDataFromTabel('event',$updateData,$upWhere);
        }
      break;
      
      //manage registration number from event table
      case "registration":
      
        $masterNumber = NULL;
        $where = array('id'=>$eventId);
        $getData = getDataFromTabel('event','master_registration_id',$where);
        if(!empty($getData)){
          $getData = $getData[0];
          $masterNumber = $getData->master_registration_id;
          
          //get next master number
          $nextMasterNumber = $CI->booking_number->bookingnumber($masterNumber);
          
          //udpate master registrantion number in event table
          $upWhere = array('id'=>$eventId);
          $updateData = array('master_registration_id'=>$nextMasterNumber);
          $CI->common_model->updateDataFromTabel('event',$updateData,$upWhere);
        }
      
      break;
      
    }
    
    
    return $masterNumber;
  }
  
  //-----------------------------------------------------------------------
  
  /*
   * @description: This function is used to get active menu class and menu is enable 
   * @param1: menuName
   * return status
   */  
  
  function getActiveMenu($menuName=''){
  
    $showClass = '';
    $CI     = & get_instance();
    $className  = $CI->router->fetch_class();
    $methodname = $CI->router->fetch_method();
    
    switch($className){
    
      case "event":
      case "report":
      case "admin":
        if($className==$menuName){
          $showClass = 'active';
        }elseif($className==$menuName){
          $showClass = 'active';
        }elseif($className==$menuName){
          $showClass = 'active';
        }
      break;
      
      default:
      $showClass = 'dash_menu_disable cursor_none inactive';
    }
    
    return $showClass ;
  }
  
  
  //-----------------------------------------------------------------------
  
  /*
   * @description: This function is used to show registrant section name
   * @param1: sectionId
   * return registrant section name
   */  
  
  function getRegistrantSectionName($categoryId){
    
      $eventId=currentEventId();
      //decode entered eventid
      $eventId = decode($eventId); 
      
      $CI =& get_instance();
      
      $result = $CI->common_model->getDataFromTabel('event_categories','category',array('category_id'=>$categoryId,'event_id'=>$eventId));
      
      return (!empty($result)) ? $result[0]->category : "" ;
      /*
      switch($sectionId){
      
        case "0":
          $sectionName = 'Main registrant';
        break;
        
        case "1":
          $sectionName = 'Side event';
        break;
        
        case "2":
          $sectionName = 'Sponsor';
        break;
        
        case "3":
          $sectionName = 'Exhibitor';
        break;
        
        default:
        $sectionName   = 'Main registrant';
      }
      */
    
  }

  
  // ------------------------------------------------------------------------ 
  
  /*
   * @descript: This function is used to get current open event id (use in backend/dashboard)
   * @return eventid or flase
   */ 

  if ( ! function_exists('currentEventId')){     
    function currentEventId(){
      $CI =& get_instance();
      $eventId=$CI->session->userdata('eventId');
      if(!empty($eventId)){
        return $eventId;
      }else{
        return false;
      }
    }
  }
  
  
  // ------------------------------------------------------------------------   
  
  /*
   * @description: This function is used to get registrant fields data
   * @param1: $registrantUserId 
   * return registrantFieldsValue 
   * 
   */ 
  
  if(! function_exists('getRegistrantAllFields')){
    function getRegistrantAllFields($registrantUserId){
      $CI =& get_instance();
      $CI->load->model('report/report_model');
      
      $where = array('registered_user_id'=>$registrantUserId);
      $getRegistrantFields = getDataFromTabel('frnt_form_field_value','field_id',$where);
      $arrayField = '';
      $FieldsId = '';
      $returnFields = '';
      
      if(!empty($getRegistrantFields)){
        foreach($getRegistrantFields as $getRegistrant){
          $arrayField[] = $getRegistrant->field_id;
        }
      }
      
      //get all fields data 
      if(!empty($arrayField)){
        $fieldResult =  $CI->report_model->getformfieldid($arrayField);
        if(!empty($fieldResult)){
          foreach($fieldResult  as $fieldValue){
            //if($fieldValue->field_name=='first name' || $fieldValue->field_name=='email' ){
              $FieldsId[$fieldValue->field_name] = $fieldValue->id;
            //}
          }
        }
      }
      
      //get all fields value data 
      if(!empty($FieldsId)){
        $fieldValueResult =  $CI->report_model->getformfieldallvalue($FieldsId,$registrantUserId);
        
        if(!empty($fieldValueResult)){
          foreach($fieldValueResult as $fieldValue){
            $returnFields[$fieldValue->field_name] = $fieldValue->value;
          }
        }
      }
      
      return $returnFields;
    }
  }
  
  // ------------------------------------------------------------------------   
  
  /*
   * @description: This function is used to serach value by specific in array
   * @param1: $array 
   * @param1: $key 
   * @param1: $value 
   * return searched array 
   * 
   */
  
  if(! function_exists('search')){
    function search($array, $key, $value)
    {
      $results = array();

      if (is_array($array)) {
        if (isset($array[$key]) && $array[$key] == $value) {
          $results[] = $array;
        }

        foreach ($array as $subarray) {
          $results = array_merge($results, search($subarray, $key, $value));
        }
      }

      return $results;
    }
  }
  
  
  
  // ------------------------------------------------------------------------   
  
  /*
   * @description: This function is used to get report name lable by report type
   * @param1: $array 
   * @param1: $key 
   * @param1: $value 
   * return searched array 
   * 
   */
  
  if(! function_exists('getReportNameLable')){
    function getReportNameLable($typeOfReport)
    {
      $reportName = "";
      switch($typeOfReport){
        case "fullReport";
          $reportName = "Full Registration Report";
        break;
        case "breakoutReport";
          $reportName = "Breakout Report ";
        break;
        case "registrationReport";
          $reportName = "Registration Report";
        break;
        case "sponsorshipReport";
          $reportName = "Sponsorship Report";
        break;
        case "sideeventReport";
          $reportName = "Sideevent Report";
        break;
        case "exhibitorReport";
          $reportName = "Exhibitor Report";
        break;
        
        
      }
      
      return $reportName;
    }
  }
  
  
  
  // ------------------------------------------------------------------------   
  
  /*
   * @description: This function is used to get acronyms of a phrase of string
   * @param1: $string 
   * return acronyms string
   */
   
  if(! function_exists('getAcronyms')){
    function getAcronyms($string){
      if(preg_match_all('/\b(\w)/',strtoupper($string),$m)) {
         $string = implode('',$m[1]); // $v is now SOQTU
      }  
      return  $string;
    }
  }
  
  
  /*
   * @description: This function is used to remove spaces with single character from string
   * @param1: $string 
   * return remove spaces string
   */
   
  if(! function_exists('replaceSpaceWithCharacter')){
    function replaceSpaceWithCharacter($string,$replaceBy){
      $string=str_replace(" ","_",$string);
      return  $string;
    }
  }
  
  // ------------------------------------------------------------------------ 
  
  /*
   * @descript: This function is used to get current open event details (use in backend/dashboard)
   * @param: eventname/ful
   * @return eventid or flase
   */ 

  if ( ! function_exists('getCurrentEventDetails')){     
    function getCurrentEventDetails($data='full'){
    
      $eventId=currentEventId();
      
      //decode entered eventid
      $eventId = decode($eventId); 
    
      if(!empty($eventId)){
        
        $where = array('id'=>$eventId);
        $getEventDetails = getDataFromTabel('event','*',$where);
        
        //get event details
        if(!empty($getEventDetails)){
          
          
          if($data=="eventname"){
            //sign multiple multidimentiant single varible
            $getEventDetails = $getEventDetails[0];
            
            //get event name by condition
            $shortEventName = (!empty($getEventDetails->short_form_title))?$getEventDetails->short_form_title:$getEventDetails->event_title;
            //get event serial number
            $eventNumber = (!empty($getEventDetails->event_number))?$getEventDetails->event_number:'';
            //get operand number
            $operandNumber= LoginUserDetails('operand_number');
            //prepare main event number concanate with event id
            $eventMainNumber = $operandNumber.'-'.$eventNumber;
            
            $getEventDetails = $shortEventName.' ('.$eventMainNumber.')';
            
          }else{
            $getEventDetails = $getEventDetails[0];
          }
        }
        
        return $getEventDetails;
        
      }else{
        return false;
      }
    }
  }
  
  /*
   * @descript: This function is used to get event categories
   * @return object
   */
   
   if ( ! function_exists('getEventCategories')){    
    function getEventCategories(){
      $eventId=currentEventId();
      //decode entered eventid
      $eventId = decode($eventId); 
      
      $CI =& get_instance();
      $CI->load->model('event/event_model');
      $result = $CI->event_model->geteventCategories($eventId);
      return $result;
    }
  }   
  
  /*
   * @descript: This function is used to get event Corporate categories
   * @return object
  */
  
  if(!function_exists('getEventCorporateCategory')){
     function getEventCorporateCategory(){
      $eventId=currentEventId();
      //decode entered eventid
      $eventId = decode($eventId); 
      
      $CI =& get_instance();
      $CI->load->model('event/event_model');
      $result = $CI->event_model->getEventCorporateCategory($eventId);
      return $result;
     }
  }
  
  /*
   * @descript: This function is used to get event Corporate categories
   * @return object
  */
  
  if(!function_exists('getCorporatePurchaseItem')){
     function getCorporatePurchaseItem($packageId=""){
      $eventId=currentEventId();
      //decode entered eventid
      $eventId = decode($eventId); 
      
      $CI =& get_instance();
      $CI->load->model('event/event_model');
      $result = $CI->event_model->getCorporatePurchaseItem(array('event_id'=>$eventId),$packageId);
      return $result;
     }
  }
  
  /*
   * @descript: // get Already reserved booths for other packages for the same event id
   * @return object
  */
  
  if(!function_exists('getReservedBoothForOtherPackage')){
     function getReservedBoothForOtherPackage($packageId=""){
      $eventId=currentEventId();
      //decode entered eventid
      $eventId = decode($eventId); 
      $booths = array();
      $CI =& get_instance();
      $CI->load->model('event/event_model');
      $result = $CI->event_model->getReservedBoothForOtherPackage(array('event_id'=>$eventId),$packageId);
      if(!empty($result)){
        foreach($result as $value){
        $booths[] = $value->booth_position;
        }//end loop
      } // end if
      return $booths;
     
     }
  }
  
  /*
   * @descript: This function is used to get event Corporate space details
   * @return object
  */
  
  if(!function_exists('getExhibitionSpaceDetails')){
     function getExhibitionSpaceDetails($where=array()){
    
      $CI =& get_instance();
      $CI->load->model('event/event_model');
      $result = $CI->event_model->getExhibitionSpaceDetails($where);
      return $result;
     }
  }
  
  
  /*
   * @descript: This function is used to get event categories
   * @return object
   */
   
   if ( ! function_exists('getCorporateSpaceType')){     
    function getCorporateSpaceType(){
      $eventId=currentEventId();
      //decode entered eventid
      $eventId = decode($eventId); 
      
      $CI =& get_instance();
      $CI->load->model('event/event_model');
      $result = $CI->event_model->getCorporateSpaceType(array('event_id'=>$eventId));
      
      return $result;
  
    }
  }
  
  /*
   * @descript: This function is used to get toal space remaining for selected event
   * @return int
   */
   
    if ( ! function_exists('getCorporateRemainingSpacesAvailable')){     
    function getCorporateRemainingSpacesAvailable(){
      
      $eventId=currentEventId();
      //decode entered eventid
      $eventId = decode($eventId); 
      
      $totalSpaceAvailable=0;$usedSpaceAvailable=0;
      
      $CI =& get_instance();
      
      // get total floor plan details for this event
      $floorPlanDetails = $CI->common_model->getDataFromTabel('corporate_floor_plan','total_space_available,is_exhibition_space',array('event_id'=>$eventId));
        
        if(!empty($floorPlanDetails)){
        $totalSpaceAvailable = $floorPlanDetails[0]->total_space_available;
      }
        
        $sumFloorPlanSpace = $CI->common_model->getSum('corporate_floor_plan_type','total_space_available',array('event_id'=>$eventId));
        
        if(!empty($sumFloorPlanSpace)){
        $usedSpaceAvailable = $sumFloorPlanSpace[0]->total_space_available;
      }
        
        return ($totalSpaceAvailable-$usedSpaceAvailable);
        
    }
  }
  
  
  /*
   * @descript: This function is used to get event package benefit
   * @return object
   */
   
   if ( ! function_exists('getCorporatePackageBenefit')){    
    function getCorporatePackageBenefit($packageId=""){
      $eventId=currentEventId();
      //decode entered eventid
      $eventId = decode($eventId); 
      
      $CI =& get_instance();
      $CI->load->model('event/event_model');
      $result = $CI->event_model->getCorporatePackageBenefit(array('event_id'=>$eventId),$packageId);
      return $result;
    }
  }
  
  
  /*
   * @description: This function is used to change the email format (like n********@g*****.com)
   * @param1: $string (email ID)
   * return $string (Secure email format)
   */
   
  if(! function_exists('changeEmailIdFormat')){
  function changeEmailIdFormat($string){
      
      $splitString = explode("@",$string);
      $firstchar = substr($splitString[0],0,1);
      $str = $firstchar;
      for($i=1; $i< strlen($splitString[0]);$i++)
      {
        $str .= "*";
      }
      
      // last char after .
      $pos = strpos($splitString[1], ".");
      $lastChars = substr($splitString[1],$pos,strlen($splitString[1]));
      
      
      // first char after @
      $firstchar1 = substr($splitString[1],0,1);
      $str .= "@".$firstchar1;
      for($i=1; $i< $pos;$i++)
      {
        $str .= "*";
      }
      
      return $str.$lastChars;
      
    }
  }
  
  /*
   * @description: This function is used to convert array obj to an single array
   * @param1: $data (Array of object)
   * $key : value access key 
   */
   
  /*
  if(! function_exists('convertObjtoArray')){
    function convertObjtoArray($data,$key){
      $returnArray = array();
      if(!empty($data)){
        foreach($data as $value){
           $returnArray[] =  $value->$key;
         }
      }
      return $returnArray;
    } // end function
  } // end if
  */
  
   function getSavedThemeDetails($themeId,$eventId){
      $CI =& get_instance();
      // get total floor plan details for this event
      $event_themes_data = $CI->common_model->getDataFromTabel('event_themes_data','',array('event_id'=>$eventId, 'theme_id'=>$themeId));
      $event_themes_data = $event_themes_data[0];
      return ($event_themes_data);
        
    }
    
    function getFontFamily($fontValue){
      $CI =& get_instance();
      // get total floor plan details for this event
      $event_themes_data = $CI->common_model->getDataFromTabel('font_packages','',array('id'=>$fontValue));
      $event_themes_data = $event_themes_data[0];
      return ($event_themes_data);
        
    }
    
      
 function curlCall($url,$post=array()) {
  $ch = curl_init($url);
	$str = http_build_query($post);
 	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_URL,$url);
  //curl_setopt($ch, CURLOPT_POST, true);                                                                     
  curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
	curl_setopt($ch, CURLOPT_POSTFIELDS, $str);                                                                  
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
	$result=curl_exec($ch);
  curl_close($ch);
	return $result;
	//return json_decode($result);
	
 }

function getUserDataById($user_id){
    $CI =& get_instance();
    $CI->db->select('*');
    $CI->db->from('nmv_venue_user');
    $CI->db->join('nmv_venue_user_details', 'nmv_venue_user_details.user_id = nmv_venue_user.id');
    $CI->db->where('id',$user_id);
    $query = $CI->db->get();
    return $query->result(); 
}


function getDataByParam($select='*',$where=array()){
    $CI =& get_instance();
    $CI->db->select($select);
    $CI->db->from('custom_form_fields');
    $CI->db->where($where);
    $query = $CI->db->get();
    //echo $CI->db->last_query();die;
    return $query->row(); 
}

function countColumnValue($table,$where,$column){
    $CI =& get_instance();
    $CI->db->select_sum($column);
    $CI->db->from($table);
    $CI->db->where($where);
    $query = $CI->db->get();
    //echo $CI->db->last_query();die;
    return $query->row();
}


  if ( ! function_exists('make_clickable')){    
    function make_clickable($text,$textColorCss) {
   $hypertext = preg_replace(
    '{\b(?:http://)?(www\.)?([^\s]+)(\.com|\.org|\.net)\b}mi',
    '&nbsp;<u><a style="'.$textColorCss.'" href="http://$1$2$3">$1$2$3</a></u>',
    $text
    );
    return $hypertext;
    }
  }
