<?php

if (!function_exists('exportPDF')) {

    function exportPDF($htmView, $fileName) {

        $CI = & get_instance();
        $CI->load->library('MPDF/mpdf');
        
        
       // $pdfSize = array('174.62', '174.62'); //Height & Width in MM
		$mpdf = new mPDF('utf-8', 'A2', 0, '', 12, 12, 10, 10, 5, 5);
		
		$mpdf->SetAuthor('newmanevents.com'); 
		//$mpdf->PDFX = true;
		$mpdf->PDFAauto = true;
		$mpdf->PDFXauto = true;
		$mpdf->WriteHTML($htmView);
		$mpdf->Output($fileName, 'I');
		//var_dump($mpdf->PDFX);die;
		$mpdf->Output('media/'.$fileName, 'F');
		
        
        
    }

} 
