<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
	
	/**
	 * @descrioption: This helper only  for frontend
	 * @auther: rajendra patidar
	 * @email:  rajendrapatidar@cndsol.com
	 * @orgnisation: cdnsoltion  
	 * 
	 **/ 
	 /*************************************Start******************************************************/
	 
	 
	/*
	 * @descript: This function is used to check general user login
	 * @return user_id or flase
	 */ 

	if ( ! function_exists('isLoginGeneralUser')){		 
		function isLoginGeneralUser(){
			$CI =& get_instance();
			$userid=$CI->session->userdata('user_id');
			
			if($userid > 0){
				return $userid;
			}else{
				return false;
			}
		}
	}
	
	 /*
	 * @descript: This function is used to check is user login
	 * @return userid or flase
	 */ 

	/*if ( ! function_exists('isLoginSponsor')){		 
		function isLoginSponsor(){
			$CI =& get_instance();
			$userid=$CI->session->userdata('sponsorid');
			if($userid > 0){
				return $userid;
			}else{
				return false;
			}
		}
	}*/
	
	 /*
	 * @descript: This function is used to check is user login
	 * @return userid or flase
	 */ 

	if ( ! function_exists('isLoginExhibitor')){		 
		function isLoginExhibitor(){
			$CI =& get_instance();
			$userid=$CI->session->userdata('exhibitorid');
			if($userid > 0){
				return $userid;
			}else{
				return false;
			}
		}
	}
	 
	// ------------------------------------------------------------------------ 
	
	/*
	 * @descript: This function is used to get event id for login user
	 * @return ecent_id or flase
	 */ 

	if(! function_exists('getEventId')){		 
		function getEventId($userid='user_id'){
			
			$CI =& get_instance();
			
			$userid=$CI->session->userdata($userid);
			
			if($userid > 0){
				 
				$eventId=$CI->session->userdata('event_id');
				if(!empty($eventId))
				{
					return $eventId;
				}
				 return false;
				
			}else{
				return false;
			}
		}
	}
	
		//..........................................................................................................

    /*
    * @description: This function is used to send email 
    * @param  :$from,$to      
    * @return : 
    */
    if ( ! function_exists('sendMail')){
		
		function sendMail($from,$to,$subject='',$emailData='')
		{
			$CI =& get_instance();
			$CI->load->library('email');
			$CI->email->from($from);
			$CI->email->to($to);
			$CI->email->subject($subject);
		
			$CI->email->message($CI->load->view('common/register_email_template',$emailData,true));
			$CI->email->send(); 

			return true;
		}
	}
	
	
	
	//..........................................................................................................

	/*
    * @description: This function is used to calcualte registration fees
    * @param      : registrantId     
    * @return     : registrant fess and gst array for registrant
    */
     	 
    if ( ! function_exists('RegistrationFees')){
		
		function RegistrationFees($registrantId)
		{
			 $feesArray=array();
			 $feesArray['fees']='0';
			 $feesArray['gst']='0';
			 $feesArray['pay_by']='0'; 
			 return $feesArray;

		/*	$feesArray=array();
			$CI =& get_instance();
			$eventId=getEventId();
			//to get payment option for event
			$paymentFees=$CI->common_model->getDataFromTabel('payment_registration_fee','*',array('event_id'=>$eventId));

			if(!empty($paymentFees))
			{
				$typeRegistrant=$paymentFees[0]->type_of_registrant;
				$typeOfRegistrant=json_decode($typeRegistrant,true);
				if(!empty($typeOfRegistrant))
				{
					if(in_array($registrantId,$typeOfRegistrant))
					{
						if(isset($paymentFees) && !empty($paymentFees))
						{
							if(!$paymentFees[0]->this_fee_included_in_registration_cost)
							{
								if($paymentFees[0]->who_will_pay_fee=='registrant')
								{
									 //to add registrant fees & gst
									 $feesArray['fees']=$paymentFees[0]->total_price_per_registrant;
									 $feesArray['gst']=$paymentFees[0]->gst;
									 $feesArray['pay_by']=$paymentFees[0]->who_will_pay_fee;
								}
							}
							else if($paymentFees[0]->who_will_pay_fee=='organiser')
							{
								 //to minus organiser fees
								 $feesArray['fees']=-($paymentFees[0]->total_price_per_registrant);
								 $feesArray['gst']=-($paymentFees[0]->gst);
								 $feesArray['pay_by']=$paymentFees[0]->who_will_pay_fee;
							}
							 
						}
					}
				}
			
			} */
			
			 
		}
	}
	
	//........................................................................................................
	/* @description: function to check breakout limit for registrant
	 * @#param: breakoutId
     * @return: enum
     */
     
	if ( ! function_exists('checkbreakoutstreamlimit'))
	{
	
		function checkbreakoutstreamlimit($sessionId,$streamId)
		{
			
			 $CI =& get_instance();
			 $user_id=$CI->session->userdata('user_id'); 
			 //to get eventId
			 $eventId=$CI->session->userdata('event_id');
			 
			 //get paid frnt registrant id
			 $frntRegistrantId=$CI->frontend_model->paidRegistrantIdArray();
		    
			if(!empty($frntRegistrantId))
			{
				
				 // to get session limit by session id
				 $streamDetails=$CI->common_model->getDataFromTabel('breakout_stream','block_limit',array('id'=>$streamId));
				 
				if(!empty($frntRegistrantId) && !empty($streamDetails))
				{
					foreach($frntRegistrantId as $frntregistrant)
					{
						 $countBreakoutStream=0;
						 $breakoutFrntDetails=$CI->common_model->getDataFromTabel('frnt_breakouts','stream_session_id',array('registered_user_id'=>$frntregistrant));
						
						if(!empty($breakoutFrntDetails))
						{
							foreach($breakoutFrntDetails as $frntbreakout)
							{
								//get stream session details by stream_session id
								$where=array('id'=>$frntbreakout->stream_session_id);
								$breakoutAboutDetails=$CI->common_model->getDataFromTabel('breakout_stream_session','breakout_stream_id,breakout_session_id',$where);
								
								if(!empty($breakoutAboutDetails))
								{
									if($breakoutAboutDetails[0]->breakout_stream_id==$streamId)
									{
									   $countBreakoutStream=+1;
									}
								}
								 
							}//end inner foreach
						} 
						   // check breakout block limit for this event
						   if($countBreakoutStream>0 && $countBreakoutStream==$streamDetails[0]->block_limit)
							{
							   return false;
							}
					} //end of the outer loop
				} 
			}
			return true; 
		}
	}
	
		
	//........................................................................................................
	/* @description: function is used to get session details 
	 * @#param: streamId
     * @return: sessionDetails array
     */
     
	if ( ! function_exists('getSessionDetails'))
	{
		
		function getSessionDetails($streamId)
		{
			$CI =& get_instance();
			$CI->db->select('breakout_session.*,breakout_stream_session.*');
			$CI->db->from('breakout_session');
			//$this->db->from('breakout_stream_session');
			$CI->db->join('breakout_stream_session','breakout_session.id =breakout_stream_session.breakout_session_id','left');
			$CI->db->where('breakout_session.stream_id',$streamId);
			//$this->db->where('tfpd.id',$paymentId);
			$query = $CI->db->get();
			$result= $query->result();
			//echo $CI->db->last_query();
			
			return $result;
		}
	}
	
	/*
	 * @descript: This function is used to get session interval for stream
	 * @return  : array for stream 
	 * @param   : @void
	 */ 
	  
	if ( ! function_exists('getStreamSessionInterval'))
	{
		function getStreamSessionInterval($breakoutId)
		{
			$CI =& get_instance();
			$sessionIntervals=$CI->common_model->getDataFromTabel('breakout_session','start_time,end_time',array('breakout_id'=>$breakoutId));
			$startTime='0';
			$endTime='0';
			$resultInterval='0';
			$timeIntervalArray=array();
			if(!empty($sessionIntervals))
			{
				
				foreach($sessionIntervals as $session)
				{
			
					if($startTime=='0' || $startTime>strtotime($session->start_time))
					{
						$startTime=strtotime($session->start_time);
						
					}
					if($endTime<strtotime($session->end_time))
					{
						$endTime=strtotime($session->end_time);
					}
					//to calculate time interval
				
					$timeInterval=strtotime($session->end_time)-strtotime($session->start_time);	
				
					if($resultInterval=='0' || $timeInterval<$resultInterval)
					{
						$resultInterval=$timeInterval;
					} 
				}
			}
			 
			for($time=$startTime; $time<=$endTime;)
			{
				$timeIntervalArray[]=date('H:i',$time);
				$time=$time+$resultInterval;	
			}
			 $timeDiff=$time-$endTime;
			
			 if($timeDiff<$resultInterval)
			 {
				 $timeIntervalArray[]=date('H:i',$time);
			 }
			 
			$brkSessionInterval=array();
			$rowCount=0;
			$nextCount = 1;
			if(!empty($timeIntervalArray)){
				
				foreach($timeIntervalArray as $timeInterv){
						
					if(isset($timeIntervalArray[$nextCount])){
						$brkSessionInterval[]['start_date'] = $timeInterv;
						$brkSessionInterval[$rowCount]['end_date'] = $timeIntervalArray[$nextCount];
					}
							
					$rowCount++;	
					$nextCount++;	
				}
			}
			return $brkSessionInterval;
		}
	}
		
	
	// ------------------------------------------------------------------------ 
	
	/*
	 * @descript: This function is used to get current open event id (use in frontend )
	 * @return eventid or flase
	 */ 

	if ( ! function_exists('eventId')){		 
		function eventId(){
			$CI =& get_instance();
			$eventId=$CI->session->userdata('eventId');
			if(!empty($eventId)){
				return $eventId;
			}else{
				return false;
			}
		}
	}
	
	
	/*
	 * @descript: This function is used to check is user login in sponsor section
	 * @return userid or flase
	 */ 
	if ( ! function_exists('frntUserLogin')){
		function frntUserLogin($userType){
			$CI =& get_instance();
			$userid=$CI->session->userdata('userid');
			$userTypeId=$CI->session->userdata('usertypeid');			
			$returnUserId = false;
			
			switch($userType){
					
					case "frantendevent":
						$configUserTypeId = $CI->config->item('registrant_user_type');
						if($userid > 0 && $configUserTypeId == $userTypeId){
							return $userid;
						}
					break;				
					case "sponsor":
						$configUserTypeId = $CI->config->item('sponsor_user_type');
						if($userid > 0 && $configUserTypeId == $userTypeId){
							return $userid;
						}
					break;				
					case "exhibitor":
						$configUserTypeId = $CI->config->item('exhibitor_user_type');
						if($userid > 0 && $configUserTypeId == $userTypeId){
							return $userid;
						}
					break;				
					case "sideevent":
						$configUserTypeId = $CI->config->item('side_event_user_type');
						if($userid > 0 && $configUserTypeId == $userTypeId){
							return $userid;
						}
					break;				
				}
				return $returnUserId;
			
			
		}
	}
	
