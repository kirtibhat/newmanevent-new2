<?PHP if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*------------common for all section language---------------*/
$lang['comm_cancle']      = "Cancel";
$lang['comm_send']        = "Send";
$lang['comm_save']        = "Save";
$lang['comm_save_changes']    = "Save";
$lang['comm_reset']       = "Clear";
$lang['comm_submit']      = "Submit";
$lang['comm_close']       = "Close";
$lang['comm_ok']        = "Ok";
/*------------header and main menu section---------------*/
$lang['comm_my_account']      = "My Account";
$lang['comm_my_profile']      = "My Profile";
$lang['comm_my_billing']      = "Billing";
$lang['comm_my_account_filling']  = "Account Users";
$lang['comm_my_event_calendar']   = "Event Calendar";
$lang['comm_notification']    = "Notifications";
$lang['comm_help']        = "Help";
$lang['comm_logout']      = "Logout";
$lang['comm_welcome']       = "Welcome";
//$lang['comm_home']        = "Home";
$lang['comm_home']        = "My<br>Events";
$lang['comm_event']       = "Event<br>Dashboard";
$lang['comm_reports']       = "Reports";
$lang['comm_event_resources']   = "Event<br>Resources";
$lang['comm_admin']       = "Admin";
$lang['comm_my_account']    = "My Account";
$lang['comm_quick_list']    = "Quick <br>List";


/*---------submenu listing of report------------*/
$lang['comm_delegate_report']      = "Delegate Reports";
$lang['comm_financial_report']     = "Financial Reports";

/*---------submenu listing of admin------------*/
$lang['comm_registrant_manager']   = "Registrant Manager";
$lang['comm_refunds']          = "Refunds";
$lang['comm_payments_transfers']   = "Payments & Transfers";
$lang['comm_corporate_request']    = "Corporate Request";
$lang['comm_funds_transfer']     = "Funds Transfer & Refunds";
$lang['comm_modify_booking']     = "Modify Bookings";
$lang['comm_test_register']      = "Test Register";
$lang['comm_preview_forms']      = "Preview Forms";
$lang['comm_your_event']       = "Your Event Calendar";

/*------------footer section---------------*/
$lang['comm_terms_condi'] = "Terms & conditions";
$lang['comm_privacy_policy'] = "Privacy policy";

/*------------event setup file drop title----------*/
$lang['comm_file_upload_drop'] = "Select or Drop File";

/*-----------event text common use in event setup-------------*/
$lang['comm_next']        = "Next";
$lang['comm_back']        = "Back";
$lang['comm_previous']      = "Previous";
$lang['comm_preview_form']    = "Preview Form";
$lang['comm_delete']      = "Delete";
$lang['comm_edit_title']    = "Edit Title";
$lang['comm_dulicate']      = "Duplicate";
$lang['comm_yes']       = "Yes";
$lang['comm_no']        = "No";
$lang['comm_limit']       = "Limit";
$lang['comm_preview_msg'] = "Coming Soon...";
$lang['comm_top'] = "Top";

// validation messages
$lang['common_field_required']      = "This field is required";
$lang['valid_url_required']       = "Please enter valid url";
$lang['enter_numeric_value']      = "Please enter numeric value only.";
$lang['basic_information'] = "Basic Information";
$lang['information_bar'] = "The Access ID# is a unique identifier assigned to every registrant using this site. Use it to access, edit, cancel, pay and rebook items in your registration.";
$lang['first_name'] = "First Name";
$lang['last_name'] = "Last Name";
$lang['organisation'] = "Organisation";
$lang['email_address'] = "Email Address";
$lang['save'] = "Save";
$lang['clear'] = "Clear";

$lang['password'] = "Password";
$lang['current_password'] = "Current Password";
$lang['new_password'] = "New Password";
$lang['confirm_new_password'] = "Confirm New Password";
$lang['show_password'] = "Show Password";
$lang['profile'] = "Profile";
$lang['update_profile_photo'] = "Update Profile Photo";
$lang['name_nickname'] = "Name / Nickname";
$lang['social_media'] = "Social Media";
$lang['facebook'] = "Facebook";
$lang['instagram'] = "Instagram";
$lang['twitter'] = "Twitter";
$lang['youtube'] = "Youtube";
$lang['pintrest'] = "Pintrest";
$lang['linkedIn'] = "LinkedIn";
$lang['notifications'] = "Notifications";
$lang['email_alert'] = "Email Alert";
$lang['email_address'] = "Email Address";
$lang['sms_alert'] = "SMS Alert";
$lang['mobile_number'] = "Mobile Number";
$lang['save'] = "Save";
$lang['clear'] = "Clear";
$lang['yes'] = "Yes";
$lang['no'] = "No";
$lang['add_social_media'] = "Add Social Media";


$lang['term_condition_text'] = "<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>";
$lang['name'] = "Name";
$lang['value'] = "Value";
$lang['close'] = "Close";

$lang['booker_form_info'] = "These are the details of the person MAKING the booking (who may also attend as well. It is particularly useful for multiple responses.";
$lang['booker_email_field_info'] = "This field is mandatory, so that the booker gets responses.";
$lang['attedee_form_info'] = "If you have a number of guest types, you can set the default values and then modify each type later.";
$lang['attedee_form_info_email'] = "This field is used to inform guests about the event, but is not mandatory.";
$lang['add_invitation_info'] = "This will appear on the registration home page. <br/><br/> Add as many as you need.";
$lang['password_access_info'] = "Decide if you want this type of invitation to require a password.";
$lang['event_limit_per_Invitation_info'] = "The maximum number of people a BOOKER can add per invitation response.";
$lang['invitation_countdown_info'] = "This will appear on the home page letting potential guests know how many places are still available for this invitation type.";
$lang['allow_waiting_list_info'] = "This field allows people who exceed the limit to still lodge a response you’re your consideration and management.";
$lang['general_event_setup_info'] = "This is the basic information about the event and is used on the home page of invitations.";
$lang['sub_title_info'] = "The subtitle can be a subtheme or the date and/or place of the event, and will appear under the main heading.";
$lang['short_form_title_info'] = "This is a short version or abbreviation of the title.";
$lang['time_zone_info'] = "Select the appropriate time zone, so it is clear to guests.";
$lang['description_info'] = "Information about the event that will appear on the invitation home page.";
$lang['event_category_info'] = "Select the type of event this is.";
$lang['total_invitation_limit_info'] = "This is an important field and will not allow any more acceptances once reached. You can set a waiting list later.";
$lang['destination_info'] = "Set the city or area the event will be in.";
$lang['sitting_style_info'] = "The seating style is important if you haven't yet set the venue.<br/>
The style determines the necessary space size.";
$lang['invitation_start_info'] = "Set the date invitations go live";
$lang['invitation_cut_off_info'] = "Set this date if you need time before the event to see who is coming.<br/>
The system will NOT close off until after the start date.";
$lang['public_event_info'] = "If the event is open to anyone, list is as public and it will appear on our public event register.";
$lang['event_title_info'] = "Please provide the full title of the event";

