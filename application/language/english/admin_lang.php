<?PHP if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*---------Full list of registrant ----------*/
$lang['admin_admin']    				= "Admin";
$lang['admin_registrant_manager']   	= "Registrant Manager";
$lang['admin_regis_list_title'] 	    = "If you need to cancel and registration or their side event click on the modify icon.";
$lang['admin_full_registrant_list']   	= "Full List of Registrants";
$lang['admin_full_regis_details']		= "Registration Details";
$lang['admin_full_personal_details']	= "Personal Details";
$lang['admin_full_paymennt_details']	= "Payment";
$lang['admin_regis_booking']		  	= "Booking #";
$lang['admin_regis_invoice']   			= "Invoice";
$lang['admin_regis_name_email']   	 	= "Name & Email";
$lang['admin_regis_rego_type']   	 	= "Rego Type";
$lang['admin_regis_total']		   		= "Total $";
$lang['admin_regis_payment_status']   	= "Payment <br> Status";
$lang['admin_regis_modify_delete']   	= "Modify & Delete";
$lang['admin_regis_no_registant']		= "No registrant";
$lang['admin_regis_unpaid']				= "Unpaid";
$lang['admin_regis_paid']				= "Paid";
$lang['admin_regis_cancelled']			= "Cancelled";

/*---------Payments & Transfers label ----------*/
$lang['admin_payments_transfers']			= "Payments & Transfers";
$lang['admin_payments_fund_transfers']		= "Payment & Funds Transfers";
$lang['admin_payments_paid_to']				= "Paid To";
$lang['admin_payments_contact_name']		= "Contact Name";
$lang['admin_payments_description']			= "Description";
$lang['admin_payments_payment_date']		= "Payment Date";
$lang['admin_payments_ammount']				= "Ammount $";
$lang['admin_payments_gst_included']		= "GST Included $";
$lang['admin_payments_total_ammount']		= "Total Amount $";
$lang['admin_payments_authorised_by']		= "Authorised By";

/*---------Processed payment list ----------*/
$lang['admin_processed_payments']  			= "Processed Payment";
$lang['admin_payments_list_date']  			= "Date";
$lang['admin_payments_list_paid_to'	]   	= "Paid To";
$lang['admin_payments_list_contact']   	 	= "Contact";
$lang['admin_payments_list_description']  	= "Description";
$lang['admin_payments_list_amount']   	 	= "Amount";
$lang['admin_payments_no_found']   	 		= "No Processed Payment Found.";


/*---------Full list of sponsor request ----------*/
$lang['admin_corportate_request']   		= "Corporate Requests";
$lang['admin_corporate_organisa']  	 		= "Organisation";
$lang['admin_corporate_contact']   			= "Contact";
$lang['admin_corporate_email']   	 		= "Email";
$lang['admin_corporate_request']   	 		= "Requests";
$lang['admin_corporate_pending']   	 		= "Pending Requests";
$lang['admin_corporate_past']   		 	= "Past Requests";
$lang['admin_no_corporate_request']   	 	= "No corporate request.";


/*----------Error and Success Message---------------------*/
$lang['admin_msg_event_record_delete']		   			= "Event user record deleted.";
$lang['admin_msg_event_record_delete']		   			= "registrant record deleted.";
$lang['admin_msg_payment_fund_transfered']				= "Payments fund transfered successfully";
$lang['admin_msg_corporate_request_approved']	   		= "Sponsor request approved successfully.";
$lang['admin_msg_corporate_request_decline']	   		= "Sponsor request cancel successfully.";


