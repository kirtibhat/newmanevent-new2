<?PHP if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*---------common tooltip for event setup-------------------*/
$lang['tooltip_edit']       = "Edit";
$lang['tooltip_delete']     = "Delete";
$lang['tooltip_limit']      = "Limit";

/*---------dashboard tooltip----*/
$lang['tooltip_dash_edit']      = "Edit";

/*---------setup event details(general event setup)-----------*/
$lang['tooltip_event_short_form_title']   = "Short Form Title";
$lang['tooltip_event_reference_number']   = "Event Reference Number";

$lang['tooltip_earlybird_start_date']       = "Earlybird registrations start date";
$lang['tooltip_earlybird_cutt_off_date']    = "This date is the last day you will accept earlybird registrations";

$lang['tooltip_description_details']        = "This is where you need to sell your event to your registrants. Keep it brief yet comprehensive.";
$lang['tooltip_start_date_regis']            = "Registrations start date";
$lang['tooltip_last_date_regis']            = "The last day that you will accept registrations";
$lang['tooltip_time_zone']                  = "Time Zone";
$lang['tooltip_max_registrant_event']       = "Max Registrants for Event";
$lang['tooltip_event_location']             = "City in which your event will be held, eg Sydney";
$lang['tooltip_sitting_style']              = "Seating Style";
$lang['tooltip_exhibition']                 = "Exhibition";
$lang['tooltip_event_destination']          = "Destination";
$lang['tooltip_event_location']             = "Location";
$lang['tooltip_event_sponsor']              = "Add sponsor logo to splashpage";

/*---------setup event details(contact person details)-----------*/
$lang['tooltip_contact_event_person']       = "Event Contact Person";
$lang['tooltip_contact_type_info']          = "Contact type information";

/*---------Setup Email Custom -----------*/
$lang['tooltip_custom_email_name']          = "Email Name";
$lang['tooltip_email_send_to']              = "Send to";

/*---------setup event details(Lost Password Contact)-----------*/
$lang['tooltip_lost_password_contact']      = "Lost Password Contact";

/*---------setup event details(Group Booking)-----------*/
$lang['tooltip_event_setup_mini_group_size'] = "Minimum group size ";

/*---------setup event details(event invoice details)-----------*/
$lang['tooltip_invoice_event_header']       = "The event name as it is to appear on the site";
$lang['tooltip_invoice_eventing_entity']    = "What name do you want to appear on your event invoice";
$lang['tooltip_invoice_abn']                = "Australian Business Number (only applies to Australian businesses)";
$lang['tooltip_invoice_above_address']      = "Tick the box to apply above address";
$lang['tooltip_invoice_address1']           = "Address1";
$lang['tooltip_invoice_regis_to_collect']   = "Registered to Collect GST";
$lang['tooltip_invoice_regis_rate']         = "GST Rate";
$lang['tooltip_invoice_email_copy_to']      = "Include email address details of other person needing copy of invoice";
$lang['tooltip_invoice_email_send_from']    = "Include details of email address that should appear in registrants inbox";
$lang['tooltip_invoice_intruduction']       = "Invoice Introduction";

/*---------setup event details(bank details)-------------------*/
$lang['tooltip_bank_bsb']                   = "A 6 digit number provided by your bank for online payments. Please ensure you include BSB details otherwise bank deposits will not be accepted";
$lang['tooltip_bank_account_number']        = "Please ensure you include Account details otherwise bank deposits will not be accepted";
$lang['tooltip_bank_details_msg']           = "The bank account details to where the final funds will be transferred to.";


/*---------setup event details(term and condition)-------------------*/
$lang['tooltip_term_term_condi']            = "Include details of your full Event Terms and conditions";
$lang['tooltip_tern_attach_term']           = "Ensure your attached document meets the criteria specified";
$lang['tooltip_term_refund_poli']           = "Include details of your Event Refund Policy";
$lang['tooltip_refund_attachment']          = "Ensure your attached document meets the criteria specified";
$lang['tooltip_term_refund_email']          = "Provide email address details for refunds";

/*---------setup registrant(personal details)-------------------*/
$lang['tooltip_persoanl_field_edit']        = "Edit";
$lang['tooltip_persoanl_field_delete']      = "Delete";

/*----------setup registrant (registrant details)-------------*/
$lang['tooltip_regist_additional']          = "Provide specific information for this type of registrant";
$lang['tooltip_regist_allow_earlybird']     = "Allow Earlybird";
$lang['tooltip_regist_regist_password_acce']= "Password Access Required";
$lang['tooltip_regist_regist_password']     = "Password";
$lang['tooltip_regist_regist_confir_pass']  = "Confirm Password";
$lang['tooltip_regist_regist_limit']        = "The maximum number of this type of invitation allowed";
$lang['tooltip_regist_unit_price']          = "This is the unit price ";
$lang['tooltip_regist_total_price']         = "This is the total price (including GST)";
$lang['tooltip_regist_gst_include']         = "This is the GST component of the total price";
$lang['tooltip_regist_earlybird_date']      = "Early Bird Date";
$lang['tooltip_regist_earlybird_limit']     = "Early Bird Limit";
$lang['tooltip_regist_earlybird_unit_price']= "Provide details of the discounted price for Earlyboard registratrants";
$lang['tooltip_regist_earlybird_price']     = "Provide details of the discounted price for Earlyboard registratrants (inclusive of GST)";
$lang['tooltip_regist_earlybird_gst_inc']   = "This is the GST component of the total price";
$lang['tooltip_regist_complimentary']       = "Complimentary";
$lang['tooltip_regist_single_day_field']    = "Single Day Registration";
$lang['tooltip_regist_day']                 = "Day";

$lang['tooltip_regist_single_allow']        = "Allow Earlybird";
$lang['tooltip_regist_single_limit']        = "Limit";
$lang['tooltip_regist_single_unit_price']   = "This is the unit price";
$lang['tooltip_regist_single_total_price']  = "This is the total price (including GST)";
$lang['tooltip_regist_single_gst_include']  = "This is the GST component of the total price";
$lang['tooltip_regist_single_early_date']   = "Early Bird Date";
$lang['tooltip_regist_single_early_unit_price'] = "Provide details of the discounted price for Earlyboard registratrants";
$lang['tooltip_regist_single_early_price']  = "Provide details of the discounted price for Earlyboard registratrants (inclusive of GST)";
$lang['tooltip_regist_single_early_gst']    = "This is the GST component of the total price";

$lang['tooltip_regist_grop_booking']        = "Allow Group Bookings";
$lang['tooltip_regist_min_booking_size']    = "Min Group booking size";
$lang['tooltip_regist_percent_diss']        = "Include the percentage of discount offered. Eg 10 (for 10%)";
$lang['tooltip_regist_single_group_book']   = "Allow Single Day Group Bookings";
$lang['tooltip_regist_single_bookingsize']  = "Min Group booking size";
$lang['tooltip_regist_single_percent']      = "Include the percentage of discount offered. Eg 10 (for 10%)";
$lang['tooltip_regist_add_regis_type']      = "Registration Type";
$lang['tooltip_regis_button_name']          = "Registrant Name";
$lang['tooltip_regis_button_type']          = "Registrant Type";

/*----------setup side event -------------*/
$lang['tooltip_side_event_limit']               = "Maximum number of participants allowed";
$lang['tooltip_side_event_venue']               = "Provide details of the venue for this side event";
$lang['tooltip_side_regis_limit']               = "Limit";
$lang['tooltip_side_additional_details']        = "Include details relevant to delegate eg. Bring workbook provided for this event";
$lang['tooltip_side_unit_price']                = "This is the unit price";
$lang['tooltip_side_total_price']               = "This is the total price (including GST)";
$lang['tooltip_side_gst_include']               = "This is the GST component of the total price";
$lang['tooltip_side_early_unit_price']          = "Early Unit Price $";
$lang['tooltip_side_early_total_price']         = "Early Total Price $";
$lang['tooltip_side_early_gst_include']         = "GST Included $";
$lang['tooltip_side_additional_guest']          = "Guests that have not registered for the event, however are eligible to attend the side event";
$lang['tooltip_side_add_unit_price']            = "Unit Price $";
$lang['tooltip_side_add_total_price']           = "Total Price $";
$lang['tooltip_side_add_gst_include']           = "GST Included $";
$lang['tooltip_side_compliemntary_guest']       = "A guest that can attend the side event but does not have to pay";
$lang['tooltip_side_compli_unit_price']         = "Unit Price $";
$lang['tooltip_side_compli_total_price']        = "Total Price $";
$lang['tooltip_side_compli_gst_include']        = "GST Included $";

/*----------setup breakout -------------*/
$lang['tooltip_breakout_popup_add_field']       = "Include name that identifies this block from any other";
$lang['tooltip_breakkout_date']                 = "Include date block will be held";
$lang['tooltip_breakkout_time']                 = "Include time block starts and finishes";

$lang['tooltip_breakkout_many_stream']          = "Number of streams to be included in your block";
$lang['tooltip_breakkout_stream_name']          = "Include specific stream name";
$lang['tooltip_breakkout_venue_stream']         = "Include details of where stream will be held, eg meeting room name";
$lang['tooltip_breakkout_stream_limit']         = "Maximum number of participarts allowed";
$lang['tooltip_breakkout_each_stream']          = "Include number of sessions you will run under this stream (only in this block)";
$lang['tooltip_breakkout_move_stream']          = "Can registrants move between streams?";

/*----------setup sponsors(floor plan)-------------*/
$lang['tooltip_sponsors_floor_plan_att']    = "Attach Terms Floor Plan";

/*----------setup sponsors (sponsor details)-------------*/
$lang['tooltip_sponsors_additional_details']    = "Provide detail description of type of sponsorship";
$lang['tooltip_sponsors_limit_package']         = "Total number of packages available for this type of sponsorship";
$lang['tooltip_sponsors_additional_booth']      = "Total price for an additional booth";
$lang['tooltip_sponsors_gst_include']           = "GST component of total price";
$lang['tooltip_sponsors_additional_guest']      = "Can each person have additional guests?";

/*----------setup exhibitor-------------*/
$lang['tooltip_exhibitor_additionla_details']   = "Additional Details";
$lang['tooltip_exhibitor_attend_breakout']      = "Can this type of sponsorship Registration attend Breakouts?";

/*----------payment option(setup payment option )-------------*/

$lang['tooltip_payment_pay_registra_fees']          = "Registeration Fee";
$lang['tooltip_payment_register_unit_price']        = "Price per Registrant $";
$lang['tooltip_payment_transction_unit_price']      = "Price per Registrant $";
$lang['tooltip_payment_pay_credit_fees']            = "Fee to be paid by";
$lang['tooltip_payment_card_statement']             = "Details as they will appear on the registrants credit card statement";
$lang['tootip_payment_recieving_payment']           = "Who is receiving this payment?";
$lang['tooltip_payment_payment_instruction']        = "Payment Instructions";
$lang['tooltip_payment_allocate_payment_option']    = "Allocate Payment Option";


/*----------customize form (Registration Form URL)-------------*/
$lang['tooltip_customize_form_url_field']           = "internet address for event page";

/*----------customize form (Logo)-------------*/
$lang['tooltip_customize_logo']                 = "Attach Event Logo as per specification requirements";

/*----------customize form (Sponsor Logo)-------------*/
$lang['tooltip_customize_sponsor_logo_field']   = "Attach Sponsor Logo";

/*----------customize form (Header)-------------*/
$lang['tooltip_customize_header']                   = "Attach Event Header as per specification requirements";

/*----------customize form (Suggested Use of Colours)-------------*/
$lang['tooltip_help_suggested_colors']              = "Suggested Use of Colours";
$lang['tooltip_help_color_1']               = "COLOUR 1";
$lang['tooltip_help_color_2']               = "COLOUR 2";
$lang['tooltip_help_color_3']               = "COLOUR 3";
$lang['tooltip_help_color_4']               = "COLOUR 4";
$lang['tooltip_help_text']                  = "TEXT";
$lang['tooltip_help_image']                 = "IMAGE";
$lang['tooltip_help_header']                = "Header";
$lang['tooltip_help_header_1']              = "Header 1";
$lang['tooltip_help_link']                  = "Link";
$lang['tooltip_help_table_text_1']          = "Table Text 1";
$lang['tooltip_help_table_text_2']          = "Table Text 2";
$lang['tooltip_help_table_bg_1']            = "Table Background 1";
$lang['tooltip_help_header__bar_bg']        = "Header Bar Background";
$lang['tooltip_help_button_icon_1']         = "Button & Icon";
$lang['tooltip_help_bg_1']                  = "Background 1";
$lang['tooltip_help_navigation_bar_bg']     = "Navigation Bar Background";
$lang['tooltip_help_button_icon_2']         = "Button & Icon";
$lang['tooltip_help_bg_2']                  = "Background 2";
$lang['tooltip_help_table_bg_2']            = "Table Background 2";

/*----------customize form (Choose Colour Theme)-------------*/
$lang['tooltip_customize_colous_header_1']              = "Header 1";
$lang['tooltip_customize_colous_header_2']              = "Header 2";
$lang['tooltip_customize_colous_link']                  = "Link";
$lang['tooltip_customize_colous_table_text_1']          = "Table Text 1";
$lang['tooltip_customize_colous_table_text_2']          = "Table Text 2";
$lang['tooltip_customize_colous_bg_1']                  = "Background 1";
$lang['tooltip_customize_colous_form_bg']               = "Form Background";
$lang['tooltip_customize_colous_header_bar_bg']         = "Header’s Bar Background";
$lang['tooltip_customize_colous_button_icon']           = "Button & Icon";
$lang['tooltip_customize_colous_table_bg_1']            = "Table Background 1";
$lang['tooltip_customize_colous_table_bg_2']            = "Table Background 2";

/*----------corporate form -------------*/
$lang['tooltip_corporate_exhibition_space']             = "Exhibition Space";
$lang['tooltip_corporate_attach_floor_plan']            = "Attach floor plan";
$lang['tooltip_corporate_total_space_available']        = "Total Space Available";
$lang['tooltip_corporate_space_name']                   = "Name of space type";
$lang['tooltip_corporate_space_available']              = "Total Spaces Avaliable";
$lang['tooltips_corporate_benefit_quantity']            = "Total benefit quantity";
$lang['title_corporate_default_purchase_name']          = "Purchase name";
$lang['title_corporate_default_purchase_limit']         = "Purchase limit";

$lang['title_registrant_email']                         =   "This email will be used to inform this registrant of the conference details.";



