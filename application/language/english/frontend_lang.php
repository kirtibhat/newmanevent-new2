<?PHP if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//------------ frontend general user section language---------------//


$lang['registerForThisEventMsg'] = "You have selected to register for this event.";
$lang['PleaseEnterEmailPassMsg'] = "Please enter your email address and password.";

$lang['frntSubmit'] = "SUBMIT";
$lang['frntLogin'] = "Login";
$lang['frntLogout'] = "Logout";
$lang['frntEmail'] = "Email";
$lang['frntPassword'] = "Password";
$lang['frntReset'] = "Reset";
$lang['frntRequiredMsg'] = "Required";
$lang['frntLoadingMsg']="Loading...";
$lang['frntTotalMsg']="Total";
$lang['priceIncGSTMsg'] = "Price Inc GST";
$lang['advertisementMsg'] = "Advertisement";

//------------for header section---------------//
$lang['headeCmpEventMsg'] = "header for company or event";
$lang['headerSecondMsg'] = "Grand Hyatt, Melbourne • 25 - 28 October 2013";

//------------Step 1 - Contact Person Details section language---------------//

$lang['regisContactPersonDetailsHeading']="Step 1 - Contact Person Details";
$lang['headinRegister'] = "Register";
$lang['regisConfirmPassword'] = "Confirm Password";
$lang['regisGender'] = "Gender";
$lang['regisMale'] = "Male";
$lang['regisFemale'] = "Female";
$lang['regisDOB'] = "DOB";
$lang['regisMonth'] = "Month";
$lang['regisDay'] = "Day";
$lang['regisYear'] = "Year";
$lang['regisButtonRegister'] = "Register";
$lang['regisConfirmPassMsg'] = "Confirm Password";
$lang['regisUserTitle']="Please choose title";
$lang['regisUserTitleMr']="Mr";
$lang['regisUserTitleMrs']="Mrs";
$lang['regisUserTitleMs']="Ms";
$lang['regisUserTitleMiss']="Miss";
$lang['regisFirstName'] = "First Name";
$lang['regisSurname'] = "Surname";
$lang['regisMobile'] = "Mobile";
$lang['regisPosition']="Position";
$lang['regisCompanyName']="Company Name";

$lang['regisPleaseNoteThatMsg']="Please note that your details when doing group or more than one booking will be
				the person invoiced and the contact person if bookings need to be modified.";
$lang['regisAllFiledsMsg']="All fields with an asterix* are mandatory.";			
$lang['addSuccessMsg'] = "Add successfully. Please check your email for verification.";
$lang['faildTryAgainMsg'] = "Failed , Please Try Again.";
$lang['varificationEmailMsg'] = "Email varification is pending. Please check your email.";
$lang['loginSuccessfullyMsg'] = "Login successfully.";
$lang['wrongEmailPasswordMsg'] = "Wrong email and password.";
$lang['passwordNotMatchMsg'] = "Password Not Match";
$lang['existsEmailMsg'] = "Email Already Used Please Try Another.";
$lang['emailVerifyMsg'] = "Email address has been verified successfully.";
$lang['alreadyemailVerifyMsg'] = "Your email address already verified.";

//------------Step 2 - Registration Type Page---------------//

$lang['step2RegistrationTypeHeading'] = "Step 2 - Registration Type";
$lang['selectTypeOfRegisMsg'] = "Please select the type of registration you would like.";
$lang['noDiscountOnGroupMsg'] = "No discount on group bookings for this type of registration.";
$lang['discountIsMsg'] = "Discount is";
$lang['typeOfRegistrationMsg'] = "Type of Registration";
$lang['registrantMsg'] = "REGISTRANT";
$lang['bookingNumberMsg'] = "Booking Number";
$lang['registrationSuccessMsg'] = "Registeration has been successfull.";
$lang['registrationFailedMsg'] = "Registeration failed.";
$lang['updateMsg'] = "update successfully.";
$lang['registrationLimitMsg'] = "Sorry! Registration limit has been exceeded for this event.";
$lang['permissionGrantedMsg'] = "Permission granted for this registration.";
$lang['enterValidPassMsg'] = "Please enter valid password.";
$lang['registrationMsg'] = "Registration";
$lang['enterPasswordMsg'] = " Please enter password for this registration.";
$lang['eventNotPublishMsg'] = "Sorry! This event is not published.";
$lang['emailExistsMsg'] = "Sorry! Event does not exists.";




//------------Step 3 Personal Details---------------//

$lang['step3PersonalDetailsHeading'] = "Step 3- Personal Details";
$lang['PleaseNoteThatFirstMsg'] = "Please note that the first persons details registered when doing group or more than one booking will be the person invoiced and the contact person if bookings need to be modified.";
$lang['allFieldsWithMsg'] = "All fields with an asterix* are mandatory.";
$lang['personalDetailsMsg'] = "Personal Details";

//------------Step 4 - Side Events---------------//

$lang['step4SideEventsHeading'] = "Step 4 - Side Events";
$lang['PleaseSelectSideeventMsg'] = "Please select the side events that you will be attending.";
$lang['sideEventsMsg'] = "Side Events";
$lang['additionalGuestsMsg'] = "Additional Guest";
$lang['guestMsg'] = "Guest";
$lang['dayMsg'] = "Day";
$lang['dietaryRequirementsMsg'] = "Dietary Requirements";



//------------Step 5 - Breakouts---------------//
$lang['step5BreakoutsHeading'] = "Step 5 - Breakouts";
$lang['PleaseselectbreakoutMsg'] = "Please select the breakouts that you will be attending.";

//------------Step 6 - Payment---------------//
$lang['step6PaymentHeading'] = "Step 6 - Payment";
$lang['pleaseconfirmMsg'] = "Please confirm these details are correct.";
$lang['registrationDetailsMsg'] = "Registration Details";
$lang['subTotalMsg'] = "SUB TOTAL";
$lang['invoiceBalanceMsg'] = "INVOICE BALANCE";
$lang['gstIncludedMsg'] = "GST INCLUDED";
$lang['discountForGroupBookingMsg'] = "DISCOUNT FOR GROUP BOOKING";
$lang['balanceOwingMsg'] = "BALANCE OWING";
$lang['term&ConditionMsg'] ="I have read, understood and agreed to the terms and conditions.";
$lang['clickHereToReadMsg'] = "Click here to read";
$lang['invoiceConfirmationMsg'] = "Send Invoice/Confirmation email to Registrant and Organiser.";
$lang['paidMsg'] = "Paid";
$lang['paymentMsg'] = "Payment";
$lang['creditCardMsg'] = "Credit Card";
$lang['pleaseSelectCardTypeMsg'] = "Please select card type";
$lang['masterCardMsg'] = "MasterCard";
$lang['visaMsg'] = "Visa";
$lang['expiryDateMsg'] = "Expiry Date";

$lang['creditCardMsg'] = "Credit Card";
$lang['chequePaymentMsg'] = "Cheque Payment";
$lang['cashPaymentMsg'] = "Cash Payment";
$lang['term_condition_default_msg']  ="The person making this booking warrants that he/she has the authority to do so on behalf of the named organisation and binds the named organisation to this agreement.The person making the booking agrees that they have read, understood & agreed to the terms & conditions of this registration."; 



$lang['amtNotZeroMsg'] = "Amount should not be zero or blank.";
$lang['paymentTransactionMsg'] = "Transaction has been completed successfully.";
$lang['merchantIdMsg'] = "Please enter valid merchant id.";
//------------frotend Home(index)---------------//

$lang['signUpMsg'] = "Sign Up!"; 
$lang['registerForEventMsg'] = "Register for Event"; 
$lang['beASponsorMsg'] = "Be a Sponsor"; 
$lang['provideAnExhibitionMsg'] = "Provide an Exhibition"; 
$lang['payments&TaxInvoiceInformationMsg'] = "Payments & Tax Invoice Information"; 
$lang['bookingChanges&CancellationsMsg'] = "Booking Changes & Cancellations"; 
$lang['ourSponsorsMsg'] = "Our Sponsors"; 



