<?PHP if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*-----dasboard title and description section-----------*/
$lang['dash_dashboard'] = 'My Events';//"Dashboard";
$lang['dash_dashboard_description_1'] = "Home introduction page. Needs some explaining about gateway and what the process is? Or this can be sent in an email when they sign up.";
$lang['dash_dashboard_description_2'] = "Given that we are doing a trial version so people can acess at no cost there would need to be an explanation about what the gateway is and that when you subscribe to Newman Events you tthen have access to this setup and are able to go live. This information should only be shown though when the person is in trial mode.";

/*-----current event listing section-----------*/
$lang['dash_current_event']       = "Current Events";
$lang['dash_add_event']           = "Add New Event";
$lang['dash_id_no']               = "ID No.";
$lang['dash_name_of_event']       = "Event Name";
$lang['dash_event_date']          = "Start Date";
$lang['dash_event_limit']         = "Registrant Limit";
$lang['dash_number_of_rego']      = "Registrants to Date";
$lang['dash_total_rego']          = "Total Registrants";
$lang['dash_status']              = "Status";
$lang['dash_approved']            = "Approved";
$lang['dash_approved_not']        = "Not Approved";
$lang['dash_edit_Delete_summery'] = "Edit, Delete or Summary";

/*-----past event listing section-----------*/
$lang['dash_past_event'] = "Past Events";
$lang['dash_no_event']   = "No Events.";

/*-----add event form section--------*/
$lang['dash_add_event_title']       = "New Event Setup";
$lang['dash_add_event_field_title'] = "Please fill out all fields below to create new event.";
$lang['dash_event_name']            = "Event Name";
$lang['dash_start_date']            = "Start Date";
$lang['dash_end_date']              = "Finish Date";
$lang['dash_error_start_date']      = "Selecte start date.";
$lang['dash_error_end_date']        = "Selected date is wrong.";

$lang['dash_field_required']        = "Please fill out this field"; 

/* free accont */
$lang['dash_event_limit_free']         = "Invitation Limit";
$lang['dash_number_of_rego_free']      = "Invitations to Date";
$lang['dash_total_rego_free']          = "Total Invitations";


$lang['home_header_home'] 		= "Home";
$lang['home_header_about_us'] 	= "About Us";
$lang['home_header_features'] 	= "Features";
$lang['home_header_packages']	= "Packages";
$lang['home_header_contact']	= "Contact";
$lang['home_header_login'] 		= "Login";
$lang['home_header_logout']		= "Log Out";
$lang['home_header_features_all']	= "All";
$lang['home_header_features_managers']	= "Managers";
$lang['home_header_features_delegates']	= "Delegates";
$lang['home_header_features_suppliers']	= "Suppliers";
$lang['home_header_features_clients']	= "Clients";
$lang['home_header_features_resources']	= "Resources";
$lang['my_profile'] 	= "My Profile";
$lang['my_account'] 	= "My Account";
$lang['alert'] 	= "Alerts";
$lang['launch'] 	= "launch";

$lang['dashboard_intro_text'] 	= "<p>
        Home introduction page. Needs some explaining about gateway and what the process is? Or this can be sent in an email when 		they sign up.</p>
        <p class='mT20 mB20'>
        Given that we are doing a trial version so people can acess at no cost there would need to be an explanation about what 						        the gateway is and that when you subscribe to Newman Events you then have access to this setup and are able to go live.         This information should only be shown though when the person is in trial mode. 
        </p>";
