<?PHP if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//------------frontend sponsor section language---------------//


$lang['registerForThisEventMsg'] = "You have selected to register for this event.";
$lang['PleaseEnterEmailPassMsg'] = "Please enter your email address and password.";

$lang['frntSubmit'] = "SUBMIT";
$lang['frntLogin'] = "Login";
$lang['frntLogout'] = "Logout";
$lang['frntEmail'] = "Email";
$lang['frntPassword'] = "Password";
$lang['frntReset'] = "Reset";
$lang['frntRequiredMsg'] = "Required";
$lang['frntRequiredemailMsg'] = "Enter valid Mail id";
$lang['frntLoadingMsg']="Loading...";
$lang['frntTotalMsg']="Total";
$lang['priceIncGSTMsg'] = "Price Inc GST";
$lang['frntSavebtn'] = "Save";


//------------Step 1 - Contact Person Details section language---------------//

$lang['regisContactPersonDetailsHeading']="Step 1 - Contact Person Details";
$lang['headinRegister'] = "Register";
$lang['regisConfirmPassword'] = "Confirm Password";
$lang['regisGender'] = "Gender";
$lang['regisMale'] = "Male";
$lang['regisFemale'] = "Female";
$lang['regisDOB'] = "DOB";
$lang['regisMonth'] = "Month";
$lang['regisDay'] = "Day";
$lang['regisYear'] = "Year";
$lang['regisButtonRegister'] = "Register";
$lang['regisConfirmPassMsg'] = "Confirm Password";
$lang['regisUserTitle']="Please choose titel";
$lang['regisUserTitleMr']="Mr";
$lang['regisUserTitleMrs']="Mrs";
$lang['regisUserTitleMs']="Ms";
$lang['regisUserTitleMiss']="Miss";
$lang['regisFirstName'] = "First Name";
$lang['regisSurname'] = "Surname";
$lang['regisMobile'] = "Mobile";
$lang['regisPosition']="Position";
$lang['regisCompanyName']="Company Name";

$lang['regisPleaseNoteThatMsg']="Please note that your details when doing group or more than one booking will be
				the person invoiced and the contact person if bookings need to be modified.";
$lang['regisAllFiledsMsg']="All fields with an asterix* are mandatory.";			


//------------Step 2 - What type of sponsorship would you like?---------------//

$lang['sponsorshipTypeHeading'] = "What type of sponsorship would you like?";
$lang['nosponsorTypeavailableMsg'] = "No sponsorship are available for this event.";

//------------Step 3 - Sponsor Type ---------------//

$lang['step3sponsorboothHeading'] = "Step 2 - Sponsor Booth";
$lang['selectdboothMsg'] = "These are the selected booth by sponsor.";
$lang['sponsorNameHeading'] = "Sponsor Name.";
$lang['boothPositionHeading'] = "Booth Position";
$lang['nosponsorboothsavailableMsg'] = "No sponsor booths are available.";

//------------Ajax Booth form---------------//

$lang['pleaseselectbothsforthissponsorHeading'] = "Please select boths for this sponsor.";
$lang['NoboothavailableforthissponsorMsg'] = "No booth available for this sponsor.";

//---------------for personal details form----------------------- //
$lang['step2PersonalDetailsHeading'] = "Step 2 - Personal Details";



