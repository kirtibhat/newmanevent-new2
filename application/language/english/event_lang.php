<?PHP if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*---------event setup heading and menu----------*/
$lang['event_event_setup_title']     = "Event Dashboard";
$lang['event_event_details']         = "Event Details";

$lang['event_setup_corporate']       = "Corporate";
$lang['event_setup_registrant']      = "Registrant";
$lang['event_setup_program']         = "Program";
$lang['event_setup_side_event']      = "Side Events";
$lang['event_setup_extras']          = "Extras";
$lang['event_setup_payment_options'] = "Payment Options";
$lang['event_setup_customize_forms'] = "Customise Invitation";
$lang['event_setup_confirm_details'] = "Confirm Details";

$lang['event_limit_per_Invitation'] = "Limit per Invitation";
$lang['event_show_invitations_remaining'] = "Invitations Count Down";
$lang['event_allow_waiting_list'] = "Allow Waiting List";


$lang['event_setup_breakouts']       = "Setup <br> Breakouts";
$lang['event_setup_sponsors']        = "Setup <br> Sponsors";
$lang['event_setkup_exhibitors']     = "Setup <br> Exhibitors";


/*---------setup event details(general event setup)-----------*/
$lang['event_general_event_setup']        = "General Event Setup";
$lang['event_event_name']                 = "Event Name";
$lang['event_event_subtitle']             = "Event Subtitle";
$lang['event_event_url']                  = "Event URL";
$lang['event_event_short_form_title']     = "Short Form Name";
$lang['event_event_reference_number']     = "Reference Number ";
$lang['event_event_start_data']           = "Start";
$lang['event_event_end_data']             = "Finish";
$lang['event_earlybird_cutt_off_date']    = "Earlybird Cut Off Date";
$lang['event_time_zone']                  = "Time Zone";
$lang['event_description_details_event']  = "Description";
$lang['event_max_registrant_event']       = "Total Registrant Limit";
$lang['event_last_date_register']         = "Registration Cut Off";
$lang['event_early_bird_date_register']   = "Earlybird Start";
$lang['event_early_bird_cut_off']         = "Earlybird Cut Off";
$lang['event_register_start_date']        = "Registration Start";
$lang['event_early_bird_reg_close_date']  = "Earlybird Registration Close Date";

$lang['event_event_location']             = "Location";
$lang['event_event_destination']          = "Destination";
$lang['event_event_venue']                = "Venue Details";
$lang['event_sitting_style']              = "Seating Style";
$lang['event_exhibition']                 = "Exhibition";
$lang['event_event_sponsor']              = "Sponsor Logo";
$lang['event_logo_sponsor_msg']           = "Tick this box if you would like to show event sponsors logos on your registration form";
$lang['event_logo_size_hint']             = "Logo size should be approx <br> 200 x 200 pixels.";
$lang['event_end_date_error']             = "End date should be greater than from start date.";
$lang['event_earlybird_cut_error']        = "Earlybird Cut Off date should be less than form start date.";
$lang['event_event_register_last_error']  = "Registration Cut Off date should be less than form end date.";
$lang['event_event_don_venue_search']     = "Find a Venue";//"<b>Venue+</b> TO  Don't have a venue? Please click here.";

/*---------setup event details(Contact Person)-----------*/
$lang['event_contact_person']           = "Event Contact Person";
$lang['event_contact_event_person']     = "Contact Person";
$lang['event_contact_title']            = "Title";
$lang['event_contact_first_last']       = "First Name";
$lang['event_contact_last_name']        = "Last Name";
$lang['event_contact_organisation']     = "Organisation";
$lang['event_general_event_posiztion']  = "Position";
$lang['event_contact_addess']           = "Address";
$lang['event_contact_address2']         = "Address2";
$lang['event_contact_city']             = "City";
$lang['event_contact_state']            = "State";
$lang['event_contact_postcode']         = "Postcode";
$lang['event_contact_country']          = "Country";
$lang['event_contact_phone']            = "Phone 1";
$lang['event_contact_phone2']           = "Phone 2";

$lang['event_contact_fax']              = "Fax";
$lang['event_contact_mobile']           = "Mobile";
$lang['event_contact_email']            = "Email";
$lang['event_confirm_email']            = "Confirm Email";
$lang['event_contact_web']              = "Web";
$lang['event_contact_fb']               = "Facebook-Id";
$lang['event_contact_twitter']          = "Twitter-Id";
$lang['event_contact_linkedin']         = "Linkedin-Id";
$lang['event_contact_gplus']            = "GooglePlus-Id";
$lang['event_contact_pintrest']         = "Pintrest-Id";
$lang['common_confirm_email_equal']     = "Confirm email should be same as email address";

// Event Category //
$lang['event_add_category']              = 'Add Category';
$lang['event_edit_category']             = 'Edit Category';
$lang['event_add_category_name']         = 'Name';
$lang['event_add_corporate_package']     = 'Corporate Category';//'Corporate Package';

/*  --- Add Custom Contact Type ---- */
$lang['add_custom_contact_type']        = 'Add Custom Field';
$lang['edit_custom_contact_type']       = 'Edit Custom Field';
$lang['custom_contact_type']            = 'Field Header'; // Contact Type
$lang['custom_contact_details']         = 'Details';

/*  --- Corporate Floor plan ---- */

$lang['add_corporate_floor_plan']       = 'Add Floor Plan Space Type';
$lang['edit_corporate_floor_plan']      = 'Edit Floor Plan Space Type';

$lang['corporate_floor_plan']           = 'Floor Plan';
$lang['corporate_exhibition_space']     = 'Exhibition Space';
$lang['corporate_attach_floor_plan']    = 'Attach Floor Plan';
$lang['corporate_select_drop_file']     = 'Select or Drop File';
$lang['corporate_total_space_available']= 'Total Spaces Available';
$lang['corporate_add_space_type']       = 'Add Space Type';
$lang['corporate_space_name']           = 'Name';
$lang['corporate_space_available']      = 'Spaces Available';


/*  --- Corporate Floor plan : Default benefit ---- */

$lang['corporate_default_package_benefit']  = 'Default Package Benefits';
$lang['corporate_default_add_benefit']      = 'Add Benefits';
$lang['corporate_default_edit_benefit']     = 'Edit Benefit';
$lang['corporate_benefit_quantity']         = 'Quantity';
$lang['corporate_benefit_name']             = 'Benefit name';
$lang['corporate_benefit_delete_success']   = 'Benefit deleted successfully';


/*--- Corporate Floor plan : Additional Purchase item ---- */

$lang['corporate_default_additional_purchase_item']     = 'Default Additional Purchase Items';
$lang['corporate_add_custom_field']     = 'Add Custom Field';

$lang['corporate_default_purchase_item']                = 'Add Purchase Item';
$lang['corporate_default_purchase_name']                = 'Name';
$lang['corporate_default_purchase_limit']               = 'Limit';
$lang['corporate_default_purchase_total_price']         = 'Total Price $ Ex. GST';
$lang['corporate_default_purchase_gst_price']           = 'Total Price $ Inc. GST';
$lang['add_corporate_pop_default_purchase_item']        = 'Add Purchase Item';
$lang['edit_corporate_pop_default_purchase_item']       = 'Edit Purchase Item';

/* Corporate category */
$lang['corporate_category']                         = 'Corporate Categories';
$lang['sponsor_corporate_category']                 = 'Corporate Category';
$lang['sponsor_category_subject_to_approval']       = 'Subject to Approval';
$lang['sponsor_category_description']               = 'Description';
$lang['sponsor_category_packages_available']        = 'Packages Available';
$lang['sponsor_category_total_price_ex_gst']        = 'Total Price $ Ex. GST';
$lang['sponsor_category_total_price_inc_gst']       = 'Total Price $ Inc. GST';
$lang['sponsor_category_registeration_included']    = 'Registration Included';

/* Add Corporate Package */
$lang['add_corporate_package'] = 'Add Corporate Package';

/*  --- Add Custom Email ---- */
$lang['add_custom_email']               = 'Add Custom Email';
$lang['add_custom_email_popup']         = 'Add Custom Email';
$lang['edit_custom_email_popup']        = 'Edit Custom Email';
$lang['custom_email_name']              = 'Name';
$lang['custom_email_subject']           = 'Subject';
$lang['custom_email_from_email']        = 'From Email';
$lang['custom_email_from_name']         = 'From Name';
$lang['custom_email_content']           = 'Contents';
$lang['custom_email_attachment']        = 'Add Attachment';
$lang['custom_email_send_to']           = 'Send To';
$lang['custom_email_email_heading']     = 'Event Emails';


/*---------setup event details(Lost Password Contact)-----------*/
$lang['event_lost_password_contact']        = "Lost Password Contact";
$lang['event_lost_password_conact_name']    = "Contact Name";
$lang['event_lost_password_phone']          = "Phone";
$lang['event_lost_password_email']          = "Email";
$lang['event_lost_password_confirm_email']  = "Confirm Email";


/*---------------- event group booking ---------------*/
$lang['event_setup_group_bookings']         = "Group Bookings";
$lang['event_setup_allow_group_booking']    = "Allow Group Bookings";
$lang['event_setup_minim_group_size']       = "Minimum Group Size";
$lang['event_setup_discount']               = "Discount";


/*--------------- setup event (Event Category) ---------- */
$lang['event_setup_catagories']   = "Event Registration Categories";
$lang['event_setup_add_category'] = "Add Category";


/*---------setup event details(Event Invoice Details)-----------*/
$lang['event_event_invoice_details']    = "Event Invoice Details";
$lang['event_invoice_header']           = "Event Header";
$lang['event_invioce_eventing_entity']  = "Invoice Entity";
$lang['event_invoice_abn']              = "ABN";
$lang['event_invoice_regis_to_collect'] = "Registered to Collect GST";
$lang['event_invoice_regis_rate']       = "GST Rate";
$lang['event_invoice_above_address']    = "Apply Event Contact Person Address";
$lang['event_invoice_address']          = "Address";
$lang['event_invoice_address2']         = "Address 2";
$lang['event_invoice_city']             = "City";
$lang['event_invoice_state']            = "State";
$lang['event_invoice_postcode']         = "Postcode";
$lang['event_invoice_country']          = "Country";
$lang['event_invoice_email_copy_to']    = "Email Invoice Copy to";
$lang['event_invoice_email_copy_to_con'] = "Confirm Email";
$lang['event_invoice_email_send_form']  = "Invoice Email Sent from";
$lang['event_invoice_email_send_form_con'] = "Confirm Email";
$lang['event_invoice_intruduction']     = "Invoice Introduction";
$lang['event_invoice_name']             ="Event Name";

/*---------setup event details(Bank Details)-----------*/
$lang['event_bank_details']             = "Reconciled Funds";
$lang['event_bank_details_msg']         = "The bank account details to where the final funds will be transferred to.";
$lang['event_bank_account_name']        = "Account Name";
$lang['event_bank_bank']                = "Bank";
$lang['event_bank_bank_loaction']       = "Bank Location";
$lang['event_bank_bsb']                 = "BSB";
$lang['event_bank_account_number']      = "Account Number";

/*---------setup event details(Terms and Conditions / Refund)-----------*/
$lang['event_term_term_condi_title']                = "Terms and Conditions";
$lang['event_term_msg']                             = "This information will be shown on the front page of the forms. If you have attached your full terms and conditions there will be a link to this on the page.";
$lang['event_term_term_condition']                  = "Terms and Conditions";
$lang['event_term_term_select_or_drop_file']        = "Select or Drop File";
$lang['event_term_attachment']                      = "Attach Event Terms and Conditions";
$lang['event_term_refund_policy']                   = "Refund Policy";
$lang['event_refund_attachment']                    = "Attach Refund Policy Document";
$lang['event_term_refund_email']                    = "Refund Email";
$lang['event_term_condition_default_msg']           ="The person making this booking warrants that he/she has the authority to do so on behalf of the named business and binds the business to this agreement. If for any reason this event does not go ahead, you will be notified as soon as possible and refunded in full."; 
//$lang['event_term_condition_default_msg']  ="The person making this booking warrants that he/she has the authority to do so on behalf of the named organisation and binds the named organisation to this agreement.The person making the booking agrees that they have read, understood & agreed to the terms & conditions of this registration."; 

/*----------setup registrant (tab1 and tab2)-------------*/
$lang['event_regis_registrant_tab1']        = "Registrant Details";
$lang['event_regis_registrant_tab2']        = "Types of Registrants";

/*----------setup registrant (add and edit registrant)-------------*/
$lang['event_regis_new_addtype']        = "Add New Registrant Type";
$lang['event_regis_addtype']            = "Registrant Type";
$lang['event_regis_add']                = "Add";
$lang['event_regis_add_type_title']     = "Please enter a name for the Field";
$lang['event_regis_button_name']        = "Registrant Name";
$lang['event_regis_button_type']        = "Registrant Type";
$lang['event_regis_password']           = "Password";
$lang['event_regis_description']        = "Description";

$lang['event_regis_name']                   = "Name";
$lang['event_regis_category_type']          = "Category";
$lang['event_regis_contact_type_info']      = "Contact type information";

/*----------setup registrant (duplicate registrant)-------------*/
$lang['event_regis_duplicate_type']     = "Duplicate Type of Registration";
$lang['event_regis_duplicate_title']    = "Please enter a name for the Field";
$lang['event_regis_dupli_button_name']  = "Name";

/*----------setup registrant (Login for Standard Registrations)-------------*/
$lang['event_regis_login_title']        = "Login for Standard Registrations";
$lang['event_regis_login_msg1']         = "If you need a separate login to standard registrations (helpful when you have complimentary registrants) please create a new type of login (doorway to register).<br><br>The password created can be handed out to the registrants you want to use this login.";
$lang['event_regis_login_add_login']    = "Add a type of Login";
$lang['event_regis_login_password']     = "Password";
$lang['event_regis_login_below_mg']     = "This information will be shown in the login window for this type of login, please fill out the details below.";
$lang['event_regis_login_org_msg']      = "If you do not have this password please contact";

/*----------setup registrant (Personal Details)-------------*/
$lang['event_personal_details_master']  = "Personal Details";
$lang['event_personal_details_for']     = "Personal Details of";
$lang['event_personal_details_msg']     = "Select the fields below that you would like to display, hide or have as mandatory on the Registration form. If there is a additional field that you need for your event that is not shown here, click on the plus and name the field you need.";
$lang['event_perosnal_add_another']     = "Add another field";
$lang['event_regis_email_msg']          = "*This email will be used to inform this registrant of the conference details.";

/*----------setup registrant (Dietary Requirements)-------------*/
$lang['event_dietary_title']            = "Dietary Requirements";
$lang['event_dietary_tick_box']         = "Tick the box if your event needs dietary requirments";
$lang['event_dietary_msg']              = "Simply set up your dietary options by either deleting the unnecessary options or adding a custom dietary requirement by clicking ‘add new’";
$lang['event_dietary_add_new']          = "Add new";
$lang['event_dietary_food_name']        = " FOOD NAME";

/*----------setup registrant (Add Dietary Requirements)-------------*/
$lang['event_dietary_add_field_title']              = "Add New Dietary Requirement";
$lang['event_dietary_add_field_msg']                = "Please enter a name for the Field";
$lang['event_dietary_field_name']                   = "Dietary Requirement Name";


/*----------setup registrant (registrant details)-------------*/

$lang['booker_details'] = 'Booker Details';

//$lang['event_regist_title_add']           = "<br> To add another type of registration, click on the ‘Add a type of Registration’ button and fill out the relevant information. To Duplicate a exsisting registration, click duplicate and rename, you are then able to make adjustments to fields. <br><br> If you wish to have earlybird pricing please fill out that field for each registration type. The early bird date and time you set here is when full pricing will commence. Please note these times do not recognise any daylight savings or local summer time changes, it is your responsibility to ensure your bookers be made aware of this.";
$lang['event_regist_title_add']             = "Select the fields below that you would like to display, hide or have as mandatory on the <br/>
                                               Registration form. If there is a additional field that you need for your event that is not shown <br/>
                                               here, click on the plus and name the field you need.";

$lang['event_regist_additional']            = "Additional Details";
$lang['event_regist_allow_earlybird']       = "Allow Earlybird";
$lang['event_regist_regist_password_acce']  = "Password Access <br> Required";
$lang['event_regist_regist_password_access']= "Password Access";
$lang['event_regist_regist_password']       = "Password";
$lang['event_regist_regist_confir_pass']    = "Confirm Password";
$lang['event_regist_regist_limit']          = "Limit";
$lang['event_regist_unit_price']            = "Unit Price $";
$lang['event_regist_total_price']           = "Total Price $ Ex. GST";
$lang['event_regist_gst_include']           = "Total Price $ Inc. GST";
$lang['tooltip_regist_price_ex_gst']        = "Price $ Ex. GST";
$lang['event_regist_price_ex_gst']          = "Price $ Ex. GST";
$lang['event_regist_price_inc_gst']         = "Price $ Inc. GST";
$lang['event_regist_earlybird_date']        = "Early Bird Date";
$lang['event_regist_earlybird_limit']       = "Early Bird Limit";
$lang['event_regist_earlybird_unit_price']  = "Earlybird Price $ Ex. GST";
$lang['event_regist_earlybird_price']       = "Earlybird Price $ Ex. GST";
$lang['event_regist_earlybird_gst_inc']     = "Earlybird Price $ Inc. GST";
$lang['event_regist_complimentary']         = "Complimentary";
$lang['event_regist_single_day_field']      = "Single Day Registration";
$lang['event_regist_day']                   = "Day";
$lang['event_regist_single_allow']          = "Allow Earlybird";
$lang['event_regist_single_limit']          = "Limit";
$lang['event_regist_single_unit_price']     = "Price $ Ex. GST";
$lang['event_regist_single_total_price']    = "Total Price $";
$lang['event_regist_single_gst_include']    = "Price $ Inc. GST";
$lang['event_regist_single_early_date']     = "Early Bird Date";
$lang['event_regist_single_early_limit']     = "Earlybird Limit";
$lang['event_regist_single_early_unit_price']   = "Earlybird Price $ Ex. GST";
$lang['event_regist_single_early_price']    = "Earlybird Price $ Ex. GST";
$lang['event_regist_single_early_gst']      = "GST Included $";
$lang['event_regist_grop_booking']          = "Allow Group Bookings";
$lang['event_regist_min_booking_size']      = "Min Group booking size";
$lang['event_regist_percent_diss']          = "Percentage discount";
$lang['event_regist_single_group_book']     = "Allow Single Day Group Bookings";
$lang['event_regist_single_bookingsize']    = "Min Group booking size";
$lang['event_regist_single_percent']        = "Percentage discount";
$lang['event_regist_allow_early_bird']      = "Allow Early Bird";
$lang['event_regist_complimentary']         = "Complimentary";
$lang['event_regist_allow_day_group_booking']= "Allow Day Group Bookings";


/*----------add & edit setup side event -------------*/
$lang['event_side_popup_add_title']     = "Add Side Event";
$lang['event_side_popup_duplicate_title']       = "Duplicate Side Event";
$lang['event_side_popup_edit_title']    = "Edit Side Event";
$lang['event_side_popup_add_msg']       = "Please enter a name for the Side Event";
$lang['event_side_popup_add_field']     = "Side Event Name";
$lang['event_side_popup_title']         = "Title";


/*----------setup side event --------------------*/
$lang['event_side_event_discription']       = " To add a side event click on the plus button below. Please note that unless you tick the ‘Registrant Type’ boxes and fill out the additional information for those registrants, all pricing will be based on the Standard Registrant.";
$lang['event_add_side_event']               = "Add Side Event";
$lang['event_side_event_limit']             = "Limit";
$lang['event_side_event_venue']             = "Venue of this Side Event";
$lang['event_side_event_date']              = "Date of Event";
$lang['event_side_event_start_data']        = "Start Date";
$lang['event_side_event_end_data']          = "Finish Date";
$lang['event_side_event_time_on_form']      = "Show Time on Form";
$lang['event_side_time_start']              = "Time of Event, Start";
$lang['event_side_time_finish']             = "Finish";
$lang['event_side_allow_earlybird']         = "Allow Earlybird";
$lang['event_side_earlybird_date']          = "Early Bird Date";
$lang['event_side_registrant_select']       = "Tick the box if you want that type of registrant to attend this break-out. Set limits for each registration type.";
$lang['event_side_regis_limit']             = "Limit";
$lang['event_side_complimentary']           = "Complimentary";
$lang['event_side_additional_details']      = "Additional Details";
$lang['event_side_unit_price']              = "Unit Price $";
$lang['event_side_total_price']             = "Total Price $";
$lang['event_side_gst_include']             = "GST Included $";
$lang['event_side_early_unit_price']        = "Early Unit Price $";
$lang['event_side_early_total_price']       = "Early Total Price $";
$lang['event_side_early_gst_include']       = "GST Included $";
$lang['event_side_dietary_msg']             = "When these boxes are ticked the extra fields for dietry requirements and full name of guest will appear on registration form.";
$lang['event_side_additional_guest']        = "Additional Guests";
$lang['event_side_add_max_num']             = "Set Maximum Number";
$lang['event_side_add_unit_price']          = "Unit Price $";
$lang['event_side_add_total_price']         = "Price $ Ex. GST";
$lang['event_side_add_gst_include']         = "Price $ Inc. GST";
$lang['event_side_compliemntary_guest']     = "Complimentary Guests";
$lang['event_side_compli_max_num']          = "Set Maximum Number";
$lang['event_side_compli_unit_price']       = "Unit Price $";
$lang['event_side_compli_total_price']      = "Price $ Ex. GST";
$lang['event_side_compli_gst_include']      = "Price $ Inc. GST";
$lang['event_side_add_registrant']          = "Add registrant for side event ";
$lang['event_side_start']                   = "Start";
$lang['event_side_finish']                  = "Finish";
$lang['event_side_venue']                   = "Venue";
$lang['event_side_additional_details']      = "Additional Details";
$lang['event_side_limit']                   = "Limit";
$lang['event_side_earlybird_limit']         = "Earlybird Limit";
$lang['event_side_common_price']            = "Common Price";
$lang['event_side_default_pricing']         = "Default Pricing";
$lang['event_side_earlybird_limit']         = "Early Bird Limit";
$lang['event_side_earlybird_total_price']   = "Earlybird Price $ Ex. GST";
$lang['event_side_earlybird_gst_inc']       = "Earlybird Price $ Inc. GST";
$lang['event_side_grop_booking']            = "Allow Group Bookings";
$lang['event_side_min_booking_size']        = "Min Group booking size";
$lang['event_side_percent_diss']            = "Percentage discount";
$lang['event_side_restrict_event_access']   = "Restrict Event Access";
$lang['event_side_yes']                     = "Yes";
$lang['event_side_no']                      = "No";
$lang['event_side_guest_limit']             = "Guest Limit";
$lang['event_side_total_early_price']       = "Earlybird Price $ Ex. GST";
$lang['event_side_complementary_guests']    = "Complementary Guests";
$lang['event_side_guests_limit']            = "Guest Limit";



/*----------add & edit breakout -------------*/
$lang['event_breakout_add_title']           = "Add a Block";
$lang['event_breakout_edit_title']          = "Edit a Block";
$lang['event_breakout_duplicate_title']     = "Duplicate a Block";
$lang['event_breakout_popup_add_msg']       = "Please enter a name for the Breakout. This name will not be shown of the form but is for your refence only.";
$lang['event_breakout_popup_add_field']     = "Block Name";

/*----------setup breakout-------------*/
$lang['event_breakout_msg']             = "To add a ‘breakout’ click on the plus button below and give it a name. Please note breakouts unlike side events do not have pricing. This area is for breakouts, workshop and other speicial purpose gathering that you may have within your event or conference. <br> <br> Please note that if the break out is running over more than one day, create a seperate breakout for each day.";
$lang['event_breakout_add_breakout']    = "Add a Block";
$lang['event_breakout_setup_1']         = "STEP 1 - Setup date of breakout and limits";
$lang['event_breakkout_date']           = "Date of Block";
$lang['event_breakkout_time']           = "Time of Block";
$lang['event_breakkout_regis_select']   = "Tick the box if you want that type of registrant to attend this break-out. Set limits for each registration type.";
$lang['event_breakkout_regis_limit']    = "Limit";
$lang['event_breakkout_setup_2']        = "STEP 2 - Setup your blocks";
$lang['event_breakkout_many_block']     = "How many block in this breakout";
$lang['event_breakkout_block']          = "Block";
$lang['event_breakkout_title']          = "Title";
$lang['event_breakkout_start']          = "Start";
$lang['event_breakkout_finish']         = "Finish";
$lang['event_breakkout_setup_3']        = "STEP 3 - Setup your streams";
$lang['event_breakkout_many_stream']    = "Number of Streams";
$lang['event_breakkout_stream_name']    = "Name of Stream";
$lang['event_breakkout_stream_limit']   = "Limit";
$lang['event_breakkout_venue_stream']   = "Venue of Stream";
$lang['event_breakkout_setup_4']        = "STEP 4 - Setup your sessions";
$lang['event_breakkout_each_stream']    = "Sessions per a Stream";
$lang['event_breakkout_move_stream']    = "Can registrants move between streams?";
$lang['event_breakkout_session']        = "Session";
$lang['event_breakkout_session_limit']  = "Limit";
$lang['event_breakkout_select_block']   = "Select Block";
$lang['event_breakkout_setup_5']        = "Please fill out the below session deatils for each stream.";
$lang['event_breakkout_about_session']  = "About Session";
$lang['event_breakkout_speaker']        = "Speakers";
$lang['event_breakkout_organisation']       = "Organisation";

/*----------setup sponsors (floor plane add)-------------*/
$lang['event_sponsors_msg']             = "Start by attaching the floor plan of the exhibition booths, then click on the ‘Add a Sponsor’ button and fill out the relevant information.";
$lang['event_sponsors_add_sponsors']    = "Add a Sponsor";
$lang['event_sponsors_floor_plan']      = "Floor Plan";
$lang['event_sponsors_floor_plan_msg1'] = "The floor plan you attach needs to have booths numbered so that can pick which booth they would like to purchase.";
$lang['event_sponsors_floor_plan_msg2'] = "Floor plan to be in PDF format.";
$lang['event_sponsors_floor_plan_att']  = "Attach Terms Floor Plan";
$lang['event_sponsors_number_of_booth'] = "Number of booths <br> avaliable";

/*----------setup sponsors (sponsor details)-------------*/
$lang['event_sponsors_additional_details']  = "Additional Details";
$lang['event_sponsors_limit_package']       = "Limit of packages <br> available";
$lang['event_sponsors_total_price']         = "Total Price $";
$lang['event_sponsors_gst_include']         = "GST Included $";
$lang['event_sponsors_additional_booth']    = "Additional booth $";
$lang['event_sponsors_gst_include']         = "GST Included $";
$lang['event_sponsors_booth_available']     = "Please select what booths are avaliable to this package";
$lang['event_sponsors_registrant_include']  = "Is registration included in this package?";
$lang['event_sponsors_complimentary']       = "Complimentary Registration";
$lang['event_sponsors_how_many_compli']     = "How many complimentary registrations?";
$lang['event_sponsors_registrant_select']   = "Tick the box if you want that type of registrant for this sponsor. Set limits for each registration type.";
$lang['event_sponsors_additional_guest']    = "Can each person have additional guests?";
$lang['event_sponsors_limit_person']        = "Limit per person";
$lang['event_sponsors_total_price']         = "Total Price each $";
$lang['event_sponsors_gst_include']         = "GST Included $";
$lang['event_sponsors_compli_attendance']   = "Please specify if there will be any complimentary attendance to side events?";
$lang['event_sponsors_additional_items']    = "Please add any additional items that are can be purchased";
$lang['event_sponsors_purchase_item']       = "Add additional purchase items";
$lang['event_sponsors_attend_breakouts']    = "Can this type of sponsorship Registration attend Breakouts?";


/*----------setup sponsors (add sponsor)-------------*/
$lang['event_sponsor_add']              = "Add a Sponsor";
$lang['event_sponsor_edit']             = "Edit a Sponsor";
$lang['event_sponsor_add_title']        = "Please enter a name for the Sponsor.";
$lang['event_sponsor_name']             = "Sponsor Name";


/*----------setup sponsors (purchase additional items)-------------*/
$lang['event_exhibitor_item_add']       = "additional purchase items";
$lang['event_exhibitor_item_edit']      = "Add additional purchase items";
$lang['event_exhibitor_item_msg']       = "Please enter a name for the additional purchase items.";
$lang['event_exhibitor_item_field']     = "Items Name";

/*----------setup exhibitor (add / edit)-------------*/
$lang['event_exhibitor_add_exhibitor']      = "Add a Exhibitor";
$lang['event_exhibitor_edit_exhibitor']     = "Edit a Exhibitor";
$lang['event_exhibitor_add_msg']            = "Please enter a name for the Exhibitor.";
$lang['event_exhibitor_add_field']          = "Exhibitor Name";

/*----------setup exhibitor (add / edit item)-------------*/
$lang['event_exhibitor_add_item']           = "Add a item";
$lang['event_exhibitor_edut_item']          = "Edit a item";
$lang['event_exhibitor_add_item_msg']       = "Please enter a name for the item.";
$lang['event_exhibitor_add_item_field']     = "Item Name";

/*----------setup exhibitor(exhibitor details)-------------*/
$lang['event_exhibitor_msg']                = "Start by attaching the floor plan of the exhibition booths, then click on the ‘Add a Exhibitor button and fill out the relevant information.";
$lang['event_exhibitor_add_exhibitor']      = "Add a Exhibitor";
$lang['event_exhibitor_additionla_details'] = "Additional Details";
$lang['event_exhibitor_limit_package']      = "Limit of packages <br> available";
$lang['event_exhibitor_total_price']        = "Total Price $";
$lang['event_exhibitor_gst_include']        = "GST Included $";
$lang['event_exhibitor_booth_available']    = "Please select what booths are avaliable to this package";
$lang['event_exhibitor_side_event_msg']     = "Please specify if there will be any complimentary attendance to side events?";
$lang['event_exhibitor_item_msg']           = "What else is avaliable in this package, eg. Complimentary Satchel. These items will be seen when choosing package and will show on the invoice.";
$lang['event_exhibitor_add_item_title']     = "Add a item";
$lang['event_exhibitor_attend_breakout']    = "Can this type of sponsorship Registration attend Breakouts?";

/*----------payment option(Registration Fees, Bank Fees, Credit card, payment gateway)-------------*/
$lang['event_payment_message_1']                = "Description to be insert Here about Personal Details on Form, Dietary Requirements and adding a Type of Registrant.";
$lang['event_payment_message_2']                = "To setup simply delete the fields not needed and if a additional field is need click on add new.";
$lang['event_payment_registrant_fees']          = "Registration Fees";
$lang['event_payment_registration_msg']         = "Choosing the registrant to pay the fees will add the amount choosen below to the total registration price. Choosing the organiser will not affect the total registration price.";
$lang['event_payment_pay_registra_fees']        = "Registeration Fee";
$lang['event_payment_pay_cheque_fees']          = "Cheque Processing Fee";
$lang['event_payment_pay_refund_fees']          = "Refund Fee";
$lang['event_payment_paid_by']                  = "Fee paid by";
$lang['event_payment_pay_transaction_fees']     = "Fee paid by";
$lang['event_paymen_fees_yes']                  = "Yes";
$lang['event_paymen_fess_no']                   = "No";
$lang['event_paymen_pay_by_1']                  = "Registrant";
$lang['event_paymen_pay_by_2']                  = "Client";
$lang['event_payment_register_unit_price']      = "Price per Registrant $";
$lang['event_payment_register_gst_include']     = "Total Price $ Inc. GST";
$lang['event_payment_register_total_price']     = "Total Price $ Ex. GST ";

/*----------------payment option(Transaction fees)-----------------------*/
$lang['event_paymen_paid_transction_by_1']      = "Registrant";
$lang['event_paymen_paid_transction_by_2']      = "Client";
$lang['event_payment_transction_unit_price']    = "Price per Registrant $";
$lang['event_payment_transction_gst_include']   = "GST Included $";
$lang['event_payment_transction_total_price']   = "Total Price $";
$lang['event_payment_register_is_addi']         = "Add as Additional Cost";

/*---------------payment option (credit card)-------------------*/
$lang['event_payment_pay_credit_fees']          = "Fee paid by";
$lang['event_paymen_paid_credit_by_1']          = "Registrant";
$lang['event_paymen_paid_credit_by_2']          = "Client";
$lang['event_payment_register_regis_select']    = "Types of Registrants who have to pay the fee?";
$lang['event_payment_transaction_fees']         = "Transactions Fees";
$lang['event_payment_cardi_card_fees']          = "Credit Card Fees";
$lang['event_payment_cheque_fees']              = "Cheque Processing Fees";
$lang['event_payment_refund_fees']              = "Refund Processing Fees";
$lang['event_payment_accepted_card']            = "Accepted Credit Cards";
$lang['event_payment_payment_gateway']          = "Payment Gateway";
$lang['event_payment_marchant_id']              = "Merchant ID";
$lang['event_payment_paypal_username']          = "Paypal Username";
$lang['event_payment_paypal_password']          = "Paypal Password";
$lang['event_payment_paypal_signature']         = "Paypal Signature";

/*----------payment option(setup payment option )-------------*/
$lang['event_payment_payment_option']           = "Payment Options";
$lang['event_payment_setup_payment_option']     = "Setup Payment Options";
$lang['event_payment_allocate_payment_option']  = "Allow Payment Option";
$lang['event_payment_recieving_payment']        = "Who is receiving this payment?";
$lang['event_payment_payment_instruction']      = "Payment Instructions";
$lang['event_payment_alternative_option']       = "Click if you would like to add an alternative payment option";
                                                                                 
$lang['event_payment_restrict_purchase_access'] = "Registratrion Included";
$lang['event_payment_add_payment_option']       = "Add Payment Option";
$lang['event_payment_edit_payment_option']      = "Edit Payment Option";


/**----------Cheque payment option-------------**/
$lang['event_payment_tickbox_cheque']= "Tick the box for registrants that can pay by Cheque";

/**----------Cheque payment option-------------**/
$lang['event_payment_tickbox_direct_depositEFT']= "Tick the box for registrants that can pay by Direct Deposit EFT";


/*----------payment option(add payment option alternative )-------------*/
$lang['event_payment_add_alternative']          = "Add alternative payment option";
$lang['event_payment_add_alternative_msg']      = "Please enter a name for the payment option";
$lang['event_payment_add_alternative_field']    = "Name";
$lang['event_payment_tickbox_alternative_msg']= "Tick the box for registrants that can pay by";


/*----------customize form (Registration Form URL)-------------*/

$lang['event_customize_form_url']               = "Registration Form URL";
$lang['event_customize_form_url_msg']           = "Below is the URL to the registration forms created for this event.";
$lang['event_customize_form_url_field']         = "Event URL";
$lang['event_customize_url_copy']               = "Copy";
$lang['event_customize_url_email']              = "Email";

/*----------customize form (Logo)-------------*/
$lang['event_customize_logo']                   = "Secondry Image";
$lang['event_customize_logo_field'] = "Upload Logo"; //Attach Event Logo";
$lang['event_customize_logo_size_msg']          = "Logo size should be (220 X 41)";

/*----------customize form (Sponsor Logo)-------------*/
$lang['event_customize_sponsor_logo'] = "Sponsor or Image Gallery";
$lang['event_customize_sponsor_logo_field'] = "Upload Logos";
$lang['event_customize_sponsor_logo_size_msg']      = "Logo size should be approx 200 x 200 pixels";

/*----------customize form (Header)-------------*/
$lang['event_customize_header'] = "Header Image";
$lang['event_customize_header_field'] = "Upload Event Header";
$lang['event_customize_header_size_msg']            = "Header size should be (1050 X 247)";

/*----------customize form (Choose Colour Theme)-------------*/
$lang['event_customize_color_theme']                    = "Choose Colour Theme";
$lang['event_customize_selecte_msg_1']                  = "01. Select four colours you would like to use for your forms";
$lang['event_customize_selecte_color_1']                = "Color 1";
$lang['event_customize_selecte_color_2']                = "Color 2";
$lang['event_customize_selecte_color_3']                = "Color 3";
$lang['event_customize_selecte_color_4']                = "Color 4";
$lang['event_customize_selecte_msg_2']                  = "02. Select text colour*";
$lang['event_customize_colous_black']                   = "Black";
$lang['event_customize_colous_grey']                    = "Grey";
$lang['event_customize_text_will_msg']                  = "*Text will automatically be converted to white on the dark background.";
$lang['event_customize_selecte_msg_3']                  = "03. Select button & icon set";
$lang['event_customize_option_1']                       = "Option 1";
$lang['event_customize_option_2']                       = "Option 2";
$lang['event_customize_colous_use_msg']                 = "You can now use these four colours and apply them to different aspects of your form.";
$lang['event_customize_colous_theme']                   = "TEXT";
$lang['event_customize_colous_header_1']                = "Header 1";
$lang['event_customize_colous_header_2']                = "Header 2";
$lang['event_customize_colous_link']                    = "Link";
$lang['event_customize_colous_table_text_1']            = "Table Text 1";
$lang['event_customize_colous_table_text_2']            = "Table Text 2";
$lang['event_customize_colous_img_bg']                  = "IMAGE / BACKGROUND";
$lang['event_customize_colous_bg_1']                    = "Background 1";
$lang['event_customize_colous_form_bg']                 = "Form Background";
$lang['event_customize_colous_header_bar_bg']           = "Header’s Bar Background";
$lang['event_customize_colous_button_icon']             = "Button & Icon";
$lang['event_customize_colous_table_bg_1']              = "Table Background 1";
$lang['event_customize_colous_table_bg_2'] = "Table Background 2";

$lang['event_customize_theme'] = "Customise Theme";
$lang['event_customize_apply_previous_theme'] = "Apply Previous colour Theme";
$lang['event_customize_apply_previous_theme_tooltip'] = "Apply Previous colour Theme";
$lang['event_customize_colour_scheme'] = "Colour Scheme";
$lang['event_customize_colour_scheme_tooltip'] = "Colour Scheme";
$lang['event_customize_colour_scheme_dark'] = "Dark";
$lang['event_customize_colour_scheme_light'] = "Light";
$lang['event_customize_back_colour'] = "Background Colour";
$lang['event_customize_main_colour'] = "Main Colour";
$lang['event_customize_highlight_colour'] = "Highlight Colour";
$lang['event_customize_font_package'] = "Font Package";
$lang['event_customize_link_colour'] = "Link Colour";
$lang['event_customize_select_font_package'] = "Select Font Package";


/*************************** Presenter ****************************************/

$lang['programer_block_type']                           =   "Block Type";
$lang['programer_stream']                               =   "Streams";
$lang['programer_no_of_session']                        =   "Number of Sessions";
$lang['programer_block_date']                           =   "Date";
$lang['programer_block_time']                           =   "Time";
$lang['programer_block_plenary']                        =   "Plenary";
$lang['programer_block_nonplenary']                     =   "Non Plenary";
$lang['programer_select_block_type']                    =   "Select Block Type";

$lang['add_presenter_button']                           =   "Add Presenter";
$lang['import_presenter_button']                        =   "Import Presenters";

$lang['program_presenter_add']                          =   "Add New Presenter";
$lang['program_presenter_edit']                         =   "Edit Presenter";
$lang['program_presenter_copy']                         =   "Copy Presenter";

$lang['program_presenter_title']                        =   "Title";
$lang['program_presenter_fname']                        =   "First Name";
$lang['program_presenter_lname']                        =   "Last Name";
$lang['program_organisation']                           =   "Organisation";
$lang['program_email']                                  =   "Email";
$lang['program_confirm_email']                                  =   "Confirm Email";
$lang['program_phone']                                  =   "Phone";
$lang['program_topic']                                  =   "Topic";
$lang['program_presentation_description']               =   "Presentation Description";
$lang['program_registration_required']                  =   "Registration Required";
$lang['program_registration_req_yes']                   =   "Yes";
$lang['program_registration_req_no']                    =   "No";
$lang['program_registration_type']                      =   "Registration Type";
$lang['program_sort_presenter']                         =   "Sort Presenters";


$lang['program_block_title']                            =   "Select the fields below that you would like to display, hide or have as mandatory on the <br/>Registration form. If there is a additional field that you need for your event that is not shown <br/>here, click on the plus and name the field you need.";

$lang['program_block_button']                      =   "Add Program Block";
$lang['programer_session']                         =   "Session";
$lang['programer_topic']                           =   "Topic";
$lang['programer_Start']                           =   "Start";
$lang['programer_finish']                          =   "Finish";
$lang['programer_location']                        =   "Location";
$lang['programer_limit']                           =   "Limit";
$lang['programer_restrict_stream_access']          =   "Restrict Stream Access";
$lang['programer_restrict_event_access']           =   "Restrict Event Access";
$lang['programer_restrict_session_access']         =   "Restrict Session Access";
$lang['programer_access_yes']                      =   "Yes";
$lang['programer_access_no']                       =   "No";
$lang['programer_sessions']                        =   "Sessions";
$lang['programer_session_stream']                  =   "Sessions per Stream";
$lang['programer_allow_stream_hopping']            =   "Allow Stream Hopping";
$lang['programer_stream_serial']                   =   "Stream";
$lang['programer_stream_name']                     =   "Name of Stream";
$lang['programer_stream_location']                 =   "Location";
$lang['programer_time']                            =   "Time";
$lang['programer_time_start']                      =   "Start";
$lang['programer_time_finish']                     =   "Finish";
$lang['programer_number_presenter']                =   "Number of Presenters";
$lang['programer_presenter']                       =   "Presenter";
$lang['programer_select_presenter']                =   "Select Presenter";
$lang['programer_description']                     =   "Description"; 
$lang['programer_plenary_session_name']            =   "Session Name";

/*
 |--------------------------------------------------------------------|
 |----------------Confirm Details Forms*------------------------------|
 |--------------------------------------------------------------------|
*/

$lang['event_conf_confirm_details']             = "Confirm Details";
$lang['event_conf_title']                       = "Title";
$lang['event_conf_start_date']                  = "Start Date";
$lang['event_conf_end_date']                    = "End Date";
$lang['event_conf_event_Status']                = "Event Status";
$lang['event_conf_change_status']               = "Change Status";
$lang['event_conf_url']                         = "URL";
$lang['event_conf_publish']                     = "Publish";
$lang['event_conf_unpublish']                   = "Unpublish";
$lang['event_conf_status_pub_msg']              = "Event published successfully.";
$lang['event_conf_status_unpub_msg']            = "Event unpublished successfully.";

/*********************** Registration *******************************/
$lang['event_default_registrant_details_master']    = "Default Invitations Details";
$lang['event_registration_custom_field']            = "Add Custom Field";
$lang['event_dietary_requirements_title']           = "Dietary Requirements";
$lang['event_dietary_popup_add_field_title']        = "Add Dietary Requirement";
$lang['event_dietary_add_dietary_req_title']        = "Add Dietary Requirement";
$lang['event_dietary_add_registrant_type']          = "Add Registrant Type";
$lang['event_registrant_category']                  = "Registrant Categories";



/************ Custom field **************/
$lang['form_custom_field']                              = "Custom Field";
$lang['event_custom_form_field_name']                   = "Name";
$lang['event_custom_form_field_type']                   = "Field Type";
$lang['event_custom_form_field_text']                   = "Text Field";
$lang['event_custom_form_field_ddm']                    = "Drop down menu";
$lang['event_custom_form_field_radio']                  = "Radio Button";
$lang['event_custom_form_field_file']                   = "Add File";
$lang['event_custom_form_quantity']                     = "Quantity";
$lang['event_custom_form_field_1']                      = "Field 1";



/*
 |--------------------------------------------------------------------|
 |---------------Error and Success Message Text ---------------------------|
 |--------------------------------------------------------------------|
*/
$lang['comm_error']                                                 = "There was an error,Please try again.";
$lang['corporate_benefit_save_succ']                                = "Default package benefits save successfully.";
$lang['corporate_package_benefit_save_succ']                        = "Package benefits save successfully.";

$lang['default_corporate_spacetype_deleted_succ']                   = "Space type deleted successfully.";
$lang['default_package_deleted_succ']                               = "Default package benefits deleted.";
$lang['corporate_purchased_item_save_succ']                         = "Purchase item saved successfully.";
$lang['default_purchase_deleted_succ']                              = "Purchase item deleted.";
$lang['default_purchase_deleted_success']                           = "Purchase item deleted successfully.";

$lang['space_booth_required']                                       = "Space booth is required";
$lang['corporate_package_save_succ']                                = "Corporate package saved successfully";
$lang['corporate_package_update_succ']                              = "Corporate package updated successfully.";
$lang['corporate_package_deleted_succ']                             = "Corporate package deleted successfully.";
$lang['corporate_app_detail_updated_succ']                          = "Corporate Application details updated successfully.";


$lang['event_msg_event_create_successfully']                        = "Event Created successfully.";
$lang['event_msg_general_event_setup_saved']                        = "General event setup saved.";
$lang['event_msg_contact_person_details_saved']                     = "Contact person details saved.";
$lang['event_msg_custom_contact_saved']                             = "Custom contact saved.";
$lang['event_custom_contact_add_error']                             = "Please add contact person details before adding custom contact type.";
$lang['event_msg_event_invoice_details_saved']                      = "Event invoice details saved.";
$lang['event_msg_event_bank_details_saved']                         = "Event bank details saved.";
$lang['event_msg_event_lost_password_saved']                        = "Lost  password details saved.";
$lang['event_msg_event_term_condition_saved']                       = "Event term condition saved.";
$lang['event_msg_event_email_saved']                                = "Event email saved.";
$lang['event_msg_event_deleted_successfully']                       = "Event deleted successfully.";
$lang['event_msg_type_of_registration_edit_successfully']           = "Type of registration edit successfully.";
$lang['event_msg_type_of_registration_add_successfully']            = "Type of registration add successfully.";
$lang['event_msg_type_registration_duplicate_created']              = "Registration duplicate created successfully. Default limit is set Zero.";
$lang['event_msg_type_registration_deleted']                        = "Type of Registration deleted successfully.";
$lang['event_msg_personal_details_form_saved']                      = "Personal details form saved.";
$lang['event_msg_custom_field_updated']                             = "Custom field updated successfully.";
$lang['event_msg_custom_field_add']                                 = "Custom field added successfully.";
$lang['event_msg_custom_field_deleted']                             = "Custom field deleted successfully.";
$lang['event_msg_dietary_add_successfully']                         = "Dietary add successfully.";
$lang['event_msg_registrant_order_changed']                         = "Registrant order changed.";
$lang['event_msg_corporate_package_order_changed']                  = "Corporate package order changed.";
$lang['event_msg_registration_form_details_saved']                  = "Registration form details saved successfully.";
$lang['event_msg_invalid_data_request']                             = "Invalid data request.";
$lang['event_msg_side_event_updated_successfully']                  = "Side event updated successfully.";
$lang['event_msg_side_event_add_successfully']                      = "Side event added successfully.";
$lang['event_msg_side_event_duplicate_successfully']                = "Side event duplicate created successfully.";
$lang['event_msg_side_event_deleted']                               = "Side event deleted.";
$lang['event_msg_side_events_setup_saved']                          = "Side event saved successfully.";
$lang['event_msg_side_events_registrant_saved']                     = "Side event registrant saved successfully.";
$lang['event_msg_invalid_data_request']                             = "Invalid data request.";
$lang['event_msg_breakout_duplicate_created_successfully']          = "Breakout duplicate created successfully. Default limit set zero";
$lang['event_msg_breakout_updated_successfully']                    = "Breakout updated successfully.";
$lang['event_msg_breakout_add_successfully']                        = "Breakout add successfully.";
$lang['event_msg_breakout_deleted']                                 = "Breakout deleted.";
$lang['event_msg_setup_breakout_saved']                             = "Setup breakout saved successfully.";
$lang['event_msg_invalid_data_request']                             = "Invalid data request.";
$lang['event_msg_exhibitor_floor_plan_updated']                     = "Floor plan updated.";
$lang['event_msg_exhibitor_floor_plan_added']                       = "Floor plan added.";
$lang['event_msg_sponsor_title_updated_successfully']               = "Sponsor Title updated successfully.";
$lang['event_msg_sponsor_add_successfully']                         = "Sponsor add successfully.";
$lang['event_msg_sponsor_additional_purchase_item_updated']         = "Sponsor additional purchase item updated successfully.";
$lang['event_msg_sponsor_additional_purchase_item_add']             = "Sponsor additional purchase item add successfully.";
$lang['event_msg_sponsor_additional_purchase_item_deleted']         = "Sponsor additional purchase item deleted.";
$lang['event_msg_sponsor_deleted']                                  = "Sponsor deleted.";
$lang['event_msg_sponsor_details_saved']                            = "Sponsor details saved.";
$lang['event_msg_invalid_data_request']                             = "Invalid data request.";
$lang['event_msg_exhibitor_title_updated']                          = "Exhibitor Title updated successfully.";
$lang['event_msg_exhibitor_add']                                    = "Exhibitor add successfully.";
$lang['event_msg_exhibitor_deleted']                                = "Exhibitor deleted.";
$lang['event_msg_exhibitor_additional_item_updated']                = "Exhibitor additional item updated successfully.";
$lang['event_msg_exhibitor_additional_item_add']                    = "Exhibitor additional item add successfully.";
$lang['event_msg_exhibitor_item_deleted']                           = "Exhibitor item deleted.";
$lang['event_msg_exhibitor_details_saved']                          = "Exhibitor details saved.";
$lang['event_msg_exhibitor_invalid_data_request']                   = "Invalid data request.";
$lang['event_msg_registration_fees_saved']                          = "Registration Fees saved.";
$lang['event_msg_bank_fees_saved']                                  = "Bank Fees saved.";
$lang['event_msg_cheque_fees_saved']                                = "Cheque saved.";
$lang['event_msg_refund_fees_saved']                                = "Refund processing fees saved successfully.";
$lang['event_msg_credit_card_fees_details_saved']                   = "Credit card fees details saved.";
$lang['event_msg_payment_gateway_detail_saved']                     = "Payment gateway detail saved.";
$lang['event_msg_payment_options_saved']                            = "Payment options saved.";
$lang['event_msg_alternative_payment_option_add_successfully']      = "Payment option added successfully.";
$lang['event_msg_alternative_payment_option_updated_successfully']  = "Payment option updated successfully.";
$lang['event_msg_payment_option_delete_successfully']               = "Payment option deleted successfully.";
$lang['event_msg_colour_theme_saved']                               = "Colour theme saved successfully.";
$lang['event_msg_breakout_select_between_date']                     = "Breakout selected date should be between event start & end date.";
$lang['corporate_floor_paln_space_type_save']                       = "Corporate floor plan space saved successfully.";
$lang['corporate_floor_paln_space_type_updated']                    = "Corporate floor plan space updated successfully.";
$lang['corporate_floor_paln_space_type_space_error']                = "Total Spaces Available should be greater or equal to space type";

$lang['program_presenter_add_succ']                                 = "Program presenter added successfully.";
$lang['program_presenter_updated_succ']                             = "Program presenter updated successfully.";
$lang['program_presenter_copy_succ']                                = "Program presenter duplicate created successfully.";
$lang['program_presenter_deleted_succ']                             = "Program presenter deleted successfully.";
$lang['program_block_session_deleted_succ']                         = "Program block session deleted successfully.";



$lang['event_category_add_succ']        = "Category added successfully.";
$lang['event_category_updated_succ']    = "Category updated successfully.";
$lang['event_category_deleted_succ']    = "Category deleted successfully.";                 



/*----------setup extras --------------------*/
$lang['event_extras_popup_add_title']               = "Add Extra Item";
$lang['event_extras_popup_edit_title']              = "Edit Extra Item";
$lang['event_extras_popup_duplicate_title']         = "Duplicate Extra Item";
$lang['event_extras_popup_title']                   = "Title";
$lang['event_msg_extra_add_successfully']           = "Extra item added successfully.";
$lang['event_msg_extra_edit_successfully']          = "Extra item updated successfully.";
$lang['event_msg_extra_delete_successfully']        = "Extra item deleted successfully.";
$lang['event_msg_extra_duplicate_successfully']     = "Extra item duplicate created successfully. Default limit set zero.";
$lang['event_extra_regis_limit']                    = "Limit";
$lang['event_extra_additional_detaokils']             = "Additional Details";
$lang['event_extra_common_price']                   = "Common Price";
$lang['event_extra_common_total_price']             = "Common Total Price";
$lang['event_extra_common_price_gst']               = "Common Price GST Included";
$lang['event_extra_restrict_purchase_access']       = "Registratrion Included";
$lang['event_extra_purchase_access_yes']            = "Yes";
$lang['event_extra_purchase_access_no']             = "No";
$lang['event_msg_extra_setup_saved']                = "Event extra item saved successfully.";
$lang['event_extra_custom_field']                   = "Add Custom Field";
$lang['event_msg_extra_custom_field_updated']       = "Custom field updated successfully.";
$lang['event_msg_extra_custom_field_add']           = "Custom field added successfully.";
$lang['event_msg_extra_custom_field_deleted']       = "Custom field deleted successfully.";


/*---------setup event social media-----------*/
$lang['event_social_media_setup']               = "Social Media";
$lang['event_website']                          = "Event Website";
$lang['event_website_tooltip']                  = "Event Website URL";
$lang['event_facebook']                         = "Facebook";
$lang['event_instagram']                        = "Instagram";
$lang['event_twitter']                          = "Twitter";
$lang['event_youtube']                          = "Youtube";
$lang['event_pintrest']                         = "Pintrest";
$lang['edit_custom_media_type']                 = "Custom Social Media Type";
$lang['event_add_social_media']                 = "Add Custom Field";
$lang['event_edit_social_media']                = "Edit Custom Field";
$lang['event_msg_custom_social_media_saved']    = "Custom Field saved successfully.";
$lang['event_msg_custom_social_media_added']    = "Custom SField added successfully.";
$lang['event_msg_custom_social_media_updated']    = "Custom Field updated successfully.";
$lang['event_msg_custom_social_media_deleted']    = "Custom Field updated successfully.";
$lang['event_msg_social_media_details_saved']   = "Social media details saved successfully.";
$lang['event_media_type_name']                  = "Social Media Label";
$lang['event_media_type_value']                 = "Link";



/*
 * set languages for free account
 */
$lang['select_font'] = 'Select'; 
$lang['event_limit_max_error_message']                          = 'The reservation entered limit is exceed form total limit.';
$lang['event_setup_invitations']                                = "Invitations"; //Reservations
$lang['add_custom_contact_type_free']                           = 'Add Custom Field';  
$lang['event_contact_phone_free']                               = "Phone";
$lang['event_add_social_media_free']                            = "Add Custom Field";
$lang['event_invitation_type_free']                             = "Add Invitation Type";
$lang['invitation_type_free']                                   = "Invitation Types";
$lang['event_invitation_add']                                   = "Add";
$lang['event_invitation_addtype']                               = "Invitation Type";
$lang['event_msg_type_of_invitation_edit_successfully']         = "Invitation type updated successfully.";
$lang['event_msg_type_of_invitation_add_successfully']          = "Invitation type added successfully.";
$lang['event_customize_theme_free'] = "Select Theme";
$lang['event_msg_type_invitation_duplicate_created']            = "Invitation duplicate created successfully. Default limit is set Zero.";
$lang['event_msg_type_invitation_deleted']                      = "Type of Invitation deleted successfully.";
$lang['event_msg_invitation_form_details_saved']                = "Invitation form details saved successfully.";

$lang['themeoption'] = 'Theme Option';

$lang['event_theme_screen_bg'] = 'Screen Background';
$lang['event_theme_header_bg'] = 'Primary'; //Header Background
$lang['event_theme_box_bg'] = 'Secondary';//Box Background
$lang['event_theme_text_color'] = 'Text Color';
$lang['event_theme_link_color'] = 'Link';//Link Color
$lang['event_theme_link_highlighted_color'] = 'Highlighted';//Link Highlighted Color
$lang['event_theme_link_highlight_color'] = 'Highlight';//Link Highlighted Color
$lang['event_theme_font'] = 'Font Family Select';
$lang['reset_btn_title'] = 'Reset';
$lang['use_btn_title'] = 'Use';
$lang['used_btn_title'] = 'Used';
$lang['customize_theme'] = 'Customize Theme';
$lang['customize_theme'] = 'Theme Name';
$lang['event_msg_colour_theme_apply'] = 'Colour theme applied successfully.';
$lang['event_default_registrant_details_free']    = "Default Attendee Details";
$lang['view_location_on_map']    = "View location on map";


$lang['link_home'] = 'Home';
$lang['link_manage_registration'] = 'Manage Existing Registration';
$lang['link_term_condition'] = 'Terms and Conditions';
$lang['link_client_web'] = 'Client Website';
$lang['link_contact_us'] = 'Contact Us';
$lang['access_id'] = 'ACCESS ID';
$lang['whatsthis'] = "what's this?";
$lang['event_program'] = 'EVENT PROGRAM';
$lang['delegate_registration'] = 'Delegate Registration';
$lang['sponsor_registration'] = 'Sponsor Registration';
$lang['exhibitor_registration'] = 'Exhibitor Registration';
$lang['speaker_registration'] = 'Speaker Registration';
$lang['custom_category1'] = 'Custom Category 1';
$lang['custom_category2'] = 'Custom Category 2';
$lang['custom_category3'] = 'Custom Category 3';

$lang['reservation_close'] = 'RESERVATION CLOSE';
$lang['register_here'] = 'Register Here';
$lang['valid_email_message'] = 'This email will be used to inform this registrant of the conference details';

$lang['event_total_invitation_limit']         = "Total Invitation Limit";
$lang['event_invitation_start_date']          = "Invitation Start";
$lang['event_last_date_invitation']           = "Invitation Cut Off";
$lang['event_public_invitation']              = "Public Event";
$lang['event_public_invitation_yes']          = "Yes";
$lang['event_public_invitation_no']           = "No";
$lang['tooltip_event_public']                 = "Is public event";
$lang['event_reference_number']               = "Reference";
$lang['event_venue_city']                     = "City";
$lang['event_venue_address']                  = "Address";
$lang['event_venue_state']                    = "State";
$lang['event_venue_zip']                      = "Postcode";
$lang['header_info_tooltip']                  = "Information";

$lang['image']                  = "Image";
$lang['header_image_text']      = "Header Image Here";
/* theme lang*/
$lang['sample_form']   = "Sample Form";
$lang['national_conference']   = "National Conference";
$lang['board_meeting']   = "Board Meeting";
$lang['party_time']   = "Party Time";
$lang['wedding_invitation']   = "Wedding Invitation";
$lang['exhibition']   = "Exhibition";
$lang['charity_walk']   = "Charity Walk";
$lang['theme_type_info']   = "Your theme name";
$lang['invitations_close'] = 'INVITATIONS CLOSE';
$lang['header_image_title'] = 'Header Image';
$lang['create_header_image_title'] = 'Create Header Image';
$lang['or_title'] = 'OR';
$lang['select_from_gallery'] = 'Select from Gallery';
$lang['select_solid_color'] = 'Select Solid Colour';
$lang['save_title'] = 'Save';
$lang['overlay_image'] = 'Overlay Image';
$lang['create_overlay_image'] = 'Create Overlay Image';
$lang['image_gallery'] = 'Image Gallery';
$lang['create_image_gallery'] = 'Create Image Gallery';
$lang['font_title'] = 'Fonts';
$lang['select_font_title'] = 'Select Font Style';
$lang['text_title'] = 'Text';
$lang['create_form'] = 'Create Your Form';
$lang['select_form'] = 'Select Custom Theme';
$lang['forgot_link'] = 'Forgot?';

$lang['mandatory_field_check'] = 'Mandatory Field Check';
$lang['all_required_field_message'] = 'You have filled all required fields. Press Launch button to publish your event.';
$lang['all_required_field_message_error'] = 'Please fill all those required field first which is mentioned below before launch your event.';
$lang['launch_event'] = 'Launch Event';

$lang['terms_page_title'] = 'Our Terms';
$lang['terms_page_message1'] = 'We want to be sure that you understand the terms as they relate to this type of account.';
$lang['add_social_media_button_text'] = 'Add Social Media';

$lang['newman_events_url_text'] = 'http://newmanevents.com/events/invitation/';
$lang['add_contact_button_text'] = 'Add Event Contact';
$lang['add_document_text'] = 'Attach Document';
$lang['url_prefix_social_media'] = 'http://';

$lang['free_sign_up'] 	= "Free Sign up";
$lang['my_profile'] 	= "My Profile";
$lang['my_account'] 	= "My Account";
$lang['alert'] 	= "Alerts";
$lang['launch'] 	= "launch";
$lang['home_header_home'] 		= "Home";
$lang['home_header_about_us'] 	= "About Us";
$lang['home_header_features'] 	= "Features";
$lang['home_header_packages']	= "Packages";
$lang['home_header_contact']	= "Contact";
$lang['home_header_login'] 		= "Login";
$lang['home_header_logout']		= "Log Out";
$lang['home_header_features_all']	= "All";
$lang['home_header_features_managers']	= "Managers";
$lang['home_header_features_delegates']	= "Delegates";
$lang['home_header_features_suppliers']	= "Suppliers";
$lang['home_header_features_clients']	= "Clients";
$lang['home_header_features_resources']	= "Resources";
$lang['invitation_type_no_added_message']	= "No Invitation Type added yet. Please add a new Invitation Type";

