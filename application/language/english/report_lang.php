<?PHP if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*---------Full list of registrant ----------*/
$lang['report_admin']    				= "Reports";
$lang['report_regis_list_title'] 	    = "If you need to cancel and registration or their side event click on the modify icon.";
$lang['report_full_list_registrants']  	= "Registration Reports";
$lang['report_full_regis_details']		= "Registration Details";
$lang['report_full_personal_details']	= "Personal Details";
$lang['report_full_paymennt_details']	= "Payment";
$lang['report_regis_booking_history']  	= "Booking No.";
$lang['report_regis_date_time']   		= "Date & Time";
$lang['report_regis_event_name']   	 	= "Event Name";
$lang['report_regis_name_email']   	 	= "Name & Email";
$lang['report_regis_rego_type']   	 	= "Registration Type";
$lang['report_regis_total_owing']   	= "Total $ Owing $";
$lang['report_regis_modify_delete']   	= "Modify & Delete";
$lang['report_regis_serach_msg']	= "Searched not found.";


$lang['report_regis_amount']			= "Amount $";
$lang['report_regis_balance_remaining']	= "Balance Remaining";
$lang['report_regis_payment_method']	= "Payment Method";
$lang['report_regis_payment_date']		= "Payment Date";

/*---------Full list of payments ----------*/
$lang['report_payments']   				= "Reports";
$lang['report_payments_details_title']  = "Show all type of payments like credit card, cheque, and cash.";
$lang['report_payments_details']   		= "Financial Reports";
$lang['report_pay_booking_id']   		= "Booking Id";
$lang['report_pay_registrant']   		= "Registration Count";
$lang['report_pay_registrant_name']   	= "Registrant Name";
$lang['report_pay_date']  	 			= "Date Time";
$lang['report_pay_total_amount']   	 	= "Total <br> Amount ($)";
$lang['report_pay_cheque_transaction'] 	= "Cheque no. /<br> Transaction Id";
$lang['report_pay_received_amt']	  	= "Received <br> Amount ($)";
$lang['report_pay_outstaning_amt'] 	  	= "Outstanding <br> Amount ($)";
$lang['report_pay_payment_status'] 	  	= "Payment <br> Received";
$lang['report_pay_received']  		 	= "Confirm";
$lang['report_pay_not_received']		= "Cancel";
$lang['report_pay_event_select_msg']	= "Please select event.";
$lang['report_pay_event_select_fail_msg']	= "Searched not found.";
$lang['report_pay_event_date']			= "Date:";
$lang['report_pay_event_cash']			= "Cash";
$lang['report_pay_event_cheque']		= "Cheque";
$lang['report_pay_event_credit_card']	= "Credit Card";
$lang['report_pay_event_sub_total']		= "Sub Total";
$lang['report_pay_event_grand_total']	= "Grand Total";
$lang['report_pay_event_select_msg']	= "Event Select";
$lang['report_pay_financial_view_report']	= " Please select event for viewing financial reports.";

/*---------Delegate report filter  ----------*/
$lang['delegate_filter_reports']	= "Reports";
$lang['delegate_filter_delegate_report']	= "Delegate Reports";



/*---------Payment cancel message  ----------*/
$lang['report_msg_cancellation_message']		   	= "Cancellation Message";
$lang['report_msg_cancellation_message_enter']	   	= "Message";
$lang['report_msg_cancellation_email_message']	   	= "We did not received your payment so your registered registrant not activated.";


/*----------Error and Success Message---------------------*/
$lang['report_msg_event_record_delete']		   			= "Event user record deleted.";
$lang['report_msg_payment_confirm_status_change']		= "Payment confirm successfully.";
$lang['report_msg_payment_cancel_status_change']		= "Payment cancel successfully.";
