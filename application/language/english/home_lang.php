<?PHP if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*--------home page (header area) ---------*/
$lang['home_header_home'] 		= "Home";
$lang['home_header_about_us'] 	= "About Us";
$lang['home_header_features'] 	= "Features";
$lang['home_header_packages']	= "Packages";
$lang['home_header_contact']	= "Contact";
$lang['home_header_login'] 		= "Login";
$lang['home_header_logout']		= "Log Out";
$lang['home_header_features_all']	= "All";
$lang['home_header_features_managers']	= "Managers";
$lang['home_header_features_delegates']	= "Delegates";
$lang['home_header_features_suppliers']	= "Suppliers";
$lang['home_header_features_clients']	= "Clients";
$lang['home_header_features_resources']	= "Resources";



/*--------home page (middle area) ---------*/
$lang['home_simple'] 			 = "SIMPLE";
$lang['home_secure'] 			 = "SECURE";
$lang['home_surprising'] 		 = "SURPRISING";
$lang['home_free'] 				 = "Free";
$lang['home_casual']			 = "Casual";
$lang['home_solo']				 = "Solo";
$lang['home_enterprise'] 		 = "Enterprise";
$lang['home_comparison'] = "Comparison";
$lang['home_solo_price'] 		 = "66";
$lang['home_enterprise_price']	 = "66";
$lang['home_per_month']			 = "per month";
$lang['home_some_basic_things']  = "Some basic things";
$lang['home_extra_things']		 = "Extra things as well";
$lang['home_absolutely_free'] 	 = "Absolutely Free";

/*About Us Page Content*/
$lang['home_header_about_us_header1'] = "Header";
$lang['home_header_about_us_header2'] = "Header";
$lang['home_header_about_us_header3'] = "Header";

/*--------home page (footer) ---------*/
$lang['home_term_condition']	 = "Terms &amp; conditions";
$lang['home_privacy_policy']	 = "Privacy policy";

/*--------home page (Login) ---------*/
$lang['home_login_title'] 	 	= "Login";


$lang['home_forgot_password']  	= "Forgot?";
$lang['home_login_button']		= "Log In";
$lang['home_login_cancel']		= "Cancel";
$lang['home_remember_me'] 		= "Remember me";
$lang['home_create_account'] 	= "Create an account";
$lang['home_trouble_login'] 	= "Trouble logging in?";

/*--------home page (Login) ---------*/
$lang['home_headinRegister'] = "Register";
$lang['home_regisName'] = "Name";
$lang['home_regisEmail'] = "Email";
$lang['home_regisPassword'] = "Password";
$lang['home_regisConfirmPassword'] = "Confirm Password";
$lang['home_regisGender'] = "Gender";
$lang['home_regisMale'] = "Male";
$lang['home_regisFemale'] = "Female";
$lang['home_regisDOB'] = "DOB";
$lang['home_regisMonth'] = "Month";
$lang['home_regisDay'] = "Day";
$lang['home_regisYear'] = "Year";
$lang['home_regisRequired'] = "required";
$lang['home_regisButtonRegister'] = "Register";
$lang['home_regisButtonReset'] = "Reset";

/*--------home page (Forgot Password) ---------*/
$lang['home_forgot_title'] 		  	 = "Forgot?";
$lang['home_forgot_description']	 = "Forgot your password?  <br/>Enter your email below and we will send you instructions to reset it.";
$lang['home_forgot_email']  		 = "Email address";
$lang['home_forgot_submit']			 = "Submit";
/*--------Mail Sent ---------*/
$lang['home_forgot_emailsent']	    = "Email Sent!";
$lang['home_forgot_emailsent_description']   = "If this email does not arrive soon, check your spam folder. It will be from Newmanevents.com";

/*--------home page (Trouble Logging) ---------*/
$lang['home_trouble_login_title']	    = "Trouble Logging In?";
$lang['home_trouble_login_firstname']   = "First Name (required)";
$lang['home_trouble_login_surname']  	= "Surname (required)";
$lang['home_trouble_login_email']  		= "Email (required)";
$lang['home_trouble_login_message']		= "Message (required)";
$lang['home_trouble_submit']			= "Send";

/*--------home page (Contact Us) ---------*/
$lang['contact_us_title']	    = "Contact Us";
$lang['contact_us_firstname']   = "First Name (required)";
$lang['contact_us_surname']  	= "Surname (required)";
$lang['contact_us_email']  		= "Email (required)";
$lang['contact_us_phone_no']  	= "Phone No. (required)";
$lang['contact_us_message']		= "Message (required)";




/*
 |---------------------------------------------------------------------------|
 | Home Section Error Message
 |---------------------------------------------------------------------------|
*/ 

$lang['home_error_login_sucess'] 	 	 	= "Login successfully.";
$lang['home_error_invalid_email']	 	 	= "Invalid email address and password.";
$lang['home_error_email_already']	 	 	= "Email already used.";
//$lang['home_error_register_success'] 		= "Registered successfully. Account activation link has been sent to your email address.";
$lang['home_error_register_success'] 		= "Registered successfully. You can login now.";
$lang['home_error_register_fail']	 	 	= "Regisration failed.";
$lang['home_error_account_active_error'] 	= "Wroung link entered. Please enter correct link.";
$lang['home_error_account_active']			= "Your account successfully activated. Please Login Now.";
$lang['home_error_account_active_already']  = "This account already activated.";
$lang['home_error_newpassword_send'] 	    = "New password send to your email address.";
$lang['home_error_newpassword_fail'] 	    = "Email address not exit. Please check your email address.";
$lang['home_error_trouble_success'] 	    = "Trouble logging in request send succesfully.";
$lang['home_contact_us_success'] 	   		= "We have received your message and will be in contact soon.";
$lang['home_error_trouble_fail'] 	    	= "Email address not exist. Please check your email address.";
$lang['pending_email_msg']            	    = "Email varification is pending. Please check your email.";

 
/*Forgot password mail sent message */

$lang['forgot_email_sent_popup_success1']  =  "We have sent an email to address %s with instructions to reset your password.";


/*Reset Password Page*/
$lang['reset_password_text'] = "Password reset for email %s";
$lang['reset_password_text1'] = "New password (minimum 6 Alpha/numeric combination)";
$lang['password_reset_server_validation'] = "Password must be minimum 6 char & alphanumeric AND password & confirm password should be same.";

/*Reset Password Success Page*/
$lang['reset_sucsess_title'] = "Password Reset";
$lang['reset_password_text'] = "Your password has been successfully reset.";
$lang['reset_password_text1'] = "We have sent an email notification to the account address.";



/*Package Section*/

/*Apply free Page*/
$lang['apply_free'] = 'Apply Free';

$lang['lable_organisation'] = 'Organisation / Venue';
$lang['label_newman_nickname'] = 'Newman Nickname';


$lang['welcome'] = 'Welcome!';
$lang['Lets_start_with_the_basics'] = 'Let’s start with the basics:';
$lang['you_are_ready'] = 'You’re ready to set up your new NEWMAN Events account. The account will be administered through your email address.';
$lang['this_will_be_your_permanent_account_address'] = 'This will be your permanent account address';
$lang['user_password'] = 'User Password';
$lang['password'] = 'Password';
$lang['confirm_password'] = 'Confirm Password';
$lang['user_password_text1'] = 'The User Password is what you will use to log into the event management site.';
$lang['user_password_text2'] = 'To log in you will need your email address and this password.';
$lang['user_password_text3'] = 'The password needs to be a minimum 6 alpha/numeric combination';
$lang['confirm_password_equal'] = "New password and confirm password should be same";
$lang['next'] = "Next";
$lang['our_terms'] = "Our Terms";
$lang['our_terms_text'] = "We want to be sure that you understand the terms as they relate to this type of account.";
$lang['our_terms_text1'] = "I have read and agree with the";
$lang['our_terms_text2'] = "Terms & Conditions";
$lang['our_terms_text3'] = "for my Newman Events account.";
$lang['congratulations'] = "Congratulations";
$lang['cong_text1'] = "You’ve finished setting up your account for your Newman Events Free Account.";
$lang['cong_text2'] = "We’ll send you an email with a link to your personalised Start Up page.";

/*Apply casual page*/
$lang['apply_casual'] = 'Apply Casual';

/*Apply enterprise page*/
$lang['apply_enterprise'] = 'Apply Enterprise';
/*Apply solo page*/
$lang['apply_solo'] = 'Apply Solo';

/*Thanks popup after registration*/

$lang['please_check_your_email_and_click_verify'] = 'Please check your email and click VERIFY in the message we just sent to %s to proceed further.';
$lang['thanks_for_choosing_newman_event'] = 'Thanks for choosing Newman Events.';
$lang['thanks_for_applying'] = 'Thanks for Applying';

/*Custom validation message*/

$lang['is_unique_email_custom_message'] = '<p>This email is already registered with us.</p> <p>Please check your inbox to verify your account.</p><p> If you already verified please login.</p>';
$lang['record_insert_error'] = 'Oops! Something went wrong at this moment. Please try later';



/*--------common required validation message ---------*/
$lang['common_the_field']            = "The field ";
$lang['common_isrequired']            = " is required";
$lang['common_field_required'] = "This field is required";
$lang['common_confirm_email_equal'] = "Confirm email should be same as email address";
$lang['common_confirm_password_equal'] = "New password and confirm password should be same";
$lang['common_password_minlength'] = "Password should be alphanumeric and minimum 6 characters in length";
/*--------contat page validation message ---------*/
$lang['contact_first_name']             = "first name";
$lang['contact_surname']                = "surname";
$lang['contact_email']                    = "email address";
$lang['contact_phonecode']            = "phonecode";
$lang['contact_phoneno']               = "phone no";
$lang['contact_message']               = "message";
$lang['contact_phone_minlength']  = "Phone no minlength should be 10 characters";
$lang['contact_phone_maxlength'] = "Phone no maxlength should be 10 characters";
$lang['contact_phone_numeric'] = "Phone no should be numeric";
$lang['contact_phonecode_minlength']  = "Phone code minlength should be 2 characters";
$lang['contact_phonecode_maxlength'] = "Phone code maxlength should be 2 characters";
$lang['contact_phonecode_numeric'] = "Phone code should be numeric";

$lang['common_read_more'] = 'read more';
$lang['common_lipsum_text'] = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sit amet est mi. Phasellus sollicitudin sit amet libero tincidunt adipiscing. Suspendisse potenti. Proin facilisis, dolor at tristique rhoncus, est justo facilisis sapien, et faucibus neque sapien ac diam. Pellentesque volutpat odio sit amet adipiscing rutrum. Nam vitae interdum sapien. Praesent at nulla';
$lang['common_lipsum_text_short'] = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sit amet est mi. Phasellus sollicitudin sit amet libero tincidunt adipiscing. Suspendisse potenti.';
$lang['common_lipsum_text_short1'] = 'Sed sit amet est mi.Phasellus sollicitudin sit amet libero.';
$lang['common_lipsum_text_short2'] = 'Sed sit amet est mi.Phasellus sollicitudin sit amet libero tincidunt adipiscing sitamet libero.';
$lang['common_lipsum_text_short3'] = 'Ramet libero tincidunt adipiscing sitamet libero.';
$lang['common_lipsum_text_short4'] = 'Phasellus sollicitudin sit amet libero tinmet libero.';
$lang['common_lipsum_text_short_short'] = 'Lorem ipsum dolor sit amet adelami uspendisse potenti.';


$lang['features_convenience_fingertips'] = 'Convenience at your fingertips';
$lang['features_hotel_transfer_options'] = 'Hotel Transfer Options';
$lang['features_wide_charity_network'] = 'Wide Charity Network';
$lang['features_discounted_airfares'] = 'Discounted Airfares';
$lang['features_realtime_event_tracking'] = 'Real-time Event Tracking';
$lang['features_great_contact_potential'] = 'Great Contact Potential';
$lang['features_the_best_venues'] = 'The Best Venues';
$lang['features_secure_online_onations'] = 'Secure Online Donations';
$lang['features_awards_and_recognition'] = 'Awards and Recognition';
$lang['features_ease_dissemination'] = 'Ease of Info Dissemination';
$lang['features_customise_stay'] = 'Customise Your Stay';
$lang['features_event_promo_materials'] = 'Event Promo Materials';
$lang['features_secure_fee_shelters'] = 'Secure Fee Shelters';
$lang['features_24hour_turnaround'] = '24-hour Turnaround';

$lang['features_quick_hotel_reservation'] = 'Quick Hotel Reservations';



/***************** New languages with less setup ********************/
$lang['newman_account_registration_step1_title'] = 'Account Set Up';
$lang['registration_step1_heading1'] = 'You are about to set up your NEWMAN account.';
$lang['registration_step1_heading2'] = 'You can log in with a Facebook or Google+ account or set up a NEWMAN ID.';
$lang['create']         = 'Create';
$lang['account']        = 'Account';
$lang['login_with']     = 'Sign up with';
$lang['login_with_']     = 'Log in with';
$lang['next_btn_title'] = 'Next';
$lang['back_btn_title'] = 'Back';
$lang['lable_firstname'] = 'First Name';
$lang['lable_surname']  = 'Last Name';
$lang['lable_email_address']    = 'Email Address';
$lang['lable_confirm_email']    = 'Confirm Email Address';
$lang['lable_organization']     = 'Organization';
$lang['home_login_password'] 	= "Password";

$lang['password_form_heading'] 	= "Your Password";
$lang['password_form_paragraph2'] 	= "The password needs to be minimum 6 alphanumeric with special character combination.";
$lang['password_form_paragraph1'] 	= "The user password is what you will use to log into the Newman system.<br><br>To log in you will need your verified email address and this password.";
$lang['register_paragraph1_bottom'] = "We want to be sure that you understand the terms as they relate to this type of account.";
$lang['register_terms_paragraph2_bottom'] = "I have read and agree with the<br><span class='link_terms'>Terms & Conditions &nbsp;</span> for my <br>Newman account.";

$lang['terms_heading'] 	= "Our Terms";
$lang['terms_form_paragraph1'] 	= 'We want to be sure that you understand the terms as they relate to this type of account.';
$lang['terms_form_paragraph2'] 	= 'I have read and agree with the<br><span class="link_terms">Terms & Conditions &nbsp;</span> for my <br> Newman account.';

                    
$lang['home_simple_text'] 			 = "<p class='bold'>CHOOSE AN ACCOUNT TYPE</p>
            <p>If your event is free, so is the basic system.
            It is designed for simple events, but still
            offers a robust solution.</p>
            <p>If you are charging a fee, it is simple to select the 
            best account option that best suits your needs and budget.</p>
            <p>We also cater for event teams, if your
            business requires integrated access by your
            team, clients and suppliers. This is a simple
            way to manage your event business.</p>";
$lang['home_secure_text'] 			 = "<p class='bold'>CREATE THE INVITATION</p>
            <p>Setting up the event is easy and the system
            offers a lot of flexibility in how your invitation
            will look.</p>
            <p>You can setup different types of registrant
            and put limits on how many can attend.</p>
            <p>You can manage the invitations once people
            start to reply, including having valuable
            information like dietary requirements.</p>
            <p>All the information is stored on a secure
            system located in Australia.</p>";
$lang['home_surprising_text'] 		 = "<p class='bold'>THE LAUNCHPAD</p>
            <p>Once you have an account, you will get
            access to a Launchpad that will provide
            basic information about your event.</p>
            <p>It is also your gateway to many other
            NEWMAN services like finding a venue,
            getting event essentials like lanyards,
            booking flights or accommodation.</p>
            <p>The more you use NEWMAN, the more
            elements will become visible on
            the Launchpad.</p>
            <p>You can also add any social media essentials
            you want to access from here as well.</p>";                    
$lang['free_sign_up'] 	= "Free Sign up";
$lang['my_profile'] 	= "My Profile";
$lang['my_account'] 	= "My Account";
$lang['alert'] 	= "Alerts";
$lang['launch'] 	= "launch";

