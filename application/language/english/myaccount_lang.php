<?PHP if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//common lable for my account 
$lang['myaccount_my_profile'] 			= "My Profile";

//-----------contact details label-------------//
$lang['myaccount_contact_details'] 		= "Contact Details";
$lang['myaccount_contact_title'] 		= "Title";
$lang['myaccount_contact_first_name'] 	= "First Name";
$lang['myaccount_contact_surname'] 		= "Surname";
$lang['myaccount_contact_organisation'] = "Organisation";
$lang['myaccount_contact_email'] 		= "Email";
$lang['myaccount_contact_phone'] 		= "Phone";
$lang['myaccount_contact_fax'] 			= "Fax";
$lang['myaccount_contact_mobile'] 		= "Mobile";
$lang['myaccount_contact_address'] 		= "Address";
$lang['myaccount_contact_suburb_city']  = "Suburb/City";
$lang['myaccount_contact_state'] 		= "State";
$lang['myaccount_contact_postcode'] 	= "Postcode";
$lang['myaccount_contact_country'] 		= "Country";
$lang['myaccount_contact_website'] 		= "Website";

//-----------account information label-------------//
$lang['myaccount_accinfo'] 						= "Account Information";
$lang['myaccount_accinfo_name'] 				= "Name";
$lang['myaccount_accinfo_operator_id'] 			= "Operator Id";
$lang['myaccount_accinfo_varify_curr_pass']		= "Varify Current Password";
$lang['myaccount_accinfo_new_password'] 		= "New Password";
$lang['myaccount_accinfo_confirm_new_pass']		= "Confirm New Password";

//----------billing information lable------------/
$lang['myaccount_billing'] 					= "Billing";
$lang['myaccount_bill_billing_info'] 		= "Billing Information";
$lang['myaccount_apply_user_address']		= "Apply User Address";
$lang['myaccount_bill_first_name'] 			= "First Name";
$lang['myaccount_bill_surname']				= "Surname";
$lang['myaccount_bill_organisation']		= "Organisation";
$lang['myaccount_bill_email'] 				= "Email";
$lang['myaccount_bill_phone'] 				= "Phone";
$lang['myaccount_bill_fax'] 				= "Fax";
$lang['myaccount_bill_mobile'] 				= "Mobile";
$lang['myaccount_bill_address'] 			= "Address";
$lang['myaccount_bill_suburb_city']  		= "Suburb/City";
$lang['myaccount_bill_state'] 				= "State";
$lang['myaccount_bill_postcode']			= "Postcode";
$lang['myaccount_bill_country'] 			= "Country";

//----------billing information lable------------/
$lang['myaccount_bill_debit_card']	 			= "Direct Debit Card Information";
$lang['myaccount_debitcard_name_on_card']		= "Name on Card";
$lang['myaccount_debitcard_cardit_num'] 		= "Credit Card Number";
$lang['myaccount_debitcard_security_code']		= "Security Code";
$lang['myaccount_debitcard_expiry']				= "Expiry";
$lang['myaccount_debitcard_expiry_month']		= "Expiry Month";
$lang['myaccount_debitcard_expiry_year']		= "Expiry Year";


//-----------my account message --------//
$lang['myaccount_msg_contact_detals']		= "Contact details saved successfully.";
$lang['myaccount_msg_password']				= "Password updated successfully.";
$lang['myaccount_msg_bill_info']			= "Billing information saved successfully.";


