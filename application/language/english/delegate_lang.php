<?PHP if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*--------home page (header area) ---------*/
$lang['invitation'] 		= "Invitation";
$lang['invitation_txt'] 	= "Invitation Details";
$lang['details'] 		    = "Details";
$lang['type'] 		        = "Type";
$lang['quantity'] 		    = "Quantity";
$lang['waitlist_txt'] 		= "Waitlist Details";
$lang['booker_txt'] 		= "Booker Details";
$lang['attendee_txt'] 		= "Attendee Details";
$lang['who_else_txt'] 		= "Who elese is attending this event?";
$lang['front_booker_infobar_txt'] 		= '<p>The booker is the person responding to the invitation and the invoice is addressed to.</p>';
$lang['front_attendee_infobar_txt'] 	= '<p>The attendee is the person attending the event.The below details will appear on the each tickets.</p>';


