<?php if (!defined('BASEPATH')) { exit('No direct script access allowed');}
/*
====================================================================================================
  * 
  * @description: This model is use to common functin use like insert, update, delete, select etc 
  * @auther: lokendra meena
  * @email: lokendrameena@cdnsol.com
  * @create date: 27-Nov-2013
  * 
  * 
====================================================================================================*/

class Common_model extends CI_Model { 
  
  
  
  function __construct()
  {
    parent::__construct();
  }
  
  
  /*
   * @description: This function is used getSum
   * 
   */ 
   
  function getMax($table='',$field='',$where=''){
    
    $this->db->select_max($field);
    if(is_array($where) && count($where) > 0){
      $this->db->where($where);
    }
    $query = $this->db->get($table);
    $result=$query->result();
    return $result;
  }
  
  /*
   * @description: This function is used getSum
   * 
   */ 
  
  function getSum($table='',$field='',$where=''){
    $result = false;
    if($table != '' && $field !=''){
      $this->db->select_sum($field);
      if(is_array($where) && count($where) > 0){
        $this->db->where($where);
      }
      $query = $this->db->get($table);
      $result=$query->result();
    }
    return $result;
  }
  
  /*
   * @description: This function is used countResult
   * 
   */ 
  
  function countResult($table='',$field='',$value='', $limit=0){
  
    if(is_array($field)){
        $this->db->where($field);
    }
    elseif($field!='' && $value!=''){
      $this->db->where($field, $value);
    }
    $this->db->from($table);
    
    if($limit >0){
      $this->db->limit($limit);
    }
    
     $res= $this->db->count_all_results();
    // echo $this->db->last_query();
     return $res;
     
  }
  
  /*
   * @description: This function is used countResultFirstInsert
   * 
   */ 
  
  function countResultFirstInsert($table,$field){
  
    $this->db->select('*');
    $this->db->from($table);
    $this->db->where($field);
    $query = $this->db->get();
    if ($query->num_rows()) 
    {
      $result = $query->row();
    }else
    {
      $result = 0;
    }
     return $result;
  }
  
  
  /*
   * @description: This function is used getDataFromMixTabel
   * 
   */ 
   
  function getDataFromMixTabel($table='', $field='*',  $where="", $orderBy='', $limit='',$retrunRow=false ){
    $sql = 'SELECT '.$field.' FROM '.$table.'  '.$where.' '.$orderBy.' '.$limit.' ' ;
    $query = $this->db->query($sql);
    //echo $this->db->last_query();
    if($retrunRow)
      return $query->num_rows();
    else 
      return $query->result();
  }
  
  function runQuery($sql=''){
    if($sql!=''){
      $result = $this->db->query($sql);
      //echo $this->db->last_query();
      return $result;
    }
    return false;
  }
  
  
  /*
   * @description: This function is used getDataFromTabelWhereIn
   * 
   */ 
  
  function getDataFromTabelWhereIn($table='', $field='*',  $whereField='', $whereValue='', $orderBy='', $order='ASC', $whereNotIn=0){
     $this->db->select($field);
     $this->db->from($table);
     
    if($whereNotIn > 0){
      $this->db->where_not_in($whereField, $whereValue);
    }else{
      $this->db->where_in($whereField, $whereValue);
    }
    
    if(is_array($orderBy) && count($orderBy)){
      /* $orderBy treat as where condition if $orderBy is array  */
      $this->db->where($orderBy);
    }
    elseif(!empty($orderBy)){  
      $this->db->order_by($orderBy, $order);
    }
    
    $query = $this->db->get();
    
    $result = $query->result_array();
    if(!empty($result)){
      return  $result;
    }
    else{
      return FALSE;
    }
    
  }
  
  
  function getDataFromTabelWhereOr($table='', $field='*',  $whereString='', $orderBy='', $order='ASC'){
    
     $this->db->select($field);
     $this->db->from($table);
    
    $this->db->where($whereString);
    
    if($orderBy!=""){
      $this->db->order_by($orderBy, $order);
    } 
    
    $query = $this->db->get();
    $result = $query->result();
    if(!empty($result)){
      return  $result;
    }
    else{
      return FALSE;
    }
  }
  
  /*
   * @description: This function is used getDataFromTabelWhereWhereIn
   * 
   */
  
  function getDataFromTabelWhereWhereIn($table='', $field='*',  $where='',  $whereinField='', $whereinValue='', $orderBy='', $whereNotIn=0,$dataType="object"){
    
    $this->db->select($field);
    $this->db->from($table);
     
    if(is_array($where)){
      $this->db->where($where);
    }
    
    if($whereNotIn > 0){
      $this->db->where_not_in($whereinField, $whereinValue);
    }else{
      $this->db->where_in($whereinField, $whereinValue);
    }
    
    if(!empty($orderBy)){  
      $this->db->order_by($orderBy);
    }
    
    $query = $this->db->get();
    //echo $this->db->last_query();
    
    if($dataType=="object"){
      $result = $query->result();
    }else{
      $result = $query->result_array();
    } 
    if(!empty($result)){
      return  $result;
    }
    else{
      return FALSE;
    }
  }
  
  /*
   * @description: This function is used getDataFromTabel
   * 
   */
  
  function getDataFromTabel($table='', $field='*',  $whereField='', $whereValue='', $orderBy='', $order='ASC', $limit=0, $offset=0, $resultInArray=false  ){
    
    
     $this->db->select($field);
     $this->db->from($table);
     
    if(is_array($whereField)){
      $this->db->where($whereField);
    }elseif(!empty($whereField) && $whereValue != ''){
      $this->db->where($whereField, $whereValue);
    }
  
    if(!empty($orderBy)){  
      $this->db->order_by($orderBy, $order);
    }
    if($limit > 0){
      $this->db->limit($limit,$offset);
    }
    $query = $this->db->get();
    
    if($resultInArray){
      $result = $query->result_array();
    }else{
      $result = $query->result();
    }
    
        
    if(!empty($result)){
      return  $result;
    }
    else{
      return FALSE;
    }
    
  }
  
  
  /*
   * @description: This function is used getLikeDataFromTabel
   * 
   */
  
  function getLikeDataFromTabel($table='', $field='*',  $like='', $where='', $orderBy='', $order='ASC', $limit=0 ){
    //echo $table.', '.$field.', '.$whereField.', '.$whereValue.', '.$orderBy;
     $this->db->select($field);
     $this->db->from($table);
     
    if(is_array($like)){
      $this->db->like($like);
    }elseif(is_array($where)){
      $this->db->where($where);
    }
    if(!empty($orderBy)){  
      $this->db->order_by($orderBy, $order);
    }
    if($limit >0){
      $this->db->limit($limit);
    }
    $query = $this->db->get();
    
    $result = $query->result();
    if(!empty($result)){
      return  $result;
    }
    else{
      return FALSE;
    }
  }
  
  /*
   * @description: This function is used addDataIntoTabel
   * 
   */
  
  function addDataIntoTabel($table='', $data=array()){
    if($table=='' || !count($data)){
      return false;
    }
    else{ 
      $inserted = $this->db->insert($table , $data);
      //echo $this->db->last_query();
      if($inserted){
        $ID = $this->db->insert_id();
        //echo "done with insert_id ". $ID."pre";
        //$ID =0;
        if(!($ID > 0)){
          $sql='SELECT LASTVAL() as ins_id';
          $res = $this->db->query($sql);
          $res =$res->result_array();
          $ID=$res[0]['ins_id'];
        }
      }
      $ID=($ID>0)?$ID:0;
      return $ID;
    }
  }
  
    /*
   * @description: This function is used addMultipleDataIntoTabel
   * 
   */
  
  function addMultipleDataIntoTabel($table='', $data=array()){
    if($table=='' || !count($data)){
      return false;
    }
    else{ 
      $inserted = $this->db->insert_batch($table , $data);
      //echo $this->db->last_query();
      if($inserted){
        $ID = $this->db->insert_id();
        //echo "done with insert_id ". $ID."pre";
        //$ID =0;
        if(!($ID > 0)){
          $sql='SELECT LASTVAL() as ins_id';
          $res = $this->db->query($sql);
          $res =$res->result_array();
          $ID=$res[0]['ins_id'];
        }
      }
      $ID=($ID>0)?$ID:0;
      return $ID;
    }
  }
  
  /*
   * @description: This function is used updateDataFromTabel
   * 
   */
   
  function updateDataFromTabel($table='', $data=array(), $field='', $ID=0){
    
    if(empty($table) || !count($data)){
      return false;
    }
    else{
      if(is_array($field)){
        
        $this->db->where($field);
      }else{
        $this->db->where($field , $ID);
        
      }
      return $this->db->update($table , $data);
      //echo $this->db->last_query();
    }
  }
  
  
  
  
  
  /*
   * @description: This function is used updateDataFromTabelWhereIn
   * 
   */
  
  function updateDataFromTabelWhereIn($table='', $data=array(), $where=array(), $whereInField='', $whereIn=array(), $whereNotIn=false){
    
    if(empty($table) || !count($data)){
      return false;
    }
    else{
      if(is_array($where) && count($where) > 0){
        
        $this->db->where($where);
      }
      
      if(is_array($whereIn) && count($whereIn) > 0 && $whereInField != ''){
        if($whereNotIn){
          $this->db->where_not_in($whereInField,$whereIn);
        }else{
          $this->db->where_in($whereInField,$whereIn);
        }
      }
      return $this->db->update($table , $data);
    }
  }
  
  
  
  /*
   * @description: This function is used insertBatch
   * 
   */
   
   
  function insertBatch($table='', $data=array()){
    if($table=='' || !count($data)>0){
      return false;
    }
    else{ 
      $this->db->insert_batch($table, $data); 
      return $this->db->insert_id();
    }
  }
  
  
  /*
   * @description: This function is used deleteRowFromTabel
   * 
   */
   
  function deleteRowFromTabel($table='', $field='', $ID=0, $limit=0){
     
    $Flag=false;
    if($table!='' && $field!=''){
      if(is_array($ID) && count($ID)){
        $this->db->where_in($field ,$ID);
      }elseif(is_array($field) && count($field) > 0){
        $this->db->where($field);
      }else{
        $this->db->where($field , $ID);
      }
      if($limit >0){
        $this->db->limit($limit);
      }
      if($this->db->delete($table)){
        $Flag=true;
      }
    }
    //echo $this->db->last_query();die;
    return $Flag;
  }
  
  /*
   * @description: This function is used deletelWhereWhereIn
   * 
   */
   
   
  function deletelWhereWhereIn($table='', $where='',  $whereinField='', $whereinValue='', $whereNotIn=0){
    
    if(is_array($where)){
      $this->db->where($where);
    }
    
    if($whereNotIn > 0){
      $this->db->where_not_in($whereinField, $whereNotIn);
    }else{
      $this->db->where_in($whereinField, $whereinValue);
    }
    
    if($this->db->delete($table)){
        return true;
    }else{
      return false;
    }
  }
  
  /*
   * @description: This function is used deleteDataFromTabel
   * 
   */
   
   
  function deleteDataFromTabel($table='', $where=''){
    $SQl='DELETE FROM "'.$table.'" '.$where;
    $result = $this->db->query($SQl);
  }
  
  /*
   * @description: This function is used deleteRow
   * 
   */

  function deleteRow($table,$where)
  {
    $this->db->delete($table, $where);
    return true;
  }
  
  /*
   * @description: This function is used executeQuery
   * 
   */
   
  function executeQuery($qureyForexection){
    $query = $this->db->query($qureyForexection);
    $result = $query->result_array();
    if(!empty($result)){
      return $result[0];  
    }else{
      return FALSE;
    }
  }


    /*
   * @description: This function is used to get row count
   * 
   */ 
  
  function countRowsTable($table,$field){ 
    $this->db->select('*');
    $this->db->from($table);
    $this->db->where($field);
    $query = $this->db->get();
    return $query->num_rows();
  }
  
  
  
/* END  */
  
}
