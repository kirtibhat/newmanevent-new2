<!-- After loggedIn pages tempale --> 
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <?php $pagSegment = $this->uri->segment(2); ?>
    <title>Newman Events</title>
    <meta name="viewport" content="width=420, maximum-scale=1, user-scalable=yes">
    <link rel="shortcut icon" href="<?php echo base_url(); ?>themes/assets/images/favicon.ico" />

<!--
    <link type="text/less" rel="stylesheet/less" href="<?php //echo base_url(); ?>themes/assets/css/style.less" media="screen">
-->



   <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>themes/assets/output_css/master.css" media="screen">



    <script type="text/javascript"> var baseUrl = '<?php echo base_url(); ?>';</script>
    <script type="text/javascript" src="<?php echo $system_js_path . 'jquery-2.1.3.js'; ?>"></script>

<!--
    <script type="text/javascript" src="<?php //echo $js_path . 'compiler.js'; ?>"></script>
-->

    <script type="text/javascript" src="<?php echo $system_js_path . 'common.js'; ?>"></script>
    <script type="text/javascript" src="<?php echo $system_js_path . 'event_custom.js'; ?>"></script>
    <script type="text/javascript" src="<?php echo $js_path . 'js.js'; ?>"></script>
    <script type="text/javascript" src="<?php echo $js_path . 'parsley.js'; ?>"></script>
    <script type="text/javascript" src="<?php echo $js_path . 'jquery-migrate-1.2.1.min.js'; ?>"></script>
    <?php if($pagSegment=='invitations') { ?>
    <script type="text/javascript" src="<?php echo $js_path . 'pagejs/jquery.validate.min.js'; ?>"></script>
    <?php } ?>
</head>
<!-- After loggedIn Header --> 
<?php $userId       = isLoginUser(); ?>
<?php $user_session = LoginUserDetails();?>
<?php
// Get data from user table 
$where['id'] = $userId; 
$userData = getUserDataById($userId);
if(!empty($userData)) {
    $user_session = $userData[0];
}
?>
<?php
$whereSocialMedia   = array('event_id' => @$eventdetails->event_id);
$eventSocialMedia   = getDataFromTabel('event_social_media','website',$whereSocialMedia);
$eventSocialMedia   = $eventSocialMedia[0];

/* Set theme colors */
$secondary_color        = (!empty($eventdetails->secondary_color)) ? $eventdetails->secondary_color : '';
$primary_color          = (!empty($eventdetails->primary_color)) ? $eventdetails->primary_color : '';
$event_theme_screen_bg  = (!empty($eventdetails->event_theme_screen_bg)) ? $eventdetails->event_theme_screen_bg : '';
$highlighted_color      = (!empty($eventdetails->event_theme_link_highlighted_color)) ? $eventdetails->event_theme_link_highlighted_color : '';

$primarycolor           = 'color:#'.$primary_color;
$primarycolorbg         = 'background:#'.$primary_color;

$secondarycolor         = 'color:#'.$secondary_color;
$secondarycolorbg       = 'background:#'.$secondary_color;

$eventthemescreencolor  = 'color:#'.$event_theme_screen_bg;
$eventthemescreenbg     = 'background:#'.$event_theme_screen_bg;

$highlightedcolor       = 'color:#'.$highlighted_color;
$highlightedcolorbg     = 'background:#'.$highlighted_color;

$menuLinkColorDefault   = 'color:#A5A5A8';
$menuBackgroundDefault  = 'background:#ffffff';

$homeLink=''; $manageinvitation=''; $termsncondition=''; $contactus='';
if($this->uri->segment(2)=='invitation') { $homeLink = $highlightedcolor; $manageinvitation = $menuLinkColorDefault; 
    $termsncondition    = $menuLinkColorDefault;
    $contactus          = $menuLinkColorDefault;
}
if($this->uri->segment(2)=='manageinvitation' || $this->uri->segment(2)=='modifyAttendee') { $homeLink = $menuLinkColorDefault; $manageinvitation = $highlightedcolor; 
    $termsncondition    = $menuLinkColorDefault;
    $contactus          = $menuLinkColorDefault;
}
if($this->uri->segment(2)=='termsncondition') { $homeLink = $menuLinkColorDefault; $manageinvitation = $menuLinkColorDefault; 
    $termsncondition    = $highlightedcolor;
    $contactus          = $menuLinkColorDefault;
}
if($this->uri->segment(2)=='contactus') { $homeLink = $menuLinkColorDefault; $manageinvitation = $menuLinkColorDefault; 
    $termsncondition    = $menuLinkColorDefault;
    $contactus          = $highlightedcolor;
}

?>
<?php if($this->uri->segment(1)=='events') { $cls='mB10'; }else { $cls='header'; }  ?>  

<body class="flexUse">
<?php if($this->uri->segment(1)=='events') { ?>
<div class="headWithout_login mB10 header beforeLogin" id="header_demo_id" >
<div class="container">
  <div class="top_bar">

      <div class="navToggleButton">
        <button class="navbar-toggle" type="button"> <img src="<?php echo IMAGE; ?>toggle.png"> </button>
      </div>
    <div class="frontNavigation managers_nav top_nav Menuresponsive delegate_nav">
      <ul>
        <li><a href="<?php echo base_url(); ?>events/invitation/<?php echo @$eventdetails->event_url; ?>" style="<?php echo $homeLink;?>"><?php echo lang('home_header_home'); ?></a></li>
        
        <li>
            <a href="<?php echo base_url(); ?>events/manageinvitation/<?php echo $eventId; ?>" style="<?php echo $manageinvitation;?>">Manage Invitation
            </a>
        </li>   
        
        <li><a href="<?php echo base_url(); ?>events/termsncondition" style="<?php echo $termsncondition;?>">Terms and Conditions</a></li>
        <li><a href="http://<?php echo @$eventSocialMedia->website; ?>" style="<?php echo $menuLinkColorDefault;?>">Client Website</a></li>
        <li><a href="<?php echo base_url(); ?>events/contactus" style="<?php echo $contactus;?>"><?php echo lang('home_header_contact'); ?></a></li>
      </ul>
    </div>
    
    <div class="top_nav">

    </div>
  </div>
</div>
</div>    
<?php }else { ?>        
<div class="header afterLogin navnormal" id="header_demo_id">
  <div class="container">
    <div class="top_bar">
      <div class="logo"> <a href="<?php echo base_url(); ?>"><img alt="Logo image" src="<?php echo IMAGE; ?>dashboard_logo_managers.png"></a>
        <div class="navToggleButton">
          <button class="navbar-toggle" type="button"> <img src="images/toggle.png"> </button>
        </div>
      </div>
      <div class="frontNavigation top_nav Menuresponsive supplier_nav">
        <ul>
            <li><a href="<?php echo base_url(); ?>"><?php echo lang('home_header_home'); ?></a></li>
            <li><a href="#"><?php echo lang('home_header_features'); ?></a></li>
            <li><a href="#"><?php echo lang('home_header_packages'); ?></a></li>
            <li><a href="#"><?php echo lang('home_header_about_us'); ?></a></li>
            <li><a href="#"><?php echo lang('home_header_contact'); ?></a></li>
        </ul>
      </div>
      
      <div class="userLoginBlock"> <a class="drpTrg" href="#">
        <div class="user_img"> 
            <?php
                /* Set user image icon on top header right position */
                $user_icon = $this->session->userdata('userLogo');
                if(!empty($user_icon)){
                    $user_icon_path = $user_icon;
                }else {
                    $user_icon_path = IMAGE.'user.jpg';
                }
                
                if(!empty($user_session->userLogo)) {
                    $user_icon_path = base_url().'media/user_logo/'.$user_session->userLogo;
                }
			?>
            <img alt="Logo image" src="<?php echo $user_icon_path; ?>"> 
        </div>
      <div class="user_name">
        <h4 class="font-small user_profile_name">
            <?php 
                //session from FB & G+ login 
                //$user_display_name = $this->session->userdata('first_name').' '.$this->session->userdata('last_name');
                //session login manualy
                $user_display_name = '';
                $company = (!empty($user_session->company_name)) ? $user_session->company_name : '';
               if(!empty($user_session)) {
                    if($user_session->newman_nickname != '' ){
                        $user_display_name = $user_session->newman_nickname;
                        } else {
                                if(!empty($user_session->firstname)) {
                                    $user_display_name .= $user_session->firstname;
                                } 
                                if(!empty($user_session->lastname)) {
                                    $user_display_name .= ' '.$user_session->lastname;
                                }
                    }
                }
                echo (strlen($user_display_name) > 10) ? substr($user_display_name,0, 10).'..' : $user_display_name; 
            ?>
        </h4>
        <span class="user_designation font-xsmall"><?php echo $company;  ?></span> </div>
      </a>
      <?php if($this->uri->segment(1) == 'profile') { $classActive = 'active'; }else { $classActive = ''; } ?>
      <div class="profileMenu mR7">
        <ul>
          <li class="<?php echo $classActive; ?>"><a href="<?php echo base_url(); ?>profile"><?php echo lang('my_profile'); ?></a></li>
          <li><a href="#"><?php echo lang('my_account'); ?></a></li>
          <hr/>
          <li><a href="#"><?php echo lang('alert'); ?></a></li>
          <hr/>
          <li><a href="<?php echo base_url(); ?>home/logout"><?php echo lang('home_header_logout'); ?></a></li>
        </ul>
      </div>
    </div>
      
      
      <div class="">
        <div class="topBarNavigation">
          <ul>
            <li><a href="#"><span class="medium_icon"><i class="icon-launch"></i></span>launch</a></li>
            <li><a href="#"><span class="medium_icon"><i class="icon-help"></i></span>
            <div style="clear:both;"></div>
            help</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="header afterLogin navresponsive" id="header_demo_id">
  <div class="container">
    <div class="top_bar">
      <div class="logo"> <a href="<?php echo base_url(); ?>"><img alt="Logo image" src="<?php echo IMAGE; ?>dashboard_logo_managers.png"></a>
      </div>
      
      <div class="userLoginBlock"> <a class="drpTrg" href="#">
        <div class="user_img"> 
            <?php
                /* Set user image icon on top header right position */
                $user_icon = $this->session->userdata('userLogo');
                if(!empty($user_icon)){
                    $user_icon_path = $user_icon;
                }else {
                    $user_icon_path = IMAGE.'user.jpg';
                }
                
                if(!empty($user_session->userLogo)) {
                    $user_icon_path = base_url().'media/user_logo/'.$user_session->userLogo;
                }
			?>
            <img alt="Logo image" src="<?php echo $user_icon_path; ?>"> 
        </div>
        
        <div class="user_name">
        <h4 class="font-small user_profile_name">
            <?php 
                //session from FB & G+ login 
                //$user_display_name = $this->session->userdata('first_name').' '.$this->session->userdata('last_name');
                //session login manualy
                $user_display_name = '';
                $company = (!empty($user_session->company_name)) ? $user_session->company_name : '';
               if(!empty($user_session)) {
                    if($user_session->newman_nickname != '' ){
                        $user_display_name = $user_session->newman_nickname;
                        } else {
                                if(!empty($user_session->firstname)) {
                                    $user_display_name .= $user_session->firstname;
                                } 
                                if(!empty($user_session->lastname)) {
                                    $user_display_name .= ' '.$user_session->lastname;
                                }
                    }
                }
                echo (strlen($user_display_name) > 10) ? substr($user_display_name,0, 10).'..' : $user_display_name; 
            ?>
        </h4>
        <span class="user_designation font-xsmall"><?php echo $company;  ?></span> 
      </div>
        
        </a>
        <div class="profileMenu">
          <?php if($this->uri->segment(1) == 'profile') { $classActive = 'active'; }else { $classActive = ''; } ?>
          <ul>
            <li><a href="<?php echo base_url(); ?>"><?php echo lang('home_header_home'); ?></a></li>
            <li><a href="#"><?php echo lang('home_header_features'); ?></a></li>
            <li><a href="#"><?php echo lang('home_header_packages'); ?></a></li>
            <li><a href="#"><?php echo lang('home_header_about_us'); ?></a></li>
            <li><a href="#"><?php echo lang('home_header_contact'); ?></a></li>
            <li class="<?php echo $classActive; ?>"><a href="<?php echo base_url(); ?>profile"><?php echo lang('my_profile'); ?></a></li>
            <li><a href="#"><?php echo lang('my_account'); ?></a></li>
            <hr/>
            <li><a href="#"><?php echo lang('alert'); ?></a></li>
            <hr/>
            <li><a href="<?php echo base_url(); ?>home/logout"><?php echo lang('home_header_logout'); ?></a></li>
          </ul>  
        </div>
      </div>
      
    </div>
  </div>
</div>
<?php } ?>

<!-- dashboard main-->
<?php if($this->uri->segment('1') != 'profile') {?>
<div class="mainNavWrapper">
<div class="container">
  <div class="mainNavigation manager_dashboard">
    <ul>
      <li> <a href="<?php echo base_url('dashboard'); ?>" class="dashboard_link <?php echo ($module=="dashboard" && $moduleMethod=="index")? 'active' : ''; ?>"> <span class="xxlarge_icon"> <i class="icon-home"></i> </span> <span class="db_linktitle">My Events</span> </a> </li>
      <li> <a href="javascript:void(0);" class="dashboard_link"> <span class="xxlarge_icon"> <i class="icon-quick_list"></i> </span> <span class="db_linktitle">Quick List</span> </a> </li>
      <li> <a href="javascript:void(0);" class="dashboard_link"> <span class="xxlarge_icon"> <i class="icon-resources"></i> </span> <span class="db_linktitle">Event Resources</span> </a> </li>
      <li> <a href="javascript:void(0);" class="dashboard_link <?php echo  getActiveMenu('event'); ?> "> <span class="xxlarge_icon"> <i class="icon-edit"></i> </span> <span class="db_linktitle">Event Dashboard</span> </a> </li>
      <li> <a href="javascript:void(0);" class="dashboard_link"> <span class="xxlarge_icon"> <i class="icon-reports"></i> </span> <span class="db_linktitle">Reports</span> </a> </li>
      <li> <a href="javascript:void(0);" class="dashboard_link"> <span class="xxlarge_icon"> <i class="icon-admin"></i> </span> <span class="db_linktitle">Admin</span> </a> </li>
    </ul>
  </div>
</div>
</div>
<?php }?>    
    
    <?php
    /*
     * Load event menus 
     */ 
    
    
	
    /*
     * load content area
     */ 
    echo $content
  ?>    
</div>
<?php
$ua = $_SERVER["HTTP_USER_AGENT"];
/*
 * Mobile
 */ 
$android    = strpos($ua, 'Android') ? true : false;
$blackberry = strpos($ua, 'BlackBerry') ? true : false;
$iphone     = strpos($ua, 'iPhone') ? true : false;
$palm       = strpos($ua, 'Palm') ? true : false;
$ipad       = strpos($ua, 'iPad') ? true : false;
/*
 * Desktop
 */ 
$linux  = strpos($ua, 'Linux') ? true : false;
$mac    = strpos($ua, 'Macintosh') ? true : false;
$win    = strpos($ua, 'Windows') ? true : false;
?>  
<script type="text/javascript" src="<?php  echo $js_path . 'bootstrap.js'; ?>"></script>
<script type="text/javascript" src="<?php  echo $system_js_path . 'bootbox.js'; ?>"></script>
<script type="text/javascript" src="<?php  echo $system_js_path . 'jquery.fs.stepper.min.js'; ?>"></script>

<script type="text/javascript" src="<?php  echo $js_path . 'parsley.js'; ?>"></script>

<script type="text/javascript" src="<?php  echo $system_js_path.'1_10_3_jquery_ui.js'; ?>"></script>
<script type="text/javascript" src="<?php  echo $system_js_path.'jquery-ui-timepicker-addon.js'; ?>"></script>
<script type="text/javascript" src="<?php  echo $js_path . 'common_free.js'; ?>"></script>
<?php if($android==true || $blackberry==true || $iphone==true || $palm==true || $ipad==true){ ?>
<script src="<?php echo base_url('themes/assets/js/touch.js') ?>" type="text/javascript"></script>
<?php } ?>

<!-----message showing custome popup------>
<?php  $this->load->view('common/custom_popup'); ?>

<!-----message showing custome popup------>
<?php  $this->load->view('common/loader_popup'); ?>

<!-----load confirm popup div------>
<?php $this->load->view('common/custom_confirm_popup'); ?>

<!------ajax open popup append html start----->
<div id="ajax_open_popup_html"></div>

<?php $this->load->view('footer'); ?>


<script type="text/javascript" src="<?php echo $js_path . 'custom.js'; ?>"></script>
<?php if($pagSegment!='invitations') { ?>
<script type="text/javascript" src="<?php echo $js_path . 'pagejs/jquery.validate.min.js'; ?>"></script>
<?php } ?>
<script type="text/javascript" src="<?php echo $js_path . 'pagejs/inner-modules.js'; ?>"></script>
<script type="text/javascript" src="<?php echo $js_path . 'pagejs/form_validation.js'; ?>"></script>
<?php if($this->uri->segment('1') == 'profile') {?>
<script type="text/javascript" src="<?php echo $js_path . 'pagejs/profile/profile.js'; ?>"></script>
<?php }?>
<?php if($this->uri->segment(1)=='events') {  ?>
    <script type="text/javascript" src="<?php echo $js_path . 'pagejs/event_frontend_home.js'; ?>"></script>
<?php } ?>
<script>
var newmanUrl = '<?php echo $this->config->item('newmanUrl');?>';
</script>
<script type="text/javascript">
$(function(){
    $('.custom-select').customSelect();
});
</script>
</body>
</html>
