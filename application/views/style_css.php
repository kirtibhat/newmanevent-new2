<?php
/*---------------- css file to set dynamically form color ----------------*/
  $customizeForms= (isset($customize_form) && !empty($customize_form))?$customize_form:'';
  $textColor='';
  $headerbg='';
  $outerbg='';
  $innerbg='';
  if(!empty($customizeForms))
  {
		if($customizeForms[0]->form_text_color=='gray')
		{
			//to set text color gray
			$textColor='#545454;';
		}
		else if($customizeForms[0]->form_text_color=='black')
		{
			$textColor='#000;';
		}
		$headerbg=$customizeForms[0]->header_bar_background;
		$outerbg=$customizeForms[0]->background1_color;
		$innerbg=$customizeForms[0]->form_background;
		
  }
 ?>
 
<style>
 /*********************form text color ***********************/	
.paragraph, mendotryfield, .formoentform_container input, .formoentform_container textarea,  .filter-option{ color:<?php echo $textColor;?>;}

.innerbody .paragraph{ color:<?php echo $textColor; ?>; }
.frontend_select .filter-option { color:<?php echo $textColor; ?>; }

.usertooltio { color:<?php echo $textColor; ?>;}
.accorianbg .accordion-body .innerbody { color:<?php echo $textColor; ?>;}

.formoentform_container input::-webkit-input-placeholder{ color:<?php echo $textColor; ?>;}
.formoentform_container textarea::-webkit-input-placeholder{ color:<?php echo $textColor; ?>;}
.ui-spinner-input{color:<?php echo $textColor; ?>;}
.pymtregdetail td {color:<?php echo $textColor; ?>;}
.paymentcond {color:<?php echo $textColor; ?>;}
 body {color:<?php echo $textColor; ?>;}
 
 /*********************form header bar color ***********************/
 .accorianbg .accordion-group { background:<?php echo $headerbg; ?>;}
 .usercontainer { background:<?php echo $headerbg; ?>;}

 /*********************form outer bg ***********************/
 
 
 .signupbg{ background: <?php echo $outerbg; ?>}
  
 
 /*********************form inner bg ***********************/
 
 .accorianbg .accordion-body{ background: <?php echo $innerbg; ?>; }
 
 .registration_table .regist_row{ background:<?php echo $innerbg; ?>}
 .registration_table .regist_row:nth-child(even) { background:<?php echo $innerbg; ?>}
 .registration_table .regist_row:nth-child(odd) { background: <?php echo $innerbg; ?>}
 .pymtregdetail table { background:<?php echo $innerbg; ?>}
 .pymtregdetail tr td:nth-child(2) { background: <?php echo $innerbg; ?>}

</style>


