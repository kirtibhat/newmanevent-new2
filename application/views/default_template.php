<?PHP if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
//load header
echo $head;
?>
<body>
<?php 
$this->load->view('common/custom_popup'); 
$this->load->view('style_css');
// load content area
echo $content;
?>
<!----show global message----->
<?php if(get_global_messages('msg')) {  ?>
	<script>
	custom_popup('<?php echo get_global_messages('msg');  ?>','<?php echo get_global_messages('is_success');  ?>');	
	</script>
	
<?php } ?>
</body>
</html>
