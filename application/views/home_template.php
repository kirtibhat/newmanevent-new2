<?PHP if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Newman Events</title>
    <meta name="viewport" content="width=420, maximum-scale=1, user-scalable=yes">
    <link rel="shortcut icon" href="<?php echo base_url(); ?>themes/assets/images/favicon.ico" />

<!--
    <link type="text/less" rel="stylesheet/less" href="<?php //echo base_url(); ?>themes/assets/css/style.less" media="screen">
-->


    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>themes/assets/output_css/master.css" media="screen">

    <script type="text/javascript"> var baseUrl = '<?php echo base_url(); ?>';</script>
    <script type="text/javascript" src="<?php echo $js_path . 'jquery-1.9.1.min.js'; ?>"></script>

<!--
    <script type="text/javascript" src="<?php //echo $js_path . 'compiler.js'; ?>"></script>
-->

    <script type="text/javascript" src="<?php echo $js_path . 'bootstrap.js'; ?>"></script>
    <script type="text/javascript" src="<?php echo $js_path . 'common.js'; ?>"></script>
    <script type="text/javascript" src="<?php echo $js_path . 'parsley.js'; ?>"></script>
    <script type="text/javascript" src="<?php echo $js_path . 'jquery-migrate-1.2.1.min.js'; ?>"></script>    
    <script type="text/javascript" src="<?php echo $js_path . 'pagejs/jquery.validate.min.js'; ?>"></script>    
</head>
<?php
//echo '<pre>';
//print_r($eventdetails);die;
$whereSocialMedia       = array('event_id' => @$eventdetails->event_id);
$eventSocialMedia       = getDataFromTabel('nm_event_social_media','website',$whereSocialMedia);
$eventSocialMedia       = $eventSocialMedia[0];

/* Set theme colors */
$secondary_color        = (!empty($eventdetails->secondary_color)) ? $eventdetails->secondary_color : '';
$primary_color          = (!empty($eventdetails->primary_color)) ? $eventdetails->primary_color : '';
$event_theme_screen_bg  = (!empty($eventdetails->event_theme_screen_bg)) ? $eventdetails->event_theme_screen_bg : '';
$highlighted_color      = (!empty($eventdetails->event_theme_link_highlighted_color)) ? $eventdetails->event_theme_link_highlighted_color : '';
$screen_bg              = (!empty($eventdetails->event_theme_screen_bg)) ? $eventdetails->event_theme_screen_bg : '';

$primarycolor           = 'color:#'.$primary_color;
$primarycolorbg         = 'background:#'.$primary_color;

$secondarycolor         = 'color:#'.$secondary_color;
$secondarycolorbg       = 'background:#'.$secondary_color;

$eventthemescreencolor  = 'color:#'.$event_theme_screen_bg;
$eventthemescreenbg     = 'background:#'.$event_theme_screen_bg;

$highlightedcolor       = 'color:#'.$highlighted_color;
$highlightedcolorbg     = 'background:#'.$highlighted_color;

$menuLinkColorDefault   = 'color:#A5A5A8';
$menuBackgroundDefault  = 'background:#ffffff';

$homeLink=''; $manageinvitation=''; $termsncondition=''; $contactus=''; $bodybg=''; 
if($this->uri->segment(2)=='invitation') { $homeLink = $highlightedcolor; $manageinvitation = $menuLinkColorDefault; 
    $termsncondition    = $menuLinkColorDefault;
    $contactus          = $menuLinkColorDefault;
    $bodybg             = 'background:#'.$screen_bg;
}
if($this->uri->segment(2)=='manageinvitation' || $this->uri->segment(2)=='attendeeConfirmation' || $this->uri->segment(2)=='modifyAttendee') { $homeLink = $menuLinkColorDefault; $manageinvitation = $highlightedcolor; 
    $termsncondition    = $menuLinkColorDefault;
    $contactus          = $menuLinkColorDefault;
}
if($this->uri->segment(2)=='termsncondition') { $homeLink = $menuLinkColorDefault; $manageinvitation = $menuLinkColorDefault; 
    $termsncondition    = $highlightedcolor;
    $contactus          = $menuLinkColorDefault;
}
if($this->uri->segment(2)=='contactus') { $homeLink = $menuLinkColorDefault; $manageinvitation = $menuLinkColorDefault; 
    $termsncondition    = $menuLinkColorDefault;
    $contactus          = $highlightedcolor;
}
?>
<?php //echo '<pre>'; print_r($eventSocialMedia->website); ?>
<body class="flexUse" style="<?php echo $bodybg; ?>">
<div class="container top_content" style="display:none;">
	<div class="box">
        	<h1><?php echo lang('home_simple'); ?></h1>
                <?php echo lang('home_simple_text'); ?>
    </div>
    <div class="box">
    		<h1><?php echo lang('home_secure'); ?></h1>
                <?php echo lang('home_secure_text'); ?>
            
    </div>
    <div class="box">
    <div id="hide" class="close">
    <span class="small_icon"> <i class="icon-closeb"></i> </span>
    </div>
    		<h1><?php echo lang('home_surprising'); ?></h1>
                <?php echo lang('home_surprising_text'); ?>
    </div>
</div>
<!-- After loggedIn Header --> 
<?php $userId       = isLoginUser(); ?>
<?php $user_session = LoginUserDetails();?>
<?php
// Get data from user table 
$where['id'] = $userId; 
$userData = getUserDataById($userId);
if(!empty($userData)) {
    $user_session = $userData[0];
}
?>
<?php 
/*
 * Menu setting for events
 */
$classForReferer = ''; $reqParam ='';  
if($this->uri->segment(1)=='events') {
  $classForReferer = 'add_refrer_url_param'; $reqParam ='?q=1'; 
  if(isset($userId) && $userId!=false) {  
?>
<div class="header beforeLogin" id="header_demo_id" style="<?php echo $menuBackgroundDefault; ?>">
<div class="container">
  <div class="top_bar">
<!--
    <div class="logo"> <a href="<?php echo base_url(); ?>"><img alt="Logo image" src="<?php echo IMAGE; ?>dashboard_logo_managers.png"></a>
      <div class="navToggleButton">
        <button class="navbar-toggle" type="button"> <img src="<?php echo IMAGE; ?>toggle.png"> </button>
      </div>
    </div>
--> <!--$homeLink=''; $manageinvitation=''; $termsncondition=''; $contactus='';-->
<div class="navToggleButton">
        <button class="navbar-toggle" type="button"> <img src="<?php echo IMAGE; ?>toggle.png"> </button>
      </div>
    <div class="frontNavigation managers_nav top_nav Menuresponsive delegate_nav">
      <ul>
        <li><a href="<?php echo base_url(); ?>events/invitation/<?php echo @$eventdetails->event_url; ?>" style="<?php echo $homeLink;?>"><?php echo lang('home_header_home'); ?></a></li>
         <li>
            <a href="<?php echo base_url(); ?>events/manageinvitation/<?php echo $eventId; ?>" style="<?php echo $manageinvitation;?>">Manage Invitation
            </a>
        </li>   
        <li><a href="<?php echo base_url(); ?>events/termsncondition" style="<?php echo $termsncondition;?>">Terms and Conditions</a></li>
        <li><a href="http://<?php echo @$eventSocialMedia->website; ?>" style="<?php echo $menuLinkColorDefault;?>">Client Website</a></li>
        <li><a href="<?php echo base_url(); ?>events/contactus" style="<?php echo $contactus;?>"><?php echo lang('home_header_contact'); ?></a></li>
      </ul>
    </div>
    <?php if($this->uri->segment(1)!='events') {  ?>  
    <div class="userLoginBlock"> <a class="drpTrg" href="#">
        <div class="user_img"> 
            <?php
                /* Set user image icon on top header right position */
                $user_icon = $this->session->userdata('userLogo');
                if(!empty($user_icon)){
                    $user_icon_path = $user_icon;
                }else {
                    $user_icon_path = IMAGE.'user.jpg';
                }
                
                if(!empty($user_session->userLogo)) {
                    $user_icon_path = base_url().'media/user_logo/'.$user_session->userLogo;
                }
			?>
            <img alt="Logo image" src="<?php echo $user_icon_path; ?>"> 
        </div>
      <div class="user_name">
        <h4 class="font-small user_profile_name">
            <?php 
                //session from FB & G+ login 
                //$user_display_name = $this->session->userdata('first_name').' '.$this->session->userdata('last_name');
                //session login manualy
                $user_display_name = '';
                $company = (!empty($user_session->company_name)) ? $user_session->company_name : '';
               if(!empty($user_session)) {
                    if($user_session->newman_nickname != '' ){
                        $user_display_name = $user_session->newman_nickname;
                        } else {
                                if(!empty($user_session->firstname)) {
                                    $user_display_name .= $user_session->firstname;
                                } 
                                if(!empty($user_session->lastname)) {
                                    $user_display_name .= ' '.$user_session->lastname;
                                }
                    }
                }
                echo (strlen($user_display_name) > 10) ? substr($user_display_name,0, 10).'..' : $user_display_name; 
            ?>
        </h4>
        <span class="user_designation font-xsmall"><?php echo $company;  ?></span> </div>
      </a>
      <?php if($this->uri->segment(1) == 'profile') { $classActive = 'active'; }else { $classActive = ''; } ?>
      <div class="profileMenu mR7">
        <ul>
          <li class="<?php echo $classActive; ?>"><a href="<?php echo base_url(); ?>profile"><?php echo lang('my_profile'); ?></a></li>
          <li><a href="#"><?php echo lang('my_account'); ?></a></li>
          <hr/>
          <li><a href="#"><?php echo lang('alert'); ?></a></li>
          <hr/>
          <li><a href="<?php echo base_url(); ?>home/logout"><?php echo lang('home_header_logout'); ?></a></li>
        </ul>
      </div>
    </div>
    <?php } ?>
    
    <div class="top_nav">
<!--
      <div class="topBarNavigation">
        <ul>
          <li><a href="#"><span class="medium_icon mB7"><i class="icon-launch"></i></span><?php echo lang('launch'); ?></a></li>
         
          <li><a href="#"><span class="medium_icon mB7"><i class="icon-help"></i></span>
          <div style="clear:both;"></div>
          help</a></li>
        </ul>
      </div>
-->
    </div>
  </div>
</div>
</div>
 
<?php }else { ?>        
<!-- Top Header -->
<div class="headWithout_login mB10 header beforeLogin" id="header_demo_id" >
<div class="container">
  <div class="top_bar">
<!--
    <div class="logo"> <a href="<?php echo base_url(); ?>"><img alt="Logo image" src="<?php echo IMAGE; ?>dashboard_logo_managers.png"></a>
      <div class="navToggleButton">
        <button class="navbar-toggle" type="button"> <img src="<?php echo IMAGE; ?>toggle.png"> </button>
      </div>
    </div>
--><!--$homeLink=''; $manageinvitation=''; $termsncondition=''; $contactus='';-->

      <div class="navToggleButton">
        <button class="navbar-toggle" type="button"> <img src="<?php echo IMAGE; ?>toggle.png"> </button>
      </div>
    <div class="frontNavigation managers_nav top_nav Menuresponsive delegate_nav">
      <ul>
        <li><a href="<?php echo base_url(); ?>events/invitation/<?php echo @$eventdetails->event_url; ?>" style="<?php echo $homeLink;?>"><?php echo lang('home_header_home'); ?></a></li>
        
        <?php 
        /*
            $userId = isLoginUser();
            if(isset($userId) && $userId!=false) { 
        ?>
        <li>
            <a href="<?php echo base_url(); ?>events/manageinvitation/<?php echo $eventId; ?>" style="<?php //echo $navigationLinkColor;?>">Manage Invitation
            </a>
        </li> 
        <?php   
            }else { ?>
          <li>  
                <a class="homelogin user_login loginbtn add_refrer_url_param" href="javascript:void(0)" title="Login Button" data-toggle="modal" data-target=".login_popup">Manage Invitation</a>
          </li>  
        <?php } */?>
        <li>
            <a href="<?php echo base_url(); ?>events/manageinvitation/<?php echo $eventId; ?>" style="<?php echo $manageinvitation;?>">Manage Invitation
            </a>
        </li>   
        
        <li><a href="<?php echo base_url(); ?>events/termsncondition" style="<?php echo $termsncondition;?>">Terms and Conditions</a></li>
        <li><a href="http://<?php echo @$eventSocialMedia->website; ?>" style="<?php echo $menuLinkColorDefault;?>">Client Website</a></li>
        <li><a href="<?php echo base_url(); ?>events/contactus" style="<?php echo $contactus;?>"><?php echo lang('home_header_contact'); ?></a></li>
      </ul>
    </div>
    
    <div class="top_nav">
<!--
      <div class="topBarNavigation mT20">
        <a href="<?php echo base_url(); ?>home/freeRegistration<?php echo $reqParam; ?>"><button type="button" class="btn-orange btn"><?php echo lang('free_sign_up'); ?></button></a>
        <a class="homelogin user_login loginbtn <?php echo $classForReferer; ?>" href="javascript:void(0)" title="Login Button" data-toggle="modal" data-target=".login_popup"><?php echo lang('home_header_login') ?></a>
      </div>
-->
    </div>
  </div>
</div>
</div>
<?php } ?>
<!-- Event condition --> 
<?php } else { ?>
<!-- Top Header -->
<div class="header headWithout_login header beforeLogin" id="header_demo_id">
<div class="container">
  <div class="top_bar">
    <div class="logo"> <a href="<?php echo base_url(); ?>"><img alt="Logo image" src="<?php echo IMAGE; ?>dashboard_logo_managers.png"></a>
      <div class="navToggleButton">
        <button class="navbar-toggle" type="button"> <img src="<?php echo IMAGE; ?>toggle.png"> </button>
      </div>
    </div>
    <div class="frontNavigation frontNavigation managers_nav top_nav Menuresponsive">
      <ul>
        <li><a href="<?php echo base_url(); ?>"><?php echo lang('home_header_home'); ?></a></li>
        <li><a href="#"><?php echo lang('home_header_features'); ?></a></li>
        <li><a href="#"><?php echo lang('home_header_packages'); ?></a></li>
        <li><a href="#"><?php echo lang('home_header_about_us'); ?></a></li>
        <li><a href="#"><?php echo lang('home_header_contact'); ?></a></li>
      </ul>
    </div>
    
    <div class="frontNavigation pull-right display_inline loginbtn">
            	<ul>
                	<li><a class="homelogin user_login loginbtn" href="javascript:void(0)" title="Login Button" data-toggle="modal" data-target=".login_popup"><?php echo lang('home_header_login') ?></a></li>                  
                </ul>
            </div>
      <div class="topBtnBar">
        <a href="<?php echo base_url(); ?>home/freeRegistration"><button type="button" class="btn-orange btn"><?php echo lang('free_sign_up'); ?></button></a>
      </div>
  </div>
</div>
</div>
<?php } ?>    
<!--- End top header -->

<?php echo $content;?>
</div>
<!-- content container-->
<?php $this->load->view('common/common_footer'); ?> 
<!--end of footer-->

<!------ajax open popup append html start----->
<div id="ajax_open_popup_html">
</div>
<!------ajax open popup append html end----->

<!-----message showing custome popup start------>
<?php $this->load->view('common/custom_popup'); ?>

<!-----message showing custome popup end------>

<?php 
if($this->uri->segment(1)=='events') { 
    $home_prefix ='home/';
}else {
    $home_prefix ='';
}
?>
    
<!-------login popup div start ------->
<?php $this->load->view($home_prefix.'login_view'); ?>
<!-------login popup div end ------->

<!-------contact_us popup div start ------->
<?php $this->load->view($home_prefix.'contact_us'); ?>

<!-------forgot_password popup  ------->
<?php $this->load->view($home_prefix.'forgot_password'); ?>

<!-------forgot_password mail sent popup ------->
<?php $this->load->view($home_prefix.'mail_sent'); ?> 

<!-------forgot_password mail sent popup ------->
<?php $this->load->view($home_prefix.'reset_success'); ?>

<!-------trouble logging pop up ------->
<?php $this->load->view($home_prefix.'trouble_logging'); ?>

<!-------trouble logging pop up ------->
<?php $this->load->view($home_prefix.'trouble_logging_pop_up'); ?>

<!-------apply free package logging pop up ------->
<?php //$this->load->view('packages/apply_free'); ?>

<!-------apply free package user password pop up ------->
<?php //$this->load->view('packages/user_password'); ?>

<!-------apply free package our terms pop up ------->
<?php //$this->load->view('packages/our_terms.php'); ?>

<!-------apply free package congratulations pop up ------->
<?php //$this->load->view('packages/congratulations_page.php'); ?>

<!-------apply casual package pop up ------->
<?php $this->load->view($home_prefix.'packages/apply_casual'); ?>

<!-------apply solo package pop up ------->
<?php $this->load->view($home_prefix.'packages/apply_solo'); ?>

<!-------apply enterprise package pop up ------->
<?php $this->load->view($home_prefix.'packages/apply_enterprise'); ?>

<!-------pop up comes after filling registration form ------->
<?php $this->load->view($home_prefix.'packages/thanks_popup'); ?>
<script type="text/javascript" src="<?php echo $js_path . 'custom.js'; ?>"></script>
<script type="text/javascript" src="<?php echo $js_path . 'pagejs/accountpages.js'; ?>"></script>
<script type="text/javascript" src="<?php echo $js_path . 'pagejs/form_validation.js'; ?>"></script>
<?php if($this->uri->segment(1)=='events') {  ?>
    <script type="text/javascript" src="<?php echo $js_path . 'pagejs/event_frontend_home.js'; ?>"></script>
<?php } ?>
</body>
<script type="text/javascript">
$(function(){ $('.custom-select').customSelect(); });
</script>
</html>
