<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Event_extras_custom_fields extends CI_Migration {

  public function up()
  {                                                                      
    //add fields                                               
    $this->dbforge->add_field('id int(11) unsigned NOT NULL AUTO_INCREMENT');
    $this->dbforge->add_field('extra_id int(11) NULL DEFAULT NULL');
    $this->dbforge->add_field('field_name varchar(100) NULL DEFAULT NULL');
    $this->dbforge->add_field('field_type varchar(50) NULL DEFAULT NULL');
    $this->dbforge->add_field('default_value text NULL DEFAULT NULL');
    $this->dbforge->add_field("field_status int(11) NULL DEFAULT NULL COMMENT 'for eg: displayed, mandatory etc.. '");  
    $this->dbforge->add_field("is_editable enum('0','1') NOT NULL DEFAULT '0' COMMENT '0 means not editable, 1 means editable '");
    $this->dbforge->add_field("field_create_by enum('0','1') NOT NULL DEFAULT '0' COMMENT '0 means user created, 1 means admin created '"); 
    //add key in the field                                                                                                                  
    $this->dbforge->add_key('id', TRUE);
    //add table with fields
    $this->dbforge->create_table('event_extras_custom_fields', TRUE);  
  }

  public function down()
  {
    
  }
  
} 
