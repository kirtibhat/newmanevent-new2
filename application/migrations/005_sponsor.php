<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Sponsor extends CI_Migration {

	public function up()
	{
		$fields = array(
				'number_complimentary_registrations' => array(
				'type' => 'varchar',
				'constraint' => 45, 
				'null' => TRUE,
				'default' => NULL
			),
				'is_additional_guests' => array(
				'type' => 'boolean',
				'null' => FALSE,
				'default' => 0
			),
				'limit_per_person' => array(
				'type' => 'varchar',
				'constraint' => 45, 
				'null' => TRUE,
				'default' => NULL
			),
				'additi_guest_total_price' => array(
				'type' => 'varchar',
				'constraint' => 45, 
				'null' => TRUE,
				'default' => NULL
			),
				'additi_guest_gst' => array(
				'type' => 'varchar',
				'constraint' => 45, 
				'null' => TRUE,
				'default' => NULL
			),
		   
		);
		
		//add coloum in sponsor table
		$this->dbforge->add_column('sponsor', $fields);
		
		//remove table
		$this->dbforge->drop_table('sponsor_item');
		
		//remove column 	
		$this->dbforge->drop_column('sponsor_side_event', 'complementary_guest');
		$this->dbforge->drop_column('sponsor_side_event', 'complementary_guest_limit');
		$this->dbforge->drop_column('sponsor_side_event', 'additional_guest');
		$this->dbforge->drop_column('sponsor_side_event', 'additional_guest_limit');
		$this->dbforge->drop_column('sponsor_side_event', 'gst');

		$fields = array(
                        'total_price' => array(
								'name' => 'side_event_limit',
								'type' => 'varchar',
								'constraint' => 45, 
								'null' => TRUE,
								'default' => NULL
						),
					);
		//rename column name			
		$this->dbforge->modify_column('sponsor_side_event', $fields);
		
	}

	public function down()
	{
		$this->dbforge->drop_table('sponsor');
	}
	
	
}	
