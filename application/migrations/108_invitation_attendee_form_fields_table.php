<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_invitation_attendee_form_fields_table extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field("id int(11) unsigned NOT NULL AUTO_INCREMENT");
        $this->dbforge->add_field("field_id int(11) NOT NULL DEFAULT '0'");
        $this->dbforge->add_field("booker_id int(11) NOT NULL DEFAULT '0'");
        $this->dbforge->add_field("field_value varchar(255) NULL DEFAULT NULL");
        $this->dbforge->add_field("form_id int(11) NOT NULL DEFAULT '0'");
        $this->dbforge->add_field("field_order int(11) NULL DEFAULT '0'");
        $this->dbforge->add_field("invitation_master_id int(11) NULL DEFAULT '0'");
 
        $this->dbforge->add_key('id', TRUE);
        
        $this->dbforge->create_table('invitation_attendee_details', TRUE);
	}

	public function down()
	{
		$this->dbforge->drop_table('invitation_attendee_details');
	}
	
	
}	
