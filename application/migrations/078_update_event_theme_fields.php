<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_update_event_theme_fields extends CI_Migration {

  public function up()
  {                                                                                                                                               
    $this->db->query("ALTER TABLE `nm_event_themes_data` ADD `theme_order` tinyint(4) NULL DEFAULT 0 AFTER `is_theme_used` ");    
  }

  public function down()
  {
    
  }
  
} 
