<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_update_attendee_fields extends CI_Migration {

  public function up()
  {                                                                                                                                               
    $this->db->query('ALTER TABLE `nm_invitation_attendee_details` ADD `dietary` varchar(255) NULL DEFAULT NULL AFTER `invitation_master_id`');
  }

  public function down()
  {
    
  }
  
} 
