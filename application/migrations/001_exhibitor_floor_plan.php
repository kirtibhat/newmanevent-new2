<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Exhibitor_floor_plan extends CI_Migration {

	public function up()
	{
		$fields = array(
				'number_of_booth' => array(
				'type' => 'int',
				'constraint' => 11, 
				'null' => FALSE,
				'default' => '0'
			),
		   
		);

		$this->dbforge->add_column('exhibitor_floor_plan', $fields);
	}

	public function down()
	{
		$this->dbforge->drop_table('exhibitor_floor_plan');
	}
	
	
}	
