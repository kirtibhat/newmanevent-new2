<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Sideevent_registrant_field_change extends CI_Migration {

  public function up()
  { 

    $fields = array(
        'event_id' => array(
        'name' => 'side_event_id',
        'type' => 'int(11)',
      ),
    );
    $this->dbforge->modify_column('sideevent_registrant', $fields);
 }

  public function down()
  {
    
  }
  
} 
