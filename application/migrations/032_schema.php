<?php  defined('BASEPATH') OR exit('No direct script access allowed');
//This is used to add is_earlybird column in frnt_registrant & frnt_side_event table
 
 class Migration_Schema extends CI_Migration {


	public function up()
	{
		
		 // insert user matster type data
         $this->db->query("INSERT INTO `nm_user_type_master` (`id`, `user_type`) VALUES (NULL, 'side event')");
         
         //add comment about master user type registration
         $this->db->query("ALTER TABLE  `nm_user_type_master` CHANGE  `user_type`  `user_type` VARCHAR( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL COMMENT  'master type for user registration';");
		
		//remove column from 'user' table
		$this->dbforge->drop_column('user', 'state_postcode');
		$this->dbforge->drop_column('user', 'postal_address_line1');
		$this->dbforge->drop_column('user', 'postal_address_line2');
		$this->dbforge->drop_column('user', 'postal_state_postcode');
		$this->dbforge->drop_column('user', 'postal_country');
		$this->dbforge->drop_column('user', 'latest_product');
		$this->dbforge->drop_column('user', 'receive_special_offer');
		
		//add column in database 'user' table
		
		//insert data in the user table database
		$fields1 = array(
				'postcode' => array(
				'type' => 'varchar',
				'constraint' => 150, 
				'null' => TRUE,
			),
		);
		
		//add field in user table
		$this->dbforge->add_column('user', $fields1);
		
		// change colom order
		$this->db->query("ALTER TABLE `nm_user` CHANGE COLUMN user_title user_title VARCHAR( 150 ) AFTER  `password`;");
	
	}
	
	
	public function down()
	{
			
	}
	
}		
