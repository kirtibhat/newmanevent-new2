<?php  defined('BASEPATH') OR exit('No direct script access allowed');
//This migration is used to add column in nm_event table
 
class Migration_Schema extends CI_Migration {


	public function up()
	{
		
		
		//insert data in the database
		$fields1 = array(
				'master_event_id' => array(
				'type' => 'varchar',
				'constraint' => 150, 
				'null' => TRUE,
			),
		);
		
		//add field in user table
		$this->dbforge->add_column('user', $fields1);
		
		
		//insert data in the database
		$fields2 = array(
				'master_registrant_id' => array(
				'type' => 'varchar',
				'constraint' => 150, 
				'null' => TRUE,
			),
				'master_registration_id' => array(
				'type' => 'varchar',
				'constraint' => 150, 
				'null' => TRUE,
			)
		);
		
	
		$fields3 = array(
				'is_approval_request' => array(
				'type' => 'boolean',
				'null' => FALSE,
				'default' => 0,
			),
				'request_password' => array(
				'type' => 'varchar',
				'constraint' => 150, 
				'null' => TRUE,
			),
		);
	
		
		//add field in user table
		$this->dbforge->add_column('event', $fields2);
		
		//add field in sponsor table
		$this->dbforge->add_column('sponsor', $fields3);
		
		//add field in exhibitor table
		$this->dbforge->add_column('exhibitor', $fields3);
		
		
		//----------payment section migration(3-june-2014)--------------
		
		$fields4 = array(
				'total_price' => array(
				'type' => 'varchar',
				'constraint' => 150, 
				'null' => TRUE,
			),
				'registeration_fee' => array(
				'type' => 'int',
				'constraint' => 11, 
				'null' => TRUE,
			),
		);
	
		//add field in user table
		$this->dbforge->add_column('payment_registration_fee', $fields4);
		
		
		//rename the colomn name
		$renameFields = array(
                        'who_will_pay_fee' => 
                        array(
								'name' => 'fee_to_be_paid_by',
								'type' => 'varchar',
								'constraint' => 150, 
								'null' => TRUE,
                            ),
                        'total_price_per_registrant' => 
                        array(
								'name' => 'price_per_registrant',
								'type' => 'varchar',
								'constraint' => 150, 
								'null' => TRUE,
                            ),    
         	);
		//rename column
		$this->dbforge->modify_column('payment_registration_fee', $renameFields);
		
		
		//remove colomn from table
		$this->dbforge->drop_column('payment_registration_fee', 'this_fee_included_in_registration_cost');
		$this->dbforge->drop_column('payment_registration_fee', 'additional_amount');
		$this->dbforge->drop_column('payment_registration_fee', 'type_of_registrant');
		
		
		//----------payment section migration payment gateway(4-june-2014)--------------
		
		//remove colomn from table
		$this->dbforge->drop_column('payment_gateway', 'paypal_password');
		$this->dbforge->drop_column('payment_gateway', 'paypal_signature');

			//rename the colomn name
		$renameFields = array(
                        'paypal_username' => 
                        array(
								'name' => 'payment_gateway_type',
								'type' => 'varchar',
								'constraint' => 256, 
								'null' => TRUE,
                            ),
                         
				);
         	
		//rename column
		$this->dbforge->modify_column('payment_gateway', $renameFields);

		
		
		
		$fields4 = array(
				'total_price' => array(
				'type' => 'varchar',
				'constraint' => 150, 
				'null' => TRUE,
			),
		);
	
		//add field in user table
		$this->dbforge->add_column('payment_bank_fee', $fields4);
		
		
		//rename the colomn name
		$renameFields = array(
                        'who_will_pay_fee' => 
                        array(
								'name' => 'fee_to_be_paid_by',
								'type' => 'varchar',
								'constraint' => 150, 
								'null' => TRUE,
                            ),
                        'total_price_per_registrant' => 
                        array(
								'name' => 'price_per_registrant',
								'type' => 'varchar',
								'constraint' => 150, 
								'null' => TRUE,
                            ),    
         	);
		//rename column
		$this->dbforge->modify_column('payment_bank_fee', $renameFields);
		
		//remove colomn from table
		$this->dbforge->drop_column('payment_bank_fee', 'this_fee_included_in_registration_cost');
		
		
			
		
		
		$fields8 = array(
				'short_form_title' => array(
				'type' => 'varchar',
				'constraint' => 150, 
				'null' => TRUE,
			),
			'event_reference_number' => array(
				'type' => 'varchar',
				'constraint' => 150, 
				'null' => TRUE,
			),
		);
	
		//add field in event table
		$this->dbforge->add_column('event', $fields8);	

	}
	
	public function down()
	{	
		
	}
	
}	
