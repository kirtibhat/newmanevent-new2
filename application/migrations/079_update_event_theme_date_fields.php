<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_update_event_theme_date_fields extends CI_Migration {

  public function up()
  {                                                                                                                                               
    $this->db->query("ALTER TABLE `nm_event_themes_data` ADD `last_modified` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP AFTER `theme_order` ");    
  }

  public function down()
  {
    
  }
  
} 
