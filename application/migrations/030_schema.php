<?php  defined('BASEPATH') OR exit('No direct script access allowed');
//This is used to add is_earlybird column in frnt_registrant & frnt_side_event table
 
 class Migration_Schema extends CI_Migration {


	public function up()
	{
		
		
		//-------migration added by lokendra for user details (12-Jun-2014)--------------

		// add column in user table
		$userFieldsAdd = array(
			'city' => array(
				'type' => 'varchar',
				'constraint' => 256, 
				'null' => TRUE,
			),
			'state' => array(
				'type' => 'varchar',
				'constraint' => 256, 
				'null' => TRUE,
			)
		);  
		$this->dbforge->add_column('user', $userFieldsAdd);
		
		
		
		// add column in sponsor table
		$fields = array(
			'subject_to_approval' => array(
				'type' => 'boolean',	 
				'null' => FALSE,
				'default' => '0'
			),
			'password_required' => array(
				'type' => 'boolean',	 
				'null' => FALSE,
				'default' => '0'
			),
			'password' => array(
				'type' => 'varchar',
				'constraint' => 256, 
				'null' => TRUE,
				'default' => NULL
			),
			'price' => array(
				'type' => 'varchar',
				'constraint' => 256, 
				'null' => TRUE,
				'default' => NULL
			),
			'registration_included' => array(
				'type' => 'varchar',
				'constraint' => 256, 
				'null' => TRUE,
				'default' => NULL
			),
			'attach_registration' => array(
				'type' => 'varchar',
				'constraint' => 256, 
				'null' => TRUE,
				'default' => NULL
			),
		);  
		$this->dbforge->add_column('sponsor', $fields);
		
		//delete column from sponsor
		$this->dbforge->drop_column('sponsor', 'additional_booth_price');
		$this->dbforge->drop_column('sponsor', 'additional_booth_gst');
		$this->dbforge->drop_column('sponsor', 'additi_guest_total_price');
		$this->dbforge->drop_column('sponsor', 'additi_guest_gst');
		$this->dbforge->drop_column('sponsor', 'is_additional_guests');
		$this->dbforge->drop_column('sponsor', 'number_complimentary_registrations');
		$this->dbforge->drop_column('sponsor', 'complementary_registration');
		$this->dbforge->drop_column('sponsor', 'can_attend_breakout');
		$this->dbforge->drop_column('sponsor', 'limit_per_person');
			
		
		// rename column name
		$renameFields = array(
			'additional_detail' => 
			array(
					'name' => 'description',
					'type' => 'varchar',
					'constraint' => 256, 
					'null' => TRUE,
					'default' => NULL
				),
                        
         	);
		//rename column
		$this->dbforge->modify_column('sponsor', $renameFields);
		
	
	
		$this->dbforge->add_field('id int(11) unsigned NOT NULL AUTO_INCREMENT');
        $this->dbforge->add_field('event_id int(11) NOT NULL DEFAULT "0"');
        $this->dbforge->add_field('sponsor_id int(11) NOT NULL DEFAULT "0"');
        $this->dbforge->add_field('exhibitor_id int(11) NOT NULL DEFAULT "0"');
        $this->dbforge->add_field('user_id int(11) NOT NULL DEFAULT "0"');
        $this->dbforge->add_field('name varchar(254) NULL DEFAULT NULL');
        $this->dbforge->add_field('email varchar(254) NULL DEFAULT NULL');
        $this->dbforge->add_field('message varchar(254) NULL DEFAULT NULL');
        $this->dbforge->add_field('date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP');
        
 
        $this->dbforge->add_key('id', TRUE);
        
        $this->dbforge->create_table('sponsor_exhibitor_booth_request', TRUE);
		// add column in user table
		$requestuser = array(
			'request_type' => array(
				'type' => 'varchar',
				'constraint' => 256, 
				'null' => TRUE,
			),
			'request_status' => array(
				'type' => 'boolean',				
				'default' => '0',
			)
		);  
		$this->dbforge->add_column('sponsor_exhibitor_booth_request', $requestuser);	
		
		
		//--------migration code added by lokendra(14-06-2014)--------------
		
		//billing information
		$this->dbforge->add_field('id int(11) unsigned NOT NULL AUTO_INCREMENT');
		$this->dbforge->add_field('user_id int(11) NOT NULL DEFAULT "0"');
        $this->dbforge->add_field('apply_user_address varchar(254) NULL DEFAULT NULL');
        $this->dbforge->add_field('firstname varchar(254) NULL DEFAULT NULL');
        $this->dbforge->add_field('surname varchar(254) NULL DEFAULT NULL');
        $this->dbforge->add_field('email varchar(254) NULL DEFAULT NULL');
        $this->dbforge->add_field('organisation varchar(254) NULL DEFAULT NULL');
        $this->dbforge->add_field('phone varchar(254) NULL DEFAULT NULL');
        $this->dbforge->add_field('fax varchar(254) NULL DEFAULT NULL');
        $this->dbforge->add_field('mobile varchar(254) NULL DEFAULT NULL');
        $this->dbforge->add_field('address1 varchar(254) NULL DEFAULT NULL');
        $this->dbforge->add_field('address2 varchar(254) NULL DEFAULT NULL');
        $this->dbforge->add_field('city varchar(254) NULL DEFAULT NULL');
        $this->dbforge->add_field('state varchar(254) NULL DEFAULT NULL');
        $this->dbforge->add_field('postcode varchar(254) NULL DEFAULT NULL');
        $this->dbforge->add_field('country varchar(254) NULL DEFAULT NULL');
        
        $this->dbforge->add_key('id', TRUE);
        
        $this->dbforge->create_table('user_billing_information', TRUE);
        
        //credit card information
        $this->dbforge->add_field('id int(11) unsigned NOT NULL AUTO_INCREMENT');
        $this->dbforge->add_field('user_id int(11) NOT NULL DEFAULT "0"');
        $this->dbforge->add_field('name_on_card varchar(254) NULL DEFAULT NULL');
        $this->dbforge->add_field('credit_card_number varchar(254) NULL DEFAULT NULL');
        $this->dbforge->add_field('security_code varchar(254) NULL DEFAULT NULL');
        $this->dbforge->add_field('expiry_month varchar(254) NULL DEFAULT NULL');
        $this->dbforge->add_field('expiry_year varchar(254) NULL DEFAULT NULL');
       
        $this->dbforge->add_key('id', TRUE);
        
        $this->dbforge->create_table('user_credit_Card_information', TRUE);
        
       $fields = array( 
			'payment_id' => array(
				'type' => 'int',
				'constraint' => '11',  
				'default' => '0'
			),
			
		
			'payment_status' => array(
				'type' => 'boolean',	 
				'null' => FALSE,
				'default' => '0'
			)		
		); 
       
       	$this->dbforge->add_column('frnt_sponsor_booth', $fields); 
       
       
      	
       $fields = array( 
			
			'is_approval_request' => array(
				'type' => 'boolean',	 
				'null' => FALSE,
		//		'default' => '0'
			)		
		); 
       
       	$this->dbforge->add_column('sponsor', $fields);
       	
       	
       	
       	
       	//payment & funds transfer information lokendra (18-4-2014)
		$this->dbforge->add_field('id int(11) unsigned NOT NULL AUTO_INCREMENT');
		$this->dbforge->add_field('event_id int(11) NOT NULL DEFAULT "0"');
		$this->dbforge->add_field('user_id int(11) NOT NULL DEFAULT "0"');
		$this->dbforge->add_field('paid_to varchar(254) NULL DEFAULT NULL');
        $this->dbforge->add_field('contact_name varchar(254) NULL DEFAULT NULL');
        $this->dbforge->add_field('description varchar(254) NULL DEFAULT NULL');
        $this->dbforge->add_field('payment_date DATETIME NULL DEFAULT NULL');
        $this->dbforge->add_field('amount varchar(254) NULL DEFAULT NULL');
        $this->dbforge->add_field('gst_include varchar(254) NULL DEFAULT NULL');
        $this->dbforge->add_field('total_amount varchar(254) NULL DEFAULT NULL');
        $this->dbforge->add_field('authorised_by varchar(254) NULL DEFAULT NULL');
       
        $this->dbforge->add_key('id', TRUE);
        
        $this->dbforge->create_table('payment_funds_transfer', TRUE);
		
		//change table name for benefits of sponsor
		$this->dbforge->rename_table('sponsor_additional_items', 'sponsor_benefits');
	
			
	}
	
	

		
	
	
	public function down()
	{
			
	}
	
}		
