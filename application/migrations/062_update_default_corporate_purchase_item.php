<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_update_default_corporate_purchase_item extends CI_Migration {

  public function up(){
    $this->db->query("Delete from `nm_corporate_purchase_item` where event_id='0' and corporate_package_id='0' and name='Corporate profile'");
    $this->db->query("Delete from `nm_corporate_purchase_item` where event_id='0' and corporate_package_id='0' and name='Full page advertisement'");
    $this->db->query("Delete from `nm_corporate_purchase_item` where event_id='0' and corporate_package_id='0' and name='Half page advertisement'");
    $this->db->query("Delete from `nm_corporate_purchase_item` where event_id='0' and corporate_package_id='0' and name='Full page advertisement upgrade'");
    $this->db->query("Delete from `nm_corporate_purchase_item` where event_id='0' and corporate_package_id='0' and name='Additional double booth'");    
  }

  public function down()
  {     
    
  }
  
} 
