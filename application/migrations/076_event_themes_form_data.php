<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_event_themes_form_data extends CI_Migration {
  
  public function up(){
      
    $prefix = $this->db->dbprefix;
    
    $this->dbforge->add_field('id int(11) unsigned NOT NULL AUTO_INCREMENT');
    $this->dbforge->add_field('event_id int(11) NOT NULL DEFAULT "0"');
    $this->dbforge->add_field('theme_id int(11) NOT NULL DEFAULT "0"');
    $this->dbforge->add_field('event_theme_name varchar(255) NULL DEFAULT NULL');
    $this->dbforge->add_field('event_theme_screen_bg varchar(255) NULL DEFAULT NULL');
    $this->dbforge->add_field('event_theme_header_bg varchar(255) NULL DEFAULT NULL');
    $this->dbforge->add_field('event_theme_box_bg varchar(255) NULL DEFAULT NULL');
    $this->dbforge->add_field('event_theme_text_color varchar(255) NULL DEFAULT NULL');
    $this->dbforge->add_field('event_theme_link_color varchar(255) NULL DEFAULT NULL');
    $this->dbforge->add_field('event_theme_link_highlighted_color varchar(255) NULL DEFAULT NULL');
    $this->dbforge->add_field('font_package tinyint(4) NULL DEFAULT 0');
    $this->dbforge->add_field('is_theme_used tinyint(4) NULL DEFAULT 0');
    

    //add key in the field 
    $this->dbforge->add_key('id', TRUE);

    //add table with fields
    $this->dbforge->create_table('event_themes_data', TRUE);
    
  }

  public function down()
  {
    
  }
  
} 
