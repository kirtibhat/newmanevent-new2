<?php  defined('BASEPATH') OR exit('No direct script access allowed');
//This is used to add is_earlybird column in frnt_registrant & frnt_side_event table
 
 class Migration_Schema extends CI_Migration {


	public function up()
	{
		
		
		// create table for add benifits
		$this->dbforge->add_field('id int(11) unsigned NOT NULL AUTO_INCREMENT');      
        $this->dbforge->add_field('sponsor_id int(11) NOT NULL DEFAULT "0"');
        $this->dbforge->add_field('benifit_id int(11) NOT NULL DEFAULT "0"');      
        $this->dbforge->add_field('user_id int(11) NOT NULL DEFAULT "0"');
        $this->dbforge->add_key('id', TRUE);        
        $this->dbforge->create_table('frnt_add_benifits', TRUE); 
        
	
		// To create new table for additional purchase for backdend	
		$this->dbforge->add_field('id int(11) unsigned NOT NULL AUTO_INCREMENT');   
        $this->dbforge->add_field('sponsor_id int(11) NOT NULL DEFAULT "0"');
        $this->dbforge->add_field('name varchar(100) NULL DEFAULT NULL');
        $this->dbforge->add_field('detail varchar(256) NULL DEFAULT NULL');
        $this->dbforge->add_field('price varchar(256) NULL DEFAULT NULL');
        $this->dbforge->add_field('gst varchar(256) NULL DEFAULT NULL');
        $this->dbforge->add_field('total_price varchar(256) NULL DEFAULT NULL');
        $this->dbforge->add_key('id', TRUE);        
        $this->dbforge->create_table('sponsor_additional_purchase', TRUE);
        
        
		// To create new table for additional purchase for frontend	
		$this->dbforge->add_field('id int(11) unsigned NOT NULL AUTO_INCREMENT');   
        $this->dbforge->add_field('sponsor_id int(11) NOT NULL DEFAULT "0"');
        $this->dbforge->add_field('user_id int(11) NOT NULL DEFAULT "0"');
        $this->dbforge->add_field('add_purch_id int(11) NOT NULL DEFAULT "0"');        
        $this->dbforge->add_field('total_price varchar(256) NULL DEFAULT NULL');
        $this->dbforge->add_key('id', TRUE);        
        $this->dbforge->create_table('frnt_sponsor_additional_purchase', TRUE);
	
		// we have to remove sponsor_registrant table after some time 
		//	$this->dbforge->create_table('sponsor_registrant', TRUE);
	
	}
	
	
	public function down()
	{
			
	}
	
}		
