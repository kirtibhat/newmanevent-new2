<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Sponsor_additional_items extends CI_Migration {

	public function up()
	{
		//add fields
		$this->dbforge->add_field("id int(11) unsigned NOT NULL AUTO_INCREMENT");
        $this->dbforge->add_field("sponsor_id int(11) NULL DEFAULT NULL");
        $this->dbforge->add_field("item_title varchar(255) NULL DEFAULT NULL");
        $this->dbforge->add_field("item_status enum('0','1') NOT NULL DEFAULT '1'");
      
		//add key in the field 
        $this->dbforge->add_key('id', TRUE);
        
        //add table with fields
        $this->dbforge->create_table('sponsor_additional_items', TRUE);
	}

	public function down()
	{
		$this->dbforge->drop_table('sponsor_additional_items');
	}
	
}	
