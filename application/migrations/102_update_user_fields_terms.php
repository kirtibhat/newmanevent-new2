<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_update_user_fields_terms extends CI_Migration {

  public function up()
  {                                                                                                                                               
    $this->db->query("ALTER TABLE `nm_user` ADD `isTermsNConditionTrue` tinyint NULL DEFAULT NULL AFTER `gplus_id` ");   
  }

  public function down()
  {
    
  }
  
} 
