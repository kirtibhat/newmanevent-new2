<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_update_sideevent_registrant_fields extends CI_Migration {

  public function up()
  {

    $this->db->query('ALTER TABLE `nm_sideevent_registrant` ADD `total_price_inc_gst` varchar(45) NULL DEFAULT NULL AFTER `gst_included_total_price_comp`');
    $this->db->query("ALTER TABLE `nm_sideevent_registrant` ADD `complimentary_allow_earlybird` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT '1 for Yes, 0 for No' AFTER `total_price_inc_gst`");
    $this->db->query('ALTER TABLE `nm_sideevent_registrant` ADD `complimentary_earlybird_limit` varchar(45) NULL DEFAULT NULL AFTER `total_price_inc_gst`');
    $this->db->query("ALTER TABLE `nm_sideevent_registrant` ADD `complimentary_allow_group_booking` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT '1 for Yes, 0 for No' AFTER `complimentary_earlybird_limit`");

    $this->db->query("ALTER TABLE `nm_sideevent_registrant` ADD `allow_earlybird_ad_guest` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT '1 for Yes, 0 for No' AFTER `gst_included_total_price_ad_guest`");
    $this->db->query('ALTER TABLE `nm_sideevent_registrant` ADD `earlybird_limit_ad_guest` varchar(45) NULL DEFAULT NULL AFTER `allow_earlybird_ad_guest`');


    $this->db->query('ALTER TABLE `nm_sideevent_registrant` ADD `total_early_bird_price_gst_inc_comp` varchar(45) NULL DEFAULT NULL AFTER `gst_included_early_bird_comp`');
    $this->db->query('ALTER TABLE `nm_sideevent_registrant` ADD `total_price_gst_inc_ad_guest` varchar(45) NULL DEFAULT NULL AFTER `gst_included_total_price_ad_guest`');
    $this->db->query('ALTER TABLE `nm_sideevent_registrant` ADD `total_early_bird_price_gst_inc_ad_guest` varchar(45) NULL DEFAULT NULL AFTER `gst_included_total_price_early_bird_ad_guest`');
  }

  public function down()
  {
    
  }
  
} 
