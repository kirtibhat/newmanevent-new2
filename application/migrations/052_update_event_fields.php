<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update_event_fields extends CI_Migration {

  public function up()
  {
      $this->db->query('ALTER TABLE `nm_event` ADD `subtitle` varchar(100) NULL DEFAULT NULL');
      $this->db->query('ALTER TABLE `nm_event` ADD `reg_startdate` datetime NULL DEFAULT NULL');
      $this->db->query('ALTER TABLE `nm_event` ADD `reg_closedate` datetime NULL DEFAULT NULL');
  }

  public function down()
  {
    
  }
  
} 

