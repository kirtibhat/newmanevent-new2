<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_update_font_fields extends CI_Migration {

  public function up()
  {                                                                                                                                               
    $this->db->query("ALTER TABLE `nm_font_packages` ADD `font_value` text NULL DEFAULT NULL AFTER `package_name` ");    
  }

  public function down()
  {
    
  }
  
} 
