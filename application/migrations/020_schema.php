<?php  defined('BASEPATH') OR exit('No direct script access allowed');
//This is used to add is_earlybird column in frnt_registrant & frnt_side_event table
 
class Migration_Schema extends CI_Migration {


	public function up()
	{
		
		$fields = array(
				'is_earlybird' => array(
				'type' => 'boolean',
				'null' => FALSE,
				'default' => 0
			),
			'earlybird_daywise_id' => array(
				'type' => 'varchar',
				'constraint' => 256, 
				'null' => TRUE,
				'default' => NULL
			),
		);  
		
			$fieldData = array(
				'is_earlybird' => array(
				'type' => 'boolean',
				'null' => FALSE,
				'default' => 0
			),
			'sideevent_registrant_id' => array(
				'type' => 'int',
				'constraint' => 11, 
				'null' => FALSE,
				'default' => '0'
			),
		);   
	
 
	     //add field in nm_frnt_registrant table
		  $this->dbforge->add_column('frnt_registrant', $fields);
		
			
	     //add fieldData in frnt_side_event table
		 $this->dbforge->add_column('frnt_side_event', $fieldData);
		 
		 
		 
	}
	
	public function down()
	{	
		
	}
	
}	
