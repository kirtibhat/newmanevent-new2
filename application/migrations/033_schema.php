<?php  defined('BASEPATH') OR exit('No direct script access allowed');
//This is used to add is_earlybird column in frnt_registrant & frnt_side_event table
 
 class Migration_Schema extends CI_Migration {


	public function up()
	{
	 	
	  
		//event custom contact (07-01-2014)
		$this->dbforge->add_field('custom_contact_id int(11) unsigned NOT NULL AUTO_INCREMENT');
		$this->dbforge->add_field('contact_person_id int(11) NOT NULL DEFAULT "0"');
		$this->dbforge->add_field('contact_type varchar(254) NULL DEFAULT NULL');
		$this->dbforge->add_field('details text NULL DEFAULT NULL');
       
        $this->dbforge->add_key('custom_contact_id', TRUE);
        
        $this->dbforge->create_table('event_custom_contact', TRUE);
       
                
		//event category  (07-01-2014)
		
		$this->dbforge->add_field('category_id int(11) unsigned NOT NULL AUTO_INCREMENT');
		$this->dbforge->add_field('event_id int(11) NOT NULL DEFAULT "0"');
		$this->dbforge->add_field('category varchar(254) NULL DEFAULT NULL');
		$this->dbforge->add_field("is_corporate enum('0','1') NOT NULL DEFAULT '0' COMMENT '1 means this category belongs to corporate section'");
       
        $this->dbforge->add_key('category_id', TRUE);
        
        $this->dbforge->create_table('event_categories', TRUE);
        
        
        // event event emails(07-01-2014)
       
        $this->dbforge->add_field('email_id int(11) unsigned NOT NULL AUTO_INCREMENT');
		$this->dbforge->add_field('event_id int(11) NOT NULL DEFAULT "0"');
		$this->dbforge->add_field('email_title varchar(254) NULL DEFAULT NULL');
        $this->dbforge->add_field('subject text NULL DEFAULT NULL');
        $this->dbforge->add_field('from_email varchar(254) NULL DEFAULT NULL');
        $this->dbforge->add_field('from_name varchar(254) NULL DEFAULT NULL');
        $this->dbforge->add_field('contents longtext NULL DEFAULT NULL');
        $this->dbforge->add_field('category_id int(11) NOT NULL DEFAULT "0"');
        
		$this->dbforge->add_key('email_id', TRUE);
        
        $this->dbforge->create_table('event_emails', TRUE);
		
		
		 // event event attachments(07-01-2014)
        
        $this->dbforge->add_field('attachment_id int(11) unsigned NOT NULL AUTO_INCREMENT');
		$this->dbforge->add_field('email_id int(11) NOT NULL DEFAULT "0"');
		$this->dbforge->add_field('document_file varchar(254) NULL DEFAULT NULL');
        $this->dbforge->add_field('document_path varchar(254) NULL DEFAULT NULL');
        $this->dbforge->add_field('document_size varchar(100) NULL DEFAULT NULL');
        $this->dbforge->add_field('document_ext varchar(10) NULL DEFAULT NULL');
              
		$this->dbforge->add_key('attachment_id', TRUE);
        
        $this->dbforge->create_table('event_email_attachments', TRUE);
		
		
		
		 // event corporate section
		 // corporate floor plan(07-01-2014)
		
		$this->dbforge->add_field('id int(11) unsigned NOT NULL AUTO_INCREMENT');
		$this->dbforge->add_field('event_id int(11) NOT NULL DEFAULT "0"');
		$this->dbforge->add_field('floor_plan varchar(254) NULL DEFAULT NULL');
        $this->dbforge->add_field('floor_plan_path varchar(254) NULL DEFAULT NULL');
        $this->dbforge->add_field('total_space_available int(11) NOT NULL DEFAULT "0"');
        $this->dbforge->add_field("is_exhibition_space enum('0','1') NOT NULL DEFAULT '1' COMMENT '1 means available space'");
              
		$this->dbforge->add_key('id', TRUE);
        
        $this->dbforge->create_table('corporate_floor_plan', TRUE);
		 
		// corporate floor plan (07-01-2014)
		 
		$this->dbforge->add_field('floor_plan_type_id int(11) unsigned NOT NULL AUTO_INCREMENT');
		$this->dbforge->add_field('event_id int(11) NOT NULL DEFAULT "0"');
		$this->dbforge->add_field('space_type varchar(254) NULL DEFAULT NULL');
        $this->dbforge->add_field('total_space_available int(11) NOT NULL DEFAULT "0"');
        $this->dbforge->add_field('available_booth text NULL DEFAULT NULL');
        
              
		$this->dbforge->add_key('floor_plan_type_id', TRUE);
        
        $this->dbforge->create_table('corporate_floor_plan_type', TRUE);
		 
		
		
		// corporate Package benefit (07-01-2014)
		
		
		$this->dbforge->add_field('package_benefit_id int(11) unsigned NOT NULL AUTO_INCREMENT');
		$this->dbforge->add_field('event_id int(11) NOT NULL DEFAULT "0"');
		$this->dbforge->add_field('corporate_package_id int(11) NOT NULL DEFAULT "0"');
        $this->dbforge->add_field('benefit_name varchar(254) NULL DEFAULT NULL');
        
		$this->dbforge->add_key('package_benefit_id', TRUE);
        
        $this->dbforge->create_table('corporate_package_benefit', TRUE);
		
		
		
		// corporate Package booths (07-01-2014)
		
		
		$this->dbforge->add_field('id int(11) unsigned NOT NULL AUTO_INCREMENT');
		$this->dbforge->add_field('floor_plan_type_id int(11) NOT NULL DEFAULT "0"');
		$this->dbforge->add_field('corporate_package_id int(11) NOT NULL DEFAULT "0"');
		$this->dbforge->add_field('event_id int(11) NOT NULL DEFAULT "0"');
		$this->dbforge->add_field('booth_position varchar(45) NULL DEFAULT NULL');
        
		$this->dbforge->add_key('id', TRUE);
        
        $this->dbforge->create_table('corporate_package_booths', TRUE);
		
		
		// corporate Package items (07-01-2014)
			
		$this->dbforge->add_field('purchase_item_id int(11) unsigned NOT NULL AUTO_INCREMENT');
		$this->dbforge->add_field('event_id int(11) NOT NULL DEFAULT "0"');
		$this->dbforge->add_field('corporate_package_id int(11) NOT NULL DEFAULT "0"');
		$this->dbforge->add_field('name varchar(255) NULL DEFAULT NULL');
		$this->dbforge->add_field('item_limit int(11) NOT NULL DEFAULT "0"');
        $this->dbforge->add_field('total_price varchar(100) NULL DEFAULT NULL');
        $this->dbforge->add_field('gst_include_price varchar(100) NULL DEFAULT NULL');
        
		$this->dbforge->add_key('purchase_item_id', TRUE);
        
        $this->dbforge->create_table('corporate_purchase_item', TRUE);
		
		

		// corporate Package (07-01-2014)
			
		$this->dbforge->add_field('id int(11) unsigned NOT NULL AUTO_INCREMENT');
		$this->dbforge->add_field('category_id int(11) NOT NULL DEFAULT "0"');
		$this->dbforge->add_field('description varchar(254) NULL DEFAULT NULL');
        $this->dbforge->add_field('limit_package_available int(11) NOT NULL DEFAULT "0"');
        $this->dbforge->add_field('total_price varchar(254) NULL DEFAULT NULL');
        $this->dbforge->add_field('gst varchar(254) NULL DEFAULT NULL');
        $this->dbforge->add_field('event_id int(11) NOT NULL DEFAULT "0"');
        $this->dbforge->add_field('corporate_package_name varchar(254) NULL DEFAULT NULL');
        $this->dbforge->add_field('is_approval_request tinyint(1) NULL DEFAULT "0"');
        $this->dbforge->add_field('request_password varchar(150) NULL DEFAULT NULL');
        $this->dbforge->add_field('subject_to_approval tinyint(1) NULL DEFAULT "0"');
        $this->dbforge->add_field('password_required tinyint(1) NULL DEFAULT "0"');
        $this->dbforge->add_field('password varchar(254) NULL DEFAULT NULL');
        $this->dbforge->add_field('price varchar(254) NULL DEFAULT NULL');
        $this->dbforge->add_field('registration_included varchar(30) NULL DEFAULT NULL');
        $this->dbforge->add_field('package_benefit_ids text NULL DEFAULT NULL');
        $this->dbforge->add_field('purchase_Item_ids text NULL DEFAULT NULL');
        $this->dbforge->add_field('attach_registration varchar(254) NULL DEFAULT NULL');
        
              
		$this->dbforge->add_key('id', TRUE);
        
        $this->dbforge->create_table('corporate_package', TRUE);
	
		
		//insert new column in the database // event_contact_person
		$fields1 = array(
				'phone2' => array(
				'type' => 'varchar',
				'constraint' => 30, 
				'null' => TRUE,
			),
		);
		
		//add field in user table
		$this->dbforge->add_column('event_contact_person', $fields1);
	
	   
		 //insert new column (group_booking_allowed) in the database // nm_event
		
		 //add comment about master user type registration
         $this->db->query("ALTER TABLE `nm_event` ADD `group_booking_allowed` ENUM( '1', '0' ) NOT NULL DEFAULT '0' COMMENT '1 means allowed booking' AFTER `event_reference_number` ");
		
				
		 $fields2 = array(
					'min_group_booking_size' => array(
					'type' => 'int',
					'null' => TRUE,
				),
					'percentage_discount_for_group_booking' => array(
					'type' => 'varchar',
					'constraint' => 50, 
					'null' => TRUE,
				),
			);	
		
		 //add field in user table
		 $this->dbforge->add_column('event',  $fields2);
		
		
	}
	
	
	public function down()
	{
			
	}
	
}		
