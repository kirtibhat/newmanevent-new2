<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_update_event_theme_fields_new extends CI_Migration {

  public function up()
  {                                                                                                                                               
    $this->db->query("ALTER TABLE `nm_event_themes_data` ADD `logo_image_status` varchar(255) NOT NULL DEFAULT 0 AFTER `is_header_or_bgcolor` ");    
  }

  public function down()
  {
    
  }
  
} 
