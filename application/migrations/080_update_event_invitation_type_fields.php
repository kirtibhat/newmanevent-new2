<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_update_event_invitation_type_fields extends CI_Migration {

  public function up()
  {                                                                                                                                               
    $this->db->query('ALTER TABLE `nm_event_invitations` ADD `limit_per_invitation` int(11) NULL DEFAULT NULL AFTER `invitations_order`');
    $this->db->query('ALTER TABLE `nm_event_invitations` ADD `show_remaining_invitations` int(11) NULL DEFAULT NULL AFTER `limit_per_invitation`');
    $this->db->query('ALTER TABLE `nm_event_invitations` ADD `allow_waiting_list` int(11) NULL DEFAULT NULL AFTER `show_remaining_invitations`');
  }

  public function down()
  {
    
  }
  
} 
