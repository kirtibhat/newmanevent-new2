<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_update_event_registrants_fields extends CI_Migration {

  public function up()
  {                                                                                                                                               
    $this->db->query("ALTER TABLE `nm_event_registrants` ADD `is_corporate_package` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT '1 for Yes, 0 for No' AFTER `category_id`");    
  }

  public function down()
  {
    
  }
  
} 
