<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_update_default_theme_fields_header extends CI_Migration {

  public function up()
  {                                                                                                                                               
    $this->db->query("ALTER TABLE `nm_event_default_themes` ADD `header_image_size` TEXT NULL DEFAULT NULL AFTER `logo_image_status` ");    
  }

  public function down()
  {
    
  }
  
} 
