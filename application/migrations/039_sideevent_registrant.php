<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Sideevent_registrant extends CI_Migration {

  public function up()
  {
    //add fields                                               
    $this->dbforge->add_field('id int(11) unsigned NOT NULL AUTO_INCREMENT');
    $this->dbforge->add_field('event_id int(11) NULL DEFAULT NULL');
    $this->dbforge->add_field('registrant_id int(11) NULL DEFAULT NULL');
    $this->dbforge->add_field('reg_limit int(11) NULL DEFAULT NULL');

    $this->dbforge->add_field("complementary enum('0','1') NOT NULL DEFAULT '0' COMMENT '1 means is complementary'");
    $this->dbforge->add_field('total_price_comp  varchar(255) NULL DEFAULT NULL');
    $this->dbforge->add_field('gst_included_total_price_comp  varchar(75) NULL DEFAULT NULL');
    $this->dbforge->add_field('total_early_bird_price_comp   varchar(75) NULL DEFAULT NULL');
    $this->dbforge->add_field('gst_included_early_bird_comp  varchar(75) NULL DEFAULT NULL');

    $this->dbforge->add_field("additional_guests enum('0','1') NOT NULL DEFAULT '0' COMMENT '1 means is additional guests'");
    $this->dbforge->add_field('guests_limit  int(11) NULL DEFAULT "1"');
    $this->dbforge->add_field('total_price_ad_guest varchar(75) NULL DEFAULT NULL');
    $this->dbforge->add_field('gst_included_total_price_ad_guest varchar(75) NULL DEFAULT NULL');
    $this->dbforge->add_field('total_early_bird_price_ad_guest varchar(75) NULL DEFAULT NULL');
    $this->dbforge->add_field('gst_included_total_price_early_bird_ad_guest varchar(75) NULL DEFAULT NULL'); 

    $this->dbforge->add_field("complementary_guests enum('0','1') NOT NULL DEFAULT '0' COMMENT '1 means is complementary guests'");          
    $this->dbforge->add_field('complementary_guests_limit int(11) NULL DEFAULT "0"');  

    //add key in the field 
    $this->dbforge->add_key('id', TRUE);

    //add table with fields
    $this->dbforge->create_table('sideevent_registrant', TRUE);
  }

  public function down()
  {
    
  }

} 
