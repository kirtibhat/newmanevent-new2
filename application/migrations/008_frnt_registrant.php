<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Frnt_registrant extends CI_Migration {


		public function up()
		{
			
			$fields = array(
					'registrant_day_wise_id' => array(
							'type' => 'varchar',
							'constraint' => 255, 
							'null' => TRUE,
							'default' => NULL
					),
				);
		
		//modify column name			
		 
		 $this->dbforge->modify_column('frnt_registrant', $fields);
		
		}
		
		public function down()
		{
			$this->dbforge->drop_table('frnt_registrant');
		}
	
	
}	
