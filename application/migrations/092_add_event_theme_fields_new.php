<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_event_theme_fields_new extends CI_Migration {

  public function up()
  {                                                                                                                                               
    $this->db->query("ALTER TABLE `nm_event_themes_data` ADD `is_header_or_bgcolor` varchar(255) NULL DEFAULT NULL AFTER `secondary_color` ");    
  }

  public function down()
  {
    
  }
  
} 
