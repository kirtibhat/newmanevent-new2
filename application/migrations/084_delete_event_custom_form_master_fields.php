<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_delete_event_custom_form_master_fields extends CI_Migration {

  public function up()
  {                                                                                                                                               
    $this->db->query('DELETE FROM `nm_custom_form_field_master` where form_id=4 and field_name="organisation"');
    $this->db->query('DELETE FROM `nm_custom_form_field_master` where form_id=4 and field_name="position"');
    $this->db->query('DELETE FROM `nm_custom_form_field_master` where form_id=4 and field_name="Phone"');
  }

  public function down()
  {
    
  }
  
} 
