<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Payment_cheque_fee extends CI_Migration {

  public function up()
  {
    //add fields                                               
    $this->dbforge->add_field('id int(11) unsigned NOT NULL AUTO_INCREMENT');
    $this->dbforge->add_field('event_id int(11) NULL DEFAULT NULL');
    $this->dbforge->add_field('fee_to_be_paid_by VARCHAR(2) NOT NULL DEFAULT 0 COMMENT "0 for registrant, 1 for client, 2 for organisor"');
    $this->dbforge->add_field('cheque_processing_fee int(11) NULL DEFAULT NULL');
    $this->dbforge->add_field('total_price  varchar(50) NULL DEFAULT NULL');
    $this->dbforge->add_field('gst varchar(50) NULL DEFAULT NULL');
    
    //add key in the field 
    $this->dbforge->add_key('id', TRUE);

    //add table with fields
    $this->dbforge->create_table('payment_cheque_fee', TRUE);
  }

  public function down()
  {
    
  }

} 
