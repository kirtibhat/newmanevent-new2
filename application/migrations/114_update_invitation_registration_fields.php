<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_update_invitation_registration_fields extends CI_Migration {

  public function up()
  {                                                                                                                                               
    $this->db->query("ALTER TABLE `nm_invitation_registration` ADD `registration_type` varchar(255) NULL DEFAULT NULL AFTER `password` ");   
    $this->db->query("ALTER TABLE `nm_invitation_registration` ADD `field_order` tinyint(4) NULL DEFAULT NULL AFTER `registration_type` ");   
  }

  public function down()
  {
    
  }
  
} 
