<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_corporate_package_default extends CI_Migration {

  public function up()
  {
    $prefix = $this->db->dbprefix;
    
    $this->dbforge->add_field('id int(11) unsigned NOT NULL AUTO_INCREMENT');
    $this->dbforge->add_field('category_id int(11) NOT NULL DEFAULT "0"');
    $this->dbforge->add_field('event_id int(11) NOT NULL DEFAULT "0"');
    $this->dbforge->add_field('description varchar(254) NULL DEFAULT NULL');
    $this->dbforge->add_field('limit_package_available int(11) NOT NULL DEFAULT "0"');
    $this->dbforge->add_field('total_price varchar(254) NULL DEFAULT NULL');
    $this->dbforge->add_field('gst varchar(254) NULL DEFAULT NULL');    
    $this->dbforge->add_field('corporate_package_name varchar(254) NULL DEFAULT "DEFAULT"');
    $this->dbforge->add_field('is_approval_request tinyint(1) NULL DEFAULT "0"');
    $this->dbforge->add_field('request_password varchar(150) NULL DEFAULT NULL');
    $this->dbforge->add_field('subject_to_approval tinyint(1) NULL DEFAULT "0"');
    $this->dbforge->add_field('password_required tinyint(1) NULL DEFAULT "0"');
    $this->dbforge->add_field('password varchar(254) NULL DEFAULT NULL');
    $this->dbforge->add_field('price varchar(254) NULL DEFAULT NULL');
    $this->dbforge->add_field('registration_included varchar(30) NULL DEFAULT NULL');
    $this->dbforge->add_field('package_benefit_ids text NULL DEFAULT NULL');
    $this->dbforge->add_field('purchase_Item_ids text NULL DEFAULT NULL');
    $this->dbforge->add_field('attach_registration varchar(254) NULL DEFAULT NULL');

    //add key in the field 
    $this->dbforge->add_key('id', TRUE);

    //add table with fields
    $this->dbforge->create_table('corporate_package_default', TRUE);


    //update corporate_package table to set package is completed or not
    $this->db->query("ALTER TABLE `nm_corporate_package` ADD `is_completed` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT '0 for completed 1 means incomplete' AFTER `attach_registration`");
    $this->db->query('ALTER TABLE `nm_corporate_package` ADD `package_order` int(11) NULL DEFAULT NULL AFTER `is_completed`');

  }

  public function down()
  {
    
  }
  
} 

