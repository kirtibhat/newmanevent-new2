<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_update_user_fields_logo extends CI_Migration {

  public function up()
  {                                                                                                                                               
    $this->db->query("ALTER TABLE `nm_user` ADD `userImageStatus` enum('0','1') DEFAULT '0' AFTER `isTermsNConditionTrue` ");   
  }

  public function down()
  {
    
  }
  
} 
