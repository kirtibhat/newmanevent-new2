<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_invitation_booker_form_fields_table extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field("id int(11) unsigned NOT NULL AUTO_INCREMENT");
        $this->dbforge->add_field("login_id int(11) NOT NULL DEFAULT '0'");
        $this->dbforge->add_field("invitation_type_id int(11) NOT NULL DEFAULT '0'");
        $this->dbforge->add_field("quantity int(11) NULL DEFAULT NULL");
        $this->dbforge->add_field("unique_id varchar(255) NULL DEFAULT NULL");
        $this->dbforge->add_field("isWaitList tinyint(4) NULL DEFAULT '0'");
        $this->dbforge->add_field("qrPath tinytext NULL DEFAULT ''");
        $this->dbforge->add_field("barcodePath tinytext NULL DEFAULT ''");
        $this->dbforge->add_field("isRegister tinyint(4) NULL DEFAULT '0'");
        $this->dbforge->add_field("event_id int(11) NULL DEFAULT NULL");
 
        $this->dbforge->add_key('id', TRUE);
        
        $this->dbforge->create_table('invitation_booker_details', TRUE);
	}

	public function down()
	{
		$this->dbforge->drop_table('invitation_booker_details');
	}
	
	
}	
