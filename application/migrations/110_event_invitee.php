<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_event_invitee extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field("id int(11) unsigned NOT NULL AUTO_INCREMENT");
        $this->dbforge->add_field("event_id int(11) NULL DEFAULT NULL");
        $this->dbforge->add_field("user_id int(11) NULL DEFAULT NULL");
        $this->dbforge->add_field("join_status tinyint(4) NULL DEFAULT NULL");
        
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('event_invitee', TRUE);
	}

	public function down()
	{
		$this->dbforge->drop_table('event_invitee');
	}
	
	
}	
