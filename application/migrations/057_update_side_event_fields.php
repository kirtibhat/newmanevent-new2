<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_update_side_event_fields extends CI_Migration {

  public function up()
  {
    $this->db->query('ALTER TABLE `nm_side_event` ADD `common_total_price` varchar(100) NULL DEFAULT NULL AFTER `common_price`');
    $this->db->query('ALTER TABLE `nm_side_event` ADD `common_gst_included` varchar(100) NULL DEFAULT NULL AFTER `common_total_price`');
  }

  public function down()
  {
    
  }
  
} 
