<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_update_social_media_custom_fields extends CI_Migration {

  public function up()
  {                                                                                                                                               
    $this->db->query("ALTER TABLE `nm_event_social_media` ADD `venue_id` int(11) NULL DEFAULT NULL AFTER `website` ");   
    $this->db->query("ALTER TABLE `nm_event_social_media` ADD `linkedIn` varchar(255) NULL DEFAULT NULL AFTER `venue_id` ");   
    $this->db->query("ALTER TABLE `nm_event_social_media_custom` ADD `venue_id` int(11) NULL DEFAULT NULL AFTER `event_id` ");
  }

  public function down()
  {
    
  }
  
} 
