<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update_breakout extends CI_Migration {

    public function up()
	{
		
		// Add new field in stream breakout table
        $fields1 = array(
				'session_location' => array(
				'type' => 'varchar',
				'constraint' => 250, 
				'null' => TRUE,
			),
				'session_limit' => array(
				'type' => 'int',
				'constraint' => 11, 
				'null' => TRUE,
			)
		); 
        
        //add field in nm_side_event table
		$this->dbforge->add_column('breakout_session', $fields1);
		
		// Add new field in breakout session table Restrict Session Acces
        $this->db->query("ALTER TABLE `nm_breakout_session` ADD `session_restrict_access` ENUM( '1', '0' ) NOT NULL DEFAULT '0' COMMENT '1 allow Restrict Session Access 0 means not' AFTER `session_limit` ");
		
		
		
		
		// Add new field in breakout table as block type
        $this->db->query("ALTER TABLE `nm_breakouts` ADD `block_type` ENUM( '1', '0' ) NOT NULL DEFAULT '1' COMMENT '1 means plenary 0 means non plenary' AFTER `event_id` ");
				
		
		//add new field in breakout table
        $this->db->query("ALTER TABLE `nm_breakouts` ADD `allow_stream_hopping` ENUM( '1', '0' ) NOT NULL DEFAULT '0' COMMENT '1 means allowed stream hopping' AFTER `number_of_session` ");
		
		
		//add new field in breakout stream table
        $this->db->query("ALTER TABLE `nm_breakout_stream` ADD `restrict_stream_access` ENUM( '1', '0' ) NOT NULL DEFAULT '0' COMMENT '1 means allowed stream access' AFTER `block_limit` ");
		
		// Create new table to store registrant category
		
		$this->dbforge->add_field('id int(11) unsigned NOT NULL AUTO_INCREMENT');
        $this->dbforge->add_field('stream_id int(11) NULL DEFAULT NULL');
        $this->dbforge->add_field('registrant_id int(11) NULL DEFAULT NULL');
        $this->dbforge->add_field('registrant_limit int(11) NULL DEFAULT NULL');
            
		//add key in the field 
        $this->dbforge->add_key('id', TRUE);
        
        //add table with fields
        $this->dbforge->create_table('breakouts_stream_registrants', TRUE);
        
		// Add new field in stream breakout table
        $fields1 = array(
				'session_name' => array(
				'type' => 'varchar',
				'constraint' => 150, 
				'null' => TRUE,
			),
				'number_of_presenters' => array(
				'type' => 'int',
				'constraint' => 11, 
				'null' => TRUE,
			),
			'presenters_ids' => array(
				'type' => 'text',
				'null' => TRUE,
			),'presenters_description' => array(
				'type' => 'varchar',
				'constraint' => 255, 
				'null' => TRUE,
			),
		); 
        
        //add field in nm_side_event table
		$this->dbforge->add_column('breakout_session', $fields1);
		
		
		
		
		 //add fields
		$this->dbforge->add_field('id int(11) unsigned NOT NULL AUTO_INCREMENT');
        $this->dbforge->add_field('event_id int(11) NULL DEFAULT NULL');
        $this->dbforge->add_field('title varchar(50) NULL DEFAULT NULL');
        $this->dbforge->add_field('firstname varchar(255) NULL DEFAULT NULL');
        $this->dbforge->add_field('lastname varchar(255) NULL DEFAULT NULL');
        $this->dbforge->add_field('organisation  varchar(255) NULL DEFAULT NULL');
        $this->dbforge->add_field('email  varchar(75) NULL DEFAULT NULL');
        $this->dbforge->add_field('mobile  varchar(75) NULL DEFAULT NULL');
        $this->dbforge->add_field('phone  varchar(75) NULL DEFAULT NULL');
        $this->dbforge->add_field('topic  varchar(255) NULL DEFAULT NULL');
        $this->dbforge->add_field('presentation_description  text NULL DEFAULT NULL');
        $this->dbforge->add_field("is_registration_required enum('0','1') NOT NULL DEFAULT '0' COMMENT '0 means registration is not required'");
        $this->dbforge->add_field('registration_type int(11) NOT NULL DEFAULT "0"');
     
		//add key in the field 
        $this->dbforge->add_key('id', TRUE);
        
        //add table with fields
        $this->dbforge->create_table('program_presenter', TRUE);
        
       
        
        
	}

	public function down()
	{
		
	}
	
}	
