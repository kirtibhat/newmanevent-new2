<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Payment_registration_fee_field_change extends CI_Migration {

  public function up()
  {
    $prefix = $this->db->dbprefix;
    $this->db->query("UPDATE {$prefix}payment_registration_fee SET `fee_to_be_paid_by`=0 WHERE `fee_to_be_paid_by`='registrant'");
    $this->db->query("UPDATE {$prefix}payment_registration_fee SET `fee_to_be_paid_by`=1 WHERE `fee_to_be_paid_by`='client'");
    $this->db->query("UPDATE {$prefix}payment_registration_fee SET `fee_to_be_paid_by`=1 WHERE `fee_to_be_paid_by`='organisor'");

    $fields = array(
        'fee_to_be_paid_by' => array(
        'name' => 'fee_to_be_paid_by',
        'type' => "VARCHAR(2) NOT NULL DEFAULT '0' COMMENT '0 for registrant, 1 for client, 2 for organisor'",
      ),
    );
    $this->dbforge->modify_column('payment_registration_fee', $fields);
 }

  public function down()
  {
    
  }
  
} 
