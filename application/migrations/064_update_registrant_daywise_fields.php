<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_update_registrant_daywise_fields extends CI_Migration {

  public function up()
  {
    $this->db->query('ALTER TABLE `nm_event_registrant_daywise` ADD `early_bird_limit` int(11) NULL DEFAULT NULL AFTER `date_early_bird`');     
    $this->db->query("ALTER TABLE `nm_event_registrant_daywise` ADD `complimentary` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT '1 for Yes, 0 for No' AFTER `registrant_daywise_limit`");
    $this->db->query('ALTER TABLE `nm_event_registrant_daywise` ADD `total_price_inc_gst` int(11) NULL DEFAULT NULL AFTER `gst`');
  }

  public function down()
  {
    
  }
  
} 

