<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Payment_gateway extends CI_Migration {


	public function up()
	{
		$this->dbforge->add_field("id int(11) unsigned NOT NULL AUTO_INCREMENT");
		$this->dbforge->add_field("merchant_id varchar(255) NULL DEFAULT NULL");
		$this->dbforge->add_field("event_id int(11) NOT NULL DEFAULT '0'");
	
		$this->dbforge->add_key('id', TRUE);
		 
		// add payment gateway table 
		$this->dbforge->create_table('payment_gateway', TRUE);
	}
	
	public function down()
	{
		$this->dbforge->drop_table('payment_gateway');
	}
	
}	
