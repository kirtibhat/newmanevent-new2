<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Payment_bank_fee_field_change extends CI_Migration {

  public function up()
  {
    $prefix = $this->db->dbprefix;
    $this->db->query("UPDATE {$prefix}payment_bank_fee SET `fee_to_be_paid_by`=0 WHERE `fee_to_be_paid_by`='registrant'");
    $this->db->query("UPDATE {$prefix}payment_bank_fee SET `fee_to_be_paid_by`=1 WHERE `fee_to_be_paid_by`='client'");
    $this->db->query("UPDATE {$prefix}payment_bank_fee SET `fee_to_be_paid_by`=1 WHERE `fee_to_be_paid_by`='organisor'");

    $fields = array(
        'fee_to_be_paid_by' => array(
        'name' => 'fee_to_be_paid_by',
        'type' => "VARCHAR(2) NOT NULL DEFAULT '0' COMMENT '0 for registrant, 1 for client, 2 for organisor'",
      ),
    );
    $this->dbforge->modify_column('payment_bank_fee', $fields);

    
    $field_add = array(
        'registeration_fee' => array(
        'type' => 'INT',
        'constraint' => 11, 
        'null' => TRUE,
      )
    );
    $this->dbforge->add_column('payment_bank_fee', $field_add);
    
 }

  public function down()
  {
    
  }
  
} 
