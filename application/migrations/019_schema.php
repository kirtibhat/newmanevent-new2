<?php  defined('BASEPATH') OR exit('No direct script access allowed');
//to remove payment status column from user table
//Add payment_status and payment_id in frnt_registrant table
 
class Migration_Schema extends CI_Migration {


	public function up()
	{
		
		$fields = array(
				'payment_status' => array(
				'type' => 'boolean',
				'null' => FALSE,
				'default' => 0
			),
			'payment_id' => array(
				'type' => 'int',
				'constraint' => 11, 
				'null' => FALSE,
				'default' => '0'
			), 
			
		);  
		
	     //add field in nm_frnt_registrant table
		 $this->dbforge->add_column('frnt_registrant', $fields);
		
		//remove payamentstatus column form 'user'
		 $this->dbforge->drop_column('user', 'paymentstatus');
		 
		 
         //this query run for block limit comment in nm_breakout_stream
         $this->db->query("ALTER TABLE  `nm_breakout_stream` CHANGE  `block_limit`  `block_limit` VARCHAR( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL COMMENT 'block limit consider as a stream limit'");
		

	}
	
	public function down()
	{	
		//drop from user table
	   $this->dbforge->drop_table('user');
	}
	
}	
