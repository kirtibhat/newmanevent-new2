<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_update_user_fields extends CI_Migration {

  public function up()
  {                                                                                                                                               
    $this->db->query("ALTER TABLE `nm_user` ADD `fb_id` bigint NULL DEFAULT NULL AFTER `newman_nickname` ");   
    $this->db->query("ALTER TABLE `nm_user` ADD `gplus_id` varchar(255) NULL DEFAULT NULL AFTER `fb_id` ");   
  }

  public function down()
  {
    
  }
  
} 
