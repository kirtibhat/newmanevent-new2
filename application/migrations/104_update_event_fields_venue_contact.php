<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_update_event_fields_venue_contact extends CI_Migration {

  public function up()
  {                                                                                                                                               
    $this->db->query("ALTER TABLE `nm_event` ADD `venue_contact_firstname` varchar(255) NULL DEFAULT NULL AFTER `event_category` ");    
    $this->db->query("ALTER TABLE `nm_event` ADD `venue_contact_lastname` varchar(255) NULL DEFAULT NULL AFTER `venue_contact_firstname` ");    
    $this->db->query("ALTER TABLE `nm_event` ADD `venue_contact_email` varchar(255) NULL DEFAULT NULL AFTER `venue_contact_lastname` ");    
  }

  public function down()
  {
    
  }
  
} 
