<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Font_packages extends CI_Migration {

  public function up()
  {
    $prefix = $this->db->dbprefix;
    
    //add fields                                               
    $this->dbforge->add_field('id int(11) unsigned NOT NULL AUTO_INCREMENT');
    $this->dbforge->add_field('package_name varchar(100) NULL DEFAULT NULL');
    
    //add key in the field 
    $this->dbforge->add_key('id', TRUE);

    //add table with fields
    $this->dbforge->create_table('font_packages', TRUE);

    $this->db->query("INSERT INTO `{$prefix}font_packages` (`package_name`) VALUES ('Helvectica')");
  }

  public function down()
  {
    
  }

} 
