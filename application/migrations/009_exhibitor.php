<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Exhibitor extends CI_Migration {


		public function up()
		{
			
			//drop not required all column name form 'nm_exhibitor'
			$this->dbforge->drop_column('exhibitor', 'nm_exhibitor_floor_plan_id');
			$this->dbforge->drop_column('exhibitor', 'booth_size_w');
			$this->dbforge->drop_column('exhibitor', 'booth_size_h');
			$this->dbforge->drop_column('exhibitor', 'walls');
			$this->dbforge->drop_column('exhibitor', 'walls_width');
			$this->dbforge->drop_column('exhibitor', 'walls_hight');
			$this->dbforge->drop_column('exhibitor', 'booth_description');
			$this->dbforge->drop_column('exhibitor', 'lights');
			$this->dbforge->drop_column('exhibitor', 'light_description');
			$this->dbforge->drop_column('exhibitor', 'signage');
			$this->dbforge->drop_column('exhibitor', 'signage_description');
			$this->dbforge->drop_column('exhibitor', 'power_outlet');
			$this->dbforge->drop_column('exhibitor', 'power_outlet_description');
			$this->dbforge->drop_column('exhibitor', 'stage');
			$this->dbforge->drop_column('exhibitor', 'stage_description');
			$this->dbforge->drop_column('exhibitor', 'stage_size_w');
			$this->dbforge->drop_column('exhibitor', 'stage_size_h');
			$this->dbforge->drop_column('exhibitor', 'chair');
			$this->dbforge->drop_column('exhibitor', 'price_each_chair');
			$this->dbforge->drop_column('exhibitor', 'gst_each_chair');
			$this->dbforge->drop_column('exhibitor', 'table');
			$this->dbforge->drop_column('exhibitor', 'price_each_table');
			$this->dbforge->drop_column('exhibitor', 'complementary_registration');
			$this->dbforge->drop_column('exhibitor', 'discount_registration');
			$this->dbforge->drop_column('exhibitor', 'limit_per_person_discount_registration');
			$this->dbforge->drop_column('exhibitor', 'percentage_discount_standard_registration');
			$this->dbforge->drop_column('exhibitor', 'table_size_w');
			$this->dbforge->drop_column('exhibitor', 'gst_each_table');
			
			//drop not required all column name form 'nm_exhibitor_side_event'
			$this->dbforge->drop_column('exhibitor_side_event', 'complementary_side_event');
			$this->dbforge->drop_column('exhibitor_side_event', 'limit_per_person_for_complementary');
			$this->dbforge->drop_column('exhibitor_side_event', 'additional_guset');
			$this->dbforge->drop_column('exhibitor_side_event', 'limit_per_person_additional_guest');
			$this->dbforge->drop_column('exhibitor_side_event', 'total_price_each_additional_guest');
	
		
			//change column name in 'exhibitor'
			$fields = array(
					'table_size_h' => array(
								 'name' => 'additional_details',
								 'type' => 'varchar',
								'constraint' => 255, 
								'null' => TRUE,
								'default' => NULL
						),
					);
					
			$this->dbforge->modify_column('exhibitor', $fields);
			
			
			 //change column name in 'exhibitor_side_event'
			  $fields   = array(
                        'gst_each_additional_guest' => array(
									 'name' => 'side_event_limit',
									 'type' => 'varchar',
									'constraint' => 45, 
									'null' => FALSE,
									'default' => 0
							),
						);
			//change column name			
			$this->dbforge->modify_column('exhibitor_side_event', $fields);
		}
		
		public function down()
		{
			$this->dbforge->drop_table('exhibitor');
			$this->dbforge->drop_table('exhibitor_side_event');
		}
	
	
}	
