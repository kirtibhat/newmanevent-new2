<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Program_presenter extends CI_Migration {

    public function up()
	{
		
		 //add fields
		$this->dbforge->add_field('id int(11) unsigned NOT NULL AUTO_INCREMENT');
        $this->dbforge->add_field('event_id int(11) NULL DEFAULT NULL');
        $this->dbforge->add_field('title varchar(50) NULL DEFAULT NULL');
        $this->dbforge->add_field('firstname varchar(255) NULL DEFAULT NULL');
        $this->dbforge->add_field('lastname varchar(255) NULL DEFAULT NULL');
        $this->dbforge->add_field('organisation  varchar(255) NULL DEFAULT NULL');
        $this->dbforge->add_field('email  varchar(75) NULL DEFAULT NULL');
        $this->dbforge->add_field('mobile  varchar(75) NULL DEFAULT NULL');
        $this->dbforge->add_field('phone  varchar(75) NULL DEFAULT NULL');
        $this->dbforge->add_field('topic  varchar(255) NULL DEFAULT NULL');
        $this->dbforge->add_field('presentation_description  text NULL DEFAULT NULL');
        $this->dbforge->add_field("is_registration_required enum('0','1') NOT NULL DEFAULT '0' COMMENT '0 means registration is not required'");
        $this->dbforge->add_field('registration_type int(11) NOT NULL DEFAULT "0"');
     
		//add key in the field 
        $this->dbforge->add_key('id', TRUE);
        
        //add table with fields
        $this->dbforge->create_table('program_presenter', TRUE);
        
        
        
        //insert data in the database
        $fields1 = array(
				'presenter_id' => array(
				'type' => 'int',
				'constraint' => 11, 
				'null' => TRUE,
			)
		); 
        
        //add field in nm_side_event table
		$this->dbforge->add_column('breakouts', $fields1);
        
	}

	public function down()
	{
		
	}
	
}	
