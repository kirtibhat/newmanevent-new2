<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Frnt_sponsor_booth extends CI_Migration {

    public function up()
	{
	    //add fields
		$this->dbforge->add_field("id int(11) unsigned NOT NULL AUTO_INCREMENT");
        $this->dbforge->add_field("sponsor_id int(11) NULL DEFAULT NULL");
        $this->dbforge->add_field("booth_id int(11) NULL DEFAULT NULL");
        $this->dbforge->add_field("user_id int(11) NULL DEFAULT NULL");
        $this->dbforge->add_field("booth_status int(11) NULL DEFAULT '0' COMMENT '0=pending, 1=approved,2=unapproved'");
     
         
		//add key in the field 
        $this->dbforge->add_key('id', TRUE);
        
        //add table with fields
        $this->dbforge->create_table('frnt_sponsor_booth', TRUE);
       
    
	}

	public function down()
	{
		$this->dbforge->drop_table('frnt_sponsor_booth');
	}
	
}	
