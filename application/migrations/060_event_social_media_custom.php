<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_event_social_media_custom extends CI_Migration {

  public function up(){
      
    $prefix = $this->db->dbprefix;
    
    $this->dbforge->add_field('id int(11) unsigned NOT NULL AUTO_INCREMENT');
    $this->dbforge->add_field('event_id int(11) NOT NULL DEFAULT "0"');
    $this->dbforge->add_field('media_name varchar(254) NULL DEFAULT NULL');
    $this->dbforge->add_field('media_value varchar(254) NULL DEFAULT NULL');

    //add key in the field 
    $this->dbforge->add_key('id', TRUE);

    //add table with fields
    $this->dbforge->create_table('event_social_media_custom', TRUE);
  }

  public function down()
  {
    
  }
  
} 



