<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_update_event_extras_fields extends CI_Migration {

  public function up()
  {
    $this->db->query('ALTER TABLE `nm_event_extras` ADD `common_total_price` varchar(100) NULL DEFAULT NULL AFTER `common_price`');
    $this->db->query('ALTER TABLE `nm_event_extras` ADD `common_gst_included` varchar(100) NULL DEFAULT NULL AFTER `common_total_price`');
  }

  public function down()
  {
    
  }
  
} 
