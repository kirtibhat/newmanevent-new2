<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_update_event_venue_fields extends CI_Migration {

  public function up()
  {                                                                                                                                               
    $this->db->query('ALTER TABLE `nm_event` ADD `venue_contact_mobile` varchar(255) NULL DEFAULT NULL AFTER `venue_contact_email`');
    $this->db->query('ALTER TABLE `nm_event` ADD `venue_contact_landline` varchar(255) NULL DEFAULT NULL AFTER `venue_contact_mobile`');
  }

  public function down()
  {
    
  }
  
} 
