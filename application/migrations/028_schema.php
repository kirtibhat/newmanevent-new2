<?php  defined('BASEPATH') OR exit('No direct script access allowed');
//This migration is used to add column in nm_event table
 
class Migration_Schema extends CI_Migration {


	public function up()
	{
		
		
		//insert data in the database
		$fields1 = array(
				'early_bird_reg_date' => array(
				'type' => 'datetime',
				'null' => TRUE,
				
			),
		);
		
		//add field in master_number_manage table
		$this->dbforge->add_column('event', $fields1);
		
	
	}
	
	public function down()
	{	
		
	}
	
}	
