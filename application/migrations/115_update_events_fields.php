<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_update_events_fields extends CI_Migration {

  public function up()
  {                                                                                                                                               
    $this->db->query('ALTER TABLE `nm_event` ADD `is_cancelled` tinyint NULL DEFAULT "0" AFTER `event_url`');
  }

  public function down()
  {
    
  }
  
} 
