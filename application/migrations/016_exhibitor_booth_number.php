<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Exhibitor_booth_number extends CI_Migration {

    public function up()
	{
	   //to call down function	
	   $this->down();
	         
	}

	public function down()
	{
		
		//remove nm_exhibitor_booth_number
		$this->dbforge->drop_table('exhibitor_booth_number');
	}
	
}	
