<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_event_theme_fields_header extends CI_Migration {

  public function up()
  {                                                                                                                                               
    $this->db->query("ALTER TABLE `nm_event_themes_data` ADD `header_image_name` varchar(255) NULL DEFAULT NULL AFTER `header_image_size` ");    
  }

  public function down()
  {
    
  }
  
} 
