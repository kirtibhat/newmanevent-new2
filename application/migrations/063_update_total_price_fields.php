<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_update_total_price_fields extends CI_Migration {

  public function up()
  {
    //Add Total Price Inc GST in nm_corporate_package Table  
    $this->db->query('ALTER TABLE `nm_corporate_package` ADD `total_price_inc_gst` varchar(45) NULL DEFAULT NULL AFTER `gst`');    
  }

  public function down()
  {
    
  }
  
} 
