<?php  defined('BASEPATH') OR exit('No direct script access allowed');
//This is used to add is_earlybird column in frnt_registrant & frnt_side_event table
 
class Migration_Schema extends CI_Migration {


  public function up()
  {
    
    $fields = array(
      'paypal_username' => array(
        'type' => 'varchar',
        'constraint' => 256, 
        'null' => TRUE,
        'default' => NULL
      ),
      'paypal_password' => array(
        'type' => 'varchar',
        'constraint' => 256, 
        'null' => TRUE,
        'default' => NULL
      ),
      'paypal_signature' => array(
        'type' => 'varchar',
        'constraint' => 256, 
        'null' => TRUE,
        'default' => NULL
      ),
    );  
    
    $fields1 = array(
      'registrant_total_price' => array(
        'type' => 'varchar',
        'constraint' => 256, 
        'null' => TRUE,
        'default' => '0'
      ),
      'registrant_gst' => array(
        'type' => 'varchar',
        'constraint' => 256, 
        'null' => TRUE,
        'default' => '0'
      ),
      'registrant_fees_price' => array(
        'type' => 'varchar',
        'constraint' => 256, 
        'null' => TRUE,
        'default' => '0'
      ),
      'registrant_fees_gst' => array(
        'type' => 'varchar',
        'constraint' => 256, 
        'null' => TRUE,
        'default' => '0'
      ),
      'registrant_fees_pay_by' => array(
        'type' => 'varchar',
        'constraint' => 256, 
        'null' => TRUE,
        'default' => NULL
      ),
    );  
    
    
    
    $fields2 = array(
      'event_contact_person' => array(
        'type' => 'varchar',
        'constraint' => 256, 
        'null' => TRUE,
        'default' => '1'
      ),
    ); 
    
    
    $fields3 = array(
      'is_abn' => array(
        'type' => 'varchar',
        'constraint' => 256, 
        'null' => TRUE,
        'default' => '1'
      ),
    );
  
      //add field in nm_payment_gateway table
    $this->dbforge->add_column('payment_gateway', $fields);
    
      //add field in nm_frnt_registrant table
    $this->dbforge->add_column('frnt_registrant', $fields1);
    
      //add field in nm_event_contact_person table
    $this->dbforge->add_column('event_contact_person', $fields2);
    
     //add field in nm_event_invoice table
    $this->dbforge->add_column('event_invoice', $fields3);
    
  }
  
  public function down()
  { 
    
  }
  
} 
