<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_update_default_theme_fields_new extends CI_Migration {

  public function up()
  {                                                                                                                                               
    $this->db->query("ALTER TABLE `nm_event_default_themes` ADD `header_image` varchar(255) NULL DEFAULT NULL AFTER `event_theme_header_image` ");   
    $this->db->query("ALTER TABLE `nm_event_default_themes` ADD `logo_image` varchar(255) NULL DEFAULT NULL AFTER `header_image` ");   
    $this->db->query("ALTER TABLE `nm_event_default_themes` ADD `header_image_path` varchar(255) NULL DEFAULT NULL AFTER `logo_image` ");   
    $this->db->query("ALTER TABLE `nm_event_default_themes` ADD `logo_image_path` varchar(255) NULL DEFAULT NULL AFTER `header_image_path` ");   
    $this->db->query("ALTER TABLE `nm_event_default_themes` ADD `logo_size` text NULL DEFAULT NULL AFTER `logo_image_path` ");   
    $this->db->query("ALTER TABLE `nm_event_default_themes` ADD `theme_type` varchar(255) NULL DEFAULT NULL AFTER `logo_size` ");   
    $this->db->query("ALTER TABLE `nm_event_default_themes` ADD `sponsor_image_object` longtext NULL DEFAULT NULL AFTER `theme_type` ");   
    $this->db->query("ALTER TABLE `nm_event_default_themes` ADD `primary_color` varchar(255) NULL DEFAULT NULL AFTER `sponsor_image_object` ");   
    $this->db->query("ALTER TABLE `nm_event_default_themes` ADD `secondary_color` varchar(255) NULL DEFAULT NULL AFTER `primary_color` ");   
  }

  public function down()
  {
    
  }
  
} 
