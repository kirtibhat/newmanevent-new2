<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_event_default_themes extends CI_Migration {
  
  public function up(){
      
    $prefix = $this->db->dbprefix;
    
    $this->dbforge->add_field('id int(11) unsigned NOT NULL AUTO_INCREMENT');
    //$this->dbforge->add_field('event_id int(11) NOT NULL DEFAULT "0"');
    $this->dbforge->add_field('event_theme_name varchar(255) NULL DEFAULT NULL');
    $this->dbforge->add_field('event_theme_screen_bg varchar(255) NULL DEFAULT NULL');
    $this->dbforge->add_field('event_theme_header_bg varchar(255) NULL DEFAULT NULL');
    $this->dbforge->add_field('event_theme_box_bg varchar(255) NULL DEFAULT NULL');
    $this->dbforge->add_field('event_theme_text_color varchar(255) NULL DEFAULT NULL');
    $this->dbforge->add_field('event_theme_link_color varchar(255) NULL DEFAULT NULL');
    $this->dbforge->add_field('event_theme_link_highlighted_color varchar(255) NULL DEFAULT NULL');
    $this->dbforge->add_field('event_theme_order varchar(255) NULL DEFAULT NULL');
    

    //add key in the field 
    $this->dbforge->add_key('id', TRUE);

    //add table with fields
    $this->dbforge->create_table('event_default_themes', TRUE);
    
    $this->db->query("INSERT INTO `{$prefix}event_default_themes` (`event_theme_name`,`event_theme_screen_bg`,`event_theme_header_bg`,`event_theme_box_bg`,`event_theme_text_color`,`event_theme_link_color`,`event_theme_link_highlighted_color`,`event_theme_order`) VALUES ('theme_1','#ccc','#FAFAD2','#90EE90','#FFB6C1','#87CEFA','#B0C4DE','1')");
    
    $this->db->query("INSERT INTO `{$prefix}event_default_themes` (`event_theme_name`,`event_theme_screen_bg`,`event_theme_header_bg`,`event_theme_box_bg`,`event_theme_text_color`,`event_theme_link_color`,`event_theme_link_highlighted_color`,`event_theme_order`) VALUES ('theme_2','#90EE90','#FAFAD2','#B0C4DE','#FFB6C1','#87CEFA','#90EE90','2')");
    
    $this->db->query("INSERT INTO `{$prefix}event_default_themes` (`event_theme_name`,`event_theme_screen_bg`,`event_theme_header_bg`,`event_theme_box_bg`,`event_theme_text_color`,`event_theme_link_color`,`event_theme_link_highlighted_color`,`event_theme_order`) VALUES ('theme_3','#87CEFA','#B0C4DE','#90EE90','#FFB6C1','#FAFAD2','#B0C4DE','3')");
    
    $this->db->query("INSERT INTO `{$prefix}event_default_themes` (`event_theme_name`,`event_theme_screen_bg`,`event_theme_header_bg`,`event_theme_box_bg`,`event_theme_text_color`,`event_theme_link_color`,`event_theme_link_highlighted_color`,`event_theme_order`) VALUES ('theme_4','#B0C4DE','#ccc','#FAFAD2','#B0C4DE','#FAFAD2','#B0C4DE','4')");
    
    $this->db->query("INSERT INTO `{$prefix}event_default_themes` (`event_theme_name`,`event_theme_screen_bg`,`event_theme_header_bg`,`event_theme_box_bg`,`event_theme_text_color`,`event_theme_link_color`,`event_theme_link_highlighted_color`,`event_theme_order`) VALUES ('theme_5','#87CEFA','#B0C4DE','#90EE90','#FFB6C1','#FAFAD2','#B0C4DE','5')");
    
    
  }

  public function down()
  {
    
  }
  
} 
