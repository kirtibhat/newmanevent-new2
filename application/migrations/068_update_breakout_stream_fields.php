<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_update_breakout_stream_fields extends CI_Migration {

  public function up()
  {                                                                                                                                               
    $this->db->query('ALTER TABLE `nm_breakout_stream` ADD `number_of_session` varchar(50) NULL DEFAULT NULL AFTER `restrict_stream_access`');    
  }

  public function down()
  {
    
  }
  
} 


