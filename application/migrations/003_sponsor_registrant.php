<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Sponsor_registrant extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field("id int(11) unsigned NOT NULL AUTO_INCREMENT");
        $this->dbforge->add_field("sponsor_id int(11) NOT NULL DEFAULT '0'");
        $this->dbforge->add_field("registrant_id int(11) NOT NULL DEFAULT '0'");
        $this->dbforge->add_field("registrant_limit int(11) NULL DEFAULT NULL");
        $this->dbforge->add_field("registrant_daywise_id int(11) NULL DEFAULT NULL COMMENT 'if user checked single day registrant the insert id'");
 
        $this->dbforge->add_key('id', TRUE);
        
        $this->dbforge->create_table('sponsor_registrant', TRUE);
	}

	public function down()
	{
		$this->dbforge->drop_table('sponsor_registrant');
	}
	
	
}	
