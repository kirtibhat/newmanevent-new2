<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_update_custom_default_theme_fields extends CI_Migration {

  public function up()
  {                                                                                                                                               
    $this->db->query("ALTER TABLE `nm_event_default_themes` ADD `event_theme_header_image` varchar(255) NULL DEFAULT NULL AFTER `event_theme_order` ");   
  }

  public function down()
  {
    
  }
  
} 
