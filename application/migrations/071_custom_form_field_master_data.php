<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_custom_form_field_master_data extends CI_Migration {

  public function up()
  {
    $prefix = $this->db->dbprefix;
    
    $this->db->query("INSERT INTO `{$prefix}custom_form_field_master` (`field_name`,`field_type`,`default_value`,`form_id`,`field_status`,`is_editable`,`status`,`order_status`) VALUES ('title','selectbox','NULL','4','1','1','1','1')");
    
    $this->db->query("INSERT INTO `{$prefix}custom_form_field_master` (`field_name`,`field_type`,`default_value`,`form_id`,`field_status`,`is_editable`,`status`,`order_status`) VALUES ('first name','text','NULL','4','1','0','1','2')");
    
    $this->db->query("INSERT INTO `{$prefix}custom_form_field_master` (`field_name`,`field_type`,`default_value`,`form_id`,`field_status`,`is_editable`,`status`,`order_status`) VALUES ('last name','text','NULL','4','1','0','1','3')");
    
    $this->db->query("INSERT INTO `{$prefix}custom_form_field_master` (`field_name`,`field_type`,`default_value`,`form_id`,`field_status`,`is_editable`,`status`,`order_status`) VALUES ('email','text','NULL','4','1','0','1','4')");
    
    $this->db->query("INSERT INTO `{$prefix}custom_form_field_master` (`field_name`,`field_type`,`default_value`,`form_id`,`field_status`,`is_editable`,`status`,`order_status`) VALUES ('organisation','text','NULL','4','1','0','1','5')");
    
    $this->db->query("INSERT INTO `{$prefix}custom_form_field_master` (`field_name`,`field_type`,`default_value`,`form_id`,`field_status`,`is_editable`,`status`,`order_status`) VALUES ('position','text','NULL','4','1','0','1','6')");
    
    $this->db->query("INSERT INTO `{$prefix}custom_form_field_master` (`field_name`,`field_type`,`default_value`,`form_id`,`field_status`,`is_editable`,`status`,`order_status`) VALUES ('Phone','text','NULL','4','1','0','1','7')");
    
    
  }

  public function down()
  {
    //$this->db->query("Delete from `{$prefix}custom_form_field_master` where  id>48");
  }
  
} 


