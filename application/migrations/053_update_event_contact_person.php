<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update_event_contact_person extends CI_Migration {

  public function up()
  {
      $this->db->query('ALTER TABLE `nm_event_contact_person` ADD `fbid` varchar(100) NULL DEFAULT NULL');
      $this->db->query('ALTER TABLE `nm_event_contact_person` ADD `twitterid` varchar(100) NULL DEFAULT NULL');
      $this->db->query('ALTER TABLE `nm_event_contact_person` ADD `linkedin` varchar(100) NULL DEFAULT NULL');
      $this->db->query('ALTER TABLE `nm_event_contact_person` ADD `gplus` varchar(100) NULL DEFAULT NULL');
      $this->db->query('ALTER TABLE `nm_event_contact_person` ADD `pintrest` varchar(100) NULL DEFAULT NULL');
  }

  public function down()
  {
    
  }
  
} 

