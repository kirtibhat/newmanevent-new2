<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Payment_cc_fee_field_change extends CI_Migration {

  public function up()
  {
    $prefix = $this->db->dbprefix;
    $this->db->query("UPDATE {$prefix}payment_cc_fee SET `who_will_pay_fee`=0 WHERE `who_will_pay_fee`='registrant'");
    $this->db->query("UPDATE {$prefix}payment_cc_fee SET `who_will_pay_fee`=1 WHERE `who_will_pay_fee`='client'");
    $this->db->query("UPDATE {$prefix}payment_cc_fee SET `who_will_pay_fee`=1 WHERE `who_will_pay_fee`='organisor'");
    
    $fields = array(
        'who_will_pay_fee' => array(
        'name' => 'who_will_pay_fee',
        'type' => "VARCHAR(2) NOT NULL DEFAULT '0' COMMENT '0 for registrant, 1 for client, 2 for organisor'",
      ),
    );
    $this->dbforge->modify_column('payment_cc_fee', $fields);
 }

  public function down()
  {
    
  }
  
} 
