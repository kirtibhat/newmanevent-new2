<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Schema extends CI_Migration {


	public function up()
	{
		
		$fields1 = array(
				'is_undeletable' => array(
				'type' => 'boolean',
				'null' => FALSE,
				'default' => 0
			),
			'dietary_order' => array(
				'type' => 'int',
				'constraint' => 11, 
				'null' => FALSE,
				'default' => '0'
			),
		);
		
		$fields2 = array(
				'start_time' => array(
				'type' => 'time',
				'null' => TRUE,
			),
				'end_time' => array(
				'type' => 'time',
				'null' => TRUE,
			),
		);
		
		$fields3 = array(
				'block_limit' => array(
				'type' => 'varchar',
				'constraint' => 256, 
				'null' => TRUE,
				'default' => NULL
			)
		);
		
		$fields4 = array(
				'organisation' => array(
				'type' => 'varchar',
				'constraint' => 256, 
				'null' => TRUE,
				'default' => NULL
			)
		);


		//add field in event_dietary table
		$this->dbforge->add_column('event_dietary', $fields1);
		
		//drop not required all column name form 'nm_event'
		$this->dbforge->drop_column('event', 'dietary_requirments');
		
		//drop from  event_dietary
		$this->dbforge->drop_table('breakout_block');
		
		//add field in breakouts table
		$this->dbforge->add_column('breakouts', $fields2);
		
		//add field in breakout_stream table
		$this->dbforge->add_column('breakout_stream', $fields3);
		
		//set colomn form breakout session
		$this->dbforge->drop_column('breakout_session', 'block_id');
		$this->dbforge->drop_column('breakout_session', 'session_limit');
		
		//add field in nm_breakout_stream_session table
		$this->dbforge->add_column('breakout_stream_session', $fields4);
		
	}
	
	public function down()
	{	
		//drop from event_dietary
		$this->dbforge->drop_table('event_dietary');
		
		//drop from event_dietary
		$this->dbforge->drop_table('breakouts');
		
		//drop from breakout_stream
		$this->dbforge->drop_table('breakout_stream');
		
	}
	
}	
