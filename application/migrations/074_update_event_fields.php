<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_update_event_fields extends CI_Migration {

  public function up()
  {                                                                                                                                               
    $this->db->query("ALTER TABLE `nm_event` ADD `event_lat` text NULL DEFAULT NULL AFTER `eventVenueZip` ");    
    $this->db->query("ALTER TABLE `nm_event` ADD `event_long` text NULL DEFAULT NULL AFTER `event_lat`");    
    $this->db->query("ALTER TABLE `nm_event` ADD `show_map` tinyint(4) NULL DEFAULT 0 AFTER `event_long`");    
  }

  public function down()
  {
    
  }
  
} 
