<?php  defined('BASEPATH') OR exit('No direct script access allowed');
//This is used to add is_earlybird column in frnt_registrant & frnt_side_event table
 
class Migration_Schema extends CI_Migration {

  public function up()
  {
    
    // insert form name in nm_form table
     $data = array(
            array('name' => "Corporate Application Details",
            'type' => "1")
         );
       
         $this->db->insert_batch('form', $data);
    
    // insert data into nm_customize_forms
    
    // insert form name in nm_form table
     $data = array(
            array('field_name'    => "organisation",
          'field_type'    => "text",
          'default_value' => "NULL",
          'form_id'     => "2",
          'field_status'  => "1",
          'is_editable'   => "0",
          'status'      => "1",         
          ),
       array('field_name'   => "title",
          'field_type'    => "selectbox",
          'default_value' => "NULL",
          'form_id'     => "2",
          'field_status'  => "1",
          'is_editable'   => "1",
          'status'      => "1",         
          ),
       array('field_name'   => "first name",
          'field_type'    => "text",
          'default_value' => "NULL",
          'form_id'     => "2",
          'field_status'  => "1",
          'is_editable'   => "0",
          'status'      => "1",         
          ),
       array('field_name'   => "last name",
          'field_type'    => "text",
          'default_value' => "NULL",
          'form_id'     => "2",
          'field_status'  => "1",
          'is_editable'   => "0",
          'status'      => "1",         
          ),
       array('field_name'   => "email",
          'field_type'    => "text",
          'default_value' => "NULL",
          'form_id'     => "2",
          'field_status'  => "1",
          'is_editable'   => "0",
          'status'      => "1",         
          ),
       array('field_name'   => "department",
          'field_type'    => "text",
          'default_value' => "NULL",
          'form_id'     => "2",
          'field_status'  => "1",
          'is_editable'   => "0",
          'status'      => "1",         
          ),
       array('field_name'   => "phone 1",
          'field_type'    => "text",
          'default_value' => "NULL",
          'form_id'     => "2",
          'field_status'  => "1",
          'is_editable'   => "0",
          'status'      => "1",         
          ),
       array('field_name'   => "website",
          'field_type'    => "text",
          'default_value' => "NULL",
          'form_id'     => "2",
          'field_status'  => "1",
          'is_editable'   => "0",
          'status'      => "1",         
          )                           
         );
         
         $this->db->insert_batch('custom_form_field_master', $data);
    
    
    
      //insert new column in the database // event_contact_person
      $fields1 = array(
            'phone1_mobile' => array(
                'type' => 'varchar',
                'constraint' => 30, 
                'null' => TRUE,
            ),'phone1_landline' => array(
                'type' => 'varchar',
                'constraint' => 30, 
                'null' => TRUE,
            ),
            'phone2_mobile' => array(
            'type' => 'varchar',
            'constraint' => 30, 
            'null' => TRUE,
            ),
            'phone2_landline' => array(
                'type' => 'varchar',
                'constraint' => 30, 
                'null' => TRUE,
            )
      );
      
      //add field in user table
      $this->dbforge->add_column('event_contact_person', $fields1);
      
      //insert new column in the database // event_contact_person
      $fields2 = array(
            'contact_mobile' => array(
                'type' => 'varchar',
                'constraint' => 30, 
                'null' => TRUE,
            )
      );
      
      //add field in user table
      $this->dbforge->add_column('event', $fields2);  
        
  }
  
  public function down()
  { 
    
  }
  
}
