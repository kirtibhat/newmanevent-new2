<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_update_user_fields extends CI_Migration {

  public function up()
  {                                                                                                                                               
    $this->db->query("ALTER TABLE `nm_user` ADD `newman_nickname` text NULL DEFAULT NULL AFTER `registration_type` ");   
  }

  public function down()
  {
    
  }
  
} 
