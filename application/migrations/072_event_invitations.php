<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_event_invitations extends CI_Migration {

  public function up()
  {
    $prefix = $this->db->dbprefix;
    
    //add fields                                               
    $this->dbforge->add_field('id int(11) unsigned NOT NULL AUTO_INCREMENT');
    $this->dbforge->add_field('event_id int(11) NOT NULL DEFAULT 0');
    $this->dbforge->add_field('invitation_name varchar(255) NOT NULL DEFAULT ""');
    $this->dbforge->add_field('password varchar(100) NULL DEFAULT NULL');
    $this->dbforge->add_field('invitation_limit int(11) NULL DEFAULT NULL');
    $this->dbforge->add_field('details text NULL DEFAULT NULL');
    $this->dbforge->add_field('invitations_order int(11) NOT NULL DEFAULT 0');
    
    //add key in the field 
    $this->dbforge->add_key('id', TRUE);

    //add table with fields
    $this->dbforge->create_table('event_invitations', TRUE);
  }

  public function down()
  {
    
  }

} 
