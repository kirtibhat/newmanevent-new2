<?php  defined('BASEPATH') OR exit('No direct script access allowed');
//This migration is used to create table for breakout session registrants
 
class Migration_Schema extends CI_Migration {


	public function up()
	{
		
		//insert data in the database
        $fields1 = array(
				'start_datetime' => array(
				'type' => 'datetime',
				'null' => TRUE,
			),
				'end_datetime' => array(
				'type' => 'datetime',
				'null' => TRUE,
			)
		); 
        
        //add field in nm_side_event table
		$this->dbforge->add_column('side_event', $fields1);
		
		//remove column from side event table
		$this->dbforge->drop_column('side_event', 'start_time');
		$this->dbforge->drop_column('side_event', 'end_time');
		$this->dbforge->drop_column('side_event', 'date');
		
		
		$fields4 = array(
				'unit_price' => array(
				'type' => 'varchar',
				'constraint' => 256, 
				'null' => TRUE,
				'default' => NULL
			),
				'unit_price_early_bird' => array(
				'type' => 'varchar',
				'constraint' => 256, 
				'null' => TRUE,
				'default' => NULL
			),
				'unit_price_for_additional_guest' => array(
				'type' => 'varchar',
				'constraint' => 256, 
				'null' => TRUE,
				'default' => NULL
			),
				'unit_price_for_complementory_guest' => array(
				'type' => 'varchar',
				'constraint' => 256, 
				'null' => TRUE,
				'default' => NULL
			)
		); 
		
		//add field in nm_side_event_registrant table
		$this->dbforge->add_column('side_event_registrant', $fields4);
	
	}
	
	public function down()
	{	
		
	}
	
}	
