<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Extra_registrant extends CI_Migration {

  public function up()
  {
    //add fields                                               
    $this->dbforge->add_field('id int(11) unsigned NOT NULL AUTO_INCREMENT');
    $this->dbforge->add_field('extra_id int(11) NULL DEFAULT NULL');
    $this->dbforge->add_field('registrant_id int(11) NULL DEFAULT NULL');
    $this->dbforge->add_field('reg_limit int(11) NULL DEFAULT NULL');

    $this->dbforge->add_field("complementary enum('0','1') NOT NULL DEFAULT '0' COMMENT '1 means is complementary'");
    $this->dbforge->add_field('total_price  varchar(255) NULL DEFAULT NULL');
    $this->dbforge->add_field('gst_included  varchar(75) NULL DEFAULT NULL');
    
    //add key in the field 
    $this->dbforge->add_key('id', TRUE);

    //add table with fields
    $this->dbforge->create_table('event_extra_registrant', TRUE);
  }

  public function down()
  {
    
  }

} 
