<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Sponsor extends CI_Migration {

	public function up()
	{
		$fields = array(
				'additional_booth_price' => array(
				'type' => 'varchar',
				'constraint' => 45, 
				'null' => TRUE,
				'default' => NULL
			),
				'additional_booth_gst' => array(
				'type' => 'varchar',
				'constraint' => 45, 
				'null' => TRUE,
				'default' => NULL
			)
		   
		);

		$this->dbforge->add_column('sponsor', $fields);
	}

	public function down()
	{
		$this->dbforge->drop_table('sponsor');
	}
	
	
}	
