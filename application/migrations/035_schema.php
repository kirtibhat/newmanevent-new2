<?php  defined('BASEPATH') OR exit('No direct script access allowed');
//This is used to add is_earlybird column in frnt_registrant & frnt_side_event table
 
class Migration_Schema extends CI_Migration {

	public function up()
	{
			
		//insert new column in the database // Allow Day Group Bookings
		$fields2 = array(
					'allow_day_group_booking' => array(
							'type' => 'tinyint',
							'constraint' => 4, 
							'null' => TRUE,
					)
		);
		
		//add field in user table
		$this->dbforge->add_column('event_registrant_daywise', $fields2);
			
		
		//insert new column in the database // category in event registrant
		
		$fields3 = array(
							'category_id' => array(
							'type' => 'int',
							'constraint' => 11, 
							'null' => TRUE,
							'default' => '0'
					)
		);
		
		//add field in user table
		$this->dbforge->add_column('event_registrants', $fields3);
		
	}
	
	public function down()
	{	
		
	}
	
}
