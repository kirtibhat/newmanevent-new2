<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_update_event_fields extends CI_Migration {

  public function up()
  {                                                                                                                                               
    $this->db->query("ALTER TABLE `nm_event` ADD `eventVenueAddress1` text NULL DEFAULT NULL AFTER `reg_closedate` ");    
    $this->db->query("ALTER TABLE `nm_event` ADD `eventVenueAddress2` text NULL DEFAULT NULL AFTER `eventVenueAddress1`");    
    $this->db->query("ALTER TABLE `nm_event` ADD `eventVenueCity` varchar(255) NULL DEFAULT NULL AFTER `eventVenueAddress2`");    
    $this->db->query("ALTER TABLE `nm_event` ADD `eventVenueState` varchar(255) NULL DEFAULT NULL AFTER `eventVenueCity`");    
    $this->db->query("ALTER TABLE `nm_event` ADD `eventVenueZip` int(11) NULL DEFAULT NULL AFTER `eventVenueState`");    
  }

  public function down()
  {
    
  }
  
} 
