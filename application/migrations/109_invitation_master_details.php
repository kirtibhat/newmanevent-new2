<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_invitation_master_details extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field("id int(11) unsigned NOT NULL AUTO_INCREMENT");
        $this->dbforge->add_field("event_id int(11) NOT NULL DEFAULT '0'");
        $this->dbforge->add_field("firstname varchar(255) NULL DEFAULT NULL");
        $this->dbforge->add_field("lastname varchar(255) NULL DEFAULT NULL");
        $this->dbforge->add_field("email varchar(255) NULL DEFAULT NULL");
        $this->dbforge->add_field("qrcode varchar(255) NULL DEFAULT NULL");
        $this->dbforge->add_field("barcode varchar(255) NULL DEFAULT NULL");
 
        $this->dbforge->add_key('id', TRUE);
        
        $this->dbforge->create_table('invitation_master_details', TRUE);
	}

	public function down()
	{
		$this->dbforge->drop_table('invitation_master_details');
	}
	
	
}	
