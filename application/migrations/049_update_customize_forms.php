<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update_customize_forms extends CI_Migration {

  public function up()
  {
    $this->db->query("ALTER TABLE `nm_customize_forms` ADD `colour_scheme` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT '0 for dark 1 means light'");
    $this->db->query('ALTER TABLE `nm_customize_forms` ADD `background_colour` VARCHAR(50) NULL DEFAULT NULL AFTER `colour_scheme`');
    $this->db->query('ALTER TABLE `nm_customize_forms` ADD `main_colour` VARCHAR(50) NULL DEFAULT NULL AFTER `background_colour`');
    $this->db->query('ALTER TABLE `nm_customize_forms` ADD `highlight_colour` VARCHAR(50) NULL DEFAULT NULL AFTER `main_colour`');
    $this->db->query('ALTER TABLE `nm_customize_forms` ADD `font_package` INT(11) NULL DEFAULT NULL AFTER `highlight_colour`');
  }

  public function down()
  {
    
  }
  
} 

