<?php  defined('BASEPATH') OR exit('No direct script access allowed');
//This is used to add is_earlybird column in frnt_registrant & frnt_side_event table
 
class Migration_Schema extends CI_Migration {


	public function up()
	{
		
		$this->dbforge->add_field("id int(11) unsigned NOT NULL AUTO_INCREMENT");
		$this->dbforge->add_field("operand varchar(255) NULL DEFAULT NULL");
		$this->dbforge->add_field("event varchar(255) NULL DEFAULT NULL");
		$this->dbforge->add_field("registrant varchar(255) NULL DEFAULT NULL");
		
		$this->dbforge->add_key('id', TRUE);
		 
		// add master booking number table 
		$this->dbforge->create_table('master_number_manage', TRUE);
		
		//insert data in the database
        $this->db->query("INSERT INTO `nm_master_number_manage` (`operand`, `event`, `registrant`) VALUES ('AA01', 'AA01', 'AA01')");
        
        
        $fields1 = array(
				'event_number' => array(
				'type' => 'varchar',
				'constraint' => 256, 
				'null' => TRUE,
				'default' => 'NULL'
			)
		); 
        
        $fields2 = array(
				'operand_number' => array(
				'type' => 'varchar',
				'constraint' => 256, 
				'null' => TRUE,
				'default' => 'NULL'
			)
		); 
		
		 $fields3 = array(
				'registrant_number' => array(
				'type' => 'varchar',
				'constraint' => 256, 
				'null' => TRUE,
				'default' => 'NULL'
			)
		); 
	    
        //add field in nm_event table
		$this->dbforge->add_column('event', $fields1);
		
		//add field in nm_user table
		$this->dbforge->add_column('user', $fields2);
		
		//add field in nm_event_registrants table
		$this->dbforge->add_column('event_registrants', $fields3);
		
		
		
		//--------add code in 13-may-2014--------//
		$fields4 = array(
				'unit_price' => array(
				'type' => 'varchar',
				'constraint' => 256, 
				'null' => TRUE,
				'default' => NULL
			),
				'unit_price_early_bird' => array(
				'type' => 'varchar',
				'constraint' => 256, 
				'null' => TRUE,
				'default' => NULL
			)
		); 
		
		
		//add field in nm_event_registrants table
		$this->dbforge->add_column('event_registrants', $fields4);
		
		
		
		$fields5 = array(
				'unit_price' => array(
				'type' => 'varchar',
				'constraint' => 256, 
				'null' => TRUE,
				'default' => NULL
			),
				'unit_price_early_bird' => array(
				'type' => 'varchar',
				'constraint' => 256, 
				'null' => TRUE,
				'default' => NULL
			)
		); 
		
		
		//add field in nm_event_registrants table
		$this->dbforge->add_column('event_registrant_daywise', $fields5);
		
		
		$fields6 = array(
				'registrants_type' => array(
				'type' => 'int',
				'constraint' => 11, 
				'null' => FALSE,
				'default' => '0'
			),
		); 
		
		
		//add field in nm_event_registrants table
		$this->dbforge->add_column('event_registrants', $fields6);
		
		//add comment 
		$this->db->query("ALTER TABLE  `nm_event_registrants` CHANGE  `registrants_type`  `registrants_type` INT( 11 ) NOT NULL DEFAULT  '0' COMMENT '0=main registrant, 1=side event registrant, 2=sponsor registrants, 3=exhibitor registrants'");
	
	}
	
	public function down()
	{	
		
	}
	
}	
