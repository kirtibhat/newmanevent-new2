<?php  defined('BASEPATH') OR exit('No direct script access allowed');
//This is used to add is_earlybird column in frnt_registrant & frnt_side_event table
 
class Migration_Schema extends CI_Migration {


	public function up()
	{
		
		$fields2 = array(
				'booking_id' => array(
				'type' => 'varchar',
				'constraint' => 256, 
				'null' => TRUE,
				'default' => '0'
			),
				'received_amount' => array(
				'type' => 'varchar',
				'constraint' => 256, 
				'null' => TRUE,
				'default' => '0'
			),
				'outstanding_amount' => array(
				'type' => 'varchar',
				'constraint' => 256, 
				'null' => TRUE,
				'default' => '0'
			),
				'payment_type' => array(
				'type' => 'varchar',
				'constraint' => 256, 
				'null' => TRUE,
				'default' => NULL
			),
				'bank_name' => array(
				'type' => 'varchar',
				'constraint' => 256, 
				'null' => TRUE,
				'default' => NULL
			),
			'payment_received' => array(
				'type' => 'varchar',
				'constraint' => 256, 
				'null' => TRUE,
				'default' => '0',
			),
			
		);  
		
		$fields3 = array(
                        'amount' => array(
						'name' => 'total_amount',
						'type' => 'varchar',
						'constraint' => 256, 
						'null' => TRUE,
						'default' => '0'
					),
				);
	  
		//add field in nm_frnt_payment_details table
		$this->dbforge->add_column('frnt_payment_details', $fields2);
		
		//add field in nm_frnt_payment_details table
		$this->dbforge->modify_column('frnt_payment_details', $fields3);
		
		//this query run for block limit comment in nm_frnt_registrant
        $this->db->query("ALTER TABLE  `nm_frnt_payment_details` CHANGE  `payment_received`  `payment_received` VARCHAR( 256 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT  '0' COMMENT  '0=pending,1=received, 2=not received'");
		
	}
	
	public function down()
	{	
		
	}
	
}	
