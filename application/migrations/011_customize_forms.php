<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Customize_forms extends CI_Migration {


	public function up()
	{
		$fields = array(
				'logo_image' => array(
				'type' => 'varchar',
				'constraint' => 255, 
				'null' => TRUE,
				'default' => NULL
			),
			'logo_image_path' => array(
				'type' => 'varchar',
				'constraint' => 255, 
				'null' => TRUE,
				'default' => NULL
			),
		);

		$this->dbforge->add_column('customize_forms', $fields);
	}
	
	public function down()
	{
		$this->dbforge->drop_table('customize_forms');
	}
	
}	
