<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Event_invoice extends CI_Migration {


	public function up()
	{
		$fields = array(
				'above_address' => array(
				'type' => 'varchar',
				'constraint' => 50, 
				'null' => TRUE,
				'default' => NULL
			),
		);
		$fieldsrefund = array(
				'refund_document_file' => array(
				'type' => 'varchar',
				'constraint' => 256, 
				'null' => TRUE,
				'default' => NULL
			),
				'refund_document_path' => array(
				'type' => 'varchar',
				'constraint' => 256, 
				'null' => TRUE,
				'default' => NULL
			),
		);
		
		$fieldsuser = array(
				'email_verify' => array(
				'type' => 'boolean',
				'null' => FALSE,
				'default' => 0
			),
		);
	
		//add field in event invoice table
		$this->dbforge->add_column('event_invoice', $fields);
		
		//add field in event terma condition table
		$this->dbforge->add_column('event_term_conditions', $fieldsrefund);
		
		//add field in user table
		$this->dbforge->add_column('user', $fieldsuser);
		
		$this->dbforge->add_field("id int(11) unsigned NOT NULL AUTO_INCREMENT");
		$this->dbforge->add_field("user_id int(11) NOT NULL DEFAULT '0'");
		$this->dbforge->add_field("old_password varchar(255) NULL DEFAULT NULL");
		$this->dbforge->add_field("manual_changed enum('0','1') NOT NULL DEFAULT '0' COMMENT '0=automatically, 1=manual'");
		$this->dbforge->add_field("failed_login_attempt  int(11) NOT NULL DEFAULT '0'");
		$this->dbforge->add_field("changed_date TIMESTAMP NOT NULL");
		
		$this->dbforge->add_key('id', TRUE);
		 
		// add payment gateway table 
		$this->dbforge->create_table('password_chagned_summery', TRUE);
	}
	
	public function down()
	{	
		//drop from  event_invoice
		$this->dbforge->drop_table('event_invoice');
		
		//drop from  event_term_conditions
		$this->dbforge->drop_table('event_term_conditions');
		
		//drop from  user
		$this->dbforge->drop_table('user');
		
		//drop password_chagned_summery
		$this->dbforge->drop_table('password_chagned_summery');
		
		
	}
	
}	
