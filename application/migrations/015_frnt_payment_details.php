<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Frnt_payment_details extends CI_Migration {

    public function up()
	{
	    //add fields
		$this->dbforge->add_field("id int(11) unsigned NOT NULL AUTO_INCREMENT");
        $this->dbforge->add_field("user_id int(11) NULL DEFAULT NULL");
        $this->dbforge->add_field("transaction_id varchar(45) NULL DEFAULT NULL");
        $this->dbforge->add_field("amount varchar(255) NULL DEFAULT NULL");
        $this->dbforge->add_field("payment_date datetime");
     
         
		//add key in the field 
        $this->dbforge->add_key('id', TRUE);
        
        //add table with fields
        $this->dbforge->create_table('frnt_payment_details', TRUE);
       
    
	}

	public function down()
	{
		$this->dbforge->drop_table('frnt_payment_details');
	}
	
}	
