<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_custom_form_field_master_data extends CI_Migration {

  public function up()
  {
    $prefix = $this->db->dbprefix;
    
    $this->db->query("INSERT INTO `{$prefix}custom_form_field_master` (`field_name`,`field_type`,`default_value`,`form_id`,`field_status`,`is_editable`,`status`) VALUES ('Facebook-Id','varchar(255)','NULL','1','1','0','1')");
    $this->db->query("INSERT INTO `{$prefix}custom_form_field_master` (`field_name`,`field_type`,`default_value`,`form_id`,`field_status`,`is_editable`,`status`) VALUES ('Twitter-Id','varchar(255)','NULL','1','1','0','1')");
    $this->db->query("INSERT INTO `{$prefix}custom_form_field_master` (`field_name`,`field_type`,`default_value`,`form_id`,`field_status`,`is_editable`,`status`) VALUES ('Linkedin-Id','varchar(255)','NULL','1','1','0','1')");
    $this->db->query("INSERT INTO `{$prefix}custom_form_field_master` (`field_name`,`field_type`,`default_value`,`form_id`,`field_status`,`is_editable`,`status`) VALUES ('GooglePlus-Id','varchar(255)','NULL','1','1','0','1')");
    $this->db->query("INSERT INTO `{$prefix}custom_form_field_master` (`field_name`,`field_type`,`default_value`,`form_id`,`field_status`,`is_editable`,`status`) VALUES ('Pintrest-Id','varchar(255)','NULL','1','1','0','1')");

    $this->db->query("INSERT INTO `{$prefix}custom_form_field_master` (`field_name`,`field_type`,`default_value`,`form_id`,`field_status`,`is_editable`,`status`) VALUES ('Facebook-Id','varchar(255)','NULL','2','1','0','1')");
    $this->db->query("INSERT INTO `{$prefix}custom_form_field_master` (`field_name`,`field_type`,`default_value`,`form_id`,`field_status`,`is_editable`,`status`) VALUES ('Twitter-Id','varchar(255)','NULL','2','1','0','1')");
    $this->db->query("INSERT INTO `{$prefix}custom_form_field_master` (`field_name`,`field_type`,`default_value`,`form_id`,`field_status`,`is_editable`,`status`) VALUES ('Linkedin-Id','varchar(255)','NULL','2','1','0','1')");
    $this->db->query("INSERT INTO `{$prefix}custom_form_field_master` (`field_name`,`field_type`,`default_value`,`form_id`,`field_status`,`is_editable`,`status`) VALUES ('GooglePlus-Id','varchar(255)','NULL','2','1','0','1')");
    $this->db->query("INSERT INTO `{$prefix}custom_form_field_master` (`field_name`,`field_type`,`default_value`,`form_id`,`field_status`,`is_editable`,`status`) VALUES ('Pintrest-Id','varchar(255)','NULL','2','1','0','1')");
    $this->db->query("INSERT INTO `{$prefix}custom_form_field_master` (`field_name`,`field_type`,`default_value`,`form_id`,`field_status`,`is_editable`,`status`) VALUES ('ABN','varchar(50)','NULL','2','1','0','1')");

    $this->db->query("INSERT INTO `{$prefix}custom_form_field_master` (`field_name`,`field_type`,`default_value`,`form_id`,`field_status`,`is_editable`,`status`) VALUES ('Facebook-Id','varchar(255)','NULL','3','1','0','1')");
    $this->db->query("INSERT INTO `{$prefix}custom_form_field_master` (`field_name`,`field_type`,`default_value`,`form_id`,`field_status`,`is_editable`,`status`) VALUES ('Twitter-Id','varchar(255)','NULL','3','1','0','1')");
    $this->db->query("INSERT INTO `{$prefix}custom_form_field_master` (`field_name`,`field_type`,`default_value`,`form_id`,`field_status`,`is_editable`,`status`) VALUES ('Linkedin-Id','varchar(255)','NULL','3','1','0','1')");
    $this->db->query("INSERT INTO `{$prefix}custom_form_field_master` (`field_name`,`field_type`,`default_value`,`form_id`,`field_status`,`is_editable`,`status`) VALUES ('GooglePlus-Id','varchar(255)','NULL','3','1','0','1')");
    $this->db->query("INSERT INTO `{$prefix}custom_form_field_master` (`field_name`,`field_type`,`default_value`,`form_id`,`field_status`,`is_editable`,`status`) VALUES ('Pintrest-Id','varchar(255)','NULL','3','1','0','1')");
    
  }

  public function down()
  {
    //$this->db->query("Delete from `{$prefix}custom_form_field_master` where  id>48");
  }
  
} 


