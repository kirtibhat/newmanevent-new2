<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_update_event_url_fields extends CI_Migration {

  public function up()
  {                                                                                                                                               
    $this->db->query('ALTER TABLE `nm_event` ADD `event_url` tinytext NULL DEFAULT NULL AFTER `venue_contact_landline`');
  }

  public function down()
  {
    
  }
  
} 
