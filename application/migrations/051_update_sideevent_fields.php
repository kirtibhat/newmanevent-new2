<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update_sideevent_fields extends CI_Migration {

  public function up()
  {
    $fields = array(
        'early_bird_total_price' => array(
        'name' => 'early_bird_total_price',
        'type' => "VARCHAR(75)",
        ),
        'early_bird_gst_included' => array(
        'name' => 'early_bird_gst_included',
        'type' => "VARCHAR(75)",
        ),
      );
    $this->dbforge->modify_column('side_event', $fields);
  }

  public function down()
  {
    
  }
  
} 
?>
