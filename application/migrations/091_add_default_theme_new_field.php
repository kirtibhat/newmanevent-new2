<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_default_theme_new_field extends CI_Migration {

  public function up()
  {                                                                                                                                               
    $this->db->query("ALTER TABLE `nm_event_default_themes` ADD `is_header_or_bgcolor` varchar(255) NULL DEFAULT NULL AFTER `secondary_color` ");   
  }

  public function down()
  {
    
  }
  
} 
