<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Sponsor_exhibitor_booths extends CI_Migration {

	public function up()
	{
		//add fields
		$this->dbforge->add_field("id int(11) unsigned NOT NULL AUTO_INCREMENT");
        $this->dbforge->add_field("sponsor_id int(11) NULL DEFAULT NULL COMMENT 'if inserted record for sponsor then sponsor id' ");
        $this->dbforge->add_field("exhibitor_id int(11) NULL DEFAULT NULL COMMENT 'if inserted record for exhibitor then exhibitor id' ");
        $this->dbforge->add_field("booth_position varchar(45) NULL DEFAULT NULL");
        $this->dbforge->add_field("event_id int(11) NOT NULL DEFAULT '0'");
      
		//add key in the field 
        $this->dbforge->add_key('id', TRUE);
        
        //add table with fields
        $this->dbforge->create_table('sponsor_exhibitor_booths', TRUE);
	}

	public function down()
	{
		$this->dbforge->drop_table('sponsor_exhibitor_booths');
	}
	
}	
