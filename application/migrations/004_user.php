<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_User extends CI_Migration {

	public function up()
	{
		$fields = array(
				'user_title' => array(
				'type' => 'varchar',
				'constraint' => 255, 
				'null' => TRUE,
				'default' => NULL
			)
		   
		);

		$this->dbforge->add_column('user', $fields);
	}

	public function down()
	{
		$this->dbforge->drop_table('user');
	}
	
	
}	
