<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_update_event_fields extends CI_Migration {

  public function up()
  {                                                                                                                                               
    $this->db->query('ALTER TABLE `nm_event` ADD `event_is_public` tinyint(4) NOT NULL DEFAULT 1 AFTER `show_map`');
  }

  public function down()
  {
    
  }
  
} 
