<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_update_event_fields_category extends CI_Migration {

  public function up()
  {                                                                                                                                               
    $this->db->query("ALTER TABLE `nm_event` ADD `event_category` tinyint(4) NOT NULL DEFAULT 0 AFTER `event_is_public` ");    
  }

  public function down()
  {
    
  }
  
} 
