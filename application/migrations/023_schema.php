<?php  defined('BASEPATH') OR exit('No direct script access allowed');
//This is used to add is_earlybird column in frnt_registrant & frnt_side_event table
 
class Migration_Schema extends CI_Migration {


	public function up()
	{
		
		$fields2 = array(
				'time_zone' => array(
				'type' => 'varchar',
				'constraint' => 256, 
				'null' => TRUE,
				'default' => NULL
			),
				'destination' => array(
				'type' => 'varchar',
				'constraint' => 256, 
				'null' => TRUE,
				'default' => NULL
			)
		); 
		
		$fields3 = array(
				'is_collect_gst' => array(
				'type' => 'varchar',
				'constraint' => 256, 
				'null' => FALSE,
				'default' => '0'
			),
				'gst_rate' => array(
				'type' => 'varchar',
				'constraint' => 256, 
				'null' => TRUE,
				'default' => NULL
			)
		); 
		
		//add field in nm_frnt_payment_details table
		$this->dbforge->add_column('event', $fields2);
		
		//remove column from event table
		$this->dbforge->drop_column('event', 'cut_off_date');
		$this->dbforge->drop_column('event', 'exhibition_number');
		
		//remove column from term and condition table
		$this->dbforge->drop_column('event_term_conditions', 'refund_policy');
		$this->dbforge->drop_column('event_term_conditions', 'refund_email');
		$this->dbforge->drop_column('event_term_conditions', 'refund_document_file');
		$this->dbforge->drop_column('event_term_conditions', 'refund_document_path');
		
		//add field in nm_event_invoice table
		$this->dbforge->add_column('event_invoice', $fields3);
		
	}
	
	public function down()
	{	
		
	}
	
}	
