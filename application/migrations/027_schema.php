<?php  defined('BASEPATH') OR exit('No direct script access allowed');
//This migration is used to create table for breakout session registrants
 
class Migration_Schema extends CI_Migration {


	public function up()
	{
		
		
		//insert data in the database
		$fields1 = array(
				'registration' => array(
				'type' => 'varchar',
				'constraint' => 45, 
				'null' => TRUE,
				'default' => NULL
				
			),
		);
		
		$fields2 = array(
				'registration_number' => array(
				'type' => 'varchar',
				'constraint' => 45, 
				'null' => TRUE,
				'default' => NULL
				
			),
			'registration_booking_number' => array(
				'type' => 'varchar',
				'constraint' => 225, 
				'null' => TRUE,
				'default' => NULL
				
			),
		); 
		$fields3 = array(
				'registrants_type' => array(
				'type' => 'int',
				'constraint' => 11, 
				'null' => FALSE,
				'default' => '0'
			),
		); 
		
			
		
		//add field in master_number_manage table
		$this->dbforge->add_column('master_number_manage', $fields1);
		
		//add field in frnt_registrant table
		$this->dbforge->add_column('frnt_registrant', $fields2);
	
		//$this->db->query("UPDATE INTO `nm_master_number_manage` (`registration`) VALUES ('AA01')");
	 	$this->db->query("UPDATE `nm_master_number_manage` SET  `registration` =  'AA01' WHERE `nm_master_number_manage`.`id` =1" );
		
		//add comment 
		$this->db->query("ALTER TABLE  `nm_frnt_registrant` ADD  `registration_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER  `registration_booking_number`");
			
		//add field in frnt_registrant table
		$this->dbforge->add_column('frnt_registrant', $fields3);
		
		//add comment 
		$this->db->query("ALTER TABLE  `nm_frnt_registrant` CHANGE  `registrants_type`  `registrants_type` INT( 11 ) NOT NULL DEFAULT  '0' COMMENT '0=main registrant, 1=side event registrant, 2=sponsor registrants, 3=exhibitor registrants'");
	
		//add code in 23-may-2013
		$fields7 = array(
				'registrants_limit_early_bird' => array(
				'type' => 'int',
				'constraint' => 11, 
				'null' => TRUE,
			)
		);
		
		//add field in frnt_registrant table
		$this->dbforge->add_column('event_registrants', $fields7);
		
		// added code 27-may-2014
		$fields8 = array(
				'contact_name' => array(
				'type' => 'varchar',
				'constraint' => 150, 
				'null' => TRUE,
			),
			'contact_phone' => array(
				'type' => 'varchar',
				'constraint' => 150, 
				'null' => TRUE,
				
			),
			'contact_email' => array(
				'type' => 'varchar',
				'constraint' => 150, 
				'null' => TRUE,
				
			)
		); 
	    
	    //add field in frnt_event table
		$this->dbforge->add_column('event', $fields8);
	
	}
	
	public function down()
	{	
		
	}
	
}	
