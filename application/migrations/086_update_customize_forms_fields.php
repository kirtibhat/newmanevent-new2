<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_update_customize_forms_fields extends CI_Migration {

  public function up()
  {                                                                                                                                               
    $this->db->query("ALTER TABLE `nm_customize_forms` ADD `logo_position` text NULL DEFAULT NULL AFTER `font_package` ");   
  }

  public function down()
  {
    
  }
  
} 
