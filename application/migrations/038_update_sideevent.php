<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update_sideevent extends CI_Migration {

    public function up()
  {
    /*
    // Add new field in nm_side_event table
       $fields1 = array(
        'additional_details' => array(
        'type' => 'text',
        //'constraint' => 250, 
        'null' => TRUE,
      )
    ); 
        
        //add field in nm_side_event table
        $this->dbforge->add_column('side_event', $fields1);
    
    // Add new field (allow group booking) in side event table 
        $this->db->query("ALTER TABLE `nm_side_event` ADD `allow_group_booking` ENUM( '1', '0' ) NOT NULL DEFAULT '0' COMMENT '1 allow group booking' AFTER `allow_earlybird` ");
// Add new field (common price) in side event table 
        $this->db->query("ALTER TABLE `nm_side_event` ADD `common_price` ENUM( '1', '0' ) NOT NULL DEFAULT '0' COMMENT '1 for common price' AFTER `allow_group_booking` ");
         */

        
        $this->db->query('ALTER TABLE `nm_side_event` ADD `early_bird_limit` INT(11) NULL DEFAULT NULL AFTER `allow_earlybird`');
        $this->db->query('ALTER TABLE `nm_side_event` ADD `early_bird_total_price` INT(11) NULL DEFAULT NULL AFTER `early_bird_limit`');
        $this->db->query('ALTER TABLE `nm_side_event` ADD `early_bird_gst_included` INT(11) NULL DEFAULT NULL AFTER `early_bird_total_price`');
        
        $this->db->query('ALTER TABLE `nm_side_event` ADD `min_group_booking_size` INT(11) NULL DEFAULT NULL AFTER `allow_group_booking`');
        $this->db->query('ALTER TABLE `nm_side_event` ADD `percentage_discount_for_group_booking` VARCHAR(45) NULL DEFAULT NULL AFTER `min_group_booking_size`');

        $this->db->query("ALTER TABLE `nm_side_event` ADD `event_restrict_access` ENUM( '1', '0' ) NOT NULL DEFAULT '0' COMMENT '1 for Allow Restrict Session Access 0 means not' AFTER `additional_details` "); 
        
  }

  public function down()
  {
    
  }
  
} 
