<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_user_details extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field("user_details_id int(11) unsigned NOT NULL AUTO_INCREMENT");
        $this->dbforge->add_field("user_id int(11) NOT NULL");
        $this->dbforge->add_field("userLogo text DEFAULT NULL");
        $this->dbforge->add_field("show_password varchar(256) DEFAULT NULL");
        $this->dbforge->add_field("linkedIn varchar(256) DEFAULT NULL");
        $this->dbforge->add_field("facebook varchar(256) DEFAULT NULL");
        $this->dbforge->add_field("instagram varchar(256) DEFAULT NULL");
        $this->dbforge->add_field("twitter varchar(256) DEFAULT NULL");
        $this->dbforge->add_field("youtube varchar(256) DEFAULT NULL");
        $this->dbforge->add_field("pintrest varchar(256) DEFAULT NULL");
        $this->dbforge->add_field("customSocial text DEFAULT NULL");
        $this->dbforge->add_field("alert_emailid varchar(256) DEFAULT NULL");
        $this->dbforge->add_field("alert_mobile_no varchar(256) DEFAULT NULL");
        
        $this->dbforge->add_key('user_details_id', TRUE);
        
        $this->dbforge->create_table('user_details', TRUE);
	}

	public function down()
	{
		$this->dbforge->drop_table('user_details');
	}
	
	
}	
