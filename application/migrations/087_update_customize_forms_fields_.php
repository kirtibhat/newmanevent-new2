<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_update_customize_forms_fields_ extends CI_Migration {

  public function up()
  {                                                                                                                                               
    $this->db->query("ALTER TABLE `nm_customize_forms` ADD `sponsor_image_object` longtext NULL DEFAULT NULL AFTER `logo_position` ");   
  }

  public function down()
  {
    
  }
  
} 
