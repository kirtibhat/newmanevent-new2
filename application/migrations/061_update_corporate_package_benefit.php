<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_update_corporate_package_benefit extends CI_Migration {

  public function up(){
    $this->db->query("Delete from `nm_corporate_package_benefit` where event_id='0' and corporate_package_id='0' and benefit_name='Only ONE available'");
    $this->db->query("Delete from `nm_corporate_package_benefit` where event_id='0' and corporate_package_id='0' and benefit_name='Acknowledgement on the website'");
    $this->db->query("Delete from `nm_corporate_package_benefit` where event_id='0' and corporate_package_id='0' and benefit_name='Logo on passport'");
    $this->db->query("Delete from `nm_corporate_package_benefit` where event_id='0' and corporate_package_id='0' and benefit_name='Two complimentary registrations'");
    $this->db->query("Delete from `nm_corporate_package_benefit` where event_id='0' and corporate_package_id='0' and benefit_name='Access to delegate information'");
    $this->db->query("Delete from `nm_corporate_package_benefit` where event_id='0' and corporate_package_id='0' and benefit_name='Half page advertisement in the program'");
    $this->db->query("Delete from `nm_corporate_package_benefit` where event_id='0' and corporate_package_id='0' and benefit_name='Full page advertisement in the program'");
  }

  public function down()
  {     
    
  }
  
} 




