<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_update_event_booker_fields extends CI_Migration {

  public function up()
  {        
	$prefix = $this->db->dbprefix;                                                                                                                                       
    $this->db->query("INSERT INTO `{$prefix}custom_form_field_master` (`field_name`,`field_type`,`form_id`,`field_status`,`is_editable`,`status`,`order_status`) VALUES ('title','selectbox','5','1','1','1','1')");
    $this->db->query("INSERT INTO `{$prefix}custom_form_field_master` (`field_name`,`field_type`,`form_id`,`field_status`,`is_editable`,`status`,`order_status`) VALUES ('first name','text','5','1','0','1','2')");
    $this->db->query("INSERT INTO `{$prefix}custom_form_field_master` (`field_name`,`field_type`,`form_id`,`field_status`,`is_editable`,`status`,`order_status`) VALUES ('last name','text','5','1','0','1','3')");
    $this->db->query("INSERT INTO `{$prefix}custom_form_field_master` (`field_name`,`field_type`,`form_id`,`field_status`,`is_editable`,`status`,`order_status`) VALUES ('email','text','5','1','0','1','4')");
    $this->db->query("INSERT INTO `{$prefix}custom_form_field_master` (`field_name`,`field_type`,`form_id`,`field_status`,`is_editable`,`status`,`order_status`) VALUES ('organisation','text','5','1','0','1','5')");
    $this->db->query("INSERT INTO `{$prefix}custom_form_field_master` (`field_name`,`field_type`,`form_id`,`field_status`,`is_editable`,`status`,`order_status`) VALUES ('position','text','5','1','0','1','6')");
    $this->db->query("INSERT INTO `{$prefix}custom_form_field_master` (`field_name`,`field_type`,`form_id`,`field_status`,`is_editable`,`status`,`order_status`) VALUES ('Phone','text','5','1','0','1','7')");
    
  }

  public function down()
  {
    
  }
  
} 
