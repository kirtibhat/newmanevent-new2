<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_update_program_presenter_fields extends CI_Migration {

  public function up()
  {
    $this->db->query('ALTER TABLE `nm_program_presenter` ADD `fbid` varchar(100) NULL DEFAULT NULL');
    $this->db->query('ALTER TABLE `nm_program_presenter` ADD `twitterid` varchar(100) NULL DEFAULT NULL');
    $this->db->query('ALTER TABLE `nm_program_presenter` ADD `linkedin` varchar(100) NULL DEFAULT NULL');
    $this->db->query('ALTER TABLE `nm_program_presenter` ADD `gplus` varchar(100) NULL DEFAULT NULL');
    $this->db->query('ALTER TABLE `nm_program_presenter` ADD `pintrest` varchar(100) NULL DEFAULT NULL');
  }

  public function down()
  {
    
  }
  
} 
