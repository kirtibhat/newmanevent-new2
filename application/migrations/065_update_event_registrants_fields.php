<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_update_event_registrants_fields extends CI_Migration {

  public function up()
  {
    //Add Total Price Inc GST in nm_event_registrants Table
    $this->db->query('ALTER TABLE `nm_event_registrants` ADD `total_price_inc_gst` varchar(45) NULL DEFAULT NULL AFTER `gst`');
    $this->db->query('ALTER TABLE `nm_event_registrants` ADD `total_price_early_bird_inc_gst` varchar(45) NULL DEFAULT NULL AFTER `gst_early_bird`');
    
  }

  public function down()
  {
    
  }
  
} 

