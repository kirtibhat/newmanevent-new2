<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_update_event_theme_fields_new extends CI_Migration {

  public function up()
  {                                                                                                                                               
    $this->db->query("ALTER TABLE `nm_event_themes_data` ADD `header_image` varchar(255) NULL DEFAULT NULL AFTER `last_modified` ");   
    $this->db->query("ALTER TABLE `nm_event_themes_data` ADD `logo_image` varchar(255) NULL DEFAULT NULL AFTER `header_image` ");   
    $this->db->query("ALTER TABLE `nm_event_themes_data` ADD `header_image_path` varchar(255) NULL DEFAULT NULL AFTER `logo_image` ");   
    $this->db->query("ALTER TABLE `nm_event_themes_data` ADD `logo_image_path` varchar(255) NULL DEFAULT NULL AFTER `header_image_path` ");   
    $this->db->query("ALTER TABLE `nm_event_themes_data` ADD `logo_size` text NULL DEFAULT NULL AFTER `logo_image_path` ");   
    $this->db->query("ALTER TABLE `nm_event_themes_data` ADD `theme_type` varchar(255) NULL DEFAULT NULL AFTER `logo_size` ");   
    $this->db->query("ALTER TABLE `nm_event_themes_data` ADD `sponsor_image_object` longtext NULL DEFAULT NULL AFTER `theme_type` ");   
    $this->db->query("ALTER TABLE `nm_event_themes_data` ADD `primary_color` varchar(255) NULL DEFAULT NULL AFTER `sponsor_image_object` ");   
    $this->db->query("ALTER TABLE `nm_event_themes_data` ADD `secondary_color` varchar(255) NULL DEFAULT NULL AFTER `primary_color` ");   
  }

  public function down()
  {
    
  }
  
} 
