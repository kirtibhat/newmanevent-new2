<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Event_extras extends CI_Migration {

  public function up()
  {
    
    //add fields                                               
    $this->dbforge->add_field('id int(11) unsigned NOT NULL AUTO_INCREMENT');
    $this->dbforge->add_field('extra_name varchar(50) NULL DEFAULT NULL');
    $this->dbforge->add_field('event_id int(11) NULL DEFAULT NULL');
    $this->dbforge->add_field('additional_details text NULL DEFAULT NULL');
    $this->dbforge->add_field('extra_limit int(11) NULL DEFAULT NULL');
    $this->dbforge->add_field("common_price enum('0','1') NOT NULL DEFAULT '0' COMMENT '1 means is common_price'");
    $this->dbforge->add_field("restrict_purchase_access enum('0','1') NOT NULL DEFAULT '0' COMMENT '1 means is allow restrict purchase'");

    //add key in the field 
    $this->dbforge->add_key('id', TRUE);

    //add table with fields
    $this->dbforge->create_table('event_extras', TRUE);
  
  }

  public function down()
  {
    
  }
  
} 
