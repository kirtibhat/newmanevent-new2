<?php  defined('BASEPATH') OR exit('No direct script access allowed');
//This migration is used to create table for breakout session registrants
 
class Migration_Schema extends CI_Migration {


	public function up()
	{
	
		$this->dbforge->add_field("id int(11) unsigned NOT NULL AUTO_INCREMENT");
		$this->dbforge->add_field("stream_id int(11) NULL DEFAULT NULL");
		$this->dbforge->add_field("session_id int(11) NULL DEFAULT NULL");
		$this->dbforge->add_field("registrant_id int(11) NULL DEFAULT NULL");
		$this->dbforge->add_field("session_limit varchar(255) NULL DEFAULT NULL");
		
		
		
		$this->dbforge->add_key('id', TRUE);
		 
		// add master booking number table 
		$this->dbforge->create_table('breakouts_session_registrants', TRUE);
		
		//insert data in the database
        
        
        $fields1 = array(
				'stream_id' => array(
				'type' => 'int',
				'constraint' => 11, 
				'null' => TRUE,
				'default' => '0'
			)
		); 
        
      
        
        //add field in nm_event table
		$this->dbforge->add_column('breakout_session', $fields1);
	
	
	}
	
	public function down()
	{	
		
	}
	
}	
