<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_update_side_event_fields extends CI_Migration {

  public function up()
  {                                                                                                                                               
    $this->db->query('ALTER TABLE `nm_side_event` ADD `sideevent_registrants` varchar(255) NULL DEFAULT NULL AFTER `event_restrict_access`');    
  }

  public function down()
  {
    
  }
  
} 

