<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Execute_migrations {
	
	private $error = array();
	
	function __construct()
	{
		$this->ci = & get_instance();
		
		$this->ci->load->library('migration');
		
		if(!$this->ci->migration->latest())
		{
			show_error($this->ci->migration->error_string());
		}
	}

}
