<?php
/*
 * @description: this libraray for pagination
 * @auther : lokendra meena
 * 
 */

class pagination_custom
{
	//private var for current page number
	public $postPage	 = '1'; 
	
	//private var for total records 
	public $totalRecords	= '0' ; 
	
	//private var for limit of record
	public $limit	 = ''; 
	
	//private var for current page number
	private $currentPage	 = '1'; 
	
	//private var for first page number
	private $firstPage		= '1'; 
	
	//private var for previous page number
	private $previousPage 	= '0'; 
	
	//private var for next page number
	private $nextPage		= '0'; 
	
	//private var for last page number
	private $lastPage		= '0' ; 
	
	//private var for last page number
	private $totalPages		= '1' ; 
	
	//private var for prevous button disable
	private $previousDisable  = ''; 
	
	//private var for next button disable
	private $nextDisable  = 'disabled';
	
	//private array for last page number
	private $pagiConfig = array();
	
	//private var for last pagination view
	private $pagiView = '';

	/**
	 * initialize detail for pagination
	 *
	 * @return void
	 */
	public function initialize()
	{
		
		//get number of page from total records
		$this->totalRecords = count($this->totalRecords);
		if(!empty($this->totalRecords)){
			$this->totalRecords = ceil($this->totalRecords/$this->limit);
		}
		
		//set total page number
		$this->totalPages = $this->totalRecords;
	}
	
	
	public function pagination_link(){
		
		$this->currentPage 		=   ($this->postPage==0)?'1':$this->postPage;
		$this->lastPage	  	    =   $this->totalPages;
		
	
		//condition for previous page
		if(1 < $this->currentPage ){
			$this->previousPage = $this->currentPage - 1;
		}
		
		//condition for next page
		if(1 < $this->totalPages ){
			$this->nextPage = $this->currentPage + 1;
			$this->nextDisable    = '';	
		}
		
		//condition for next page
		if($this->nextPage > $this->totalPages){
			$this->nextPage  		= $this->totalPages;
			$this->nextDisable    = 'disabled';	
		}
		
		//condition for disable
		if($this->previousPage==0 && $this->firstPage==1){
			$this->previousDisable ='disabled';
		}
		
		//set in array
		$this->pagiConfig['currentPage']  		= $this->currentPage;
		$this->pagiConfig['firstPage']   		= $this->firstPage;
		$this->pagiConfig['previousPage']  		= $this->previousPage;
		$this->pagiConfig['nextPage']   		= $this->nextPage;
		$this->pagiConfig['lastPage']   		= $this->lastPage;
		$this->pagiConfig['previousDisable']    = $this->previousDisable;	
		$this->pagiConfig['nextDisDisable']     = $this->nextDisable;	
		
		//set in html content
		$this->pagiView .= '<div class="pagination">';
		$this->pagiView .= '<a href="javascript:void(0)" page="'.$this->firstPage.'" class="first pagination_action '.$this->previousDisable.'" data-action="first">&laquo;</a>';
		$this->pagiView .= '<a href="javascript:void(0)" page="'.$this->previousPage.'" class="previous pagination_action '.$this->previousDisable.'" data-action="previous">&lsaquo;</a>';
		$this->pagiView .= '<input type="text" disabled="true" class="white" value="Page '.$this->currentPage.' of '.$this->lastPage.'"/>';
		$this->pagiView .= '<a href="javascript:void(0)" page="'.$this->nextPage.'" class="next pagination_action '.$this->nextDisable.'"  data-action="next">&rsaquo;</a>';
		$this->pagiView .= '<a href="javascript:void(0)" page="'.$this->lastPage.'" class="last pagination_action '.$this->nextDisable.'"  data-action="last">&raquo;</a>';
		$this->pagiView .= '</div>';

		return $this->pagiView;
	}
	
	

	
}
