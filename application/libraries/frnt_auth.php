<?php if (!defined("BASEPATH")) exit("No direct script access allowed");

class Frnt_auth {
  
    private $ci;
    
    /**
     * Constructor, loads dependencies, initializes the library
     * and detects the autologin cookie
     */
    public function __construct($config = array()) {
        $this->ci = &get_instance();
        
        // load session library
        $this->ci->load->library('session');
        
 
    }
  
     /**
     * function to login 
     * 
     * @param array $config
     */
    function login($usertype)
		{
		
		//encreapt pass and define array for passing private below function 
		$whereInuser['password']=md5($this->ci->input->post('login_password'));
		$whereInuser['email']=$this->ci->input->post('login_email');
		$whereInuser['event_id']=$this->ci->input->post('event_id');
		$whereInuser['user_type_id']=$usertype;
		
		//check for validation for field for login popup 
		$this->ci->form_validation->set_rules('login_email', 'email', 'trim|required');
		$this->ci->form_validation->set_rules('login_password', 'password', 'trim|required');
		
		//if validation goes true
		if ($this->ci->form_validation->run())
		{	
			//get return for private funtion and go into if condition
			$validusers = $this->_checklogindetail($whereInuser);
		
			if(!empty($validusers))
			{
				$this->ci->session->set_userdata('userid',$validusers[0]->id);
				$this->ci->session->set_userdata('username',$validusers[0]->username);
				$this->ci->session->set_userdata('firstname',$validusers[0]->firstname);
				$this->ci->session->set_userdata('eventid',$validusers[0]->event_id);
				$this->ci->session->set_userdata('usertypeid',$validusers[0]->user_type_id);
			}
			
		}
		// if validatation goes wrong then user comes in below else condition
		else
		{
		echo json_encode(array('msg' => validation_errors(), 'status' => '0'));
		return True;
		}
		
	}
    
    function _checklogindetail($whereInuser)
		{
			//check filled credential is exist in db or not
			$userDetails=$this->ci->common_model->getDataFromTabel('user','*',$whereInuser);
				
			// if credential not exist in database
			if(empty($userDetails))
				{
					echo json_encode(array('msg' => 'Wrong email and pass Re-enter our detail.', 'status' => '0'));
					return FALSE;
				}
			// if credential exist in database but email not verified still
			else if(isset($userDetails[0]->email_verify) && $userDetails[0]->email_verify == 0 )
					{
						echo json_encode(array('msg' => 'Email varification is pending. Please check your email.', 'status' => '0'));
						return TRUE;
					}			
				else{
						$msgArray1 = array('msg'=>'Login successfully','is_success'=>'true');
						$msgArray = array('msg' => 'Login successfully.', 'status' => '1');
						echo json_encode($msgArray);
						set_global_messages($msgArray1);
						return $userDetails;
					}
		}
    
   
}
