<?php 
//error_reporting(0); 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');	
	class Default_template {		
		var $template_data = array();		
		function Default_template(){
		
		}

		function load($template = '', $view = '' , $view_data = array(), $return = FALSE){           
			$CI = get_instance();
		
			//define constant
			define("BASEURL",base_url());
			define("IMAGE",BASEURL.$CI->config->item('template_path')."images/");
			
			$data 				= 	$view_data;
			
			//side title
			$config['site_title']= $CI->config->item('site_title');			
			
			//add meta
			$CI->head->add_meta('viewport', 'width=device-width, initial-scale=1.0, maximum-scale=1');
			
		   //add css
			$CI->head->add_css($CI->config->item('bootstrap'),'screen');
			$CI->head->add_css($CI->config->item('bootstrap_responsive'),'screen');
			$CI->head->add_css($CI->config->item('docs'),'screen');
			$CI->head->add_css($CI->config->item('customstyle'));
			$CI->head->add_css($CI->config->item('customresponsive'));
			$CI->head->add_css($CI->config->item('datetimepicker_min_css'));
			$CI->head->add_css($CI->config->item('jquery_ui_numberstype_css'));
			$CI->head->add_css($CI->config->item('common_css'));
			$CI->head->add_css($CI->config->item('base_css'));
			$CI->head->add_css($CI->config->item('reset_css'));
			$CI->head->add_css($CI->config->item('demo1_css'));
			$CI->head->add_css($CI->config->item('style_css'));
			$CI->head->add_css($CI->config->item('bootstrap_select_css'));
			$CI->head->add_css($CI->config->item('table_responsive_css'));
			$CI->head->add_css($CI->config->item('table_responsive_css'));
			$CI->head->add_css($CI->config->item('developerstyle_css'));
			$CI->head->add_css($CI->config->item('perfect_scroll_css'));
			
			//to add country flag
			$CI->head->add_css($CI->config->item('country_flag_css'));
			//$CI->head->add_css($CI->config->item('chrome_css')); 
			
			
			//add js
			
			$CI->head->add_js($CI->config->item('jquery_1_10_1_min'));
			$CI->head->add_js($CI->config->item('bootstrap_modal'));
		//	$CI->head->add_js($CI->config->item('bootstrap_datetimepicker_min'));
			$CI->head->add_js($CI->config->item('bootstrap_collapse'));
			$CI->head->add_js($CI->config->item('bootstrap_transition'));
			$CI->head->add_js($CI->config->item('bootstrap_min'));
			$CI->head->add_js($CI->config->item('bootstrap_select'));
			
			$CI->head->add_js($CI->config->item('curtain_js'));
			$CI->head->add_js($CI->config->item('jquery_localscroll_1_2_7_min'));
			$CI->head->add_js($CI->config->item('jquery_parallax_1_1_3'));
			$CI->head->add_js($CI->config->item('jquery_screwdefaultbuttonsV2'));
			$CI->head->add_js($CI->config->item('jquery_scrollTo_1_4_2_min'));
			$CI->head->add_js($CI->config->item('kinetics_js'));
			//$CI->head->add_js($CI->config->item('parallax_js'));
		//	$CI->head->add_js($CI->config->item('bootstrap_timepicker_js'));
		//	$CI->head->add_js($CI->config->item('bootstrap_datetimepicker_js'));
			
			//js for custom for fields validation
			$CI->head->add_js($CI->config->item('customValidation')); 
			$CI->head->add_js($CI->config->item('1_10_3_jquery_ui'));
			$CI->head->add_js($CI->config->item('common_js'));
			$CI->head->add_js($CI->config->item('perfect_scroll_js'));
			
			//add ui js timepicker addons
			$CI->head->add_js($CI->config->item('timepicker_addon'));
			$CI->head->add_js($CI->config->item('sliderAccess_ui'));
			
			
			//bootbox js
			$CI->head->add_js($CI->config->item('bootbox_js'));
			
			//add inline js code
			$CI->head->add_inline_js("var baseUrl= '".base_url()."' ; ");
			
			
			//include languge js file
			$CI->head->add_js($CI->config->item('language_js'));
			
			//to add country flag include js
			$CI->head->add_js($CI->config->item('country_flag'));
			
			$data['head'] = $CI->head->render_head($config);			
		
			$data['content']=$CI->load->view($view, $data,true );
			
			$data['module']=$CI->router->fetch_class();
			$data['moduleMethod']=$CI->router->fetch_method();
			
			$CI->load->view($template, $data);					
		}
	}

?>
