<?php 
/*
 * auther: lokedra meena
 * email : lokendrameena@cdnsol.com
 * description: this class is used to manage booking number 
 * give old booking number then return new booking number
 * date: 29-apr-2014
 */ 
 
class Booking_number{
	
	
	/*
	 * @param: old booking number
	 * @description: This function will be new booking number 
	 * @retunr booking number
	 */ 
	
	public function bookingnumber($oldBookingNo){
	
		//change string lower case to upper case					
		$oldBookingNo 	 = strtoupper($oldBookingNo);
		//split string into array
		$getArray		 = str_split($oldBookingNo);
		$firstLatter	 = $getArray[0]; 
		$secondLatter 	 = $getArray[1]; 
		$serialNumber	 = $getArray[2].$getArray[3]; 
		
		//call serial number
		$nextNumber 	 = $this->serialNumber($serialNumber);
		//call second position latter
		$getSecondLatter = $this->secondPostionLatter($secondLatter,$nextNumber);
		//call first position latter
		$getFirstLatter  = $this->firstPositionLatter($firstLatter,$getSecondLatter,$nextNumber);
		//concanate all variable 	
		$bookingNumber   = $getFirstLatter.$getSecondLatter.$nextNumber;
	
		return $bookingNumber;
	}
	
	//-----------------------------------------------------------------------------------------
	
	/*
	 * @param: current serial number
	 * @description: check serial number for next number 
	 * @return next serial number
	 */ 
	
	private function serialNumber($serialNumber){
		
		if($serialNumber < 99){
			$nextSerialNumber = 	$serialNumber + 1;
		}else{
			$nextSerialNumber = '1';
		}
		
		$nextSerialNumber = $this->numberformate($nextSerialNumber);
		
		return $nextSerialNumber;
	}
	
	//-----------------------------------------------------------------------------------------
	
	/*
	 * description: second position latter check
	 * return: next latter
	 */ 
	
	private function secondPostionLatter($secondLatter,$nextSerialNumber){
		
		if($nextSerialNumber<2){
			$nextLatter = $this->nextLatter($secondLatter);
		}else{
			$nextLatter = $secondLatter;
		}	
		
		return $nextLatter;
	}
	
	//-----------------------------------------------------------------------------------------
	
	/*
	 * description: first position latter check
	 * return: next latter
	 */ 
	
	private function firstPositionLatter($firstLatter,$getSecondLatter,$nextSerialNumber){
		
		$lattersArray = range('A','Z');
		$latterPosition = array_search($getSecondLatter, $lattersArray); 
		
		if($latterPosition == '0' && $nextSerialNumber == '01'){
			$nextLatter= $this->nextLatter($firstLatter);
		}else{
			$nextLatter = $firstLatter;
		} 
		
		return $nextLatter;
	}
	
	
	/*
	 * description: check latter in alphabet array 
	 * return: next latter
	 */
	
	//-----------------------------------------------------------------------------------------
	
	private function nextLatter($latter){
		$lattersArray = range('A','Z');
		$latterPosition = array_search($latter, $lattersArray); 
		if($latterPosition < 25){
			$nextLatter = $latterPosition+1;
		}else{
			$nextLatter = 0;
		}
		return $lattersArray[$nextLatter];
	}
	
	//-----------------------------------------------------------------------------------------
	
	/*
	 * description: set number formate in proper order 
	 * return: integer
	 */  
	
	private function numberformate($number){
			
		//get digit lenght
		$digitlen = strlen($number);
		
		//concanate zero if lenght is less then two
		if($digitlen < 2){
			$number =   '0'.$number;
		}
		return $number;
	}
	
} 
?>
