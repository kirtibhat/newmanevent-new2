<?php 

/*
 *@description: This template class for backend section of pco
 *@auther: lokendra 
 */ 

//error_reporting(0); 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');  
  class Template {    
    var $template_data = array();   
    
    
    function Template(){
    
    }

    function load($template = '', $view = '' , $view_data = array(), $return = FALSE){           
      $CI = get_instance();

      //define constant
      define("BASEURL", base_url());
      define("IMAGE", BASEURL . $CI->config->item('themes_path') . "images/");
      define("SYSTEMIMAGE", BASEURL . $CI->config->item('template_path') . "images/");
     
      $data = $view_data;

      //website title side title
      $config['site_title'] = $CI->config->item('site_title');

      //add meta for responsive device
      $CI->head->add_meta('viewport', 'width=420, maximum-scale=1, user-scalable=yes');

      //add require css
      $CI->head->add_css($CI->config->item('css_path') . 'bootstrap.css', 'screen');
      //$CI->head->add_css($CI->config->item('css_path') . 'bootstrap-datetimepicker.min.css', 'screen'); //For new calender
      $CI->head->add_css($CI->config->item('css_path') . 'style.css', 'screen');        
      $CI->head->add_css($CI->config->item('system_css_path') . 'customstyle.css', 'screen');  
      $CI->head->add_css($CI->config->item('system_css_path') . 'common.css', 'screen');
      $CI->head->add_css($CI->config->item('system_css_path') . 'jquery-ui-calendar.css', 'screen'); //For old calender

      //add css file for parsley js validation
      $CI->head->add_css($CI->config->item('css_path') . 'parsley.css', 'screen');


		$CI->head->add_js($CI->config->item('system_js_path').'jquery-2.1.3.js');
		$CI->head->add_js($CI->config->item('system_js_path').'common.js'); 
		$CI->head->add_js($CI->config->item('system_js_path').'event_custom.js');
		$CI->head->add_js($CI->config->item('js_path').'js.js');
				 
		//add website url in js varibale
		$CI->head->add_inline_js("var baseUrl= '" . base_url() . "' ; ");

		$data['js_path'] = base_url() .$CI->config->item('js_path');
		$data['system_js_path'] = base_url() .$CI->config->item('system_js_path');
		$data['head'] = $CI->head->render_head($config);
		$data['content'] = $CI->load->view($view, $data, true);

		$data['module'] = $CI->router->fetch_class();
		$data['moduleMethod'] = $CI->router->fetch_method();
		
		//echo '<pre>';		print_r($view_data);
		
		$data['accountType'] = $CI->config->item('accountType');
		$data['fileSuffix'] = $CI->config->item('fileSuffix');
			
		$CI->load->view($template, $data);
            
    }
  }

?>
