<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$CI =& get_instance();
$lang=$CI->uri->segment(1,'en');




// --------------------------------------------------------------------------
// Head Library Configs
// --------------------------------------------------------------------------

/*
|--------------------------------------------------------------------------
| Default configs
|--------------------------------------------------------------------------
|
| These configs can override class variables for the head class. You can find
| These variables listed and commented in the head class file.
|
*/
/*---------site title-----------*/ 
$config['site_title'] = 'Newman Events';

/*---------main template path--------*/
$mainThemePath 	= 'themes/';

/*---------sub assets path--------*/
$templatePath 	= $mainThemePath.'system/';

/*------define main tempalte path for config------*/
$config['template_path'] = $templatePath;

$config['main_theme_path'] = $mainThemePath;

/*------common css list---------*/
$config['bootstrap']			= $templatePath.'css/bootstrap.css';
$config['bootstrap_responsive'] = $templatePath.'css/bootstrap-responsive.css';
$config['docs'] 				= $templatePath.'css/docs.css';
$config['customstyle'] 			= $templatePath.'css/customstyle.css';
$config['customresponsive'] 	= $templatePath.'css/customresponsive.css';
$config['datetimepicker_min_css'] = $templatePath.'css/datepicker.css';
$config['jquery_ui_numberstype_css'] = $templatePath.'css/jquery-ui_numbertype.css';
$config['jquery_ui_calendar_css'] = $templatePath.'css/newmanevent_managers/jquery-ui-calendar.css';

$config['common_css'] 				= $templatePath.'css/common.css';
$config['bootstrap_timepicker_css'] = $templatePath.'css/bootstrap-timepicker.min.css';
$config['base_css'] 				= $templatePath.'css/base.css';
$config['reset_css']				= $templatePath.'css/reset.css';
$config['demo1_css'] 				= $templatePath.'css/demo1.css';
$config['style_css'] 				= $templatePath.'css/style.css';
$config['bootstrap_select_css'] 	= $templatePath.'css/bootstrap-select.css';
$config['customfields_css'] 		= $templatePath.'css/customfields.css';
$config['developerstyle_css'] 		= $templatePath.'css/developerstyle.css';

/*-------jquery ui common css----*/
$config['jquery_ui_css'] 			= $templatePath.'css/jquery-ui.css';

/*-------dashboard css--------------*/
$config['table_responsive_css'] = $templatePath.'css/table-responsive.css';

/*---to add country flag----*/
$config['country_flag_css']  = $templatePath.'js/country_code_flag/css/intlTelInput.css';


/*-------js list common js----------*/
$config['bootstrap_min'] 				   = $templatePath.'js/bootstrap.min.js';


$config['bootstrap_datetimepicker_js'] 	 = $templatePath.'js/bootstrap-datetimepicker.min.js';
$config['common_js'] 					   = $templatePath.'js/common.js';
$config['bootstrap_modal'] 				   = $templatePath.'js/bootstrap-modal.js';
$config['bootstrap_datetimepicker_min'] = $templatePath.'js/bootstrap-datepicker.js';
$config['bootstrap_timepicker_js'] 		   = $templatePath.'js/bootstrap-timepicker.min.js';

/*-------jquery library with version -------*/
$config['jquery_1_10_1_min']			   = $templatePath.'js/jquery-1.10.1.min.js';
$config['jquery_1_8_1_min']				   = $templatePath.'js/jquery-1.8.1.min.js';

/*--------this config for pl upload--------*/
$config['plupload'] 			= $templatePath.'js/upload_js/plupload.js';
$config['plupload_gears'] 		= $templatePath.'js/upload_js/plupload.gears.js';
$config['plupload_silverlight'] = $templatePath.'js/upload_js/plupload.silverlight.js';
$config['plupload_flash'] 		= $templatePath.'js/upload_js/plupload.flash.js';
$config['plupload_browserplus'] = $templatePath.'js/upload_js/plupload.browserplus.js';
$config['plupload_html4'] 		= $templatePath.'js/upload_js/plupload.html4.js';
$config['plupload_html5'] 		= $templatePath.'js/upload_js/plupload.html5.js';

/*-------all common jst--------*/ 
$config['bootstrap_collapse'] 		   = $templatePath.'js/bootstrap-collapse.js';
$config['bootstrap_select'] 			= $templatePath.'js/bootstrap-select.js';
$config['bootstrap_transition']			= $templatePath.'js/bootstrap-transition.js';
$config['curtain_js'] 					= $templatePath.'js/curtain.js';
$config['jquery_localscroll_1_2_7_min'] = $templatePath.'js/jquery.localscroll-1.2.7-min.js';
$config['jquery_parallax_1_1_3'] 		= $templatePath.'js/jquery.parallax-1.1.3.js';
$config['jquery_screwdefaultbuttonsV2'] = $templatePath.'js/jquery.screwdefaultbuttonsV2.js';
$config['jquery_scrollTo_1_4_2_min']	= $templatePath.'js/jquery.scrollTo-1.4.2-min.js';
$config['kinetics_js'] 					= $templatePath.'js/kinetics.js';
$config['parallax_js'] 					= $templatePath.'js/parallax.js';

/*-------ui library with version---------*/
$config['1_10_3_jquery_ui'] 			= $templatePath.'js/1_10_3_jquery_ui.js';

/*-------custome validation------------*/
$config['customValidation'] = $templatePath.'js/customValidation.js';

/*------js for language----------*/
$config['language_js'] = $templatePath.'js/js_language/en.js';							


/*--------bootstrap-tooltip--------------*/
$config['bootstrap_tooltip'] = $templatePath.'js/bootstrap-tooltip.js';

/*-------country flag & code js and css-------------*/
$config['intlTelInput_css'] = $templatePath.'js/country_code_flag/css/intlTelInput.css';
$config['intlTelInput_js']  = $templatePath.'js/country_code_flag/js/intlTelInput.js';

/*------scroll bar in popup modal----------*/
$config['perfect_scroll_css'] = $templatePath.'css/perfect-scrollbar.css';
$config['mousewheel_js']  = $templatePath.'js/jquery.mousewheel.js';
$config['perfect_scroll_js'] = $templatePath.'js/perfect-scrollbar.js';


/*-------add time picker slider in calendar------------*/
$config['timepicker_addon'] = $templatePath.'js/jquery-ui-timepicker-addon.js';
$config['sliderAccess_ui']  = $templatePath.'js/jquery-ui-sliderAccess.js';

/*-------add time picker slider in calendar------------*/
$config['bootbox_js']  = $templatePath.'js/bootbox.js';

/*------pagination js and css----------*/
$config['jqpagination_css'] = $templatePath.'css/jqpagination.css';
$config['jqpagination_js']  = $templatePath.'js/jquery.jqpagination.js';

/*---to add country flag----*/
$config['country_flag']  = $templatePath.'js/country_code_flag/js/intlTelInput.js';

//-----------------------------------------------------------------------------------------

/*
 * new design change for newman config define start from here
 * 
 */ 


/*---------main template path--------*/
$templatePath 	= $mainThemePath.'assets/';

/*---------define tempalte path----------------*/
$config['themes_path'] = $templatePath;

/*------ css path ---------*/
$config['css_path']		=	$templatePath.'css/';

/*------- js path ---------*/
$config['js_path'] 		= 	$templatePath.'js/';

/*------- js path ---------*/
$config['system_js_path'] 	= 	$mainThemePath.'system/js/';
$config['system_css_path'] 	= 	$mainThemePath.'system/css/';

/* End of file head.php */
/* Location: ./application/config/head.php */

