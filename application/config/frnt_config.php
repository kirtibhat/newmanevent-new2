<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Authentication configuration
|--------------------------------------------------------------------------
| The basic settings for the auth library.
|
| 'admin_user_type'			= main event creater user type for admin/backend
| 'registrant_user_type'	= first or main register user for event in frontend
| 'sponsor_user_type'		= register user for sponsor section in frontend
| 'exhibitor_user_type'		= register user for exhibitor section in frontend
| 'side_event_user_type'	= register user for side event section in frontend
*/

$config['admin_user_type']			= '1';
$config['registrant_user_type']		= '2';
$config['sponsor_user_type']		= '3';
$config['exhibitor_user_type']		= '4';
$config['side_event_user_type']		= '5';
