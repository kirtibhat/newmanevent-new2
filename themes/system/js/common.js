  // Define Global Message
  
  var ajaxErrorMsg = 'Request not mached! Please fill correct data.';
  
  //set default value form open
  var isChanged = false;
  
    //---------------------------------------------------------------------------------------
  
  //load calender data picker 
  $(function() {
    
    //load datetimepicker 
    /*$( ".datetimepicker" ).datetimepicker({
      showOn: "both",
      buttonImage: baseUrl+"/templates/system/images/calender_icon.png",
      dateFormat: 'dd M yy',
      buttonText: 'Calendar',
     // addSliderAccess: true,
     // sliderAccessArgs: { touchonly: false },
      controlType: 'select',
     // timeFormat: 'hh:mm tt',
      timeFormat: 'HH:mm',
      buttonImageOnly: true
    });
    */
    
    
    //load datepicker 
    /*$( ".datepicker" ).datepicker({
      showOn: "both",
      buttonImage: baseUrl+"/templates/system/images/calender_icon.png",
      dateFormat: 'dd M yy',
      buttonText: 'Calendar',
      buttonImageOnly: true
    });
    */
    
    
    //load timepicker 
    //timepickercall();
    
    // spinner selectbox 
    //$( ".spinner_selectbox" ).spinner();
    
    //show_tool_tip
    //$('.show_tool_tip').tooltip();
    
    
    //scroll button
    /*$('.modelbodyscroll').perfectScrollbar({
      wheelSpeed: 20,
      wheelPropagation: false
    });
    */
    
    //reset form while click on reset button
    $(".reset_form").click(function(){
      var clickedFormName = $(this).closest('form').attr('id');
      
      //call reseted form
      resetSelectedFrom(clickedFormName);
    })  
    
  });
  
  //---------------------------------------------------------------------------------------
  
  /*
   * @desctiop: This function is used to reset form 
   * @param: formId 
   */ 
  
  function resetSelectedFrom(clickedFormName){
    
    //data blank code
    $(':input','#'+clickedFormName)
    .not(':button, :submit, :reset, :hidden')
    .val('').removeAttr('checked')
    .removeAttr('selected');
    
    //unchecked checkbox
    $('input:checkbox','#'+clickedFormName).prop( "checked", false ); 
  }
  
  

  //---------------------------------------------------------------------------------------
  
  // define time into function for directly call
  /*function timepickercall()
  {
    //load timepicker 
    $('.timepicker').timepicker({
      minuteStep: 1,  
      //addSliderAccess: false,
      controlType: 'select',
      timeFormat: 'HH:mm',
      //sliderAccessArgs: { touchonly: false },
    });
  } */

  //---------------------------------------------------------------------------------------
  
  /*
   * @description: This function is used to cancle popop and reset it form 
   * @param1 : eventClass
   * @param1 : formId
   */
    
  function popup_cancle_form(eventClass,formId){
    $('.'+eventClass).click(function(){
      $('#'+formId)[0].reset();
    });
  } 

  //---------------------------------------------------------------------------------------

  /*
   * @description: This function is use to custom popup 
   * @param: msg
   * @param: is_success (true,false)
   * 
   */ 

  function custom_popup(msg,is_success,add_error_class){
    
    if(is_success   == undefined) { is_success  = true;  } // success message
    if(add_error_class  == undefined) { add_error_class  = true;  } // error class message
    
    if(typeof(is_success)=="string")
      is_success = (is_success == "true") ? true : false;
    
    
    if(is_success){
      
      // remove class if exit class are there
      $("#thanks_popup").find('.modal-header').removeClass('bg_b5101E');
      $("#thanks_popup").find('.popup_cancel').removeClass('error_close_button');
      
      //remove old class form ok button
      var clickbox = $("#ajax_open_popup_html").attr('clickbox');
      
      if(clickbox!=undefined){
        $(".cust_popup_button").removeClass(clickbox);
      }
      //set title 
      $('.popu_title').html('Success');
      
      //remove error class
      //$('#custom_ok_div').hide();
      $("#thanks_popup").find('.modal-header').addClass('managers_color');
      
    } 
    else{
             
      $('.popu_title').html('Error');
      
      //add error class
      //$('#custom_ok_div').show();
      $("#thanks_popup").find('.modal-header').addClass('danger_color');
      
      if(add_error_class==true){
        $("#thanks_popup").find('.popup_cancel').addClass('error_close_button');
      } 
      
      
    } 
     
    $('.show_custome_msg').html(msg);
   
    
    open_model_popup('thanks_popup');

    
    
    //hide custome popup after 3 second
    if(is_success){
      setTimeout(function() {
        hide_popup('thanks_popup');
      }, 2000);
    }    
  }
  
  
  
  
  //---------------------------------------------------------------------------------------

  /*
   * @description: This function is use to open model all places by this common code 
   * @param1: modelId
   * return true;
   */ 

  function open_model_popup(modelId){
    $('#'+modelId).modal({ backdrop: 'static', keyboard: true });
    return true;
  } 

  //---------------------------------------------------------------------------------------

  /*
   * @description: This function is use to open popup 
   * @param1: clickClass
   * @param2: modelId
   * return void
   */ 

  function popupopen(clickClass,popupDivId,formId){
    if(formId   == undefined) { formId  = false;  }
    $(document).on('click','.'+clickClass,function(){
        if(formId){
            $('#'+formId).closest("form").parsley().reset();
        }        
        //open login popup
        open_model_popup(popupDivId);
        //set class and id in attribute
        $("#ajax_open_popup_html").attr('openbox',popupDivId);  
        $("#ajax_open_popup_html").attr('clickbox',clickClass); 
    });
  } 

  //---------------------------------------------------------------------------------------

  /*
   * @description: This function is use to hide any popoup
   * @param: id
   * 
   */ 

  function hide_popup(popup_id){
    $('#'+popup_id).modal('hide');
  }

  
  //---------------------------------------------------------------------------------------
  
  /*
   * @description: This function  is use to refreah 
   * @auther: lokendra
   * 
   */ 
   
  function refreshPge()
  { 
    window.location.href=window.location.href;
  }
  
  
    
  //---------------------------------------------------------------------------------------
  
  /*
   * @description: This function  is use to refreah 
   * @auther: lokendra
   * 
   */ 
   
  function refreshPgeDelay(dataId)
  { 
    setTimeout(function () {
      var url = window.location.href;
      var pageUrl = window.location.href;
      checkIfHash = pageUrl.indexOf('#');
      if(checkIfHash>0){
        var newUrl = pageUrl.split("#");
        url = newUrl[0];
      }
      if(dataId > 0) {
        window.location = baseUrl+'event/invitations/?dAtaiD='+dataId;
      }else {
        window.location.href = url;
      }
    }, 2000); 
    
  }


  
  //---------------------------------------------------------------------------------------
  
  /*
   *--------------------------------------------------------------------------------------- 
   * This function is used to save and Show Success Message, Error Message, Page Refresh 
   * File Upload Trigger Manage, Hide Current Save Form, Open Next form
   *---------------------------------------------------------------------------------------
   * @parm1 = formid  (string)
   * @param2 = posturl  (string)
   * @param3 = isMsgshow (boolean)
   * @param4 = isErrorshow (boolean)
   * @param5 = isrefresh (boolean)
   * @param6 = isredirect (boolean)
   * @param7 = isUpload (string)
   * @param8 = isCurrentForm (string)
   * @param9 = isNextForm (string)
   * @param10 = submitLoader (boolean) 
   */ 

  function ajaxdatasave(formId,postUrl,isMsgshow,isErrorshow,isrefresh,isredirect,isUpload,isCurrentForm,isNextForm,isSubmitLoader,isPopup,popupModelId,reOpenPopUp,reOpenPopUpId,custom_value_next_save){
 
    //these vaalue set if not passing these value
    if(isMsgshow        == undefined) {  isMsgshow      = true;  } // success message
    if(isErrorshow      == undefined) {  isErrorshow    = true;  } // all error meesage
    if(isrefresh        == undefined) {  isrefresh      = false; } // page refresh after save
    if(isredirect       == undefined) {  isredirect     = false; } // redirect after save
    if(isUpload         == undefined) {  isUpload       = false; } // upload with data save
    if(isCurrentForm    == undefined) {  isCurrentForm  = false; } // current form 
    if(isNextForm       == undefined) {  isNextForm     = false; } // next form
    if(isSubmitLoader   == undefined) {  isSubmitLoader = true;  } // submit button loader
    if(isPopup          == undefined) {  isPopup        = false; } // Check Popup open or not
    if(popupModelId     == undefined) {  popupModelId   = false; } // Model Popup Id
    if(reOpenPopUp      == undefined) {  reOpenPopUp    = false; } // Model Popup Id
    if(reOpenPopUpId    == undefined) {  reOpenPopUpId  = false; } // Model Popup Id
    
    if(custom_value_next_save != undefined && custom_value_next_save != '' ){
      //console.log(custom_value_next_save); 

		$("#"+formId).each(function() {
    	var fromData = new FormData(this);
      //console.log(fromData); 
			var url = baseUrl+postUrl;
			$.ajax({
				type:'POST',
				data:fromData,
				contentType: false,
				processData:false,
				url: url,
				dataType: 'json',
				cache: false,
				success: function(data){  
					//check data 
					//console.log(data);
					if(data){
						if(data.is_success == true){
							
						}
					}
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) {}
			});
		});
	}
    
    
    $(document).on('submit',"#"+formId,function( event ) {
        
      if($("#"+formId).find("input[type='submit']").hasClass('button_disabled')){
        return false;
      }
      
      var fromData=$(this).serialize();
      
      //var fromData=$(this).serialize();
      
      if(formId=='userProfile'){
        var fromData = new FormData(this);
      }
      
      var url = baseUrl+postUrl;
        $.ajax({
            type:'POST',
            data:fromData,
            url: url,
            dataType: 'json',
            async: true,
            cache: false,
            beforeSend: function( ) {
            //open loader
            if(isSubmitLoader==true){
                
              //submit button loader
              loadersaving(formId);
            }else{
              //overlay loader
              loaderopen(formId);
              
              $("#"+popupModelId).modal('hide');
            } 
          },
          success: function(data){  
            
            if(formId== 'userBasicInformation') {
				$('.user_designation').html(data.company_name);
			}
			if(formId=='userProfile'){
				if(data.userLogo){
					if(data.userLogo=='delete'){
						$('.user_img img').attr('src',baseUrl+'themes/system/images/user.jpg');
					}else{
						$('.user_img img').attr('src',newmanUrl+'media/user_logo/'+data.userLogo);
						$('.user_profile_name').html(data.nick_name_user);
					}
				}						
				if(data.nick_name_user) {
						$('.user_profile_name').html(data.nick_name_user);
				}	
			}
						
            // check model popup
            if(isPopup==true){
              $('#'+formId).find("input[type='submit']").removeClass('button_disabled');
              $("#"+popupModelId).modal('hide'); // hide the model
            }else{
              $('#'+formId).find("input[type='submit']").removeClass('button_disabled');
              $("#thanks_popup").find('.popup_cancel').removeClass('error_close_button');
            }


            
            //check data 
            if(data){
                
            
              if(data.is_success=='true'){
                    
                  if(isSubmitLoader==true){
                    
                    if(formId=='formEmailInvitationSetup'){
                        show_custom_popup('Email invitations sent successfully',true);
                        setTimeout(function(){
                            $('.show_invitation_success').trigger('click');
                        },2000);
                        
                    }else{
                        show_custom_popup('Saved successfully',true);
                    }
                      
                    //submit button loader
                    loadersaved(formId);
                  }else{
                    //overlay loader
                    //  loaderclose()
                  } 
                
                  //is upload
                  if(isUpload==true){
                    //trigger fire for media upload 
                    var tableInsertedId = data.tableInsertedId;
                    $("#tableInsertedId").val(tableInsertedId);
                    $('#div_ar2RA_a').trigger('click'); 
                  }
                  
                  if(formId=='formInvitationDetailsSetup'){
                      var databookerid = data.bookerId;
                      $('#confirmbookerId').val(databookerid);
                  }
                  
     
                  
                //then condition for hide current saved form and open next form
                if(isCurrentForm!=false || isNextForm!=false){
                
                  if(isCurrentForm!=false){
                      
                      
                    // form open arrow closed
                    //hidearea(isCurrentForm); // hide current form open
                    //$(isCurrentForm).parent().parent().removeClass('in');
                    //$(isNextForm).parent().parent().addClass('in');
                    //$(isNextForm).parent().parent().css('height','auto');
                    
                    $(isCurrentForm).parent().removeClass('open');
                    $(isCurrentForm).css('display','none');
                    $(isNextForm).parent().addClass('open');
                    $(isNextForm).css('display','block');
                    
                    if (typeof displayMap == 'function') { 
                      displayMap(); 
                    }
                    
                   
                    //$(isCurrentForm).parent().parent().find('.showhideaction').removeClass('typeofregistH').addClass('typeofregistV');
                    //$(isCurrentForm).parent().parent().find('.headingbg').css('border-bottom-left-radius','22px');//add bottom radius
                  }
                  
                  /*if(isCurrentForm!=false){
                    // form closed arrow closed
                    showarea(isNextForm); // show next form 
                    $(isNextForm).parent().parent().find('.showhideaction').removeClass('typeofregistV').addClass('typeofregistH');
                    $(isNextForm).parent().parent().find('.headingbg').css('border-bottom-left-radius','0px');//remove bottom radius
                  } */
                }
                
                
                
                //if want to refresh
                if(isrefresh==true){
                
                  //show text in loader
                  showlaoderwithmessag(data.msg);
                  if(formId == 'formInvitationType'){
                    refreshPgeDelay(data.invitationtypeid); 
                  }else {
                    //after show text and page will refresh 2 second delay
                        refreshPgeDelay(0);
                    }
                }
                
                //if want to redirect to spefice loaction
                
                if(isredirect==true){ 
                  //show text in loader
                  showlaoderwithmessag(data.msg);
                  
                  var rediectUrl = data.url;
                  
                  setTimeout(function () {
                    if(rediectUrl=='event/invitations'){
                        window.location = baseUrl+rediectUrl+'/?q='+data.invitationtypedetailsid;
                    }else{
                        window.location = baseUrl+rediectUrl;
                    }  
                    
                  }, 2000); 
                  
                }
                
                //set is changed saved
                isChanged = false;
                
              }else{
                //if want to show success msg
                
                //hide loader
                if(isSubmitLoader==true){
                  //submit button loader
                  loadererror(formId);
                }else{
                  //overlay loader
                  loaderclose()
                } 
                  
                if(isErrorshow==true){                  
                  //if want to show success msg
                  var showMsg = '';
                  if(typeof(data.msg)=='string'){
                    showMsg = data.msg;
                  }else{              
                    $.each(data.msg, function(index, val) {
                      showMsg+= val+'<br>';
                    });
                  }
                  //open error message
                  setTimeout(function(event){
                    custom_popup(showMsg,false);
                  },500);
                  

                if(reOpenPopUp==true){
                   $('.popup_cancel').click(function(){
                        open_model_popup(reOpenPopUpId);
                   }); 
                } 
                  
                  //add class in custome popup button
                  var clickbox = $("#ajax_open_popup_html").attr('clickbox');
                  $(".cust_popup_button").addClass(clickbox);

                    

                    
                  
                  
                  
                }
              }
            }
          },
          error: function(XMLHttpRequest, textStatus, errorThrown) {
            
            // check model popup
            if(isPopup==true){
              $("#"+popupModelId).modal('hide'); // hide the model
            }
            
            //hide loader
            //loaderclose();
            if(isSubmitLoader==true){
              //submit button loader
              loadererror(formId);
            }else{
              //overlay loader
              loaderclose()
            } 

            
            //open error message
            custom_popup(ajaxErrorMsg,false);
            
            //add class in custome popup button
            var clickbox = $("#ajax_open_popup_html").attr('clickbox');
            $(".cust_popup_button").addClass(clickbox);
          }
        });
        
      return false;  
    });
    
  }


   //---------------------------------------------------------------------------------------
   /*
   *--------------------------------------------------------------------------------------- 
   * This function is used to save data without form submit and Show Success Message, Error Message, Page Refresh 
   * File Upload Trigger Manage, Hide Current Save Form, Open Next form
   *---------------------------------------------------------------------------------------
   * @parm1     = formid  (string)
   * @param2    = posturl  (string)
   * @param3    = isMsgshow (boolean)
   * @param4    = isErrorshow (boolean)
   * @param5    = isrefresh (boolean)
   * @param6    = isredirect (boolean)
   * @param7    = isUpload (string)
   * @param8    = isCurrentForm (string)
   * @param9    = isNextForm (string)
   * @param10   = submitLoader (boolean) 
   * @param11   = popup (boolean) 
   * @param10   = popupModelId (string) 
   */ 
    function ajaxSaveWithoutSubmit(formId,postUrl,isMsgshow,isErrorshow,isrefresh,isredirect,isUpload,isCurrentForm,isNextForm,isSubmitLoader,isPopup,popupModelId){
        //these vaalue set if not passing these value
        if(isMsgshow      == undefined) {  isMsgshow      = true;   } // success message
        if(isErrorshow    == undefined) {  isErrorshow    = true;   } // all error meesage
        if(isrefresh      == undefined) {  isrefresh      = false;  } // page refresh after save
        if(isredirect     == undefined) {  isredirect     = false;  } // redirect after save
        if(isUpload       == undefined) {  isUpload       = false;  } // upload with data save
        if(isCurrentForm  == undefined) {  isCurrentForm  = false;  } // current form 
        if(isNextForm     == undefined) {  isNextForm     = false;  } // next form
        if(isSubmitLoader == undefined) {  isSubmitLoader = true;   } // submit button loader
        if(isPopup        == undefined) {  isPopup        = false;  } // Check Popup open or not
        if(popupModelId   == undefined) {  popupModelId   = false;  } // Model Popup Id
        
        var fromData=$("#"+formId).serialize();
        var url = baseUrl+postUrl;
        //return false;
        $.ajax({
            type:'POST',
            data:fromData,
            url: url,
            dataType: 'json',
            async: true,
            cache: false,
            beforeSend: function( ) {                
            },
            success: function(data){
                // check model popup
                if(isPopup==true){
                  $('#'+formId).find("input[type='submit']").removeClass('button_disabled');
                  $("#"+popupModelId).modal('hide'); // hide the model
                }else{
                  $('#'+formId).find("input[type='submit']").removeClass('button_disabled');
                  $("#thanks_popup").find('.popup_cancel').removeClass('error_close_button');
                }
                
                //is upload
                if(isUpload==true){
                    //trigger fire for media upload 
                    var tableInsertedId = data.tableInsertedId;
                    $("#tableInsertedId").val(tableInsertedId);
                    $('#div_ar2RA_a').trigger('click'); 
                }
            }
        });
    }
  
  //---------------------------------------------------------------------------------------
  
  /*
   * @description: This function is used to open form popup for add and value
   * @parm1 = buttonClass
   * @param2 = popupDivId
   * @param3 = formId
   */ 
  
  function openFormPopup(buttonClass,popupDivId,formId){
    $("."+buttonClass).click(function(){
      if(formId!=undefined){
        $("#"+formId).trigger('reset');
      }
      open_model_popup(popupDivId)
    });
  } 
  
  //---------------------------------------------------------------------------------------
  
  /*
   * @description: This function is used to common hide function
   * @areaValue = it will be id and class what you want hide 
   */ 
  
  function hidearea(areaValue){
    $(areaValue).slideUp('slow');
  }
  
  //---------------------------------------------------------------------------------------
  
  /*
   * @description: This function is used to common show loader message
   * @param1 = loader message
   * @param2 = it will be id and class what you want show 
   */
  
  
  function showlaoderwithmessag(msgValue){
    $('.load_succ_txt').slideDown('slow').html(msgValue);
  }
  
  //---------------------------------------------------------------------------------------
  
  /*
   * @description: This function is used to common show function
   * @areaValue = it will be id and class what you want show 
   */
   
  function showarea(areaValue){
    $(areaValue).slideDown('slow');
  }
  
  //---------------------------------------------------------------------------------------
  
  /*
   * @description: This function is used hide error message class exist 
   * return void
   */
   
  function hide_error_message(){
    $(".common_errormsg").addClass('dn')
    setTimeout('hide_error_message()',7000);
  }
  
  //---------------------------------------------------------------------------------------
  
  //hide error message every 5 second
  hide_error_message();
  
  //---------------------------------------------------------------------------------------
  
  /*
   * @name: ajaxpopupopen
   * @description: this function is used to open model popup by ajax requrest
   * param1: popupDivId  // open popup div id
   * param2: postUrl  // form post url
   * param3: sendData  // form send data
   */ 
  
  function ajaxpopupopen(popupDivId,postUrl,sendData,isClick,stepperId){
    
    //check variable exist
    if(isClick==undefined)  { isClick = 'no_action';  }
    if(stepperId==undefined)  { stepperId = "";  }
    
    var url = baseUrl+postUrl;
    $.post(url, sendData, function(data){ 
      
      $("#ajax_open_popup_html").html(data);  
      $("#ajax_open_popup_html").attr('openbox',popupDivId);  
      $("#ajax_open_popup_html").attr('clickbox',isClick);  
      
      open_model_popup(popupDivId); 
      
      
      // initialize tooltips : call a function
      ini_tooltips();
      
      // initialize stepper
      if(stepperId!=""){
        $("#"+stepperId).stepper();
      } 
      
      //spinnner call
      /*  $( ".spinner_selectbox" ).spinner();
        
      //scroll bar in modal 
      $('.modelbodyscroll').perfectScrollbar({
        wheelSpeed: 20,
        wheelPropagation: false
      });
      */
    }); 
  }
  
  //---------------------------------------------------------------------------------------
  
  /*
   * @name: formToggle
   * @description: This function is used to open and close  form by toggle effect
   * param1: options  (object)
   */ 
  
  (function($){
    $.fn.formToggle = function(options){

        var settings = $.extend({}, {clickClass: 'showhideaction',conconateClass: 'showhideformdiv'}, options);
      
        var formDivId     = settings.conconateClass;
        var clickClass    = settings.clickClass;
        
        $('.'+clickClass).click(function(){
          var getRegisId = $(this).attr('id');
          var getDivStatus = $("#"+formDivId+getRegisId).is(':visible');
          if(getDivStatus==true){
            //form header arrow show
            $(this).removeClass('typeofregistH').addClass('typeofregistV');
            //form div hide
            hidearea("#"+formDivId+getRegisId);
            // form header left border redius
            $(this).parent().css('border-bottom-left-radius','22px');
          }else{
            //form header arrow show
            $(this).removeClass('typeofregistV').addClass('typeofregistH');
            //form div show
            showarea("#"+formDivId+getRegisId);
            // form header left border redius
            $(this).parent().css('border-bottom-left-radius','0px');
          }
        }); 
        
        
        /*
         * This code is used to bottom left redius border remove if form is open
         */
        $("."+clickClass).each(function(){
          var idName = $(this).attr('id');
          if($('#showhideformdiv'+idName).is(':visible')){
            //add border left redius
            $(this).parent().css('border-bottom-left-radius','0px');
          }
        });
        
        /*
         * Mouseenter change form header arrow position 
         */
          
        $("."+clickClass).mouseenter(function(){
          $(this).removeClass('typeofregistV').addClass('typeofregistH');
        });
        
        /*
         * Mouseout change form header arrow position 
         */
        $("."+clickClass).mouseout(function(){
          var idName = $(this).attr('id');
          if($('#showhideformdiv'+idName).is(':visible')==false){
            $(this).removeClass('typeofregistH').addClass('typeofregistV');
          }
        });
        
        
      };
    })(jQuery);
  
  //---------------------------------------------------------------------------------------
  
  /*
   * @access: public 
   * @description: this function is used to append loader before submit button
   * @return: void
   */ 
  
  
  function loadersaving(formId){
    //$('#'+formId).find("input[type='submit']").after('<img src="'+baseUrl+'themes/system/images/form_loader.gif" class="submit_loading" width="45" height="45">');
    //$('#'+formId).find("input[type='submit']").addClass('button_disabled').removeClass('bg_F89728');
  }
  
  
  //---------------------------------------------------------------------------------------
  
  /*
   * @access: public 
   * @description: this function is used to remove loader before submit button
   * @return: void
   */ 
  
  
  function loadersaved(formId){
    $('#'+formId).find(".submit_loading").remove();
    $('#'+formId).find("input[type='submit']").removeClass('button_disabled').addClass('bg_F89728');
    
  }
  
  //---------------------------------------------------------------------------------------
  
  /*
   * @access: public 
   * @description: this function is used to remove loader before submit button
   * @return: void
   */ 
  
  
  function loadererror(formId){
    $('#'+formId).find(".submit_loading").remove();
    $('#'+formId).find("input[type='submit']").removeClass('button_disabled').removeClass('bg_F89728');
  }
  
  //---------------------------------------------------------------------------------------
  
  /*
   * @access: public 
   * @description: this function is used to open loader for ajax 
   * @return: void
   */ 
  
  
  function loaderopen(formId){
    $('#'+formId).find("input[type='submit']").addClass('button_disabled');
   
    var openbox = $("#ajax_open_popup_html").attr('openbox');
    //hide open model box 
    hide_popup(openbox);
    
    var loadearHtml = '';
    loadearHtml += '<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="loader" class="modal fade loader_with_txt dn">';
    loadearHtml += '<div class="modal-dialog">';
    loadearHtml += '<div class="">';//modal-content
    loadearHtml += '<div class="modal-body">';
    loadearHtml += '<div class="modelinner">';
    loadearHtml += '<div class="loader_wrapper">';
    loadearHtml += '<img src="' + baseUrl + 'themes/assets/images/loader_img.gif" alt="loader_img" /><p class="loader_msg_text">Please wait for a while..</p>';
    loadearHtml += '<div class="load_succ_txt dn lh_28px"></div>';
    loadearHtml += '</div>';
    loadearHtml += '</div>';
    loadearHtml += '</div>';
    loadearHtml += '</div>';
    loadearHtml += '</div>';
    loadearHtml += '</div>';
    $("#ajax_open_popup_html").html(loadearHtml); 
    //$("#ajax_open_popup_html").attr('openbox','loader_popup_show');
    //open loader
    open_model_popup('loader_popup_show');
    
  }
  
  //---------------------------------------------------------------------------------------
  
  /*
   * @access: public 
   * @description: this function is used to close loader for ajax 
   * @return: void
   */ 
  
  function loaderclose(){
    //hide loader
    hide_popup('loader_popup_show');
    
    //remove active loader id
    //$("#ajax_open_popup_html").removeAttr('openbox');
  }
  
  //---------------------------------------------------------------------------------------
  
  /*
   * @access: public 
   * @description: this function is used to check validate email address
   * @return: void
   */ 
   
  function isValidEmailAddress(emailAddress) {
  var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
  return pattern.test(emailAddress);
  }
  
  //---------------------------------------------------------------------------------------
  
  /*
   * @access: public 
   * @description: this function is used to for user login
   * @return: void
   */ 
  
  function doLogin(){
    
    $("#formLogin").submit(function( event ) {
      var fromData=$("#formLogin").serialize();
      var url = baseUrl+'home/login';
        $.ajax({
          type:'POST',
          data:fromData,
          url: url,
          dataType: 'json',
          cache: false,
          beforeSend: function( ) {
            //open loader
            loaderopen();
          },
          success: function(data){  
            //check data 
            if(data){
              if(data.is_success=='true'){
                  //redirect to home page
                  window.location = baseUrl+'dashboard';
              }else{
                //hide loader
                loaderclose();
                //if want to show success msg
                var showMsg = '';
                if(typeof(data.msg)=='string'){
                  showMsg = data.msg;
                }else{              
                  $.each(data.msg, function(index, val) {
                    showMsg+= val+'<br>';
                  });
                }
                //open error message
                custom_popup(showMsg,false);
                
                //add class in custome popup button
                var clickbox = $("#ajax_open_popup_html").attr('clickbox');
                $(".cust_popup_button").addClass(clickbox);
              }
            }
          },
          error: function(XMLHttpRequest, textStatus, errorThrown) {
            //hide loader
            loaderclose();
            
            //open error message
            custom_popup(ajaxErrorMsg,false);
            
            //add class in custome popup button
            var clickbox = $("#ajax_open_popup_html").attr('clickbox');
            $(".cust_popup_button").addClass(clickbox);
          }
        });
        
      return false;  
    });
    
  }
  
  //---------------------------------------------------------------------------------------
  
  /*
   * @access: public 
   * @description: this function is used to for user register
   * @return: void
   */ 
  
  function doRegistration(){
    
    $("#formRegister").submit(function( event ) {
      var fromData=$("#formRegister").serialize();
      var url = baseUrl+'home/register';
        $.ajax({
          type:'POST',
          data:fromData,
          url: url,
          dataType: 'json',
          cache: false,
          beforeSend: function( ) {
            //open loader
            loaderopen();
          },
          success: function(data){  
            //check data 
            if(data){
              if(data.is_success=='true'){
                  //hide loader
                  loaderclose();
                  //open sucesss message
                  custom_popup(data.msg,true);
                  $(".cust_popup_button").removeClass('user_register');
              }else{
                //hide loader
                loaderclose();
                //if want to show success msg
                var showMsg = '';
                if(typeof(data.msg)=='string'){
                  showMsg = data.msg;
                }else{              
                  $.each(data.msg, function(index, val) {
                    showMsg+= val+'<br>';
                  });
                }
                //open error message
                custom_popup(showMsg,false);
                
                //add class in custome popup button
                var clickbox = $("#ajax_open_popup_html").attr('clickbox');
                $(".cust_popup_button").addClass(clickbox);
              }
            }
          },
          error: function(XMLHttpRequest, textStatus, errorThrown) {
            //hide loader
            loaderclose();
            
            //open error message
            custom_popup(ajaxErrorMsg,false);
            
            //add class in custome popup button
            var clickbox = $("#ajax_open_popup_html").attr('clickbox');
            $(".cust_popup_button").addClass(clickbox);
          }
        });
        
      return false;  
    });
    
  }
  
  //---------------------------------------------------------------------------------------
  
  /*
   * @access: public 
   * @description: this function is used to get data by ajax requrest data in return 
   * @return: response
   */ 
  
  function doAjaxRequest(postUrl,sendData,dataSetId,selectedId){
      
    var url = baseUrl+postUrl;
    var returnData = 'first';
    $.ajax({
      type:'POST',
      data:sendData,
      url: url,
      //dataType: 'json',
      cache: false,
      beforeSend: function( ) {
        //open loader
        //loaderopen();
      },
      success: function(data){  
        //check data 
      
        $("#"+dataSetId).html(data);
          if(selectedId   != undefined) { 
            $("#"+dataSetId).val(selectedId);  
          } 
      },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
        //hide loader
        //loaderclose();
        
        //open error message
        //custom_popup(ajaxErrorMsg,false);
      }
    });
  }
  
  //---------------------------------------------------------------------------------------
  
  /*
   * @access: public 
   * @description: this function is used to delete after user confirmation 
   * bootstrape confirm box
   * @return: response
   */ 
   
  function customconfirm(clickCls,postUrl,message,headerbgval,buttonbgval,isRemoveDiv,isHeader,isLoaderShow,msgToShow){

    if(isLoaderShow  == undefined)  {  isLoaderShow = true;  } // showloader
    if(msgToShow  == undefined)     {  msgToShow = 'deleted successfully';  } // success message

    
    $(document).on("click", "."+clickCls, function(){
      // get Remove div id
        var removeDivId = $(this).parent().parent().parent().attr('id');
      if(isHeader){
        var removeDivId = $(this).parent().parent().parent().attr('id');
      }
      
      var deleteId = $(this).attr('deleteid'); //delete record id 
      
      //check not undefined
      if(headerbgval==undefined){
        //var headerbg = $('#ajax_open_popup_html').attr('headerbg'); //get current page header color
        var headerbg = 'bg_0073ae'; //get current page header color
        headerbgval  = headerbg;
      }
      
      //check not undefined
      if(buttonbgval==undefined){
        //var buttonbg = $('#ajax_open_popup_html').attr('buttonbg'); //get current page button color
        var buttonbg = 'custombtn_save';  //get current page button color
        buttonbgval  = buttonbg;
      }
      //check not undefined
      if(message   == undefined || message==false) { message  = '<p class="form-label ">Are you sure you want to delete?</p>';  }
      
      //confirm poup code
      bootbox.confirm({
        title: 'Confirm',
        message: message,
        className: 'in',
        buttons: {
          'cancel': {
            label: 'Cancel',
            className: 'btn-normal btn'
          },
          'confirm': {
            label: 'Ok',
            className: 'btn-normal btn'
          }
        },
        callback: function(result) {
          
          if (result) {
            var url = baseUrl+postUrl;
            var fromData = {'deleteId':deleteId };
            
            $.ajax({
              type:'POST',
              data:fromData,
              url: url,
              dataType: 'json',
              async: true,
              cache: false,
              beforeSend: function( ) {
                if(isLoaderShow){  
                    //open loader
                    loaderopen();
                } 
              },
              success: function(data){  
                //check data 
                if(data){

                  if(isLoaderShow){  
                    showlaoderwithmessag(data.msg);
                  } else {
                    custom_popup(msgToShow);
                  }

                  setTimeout(function () {
                    if(isLoaderShow){  
                        //hide loader
                        loaderclose();
                    }
                  }, 2000);
                }
                if(data.is_success=='true'){
                  // set collapse hight
                  //$('.accordion_content').css('height','auto');
                  if(isRemoveDiv==true){
                    $("#"+removeDivId).remove();
                  } 
                }
                
                
              },
              error: function(XMLHttpRequest, textStatus, errorThrown) {
                if(isLoaderShow){  
                    //hide loader
                    loaderclose();
                }
                
                //open error message
                custom_popup(ajaxErrorMsg,false);
                $("#thanks_popup").find('.modal-header').addClass("danger_color");
                //add class in custome popup button
                var clickbox = $("#ajax_open_popup_html").attr('clickbox');
                $(".cust_popup_button").addClass(clickbox);
              }
            });
          }
        }
      }).find("div.modal-header").addClass("medium dt-large");
    
    });
    
  } 
  
  //---------------------------------------------------------------------------------------
  
  /*
   * @access: public 
   * @description: this function is used to detect page leaving
   * @return: void
   */ 

  
  function pageLeave(){
    $(function() {
    
    //detect any key press set true
    $('input, textarea').keyup(function(){
      isChanged = true;
      $(this).closest('.form-horizontal').find("input[type='submit']").removeClass('button_disabled').removeClass('bg_F89728');
    });
    
    //detect any data change set true
    $('input, textarea, radio, checkbox, select, file').change(function(){
      isChanged = true;
      $(this).closest('.form-horizontal').find("input[type='submit']").removeClass('button_disabled').removeClass('bg_F89728'); 
    })
    
    
    var themeChanges = $('#is_theme_').val();
    themeChanges = (themeChanges == 1) ? 1 : '';
      /*$("body").keydown(function(e){
        //|| e.which==13
        if(e.which==116){
          //set default value form open
          isChanged = false;
          $('.form_open_chk').each(function(){
            if($(this).is(':visible')){
              isChanged = true;
            }
          });
              
          var headerbgval = $('#ajax_open_popup_html').attr('headerbg');  //get current page header color
          
          //check not undefined
          var buttonbgval = $('#ajax_open_popup_html').attr('buttonbg');  //get current page button color
            
          message  = 'Are you sure you want to leave this Event?';  
          
          if(isChanged){
            //confirm poup code
            bootbox.confirm({
              title: 'Warning',
              message: message,
              buttons: {
                'cancel': {
                  label: 'Cancel',
                  className: 'btn-default custombtn'
                },
                'confirm': {
                  label: 'Exit',
                  className: 'btn-danger custombtn '+buttonbgval
                }
              },
              callback: function(result) {
                if (result) {
                  refreshPge();
                }
              }
            }).find("div.modal-header").addClass(headerbgval);
          
            e.preventDefault();
          } 
        }
      });*/
        
      $('.page_leave').click(function(e){
        
        //set default value form open
        /*isChanged = false;
        $('.form_open_chk').each(function(){
          if($(this).is(':visible')){
            isChanged = true;
          }
        });*/
        
        var hrefLink = $(this).attr('href');
      
        //var headerbgval = $('#ajax_open_popup_html').attr('headerbg');  //get current page header color
        var headerbgval = 'bg_b5101c';  //get current page header color
            
        //check not undefined
        //var buttonbgval = $('#ajax_open_popup_html').attr('buttonbg');  //get current page button color
        var buttonbgval = 'custombtn_save'; //get current page button color
        if(themeChanges==1){  
            message  = '<span class="small">It looks like you have been editing something, if you leave before saving, then your changes will be lost. Are you sure you want to leave this page?</span>';  
        }else   {
            message  = '<span class="small">Are you sure you want to leave this page?</span>';  
        }
        
        if(isChanged){
          //confirm poup code
          bootbox.confirm({
            title: 'Confirm',
            message: message,
            buttons: {
              'cancel': {
                label: 'Cancel',
                className: 'btn-normal btn'
              },
              'confirm': {
                label: 'Ok',
                className: 'btn-normal btn'
              }
            },
            callback: function(result) {
              if (result) {
                window.location.href = hrefLink;
              }
            }
          }).find("div.modal-header").addClass("medium dt-large");
        
          e.preventDefault();
        }
      }); 
    
    });   

  }
  
  //------------------------------------------------------------------------
  
  /*
   * @access: public 
   * @description: this function is used to manage record list pagination
   * bootstrape confirm box
   * @return: response
   */ 
  
  function recordpagination(postUrl,formId){
    
    $(document).on('click','.pagination_action',function() {
      
        //check if buttton is disabled
        if($(this).hasClass('disabled')){
          return false;
        }
        
        var  searchedData = parseInt($("#searchedData").val());
        
        if(formId!=undefined){
          //if user not searched then reset form
          if(searchedData==0){
            $('#'+formId)[0].reset();
          }
          
          var fromData=$('#'+formId).serialize();
          var page = $(this).attr('page');
          fromData = fromData+'&page='+page;
        }else{
          var page = $(this).attr('page');
          fromData = {'page':page};
        } 
        
        var url = baseUrl+postUrl;
        $.ajax({
          type:'POST',
          data:fromData,
          url: url,
          dataType: 'html',
          cache: false,
          beforeSend: function( ) {
            //open loader
            loaderopen();
          },
          success: function(data){  
            //check data 
            if(data){
              loaderclose();
              $(".loadviewsection").html(data);
            }
            
            //call tooltip
            $('.show_tool_tip').tooltip();
          },
          error: function(XMLHttpRequest, textStatus, errorThrown) {
            //hide loader
            loaderclose();
            custom_popup('Request failed.',false);
          }
        });
      
    });
  }
  

  /*
   *--------------------------------------------------------------------------------------- 
   * This function is used to search record and load new records 
   *---------------------------------------------------------------------------------------
   * @parm1 = formid  (string)
   */ 

  function searchrecord(formId,postUrl){
    
    $(document).on('submit',"#"+formId,function( event ) {
      var fromData=$(this).serialize();
      var url = baseUrl+postUrl;
        $.ajax({
          type:'POST',
          data:fromData,
          url: url,
          cache: false,
          beforeSend: function( ) {
            //open loader
            loaderopen();
          },
          success: function(data){  
            //check data 
            if(data){
              loaderclose();
              $(".loadviewsection").html(data);
              //set one if user searched 
              $("#searchedData").val('1');
            }
            //call tooltip
            $('.show_tool_tip').tooltip();
            
            //scroll 
            $('.tabledatascrolling').perfectScrollbar({wheelSpeed:100});
          },
          error: function(XMLHttpRequest, textStatus, errorThrown) {
            //hide loader
            loaderclose();
            custom_popup('Request failed.',false);
          }
        });
        
      return false;  
    });
  }
  
  
  /*
   *--------------------------------------------------------------------------------------- 
   * This function is used to accept only alphanumeric value
   *---------------------------------------------------------------------------------------
   * @parm1 = @void
   */ 
  $(document).ready(function(){
     
    $(".alphaNumValue").keypress(function(e) {
              
                 
        if(((e.which>=34 && e.which<=47) || (e.which>=58 && e.which<=64) || (e.which>=91 && e.which<=96) || (e.which>=123 && e.which<=126)) && e.which!=45)
        {
            custom_popup('Please enter alpha numeric value only.',false);
          return false;
        } 
      }); 
      
      
  });
  
    /*
   *--------------------------------------------------------------------------------------- 
   * This function is used to accept only charactor value
   *---------------------------------------------------------------------------------------
   * @parm1 = @void
   */ 
  $(document).ready(function(){
     
    $(".alphaValue").keypress(function(e) {
      
        if(!((e.which>=65 && e.which<=90) || (e.which>=97 && e.which<=122) || e.which==8 || e.which==0 ||  e.which==45 || e.which<34))
        {
            custom_popup('Please enter letters  only.',false);
          return false;
        } 
      }); 
      
      
  });
  /*
   *--------------------------------------------------------------------------------------- 
   * This function is used to allowed numeric and + sign
   *---------------------------------------------------------------------------------------
   * @parm1 = @void
   */ 
   
   $(document).ready(function(){
   $(".mobileValue").keypress(function(e) {
     
           
      if(((e.which!=43) && (e.which<48 || e.which>57 ) && (e.which!=8 && e.which!=0)))
      {
        custom_popup('Please enter numeric value.',false);  
          return false;
      } 
    });
     
    }); 
    
    
   $(document).ready(function(){
   $(".phoneValue").keypress(function(e) {
      
       var objRegex = /(^[0-9]+[-]*[0-9]+$)/;
           
      if((objRegex.test(e)))
      {
        custom_popup('Please enter numeric value.',false);  
          return false;
      } 
    });
     
    }); 
    
    
      /*
   *--------------------------------------------------------------------------------------- 
   * This function is used to allowed only numeric value
   *---------------------------------------------------------------------------------------
   * @parm1 = @void
   */ 
   
   $(document).ready(function(){
   $(".numValue").keypress(function(e) {
     
     
      if((e.which<48 || e.which>57) && (e.which!=8 && e.which!=0))
      {
          custom_popup('Please enter numeric value only.',false); 
          return false;
      } 
    });
     
    }); 
    
    
    /*
   *--------------------------------------------------------------------------------------- 
   * This function is used to prevent past of the data
   *---------------------------------------------------------------------------------------
   * @parm1 = @void
   */ 
      //this is used for prevent past
    $('.block_past').bind("paste",function(e) {
      e.preventDefault();
    });
    
  
  /*
   * 
   *-------------------------------------------- 
   * disable button click show error message
   * @description: while data is saving and user click on save button 
   * then error message would be appear
   *-------------------------------------------- 
   * 
   */ 
   
  $(function() {
    $(document).on("click",".button_disabled",function(){
      //open error message
      custom_popup('Form data is saving, please wait...',false);
    });
  });
  
  
    /*
    *-------------------------------------------- 
    * @description: This function is used to manage unit price, gst and total price 
    *-------------------------------------------- 
    */ 
  
    function unitPriceManage(unitPrice,selectionId,GSTField,TotalPrice,gstRate,TotalPriceIncGST){
        if(TotalPriceIncGST  == undefined) { TotalPriceIncGST = false;  }   
        //these value set if not passing these value
        if(gstRate  == undefined) { gstRate = 0;  } //gstRate    
        $('.'+unitPrice).change(function(){
            //if set false then only apply static forms
            if(selectionId != false){
                registrantId = $(this).attr(selectionId);
            }else{
                registrantId = '';
            }
            //get entered unit price
            unitPrice  = parseInt($(this).val());
            if($(this).val() != ""){
                //check gst rate is greater than
                if(gstRate > 1 ){
                  var GSTAddition = (unitPrice*gstRate)/100;
                  var totalPrice = unitPrice;// + GSTAddition;
                }else{
                  var GSTAddition = gstRate;
                  var totalPrice = unitPrice;// + GSTAddition;
                }
                var totalpricegstinc= unitPrice + GSTAddition;

                $("#"+GSTField+registrantId).val(GSTAddition.toFixed(2));
                $("#"+TotalPrice+registrantId).val(totalPrice.toFixed(2));
                if(TotalPriceIncGST){
                   $("#"+TotalPriceIncGST+registrantId).val(totalpricegstinc.toFixed(2));
                }
            } else {
                $("#"+GSTField+registrantId).val('');
                $("#"+TotalPrice+registrantId).val('');
                $("#"+TotalPriceIncGST+registrantId).val('');        
            } 
        });
    }


    //---------------------------------------------------------------------------------------

    /*
    *--------------------------------------------------------------------------------------- 
    * This function is used to Show/Hide area
    *---------------------------------------------------------------------------------------
    * @parm1 = action  (click/change)
    * @param2 = actionAttr  (class/id)
    * @param3 = affectedArea (area need to hide show)
    */ 
    function showHideArea(action, actionAttr, affectedArea, fieldValidate1, fieldValidate2){
        if(fieldValidate1      == undefined) {  fieldValidate1      = true;  }
        if(fieldValidate2      == undefined) {  fieldValidate2      = true;  }
        $(document).on(action, actionAttr, function(){
            $(".accordion_content").css("height", 'auto');
            if($(this).val()==1){
                showarea(affectedArea);
                if(fieldValidate1){
                    $("#"+fieldValidate1).attr('required','');
                }
                if(fieldValidate2){
                    $("#"+fieldValidate2).attr('required','');
                }
            } else {
                hidearea(affectedArea);
                if(fieldValidate1){
                    $("#"+fieldValidate1).attr('data-parsley-required', 'false');
                    $("#"+fieldValidate1).removeAttr('required');
                }
                if(fieldValidate2){
                    $("#"+fieldValidate2).attr('data-parsley-required', 'false');
                    $("#"+fieldValidate2).removeAttr('required');
                }
            }
        });
    }
  
  
/**************************************************************************************/
/*function callDatetimePicker(actionId){     
  $("#"+actionId).datetimepicker({
    startDate: new Date(), 
    //showMeridian: true,
    autoclose: true,
    todayBtn: true,
    pickerPosition: "bottom-left",
    minuteStep: 1
  });
}

function callDatePicker(actionId){
   $("#"+actionId).datetimepicker({
    startDate: new Date(), 
    pickDate: true,
  });
}

function callTimePicker(actionId){
   $("#"+actionId).datetimepicker({
    pickTime: true,  
  });
} */

//for date picker
$(document).ready(function(e) {
  /*$(".datetimepicker").click(function(e) {
    var actionId = $(this).parent().parent().attr('id'); 
    callDatetimePicker(actionId);
  }); 
  
  $(".datepicker").click(function(e) {
    var actionId = $(this).parent().parent().attr('id'); 
    callDatePicker(actionId);
  }); 
  
  $(".timepicker").click(function(e){
    var actionId = $(this).parent().parent().attr('id'); 
    callTimePicker(actionId);
  });*/


  
  //load datetimepicker
  var startDate = new Date();
    $( ".datetimepicker" ).datetimepicker({
      minDate: startDate,
      //minDate: eventStartDate,
      //maxDate: eventEndDate,
      changeMonth: true,
      changeYear: true,
      showOn: "both",
      buttonImage: baseUrl+"themes/system/images/calender_icon.png",
      dateFormat: 'dd M yy',
      buttonText: 'Calendar',
      controlType: 'select',      
      buttonImageOnly: true,
      pickerPosition: "bottom-left",
      timeFormat: 'HH:mm',
      /*timeFormat: 'HH:mm:ss z',
      timezone: 'MT',
      timezoneList: [ 
          { value: 'ET', label: 'Eastern'}, 
          { value: 'CT', label: 'Central' }, 
          { value: 'MT', label: 'Mountain' }, 
          { value: 'PT', label: 'Pacific' } 
        ]*/
      controlType: myControl
    });
    
    
    //~ //load datepicker 
    //~ $( ".datepicker" ).datepicker({
      //~ controlType: 'select',
      //~ minDate: 0,
      //~ dateFormat: 'dd M yy',
    //~ });
    
    $(function() {
    $( ".s_datepicker" ).datetimepicker({
      controlType: 'select',  
      minDate: 0,
      defaultDate: "+1",
      changeMonth: true,
      numberOfMonths: 1,
      showTime:false,
      showSecond:true,
      dateFormat: 'dd M yy',
       timeFormat: 'HH:mm:ss',
      onClose: function( selectedDate ) {
        $( ".e_datepicker" ).datepicker( "option", "minDate", selectedDate );
      }
    });
    $( ".e_datepicker" ).datetimepicker({
      controlType: 'select',
      defaultDate: "+1",
      changeMonth: true,
      numberOfMonths: 1,
      showSecond:true,
      showTime:false,
      dateFormat: 'dd M yy',
       timeFormat: 'HH:mm:ss',
      onClose: function( selectedDate ) {
        $( ".s_datepicker" ).datepicker( "option", "maxDate", selectedDate );
      }
    });
  });
  
  
 $(function() {
    $( "#registerStartDate" ).datetimepicker({
      controlType: 'select',  
      minDate: 0,
      defaultDate: "+1",
      showSecond:true,
      changeMonth: true,
      numberOfMonths: 1,
      dateFormat: 'dd M yy',
      timeFormat: 'HH:mm:ss',
      showTime:false,
      onClose: function( selectedDate ) {
        $( "#registerLastDate" ).datepicker( "option", "minDate", selectedDate );
      }
    });
    $( "#registerLastDate" ).datetimepicker({
      controlType: 'select',
      defaultDate: "+1",
      changeMonth: true,
      numberOfMonths: 1,
      dateFormat: 'dd M yy',
        timeFormat: 'HH:mm:ss',
      showTime:false,
      onClose: function( selectedDate ) {
        $( "#registerStartDate" ).datepicker( "option", "maxDate", selectedDate );
      }
    });
  });  
    

    $('.timepicker').timepicker({
      showOn: "both",
      buttonImage: baseUrl+"themes/system/images/calender_icon.png",
      buttonText: 'Calendar',
      controlType: 'select',
      timeFormat: 'HH:mm',
      buttonImageOnly: true,
      //timeFormat: 'HH:mm:ss z',
    });
    
   /* 
    //load timepicker 
    timepickercall();
  */


  /*$(".date_input").datetimepicker({
    format: "dd M yyyy hh:ii",
    startDate: new Date(), 
    //showMeridian: true,
    autoclose: true,
    todayBtn: false,
    pickerPosition: "bottom-right",
    minuteStep: 1
  }); */
  
  /*$(".datepicker_btn").click(function(e) {
    var actionId = $(this).parent().find('input').attr('id'); 
    $(this).addClass("calender_opened");
    $(this).parent().find('input').datetimepicker('show');
    callDatetimePicker(actionId);
  }); */
  

  /*  
  $('.date_input').datetimepicker().on('show', function(ev){
      $(this).siblings(".datepicker_btn").addClass("calender_opened");
  });

  $('.date_input').datetimepicker().on('hide', function(ev){
      $(".datepicker_btn").removeClass("calender_opened");
  });  
  */
});


var myControl=  {
  create: function(tp_inst, obj, unit, val, min, max, step){
    $('<input class="ui-timepicker-input" value="'+val+'" style="width:50%">')
      .appendTo(obj)
      .spinner({
        min: min,
        max: max,
        step: step,
        change: function(e,ui){ // key events
            // don't call if api was used and not key press
            if(e.originalEvent !== undefined)
              tp_inst._onTimeChange();
            tp_inst._onSelectHandler();
          },
        spin: function(e,ui){ // spin events
            tp_inst.control.value(tp_inst, obj, unit, ui.value);
            tp_inst._onTimeChange();
            tp_inst._onSelectHandler();
          }
      });
    return obj;
  },
  options: function(tp_inst, obj, unit, opts, val){
    if(typeof(opts) == 'string' && val !== undefined)
      return obj.find('.ui-timepicker-input').spinner(opts, val);
    return obj.find('.ui-timepicker-input').spinner(opts);
  },
  value: function(tp_inst, obj, unit, val){
    if(val !== undefined)
      return obj.find('.ui-timepicker-input').spinner('value', val);
    return obj.find('.ui-timepicker-input').spinner('value');
  }
};
/**************************************************************************************/


//Capital each word in string
function capWords(str){ 
   var words = str.split(" "); 
   for (var i=0 ; i < words.length ; i++){ 
      var testwd = words[i]; 
      var firLet = testwd.substr(0,1); 
      var rest = testwd.substr(1, testwd.length -1) 
      words[i] = firLet.toUpperCase() + rest 
   } 
   return words.join(" "); 
} 
function checkFileSize(input){
	 if(input.files[0].size > 2042890){
		 $(input).val('');
		$('.show_error').trigger('click');
		$('.error_message_show').html('File too large, please upload file upto 2MB.');
		return false;
	 }else {
		return true;
	 }
}
//setup ajax submtting for all venue form 
ajaxdatasave('userBasicInformation', 'profile', false, true, false, false, true, '#showHideBasicInfoForm', '#showHideUserPassword');
ajaxdatasave('userPassword', 'profile', false, true, false, false, true, '#showHideUserPassword', '#showHideUserLogoProfile');
ajaxdatasave('userProfile', 'profile', false, true, false, false, true, '#showHideUserLogoProfile', '#showHideSocailMediaForm');
ajaxdatasave('userSocialMedia', 'profile', false, true, false, false, true, '#showHideSocailMediaForm', '#showHideNotificationForm');
ajaxdatasave('userNotification', 'profile', false, true, false, false, true, '#showHideNotificationForm', '#showHideBasicInfoForm');

// form social media for profile section
function tempId(){
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for( var i=0; i < 5; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
} 

function show_custom_popup(msg,is_success){
  
    if(is_success){
     
      var clickbox = $("#ajax_open_popup_html").attr('clickbox');
      
      if(clickbox!=undefined){
        $(".cust_popup_button").removeClass(clickbox);
      }
      //set title 
      $('.popu_title').html('Success');
      
      $("#success_popup_with_message").find('.modal-header').addClass('managers_color');
      
    } 
     
    $('.show_custome_msg').html(msg);
    $('.popup_message').html(msg);
   
    
    open_model_popup('success_popup_with_message');

    //hide custome popup after 3 second
    if(is_success){
      setTimeout(function() {
        hide_popup('success_popup_with_message');
      }, 1000);
    }    
  }
