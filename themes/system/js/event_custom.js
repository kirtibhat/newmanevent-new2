function eventcustompopup(postUrl,modelId,formPostData,stepperId){
        
        
        var url = baseUrl+postUrl;
        $.ajax({
          type:'POST',
          data:formPostData,
          url: url,
          cache: false,
          beforeSend: function( ) {
            //open loader
            loaderopen();
          },
          success: function(data){  
            //check data 
            if(data){
              loaderclose();
              
              $("#opentModelBox").html(data);
              $('#'+modelId).modal({ keyboard: false});
                                        
              // initialize dropdown : call a function
              //ini_custom_dropdown();
              
              // initialize tooltips : call a function
              ini_tooltips();
              
              // initialize stepper
              if(stepperId!=""){
                $("#"+stepperId).stepper();
              } 
              
            }else{
              //hide loader
              loaderclose();
              custom_popup('Request failed.',false);
            }
          },
          error: function(XMLHttpRequest, textStatus, errorThrown) {
            //hide loader
            loaderclose();
            //open error message
            custom_popup(ajaxErrorMsg,false);
          }
        });
        
      return false;  
  
}


// Save corporate space type
$(document).on('click',"#corporateSpaceTypeButton",function( event ) {
            
  //validate form
  if($("#formCorporateSpaceType").parsley().isValid()){
      
      if($(this).find("input[type='submit']").hasClass('button_disabled')){
        return false;
      }
      event.preventDefault(); 
      var checkedCount=0;
       // get total checked checkbox
      $( "input[name='space[]']:checked" ).each(function(){
          if($(this).prop('disabled')==false){
           checkedCount = checkedCount+1;
          }
      });
      
     if(checkedCount==$("#space_ava").val()*1){ 
      
      var removeDivID = $("#removeDivID").val();
      var fromData=$("#formCorporateSpaceType").serialize();
      var url = baseUrl+'event/addupdatecorporatespacetype';
      
        $.ajax({
            type:'POST',
            data:fromData,
            url: url,
            dataType: 'json',
            async: true,
            cache: false,
            beforeSend: function( ) {
            //overlay loader
            loaderopen();
          },
            success: function(data){
            
            
             //open error message
            $("#space_type_popup").modal('hide');
            
            //check data 
            if(data){
              
              if(data.is_success=='true'){
              
               //show text in loader
               showlaoderwithmessag(data.msg);               
               
               var rediectUrl = data.url;
               setTimeout(function () {
                window.location = baseUrl+rediectUrl;
               }, 2000);  
              
                  
              }else{
                
                //hide loader
                loaderclose();
                
                //if want to show success msg
                var showMsg = '';
                if(typeof(data.msg)=='string'){
                  showMsg = data.msg;
                }else{              
                  $.each(data.msg, function(index, val) {
                    showMsg+= val+'<br>';
                  });
                }
                //open error message
                custom_popup(showMsg,false);
                
              }
            } 
          },
          error: function(XMLHttpRequest, textStatus, errorThrown) {
            //hide loader
            loaderclose();
            
            $("#space_type_popup").modal('hide');
            
            //open error message
            custom_popup(ajaxErrorMsg,false); 
              
          }
        });
     }else{
      $("#space_type_popup").modal('hide'); // hide the model
      //open error message
      //custom_popup("Allocated booths does not match with total spaces available.",false);
      custom_popup("The spaces selected exceed the amount of spaces available to this space type.",false);
    }
    } //end validation     
    
});
  

function loadPerticularDiv(formId){
  
  if('formContactPerson'==formId){
    var eventContactPersionId = $("#eventContactPersionId").val();
    var reloadDiv = $("#eventcustomcontactlistId");
    var postUrl   = 'event/geteventcustomcontact/'+eventContactPersionId;
  }
  
  var url = baseUrl+postUrl;
  $.ajax({
    type:'POST',
    data:{},
    url: url,
    cache: false,
    success: function(data){
      reloadDiv.html(data);
    }
  });
      return false;
}

