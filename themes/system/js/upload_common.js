//---------------------------------------------------------------------------------------

  /*
   * @description: This function  is use to upload file 
   * @param uploadUrl(string)
   * @param postData(object)
   * @param fileTypes(string)
   * @param delteUrl(string)
   * @param isUploadList(bool) 
   * @auther: lokendra
   * 
   */ 


  function upload_file(uploadUrl,postData,otherObj,modelId){
    
      var uploader = new plupload.Uploader({
        runtimes: 'html5,flash,gears,browserplus,silverlight,html4', 
        url: uploadUrl,
        browse_button : otherObj.pickfiles,
        button_browse_hover : true,
        drop_element : otherObj.dropArea,
        autostart : otherObj.autoStart,
        max_file_size: otherObj.maxfilesize,
        container: otherObj.FileContainer,
        chunk_size: '1mb',
        unique_names: otherObj.uniqueNames,
        multi_selection:otherObj.multiselection,
        multipart_params: postData,
        // Flash settings
        flash_swf_url: "js/plupload.flash.swf",
        // Silverlight settings
        silverlight_xap_url: "js/plupload.silverlight.xap"
      });
      
      var fileTypes = otherObj.fileTypes;
      var fileTypesFilter = 'notallow'; 
      var $body = $("body");
      var $dropArea = $("#"+otherObj.dropArea);

      $body.bind("dragenter", function(e){ 
        $dropArea.addClass("draggingFile");
        e.stopPropagation();
        e.preventDefault();
      });

      $body.bind("dragleave", function(e){ $dropArea.removeClass("draggingFile"); });
      
      $body.bind("dragover", function(e){
        $dropArea.addClass("draggingFile");
        e.stopPropagation();
        e.preventDefault();
      });

      $body.bind("drop", function(e){
        e.stopPropagation();
        e.preventDefault();
        $dropArea.removeClass();
      });

      $dropArea.bind("dragenter", function(e){
        $dropArea.addClass("draggingFileHover");
        e.stopPropagation();
        e.preventDefault();
      });
      $dropArea.bind("dragleave", function(e){ $dropArea.removeClass("draggingFileHover"); });
      $dropArea.bind("dragover", function(e){
        $dropArea.addClass("draggingFileHover");
        e.stopPropagation();
        e.preventDefault();
      });
      
      //Checks to make sure the browser supports drag and drop uploads
      uploader.bind('Init', function(up, params){
        //if(window.FileReader && $.browser.webkit && !((params.runtime == "flash") || (params.runtime == "silverlight")))
        if(window.FileReader && !((params.runtime == "flash") || (params.runtime == "silverlight")))
        {
          $("#dropArea").show();
          $("#drag_msg").hide();
        }else{
          $("#drag_msg").show();
        }
      });
      //$('ele').click(function(){
        uploader.init();  
      //});
      
      
      uploader.bind('FilesAdded', function(up, files) {
        $dropArea.removeClass();
        $.each(files, function(i, file) {
          
          
          //Checks a comma delimted list for allowable file types set file types to allow for all
          var fileExtension = file.name.substring(file.name.lastIndexOf(".")+1, file.name.length).toLowerCase();
          
          var supportedExtensions = fileTypes.split(",");
          var supportedFileExtension = ($.inArray(fileExtension, supportedExtensions) >= 0);
          if(fileTypesFilter == "allow")
          {
            supportedFileExtension = !supportedFileExtension
          }
          //alert(supportedFileExtension);
          
          if((fileTypes == "all") || supportedFileExtension)
          {
            var addFile = otherObj.maxfileupload+1;
            
            if(up.files.length < addFile){
                
                var filename = file.name;
              
              if(filename.length > 25)
              {
                filename = filename.substring(0,25)+"...";       
              }
              
              //$('.as_themeBannerImg_').attr("src", 'http://localhost/newman/newmanfree/media/event_logo/'+file.id+'.'+fileExtension);
              //Add file list div
              if(otherObj.isFileList){
                $("#"+otherObj.mainDivFileList).show();
                $("#"+otherObj.listAddFile).append('<div class="'+otherObj.adddedfile+' display_file" id="'+file.id+'"> <span class="imagename_cont file_value font-xsmall">'+file.name+'</span><span class="clear_value delete_file small_icon" id="cancel_'+file.id+'" deleteid=""  fileid="'+file.id+'"><img src="'+baseUrl+'themes/assets/images/crox.png" onclick="deleteFileRow()"></span></div>');
                  $('.inputFileWrapper').css('display','none');  
                  // For set collapse hight
                  $(".accordion_content").css("height",'auto');
                  $(".addFileEvent").addClass('uploadedName');
              }
              
              up.refresh(); // Reposition Flash/Silverlight
               
               //fire upload when call function
                /*(function( $ ){
                   $.fn.fireupload = function() {
                     uploader.start();
                   }; 
                })( jQuery );*/
                
              $("#div_ar2RA_a").click(function(){
                
                //get last inserted table row id
                var tableInsertedId = $("#tableInsertedId").val();
                postData.tableInsertedId = tableInsertedId;
                  uploader.start();
              }); 

              //Bind cancel click event
              if(otherObj.isDelete){
                $('#cancel_'+file.id).click(function(){
                  var upfileId = $(this).attr('fileid');
                  var delete_id =$(this).attr('deleteid');
                  $fileItem = $('#' + file.id);
                  $fileItem.addClass("cancelled");
                  uploader.removeFile(file);
                  if(delete_id!=""){
                    deleteFile(otherObj.deleteUrl,delete_id);
                  }
                  $("#"+file.id).remove();
                  
                  //check if any file added for then show added div otherwise hide div
                  var filecount=0;
                   $('.'+otherObj.adddedfile).each(function(){
                    filecount++;
                  });
                  //if no file then hide file list div
                  if(filecount == 0){
                    $("#"+otherObj.mainDivFileList).hide();
                  }
                });
              }
            }else{
              
              if(modelId!=""){
                $('#'+modelId).modal('hide'); 
              } 
              
              //alert("You can only add "+otherObj.maxfileupload+" file(s).");
              //open success message
              custom_popup("You can only add "+otherObj.maxfileupload+" file(s).",false);
              
              up.removeFile(file);
              //Not a supported file extension
              $errorPanel = $('div.error:first');
              $errorPanel.show().html('<p>The file you selected is not supported in this section.');
              
          
              
            }
            
            
          }
          else
          {
            if(modelId!=""){
              $('#custom_email').modal('hide');
            } 
            $("#thanks_popup").find('.modal-header').addClass("danger_color");
            //alert("The file you selected is not supported");
            custom_popup("The file you selected is not supported",false);
            
             up.removeFile(file);
            //Not a supported file extension
            $errorPanel = $('div.error:first');
            $errorPanel.show().html('<p>The file you selected is not supported in this section.');
            
            
          }
        });
      });

      uploader.bind('UploadProgress', function(up, file) {
        var  $fileWrapper = $('#' + file.id);
        $fileWrapper.find(".plupload_progress").show();
        $fileWrapper.find(".plupload_progress_bar").attr("style", "width:"+ file.percent + "%");
        $fileWrapper.find(".percentComplete").html(file.percent+"%");
        $fileWrapper.find('#cancel'+file.id).addClass('hide');
      });

      uploader.bind('Error', function(up, err) {
        $errorPanel = $("div.error:first");
        //-600 means the file is larger than the max allowable file size on the uploader thats set in the options above.
        if(err.code == "-600")
        {
          $errorPanel.show().html('<p>The file you are trying to upload exceeds the single file size limit of 250MB</p>');
        }
        else
        {
          $errorPanel.show().html('<p>There was an error uploading your file '+ err.file.name +'.</p>');
        }

        $('#' + err.file.id).addClass("cancelled");
        uploader.stop();
        uploader.refresh(); // Reposition Flash/Silverlight
      });

      uploader.bind('FileUploaded', function(up, file, response) {
        var getObj = $.parseJSON(response.response);
        $('#cancel_'+getObj.fileId).attr("deleteid",getObj.id);
      });
    
      
      /*
       * @description: This function  is use to delete after page loaded 
       * any file is uploaded then manual delete it
       * 
       */ 
       
       
      /*$(".delete_file").click(function(){
        var upfileId = $(this).attr('fileid');
        var delete_id =$(this).attr('deleteid');
        if(delete_id!=""){
          deleteFile(otherObj.deleteUrl,delete_id);
        }
        $("#"+upfileId).remove();
        //check if any file added for then show added div otherwise hide div
        var filecount=0;
         $('.'+otherObj.adddedfile).each(function(){
          filecount++;
        });
        //if no file then hide file list div
        if(filecount == 0){
          $("#"+otherObj.mainDivFileList).hide();
        }
      });*/
  }
  
  //--------------------------------------------------------------------------------------- 
  
  /*
   * description: delete upload file after page loaded this section for implment 
   * when you used mulitple upload field in one page
   * @return void 
   *
   */
  function deleteUploadFiles(modelId){ 
    
    if(modelId      == undefined) {  modelId      = false;  } // success message
    
    $(".delete_file").click(function(){
      var upfileId = $(this).attr('fileid');
      var delete_id =$(this).attr('deleteid');
      var deleteUrl =$(this).attr('deleteurl');
        deleteUrl = baseUrl+deleteUrl;
      
      if(delete_id!=""){
        deleteFile(deleteUrl,delete_id,modelId);
      }
      $("#"+upfileId).remove();
      //check if any file added for then show added div otherwise hide div
      var filecount=0;
       $('.'+otherObj.adddedfile).each(function(){
        filecount++;
      });
      //if no file then hide file list div
      /*if(filecount == 0){
        $("#"+otherObj.mainDivFileList).hide();
      }*/
    });  
  } 
  
  //---------------------------------------------------------------------------------------
  
  /*
   * @description: This function  is use to delete upload file 
   * @param delteUrl(string)
   * @param deltetId(Integer)
   * @auther: lokendra
   * 
   */ 
  
  
  function deleteFile(deleteUrl,deleteId,modelId){
    var fromData={"deleteId":deleteId};
    $.post(deleteUrl,fromData, function(data) {
        if(data){
           if(modelId!=false){
          // close model popup and display message
          $("#"+modelId).modal('hide');
          }
          //display message
          custom_popup(data.msg,true);  
         
        }
      },"json");
    return false;  
  }
  
