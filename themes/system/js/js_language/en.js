
//****************This is frontend customValidation variables *********************************************

/*********------------------this is for simple registration --- *************************************/
 var error_msg='ERROR';
 var email_error_msg='Please enter valid email';
 var guest_label='Guest';
 var additional_guest_label='Additional Guests';
 var dietary_require_label='Dietary Requirements';
 var change_regis_type_msg='Are you sure you want to change registration type?';
 var sub_total_label='SUB TOTAL';
 var single_day_msg='Please select single day registration option.';
 var day_limit_msg='Sorry! Day registration limit has been excceded.';
 var guest_required_msg='Side event guest fileds are required.';
 var breakout_limit_msg='Breakout limit has been completed for this event.';
 var select_regis_msg='Please select registration type.';
 var amt_zero_msg='Amount should not be zero or blank.';
 var pay_sure_msg='Are you sure you want to payment by';
 var required_msg='Required';
 var delete_sure_msg='Are you sure you want to delete?';
 var ok_msg='Ok';
 var cancel_msg='Cancel';
/*********------------------this is for sponsor section --- *************************************/
 var select_booth_msg='Please select booth.';
 var booth_pending_msg='Sorry! This booth request is pending.';
 var pdf_plan_msg='Sorry! No floor plan pdf available for this event.';

