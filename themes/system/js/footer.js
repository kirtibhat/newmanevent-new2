//scroll 
$('.tabledatascrolling').perfectScrollbar({wheelSpeed:100});

//call form toggle plugin class
$('.showhideaction').formToggle();

 
//--------ADD MOBILE MENU JS------/
$("#main_togle_nav").click(function(){
	$("#dash_main_menu").slideToggle();
});	
//--------ADD MOBILE MENU JS------/


//remove padding-bottom
$('.past_event_list').css('padding-bottom','0px');

//hide and show current event list
$(".currenteventheader").click(function(){
	var getDivStatus = $("#show_event_list").is(':visible');
	console.log(getDivStatus);
	if(getDivStatus==true){
		$(".currenteventheader").removeClass('currenteventH').addClass('past_event');
		$('.current_event_list').addClass('bg_dee6e7').addClass('eventcontainerbg_radius');
		$('.addanother_event a').css('color','#425968');
		$('.current_event_list').css('padding-bottom','0px');
		hidearea("#show_event_list");
	}else{
		$(".currenteventheader").removeClass('past_event').addClass('currenteventH');
		$('.current_event_list').removeClass('bg_dee6e7').removeClass('eventcontainerbg_radius');
		$('.addanother_event a').css('color','#FFFFFF');
		$('.current_event_list').css('padding-bottom','25px');
		showarea("#show_event_list");
	}
});

//hide and show past event list
$(".pasteventheader").click(function(){
	var getDivStatus = $("#past_event_list").is(':visible');
	if(getDivStatus==true){
		//console.log('close');
		$(".pasteventheader").removeClass('currenteventH').addClass('past_event');
		$('.past_event_list').addClass('bg_dee6e7').addClass('eventcontainerbg_radius');
		$('.past_event_list').css('padding-bottom','0px');
		hidearea("#past_event_list");
	}else{
		//console.log('open');
		$(".pasteventheader").removeClass('past_event').addClass('currenteventH');
		$('.past_event_list').removeClass('bg_dee6e7').removeClass('eventcontainerbg_radius');
		$('.past_event_list').css('padding-bottom','25px');
		showarea("#past_event_list");	
	}
	
});

//remove padding-bottom
$('.past_event_list').css('padding-bottom','0px');
