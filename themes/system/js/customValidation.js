/** This js form used for frontend  form validation, to get fields data **/

/** ----------------- Function for validation of contact form form fields-------------------------------------------------**/

//function ContactFormValidation()

    $(document).ready(function(){
		
	var emailEmpty=false;
	$('.error_message').blur(function(){
	    var value=$(this).val();
	    var email =$.trim($("#contact_email").val());
		if($(this).hasClass('email'))
		{
			var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
			if(pattern.test(email)==false)
			{
                $('.contact_email').show();	
                $('.contact_check').remove();
		        $('.contact_person').removeClass('check_form');
		        emailEmpty=true;
		        return false;
			}
			else
			{
				emailEmpty=false;
				$('.contact_email').hide();	
			}	
	   }		
	     if(value=='')
         {
			if(!$(this).hasClass('empty_filed') && !$(this).hasClass('email'))
			{
			    
			   $(this).addClass('empty_filed');
			   $(this).after('<label class="efformessage"><span class="errorarrow"></span>'+error_msg+'</label>');
			   
		    }
		 }
		 else if(!$(this).hasClass('email'))
		 {
			  $(this).removeClass('empty_filed');
			  $(this).next(".efformessage").remove();
		 }
		 
		 
	    var empty=false; 
		 $('.error_message').each(function(){
			
			 if($(this).val()=='')
			 {
				 empty=true;
				 $('.contact_check').remove();
		         $('.contact_person').removeClass('check_form');
			 }
		 });
		 if(empty==false &&  emailEmpty==false)
		 {
			  $('.contact_person').append('<div class="complete_check contact_check"></div>');
		      $('.contact_person').addClass('check_form');
		      return true;
			
		 }
		 return false;
    });	
 /**beacuse select box proper view not proper in sponsor personal detaila section **/	 
	$('.selectpicker').selectpicker(); 
});	

	
/** ----------------- Function for validation of personal details form fields-------------------------------------------------**/
  $(document).ready(function(){ 
    
    $(document).on('blur','.required_field', function() {
	
				var value= $(this).val(); 
				 var empty=false; 
				   
				//this condition for checking selecting box is blur
				if($(this).hasClass('bootstrap-select')){
					var getVal = $(this).find('button').attr('title');
					if(getVal=='Please Select Title*'){
						 value='';
					}else{
						 value=getVal;
					}
				}
				 $(this).next(".efformessage").remove();
				 if(value=='')
				 {
					$(this).after('<label class="efformessage"><span class="errorarrow"></span>'+error_msg+'</label>');
				    empty=true;
				 }

				  $('input.required_field').each(function(){
				
					 if($(this).val()=='')
					{
						 $('.personal_check').remove();
						 $('.regi_form').removeClass('check_form');
						 empty=true;
						
					}
				 });
				 
				if(empty==false)
				{
					
					$('.regi_form').append('<div class="complete_check personal_check"></div>');
					$('.regi_form').addClass('check_form');
					return true;
				}
				 
			 });
    });
    
  

   /**
	 * Function for valid email address  status
	 */
	  
	function email_avila_chk(){
		
     var check=true;
    $('.email').each(function(){
		   	
	var email =$.trim($(this).val());

	var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
		 if(email!='' && pattern.test(email)==false)
		 {
			
			$('.email').after('<label class="efformessage email_error"><span class="errorarrow"></span>'+email_error_msg+'</label>');
		     check= false;
		 }
		 else
		 {
			 check=true;
			 $(".email_error").remove(); 
		 }
	});
	  return check;
	 
	}

	
/** ----------------- Function to guest fields validation--------------------------------------------------**/
  
 
   $(document).ready(function(){ 
	    
		$(document).on('blur','.guest_required', function() {
			
			var fieldEmpty=false;
			var value=$(this).val();
			
			//this condition for checking selecting box is blur
			if($(this).hasClass('bootstrap-select')){
				var getVal = $(this).find('button').attr('title');
				if(getVal==dietary_require_label+'*'){
					var value='';
				}else{
					var value=getVal;
				}
			}
			$(this).next(".efformessage").remove();
			if(value=='')
			{
				$(this).after('<label class="efformessage"><span class="errorarrow"></span>'+error_msg+'</label>');
				fieldEmpty=true;
			}
			 
			$('input.guest_required').each(function() {
				
				value=$(this).val();
				if(value=='')
				{
					fieldEmpty=true;
				}
		   });

		    if(fieldEmpty==false)
		    {
				 $('.sideevent').append('<div class="complete_check sideeventCheck"></div>');
		         $('.sideevent').addClass('check_form');
		         
		         return false; 
		    }
		     $('.sideeventCheck').remove();
		     $('.sideevent').removeClass('check_form');
		
		});
	});	
		
/** ----------------- Function to get  Side Event  guests--------------------------------------------------**/

	function additionalGuest(id)
    {
	   
	  var eventTotalPrice=0;
	  var guestTotalPrice=0;
	  var totalPrice=$('#days_total_price').html();
	  var Totalguest=$('#additional_guest'+id).val(); 
	  var eventPrice=$('#event_price'+id).val(); 
	  var guestPrice=$('#guest_price'+id).val(); 
	  var registered_id=$('#registered_id').val();
	  var registrantId= $('#registrant_id').val();
	  var maxCompGuest=$('#max_comp_guest'+id).val();
	  var compGuestPrice=$('#comp_guest_price'+id).val();
	 
	  emptyGuest=true;
	  
	    if(Totalguest!='')
	    {
		     var additionTotal='0';
		     var adiGuest='0';
		     if(maxCompGuest!='' && maxCompGuest>=Totalguest)
		    {
				additionTotal=compGuestPrice*Totalguest;
				guestPrice=compGuestPrice;
				$('#comp_guest'+id).html(compGuestPrice);
			}
			else
			{
				if(Totalguest>maxCompGuest)
				{
					adiGuest=parseInt(Totalguest)-parseInt(maxCompGuest);
					additionTotal=compGuestPrice*maxCompGuest;
				}
				 $('#comp_guest'+id).html(guestPrice);
				 //adiGuest=Totalguest;	
			} 
			 
			  additionTotal=parseInt(additionTotal)+parseInt(guestPrice*adiGuest);
			  guestTotalPrice=additionTotal;
			 	
			 // guestTotalPrice=Totalguest*guestPrice;
			  $('#eventTotalPrice'+id).val(guestTotalPrice+parseInt(eventPrice));
			 // $('#side_event'+id).html(additionTotal);
			 
			
	    }
	    else
	    {
		   $('#eventTotalPrice'+id).val(eventPrice);
		   $('#side_event'+id).html('');
		  
	    }
	  
	  $(".event_total_price").each(function() {
			var value=$(this).val();
			if(value!='')
			{
			  eventTotalPrice=parseInt(eventTotalPrice)+parseInt(value);
		    }
			
	  }); 
	   
		$('#days_total_price').html(eventTotalPrice);
		
		 $('#total_amount').val(eventTotalPrice);
		 
			var fields='';
			var addguest=0;
			if($('#total_guest'+id).length > 0)
			{
				addguest=$('#total_guest'+id).val();
			}
			 
		    var guests=parseInt(Totalguest)-parseInt(addguest);
		   
			for(guest=1; guest<=guests; guest++)
			{
				fields=fields+'<div class="regist_mmm"><div class="sideSgestcont guestrow"><div class="paragraph">'+guest_label+'</div><div class="formoentform_container"><div class="position_R"> <input type="text" name="guest_first_name'+id+'[]" id="guest_first_name'+id+'" value="" placeholder="First Name*" class="guest_required"></div><div class="position_R"><input type="text" name="guest_surname'+id+'[]" id="guest_surname'+id+'" value="" placeholder="Surname*" class="guest_required"></div><div class="position_R"><div class="pull-left frontend_select"><select  class="selectpicker bla bla bli guest_required"  name="guest_dietary'+id+'[]"  id="guest_dietary'+id+'"><option class="selectitem" value="" >Dietary Requirements*</option>.'+eventDietary+'.</select> </div><div class="clearfix"></div></div><div class="position_R"><textarea name="guest_details'+id+'[]" placeholder="Details" class="details_filed"></textarea></div></div></div></div>'; 
			} 
			
			$('#guest_fields'+id).html(fields);
			$('.selectpicker').selectpicker(); 
		  
		  
		   $('.sideeventCheck').remove();
		   $('.sideevent').removeClass('check_form');
		   
		
			 var value='0'; 
			 $('select.guest_required').each(function() {
				  value=$(this).val();
			});
			$('input.guest_required').each(function() {
			    value=$(this).val();
		  });
		   if(value!='')
		    {
			     $('.sideevent').append('<div class="complete_check sideeventCheck"></div>');
			     $('.sideevent').addClass('check_form');
		    }

     var guests= $('#additional_guest'+id).val(); 
	
	 setTimeout(function(){ 
	
		var event_price= $('#eventTotalPrice'+id).val();
	
		var total_price= parseInt($('#total_amount').val())+parseInt($('#regis_total_amt').val());
		
		 if(registered_id>0 && id>0)
		 {
			
			 addsideevent=false;
			 $('#sideeventguest').html(guests+' '+additional_guest_label);
		     $('#sideeventprice').html(guestTotalPrice);
			 $('#sideeventguest'+registered_id).html(guests+' '+additional_guest_label);
		     $('#sideeventprice'+registered_id).html(guestTotalPrice);
			 $('#regi_total_amt'+registered_id).html(total_price);
			 $('#payment_total_amt'+registered_id).html(total_price); 
		 }
		 else if(id>0)
		 {
			 $('#sideeventguest').html(guests+' '+additional_guest_label);
		     $('#sideeventprice').html(guestTotalPrice);
			 $('#regi_total_amt').html(total_price);
			 $('#payment_total_amt').html(total_price);
			 //to show total amt
			  var payAmt=$('#regis_pay_amount').val();
			  var calAmt=parseInt(payAmt)+parseInt(total_price);
			  $('#pay_amount').val(calAmt);
			  $('.pay_amt').html(calAmt);
			 
		 }
	  
	 }, 800); 
 
	 
    }
 

	  
/** ----------------- Function for Side Event --------------------------------------------------**/

function selectSideEvent(id)
{
      
	 addsideevent=true;	
	 var check=true;  
	 var eventTotalPrice=''; 
     var id = $('#sideeventcheck'+id).val();
	 var eventPrice=$('#event_price'+id).val(); 
	 var totalPrice=$('#days_total_price').val();
	 var registered_id=$('#registered_id').val();

    if($('#sideeventcheck'+id).hasClass('readonly'))
    {
	   $('.sideeventcheck'+id).removeClass('bgminus17');
	   $('.sideeventcheck'+id).addClass('bgzero'); 
	   custom_popup('Sideevent limit has been completed.',false)
	   return false;
    }

	 if($('#sideeventcheck'+id).hasClass('bgminus17'))
	 {
		
	     hidearea('#inner_wraaper'+id);
	     eventTotalPrice=$('#eventTotalPrice'+id).val();
	     totalPrice=parseInt(totalPrice)-parseInt(eventTotalPrice);
	     $('#eventTotalPrice'+id).val('0');
	   
	     $('.sideeventcheck'+id).addClass('bgzero');
	     $('.sideeventcheck'+id).removeClass('bgminus17');
		$('.editsideevent'+registered_id+id).remove();
	  
	     check=false;
	 }
	 else
	 {
		
		 showarea('#inner_wraaper'+id);
		 guestspinner(); 
		
		 totalPrice=parseInt(totalPrice)+parseInt(eventTotalPrice);
	
		 $('#eventTotalPrice'+id).val(eventPrice);
		
		 $('.sideeventcheck'+id).removeClass('bgzero');
		 $('.sideeventcheck'+id).addClass('bgminus17');
			 
			
	 }
	   
	 var dayTotalPrice=0;
	 eventTotalPrice= $('#eventTotalPrice'+id).val();
	 
	$(".event_total_price").each(function() {
			var value=$(this).val();
		   if(value!='')
		   {
			 dayTotalPrice=parseInt(dayTotalPrice)+parseInt(value);
		   }
			
	  });
	
     $('#days_total_price').html(dayTotalPrice);

     $('#total_amount').val(dayTotalPrice);
  
     setTimeout(function(){ 
     var data='';
     
     $('.sideevent_check').each(function(){
	   if($(this).hasClass('bgminus17'))
	    {
			 var value= $(this).val();
			 
			if(data!='' && value!='')
			{
				data=data+','+value;
			}
			else if(value!='')
			{
				data=value;
			}
			$('#select_side_event').val(data);
			
	    } 
     });
       
	if(data!='')
	{		
		$('.guest_required').each(function() {
			
			data=$(this).val();
			 //this condition for checking selecting box is blur
			if($(this).hasClass('bootstrap-select')){
				var getVal = $(this).find('button').attr('title');
				if(getVal=='Dietary Requirements*'){
					  data='';
				}else{
					 data=getVal;
				}
			}
		});
	}
	   
        if(!$('.sideevent').hasClass('check_form'))
		{
			 //to add form check
			 $('.sideevent').append('<div class="complete_check sideeventCheck"></div>');
			 $('.sideevent').addClass('check_form');
		}
		 if(data=='')
		 {
			 //to remove form check
			 $('.sideeventCheck').remove();
			 $('.sideevent').removeClass('check_form');
		   
		 }
  }, 200);

    if(check==false)
	{
		 $('.sidepayment'+id).remove();
	}
	else
	{
        setTimeout(function(){ 

		var registrantId= $('#registrant_id').val();
		var event_price= $('#eventTotalPrice'+id).val();
	    var guests=0;
	    
		var total_price= parseInt($('#total_amount').val())+parseInt($('#regis_total_amt').val());
		
	    if($('#additional_guest'+id).length > 0){
			guests=$('#additional_guest'+id).val(); 
		}
		
		
	      
		 $.ajax({
			  type: "POST",
			  url: base_url+'frontend/sideeventpayment',
			  data: 'select_sideevent='+id+'&registrantId='+registrantId+'&guests='+guests+'&event_price='+event_price,
				success: function(data) {
					
				 if(registered_id>0)
				 {
					
					 $('.editside'+registered_id).remove(); 
					 $('.addsideevent_regi'+registered_id).before(data); 
				     $('#payment_total_amt'+registered_id).html(total_price);
					 $('.sideeventrow'+registered_id).after(data);
					return false;
				 }
				 else if(id>0)
				 {
					
					$('#sideeventPayment').before(data); 
				    $('#payment_total_amt').html(total_price);
				     //to show total amt
					  var payAmt=$('#regis_pay_amount').val();
						
					   var calAmt=parseInt(payAmt)+parseInt(total_price);
					   $('#pay_amount').val(calAmt);
					   $('.pay_amt').html(calAmt);
					   
			     }
				 
				}
		  }); 
	  
	 }, 400);
    }
 
	if(emptyGuest==true)
	{
		
	   $('.sideeventCheck').remove();
	   $('.sideevent').removeClass('check_form');
	}
}	


 /** ----------function to get sideevetn and breakout form values  by registrant id --------------------------**/
  
    function SelectRegistrant(id)
	{
	    
	   	$('#regis_select_id').val(id);
	    $('#daywise_id').val('');
	    if($('.select_registration'+id).hasClass('readonly'))
	    {
		   $('.select_registration'+id).removeClass('bgminus17');
		   $('.select_registration'+id).addClass('bgzero');
		   custom_popup('Sorry! Registrant limit has been excceded.',false)
		   return false;
	    }
	 
	    var response=checkRegistrationPass(id);
	    //prevId!=id
	   if(prevId!=0  && response==true)
	    {
		    //this message from lang file
			var message=change_regis_type_msg;
			
		    // to create custom popupbox by js
		    bootbox.confirm({
				title: 'Confirm',
				message: message,
				buttons: {
					'cancel': {
						label: cancel_msg,
						className: 'btn-default custombtn'
					},
					'confirm': {
						label: ok_msg,
						className: 'btn-danger custombtn confirm',
						
					}
				},
				callback: function(result) {
					if (result) {
						  
						  //set single day registrant
						 $('#singleday_regis_id').val('');  
						 //to remove check personal form
						 $('.personal_check').remove();
		                 $('.regi_form').removeClass('check_form');
		                 
						 $('.select_day'+prevId).removeClass('bgminus17 cursor_none');
						 $('.select_day'+prevId).addClass('bgzero');
						
						 $('.check_day'+prevId).removeClass('bgminus17 cursor_none');
						 $('.check_day'+prevId).addClass('bgzero');
						
						 $('.select_registration'+prevId).removeClass('bgminus17 cursor_none');
						 $('.select_registration'+prevId).addClass('bgzero');
						
						 $('.select_registration'+id).addClass('bgminus17 cursor_none');
						 $('.select_registration'+id).removeClass('bgzero');
					 
						 $('.regis_check').remove('complete_check');
						 $('.registrant_form').removeClass('check_form');
					 
						 $('.registrantpayment').remove();
						 $('.sidepayment').remove();
						
						 $('.paymenttotal').remove();
						
						 $('.select_registration'+prevId).attr("enabled",true); 
						 $('#select_side_event').val('');
						 
						 //to unselect single day registrant
						$('.select_day'+id).addClass('bgzero');
						$('.select_day'+id).removeClass('bgminus17 cursor_none');
						
						$('.check_day'+id).addClass('bgzero');
						$('.check_day'+id).removeClass('bgminus17 cursor_none');
						
						 prevId=id;
						 
						 // get value of sidevent form 
						if(prevId!='0' || registered_id=='')
						{
							
						   $.ajax({
							  type: "POST",
							  url: baseUrl+'frontend/createsideeventform',
							  data: 'registrantId='+id,
								success: function(data) {
								   
								 $('#total_amount').val('0');
								 $('#days_total_price').html('0');	
								 $('#sideeventContent').html(data);	
								 
									$('.sideevent_check').screwDefaultButtons({
										image: 'url("'+IMAGE+'frontendcheck.png")',
										width: 17,
										height: 17,
									});
								}
							  });
						}
						 getFormValues(id);
					}
					else
					{
						$('.select_registration'+id).removeClass('bgminus17 cursor_none');
			            $('.select_registration'+id).addClass('bgzero');     
					}
					
				},
				
				
			}).find("div.modal-header").addClass('frnt_bg_header'); 
		
	    }
	   else if(response==true)
	   {
		 // check it aleray checked  
		 var registered_id=$('#registered_id').val();
		 $('.select_registration'+id).addClass('bgminus17 cursor_none');
		 $('.select_registration'+id).removeClass('bgzero'); 
		 $("#select_registration"+id).removeClass("select_registration");
		// get value of sidevent form 
		if(prevId!='0' || registered_id=='')
		{
			
		   $.ajax({
			  type: "POST",
			  url: baseUrl+'frontend/createsideeventform',
			  data: 'registrantId='+id,
				success: function(data) {
				   
				 $('#total_amount').val('0');
				 $('#days_total_price').html('0');	
				 $('#sideeventContent').html(data);	
				 
					$('.sideevent_check').screwDefaultButtons({
						image: 'url("'+IMAGE+'frontendcheck.png")',
						width: 17,
						height: 17,
					});
				}
			  });
		}
	      getFormValues(id);
	      prevId=id;
	   }
	 
	    
    }   
    /******* This funtion used to open registration pass popup******/

     function checkRegistrationPass(id)
     {
		 var registered_id=$('#registered_id').val();
		if(!$('.select_registration'+id).hasClass('bgminus17'))
	    {
			if($('.select_registration'+id).hasClass('required'))
			{
				var pass= $('#pass_registrant').val();
				var regisId=$('#pass_registrant_id').val();
				if(pass=='' || regisId!=id)
				{
					$('#registrant_password_popup').modal({ backdrop: 'static', keyboard: true });	
					$('.select_registration'+id).removeClass('bgminus17');
					$('.select_registration'+id).addClass('bgzero');
					$('#pass_registrant_id').val(id);
					$('.select_registration'+id).removeClass('bgminus17');
					$('.select_registration'+id).addClass('bgzero'); 
					return false;
				}
				else
				{
					 return true;
				}
			}
		}
	    return true;
	 
	 }
    /** ----------function to get forms values  by registrant id **/
    
    function getFormValues(id)
    {    
		 
		 var registered_id=$('#registered_id').val();		 
		 $('#regis_total_amt').val($('#regis_amt'+id).val());
	   
	   	 var total_amt=$('#regis_total_amt').val();
	     
	   	 var totalrow='<tr class="headingrow paymenttotal breakoutPayment"><th class="text-right">'+sub_total_label+'</th><th>$<span id="payment_total_amt">'+total_amt+'</span></th><th></th></tr>';
	     
		 if(total_amt>0 || id>0)
		 {
			 // to get selected registrant for payment page
			 $('.registrant_form').append('<div class="complete_check regis_check"></div>');
			 $('.registrant_form').addClass('check_form');
				$.ajax({
				  type: "POST",
				  url: base_url+'frontend/registrantpayment',
				  data: 'registrantId='+id+'&total_amt='+total_amt+'&registered_id='+registered_id,
					success: function(data) {
					
					    if(id>0)
					    {
						   $('#registrant_id').val(id);	
						   if(registered_id>0)
						    {
							    	
							   $('.editsideevent'+registered_id).remove();
							   $('#registered_user').html('');
							   $('.editside'+registered_id).remove();
							   $('.editregi'+registered_id).remove();
							 
							   $('#total_amt'+registered_id).html(total_amt);
							   $('#payment_total_amt'+registered_id).html(total_amt);
							   
							   $('.regi'+registered_id).after(data);
							   $('.regiscollapse').addClass('in');
						    }
						   else
						    {
							  
							   var payAmt=$('#regis_pay_amount').val();
							  
								 var calAmt=parseInt(payAmt)+parseInt(total_amt);
								 $('#pay_amount').val(calAmt);
								 $('.pay_amt').html(calAmt);
							     $('.registrantpayment').remove();
							     $('.paymenttotal').remove();
							     
					             $('#registrantPayment').after(data);
					             $('#subtotalrow').before(totalrow); 
					        }
					     
					    }
					   
					}
			  });
		 }
		if(id!='')
		{ 
		
			// get value of personal registrant form
			   $.ajax({
			  type: "POST",
			  url: base_url+'frontend/standardregister',
			  data: 'registeredId='+registered_id+'&registrantId='+id,
				success: function(data) {
					$('#form_fileds').html(data);
					$(".efformessage").hide();
					$('.selectpicker').selectpicker(); 
					//to add country flag
				    $(".codeFlag").intlTelInput({defaultCountry:'au'});
			
				}
			  });
			
			    
		}
		 $('#regis_total_amt').val($('#regis_amt'+id).val());
		 
		var total=0;
		setTimeout(function() {	 
	        total=$('#regis_total_amt').val();
	  
	        if(total>0)
	        {
		       $('#regis_amount').html(total);
		    }
		    else
		    {
				$('#regis_amount').html('0');
			}
	     }, 300);     
	}
	
	function dayRegistration(id,dayid)
	{
		
		var registered_id=$('#registered_id').val();
		if(!$('.select_day'+id).hasClass("bgminus17") && dayid!='')
		{
			 $('.checked'+dayid).removeClass('bgminus17');
			 $('.checked'+dayid).addClass('bgzero'); 
			 
			$('.check_day'+id).attr(':checked',false);  
			custom_popup(single_day_msg,false);
			return false;
		}
		
	   if($('.checked'+dayid).hasClass('readonly'))
	   {
		  
		   $('.checked'+dayid).removeClass('bgminus17');
		   $('.checked'+dayid).addClass('bgzero'); 
		   custom_popup(day_limit_msg,false)
		   return false;
	   }
	
		if($('.checked'+dayid).hasClass('bgminus17'))
		{
		  
		  $('.checked'+dayid).removeClass('bgminus17');
		  $('.checked'+dayid).addClass('bgzero'); 
		}
		else if(dayid!='0')
		{
	
		    $('.checked'+dayid).removeClass('bgzero'); 
		    $('.checked'+dayid).addClass('bgminus17');
		}
	  
		var total_amt=0;
		setTimeout(function() {	
			
		 var dayId='';  	
         var count=0;
        
		 $('.check_day'+id).each(function(){
			    
				if($(this).hasClass("bgminus17"))
				{
					
					value= $(this).val();
				
					if(count!=0 && value!='')
					{
					  dayId=dayId+','+value;	 
				    } 
				    else if(value!='')
				    {
						dayId=value;
						count+=1;
					} 
				   
				}
			});
			
		  
			//form check 
			if(count>0)
			{   
				 //to remove 
				 $('.select_registration'+id).removeClass('bgminus17 cursor_none');
		         $('.select_registration'+id).addClass('bgzero'); 
				 $('#pay_single_day').html('single day registration.');
				 $('#registrant_id').val(id); 
				 $('.registrant_form').append('<div class="complete_check regis_check"></div>');
		         $('.registrant_form').addClass('check_form');
				 // get value of personal registrant form
				 
		  
			}
			else
			{
			   $('.regis_check').remove();	
		       $('.registrant_form').removeClass('check_form');
			}
			  var  total=0;
			 
			  $('#daywise_id').val(dayId);
			  $('.check_day'+id).each(function(){
				 
				 var regisId=$(this).val();
				
				if(regisId>0 && $(this).hasClass('bgminus17'))
				{
					   
				   if(!$(this).hasClass('select_registration'+id))
				   {	 
						var amt=$('#day_amt'+regisId).val();
				    	total=parseInt(total)+ parseInt(amt);	
				   }
		       }
	      });
	        
	        total_amt=total;
	         
			  var totalamt=total_amt;
				
			  var sideevent_total=$('#total_amount').val();
				 
			   if(sideevent_total>0)
			   {
				   totalamt=parseInt(sideevent_total)+parseInt(total_amt);
			   }
			 
			 if(total_amt==0)
			{
				total_amt='0';
				
			}  
			if(registered_id>0)
			{
				if(dayid>0)
				{
				  	
				   $('.editsideevent'+registered_id).remove();
				   $('#payment_total_amt'+registered_id).html(total_amt);
			    }
			   
			     $('#pay_single_day'+registered_id).html('single day registration.');
				 $('#regis_total_amt').val(total_amt);
			     $('#regis_amount').html(total_amt);
				 $('#regis_payment_amt'+registered_id).html(total_amt);
				
				 
			}
			else
			{
				
				 $('#regis_total_amt').val(total_amt);
				 $('#regis_amount').html(total_amt);
				 $('.payment_total_amt').html(total_amt);
				 $('#payment_total_amt').html(totalamt);
				 $('.pay_day_price').html(totalamt);
				 //to show total amt
			      var payAmt=$('#regis_pay_amount').val();
			      
					var calAmt=parseInt(payAmt);
					if(totalamt>0)
					{
					  calAmt=parseInt(payAmt)+parseInt(totalamt);
					}
					 $('#pay_amount').val(calAmt);
					 $('.pay_amt').html(calAmt);
					
			}
			  
			  
			 $('.sidepayment').remove();
			 $('#select_side_event').val('');
			 var daywise_id=$('#daywise_id').val();
			if(dayid!='0' || registered_id=='')
			{ 
			   // get value of sidevent form 
			    $.ajax({
				  type: "POST",
				  url: base_url+'frontend/createsideeventform',
				  data: 'registrantId='+id+'&regisDayId='+daywise_id,
					success: function(data) {
					
					 $('#total_amount').val('0')
			         $('#days_total_price').html('0');	
					 $('#sideeventContent').html(data);	
						$('.sideevent_check').screwDefaultButtons({
							image: 'url("'+IMAGE+'frontendcheck.png")',
							width: 17,
							height: 17,
						});
					}
				  }); 
			 }
		
	  }, 800);
	  
	   
		   
 }
   var check=0;
   function SingleDay(id)
   {
	    var registered_id=$('#registered_id').val();
	    
	    if($('.select_day'+id).hasClass('readonly'))
	    {
		   $('.select_day'+id).removeClass('bgminus17');
		   $('.select_day'+id).addClass('bgzero');
		   custom_popup('Sorry! Registrant limit has been excceded.',false)
		   return false;
	    }
	    
		if(prevId!='')
		{
			 var message=change_regis_type_msg;
			
		    // to create custom popupbox by js
		    bootbox.confirm({
				title: 'Confirm',
				message: message,
				buttons: {
					'cancel': {
						label: cancel_msg,
						className: 'btn-default custombtn'
					},
					'confirm': {
						label: ok_msg,
						className: 'btn-danger custombtn confirm',
						
					}
				},
				callback: function(result) {
					if (result) {
						
						 getFormValues(id);
						 $('.select_day'+id).addClass('bgminus17 cursor_none');
						 $('.select_day'+id).removeClass('bgzero');
						
						 $('.select_registration'+prevId).removeClass('bgminus17 cursor_none');
						 $('.select_registration'+prevId).addClass('bgzero');
						
						 $('#regis_total_amt').val('0'); 
						 $('#registrant_id').val(id);
						 $('#singleday_regis_id').val(id); 
						 $('#daywise_id').val('');
						 $('#regis_amount').html('0');
						 $('#sideeventContent').html('');	
						 $('#singleday_regis_id').val(id);	
						 
						 prevId=id;
						
					}
					else
					{
						 $('.select_day'+id).removeClass('bgminus17 cursor_none');
			             $('.select_day'+id).addClass('bgzero');
					     
					}
					
				},
				
				
			}).find("div.modal-header").addClass('frnt_bg_header'); 
			 
			
		}
		else
		{
			 getFormValues(id);
			 prevId=id;
			 $('#singleday_regis_id').val(id);	
			 $('#regis_amount').html('0');
			 $('#registrant_id').val(id);
			 $('#singleday_regis_id').val(id); 
			 $('#regis_total_amt').val('0'); 
			 $('.select_day'+id).addClass('bgminus17 cursor_none');
			 $('.select_day'+id).removeClass('bgzero');
			 $('#sideeventContent').html('');	
		}
		
	
   }
   
   /** -------------------------------- function to remove guest --------------------------------------**/
   
   function removeGuest(guestid)
   {
	  
	  var sideeventId=$('#guest_sideevent'+guestid).val();
	  var eventtotalprice=$('#eventTotalPrice'+sideeventId).val();
	  var eventprice=$('#event_price'+sideeventId).val();
	  var remainamt=parseInt(eventtotalprice)-parseInt(eventprice);
	  var totalguest=$('#additional_guest'+sideeventId).val();  
	  var remainGuest=parseInt(totalguest)-parseInt(1); 
	   $('#total_guest'+sideeventId).val(remainGuest);
	   
	    //to check guest
	    var guest='0';
	    $('.guest_required').each(function(){
	  
		var value=$(this).val();  
		if(!$(this).hasClass('btn-group'))
		{
			if(value=='')
			{   
				guest+=1;
			} 
		} 
	    }); 
	    if(guest!=0)
	    {
		   custom_popup(guest_required_msg,false);
		   return false;
	    }   
	   var message =delete_sure_msg;
		    // to create custom popupbox by js
		    bootbox.confirm({
				title: 'Confirm',
				message: message,
				buttons: {
					'cancel': {
						label: cancel_msg,
						className: 'btn-default custombtn'
					},
					'confirm': {
						label: ok_msg,
						className: 'btn-danger custombtn confirm',
						
					}
				},
				callback: function(result) {
					if (result) {
						
						$.ajax({
						  type: "POST",
						  url: base_url+'frontend/removeguest',
						  data: 'guestId='+guestid+'&remainamt='+remainamt,
							success: function(msg) {
							 if(msg==true)
							 {
								 $('#eventTotalPrice'+sideeventId).val(remainamt);  
								 $('#additional_guest'+sideeventId).val(remainGuest)
								 $('#additional_guest'+sideeventId).attr('min',remainGuest);
								 $('.guestrow'+guestid).remove();
								 additionalGuest(sideeventId);
								
							 }
						  }
					  }); 
					}
					
					
				},
			}).find("div.modal-header").addClass('frnt_bg_header'); 
	
   }
   
    /**
	 * Function to calculate total amount
	 */	
	 
	  function getTotalAmount()
	  {
		   var regiAmt=$('#regis_total_amt').val();
		   var sideeventAmt=$('#total_amount').val();
		  	$.ajax({
			  type: "POST",
			  url: base_url+'frontend/calculatetotalamount',
			  data: 'regisAmt='+regiAmt+'&sideeventAmt='+sideeventAmt,
				success: function(msg) {
				
			  }
		  }); 
	  }
   
   /**
	 * Function for spiner
	 */		
  function guestspinner()
  {
    
    $(function() {
			
	var spinner = $(".guestspinner").spinner();$( "#disable" ).click(function()
	 { if ( spinner.spinner( "option", "disabled" ) ) 
	 { spinner.spinner( "enable" );}
	  else{spinner.spinner( "disable" );} });$( "#destroy" ).click(function() {
		        if ( spinner.data( "ui-spinner" ) ) {spinner.spinner( "destroy" );} else {spinner.spinner();}});$( "#getvalue" ).click(function()
				 {alert( spinner.spinner( "value" ) );}); $( "#setvalue" ).click(function() {spinner.spinner( "value", 5 );}); $( "button" ).button();});  
 
 }

/** ------------------------ function to get selected side event ----------------------------------------------- **/

	function getSelectedSideevent(regiid)
	{
	    var total_amt='0';
		setTimeout(function() {	 
			 
		  $('.event_total_price').each(function(){ 
			  var value=$(this).val();
			  if(value>0)
			  {
				  total_amt=parseInt(total_amt)+parseInt(value);
			  }
			  
		  });
		  if(total_amt>0)
		  {
			 $('#total_amount').val(total_amt)
			 $('#days_total_price').html(total_amt); 
		  }
		   
		  guestspinner();
		  $('.sideevent_check').screwDefaultButtons({
			image: 'url("'+IMAGE+'frontendcheck.png")',
			width: 17,
			height: 17,
		}); 
		
		  $('.sideevent_check').each(function(){
			  if($(this).hasClass('bgminus17'))
			  {
				  var value= $(this).val();
				  if(selectId!='' && value!='')
				  {
					  selectId=selectId+','+value;
				  }
				  else if(value!='')
				  {
					  selectId=value;
				  }
				   $('#select_side_event').val(selectId);
				 
			  } 
		  });
			
	    }, 400);
		
	}
	
	
	
$(document).ready(function() {	

	$(document).on('click','.breakout_readonly', function() {
		
		custom_popup(breakout_limit_msg, false);
		return false;
	
	});
});
	
/** ------------function to breakout selection -----------------------------------------------------**/

$(document).ready(function() {	

	$(document).on('click','.breakoutradio', function() {
     
	 var streamSessionId='';
	 var breakoutId='';
	 var id=$(this).find('input.breakoutradio').val();
   
	 var registered_id=$('#registered_id').val();
	 if($('.breakout'+id).hasClass('bgminus17'))
		{
			$('.breakout'+id).removeClass('bgminus17');
			$('.breakout'+id).addClass('bgzero'); 
			$('.breakoutrow'+id).remove();
		}
		else
		{
			$('.breakout'+id).removeClass('bgzero'); 
			$('.breakout'+id).addClass('bgminus17'); 
		}
     $('.breakout_row').remove();		
	 var breakoutData='';
	 $('input.breakoutradio').each(function()
	 {
	     var value=$(this).val();
	    if($(this).hasClass('bgminus17'))
	    {
			if(value!='')
			{
				if(streamSessionId!='')
				{
					streamSessionId=streamSessionId+','+value;
				}
				else
				{
					streamSessionId=value;
				}	
			}
			var fieldId=$(this).val();
			var data=$('#breakout_data'+fieldId).val();
		    breakoutData=breakoutData+'<tr class="breakoutrow'+id+' breakout_row"><td>'+data+'</td><td></td><td>&nbsp;</td></tr>'; 
			
		}
	});
	  // $('#stream_session_id').val(breakoutId);
	   $('#stream_session_id').val(streamSessionId);
	
		if(registered_id>0)
		{
			$('.editbreakout'+registered_id).before(breakoutData);	
		}
		else
		{
			$('#breakoutrow').before(breakoutData);	
		}
	  

		if(streamSessionId!='')
		{
			$('.breakoutform').append('<div class="complete_check breakoutCheck"></div>');
			$('.breakoutform').addClass('check_form');
			return false;
		} 
		$('.breakoutCheck').remove();
		$('.breakoutform').removeClass('check_form');
    
    });

});

	function getSelectedBreakout()
	{
		
		 var streamSessionId='';
		 $('input.breakoutradio').each(function(){
			  
			var value=$(this).val();
		    if($('#breakout'+id).is(':checked'))
		    { 
				if(value!='')
				{
					if(streamSessionId!='')
					{
						streamSessionId=streamSessionId+','+value;
					}
					else
					{
						streamSessionId=value;
					}
				}
			}
		});
		 
		   $('#stream_session_id').val(streamSessionId); 	
		   
	}

/** -----------------------------function for user login ------------------------------------------------**/
   $(document).ready(function(){ 
	   
	$("#loginform").submit(function(event) {
		
	//to check validation for fields
	var validate=checkValidateFormField();
    if(validate==true)
    {  
		var fromData=$("#loginform").serialize();
		var url = baseUrl+'frontend/userLogin';
		
		$.post(url,fromData, function(data) {
		  if(data){
			   var status = parseInt(data.status);
			   hide_popup("add_login_form_field_popup");
				  
				   if(status==1){
						  
						 $('.logout').show();
						 $('.add_login_form_field').hide(); 
						 refreshPge();
						
					}else{
						   
						   custom_popup(data.msg,false);	
					}
			}
		},"json");
	}
	return false;	
	
  });
   
});

/** function to select sponsor */

   function selectSponsor(id)
   {
	
	 $('#sponsor_id').val(id);
   
	   var total=0;
		
		total=$('#select_sponsor'+id).val();
		if(total>0)
		{
		   $('#sponsor_amount').html(total);
		}
		else
		{
			$('#sponsor_amount').html('0');
		}
	    getsponsorbooth(id);
    
    }
    
    /** to get sponsor booths **/
    
    function getsponsorbooth(sponsorId)
    {
         
		  $.ajax({
			  type: "POST",
			  url: baseUrl+'sponsor/getsponsorbooth',
			  data: 'sponsorId='+sponsorId,
				success: function(data) {
				   
					$('#event_booth').html(data);
					$('input:checkbox').screwDefaultButtons({
							image: 'url("'+IMAGE+'frontendcheck.png")',
							width: 17,
							height: 17,
						});
					
	                 $('#booth_popup_field').modal({ backdrop: 'static', keyboard: true });	
	                 
	                 /** to get selected booth **/	
	                 $( ".sponsor_booth" ).click(function() {
		                 
		                 var boothId=''; 
		                 $('.sponsor_booth').each(function(){ 
							 
							   if ($(this).is(':checked'))
							   {
									var value=$(this).val();
									if(value!='')
									{
										if(boothId!='')
										{ 
										  boothId=boothId+',' ;
									    }
									     
								         boothId=boothId+value;
								    }
								}
					     });
					     
					     $('#selected_booths').val(boothId);
		                 $('#booth_sponsor_id').val(sponsorId);
		                 
		                  $("#booth_save_btn").click(function() {
							 savesponsorbooth();
							});
	                 });
			  }
		  });
	}
    
   /** -----------------------function to submit sponsor booth form -----------------------------------------------**/
     
   function savesponsorbooth()
    {
		
		var spoonsorId=$('#booth_sponsor_id').val();
		var boothId= $('#selected_booths').val();
		var limit_package=$('#limit_package').val();
		
		 $.ajax({
			  type: "POST",
			  url: baseUrl+'sponsor/savesponsorbooth',
			  data: 'spoonsorId='+spoonsorId+'&boothId='+boothId,
				success: function(data) {
				 
			         refreshPge();
				}
		  });
	}
	
	/** function to logout */
	$(document).ready(function(){
		$('.logout').click(function(){
    
	     $.ajax({
			  type: "POST",
			   url: baseUrl+'frontend/logout',
				success: function(data) {
					 
					  refreshPge();
				}
		  });
       });
       
	});
     
   /** -----------------------------------function to check user login or not -------------------------------**/ 	
    $(document).ready(function(){ 
		
     $(".step2").click(function(){
		
		var userid=$('#user_id').val();
		if(userid>0)
		{	
	      return true;
	    }
	      custom_popup('Please login.',false);	
	      return false;
	 }); 
	 
	 /**function to show popup */

	$(".add_login_form_field").click(function(){
		//$("#loginform").trigger('reset');
		$('#add_login_form_field_popup').modal({ backdrop: 'static', keyboard: true });
	});
	
	
	});	
	

/**-----------------------function for selected booth position for exhibitor----------------------------------------------------------------**/

  $(document).ready(function(){ 
	
	$('.selectbooth').click(function(){
		
		var boothId=$(this).next('input').val();
		var boothposition=$('#booth_position'+boothId).val();
		var user_id=$('#user_id').val();
		var password_required=$('#password_required').val();
		var subject_to_approval=$('#subject_to_approval').val();
		var reqstatus=$('#reqstatus').val();
		var userreq=$('#userreq').val();
		var verify_pass = $("#is_password_enter").val();
		var userselectionlimit = $("#userselectionlimit").val();
		var limit_package = $("#limit_package").val();
		var benifit = $("#benifit").val();
		
		//var verify_pass ;
		
		$('#current_booth_id').val(boothId);
		
		//to check pending request
		if(limit_package >= userselectionlimit){}
				if($('.booth'+boothId).hasClass('pendingbooth'))
			{
				custom_popup(booth_pending_msg,false);
				return false;
			}
					
			if($('.booth'+boothId).hasClass('slectedbooth'))
			{	
			   $('.back_btn').show();
			   $('.continueSelectedBooth').hide();		    
			}
			else if(limit_package >= userselectionlimit)
			{	
				$('.back_btn').hide();
				$('.continueSelectedBooth').show();
			} 
						
		if(user_id>0)
		{  	
			if(limit_package <= userselectionlimit){
				var msg = 'you can not purchase more booths';
							
							custom_popup(msg,false);
							return false;						
				}
			else {
			
				// if request is send then this condition will be true
				if(userreq == 1)
				{
						
						
						if(reqstatus==0){
							var msg = 'request pending';
							
							custom_popup(msg,false);
							return false;
							}
						if(reqstatus==1){
							
							//$('#add_password_form_field_popup').modal({ backdrop: 'static', keyboard: true });
							
										
								if(password_required == 1)
								{	
									if(verify_pass == 1)
											{
											  $(this).addClass('slectedbooth'); 
											  $('#booth_popup').modal({ backdrop: 'static', keyboard: true });
											  $('#select_booth_position').html(boothposition);
											 
											} else{	
											   $('#add_password_form_field_popup').modal({ backdrop: 'static', keyboard: true });
											}
								}	
										else{
											  $(this).addClass('slectedbooth'); 
											  $('#booth_popup').modal({ backdrop: 'static', keyboard: true });
											  $('#select_booth_position').html(boothposition);			 
											}
							
							
							
							return true;
							}
						if(reqstatus==2){
							var msg = 'request cencle';
							custom_popup(msg,false);
							return false;
						} 
				}							
							
			// if request is not send then this condition will be true		
			if(userreq == 0){
				if(subject_to_approval == 1) 
				
				{
					$('#add_password_request_form_popup').modal({ backdrop: 'static', keyboard: true });
					
					}
				else  if(subject_to_approval == 0){
					
						if(password_required == 1)
								{	
									if(verify_pass == 1)
											{
											  $(this).addClass('slectedbooth'); 
											  $('#booth_popup').modal({ backdrop: 'static', keyboard: true });
											  $('#select_booth_position').html(boothposition);
											 
											} else{	
											   $('#add_password_form_field_popup').modal({ backdrop: 'static', keyboard: true });
											}
								}	
						else {
						  $(this).addClass('slectedbooth'); 
						  $('#booth_popup').modal({ backdrop: 'static', keyboard: true });
						  $('#select_booth_position').html(boothposition);
							}
					}	
					
				else {

						//var verify_pass = $("#is_password_enter").val();			
								if(password_required == 1)
								{	
									if(verify_pass == 1)
											{
											  $(this).addClass('slectedbooth'); 
											  $('#booth_popup').modal({ backdrop: 'static', keyboard: true });
											  $('#select_booth_position').html(boothposition);
											 
											} else{	
											   $('#add_password_form_field_popup').modal({ backdrop: 'static', keyboard: true });
											}
								}	
										else{
											  $(this).addClass('slectedbooth'); 
											  $('#booth_popup').modal({ backdrop: 'static', keyboard: true });
											  $('#select_booth_position').html(boothposition);			 
											}
								
						} 
				}
				
			}
			}
		else{
			$('#add_login_form_field_popup').modal({ backdrop: 'static', keyboard: true });	
			}
			
		
		
		
	}); 
	
});	


/**-----------------------This function is used to check selected  exhibitor booth----------------------------------------------------------------**/
  $(document).ready(function(){ 
	
	$('.continueSelectedBooth').click(function(){
		
		 var selectedBooth='';
		$('.slectedbooth').each(function(){
			
			var boothId=$(this).next('input').val();
			if(boothId!='')
			{
				if(selectedBooth!='')
				{
					selectedBooth=selectedBooth+',';
				}
				selectedBooth=selectedBooth+boothId;
			}
		});
		
		 $('#selected_booth_id').val(selectedBooth);
	});
});

/**-----------------------This function is used to remove cancel exhibitor booth----------------------------------------------------------------**/
 
  $(document).ready(function(){ 
	$('.cancelSelectBooth').click(function(){
	
		var boothId=$('#current_booth_id').val();
		$('.booth'+boothId).removeClass('slectedbooth');
		
		var selectedBooth='';
		$('.slectedbooth').each(function(){
			
			var boothId=$(this).next('input').val();
			if(boothId!='')
			{
				if(selectedBooth!='')
				{
					selectedBooth=selectedBooth+',';
				}
				selectedBooth=selectedBooth+boothId;
			}
		});
		
		 $('#selected_booth_id').val(selectedBooth);
	});
	
	
  });	
	
/**-----------------------This function is used to save exhibitor booth----------------------------------------------------------------**/

  $(document).ready(function(){ 
	
	$('.saveExhibitorBooth').click(function(){
		var boothIds=$('#selected_booth_id').val();
		var currentbooth=$('#current_booth_id').val();
		var exhibitor_id=$('#exhibitor_id').val();
		if(currentbooth=='')
		{
			custom_popup(select_booth_msg, true);
			return false;
		}
	
		 $.ajax({
			   type: "POST",
			   url:  baseUrl+'exhibitor/saveexhibitorbooth',
			   data: 'boothIds='+boothIds+'&exhibitor_id='+exhibitor_id,
			   success: function(msg) {
				  
				 //to get json formate values
				 var obj = $.parseJSON(msg);
				 custom_popup(obj['msg'],obj['is_success']);
			  
			   }, 
			}); 
		
	});
});
		



/**-----------------------function for eaxhibitor login----------------------------------------------------------------**/

  $(document).ready(function(){ 
	
	$("#exhibitorlogin").submit(function( event ) {
      
    var fromData=$("#exhibitorlogin").serialize();
	var url = baseUrl+'exhibitor/exhibitorLogin';
	
	$.post(url,fromData, function(data) {
	  if(data){
		   var status = parseInt(data.status);
		   hide_popup("add_login_form_field_popup");
	        
	           if(status==1){
				      
				    
				     $('.logout').show();
		             
		              refreshPge();
					
				}else{
					   custom_popup(data.msg,false);	
				}
		}
	},"json");

		
	});
	
});	
	
/**-----------------------This function is used to check login form validation ----------------------------------------------------------------**/
	
  $(document).ready(function(){ 	
	  
	$('.loginrequired').blur(function(){
		
	    var value=$(this).val();
	    if(value=='')
		{
			$(this).next(".loginvalidate").show();
			return false;
		}
		else
		{
			$(this).next(".loginvalidate").hide();
			
		} 
	
	});	
});	
 
function checkValidateFormField()
{
	 var validate=true;
	$('.loginrequired').each(function(){
		
	    var value=$(this).val();
	    if(value=='')
		{
			
			$(this).next(".loginvalidate").show();
			validate=false;
		} 
	});	
	 return validate;
}
  /**---This function used for check single day registrant-- ***/

	function isDayRegistrant()
	{
		var registrant_id=$('#registrant_id').val();
		var singleDay=$('#singleday_regis_id').val(); 
	
		if(singleDay!='')
		{
		    var dayWiseId=$('#daywise_id').val();	
		    if(dayWiseId=='')
		    {
				 custom_popup('Please select day for this registrant',false);
		         return false;
			} 
		}
		return true;
	}


/**----------------function to submit  registrantTypeForm  for general user-----------------------------------------**/
	  
     $(document).ready(function(){ 
		
     $('#add_regis_btn').click(function(event){
		
        var registrant_id=$('#registrant_id').val();
        var guest=0; 
       
        if(registrant_id=='')
        {
		  custom_popup(select_regis_msg,false);
		  return false;
	    }  
	     //to check single day registrant
	  /*  var isDay=isDayRegistrant();
	    if(!isDay)
	    {
			return false;
		} */
	     
	   $('.guest_required').each(function(){
		  
			var value=$(this).val();  
			if(!$(this).hasClass('btn-group'))
			{
				if(value=='')
				{   
					guest+=1;
				} 
			} 
	   }); 
	     
	   if(guest!=0)
	   {
		   custom_popup(guest_required_msg,false);
		   return false;
	   }   
		var fromData=$("#registrantTypeForm").serialize();
		
		var url=baseUrl+'frontend/savestandardregister';
		$('.loadershow').show();
	    //$('.frontendsubmitbtn').attr("disabled", true);
	     
		$.post(url,fromData, function(data) {
		  if(data){
			        
					var status = parseInt(data.status);
					
					 $('.loadershow').hide();
				     $('.frontendsubmitbtn').removeAttr("disabled");
	             
					if(status==1)
					{
						refreshPge();	
						return false;
					}
					else
					{
				      custom_popup(data.msg,false);
				    }
					  
			}
		
		},"json");
		
	   return false;
	
    });
    
});	
    function updateRegistrantDetails()
    {
		
        //to update registrant 
        var registrant_id=$('#registrant_id').val();
        var guest=0; 
       
        if(registrant_id=='')
        {
		  custom_popup(select_regis_msg,false);
		  return false;
	    }  
	     //to check single day registrant
	     var isDay=isDayRegistrant();
	    if(!isDay)
	    {
			return false;
		}
	     
	   $('.guest_required').each(function(){
		  
			var value=$(this).val();  
			if(!$(this).hasClass('btn-group'))
			{
				if(value=='')
				{   
					guest+=1;
				} 
			} 
	   }); 
	  
	   if(guest!=0)
	   {
		   custom_popup(guest_required_msg,false);
		   return false;
	   }   
		var fromData=$("#registrantTypeForm").serialize();
		  
		$('.loadershow').show();
	    $('.frontendsubmitbtn').attr("disabled", true);
	     
	    url=baseUrl+'frontend/updatestandardregister';
		
		 
		$.post(url,fromData, function(data) {
		  if(data){
			         
					var status = parseInt(data.status);
					
					$('.loadershow').hide();
					$('.frontendsubmitbtn').removeAttr("disabled");
					
					if(status==0)
					{
						custom_popup(data.msg,false);
						return false;
					}
				    history.go(-1);
			}
		
		},"json");
		
	   return false;
	   
	}
/**-----------------------function for paypal payment----------------------------------------------------------------**/

					   
$(document).ready(function(){ 
	
  $('.registrantPayment').click(function(){

  
    var chequeOption=true;
    var amt=$('#pay_amount').val();
	if(amt==0)
	{
		custom_popup(amt_zero_msg,false);	
		return false;
	}	
	 //to check single day registrant
	var isDay=isDayRegistrant();
	if(!isDay)
	{
		return false;
	}
	 
	var paymentOption= $('input[name$="select_payment"]:checked').val();
	 $('#payment_by').val(paymentOption);
	 
	if(paymentOption=='cheque')
	{	
					
    	chequeOption=checkChequeFields();
	}
	if(paymentOption=='credit_card')
	{
		var exp_date=$('#expire_month').val()+$('#expire_year').val();
		$('#expire_month_year').val(exp_date);
		chequeOption=creditCardFieldsValidation();
	}
	if(chequeOption)
	{
		
		var message=pay_sure_msg+' '+paymentOption.replace("_", ' ')+'?';

		   // to create custom popupbox by js
				bootbox.confirm({
					title: 'Confirm',
					message: message,
					buttons: {
						'cancel': {
							label: cancel_msg,
							className: 'btn-default custombtn'
						},
						'confirm': {
							label: ok_msg,
							className: 'btn-danger custombtn confirm',
							
						}
					},
					callback: function(result) {
						if (result) 
						{
							
							var fromData=$("#registrantTypeForm").serialize();
							var url=baseUrl+'frontend/savestandardregister';
							$('.loadershow').show();
							$('.frontendsubmitbtn').attr("disabled", true);
							 
							$.post(url,fromData, function(data) {
							  if(data){
										  
										 var status = parseInt(data.status);
										 $('.loadershow').hide();
										 $('.frontendsubmitbtn').removeAttr("disabled");
							             
										if(status==1)
										{
											refreshPge();
										    return false;
										}
										custom_popup(data.msg,false);	  
								}
							
							},"json");
							
						}
						
					},
					
				}).find("div.modal-header").addClass('frnt_bg_header'); 
		}
		return false;
		
    }); 
   });
		
	
/** -----------------------------function for exhibitor login ------------------------------------------------**/
   $(document).ready(function(){ 
	   
	$("#exhibitorlogin").submit(function(event) {
		
	//to check validation for fields
	var validate=checkValidateFormField();
    if(validate==true)
    {
		  
		var fromData=$("#exhibitorlogin").serialize();
		var url = baseUrl+'exhibitor/exhibitorLogin';
		
		$.post(url,fromData, function(data) {
		  if(data){
			   var status = parseInt(data.status);
			   hide_popup("add_login_form_field_popup");
			
				   if(status==1){
						  
						 $('.logout').show(); 
						  refreshPge();
						
					}else{
						   
						   custom_popup(data.msg,false);	
					}
			}
		},"json");
	}
	return false;	
	
  });
   
});

/********----------------------function for exhibitor login -------------------------------*********/
  $(document).ready(function(){ 
	 
	$('.noPDFMSG').click(function(){
		custom_popup(pdf_plan_msg,false);	
	});
	
});	


/** -----------------------------function for sponsor login ------------------------------------------------**/
   $(document).ready(function(){ 
	   
	$("#sponsorlogin").submit(function(event) {
		
	//to check validation for fields

	var validate=checkValidateFormField();
    //if(validate==true)
  //  {
		 
		var fromData=$("#sponsorlogin").serialize();
		var url = baseUrl+'sponsor/sponsorLogin';
		 $('.loadershow').show();
		$.post(url,fromData, function(data) {
		  if(data){
			   var status = parseInt(data.status);
			  
			   hide_popup("add_login_form_field_popup");
				   
				   if(status==1){
						 refreshPge(); 
						 $('.loadershow').hide();
						 $('.logout').show();
						  
						
					}else{
						   
						   custom_popup(data.msg,false);	
					}
			}
		},"json");
	//}
	return false;	
	
  });
   
});
/**-----------------------This function is used to save sponsor booth---------------**********/

  $(document).ready(function(){ 
	
	$('.saveSponsorBooth').click(function(){
		var boothIds=$('#selected_booth_id').val();
		var currentbooth=$('#current_booth_id').val();
		var sponsor_id=$('#sponsor_id').val();
		var user_id=$('#user_id').val();
		var limit_package=$('#limit_package').val();
		
		/** get values of  selected additional purchase item **/		
		var chkvals = [];
		var chkvals = $('input:checkbox:checked').map(function(){ 
			return $(this).val(); 
				 });
				 						
		var chkvalue  = jQuery.makeArray(chkvals);
		
				
		if(currentbooth=='')
		{
			custom_popup('Please select booth.', true);
			return false;
		}
	
		 $.ajax({
			   type: "POST",
			   url:  baseUrl+'sponsor/savesponsorbooth',			  
			   data:{'id':chkvalue,'sponsor_id':sponsor_id,'boothIds':boothIds,'chkvals':chkvalue,'limit_package':limit_package},			   
			   success: function(msg) {

				 
				 //to get json formate values
				 var obj = $.parseJSON(msg);
				refreshPge();
				// custom_popup(obj['msg'],obj['is_success']);
				 
			   }, 
			}); 
		
	});
});
		
/** --------------------------This function is used for registration password ------------------------------------------------**/
   
   $(document).ready(function(){ 
	   
	$("#registrant_pass_form").submit(function(event) {
	
		var fromData=$("#registrant_pass_form").serialize();
		var url = baseUrl+'frontend/checkRegistrationPass';
		var regis_id=$('#regis_select_id').val();
		$('#regis_select_id').val('');
	  //to check validation for fields
	   var value=$('#registrant_password').val();
	    if(value=='')
		{
			$('#registrant_password').next(".regisValidate").show();
			return false;
		}
		else
		{
			$('#registrant_password').next(".regisValidate").hide();
		} 
	
		$.post(url,fromData, function(data) {
		  if(data){
			   var status = parseInt(data.status);
			   hide_popup("registrant_password_popup");
			   $('#registrant_password').val(''); 
				   if(status==1){
					   
					      $('#pass_registrant').val(value);
					      SelectRegistrant(regis_id);
						  //custom_popup(data.msg,true);  
						  return false;
						 
					}else{
					    $('#pass_registrant').val('');
					    custom_popup(data.msg,false);	
					}
					
			}
		},"json");
    return false;
  });
   
});

/** ------------------------ This function used to cheque payment option----------------------------------------------- **/
   
    $(document).ready(function(){ 
		
		$('.select_payment').click(function(){
			var value= $('input[name$="select_payment"]:checked').val();
			if(value=='cheque')
			{
			   showarea('#cheque_fields');
			   hidearea('.credit_card_field');
			   return true;
			}
			if(value=='credit_card')
			{
			   showarea('.credit_card_field');
			   hidearea('#cheque_fields');
			   return true;
			}
		    hidearea('.credit_card_field');
			hidearea('#cheque_fields');  
		});

   }); 
   /** ------------------------ This function used to check validation field for cheque----------------------------------------------- **/
   function checkChequeFields()
   {
	    var empty=false;
	    
	   $('.cheque_field').each(function(){
		   
		   var value=$(this).val();
		    if($('.efformessage').hasClass('chequeField')=='' && empty==false)
		    {	
				if(value=='')
				{
				   $(this).after('<label class="efformessage chequeField"><span class="errorarrow"></span>'+required_msg+'</label>');
				   empty=true;
				}
			}
			else
			{
				empty=true;
			}
		
	   });
	    
	   if(empty==true)
	   {
		   return false;
	   }
	   $('.chequeField').remove();
	   return true;
   }
   /** ------------------------ This function used blur for bank details----------------------------------------------- **/

    $(document).ready(function(){ 
	   
	$('.cheque_field').blur(function() {
		
		var value=$(this).val();
		if(value=='')
		{
		  $(this).after('<label class="efformessage chequeField"><span class="errorarrow"></span>'+required_msg+'</label>');
		  return false;
		}
		 $(this).next('.chequeField').remove();
		
	    });
	});
	
	  /** ------------------------ This function used  for credit card details fields validation----------------------------------------------- **/
     function creditCardFieldsValidation()
     {
		var empty=false;
		$('.creditCardField').each(function(){
			   
			var value=$(this).val();
			//this condition for checking selecting box is blur
			if($(this).hasClass('bootstrap-select')){
				var getVal = $(this).find('button').attr('title');
				if(getVal=='Please select card type*'){
					 value='';
				}else{
					 value=getVal;
				}
			}
			if($('.efformessage').hasClass('cardField')=='' && empty==false)
			{
				if(value=='')
				{
					 $(this).after('<label class="efformessage cardField"><span class="errorarrow"></span>'+required_msg+'</label>');
					 empty=true;
				}
			}
			else
			{
				 empty=true;
			}
	    });
	  
		if(empty==true)
		{
			return false;
		}
		 $('.cardFields').remove();
		 return true;
    } 
    
    
		$(document).ready(function(){ 
		   
		//$('.creditCardField').blur(function() {
		$(document).on('blur','.creditCardField',function() {	
			
				var value=$(this).val();
				
				if($(this).hasClass('bootstrap-select')){
				var getVal = $(this).find('button').attr('title');
				if(getVal=='Please select card type*'){
					 value='';
				}else{
					 value=getVal;
				}
			}

			if(!$(this).next('.efformessage').hasClass('cardField'))
			{
				if(value=='')
				{
				  $(this).after('<label class="efformessage cardField"><span class="errorarrow"></span>'+required_msg+'</label>');
				  return false;
				}
				 
			}
			if(value!='')
			{
			  $(this).next('.cardField').remove();
		    }

			});
		});
	   
	
	   /** ------------------------ This function used to show eventDate Expired message----------------------------------------------- **/
    
    $(document).ready(function(){ 
	   
	   $('.eventDateExpired').click(function(){
		   custom_popup('Sorry! Registration date has been expired for this event.',false);
		   
		   return false;
	   });
	   
     });
     
    /** ------------------------ This function used to show eventDate Expired message----------------------------------------------- **/
    $(document).ready(function(){ 
	   
	   $('.term_condition').click(function(){
		   
		    if($('#term_condition').is(':checked',true))
		    {
				open_model_popup('termConditionPopup');
			}
			
		    return false;
	   });
	   
	 $('.noTermCondition').click(function(){
	   
		 custom_popup('Sorry! No document file available  for this event.');
		return false;
     });
	   
     });
     
     function doFrontendAjaxRequest(postUrl,sendData,dataSetId,selectedId){
			
		var url = baseUrl+postUrl;
		var returnData = 'first';
		$.ajax({
			type:'POST',
			data:sendData,
			url: url,
			//dataType: 'json',
			cache: false,
			beforeSend: function( ) {
				//open loader
				//loaderopen();
			},
			success: function(data){	
				//check data 
			    
				$("."+dataSetId).html(data);
				$('.selectpicker').selectpicker(); 
					/*if(selectedId   != undefined) {	
						$("#"+dataSetId).val(selectedId);  
					}  */
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				//hide loader
				//loaderclose();
				
				//open error message
				//custom_popup(ajaxErrorMsg,false);
			}
		});
	}
	
/**--------------------------frontend side event section ----------------------------------------------**/
    
   
   $(document).ready(function(){ 
	   	  	
	$('.sideeventRadio').click(function(){
	
	    $('.sideeventRegis').html('');
	    hidearea('.inner_wraaper');
	   
		var id=$(this).next('input').val();
	    //to set side event id
	     $('#select_side_event').val(id);
	     var sideeventData= $('#sideevent_data'+id).val();
	     //remove payment fields
	     $('.registrantpayment').remove();  
	     
	      var paymentData='<td >'+sideeventData+'</td><td><span id="paySideevent">$0</span></td><td></td>';
		  $('#sideeventPayment').html(paymentData);	
		 showarea('#inner_wraaper'+id);
			
			$.ajax({
				  type: "POST",
				  url: baseUrl+'sideevent/getregistrants',
				  data: 'sideevent_id='+id,
					success: function(data) {
						$('#sideevent_registrant'+id).html(data); 
						guestspinner();
						loadCheckRadioButton();
					}
				});
		
		 return true;
	});
});


/**--------------This function used to select side event registrants ----------------------------------------------**/
    

 $(document).ready(function(){
	 
	 
	$(document).on('click','.selectRegistrant', function(){
		
		var regiId=$(this).next('input').val();
		var regisAmt= $('#regis_amt'+regiId).val();
	    var sideEventId= $('#select_side_event').val();
	    var regisName=$('#regisName'+regiId).val();
	    var sideRegisId=$('#sideeventRegisId'+regiId).val();
	    var sideEventAmt= $('#sideEventAmt'+sideRegisId).val();
	    var totalAmt=parseInt(regisAmt)+parseInt(sideEventAmt);
	    $('#select_sideeventRegisId').val(sideRegisId);
	    //to check guest
		var guest=$('#additional_guest'+sideRegisId).attr('max');
	      
	    $('#regis_select_id').val(regiId);
	     hidearea('.guestFieldData');
	    
	    //to reset amt
	    $('#days_total_price').html('0');
	    $('.sideeventAmt'+sideEventId).html('0');	
	  
	    if(!$('.select_day'+regiId).is(':checked'))
		{
		     
		    if(guest>0)
			{
				showarea('.guestFiled'+regiId);
			}
	   
		    $('#registrant_id').val(regiId);
			//to show side event amt
			if(sideEventAmt>0)
			{
				$('.sideeventAmt'+sideEventId).html(sideEventAmt);	
			}   
			else
			{
				 $('.sideeventAmt'+sideEventId).html('0');	
			} 
			//to remove all day registrants
			$('.sideEventDayRegis').addClass('bgzero');
			
			//set registrant id
			$('#registrant_id').val(regiId);
			//to set all sideevent amt
			$('#sideEventAmt').val(sideEventAmt);
			$('#days_total_price').html(totalAmt);
		}
		else
		{
			 
			 $('#registrant_id').val('');
			 regisName=regisName+' Single Day Registration';
		}
		
		
		// get value of personal registrant form
			   $.ajax({
			  type: "POST",
			  url: base_url+'frontend/standardregister',
			  data: 'registeredId='+registered_id+'&registrantId='+regiId,
				success: function(data) {
					
					$('#form_fileds').html(data);
					
					$(".efformessage").hide();
					$('.selectpicker').selectpicker(); 
					//to add country flag
				    $(".codeFlag").intlTelInput({defaultCountry:'au'});
			
				}
			  });
			  
		//to calculate total amt
		
		var payTotal= parseInt($('#regis_pay_amount').val())+parseInt(totalAmt);
		$('#pay_amount').val(payTotal);	
		$('.pay_amt').html(payTotal);
		$('#paySideevent').html(sideEventAmt);
		  
			$('.registrantpayment').remove();  
		 var paymentRegis='<tr class="registrantpayment"><td><div class="spacer15"></div></td><td></td><td></td></tr><tr class="position_R registrantpayment"><td><div class="fsb">REGISTRANT</font></td><td></td><td></td> </tr>';
		     paymentRegis+='<tr class="registrantpayment"><td>'+regisName+'<span id="pay_single_day"></span></td><td>$<span class="pay_day_price">'+regisAmt+'</span></td><td></td></tr>';	 
			 paymentRegis+='<tr class="registrantpayment"><td><div class="spacer15"></div></td><td></td><td></td></tr>';
	         subtotalrow='<tr class="headingrow registrantpayment"><th class="text-right">SUB TOTAL</th><th>$<span id="payment_total_amt">500</span></th><th></th></tr>';
	        $('#paymentRegis').after(paymentRegis);
	        $('#subtotalrow').after(subtotalrow);
		
	}); 
	 

/**--------------This function used to select sideevent registrants personal form----------------------------------------------**/
 
	$(document).on('click','.selectSideEventRegistrant', function(){
		
		var regiId=$(this).next('input').val();
		var regisAmt= $('#regis_amt'+regiId).val();
	    var sideEventId= $('#select_side_event').val();
	    var regisName=$('#regisName'+regiId).val();
	    var sideRegisId=$('#sideeventRegisId'+regiId).val();
	    var sideEventAmt= $('#sideEventAmt'+sideRegisId).val();
	    var totalAmt=parseInt(regisAmt)+parseInt(sideEventAmt);
	    $('#select_sideeventRegisId').val(sideRegisId);
	    //to check guest
		var guest=$('#additional_guest'+sideRegisId).attr('max');
	      
	    $('#regis_select_id').val(regiId);
	     hidearea('.guestFieldData');
	    
	    //to reset amt
	    $('#days_total_price').html('0');
	    $('.sideeventAmt'+sideEventId).html('0');	
	  
	    if(!$('.select_day'+regiId).is(':checked'))
		{
		     
		    if(guest>0)
			{
				showarea('.guestFiled'+regiId);
			}
	   
		    $('#registrant_id').val(regiId);
			//to show side event amt
			if(sideEventAmt>0)
			{
				$('.sideeventAmt'+sideEventId).html(sideEventAmt);	
			}   
			else
			{
				 $('.sideeventAmt'+sideEventId).html('0');	
			} 
			//to remove all day registrants
			$('.sideEventDayRegis').addClass('bgzero');
			
			//set registrant id
			$('#registrant_id').val(regiId);
			//to set all sideevent amt
			$('#sideEventAmt').val(sideEventAmt);
			$('#days_total_price').html(totalAmt);
		}
		else
		{
			 
			 $('#registrant_id').val('');
			 regisName=regisName+' Single Day Registration';
		}
		
		
		// get value of personal registrant form
			   $.ajax({
			  type: "POST",
			  url: base_url+'sideevent/standardregister',
			  data: 'registeredId='+registered_id+'&registrantId='+regiId,
				success: function(data) {
					
					$('#form_fileds').html(data);
					
					$(".efformessage").hide();
					$('.selectpicker').selectpicker(); 
					//to add country flag
				    $(".codeFlag").intlTelInput({defaultCountry:'au'});
			
				}
			  });
			  
		//to calculate total amt
		
		var payTotal= parseInt($('#regis_pay_amount').val())+parseInt(totalAmt);
		$('#pay_amount').val(payTotal);	
		$('.pay_amt').html(payTotal);
		$('#paySideevent').html(sideEventAmt);
		  
			$('.registrantpayment').remove();  
		 var paymentRegis='<tr class="registrantpayment"><td><div class="spacer15"></div></td><td></td><td></td></tr><tr class="position_R registrantpayment"><td><div class="fsb">REGISTRANT</font></td><td></td><td></td> </tr>';
		     paymentRegis+='<tr class="registrantpayment"><td>'+regisName+'<span id="pay_single_day"></span></td><td>$<span class="pay_day_price">'+regisAmt+'</span></td><td></td></tr>';	 
			 paymentRegis+='<tr class="registrantpayment"><td><div class="spacer15"></div></td><td></td><td></td></tr>';
	         subtotalrow='<tr class="headingrow registrantpayment"><th class="text-right">SUB TOTAL</th><th>$<span id="payment_total_amt">500</span></th><th></th></tr>';
	        $('#paymentRegis').after(paymentRegis);
	        $('#subtotalrow').after(subtotalrow);
		
	});
	
	
   
	$(document).on('click','.sideEventDayRegis', function(){
		
	
		//to get reigstrant id
		var regiId=$('#regis_select_id').val();
		var sideEventId= $('#select_side_event').val();
		
		//to check is select single day option
		if(!$('.select_day'+regiId).is(':checked'))
		{
			//$('.select_day'+regiId).attr(':checked',false);
				
			$(this).removeClass('bgminus17');
			$(this).addClass('bgzero');
			custom_popup('Please select single day registration option.',false);
			return false;	
		}
		
		if($(this).hasClass('bgminus17'))
		{
			$(this).removeClass('bgminus17');
			$(this).addClass('bgzero');
		}
		else
		{
			//to check checkbox
			$(this).addClass('bgminus17');
			$(this).removeClass('bgzero');
		}
		
	
	//to cal culate day amt
	setTimeout(function() {	 
		var dayRegisAmt='0';
		var dayTotalAmt='0';
		var dayId='';
		var sideeventAmt='0';
		var daySideeventRegisId='';
		
	   $('.check_day'+regiId).each(function(){
			var day_id= $(this).next('input').val();
			if($(this).hasClass("bgminus17"))
			{   
				var sideRegisId= $('#daySideRegistId'+day_id).val();
				
				var amt=$('#day_amt'+day_id).val();
				sideeventAmt=parseInt(sideeventAmt)+parseInt($('.sideEventDayAmt'+day_id).val());
				if(dayId!='')
				{
				  dayId=dayId+','+day_id;	
				  dayTotalAmt=parseInt(dayTotalAmt)+parseInt(amt);
				  dayRegisAmt=parseInt(dayRegisAmt)+parseInt(amt);
				  daySideeventRegisId=daySideeventRegisId+','+sideRegisId;
				} 
				else 
				{
					dayTotalAmt=parseInt(amt);
				    dayRegisAmt=amt;
					dayId=day_id;
					daySideeventRegisId=sideRegisId;
				} 
				
				showarea('.dayGuestFiled'+day_id);	
			}
			else
			{
				hidearea('.dayGuestFiled'+day_id);
				$('.guestFieldData'+day_id).html('');
			}
		});

		$('#select_daySideeventRegisId').val(daySideeventRegisId);
		//var sideDayId=$('#daySideRegistId'+dayId).val();
		//$('#daySideeventRegisId').val(sideDayId);
		//to add side event amt
		dayTotalAmt=parseInt(dayTotalAmt)+parseInt(sideeventAmt); 
       
		if(dayId=='')
		{
			$('#registrant_id').val(''); 
		}
		else
		{
			$('#registrant_id').val(regiId); 
		}
		 //to set day registrant id
		 $('#daywise_id').val(dayId);
		 
		 //to set amt of day registrants
		 $('#days_total_price').html(dayTotalAmt);
	     $('.sideeventAmt'+sideEventId).html(sideeventAmt);
		 //$('#regis_total_amt').val(dayAmt);
		
		 //to set side event amt
		 $('#sideEventAmt').val(sideeventAmt);
		 
		 //to set on payment page
		 $('.pay_day_price').html(dayRegisAmt);
		 
		 //to set registrant taotal amt on payment page
		 $('#payment_total_amt').html(dayTotalAmt);
		 $('#paySideevent').html(sideeventAmt);
		 
		 //to show all registrant total value
		 var payTotal= parseInt($('#regis_pay_amount').val())+parseInt(dayTotalAmt);
		 $('#pay_amount').val(payTotal);	
		 $('.pay_amt').html(payTotal);
		
		}, 200);
		//console.log(dd);
		 
		//var sideevetId=$(this).val();
		//alert(sideevetId);
		
		//select_day
		
		
	});
});



/** ----------------- Function to get  sideevent sectio guest-------------------------------------------------**/

	function sideEventAdditionalGuest(id)
    {
	  
	  var totalPrice=$('#days_total_price').html();
	  var registrantId= $('#registrant_id').val();
	  var regisAmt= $('#regis_amt'+registrantId).val();
	  var sideEventAmt=	$('#sideEventAmt'+id).val();
		
	  var guests=$('#additional_guest'+id).val(); 
	 
	  var guestPrice=$('#guest_price'+id).val();
	  
	  var fields='';
	  var totalSideEventAmt=parseInt(sideEventAmt)+parseInt(guestPrice)*parseInt(guests);

	  var totalAmt=parseInt(regisAmt)+parseInt(sideEventAmt)+parseInt(guestPrice)*parseInt(guests);  
		for(guest=1; guest<=guests; guest++)
		{
			 
			fields=fields+'<div class="regist_mmm sideevent_regis"><div class="sideSgestcont guestrow"><div class="paragraph">'+guest_label+'</div><div class="formoentform_container"><div class="position_R"> <input type="text" name="guest_first_name'+id+'[]" id="guest_first_name'+id+'" value="" placeholder="First Name*" class="guest_required"></div><div class="position_R"><input type="text" name="guest_surname'+id+'[]" id="guest_surname'+id+'" value="" placeholder="Surname*" class="guest_required"></div><div class="position_R"><div class="pull-left frontend_select"><select  class="selectpicker bla bla bli guest_required"  name="guest_dietary'+id+'[]"  id="guest_dietary'+id+'"><option class="selectitem" value="" >Dietary Requirements*</option>.'+eventDietary+'.</select> </div><div class="clearfix"></div></div><div class="position_R"><textarea name="guest_details'+id+'[]" placeholder="Details" class="details_filed"></textarea></div></div></div></div>'; 
		} 
		 
		$('#guest_fields'+id).html(fields);
		//to drop down list
        $('.selectpicker').selectpicker(); 
		
		$('#days_total_price').html(totalAmt);
		 //to set side event amt
		$('#sideEventAmt').val(totalSideEventAmt);
		
		//to  calculate amt
		 $('#pay_amount').val(totalAmt);	
		 $('.pay_amt').html(totalAmt);
		
		 //to set registrant taotal amt on payment page
		 $('#payment_total_amt').html(totalAmt);
		 $('#paySideevent').html(totalSideEventAmt);
		
    }
    
    /**----------------function to submit  side event details-----------------------------------------**/
	  
     $(document).ready(function(){ 
		
     $('#add_sideevent_regis').click(function(event){
		 
        var registrant_id=$('#registrant_id').val();
        var guest=0; 
        
       if(registrant_id=='')
        {
		  custom_popup(select_regis_msg,false);
		  return false;
	    }  
	   
	   $('.guest_required').each(function(){
		  
			var value=$(this).val();  
			if(!$(this).hasClass('btn-group'))
			{
				if(value=='')
				{   
					guest+=1;
				} 
			} 
	   });  
	     
	   if(guest!=0)
	   {
		   custom_popup(guest_required_msg,false);
		   return false;
	   }   
		var fromData=$("#registrantTypeForm").serialize();
		
		var url=baseUrl+'sideevent/savestandardregister';
		$('.loadershow').show();
	    //$('.frontendsubmitbtn').attr("disabled", true);
	     
		$.post(url,fromData, function(data) {
		  if(data){
			        
					var status = parseInt(data.status);
					
					 $('.loadershow').hide();
				     $('.frontendsubmitbtn').removeAttr("disabled");
	             
					if(status==1)
					{
						refreshPge();	
					}
					else
					{
				      custom_popup(data.msg,false);
				    }
					  
			}
		
		},"json");
		
	   return false;
	
    });
    
});
 

	
	
	/**-----------------------function for side event registrant section paypal payment----------------------------------------------------------------**/

					   
$(document).ready(function(){ 
	
  $('.sideeventRegistrantPayment').click(function(){

    
    var chequeOption=true;
    var amt=$('#pay_amount').val();
	if(amt==0)
	{
		custom_popup(amt_zero_msg,false);	
		return false;
	}	

	var paymentOption= $('input[name$="select_payment"]:checked').val();
	 $('#payment_by').val(paymentOption);
	 
	if(paymentOption=='cheque')
	{	
					
    	chequeOption=checkChequeFields();
	}
	if(paymentOption=='credit_card')
	{
		var exp_date=$('#expire_month').val()+$('#expire_year').val();
		$('#expire_month_year').val(exp_date);
		chequeOption=creditCardFieldsValidation();
	}
	if(chequeOption)
	{
		
		var message=pay_sure_msg+' '+paymentOption.replace("_", ' ')+'?';

		   // to create custom popupbox by js
				bootbox.confirm({
					title: 'Confirm',
					message: message,
					buttons: {
						'cancel': {
							label: cancel_msg,
							className: 'btn-default custombtn'
						},
						'confirm': {
							label: ok_msg,
							className: 'btn-danger custombtn confirm',
							
						}
					},
					callback: function(result) {
						if (result) 
						{
							
							var fromData=$("#registrantTypeForm").serialize();
							
							var url=baseUrl+'sideevent/savestandardregister';
							$('.loadershow').show();
							$('.frontendsubmitbtn').attr("disabled", true);
							 
							$.post(url,fromData, function(data) {
							  if(data){
										  
										 var status = parseInt(data.status);
										 $('.loadershow').hide();
										 $('.frontendsubmitbtn').removeAttr("disabled");
							         
										if(status==1)
										{
											refreshPge();
										    return false;
										}
										custom_popup(data.msg,false);	  
								}
							
							},"json");
							
						}
						
					},
					
				}).find("div.modal-header").addClass('frnt_bg_header'); 
		}
		return false;
		
    }); 
   });
   
      /**--------------------------this function used to load check and radio bu in sideevent ----------------------------------------------**/ 

   
   $(document).ready(function(){ 
	   
	   $("#formSponsorPayment").submit(function(event) {
	    
	    var sendData = {"breakoutId":'1', "eventId":'1'};
	    //to check password
	    if($('.frontendsubmitbtn').hasClass('sponsorshipPayment'))
	    {
		   ajaxpopupopen('sponsorship_payment_popup','sponsor/sponsorshippayment',sendData,'sponsorshipPayment');
		   
		} 
		
		return false;
	 });
	  
	  
   
    });

	
   /**--------------------------this function used to load check and radio bu in sideevent ----------------------------------------------**/ 
    function loadCheckRadioButton()
    {
		$('input:checkbox').screwDefaultButtons({
			image: 'url("'+baseUrl+'templates/system/images/frontendcheck.png")',
			width: 17,
			height: 17,
		});
		$('input.selectRegistrant').screwDefaultButtons({
			image: 'url("'+baseUrl+'templates/system/images/foontendradio.png")',
			width: 17,
			height: 17,
		});
	} 
			      

 /** -----------------------------function for ceck sponsor password ------------------------------------------------**/
   $(document).ready(function(){ 
	   
	$("#sponsor_pass").submit(function(event) {
		
	
	//to check validation for fields


		var fromData=$("#sponsor_pass").serialize();
		
		var url = baseUrl+'sponsor/sponsorpass';
		
		 $('.loadershow').show();
		$.post(url,fromData, function(data) {
		  if(data){
			   var status = parseInt(data.status);
			   hide_popup("add_password_form_field_popup");
				  
				   if(status==1){
						  
						 $('.loadershow').hide();
						 // $('.logout').show();
						 		 
						  refreshPge();
						  
						  
						
					}else{
						   
						   custom_popup(data.msg,false);	
					}
			}
		},"json");
	
	return false;	
	
  });
   
});  

/**----------------------------------open password request form----------------------------------**/

 $(document).ready(function(){	
	$('.pass_request').click(function(){
		hide_popup("add_password_form_field_popup");
		$('#add_password_request_form_popup').modal({ backdrop: 'static', keyboard: true });		
		//to check pending request
		/* if($('.booth'+boothId).hasClass('pendingbooth'))
		{
			custom_popup(booth_pending_msg,false);
			return false;
		}
		if($('.booth'+boothId).hasClass('slectedbooth'))
		{
		   $('.back_btn').show();
		   $('.continueSelectedBooth').hide();
		    
	    }
	    else
	    {
			$('.back_btn').hide();
		    $('.continueSelectedBooth').show();
		}
		*/
		
		
			
		
		
		
	}); 
	
});	



/**---------------------------------request for new booth purchase password-------------------------**/

 $(document).ready(function(){ 
	   
	$("#sponsor_pass_request").submit(function(event) {
		
	
	//to check validation for fields

		var fromData=$("#sponsor_pass_request").serialize();			
		var url = baseUrl+'sponsor/sponsorpassrequest';		
		 $('.loadershow').show();
		$.post(url,fromData, function(data) {
		  if(data){
			   var status = parseInt(data.status);
			   hide_popup("add_password_request_form_popup");				  
				   if(status==1){						  
						 $('.loadershow').hide();
						 // $('.logout').show();						 		 
						  refreshPge();				  
						
					}else{
						   
						   custom_popup(data.msg,false);	
					}
			}
		},"json");
	
	return false;	
	
  });
   
}); 
 

	
	
$(document).ready(function(){	
 $("#registrantForm").submit(function( event ) {
    
        var check_email= email_avila_chk();
   
        if(check_email==false)
        {
		   return false;    
	    }
	   
		var fromData=$("#registrantForm").serialize();
		var url =baseUrl+'sponsor/savecontactperson';
		$('.loadershow').show();    
		$.post(url,fromData, function(data) {
			  if(data){
                 
				if(data.is_success=='true'){	
						  refreshPge();	
						  return false;
					}
					$('.loadershow').hide();
					if(data.is_success=='1')
					{
						custom_popup(data.msg,true);
					}
					else{
						custom_popup(data.msg,false);		
					}
				}
			},"json");
		return false;	 
	});
});


/** this code right now is not using for any purpose that's why it si commented **/


/**-----------------------function for side event registrant section paypal payment----------------------------------------------------------------**/

					   
$(document).ready(function(){ 
	
  $('.sponsorshipPayment').click(function(){

							
		var fromData=$("#formSponsorPersonal").serialize();
		var url=baseUrl+'sponsor/savessponsorpersonal';
		$('.loadershow').show();
		$('.frontendsubmitbtn').attr("disabled", true);
		
		$.post(url,fromData, function(data) {
		  if(data){
					  
					 var status = parseInt(data.status);
					 $('.loadershow').hide();
		
				 
					if(status==1)
					{
						refreshPge();
						return false;
					}
					custom_popup(data.msg,false);	  
			}
		
		},"json");
		
	
		return false;
		
    }); 
   });
   
 /**-----------------------for payment from payment section in sponsor section------------------**/
	
	/**-----------------------function for side event registrant section paypal payment----------------------------------------------------------------**/

					   
$(document).ready(function(){ 
	
  $('#sponssortypepaymentform').submit(function(){

	/*var isValidationActive = false;

	$(".required_field").each(function(){
		var getValue = $(this).val();
		if(getValue.trim()== ""){
			isValidationActive = true;
		}
	});
	
	if(isValidationActive){
		var msg = 'please fill personal detail form';
		  custom_popup(msg,false);
		 return false;
		
	}*/
	
		
	  //check got value if blank then set 0 otherwise set actual value
   var getValue1 = ($(this).find(':submit').attr('ispyament') !="")?$(this).find(':submit').attr('ispyament'):'0';		
   var getValue2 = ($(this).find(':submit').attr('isattachregist') !="")?$(this).find(':submit').attr('isattachregist'):'0';		
		//convert get value in integer
	var  ispyament		= parseInt(getValue1);
	var  isattachregist = parseInt(getValue2);
	
	//check for payment not allowed   
	if(ispyament == 0){
		  var msg = 'you can not proceed due to registratio include';
		  custom_popup(msg,false);
		  return false;
		}
		//check for payment if attachregistrant filled is blank for user personal detail form id
	if(isattachregist == 0){
		var msg = 'please fill personal detail before payment submit';
		  custom_popup(msg,false);
		  
		 // refreshPge();
		  return false;
		
		}  
	
	  
    	var fromData=$("#sponssortypepaymentform").serialize();
    	var url=baseUrl+'sponsor/sponssortypepayment';
		$('.loadershow').show();
	    //$('.frontendsubmitbtn').attr("disabled", true);
	     
		$.post(url,fromData, function(data) {
		  if(data){
			    
			    	var status = parseInt(data.status);
					
					//$('#add_password_form_field_popup').modal('hide');
					 $('.loadershow').hide();
				    // $('.frontendsubmitbtn').removeAttr("disabled");
	             
					if(status)
					{	refreshPge();
						//custom_popup(data.msg,false);
					}
					else
					{
				      custom_popup(data.msg,false);
				    }
					  
			} 
		
		},"json");
		
	   return false;
	 
    }); 
   });
   
   
 /** ------------------------ This function used to save benifits ----------------------------------------------- **/
   $(document).ready(function(){  
   $('.additem').click(function() {	
		 
		 var getCheckboxStaus	= $(this).find('input:checked').is(":checked");
		 var getFieldId			= $(this).find('input').val();
		 var sponsor_id			= $('#sponsor_id').val();
		 var user_id			= $('#user_id').val();
		 
		 console.log(getFieldId);
		 return false;
		 
		 
		 if(user_id==0){
			 return false;
			 }
		 if(getCheckboxStaus){
				var checkboxCheccked = 1;
			}else{
				var checkboxCheccked = 0;
			}	
		 	
			
			
		});
     });
/**----------------------------------------------*end*----------------------------------------------------------------**/
		 
  
	
    
