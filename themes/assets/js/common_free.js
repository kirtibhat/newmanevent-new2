﻿var  baseUrl, loadersaving, hidearea, showarea, loadersaved, isChanged, loadererror, open_model_popup, hide_popup, doLogin, ajaxdatasave,event_msg_custom_social_media_deleted, event_msg_type_registration_deleted,event_msg_custom_field_deleted, gstRate,registrantPerFrm,IMAGE;
$(document).ready(function(e) {
    /**
     * @description: all ready state function come in document ready function
     */

$("#eventSponsorsLogos").click(function() {
    if (this.checked) {
        $(".sponsors_log_div").slideToggle('slow');
    } else {
        $(".sponsors_log_div").slideToggle('slow');
    }
});

//call for event data save and media upload
ajaxdatasave('formGeneralEventSetup', 'event/eventdetails', false, true, false, false, true, '#showhideformdivgeneralSection', '#showhideformdivVenueSection');
ajaxdatasave('formVenueEventSetup', 'event/eventdetails', false, true, false, false, true, '#showhideformdivVenueSection', '#showhideformdivVenueContactPerson');

$( "#formVenueContactPerson" ).submit(function( event ){   
    var selectedId  = $(this).find(".phone_field_opt").find(".selected").attr('id');
    $("#input_hidden_"+selectedId).val($("#venueContactPhone").val());  

    ajaxdatasave('formVenueContactPerson', 'event/eventdetails', false, true, false, false, true, '#showhideformdivVenueContactPerson', '#showhideformdivpersonalContact');
});


//show and hide exhibition
$(".exhibitionSelection").click(function() {
    var getValue = parseInt($(this).val());
    if (getValue == 1) {
        $("#exhibition_field_div").slideDown('slow');
        $("#exhibitionNumber").attr('required', '');
    } else {
        $("#exhibition_field_div").slideUp('slow');
        $("#exhibitionNumber").removeAttr('required');
    }
});

// campare two date event end date greater than start date
//compare_greaterthan_date('eventStartDate','eventEndDate','event end date');

$("#eventEndDate").change(function() {
    var startdateval = $("#eventStartDate").val();
    var enddateval = $("#eventEndDate").val();
    var sDate = Date.parse(startdateval);
    var eDate2 = Date.parse(enddateval);
    if (sDate > eDate2) {
        $("#end_date_error").removeClass('dn');
        $("#eventEndDate").val('');
    } else {
        $("#end_date_error").addClass('dn');
    }
    return true;
});

//registerLastDate (Registration Cut Off date) greater than  or equal to start date
$("#registerLastDate").change(function() {
    var enddateval = $("#eventEndDate").val(); // end date
    var lastdateval = $("#registerLastDate").val(); //Registration Cut Off date
    var eDate = Date.parse(enddateval);
    var lDate = Date.parse(lastdateval); //Registration Cut Off date
    if (eDate < lDate) {
        $("#last_regis_date_error").removeClass('dn');
        $("#registerLastDate").val('');
    } else {
        $("#last_regis_date_error").addClass('dn');
    }
    return true;
});

$(document).click(function(){
    if( $(".mapvenue").is(":visible") ) {
        window.setTimeout(function(e){
            $("#map_canvas").css({"left": ($(".mapvenue").parents('.dashboard_panel').offset().left + $(".mapvenue").parents('.dashboard_panel').innerWidth()+50)} );
        },200);
    }else{}
});

$('.reset_location').click(function(){
   $('#eventVenue').val('');
   $('#eventVenueAddress1').val('');
   $('#eventVenueAddress2').val('');
   $('#eventVenueCity').val('');
   $('#eventVenueState').val('');
   $('#eventVenueZip').val('');
});

popupopen('viewLocationOnMap','location_popup_map');

var max_fields      = 10; //maximum input boxes allowed
var wrapper         = $(".input_fields_wrap"); //Fields wrapper
var add_button      = $(".add_field_button"); //Add button ID
var x = 1; //initlal text box count

 $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
    e.preventDefault(); $(this).parent('div').remove(); x--;
    $("#hidden_input_no").val((($("#hidden_input_no").val()*1)-1));
});

var IsEditCustomObj = null;
//edit custom contact
$(document).on("click", ".add_contact_type", function(e){
    formPostData = {contactType:'',contactDetails:''};
    //call ajax popup function
    ajaxpopupopen('custom_type','event/eventcustomcontactpopup',formPostData,'add_contact_type');
    
});

//edit custom contact
$(document).on("click", ".edit_contact_type", function(e){
    IsEditCustomObj = $(this).siblings();   
    var contactType   = $(this).attr('data-contactType') // undefined
    var contactDetails  = $(this).attr('data-contactDetails');
    contactType   = (contactType=='undefined') ? '' : contactType;
    contactDetails  = (contactDetails=='undefined') ? '' : contactDetails;
    formPostData = {contactType:contactType,contactDetails:contactDetails};
    //call ajax popup function
    ajaxpopupopen('custom_type','event/eventcustomcontactpopup',formPostData,'add_contact_type');
    
});

//edit custom contact
$(document).on("click", ".editeventcustomcontact", function(e){
    var contactId = $(this).attr('data-contact');
    formPostData = {};
    eventcustompopup('event/eventcustomcontactpopup/'+contactId,'custom_type',formPostData);
  
    $("#hidden_custom_contact_array").append('<input type="hidden" name="hidden_deleted_custom_contact[]" value="'+contactId+'" />');
    $(this).parent().parent('div').remove();
});

// delete custom contact
$(document).on("click", ".deleteeventcustomcontact", function(e){
  //$(".accordion_content").css("height",'auto');
  var contactId = $(this).attr('data-contact');
  //console.log($(this).parent().parent().parent('div').attr('class'));
  $(this).parent().parent().parent('div').remove();
});


$(document).on("click", "#add_custom_contact_button", function(e){
  
  // For set collapse hight
  $(".accordion_content").css("height",'auto');
  if($("#customContactType").parsley().isValid()){
    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
    var hidden_input_no = $("#hidden_input_no").val();  
    var contactType     = $.trim($("#ctype").val());
    var contactDetails  = $("#ctype_details").val();
    var contactType     = contactType.charAt(0).toUpperCase() + contactType.substring(1);
    
    if(IsEditCustomObj!=null){
      
      $(IsEditCustomObj).parent().find('a').attr('data-contactType',contactType);
      $(IsEditCustomObj).parent().find('a').attr('data-contactDetails',contactDetails);
        
      $(IsEditCustomObj).parent().siblings('.short_field').val(contactDetails);
      $(IsEditCustomObj).parent().parent().prev().find('label').html(contactType+'<input type="hidden" name="contactType[]" value="'+contactType+'">');
      IsEditCustomObj = null;
    }else{
 $(wrapper).append('<div class="row"><div class="col-3"><div class="labelDiv"><label class="form-label text-right" for="contactType">'+contactType+'<input type="hidden" value="'+contactType+'" name="contactType[]"  /></label></div></div><div class="col-5"><input  class="short_field medium_input pull-left" type="text" value="'+contactDetails+'" name="contactDetails[]" ><div class="editButtonGroup pull-left"><a class="eventsetup_btn td_btn edit_contact_type btn" href="javascript:void(0)" data-contactType="'+contactType+'" data-contactDetails="'+contactDetails+'"><span class="medium_icon"> <i class="icon-edit"></i> </span></a><a class="delete_btn td_btn deleteeventcustomcontact btn red" href="javascript:void(0)"><span class="medium_icon"> <i class="icon-delete"></i> </span></a></div></div></div>');
    }
    
    $('#custom_type').modal('hide');
    $("#hidden_input_no").val((($("#hidden_input_no").val()*1)+1));
    
      $("#customContactType")[0].reset();
  } 
   
   $('.parsley-errors-list').remove();
   var errorMsg ='<label id="ctype-error" class="error" for="ctype">This field required.</label>';
   $("#error-ctype").html(errorMsg);
   $("#ctype").addClass('error');
   var errorMsg1 ='<label id="ctype_details-error" class="error" for="ctype_details">This field required.</label>';
   $("#error-ctype_details").html(errorMsg1);
   $("#ctype_details").addClass('error');
  e.preventDefault();
  
});

$(document).on("click", ".contact_person", function(e){
    var email = $('#email').val();
    if(email.trim() !=''){
        $('#confirmEmail').attr('required','required');
    } else{
        $('#confirmEmail').removeAttr('required');
    }
});
	
  $( "#formContactPerson" ).submit(function( event ){   

    var selectedId  = $(this).find(".phone_field_opt").find(".selected").attr('id');
    $("#input_hidden_"+selectedId).val($("#mobile").val()); 
    
    var selectedId  = $(this).find(".phone_field_opt_1").find(".selected").attr('id');
    $("#input_hidden_"+selectedId).val($("#mobile2").val());  
    
    //save contact person details
    ajaxdatasave('formContactPerson','event/eventdetails',true,true,false,false,false,'#showhideformdivpersonalContact','#showhideformdivSocialMedia');
  });
  
    
  $(".event_contact_person").click(function(){
    var contactPerson = parseInt($(this).val());
    
    if(contactPerson==1){
      $.each(contact_userdata, function(index, val) {
        $("#"+index).val(val);
      });
    }else{
      //reset form
      resetSelectedFrom('formContactPerson');
    } 
  });
  
    //call for event data save and media upload
ajaxdatasave('formSocialMediaSetup', 'event/eventdetails',true,true,false,false,false,'#showhideformdivSocialMedia','#showhideformdivtermSection');

/*---This function is used to add and edit custome field for personal field -----*/
$(document).on('click','.event_social_media',function(){                                                
    var formAction = $(this).attr('formAction');

    var customfieldid = $(this).attr('customfieldid');
    var sendData = {"eventId":eventId, "customfieldid":customfieldid};

    //call ajax popup function
    ajaxpopupopen('custom_social_media','event/eventcustomsocialmediapopup ',sendData,'custom_social_media','');
});


//Save Extra Form Custome Field Via Ajax
$(document).on("click", "#saveCustomeSocialMediaType", function(e) {
    
    $(".accordion_content").css("height",'auto');
    
    //$("#formcustomSocialMediaType").parsley();
   
    if($("#formcustomSocialMediaType").parsley().isValid()){    
        //var parentFormId = data.id;
        var fromData=$("#formcustomSocialMediaType").serialize();
        var url = baseUrl+'event/saveCustomeSocialMediaType';
        //console.log(fromData); console.log(url); //return false;
        $.ajax({
            type:'POST',
            data:fromData,
            url: url,
            dataType: 'json',
            async: false,
            cache: false,
            beforeSend: function( ) {                
            },
            success: function(data){
                
                if(data.is_success=='true'){
                    var mediatypename  = $("#formcustomSocialMediaType #mediatypename").val();
                    var mediatypevalue = $("#formcustomSocialMediaType #mediatypevalue").val();
                    var fieldId = data.id; 
                    //console.log("mediatypename: "+mediatypename+"===mediatypevalue: "+mediatypevalue+"====fieldId: "+fieldId);
                    
                    var fieldHtml = '<div class="row" id="masterfieldlbl_'+fieldId+'"><div class="col-3"><div class="labelDiv"><label class="form-label text-right">'+capWords(mediatypename)+'</label></div></div><div class="col-5"><input type="text" name="" value="http://" disabled="disabled" class="xsmall_input trim_check pull-left" autocomplete="off"><input type="text" name="mediatypevalue[]" value="'+mediatypevalue+'" class="medium_input pull-left"><span class="editButtonGroup pull-left"><a class="eventsetup_btn btn event_social_media"  href="javascript:void(0)" formAction="bookerCustomFieldSave" customfieldid="'+fieldId+'"><span class="medium_icon"><i class="icon-edit"></i> </span></a><a href="javascript:void(0)" class="btn red delete_custom_social_media" deleteid="'+fieldId+'"><span class="medium_icon"><i class="icon-delete"></i></span></a></span></div></div>';
                    //console.log(fieldHtml);
                    //console.log(data.saveType);
                    if(data.saveType=='insert'){
                        $(".custom_field_div_social_media").append(fieldHtml);
                        $("#custom_social_media").modal('hide');
                    } else if(data.saveType=='update'){
                        $(".custom_field_div_social_media #masterfieldlbl_"+fieldId).html(fieldHtml);
                        $("#custom_social_media").modal('hide');
                    }
                }
                
                $('.parsley-errors-list').remove();
               var errorMsg ='<label id="mediatypename-error" class="error" for="mediatypename">This field required.</label>';
               $("#error-mediatypename").html(errorMsg);
               $("#mediatypename").addClass('error');
               var errorMsg1 ='<label id="mediatypevalue-error" class="error" for="mediatypevalue">This field required.</label>';
               $("#error-mediatypevalue").html(errorMsg1);
               $("#mediatypevalue").addClass('error');
                
                e.preventDefault();
                return false;
            }
        });        
    }     
});

/*---This function is used to delete custome field for personal field ----- */
    customconfirm('delete_custom_social_media','event/deleteCustomeSocialMediaType', '', '', '', true,'',true,event_msg_custom_social_media_deleted);

  
//call for term and condition data save and media upload
ajaxdatasave('formTermsCondition','event/eventdetails',true,true,false,false,false,'#showhideformdivtermSection','');  

$(document).on('click','.term_condi_submit',function(){
  
  var termcount   = 0; // set term attachment count
  //set refund count
  $('.attchadddedfileTerm').each(function(){
    termcount = 1;
  });
  
  
  //set value in field
  $("#termSelection").val(termcount);
    
});
 
 $('.resetInput').click(function(){
       $('#eventnTermsCondition').val('');
       //deleteUploadFiles();  
       //$('.attchadddedfileTerm').remove();
    });
    
 /*
  * add and editlogin type by popup
  */
$(".add_edit_login_type").click(function(){
  $("#formTypeofRegistration").trigger('reset');//reset form
  var getRegisId     = parseInt($(this).attr('id'));
  var getRegisTitle  = $(this).attr('registitle');
  var pageaction  = $(this).attr('pageaction');

  $("#invitationTypeId").val(getRegisId);
  $("#invitationName").val(getRegisTitle);


   $("#registrantTypeTitle").html(capWords(pageaction));
   if(pageaction=='duplicate') {
    $(".formInvitationType").attr('id','formInvitationTypeDuplicate');
    }
  //regis_action
  
  //open add registrant type form and set formdivid and clickclass
  $("#ajax_open_popup_html").attr('openbox','add_type_login_popup');  
  $("#ajax_open_popup_html").attr('clickbox','add_edit_login_type');  
  open_model_popup('add_type_login_popup');
});

//save type of registration
ajaxdatasave('formInvitationType','event/addediteventInvitation',false,true,true,false,false,false,false,false);


/*
 * This section is used to show and hide div of 
 * registant details and types
 */

$(".ispasswordaccess").click(function(){
  // For set collapse hight
  $(".accordion_content").css("height",'auto');
  var getregisid=$(this).attr('passwordaccessid');
  var getval=parseInt($(this).val());
  //var getid=$(this).attr('checked');
  if(getval==1 ){
    $("#password_access_div"+getregisid).addClass('row');  
    showarea("#password_access_div"+getregisid);
  }else{
    hidearea("#password_access_div"+getregisid);
  }
});


//create duplidate type of registration
ajaxdatasave('formInvitationTypeDuplicate','event/duplicateeventinvitation',false,true,true,false,false,false,false,false);

//reset duplidate type of registration
popup_cancle_form('dup_cancel_form','formInvitationTypeDuplicate');

//delete type of registrant
customconfirm('delete_invitation','event/deleteeventinvitation', '', '', '', true,'',false,event_msg_type_registration_deleted);

/*---This function is used to add and edit custome field for personal field -----*/
$(document).on('click','.add_personal_form_field',function(){                                                
    
    //set value in temparray hidden field for if again open
    if($(this).attr('invitationid') !== undefined && $(this).attr('customfieldid') !== undefined ){  
        var parentFormId    = $(this).data('parent-form-id');
        var registrantid    = $(this).attr('invitationid');
        var customfieldid   = $(this).attr('customfieldid');
        var formAction      = $(this).attr('formAction');
        var formId          = $(this).attr('formId');
        var tempObj = registrantid+','+customfieldid;
        $("#tempHidden").val(tempObj);
    }else{
        var getTempData = $("#tempHidden").val();
        var getData     = getTempData.split(',');
        //get value from hidden field and set value
        registrantid    = getData[0];
        customfieldid   = getData[1];
    }

    var sendData = {"registrantid":registrantid, "customfieldid":customfieldid, "eventId":eventId,"formAction":formAction,"formId":formId, "parentFormId":parentFormId};
    //console.log(sendData);
    //call ajax popup function
    ajaxpopupopen('add_personal_form_field_popup','event/add_edit_custom_field_free',sendData,'add_personal_form_field','');
});

/*---This function is used to delete custome field for personal field -----*/
customconfirm('delete_personal_form_field','event/deleteeditcustomefield', '', '', '', true,'',true,event_msg_custom_field_deleted);

/*----This function is used to unit price enter and show-----*/
unitPriceManage('unitPriceEnter','registrantId','GSTInclude','totalPrice',gstRate, 'totalPriceIncGST');

/*----This function is used to unit price enter and show early bird-----*/
unitPriceManage('unitPriceEarlyBird','registrantId','earlyGSTInclude','earlyBirdPrice',gstRate, 'earlyBirdPriceIncGST');

/*----This function is used to day wise unit price enter and show early bird-----*/
unitPriceManage('dayUnitPriceEarlyBird','registrantDayId','registrantGSTInclude','registrantDayTotalPrice',gstRate,'registrantDayPriceIncGST');

/*----This function is used to early bird day wise unit price enter and show early bird-----*/
unitPriceManage('earlyDayUnitPriceEarlyBird','registrantDayId','registrantEarlybirdGSTInclude','registrantEarlybirdDayPrice',gstRate,'registrantEarlybirdDayPrice');


/*
*  This section is used to select custome field type select
*/
$(document).on('click','.fieldtype',function(){

  var getval = $(this).val(); 
  var customFieldId = parseInt($("#customFieldId").val());
  //if use add field then remove old data
  if(customFieldId==0){
    $("#selectedField").val('0');
    $(".default_value_list_personal").html('');
  }
  if(getval=="text" || getval=="file"){
    $("#show_dropdown_feature").hide();
  }else{
    $("#show_dropdown_feature").show();
    $("#custom_field_qty_personal").val('1');
    var fieldHtml = ' <div class="row clearFix"><div class="labelDiv pull-left"> <label class="form-label text-right" for="field_1">Field 1<!--<span aria-required="true" class="required">*</span>--></label> </div><input type="text" value="" name="defaultValue[]" id="defaultValue_0"  class="xxlarge_input trim_check" data-parsley-error-message = "'+common_field_required+'" data-parsley-error-class="custom_li"  data-parsley-trigger="keyup" /><span id="error-link_name"></span></div>';
   $(".default_value_list_personal").html(fieldHtml);
  }
});

/*
*  This section is used select option fields
*/   
$(document).on('click','.step-personal',function(){
  var qty = parseInt($("#custom_field_qty_personal").val());    
  if($(this).hasClass('up')){ // increment
    if(qty<10){
      var currentId = 'defaultValue_'+qty; 
      var currentLblId = 'defaultLblValue_'+qty; 
      $("#custom_field_qty_personal").val((qty*1)+1);    
      var fieldHtml = '<div class="row clearFix" id="'+currentLblId+'"><div class="labelDiv pull-left"><label class="form-label text-right" for="field_1">Field '+((qty*1)+1)+' <!--<span aria-required="true" class="required">*</span>--></label></div><input type="text"   class="xxlarge_input trim_check"  value="" data-parsley-error-message = "'+common_field_required+'" data-parsley-error-class="custom_li"  data-parsley-trigger="keyup" name="defaultValue[]" id="'+currentId+'"  />	<span id="error-link_name"></span> </div>';
      $(".default_value_list_personal").append(fieldHtml);
    }   
  }else{
    var currentId = 'defaultValue_'+((qty*1)-1); 
    var currentLblId = 'defaultLblValue_'+((qty*1)-1); 
    if(qty>1){
      
      $("#custom_field_qty_personal").val(((qty*1)-1)); 
      $("#"+currentId).remove();
      $("#"+currentLblId).remove();
    }  
  } 
});

/*
* This section is used to show and hide div of 
* registant details and types
*/
$(".comaplimentary").click(function(){
    // For set collapse hight
    $(".accordion_content").css("height",'auto');
    var registrantid=$(this).data('registrantid');
    if($(this).val()==1){
        hidearea("#comaplimentaryDiv"+registrantid);
        $("#totalPrice"+registrantid).removeAttr('required');
        $("#totalPrice"+registrantid).attr('data-parsley-required', 'false');               
    } else if($(this).val()==0){
        $("#totalPrice"+registrantid).attr('required','');
        showarea("#comaplimentaryDiv"+registrantid);                      
    }
});


//form add and edit custome field save
//ajaxdatasave('formAddCustomeField','event/addeditcustomefieldsave',false,true,true,false,false,false,false,false);

//Save Extra Form Custome Field Via Ajax
$(document).on("click", ".saveCustomeField", function(e) {
    $(".accordion_content").css("height",'auto');
    
    if($("#formAddCustomeField").parsley().isValid()){
        //var parentFormId = data.id;
        var fromData        =   $("#formAddCustomeField").serialize();            
        var parentFormId    =   $("#formAddCustomeField #parentFormId").val();
        var formId          =   $("#formAddCustomeField #formId").val();
       
        var url = baseUrl+'event/addeditcustomefieldsavefree';
        $.ajax({
            type:'POST',
            data:fromData,
            url: url,
            dataType: 'json',
            async: false,
            cache: false,
            beforeSend: function( ) {
            },
            success: function(data){
                
                if(data.is_success=='true'){
                    var fieldName = $("#formAddCustomeField #fieldName").val();
                    var fieldId = data.id;
                    var selectarr = '<span class="select-wrapper"><div class="customSelectWrapper"><select name="field_'+fieldId+'" id="field_'+fieldId+'" class="custom-select medium_select hasCustomSelect" style="-webkit-appearance: menulist-button; width: 175px; position: absolute; opacity: 0; height: 28px; font-size: 15px;">';
                    $.each(data.fieldsmastervalue, function(index, val) {
                        var selectindex = (index=="1") ? "selected='selected'" : "";
                        selectarr+= '<option value="'+index+'" '+selectindex+'>'+val+'</option>';;
                    });     
                    selectarr += '</select></div><div class="customSelectWrapper" style="width: auto;"><span class="customSelect custom-select medium_select" style="display: inline-block;"><span class="customSelectInner" style="width: 125px; display: inline-block;">Displayed</span></span></div></span>'; 
                   
                    var fieldHtml = ' <div class="row" id="masterformregistrant_'+fieldId+'"><div class="col-3"><div class="labelDiv"><label class="form-label text-right" id="masterfieldlbl_'+fieldId+'">'+capWords(fieldName)+'</label></div></div><div class="col-5">';
                    fieldHtml += '<input type="hidden" name="fieldid[]" value="'+fieldId+'">'+selectarr;
                    fieldHtml += '<div class="editButtonGroup pull-left"><a class="btn add_personal_form_field" invitationid="0" formAction="bookerCustomFieldSave" formId="'+formId+'" customfieldid="'+fieldId+'" data-parent-form-id="'+parentFormId+'"><span class="medium_icon"> <i class="icon-edit"></i> </span></a>';
                    fieldHtml += '<a class="btn red delete_personal_form_field " deleteid="'+fieldId+'"><span class="medium_icon"> <i class="icon-delete"></i></span></a>'; 
                    fieldHtml += '<span id="error-dinner_dance6"></span></div> </div></div>';
                
                    if(data.saveType=='insert'){
                        $("#"+parentFormId+" .custom_field_div").append(fieldHtml);
                        $("#add_personal_form_field_popup").modal('hide');
                    } else if(data.saveType=='update'){
                        $("#"+parentFormId+" #masterfieldlbl_"+fieldId).html(capWords(fieldName));
                        $("#add_personal_form_field_popup").modal('hide');
                    }
                    
                    
                    //$('.custom-select').customSelect();
                    
                    e.preventDefault();
                    return false;
                }
            }
        });
    }
});


//Save dietary Custom Field Via Ajax
$(document).on("click", ".saveDietaryCustomeField", function(e) {
    $(".accordion_content").css("height",'auto');

    if($("#formAddTypeofDietary").parsley().isValid()){
        //var parentFormId = data.id;
        
        var fromData=$("#formAddTypeofDietary").serialize();            
        var url = baseUrl+'event/addeventdietaryfree';
        $.ajax({
            type:'POST',
            data:fromData,
            url: url,
            dataType: 'json',
            async: false,
            cache: false,
            beforeSend: function( ) {
            },
            success: function(data){
                if(data.is_success=='true'){
                    var fieldName = $("#formAddTypeofDietary #dietaryName").val();
                    var fieldId = data.id;
                    var fieldHtml = ' <div class="col-2 mL35" id="row_'+fieldId+'"> <div class="inputFileWrapper"> <div class="crosslable"><span>'+fieldName+'</span><span id="'+fieldId+'"  class="small_icon delete_dietary"><img src="'+baseUrl+'themes/assets/images/crox.png"></span></div> </div> </div>';
                    $(".requirement_ul").append(fieldHtml);
                    $("#add_type_dietary_popup").modal('hide');
                    $('#formAddTypeofDietary').trigger("reset");
                    e.preventDefault();
                    return false;
                } else{
                    return false;
                }
             }
          });
    }
});


$(document).on("change", ".temp_cls", function(e) {
    $(this).next('.holder').text($(this).find(":selected").text());
});

popupopen('add_dietary_type','add_type_dietary_popup');
    
//form Add Type of Dietary
//ajaxdatasave('formAddTypeofDietary','event/addeventdietary',false,true,true,false,false,false,false,false);
    
// add dietary type by popup
$(document).on('click','.delete_dietary', function(){
    
    var deleteid = $(this).attr('id');
    //$('#row_'+deleteid).fadeOut('slow');
    jQuery('#row_'+deleteid).css('opacity', '0.4');
    //jQuery('#row_'+deleteid).css('opacity') = '0.6';

    var fromData={"deleteId":deleteid};
    var url = baseUrl+'event/deletedietarytype';
    $.post(url,fromData, function(data) {
          if(data){
                if(data.is_success=='true'){
                        //custom_popup(data.msg,true);  
                        $("#row_"+deleteid).remove();
                }
            }
        },"json");
});

//Filter Invitation Types

$(document).on('keyup', '.filterinvitation', function(){
var filter = $(this).val().trim().toLowerCase();
    filter =filter.replace(/ /g,"_");
    FilterCustomers(filter);
});
	
function FilterCustomers(filter) {
	if (filter == "") {
		$('.accordion_content').show();	
		$('.filter_credentials').css('display','block');
        $('.filter_credentials').each(function(){
            if($(this).last().is(':visible')){
                $('.credential_filter_div').removeClass('pb_30');
            }else{
                $('.credential_filter_div').addClass('pb_30');
            }
            $('.filter_credentials').on('click', function(){
                if ($(this).parent().find('div.sabPanel:visible:last').attr('id') == $(this).attr('id') && $(this).hasClass('open')){
                    $('.credential_filter_div').removeClass('pb_30');
                } else {
                    $('.credential_filter_div').removeClass('pb_30');
                }
            });
		});
	} else {
		$('.filter_credentials').each(function(){
			var values_div_main = $(this).attr('lang');
			if ( $(this).find('.showHideTitle'+values_div_main).attr('lang').toLowerCase().indexOf(filter) >= 0 ) {
				$('.checkdata_'+values_div_main).removeClass('open');
				$('.sabPanel_contentWrapper_find_'+values_div_main).css('display','none');
				$('.checkdata_'+values_div_main).show();
				$('.filter_credentials').on('click', function(){
					if ($(this).parent().find('div.sabPanel:visible:last').attr('id') == $(this).attr('id') && $(this).hasClass('open')){
						$('.credential_filter_div').removeClass('pb_30');
					} else {
						$('.credential_filter_div').addClass('pb_30');
					}
				});
			} else {
				$('.sabPanel_contentWrapper_find_'+values_div_main).css('display','none');
				$('.checkdata_'+values_div_main).hide();
			}
		});
		$('.filter_credentials').each(function(){
			if($(this).last().is(':visible')){
				$('.credential_filter_div').removeClass('pb_30');
			}else{
				$('.credential_filter_div').addClass('pb_30');
			}
		});
	}
}
 

//personal details save
ajaxdatasave('formPersonalDetailsregisDetails','event/invitations',true,true,false,false,false,'#showhideformdivperosnal_regisDetails','#showhideformdivdieteryrequirement');
ajaxdatasave('formbookerdetails','event/invitations',true,true,false,false,false,'#showhideformdivbooker_details','#showhideformdivperosnal_regisDetails');


/*
*  This section is used select option fields
*/
    
$(document).on('click','.stepper-step',function(){
    $(".default_value_list").html('');
    var getval = parseInt($("#custom_field_qty").val());    
    var addField = '';
    for(i=1;i<=getval;i++){
        addField+='<label class="pull-left" for="field_1">Field '+i+'</label>';
        addField+='<input type="text" value="" id="defaultValue_'+i+'" name="defaultValue[]" class="small" required="" data-parsley-error-class = "custom_li"  data-parsley-trigger="keyup" >';
        
        $(".default_value_list").append(addField);
        addField = '';
    }
});

/*
 *  This section is used to select custome field type select
*/
    
$(document).on('click','.fieldtype',function(){
    var getval = $(this).val(); 
    var customFieldId = parseInt($("#customFieldId").val());
    //if use add field then remove old data
    if(customFieldId==0){
        $("#selectedField").val('1');
        $(".default_value_list").html('<label class="pull-left" for="field_1">Field 1</label><input type="text" value="" id="defaultValue_1" name="defaultValue[]" class="small" required="" data-parsley-error-class = "custom_li"  data-parsley-trigger="keyup" >');
    }
    if(getval=="text" || getval=="file"){
        $("#show_dropdown_feature").hide();
    }else{
        $("#show_dropdown_feature").show();
    }
});

/*
* add and editlogin type by popup
*/
$(".customize_theme_popup").click(function(){

var is_theme_used     = parseInt($(this).attr('is_theme_used_data'));
$('#is_theme_used_data').val(is_theme_used);
var theme_name     = $(this).attr('theme_name');
var theme_type     = $(this).attr('theme_type');
var sendData = {"theme_name":theme_name,"is_theme_used":is_theme_used,"theme_type":theme_type};
ajaxpopupopen('customize_theme_popup','event/use_custom_theme_apply',sendData,'customize_theme_popup','');
});

/*---This function is used to add and edit custome field for personal field -----*/
$(document).on('click','.theme_popup_zoom',function(){                                                   
var themeid     = parseInt($(this).attr('themeid'));
var eventid     = parseInt($(this).attr('eventid'));
var theme_name  = $(this).attr('theme_name');
var is_theme_used     = parseInt($(this).attr('is_theme_used'));
var event_theme_screen_bg = $('.event_theme_screen_bg'+themeid).val();
var event_theme_header_bg = $('.event_theme_header_bg'+themeid).val();
var event_theme_box_bg = $('.event_theme_box_bg'+themeid).val();
var event_theme_text_color = $('.event_theme_text_color'+themeid).val();
var event_theme_link_color = $('.event_theme_link_color'+themeid).val();
var event_theme_link_highlighted_color = $('.event_theme_link_highlighted_color'+themeid).val();
var font_package = $('#font_package'+themeid).val();

var sendData = {"eventId":eventid,"themeid":themeid,"event_theme_screen_bg":event_theme_screen_bg,"event_theme_header_bg":event_theme_header_bg,"event_theme_box_bg":event_theme_box_bg,"event_theme_text_color":event_theme_text_color,"event_theme_link_color":event_theme_link_color,"event_theme_link_highlighted_color":event_theme_link_highlighted_color,"font_package":font_package,"is_theme_used":is_theme_used,"IMAGE":IMAGE,"theme_name":theme_name};

//call ajax popup function
ajaxpopupopen('form_customize_color_theme_popup_zoom','event/add_edit_custom_theme_popup',sendData,'theme_popup_zoom','');

});

ajaxdatasave('formAddEventTheme1', 'event/customizeforms',true,true,false,true,true, '#showhideformdiv1', '#showhideformdiv1',false);

$('.theme').change(function() {   
   $this = $(this);
   var theme_val = $this.val();
   var attr_val = $this.attr('themeId');
   var cssAction = $this.attr('cssAction');

   if(cssAction=='screen_bg'){
       $('.theme_box'+attr_val).css('background', "#"+theme_val);
       $('.theme_head'+attr_val).css('background', "#"+theme_val);
   }
   if(cssAction=='header_bg'){
        $('.themeBannerInfo'+attr_val).css('background', "#"+theme_val);
        $('.eventDescription'+attr_val).css('background', "#"+theme_val);
   }
   if(cssAction=='box_bg'){           
        $('.bx-bg-color'+attr_val).css('background', "#"+theme_val);
   }
   if(cssAction=='link_color'){
       $('.as_small_nav'+attr_val).css('color', "#"+theme_val);
       $('.bx-txt-clor'+attr_val).css('color', "#"+theme_val);
   }
   if(cssAction=='link_highlighted_color'){
       $('.eventTitle'+attr_val).css('color', "#"+theme_val);
       $('.hltd-colr'+attr_val).css('color', "#"+theme_val);
       $('.hltd-colr-box'+attr_val).css('background', "#"+theme_val);
   }
  
   if(cssAction=='text_color') {
        $('.eventSubTitle'+attr_val).css('color', "#"+theme_val);
        $('.eventlocation'+attr_val).css('color', "#"+theme_val);            
   }        
    if(cssAction=='font_family'){
        var fonts = $(".font_package"+attr_val+" option:selected").text();
        $('.eventSubTitle'+attr_val).css("font-family", fonts);
        $('.eventlocation'+attr_val).css('font-family',fonts);
        $('.bx-txt-clor'+attr_val).css('font-family',fonts);
        $('.as_small_nav'+attr_val).css('font-family',fonts);
        $('.eventTitle'+attr_val).css('font-family',fonts);
        $('.hltd-colr'+attr_val).css('font-family',fonts);
    }

});
	
$('.reset_theme_data').on('click', function(){
    var attr_val = $(this).attr('themeid');
    var screen_bg = $('#theme_data'+attr_val).attr('screen_bg');
    var primary_clr = $('#theme_data'+attr_val).attr('primary_clr');
    var secondary_clr = $('#theme_data'+attr_val).attr('secondary_clr');
    var text_clr = $('#theme_data'+attr_val).attr('text_clr');
    var link_clr = $('#theme_data'+attr_val).attr('link_clr');
    var heighlight_clr = $('#theme_data'+attr_val).attr('heighlight_clr');
    var fonts = $('#theme_data'+attr_val).attr('font_fmly');
    
    $('.theme_box'+attr_val).css('background', "#"+screen_bg);
    $('.theme_head'+attr_val).css('background', "#"+screen_bg);
    $('input.event_theme_screen_bg'+attr_val).val(screen_bg);
    $('span#event_theme_screen_bg'+attr_val).css('background', "#"+screen_bg);
    
    $('.themeBannerInfo'+attr_val).css('background', "#"+primary_clr);
    $('.eventDescription'+attr_val).css('background', "#"+primary_clr);
    $('input.event_theme_header_bg'+attr_val).val(primary_clr);
    $('span#event_theme_header_bg'+attr_val).css('background', "#"+primary_clr);
    
    $('.bx-bg-color'+attr_val).css('background', "#"+secondary_clr);
    $('input.event_theme_box_bg'+attr_val).val(secondary_clr);
    $('span#event_theme_box_bg'+attr_val).css('background', "#"+secondary_clr);
    
    $('.as_small_nav'+attr_val).css('color', "#"+link_clr);
    $('.bx-txt-clor'+attr_val).css('color', "#"+link_clr);
    $('input.event_theme_link_color'+attr_val).val(link_clr);
    $('span#event_theme_link_color'+attr_val).css('background', "#"+link_clr);
    
    $('.eventTitle'+attr_val).css('color', "#"+heighlight_clr);
    $('.hltd-colr'+attr_val).css('color', "#"+heighlight_clr);
    $('.hltd-colr-box'+attr_val).css('background', "#"+heighlight_clr);
    $('input.event_theme_link_highlighted_color'+attr_val).val(heighlight_clr);
    $('span#event_theme_link_highlighted_color'+attr_val).css('background', "#"+heighlight_clr);
    
    $('.eventSubTitle'+attr_val).css('color', "#"+text_clr);
    $('.eventlocation'+attr_val).css('color', "#"+text_clr); 
    $('input.event_theme_text_color'+attr_val).val(text_clr);
    $('span#event_theme_text_color'+attr_val).css('background', "#"+text_clr);
    
    $('.eventSubTitle'+attr_val).css("font-family", fonts);
    $('.eventlocation'+attr_val).css('font-family',fonts);
    $('.bx-txt-clor'+attr_val).css('font-family',fonts);
    $('.as_small_nav'+attr_val).css('font-family',fonts);
    $('.eventTitle'+attr_val).css('font-family',fonts);
    $('.hltd-colr'+attr_val).css('font-family',fonts);
    $('select#font_package'+attr_val).parent('.select-wrapper').children('.holder').text(fonts);
});

});

function loadSelectedTheme(themeId) {
    window.location.href=baseUrl+"event/customizeforms/"+themeId;
}


$(document).on('focusout','.trim_check', function(){
	$(this).val( $.trim($(this).val()));
});
