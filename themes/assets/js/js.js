﻿$(document).ready(function(e) {
  
  // initialize custom dropdown
  ini_custom_dropdown('custom-select');
  
  // initialize tooltips
  ini_tooltips();
    
});
//for custom-select custom class

$(".custom-select.as-custom-select").parent(".select-wrapper").addClass("custom_select_wrapper_as");
$(".custom_select_wrapper_as").parent().addClass("div_box_as");

function ini_custom_dropdown(className){
  
  $("."+className).each(function(){
    $(this).wrap("<span class='select-wrapper'></span>");
    //$(this).after("<span class='holder'></span>");
  });
  
  $("."+className).each(function(){
    var selectedOption = $(this).find(":selected").text();
    $(this).parent(".select-wrapper").find(".holder").text(selectedOption);
  });
  
  
  $("."+className).change(function(){
    var selectedOption = $(this).find(":selected").text();
    $(this).parent(".select-wrapper").find(".holder").text(selectedOption);
  }).trigger('click');
  
  
  return true;  
}

//for custom tooltip
function ini_tooltips(){
  
  $(".info_btn").hover(function(){
    var topposition = $(this).offset().top,
    leftposition = $(this).offset().left;
    $("#tooltip_wrapper").html( $(this).html() );
    $("#tooltip_wrapper").css({"left":leftposition + 20 ,"top":topposition + 18 });
  },function(){
    $("#tooltip_wrapper").html(" ");
    $("#tooltip_wrapper").css({"left":0,"top":0});
  })
}
//for custom tooltip ends

$(document).ready(function(e) {

// initialize stepper 
//$("input[type='number']").stepper({customClass:"stepperCustomClass"});  
$("input[type='number']").not(".presenter_class").stepper();  
$(".presenter_class").stepper({customClass:"stepperCustomClass"});  
  
//for svg support 
  var svgSupport = (window.SVGSVGElement) ? true : false;
  if(svgSupport){}
  else{ 
    $(".logo_wrapper img").attr("src","images/logo.png");
    $(".centerlogocontainer img").attr("src","images/homecenterlogo.png");
  }
//for svg support 

//for pop up height
if($(".modal").height() >480){
  $(".modal").css('position','absolute');
}
//for pop up height 

//for accordion
$(".panel-heading a").click(function(){
  var parentname = $(this).attr("data-parent");
  $(this).parents(parentname).find(".panel").each(function(){
    if($(this).find('.collapse').hasClass('in')){
      //true while click for close
      $(this).find('.panel-heading').removeClass('opened');
    }
  });
  
  if( $(this).parents('.panel-heading').siblings('.collapse').hasClass('in') ){
    //true while click for close
    $(this).parents('.panel-heading').removeClass('opened');    
    $(this).parents(".accordion_content").height( "auto" );
    
    //substract clicked element height
    /*if( $(this).parents("div").hasClass("sub-panel-group") ){
      callbackfun = window.setTimeout(function(){
        var thispanelheight = $(this).parents(".sub_panel").find(".accordion_content").height();
        $(this).parents(".accordion_content").height( ($(this).parents(".accordion_content").height() - thispanelheight) );
        window.clearTimeout(callbackfun);
      },1000);
    }*/
  }else{
    //false while click for open
    $(this).parents('.panel-heading').addClass('opened');
    /*var thiselement = $(this);
    window.setTimeout(function(){
      thiselement.parents('.panel-heading').siblings(".accordion_content").height('auto');
    },500);*/
    
    //add clicked element height
    /*if( $(this).parents("div").hasClass("sub-panel-group") ){
      callbackfun = window.setTimeout(function(){
        var thispanelheight = $(this).parents(".sub_panel").find(".accordion_content").height();
        $(this).parents(".accordion_content").height( ($(this).parents(".accordion_content").height() + thispanelheight) );
        window.clearTimeout(callbackfun);
      },1000);
    }*/
  }

  setTimeout(function (){
    $(".getnme_logo").height($("#page_content").height() );
  }, 300); 

});
//for accordion
//for event image div height
$(".events_img").height($("#page_content").height());
//for event image div height


$(".change_input_class").click(function() {
       
       var a = $(this).parent().find('.contact_input').val();
       
       var selectVal  = $(this).find('.selected').attr('data-select');
       var selectdataid = $(this).find('.selected').attr('data-id');
       var notSelectVal = $(this).find('.not-selected').attr('data-select');
    
       if(selectVal==="mobile3" || selectVal==="mobile2" || selectVal==="mobile1" || selectVal==="mobile"+selectdataid){
      $(this).parent().find('.contact_input').attr("placeholder", "123456789");
      $(this).parent().find('.contact_input').val($("#input_hidden_"+selectVal).val());
      $("#input_hidden_"+notSelectVal).val(a);
      
     }else if(selectVal==="landline3" || selectVal==="landline2" || selectVal==="landline1" || selectVal==="landline"+selectdataid){
      $(this).parent().find('.contact_input').attr("placeholder", "01-123456789");
      $(this).parent().find('.contact_input').val($("#input_hidden_"+selectVal).val());
      $("#input_hidden_"+notSelectVal).val(a);
      
     }
       
       
       // "mobile3" === $(this).siblings().attr("id") ? ($(this).parent().find('contact_input').attr("placeholder", "123456789"), $("#input_landline_"+selectVal).val(a),$(this).parent().find('contact_input').val($("#input_mobile_"+selectVal).val())) : "landline3" === $(this).siblings().attr("id") && ($(".contact_input").attr("placeholder", "01-123456789"), $("#input_mobile_"+selectVal).val(a), $(".contact_input").val($("#input_landline").val()))
});



//for phone field selector
$(".phone_field_selector").click(function(e) {
  
  e.preventDefault();
  $(this).attr("tab-index");
  if( $(this).hasClass("open_field") ){
    $(this).removeClass("open_field");
      $(this).siblings(".phone_field_opt").css("display","none");
  }
  else{
    $(this).addClass("open_field");
      $(this).siblings(".phone_field_opt").css("display","block");
  }
});

$("ul.phone_field_opt li").click(function(e){
  e.preventDefault();
  $(this).parent("ul.phone_field_opt").css("display","none").siblings(".open_field").removeClass("open_field");
  $(this).parent("ul.phone_field_opt").find(".selected").removeClass("selected").addClass("not-selected");
  $(this).addClass("selected").removeClass('not-selected');
});

//for phone field selector

$(".hassubmenu").click(function(e) {
  if( window.innerWidth <= 1049 ){
    e.preventDefault(); 
  }
  $(this).parents(".collapse.in").css("height",'auto');
});


$(".feature_box >h4").click(function(e) {
  $(this).parent(".feature_box").siblings(".feature_box").children("h4").removeClass("open_heading");
  $(this).parent(".feature_box").siblings(".feature_box").animate({ height: 70 },"fast");
  
  if( $(this).hasClass("open_heading") ){
    $(this).removeClass("open_heading");
    $(this).parent(".feature_box").animate({ height: 70 },"fast");

  }
  else{
    $(this).addClass("open_heading");
      $(this).siblings("p").addClass("open_feature");

    var curHeight =  $(this).parent(".feature_box").height(),
    autoHeight = $(this).parent(".feature_box").css('height', 'auto').height();
    
    $(this).parent(".feature_box").height(curHeight).animate({ height: autoHeight },"fast");
  }
});

//for custom input
  $('.custom-upload input[type=file]').change(function(){
      $(this).next().find('input').val($(this).val());
  });
//for custom input ends
  
});
//for event logo div hieght
$(document).ready(function(e){
  $(".getnme_logo").height($("#page_content").height());

  //for device orientation change
   if (navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPad/i)) {
    var viewportmeta = document.querySelector('meta[name="viewport"]');
    if (viewportmeta) {
       if( window.innerWidth >=629){
        var viewportwidth = window.innerWidth;
        viewportmeta.content = 'width='+viewportwidth+', maximum-scale=1, user-scalable=no,initial-scale=1';
      }
    }
  }
});

//for event logo div hieght
$(document).resize(function(e){
   if (navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPad/i)) {
    var viewportmeta = document.querySelector('meta[name="viewport"]');
    if (viewportmeta) {
       if( window.innerWidth >=629){
          var viewportwidth = window.innerWidth;
          viewportmeta.content = 'width='+viewportwidth+', maximum-scale=1, user-scalable=yes,initial-scale=1';
        }
    }
  }
});

$(window).resize(function(e) {
  //for accordion resize
  $("#dashboard_content .accordion_content").css("height","auto");  
  //for notification icon
  if($(window).width() > 629){
    var $elem = $(".notification");
    var maxSize = Math.max($elem.width(), $elem.height());
    $elem.width(maxSize).height(maxSize).css("line-height",maxSize + "px" );
  }
  else{
    var $elem = $(".notification");
    $elem.width('auto').height('auto').css("line-height","" );
  }
  //for notification icon ends
});


$(document).ready(function(e) {
  //for notification icon
  if($(window).width() > 629){
    var $elem = $(".notification");
    var maxSize = Math.max($elem.width(), $elem.height());
    $elem.width(maxSize).height(maxSize).css("line-height",maxSize + "px" );
  }
  //for notification icon ends

  //for custom tooltip
  $(".info_btn").hover(function(){
    var topposition = $(this).offset().top,
    leftposition = $(this).offset().left;
    $("#tooltip_wrapper").html( $(this).html() );
    $("#tooltip_wrapper").css({"left":leftposition + 20 ,"top":topposition + 18 });
  },function(){
    $("#tooltip_wrapper").html(" ");
    $("#tooltip_wrapper").css({"left":0,"top":0});
  })
  //for custom tooltip ends
});

//for modal on current page offset
$(window).on('show.bs.modal', function() { 
  var scrolled_val = window.pageYOffset;
  if( $(".modal").hasClass("in") ){
  }else{  
    $(".modal").css("top", scrolled_val );
  }
});

//Convert color code to rgb value
function hexToRgb(hex) {
  var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
  result = result ? {
     r: parseInt(result[1], 16),
     g: parseInt(result[2], 16),
     b: parseInt(result[3], 16)
  } : null;
  if(result){
     return result.r+','+result.g+','+result.b;
  } else { return null; }
}

//color pallet theme js
$(document).ready(function(e){
	$(".sub_panel.panelWithThemeEditor .panel-title a").click(function(e){
		window.setTimeout(function(e){
			var parentdiv = $(".sub_panel.panelWithThemeEditor .panel-title a").data("parent");
			
			if(parentdiv){
				
				var childdiv = $(parentdiv).find("div").hasClass('in');
					if(childdiv){
						$(parentdiv).addClass("panelWithThemeEditor");
						$(".pallet_main").addClass("panelWithThemeEditor");
					}else{
						$(parentdiv).addClass("panelWithThemeEditor");
						$(".pallet_main").removeClass("panelWithThemeEditor");
					}
					//console.log(childdiv);
			}
		},500);
	});
	
	$(".pallet_main > .panel-heading a").click(function(e){
		if( $(this).parents(".panel-heading").hasClass("opened") ){//alert("here");
		}else{
			$(this).parents(".pallet_main").removeClass("panelWithThemeEditor");
			$(this).parents(".pallet_main").find(".sub_panel.panelWithThemeEditor").each(function(index, element) {
                if( $(this).children(".theme_panelcontent").hasClass("in") ){
					$(this).children(".theme_panelcontent").removeClass("in");
				}
            });
		}
	});
	

//custom function for scroll form top
$('a.scroll_top').click(function(e){
    e.preventDefault();
    var topOffset = $(this).attr("href");
    //alert($(topOffset).offset().top);
    $('html, body').animate({scrollTop: $(topOffset).offset().top }, 'slow');
});   
 
});
