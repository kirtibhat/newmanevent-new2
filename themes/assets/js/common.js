﻿var  baseUrl, loadersaving, hidearea, showarea, loadersaved, isChanged, loadererror, open_model_popup, hide_popup, doLogin, ajaxdatasave;
$(document).ready(function(e) {


    /**
     * @description: all ready state function come in document ready function
     */
    
    $(document).on('click', '#create_account', function() {
        var is_refrer_url = $('#is_refrer_url').val();
        if(is_refrer_url=='1') {
            window.location = baseUrl + 'home/freeRegistration?q=1';
        }else {
            window.location = baseUrl + 'home/freeRegistration';
        }
    });

    $('form').each(function() {
        var list = $(this).find('*[tabindex]').sort(function(a, b) {
            return a.tabIndex < b.tabIndex ? -1 : 1;
        }),
                first = list.first();
        list.last().on('keydown', function(e) {
            if (e.keyCode === 9) {
                first.focus();
                return false;
            }
        });
    });
    

    $(document).on('click', '.single_btn', function() {
        var id = $("#ajax_open_popup_html").attr('openbox');
        if (id !== "")
        {
            open_model_popup(id, false);
        }
    });

    $(document).on('click', '#apply_enterprise', function() {
        //reset parsley js form
        $('.enterprise_form').parsley().reset();
        //open login popup
        open_model_popup('enterprise_popup');
        //set class and id in attribute
        $("#ajax_open_popup_html").attr('openbox', 'enterprise_popup');

    });

    $(document).on('click', '#apply_solo', function() {
        //reset parsley js form
        $('.solo_form').parsley().reset();
        //open login popup
        open_model_popup('solo_popup');
        //set class and id in attribute
        $("#ajax_open_popup_html").attr('openbox', 'solo_popup');

    });

    $(document).on('click', '#apply_casual', function() {
        //reset parsley js form
        $('.casual_form').parsley().reset();

        //open login popup
        open_model_popup('casual_popup');
        //set class and id in attribute
        $("#ajax_open_popup_html").attr('openbox', 'casual_popup');

    });


    $(document).on('click', '#apply_free', function() {
        //reset parsley js form
        $('.free_form').parsley().reset();

        //open login popup
        open_model_popup('free_popup');
        //set class and id in attribute
        $("#ajax_open_popup_html").attr('openbox', 'free_popup');

    });
    // ajax request initilization for free registration  
    ajaxdatasave('registration', 'home/registration', false, true, true, false, false, false, false, false);
    
    ajaxdatasave('free_registration', 'home/freeRegistration', false, true, true, true, false, false, false, false);
    ajaxdatasave('registration_password', 'home/freePassword', false, true, true, false, false, false, false, false);
    ajaxdatasave('registration_terms', 'home/freeTerms', false, true, true, false, false, false, false, false);
    
    ajaxdatasave('form_changepwd', 'home/reset_password', false, true, true, false, false, false, false, false);

    $(document).on('click', '.trouble_login', function() {
        //reset parsley js form
        $('#formTroubleLogging').parsley().reset();
        //open login popup
        open_model_popup('troublelogin_popup');
        //set class and id in attribute
        $("#ajax_open_popup_html").attr('openbox', 'troublelogin_popup');
        $("#ajax_open_popup_html").attr('clickbox', 'user_login');

    });

    ajaxdatasave('formTroubleLogging', 'home/troublelogging', false, true, true, false, false, false, false, false);

    // to open forgot password box
    $(document).on('click', '.forgot_password', function() {
        //reset parsley js form
        $('#formForgotPassword').parsley().reset();
        //open login popup
        open_model_popup('forgot_popup');
        //set class and id in attribute
        $("#ajax_open_popup_html").attr('openbox', 'forgot_popup');
        $("#ajax_open_popup_html").attr('clickbox', 'user_login');


    });

    ajaxdatasave('formForgotPassword', 'home/forgotpassword', false, true, true, false, false, false, false, false);


    //call user login function in ready state
    //---------------------------User Login------------------------------------------

    $(document).on('click', '.user_login', function() {

        //reset parsley js form
        $('#formLogin').parsley().reset();

        //open login popup
        open_model_popup('login_popup');
        //set class and id in attribute
        $("#ajax_open_popup_html").attr('openbox', 'login_popup');
        $("#ajax_open_popup_html").attr('clickbox', 'user_login');

        //get click type
        var type = $(this).attr('type');
        if (type === undefined) {
            //reset form
            $('#formLogin')[0].reset();
        }
    });

    //call function for uesr login
    doLogin();

    //-----------------------Contact Us--------------------------------------

    $(document).on('click', '.contactUs', function() {
        //reset parsley js form
        $('#formContact').parsley().reset();

        //open login popup
        open_model_popup('contact_popup');

        //set class and id in attribute
        $("#ajax_open_popup_html").attr('openbox', 'contact_popup');
        // $("#ajax_open_popup_html").attr('clickbox','contactUs');	
    });

    //send data by ajax request
    ajaxdatasave('formContact', 'home/contactus', false, true, true, false, false, false, false, false);



    //Contact us page parsley js        
    $("#phonecode + ul > li ").addClass("hide");
    $('#formContact').parsley().subscribe('parsley:form:validate', function(formInstance) {
        if (formInstance.isValid('block1', true)) {
            $('.invalid-form-error-message').html('');
            $('.phonecode_error').removeClass("show").addClass("hide");
            $("#phoneno + ul > li ").removeClass("filled").addClass("hide");
            return;
        }
        // else stop form submission
        formInstance.submitEvent.preventDefault();
        // and display a gentle message
        var phonecode = $("#phonecode").val();
        if (phonecode !== "" && !$.isNumeric(phonecode)) {
            $("#phoneno + ul > li ").removeClass("filled").addClass("hide");
            $('#phoneno').removeClass("parsley-error parsley-success").addClass("parsley-error custom_li");
            $('.phonecode_error').removeClass("hide").addClass('custom_li show').html('Phone code must be numeric');
        }
        if (phonecode !== "" && phonecode.length !== 2) {
            $("#phoneno + ul > li ").removeClass("filled").addClass("hide");
            $('#phoneno').removeClass("parsley-error parsley-success").addClass("parsley-error custom_li");
            $('.phonecode_error').removeClass("hide").addClass('custom_li show').html('Phone code should be 2 characters in length');
        }
        if (phonecode === '') {
            $('.phonecode_error').removeClass("filled").removeClass("show").addClass("hide");
        }
        if ($('#phoneno').val() !== '') {
            if (phonecode === '') {
                $('#phoneno').removeClass("parsley-error parsley-success").addClass("parsley-error custom_li");
                $('.phonecode_error').removeClass("hide").addClass('custom_li show').html('Phone code is required');
                $("#phoneno + ul > li ").removeClass("filled").addClass("hide");
            }
        }
        return;
    });
    //Contact us page parsley js

    //Contact page phonecode and phone input valid
    $("#phonecode").keyup(function() {
        $("#phonecode + ul > li ").addClass("hide");
        if ($(this).val() && (!$.isNumeric($(this).val()) || $(this).val().length !== 2)) {
            $('#phoneno').removeClass("parsley-success").addClass("parsley-error");
            $('#phonecode').removeClass("parsley-success").addClass("parsley-error");
            $('.phonecode_error').removeClass("hide").addClass('custom_li show').html('Phone code must be numeric and 2 characters in length');
        }
        else if ($("#phoneno").val() && !$(this).val()) {
            $('.phonecode_error').removeClass("hide").addClass('custom_li show').html('Phone code must be numeric and 2 characters in length');
            $('#phoneno').removeClass("parsley-success").addClass("parsley-error");
            $('#phonecode').removeClass("parsley-success").addClass("parsley-error");
        }
        else if ($(this).val() && $.isNumeric($(this).val()) && $(this).val().length === 2 && $("#phoneno").val()) {
            $('#phoneno').removeClass("parsley-error").addClass("parsley-success");
            $('#phonecode').removeClass("parsley-error").addClass("parsley-success");
            $('.phonecode_error').addClass("hide");
        }
    });
    $("#phoneno").keyup(function() {
        if (!$("#phoneno").val()) {
            $('#phoneno').removeClass("parsley-success").addClass("parsley-error");
            $('#phonecode').removeClass("parsley-success").addClass("parsley-error");
            $('.phonecode_error').addClass("hide");
        }
        else if ($("#phoneno").val()) {
            if (!$("#phonecode").val() || !$.isNumeric($("#phonecode").val()) || $("#phonecode").val().length !== 2) {
                $('#phoneno').removeClass("parsley-success").addClass("parsley-error");
                $('#phonecode').removeClass("parsley-success").addClass("parsley-error");
                $('.phonecode_error').removeClass("hide").addClass('custom_li show').html('Phone code must be numeric and 2 characters in length');
            }
            else if ($("#phonecode").val() && $("#phonecode").val().length === 2) {
                $('#phoneno').removeClass("parsley-error").addClass("parsley-success");
                $('#phonecode').removeClass("parsley-error").addClass("parsley-success");
            }
            else{
                $('#phoneno').removeClass("parsley-success").addClass("parsley-error");
                $('#phonecode').removeClass("parsley-success").addClass("parsley-error");
                $('.phonecode_error').removeClass("hide").addClass('custom_li show').html('Phone code must be numeric and 2 characters in length');
            }
        }
    });
    //Contact page phonecode and phone input valid


    //Packages selected when redirect from home page & contact page redirect 
    var doc_hash = document.location.hash;
    var select_id = doc_hash.split('#');
    var new_id = select_id[1];
    var div_id = '';

    if (new_id && new_id === 'contact_email') {
        $('.contactUs').click();
    } //redirect to contact page popup

    if (new_id && new_id === 'free') {
        div_id = "collapsefive";
    }
    if (new_id && new_id === 'casual') {
        div_id = "collapsefour";
    }
    if (new_id && new_id === 'solo') {
        div_id = "collapsethree";
    }
    if (new_id && new_id === 'enterprise') {
        div_id = "collapsetwo";
    }

    if (new_id) {
        $('#collapseOne').removeClass('in');
        $(".panel").each(function() {
            $('.panel-heading').removeClass('opened');
        });
        $('#' + div_id).addClass('collapse in');
        $('.' + new_id + '_heading').addClass('opened');
        $(".getnme_logo").height($("#page_content").height());
    }
    //Packages selected when redirect from home page 

    //Reset password success close click event
    $(document).on('click', '.reset_password_close', function() {
        window.location.href = baseUrl;
    });


    //parsley js for input fields
    $('input[type=text], input[type=email], input[type=password], textarea, select').on('focusout', function() {
        $(this).parsley().validate();
    });
    //parsley js for input fields




});



// Define Global Message for ajax request fail
var ajaxErrorMsg = 'Request failed';

//---------------------------------------------------------------------------------------

/*
 * @description: This function is use to custom popup 
 * @param: msg
 * @param: is_success (true,false)
 * 
 */

function custom_popup(msg, is_success) {

    if (is_success === undefined) {
        is_success = true;
    } // success message

    if (typeof(is_success) === "string")
    {    if (is_success === "true") {
            is_success = true;
        } else {
            is_success = false;
        }
    }

    if (is_success) {
        //remove old class form ok button
        var clickbox = $("#ajax_open_popup_html").attr('clickbox');

        if (clickbox !== undefined) {
            $(".cust_popup_button").removeClass(clickbox);
        }
        //set title 
        $('#popup_heading').html('Thanks!');

        //remove error class
        $('#custom_ok_div').hide();

        $("#thanks_popup").find('.modal-header').addClass("manger_color");
        //$("#error_popup").find('.modal-header').removeClass('bg_b5101c');
        $("#thanks_popup").find("input[type='button']").removeClass("single_btn");
    }
    else
    {
        $('#popup_heading').html('Warning');
        //add error class
        $('#custom_ok_div').show();
        $("#thanks_popup").find('.modal-header').addClass("danger_color");
        $("#thanks_popup").find("input[type='button']").addClass("single_btn");
    }

    $('.show_custome_msg').html(msg);
    open_model_popup('thanks_popup');

    //hide custome popup after 3 second
    if (is_success) {
        setTimeout(function() {
            hide_popup('error_popup');
        }, 5000);
    }

}


//---------------------------------------------------------------------------------------

/*
 * @description: This function is use to open model all places by this common code 
 * @param1: modelId
 * return true;
 */

function open_model_popup(modelId, is_form_reset) {

    if (is_form_reset === undefined)
    {
        is_form_reset = true;
    }

    if (is_form_reset)
    {
        $('#' + modelId).find("form").trigger("reset");
    }
    //alert($(window).height());
    //alert($(window).scrollTop());
    $('#' + modelId).modal({backdrop: 'static', keyboard: false});
    //for pop up height
    if ($('#' + modelId).height() > 480) {
        $('#' + modelId).css('position', 'absolute');
        $('#' + modelId).css({'top': $(window).scrollTop() + 'px'});
    }
    return true;
}

//---------------------------------------------------------------------------------------

/*
 * @description: This function is use to hide any popoup
 * @param: id
 * 
 */

function hide_popup(popup_id) {
    $('#' + popup_id).modal('hide');
}


//---------------------------------------------------------------------------------------

/*
 * @description: This function  is use to refreah 
 * @auther: lokendra
 * 
 */

function refreshPgeDelay()
{
    setTimeout(function() {
        window.location.href = window.location.href;
    }, 2000);

}

//------------------------------------------------------------------------------------

/*
 * @description: This function  is use to redirect to page
 * @param: pageUrl
 * @auther: lokendra
 * 
 */

function redirectToPage(pageUrl) {
    setTimeout(function() {
        window.location = pageUrl;
    }, 1000);
}

//---------------------------------------------------------------------------------------

/*
 * @description: This function is used to common show loader message
 * @param1 = loader message
 * @param2 = it will be id and class what you want show 
 */


function showlaoderwithmessag(msgValue) {
    $('.load_succ_txt').slideDown('slow').html(msgValue);
}

//---------------------------------------------------------------------------------------

/*
 * @access: public 
 * @description: this function is used to open loader for ajax 
 * @return: void
 */


function loaderopen() {
    var openbox = $("#ajax_open_popup_html").attr('openbox');
    //hide open model box	
    hide_popup(openbox);

    var loadearHtml = '';
    loadearHtml += '<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="loader" class="modal fade loader_with_txt dn">';
    loadearHtml += '<div class="modal-dialog">';
    loadearHtml += '<div class="">';//modal-content
    loadearHtml += '<div class="modal-body">';
    loadearHtml += '<div class="modelinner">';
    loadearHtml += '<div class="loader_wrapper">';
    loadearHtml += '<img src="' + baseUrl + 'themes/assets/images/loader_img.gif" alt="loader_img" /><p class="loader_msg_text">Please wait for a while..</p>';
    loadearHtml += '<div class="load_succ_txt dn lh_28px"></div>';
    loadearHtml += '</div>';
    loadearHtml += '</div>';
    loadearHtml += '</div>';
    loadearHtml += '</div>';
    loadearHtml += '</div>';
    loadearHtml += '</div>';


    $("#ajax_open_popup_html").html(loadearHtml);
    //$("#ajax_open_popup_html").attr('openbox','loader');
    //open loader
    open_model_popup('loader');
}


//---------------------------------------------------------------------------------------

/*
 * @access: public 
 * @description: this function is used to close loader for ajax 
 * @return: void
 */

function loaderclose() {
    //hide loader
    hide_popup('loader');
}

//---------------------------------------------------------------------------------------

/*
 * @access: public 
 * @description: this function is used to for user login
 * @return: void
 */

function doLogin() {

    $("#formLogin").submit(function(event) {
        event.preventDefault();
        if (login_validate.errorList.length==0) {
            var fromData = $("#formLogin").serialize();
            var url = baseUrl + 'home/login';
            $.ajax({
                type: 'POST',
                data: fromData,
                url: url,
                dataType: 'json',
                cache: false,
                beforeSend: function( ) {
                    //open loader
                    //loaderopen();
                },
                success: function(data) {
                    //check data 
                    if (data) {
                        if (data.is_success === 'true') {
                            loaderopen();
                            
                            
                            rurl = data.redirect_uri;
                            if(rurl==undefined || rurl=='false' || rurl=='') {
                                redirectToPage(baseUrl);
                            }else {
                                redirectToPage(rurl);
                            }
                        } else {
                            //hide loader
                            loaderclose();
                            //if want to show success msg
                            var showMsg = '';
                            if (typeof(data.msg) === 'string') {
                                showMsg = '<p>' + data.msg + '</p>';
                            } else {
                                $.each(data.msg, function(index, val) {
                                    showMsg += '<p>' + val + '</p>';
                                });
                            }
                            $('#error-login_email_error').html(showMsg);
                            return false;
                            //open error message
                            //custom_popup(showMsg, false);

                            //add class in custome popup button
                            var clickbox = $("#ajax_open_popup_html").attr('clickbox');
                            $(".cust_popup_button").addClass(clickbox);
                        }
                    }
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    //hide loader
                    loaderclose();

                    //open error message
                    custom_popup(ajaxErrorMsg, false);

                    //add class in custome popup button
                    var clickbox = $("#ajax_open_popup_html").attr('clickbox');
                    $(".cust_popup_button").addClass(clickbox);
                }
            });

            return false;

        }
    });

}

//---------------------------------------------------------------------------------------

/*
 *--------------------------------------------------------------------------------------- 
 * This function is used to save and Show Success Message, Error Message, Page Refresh 
 * File Upload Trigger Manage, Hide Current Save Form, Open Next form
 *---------------------------------------------------------------------------------------
 * @parm1 = formid  (string)
 * @param2 = posturl  (string)
 * @param3 = isMsgshow (boolean)
 * @param4 = isErrorshow (boolean)
 * @param5 = isrefresh (boolean)
 * @param6 = isredirect (boolean)
 * @param7 = isUpload (string)
 * @param8 = isCurrentForm (string)
 * @param9 = isNextForm (string)
 * @param10 = submitLoader (boolean) 
 */

function ajaxdatasave(formId, postUrl, isMsgshow, isErrorshow, isrefresh, isredirect, isUpload, isCurrentForm, isNextForm, isSubmitLoader) {


    //these value set if not passing these value
    if (isMsgshow === undefined) {
        isMsgshow = true;
    } // success message
    if (isErrorshow === undefined) {
        isErrorshow = true;
    } // all error meesage
    if (isrefresh === undefined) {
        isrefresh = false;
    } // page refresh after save
    if (isredirect === undefined) {
        isredirect = false;
    } // redirect after save
    if (isUpload === undefined) {
        isUpload = false;
    } // upload with data save
    if (isCurrentForm === undefined) {
        isCurrentForm = false;
    } // current form 
    if (isNextForm === undefined) {
        isNextForm = false;
    } // next form
    if (isSubmitLoader === undefined) {
        isSubmitLoader = true;
    }  // submit button loader

    $(document).on('submit', "#" + formId, function(event) {

        if ($(this).find("input[type='submit']").hasClass('button_disabled')) {
            return false;
        }

        var fromData = $(this).serialize();
        var url = baseUrl + postUrl;
        $.ajax({
            type: 'POST',
            data: fromData,
            url: url,
            dataType: 'json',
            async: true,
            cache: false,
            beforeSend: function( ) {
                //open loader
                if (isSubmitLoader === true) {
                    //submit button loader
                    loadersaving(formId);
                } else {
                    //overlay loader
                    loaderopen();
                }
            },
            success: function(data) {
                //check data 
                if (data) {
                    if (data.is_success === 'true') {

                        //if want to show success msg
                        //if(isMsgshow===true){
                        //hide loader

                        if (isSubmitLoader === true) {
                            //submit button loader
                            loadersaved(formId);
                        } else {
                            //overlay loader
                            loaderclose();
                        }
                        //open success message
                        if (formId === "formForgotPassword")
                        {
                            open_model_popup('mail_sent');
                            $("#useremailid").text(data.useremailid);
                        }
                        else if (formId === "form_changepwd")
                        {
                            open_model_popup('passwordreset_popup');
                        }
                        else if (formId === "formTroubleLogging")
                        {
                            open_model_popup('trouble_login_popup');
                        }
                        else if (formId === "registration")
                        {
                            open_model_popup('verify_email');
                            $("#verify_email_popup_content").text(data.content);
                        } else if (formId === "free_registration" || formId === "registration_password" || formId === "registration_terms")
                        { 	
							window.location = baseUrl + data.url;
                        }
                        else
                        {
                            custom_popup(data.msg, true);
                        }
                        //is upload
                        if (isUpload === true) {
                            //trigger fire for media upload	
                            $('#div_ar2RA_a').trigger('click');
                        }

                        //then condition for hide current saved form and open next form
                        if (isCurrentForm !== false || isNextForm !== false) {

                            if (isCurrentForm !== false) {
                                // form open arrow closed
                                hidearea(isCurrentForm); // hide current form open

                                $(isCurrentForm).parent().parent().find('.showhideaction').removeClass('typeofregistH').addClass('typeofregistV');
                                $(isCurrentForm).parent().parent().find('.headingbg').css('border-bottom-left-radius', '22px');//add bottom radius
                            }

                            if (isCurrentForm !== false) {
                                // form closed arrow closed
                                showarea(isNextForm); // show next form	
                                $(isNextForm).parent().parent().find('.showhideaction').removeClass('typeofregistV').addClass('typeofregistH');
                                $(isNextForm).parent().parent().find('.headingbg').css('border-bottom-left-radius', '0px');//remove bottom radius
                            }
                        }
                        //}
                        //if want to refresh
                        if (isrefresh === true) {

                            //show text in loader
                            //showlaoderwithmessag(data.msg);

                            //after show text and page will refresh 2 second delay
                            //refreshPgeDelay();
                        }

                        //if want to redirect to spefice loaction
                        if (isredirect === true) {
                            //show text in loader
                            showlaoderwithmessag(data.msg);

                            var rediectUrl = data.url;
                            setTimeout(function() {
                                window.location = baseUrl + rediectUrl;
                            }, 5000);

                        }

                        //set is changed saved
                        isChanged = false;
                        $('#' + formId).trigger("reset");

                    } else {
                        //if want to show success msg

                        //hide loader
                        if (isSubmitLoader === true) {
                            //submit button loader
                            loadererror(formId);
                        } else {
                            //overlay loader
                            loaderclose();
                        }

                        if (isErrorshow === true) {


                            //if want to show success msg
                            var showMsg = '';
                            if (typeof(data.msg) === 'string') {
                                showMsg = '<p>' + data.msg + '</p>';
                            } else {
                                $.each(data.msg, function(index, val) {
                                    showMsg += '<p>' + val + '</p>';
                                });
                            }
                            //open error message
                            custom_popup(showMsg, false);

                            //add class in custome popup button
                            var clickbox = $("#ajax_open_popup_html").attr('clickbox');
                            $(".cust_popup_button").addClass(clickbox);
                        }
                    }
                }
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                //hide loader
                //loaderclose();
                if (isSubmitLoader === true) {
                    //submit button loader
                    loadererror(formId);
                } else {
                    //overlay loader
                    loaderclose();
                }


                //open error message
                custom_popup(ajaxErrorMsg, false);

                //add class in custome popup button
                var clickbox = $("#ajax_open_popup_html").attr('clickbox');
                $(".cust_popup_button").addClass(clickbox);
            }
        });

        return false;
    });
}




$("#change_input").click(function() {
    var a = $(".contact_input").val();
    "mobile" == $(this).siblings().attr("id") ? ($(".contact_input").attr("placeholder", "123456789"), $("#input_landline").val(a), $(".contact_input").val($("#input_mobile").val())) : "landline" == $(this).siblings().attr("id") && ($(".contact_input").attr("placeholder", "01-123456789"), $("#input_mobile").val(a), $(".contact_input").val($("#input_landline").val()))
});

//check email exist
$('#email').on('keyup',function() {
	var emailObj = $(this);
	var email = emailObj.val();
	var url = baseUrl + 'home/checkEmailExist';
        $.ajax({
            type: 'POST',
            data: {email:email},
            url: url,
            dataType: 'json',
            async: true,
            cache: false,
            success: function(data) {
				if(data.is_success=='false')
				{
					emailObj.parents('#free_registration').children().find("input[type='submit']").attr('disabled','disabled');
					
					//emailObj.addClass('custom_li');
					//emailObj.next('.parsley-errors-list').addClass('filled').append('<li class="parsley-type emailexist">This email is already registered with us.</li>');
					emailObj.next('.parsley-errors-list').addClass('filled').append('<li class="parsley-type emailexist phonecode_error">This email is already registered with us.</li>');
				} else{
					emailObj.parents('#free_registration').children().find("input[type='submit']").removeAttr('disabled');
					
					//emailObj.next('.parsley-errors-list').children('.emailexist').remove();
					emailObj.next('.parsley-errors-list').children('.phonecode_error').remove();
				}
			}
		});
});
