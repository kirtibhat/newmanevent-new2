//*** General events setup page js ***//
$("input[name='isRegisterLastDateYes']").on("change",function(){
    if($(this).val()=='1'){
        $('#registerLastDate').val(reglastDate);
        $('.reg_cutoff_invitation').show();
        $('.addHeight').css('height','auto');
    }else {
        $('.clearVl').val('');
        $('.reg_cutoff_invitation').hide();
    }
});
if(opentab!="") {
    $("document").ready(function() {
        setTimeout(function() {
            if(opentab==1){
                window.location.hash = '#formGeneralEventSetup';
            }else if(opentab==4){
                window.location.hash = '#formContactPerson';
            }
            
        },1000);
    });
}
$(function() {
    setTimeout(function() { $("#testdiv").fadeOut(1500); }, 1000);
    $('.confirm_check_trigger').click(function() {
        $('.parsley-errors-list').show();
        setTimeout(function() { $(".parsley-errors-list").fadeOut(1500); }, 1000);
    });
});

//*** Display map in vanue page **//
function displayMap() {
    document.getElementById(divIdMapCanvas).style.display="block";
    setTimeout(function(e){
        initialize();
    },1000);
}

var placeSearch, autocomplete;
var componentForm = {
  street_number: 'short_name',
  route: 'long_name',
  locality: 'long_name',
  administrative_area_level_1: 'short_name',
  country: 'long_name',
  postal_code: 'short_name'
};
function initialize() {
    $(function () {
            var lat = $('#event_lat').val(),
                lng = $('#event_long').val(),
             latlng = new google.maps.LatLng(lat, lng),
             image = 'http://www.google.com/intl/en_us/mapfiles/ms/micons/blue-dot.png';

         var mapOptions = {
             center: new google.maps.LatLng(lat, lng),
             zoom: 5,
             mapTypeId: google.maps.MapTypeId.ROADMAP,
             panControl: true,
             panControlOptions: {
                 position: google.maps.ControlPosition.TOP_RIGHT
             },
             zoomControl: true,
             zoomControlOptions: {
                 style: google.maps.ZoomControlStyle.LARGE,
                 position: google.maps.ControlPosition.TOP_left
             }
         },
         map = new google.maps.Map(document.getElementById(divIdMapCanvas), mapOptions),
             marker = new google.maps.Marker({
                 position: latlng,
                 map: map,
                 icon: image
             });

         var input = document.getElementById('eventVenueAddress1');
         var autocomplete = new google.maps.places.Autocomplete(input, {
             types: ["geocode"]
         });

         autocomplete.bindTo('bounds', map);
         var infowindow = new google.maps.InfoWindow();

         google.maps.event.addListener(autocomplete, 'place_changed', function (event) {
             //Set blank on chage location
             //$("#eventVenueAddress1").val('');
             $("#eventVenueAddress2").val('');
             $("#eventVenueZip").val('');
             
             infowindow.close();
             var place = autocomplete.getPlace();
             if (place.geometry.viewport) {
                 map.fitBounds(place.geometry.viewport);
             } else {
                 map.setCenter(place.geometry.location);
                 map.setZoom(13);
             }
             
             // Get each component of the address from the place details
              // and fill the corresponding field on the form.
              for (var i = 0; i < place.address_components.length; i++) {
                var addressType = place.address_components[i].types[0];
               // console.log(addressType);
                if (componentForm[addressType]) {
                  var val = place.address_components[i][componentForm[addressType]];
                  console.log(val);
                  if(addressType=='locality'){
                      $('#eventVenueCity').val(val);
                  }
                  if(addressType=='administrative_area_level_1'){
                      $("#eventVenueState").val(val);
                  }
                  if(addressType=='postal_code'){
                      $("#eventVenueZip").val(val);
                  }
                  if(addressType=='route'){
                      //$("#eventVenueAddress1").val(val);
                  }
                  if(addressType=='administrative_area_level_2'){
                      $("#eventVenueAddress2").val(val);
                  }
                }
              }
                         
             moveMarker(place.name, place.geometry.location); 
             $('.MapLat').val(place.geometry.location.lat());
             $('.MapLon').val(place.geometry.location.lng());
             $('#event_lat').val(place.geometry.location.lat());
             $('#event_long').val(place.geometry.location.lng());
             
         });
         google.maps.event.addListener(map, 'click', function (event) {
             $('.MapLat').val(event.latLng.lat());
             $('.MapLon').val(event.latLng.lng());
             infowindow.close();
                     var geocoder = new google.maps.Geocoder();
                     geocoder.geocode({
                         "latLng":event.latLng
                     }, function (results, status) {
                         if (status == google.maps.GeocoderStatus.OK) {
                             var lat = results[0].geometry.location.lat(),
                                 lng = results[0].geometry.location.lng(),
                                 placeName = results[0].address_components[0].long_name,
                                 latlng = new google.maps.LatLng(lat, lng);

                             moveMarker(placeName, latlng);
                             $("#eventVenueAddress1").val(results[0].formatted_address);
                             
                              for (var i = 0; i < results[0].address_components.length; i++) {
                                var addressType = results[0].address_components[i].types[0];
                               
                                if (componentForm[addressType]) {
                                  var val = results[0].address_components[i][componentForm[addressType]];
                                  
                                  if(addressType=='locality'){
                                      $('#eventVenueCity').val(val);
                                  }
                                  if(addressType=='administrative_area_level_1'){
                                      $("#eventVenueState").val(val);
                                  }
                                  if(addressType=='postal_code'){
                                      $("#eventVenueZip").val(val);
                                  }
                                  if(addressType=='route'){
                                      //$("#eventVenueAddress1").val(val);
                                  }
                                  if(addressType=='administrative_area_level_2'){
                                      $("#eventVenueAddress2").val(val);
                                  }
                                }
                              }
                             
                         }
                     });
         });
        
         function moveMarker(placeName, latlng) {
             marker.setIcon(image);
             marker.setPosition(latlng);
             infowindow.setContent(placeName);
             //infowindow.open(map, marker);
         } 
     });
}
// End map code //

$('#buttonid').on('click', function () {
    $( "#toggle" ).toggle( "fast" );
});

$('#buttonid2').on('click', function () {
    $( "#toggle2" ).toggle( "fast" );
});
