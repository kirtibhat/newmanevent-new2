﻿$('.show_create_acc_form').click(function(){
    $('.create_acc_form').show();
    $(this).addClass('supbtn');
    $('.gplusbtnevent').removeClass('supbtn');
    $('.fbbtnevent').removeClass('supbtn');
    $('.termsevent').hide();
    $('.next_btn_action').addClass('formsubmit');
    $('.next_btn_action').removeAttr('style');
    $('.next_btn_action').removeClass('disable');
    $('#terms_checkbox').removeAttr('checked');
    $('.next_btn_action').removeAttr('href');
    $('.checked_action').addClass('dn');
    $('.next_btn_action').removeClass('dn');
    //$('.next_btn_action').removeClass('checked_action');
});
$('.fbbtnevent').click(function(){
    var login_fb_url   = $('#get_login_hit_url').attr('lang');
    $('.next_btn_action').attr('href',login_fb_url);
    $('.termsevent').show();
    $('.create_acc_form').hide();
    $(this).addClass('supbtn');
    $('.gplusbtnevent').removeClass('supbtn');
    $('.show_create_acc_form').removeClass('supbtn');
    $('.next_btn_action').removeClass('formsubmit');
    //$('.next_btn_action').addClass('disable');
    //$('.next_btn_action').addClass('checked_action');
    $('.next_btn_action').css('pointer-events','none');
    $('.next_btn_action').css('cursor','default');
    $('#terms_checkbox').removeAttr('checked');
    $('.checked_action').removeClass('dn');
    $('.next_btn_action').addClass('dn');
    $('#set_checked_status').val(0);
});
$('.gplusbtnevent').click(function(){
    var google_login_url  = $('#get_login_hit_url').data('google_login');
    $('.next_btn_action').attr('href',google_login_url);
    $('.termsevent').show();
    $('.create_acc_form').hide();
    $(this).addClass('supbtn');
    $('.fbbtnevent').removeClass('supbtn');
    $('.show_create_acc_form').removeClass('supbtn');
    $('.next_btn_action').removeClass('formsubmit');
    //$('.next_btn_action').addClass('disable');
    $('.next_btn_action').css('pointer-events','none');
    $('.next_btn_action').css('cursor','default');
    $('#terms_checkbox').removeAttr('checked');
    $('.checked_action').removeClass('dn');
    $('.next_btn_action').addClass('dn');
    $('#set_checked_status').val(0);
    //$('.next_btn_action').addClass('checked_action');
});

$('.accept_event').click(function(){ 
    $('.checked_action').addClass('dn');
    $('.next_btn_action').removeClass('dn');
    $('.next_btn_action').removeAttr('style');
    $('.next_btn_action').removeClass('disable');
    $('#set_checked_status').val(1);
    $('#terms_checkbox').attr('checked',true);
    $('.set_error').hide();
});

jQuery(document).ready(function(){
    $(document).on('click','.formsubmit',function(){
		$('#free_registration').submit();
	});
});

$(document).ready(function(){
$('.next_btn_action').css('cursor','default');

$('.checked_action').click(function(){
var set_checked_status =  $('#set_checked_status').val();
if(set_checked_status==0) {
    $('.set_error').show();
    return false;
}
});

$('#terms_checkbox').click(function(){
    if (this.checked) {
        $('#set_checked_status').val(1);
        $('.next_btn_action').removeAttr('style');
        $('.next_btn_action').removeClass('disable');
        $('.checked_action').addClass('dn');
        $('.next_btn_action').removeClass('dn');
        $('.set_error').hide();
        setSession(1);
    }else{
        $('.checked_action').removeClass('dn');
        $('.next_btn_action').addClass('dn');
        $('#set_checked_status').val(0);
        //$('.next_btn_action').addClass('disable');
        $('.next_btn_action').css('pointer-events','none');
        $('.next_btn_action').css('cursor','default');
        setSession(0);
    }
}) 
$('.confirm_email').bind("cut copy paste",function(e) {
    e.preventDefault();
});
    
});
function setSession(status)
{
    $.ajax({
        type:"post",
        dataType:"html",
        url: baseUrl+'home/isLoginOrRegisterSession',
        data: "status="+status,
        success: function(response){
        },
        error: function(){						
            alert('Error while request..'); 
        }
    }); // Ajax End	
}

$('#termsncondition').click(function(){
    if (this.checked) {
        $(".addlink").attr("href", dashboardUrl);
        $('.isConfirmTrue').removeAttr('style');
        $('.isConfirmTrue').removeClass('disable');
    }else{
        $(".addlink").attr("href", "");
        $('.isConfirmTrue').addClass('disable');
        $('.isConfirmTrue').css('pointer-events','none');
        $('.isConfirmTrue').css('cursor','default');
        }
}); 

function setTermsData()
{
        var user_id = userId;
        $.ajax({
            type:"post",
            //dataType:"html",
            url: setLoginOrRegisterCheckUrl,
            data: "user_id="+user_id,
            success: function(response){
            },
            error: function(){						
                //alert('Error while request..'); 
            }
        }); // Ajax End	
}

$('#our_terms').click(function(){
    if (this.checked) {
        $('.our_terms_next_event').removeAttr('style');
        $('.our_terms_next_event').removeClass('disable');
    }else{
        $('.our_terms_next_event').addClass('disable');
        $('.our_terms_next_event').css('pointer-events','none');
        $('.our_terms_next_event').css('cursor','default');
    }
}) 

 $('#formForgotPassword').validate({ 
		rules: {
			forgotEmail: {
				required: true,
				email: true
			},
		},
		messages: {
			forgotEmail: {
				required:"Email address required.",
				checkRegx:"Enter valid email address."
			},
        },
		errorPlacement: function ($error, $element) {
			var name = $element.attr("name");
			$("#error-" + name).append($error);
            $('.parsley-errors-list').remove();
		}
	});	   
    
$(document).ready(function(){
        
        
        $('#loginPassword').keypress(function(event){
            if (event.which == 13) { 
                 event.preventDefault();
                $('#formLogin').submit();
                }
            });
        $('#loginUsername').keypress(function(event){
            if (event.which == 13) { 
                 event.preventDefault();
                $('#formLogin').submit();
                }
            });
            
            	$('#pills-first a').click(function (e) {
	  e.preventDefault()
	  $(this).tab('show')
   });
    $("#hide").click(function(){
            $(".top_content").slideUp("fast");
        });
        $("#show").click(function(){
            $(".top_content").slideDown("fast");
        });
});     

function showHideInfoBar(info_msg_id) {
    $('#'+info_msg_id).toggle();
}

$('.accept_terms').click(function(){ 
    $('#our_terms').trigger('click');
});
