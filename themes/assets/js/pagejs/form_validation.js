﻿$('#free_registration').validate({ 
		rules: {
			registerUsername: {
				required: true
			},
			registerUserSurname: {
				required: true
			},
			email: {
				required: true,
				email: true
			},
			confirm_email: {
				required: true,
				equalTo: "#email",
				email: true
			}
		},
		messages: {
            registerUsername: "First name required.",
            registerUserSurname: "Last name required.",
            email: {
				required:"Email address required.",
				checkRegx:"Enter valid email address."
			},
			confirm_email: {
				required:"Confirm email address required.",
				checkRegx:"Enter valid email address.",
				equalTo: "Please enter the same value again."
			}
            
		},
		errorPlacement: function ($error, $element) {
			var name = $element.attr("name");
			$("#error-" + name).append($error);
		}
});	

$('#registration_password').validate({ 
		rules: {
			password: {
				required: true,
				checkRegx:/^(?=.*[_$@#%&*!%.])(?=.*[^_$@#%&*!%.])[\w$@#&*!%.]{6,25}$/,
				minlength:6
			},
			confirm_password: {
				required: true,
				equalTo: "#password"
			}
		},
		messages: {
            password: {
				minlength:"Minimum password length is 6.",
				required:"Password required.",
				checkRegx: "The password needs to be minimum 6 alphanumeric with special character combination."
			},
			confirm_password: {
				required:"Confirm password required.",
				equalTo: "Please enter the same value again."
			}
            
		},
		errorPlacement: function ($error, $element) {
			var name = $element.attr("name");
			$("#error-" + name).append($error);
		}
	});	

login_validate = $('#formLogin').validate({ 
		rules: {
			loginUsername: {
				required: true,
				email: true
			},
			loginPassword: {
				required: true
			}
		},
		messages: {
			loginUsername: {
				required:"Email address required.",
				checkRegx:"Enter valid email address."
			},
            loginPassword:"Password required."
        },
		errorPlacement: function ($error, $element) {
            $('.parsley-errors-list').remove();
			var name = $element.attr("name");
			$("#error-" + name).append($error);
		}
	});	    
    
$(document).on('click','.user_login',function(){
    login_validate.resetForm();
    $('#loginUsername').removeClass('error');
    $('#loginPassword').removeClass('error');
});
$('.press_cancel').on('click',function(){
    $('#loginUsername').removeClass('error');
    $('#loginPassword').removeClass('error');
    login_validate.resetForm();
});   



$('#formAddEvent').validate({ 
		rules: {
        eventName: {
				required: true
			},    
        eventStartDate: {
            required: true,
            //dateITA: true,
            date:true,
            //dateLessThan: '#eventEndDate'
        },
        eventEndDate: {
            required: true,
            //date:true,
            //dateITA: true,
            //dateGreaterThan: "#eventStartDate"
        }
    },
    
    messages: {
        eventStartDate: {
            required:"Valid date format required.",
        },
        eventEndDate: {
            required:"Valid date format required.",
        },
        eventName: {
            required:"Event name required.",
        }
        
    },
    errorPlacement: function ($error, $element) {
        var name = $element.attr("name");
        $("#error-" + name).append($error);
    }
});	

/* General Event Form Validation */

$('#formGeneralEventSetup').validate({ 
		rules: {
			eventName: {
				required: false
			},
			eventShortFormTitle: {
				required: false
			},
			eventUrl: {
				required: false,
				//url: true,
			},
			eventStartDate: {
				required: false,
			},
			eventEndDate: {
				required: false,
				date:true,
			},
			timeZone: {
				required: false,
			},
			eventMaxRegistrants: {
				required: false,
                number: true
			},
			registerStartDate: {
				required: false,			
			},
		},
		messages: {
            eventName: "Event Name field required.",
            
            eventShortFormTitle: "Sort form name field required.",
            
            eventUrl: "Event Url field required.",
           
            eventStartDate: "Start Date field required.",
            
            eventEndDate: "End Date field required.",
            
            timeZone: "Time zone is required.",
            
            eventMaxRegistrants: { 
                    required: "Limit field required.", 
                    checkRegx: "Please enter a valid number."
                 },
            
            registerStartDate: "Invitation Date field required."
		},
		errorPlacement: function ($error, $element) {
			var name = $element.attr("name");
			$("#error-" + name).append($error);
		}
});

/* Vanue contact Validation */

$('#formVenueContactPerson').validate({ 
		rules: {
			venue_contact_email: {
				//required:true,
				email: true
			},
		},
		
		messages: {
            venue_contact_email: {
				//required:"Email field required.",
				checkRegx:"Enter valid email address."
			},
         
		},
		errorPlacement: function ($error, $element) {
			var name = $element.attr("name");
			$("#error-" + name).append($error);
		}
});

/* Event contact Validation */

$('#formContactPerson').validate({ 
		rules: {
            /*firstName: {
				required: false,
			},
			lastName: {
				required: false,
			},
			contactPhone: {
				required: false,
			},*/
			contactEmail: {
                required: false,
				email: true
			},
			confirmEmail: {
                required: false,
				email: true,
				equalTo: "#email",
				
			},
		},
		
		messages: {
            /*firstName: {
				required: "First name is required",
			},
			lastName: {
				required: "Last name is required",
			},
			contactPhone: {
				required: "Contact number is required",
			},*/
            contactEmail: {
                //required: "Email is required",
				checkRegx:"Enter valid email address.",
			},
            confirmEmail: {
                //required: "Confirm email is required",
				checkRegx:"Enter valid email address.",
				equalTo:"Please enter the same email again.",
			},
         
		},
		errorPlacement: function ($error, $element) {
			var name = $element.attr("name");
			$("#error-" + name).append($error);
		}
});


/* Social Media Validation */

$('#formSocialMediaSetup').validate({ 
		rules: {
			eventWebsite: {
                checkRegx:/^(?!.*:(ftp|http|https):\/\/)?(?:[\w-]+\.)+[a-z\/a-z]{3,6}(.*?)$/,
				//required: true,
				
                
			},
			eventFacebook: {
                checkRegx:/^(?!.*:(ftp|http|https):\/\/)?(?:[\w-]+\.)+[a-z\/a-z]{3,6}(.*?)$/,
				//required: true,
				
			},
			eventInstagram: {
                checkRegx:/^(?!.*:(ftp|http|https):\/\/)?(?:[\w-]+\.)+[a-z\/a-z]{3,6}(.*?)$/,
				//required: true,
				
			},
			eventTwitter: {
                checkRegx:/^(?!.*:(ftp|http|https):\/\/)?(?:[\w-]+\.)+[a-z\/a-z]{3,6}(.*?)$/,
				//required: true,
				
			},
			eventYoutube: {
                checkRegx:/^(?!.*:(ftp|http|https):\/\/)?(?:[\w-]+\.)+[a-z\/a-z]{3,6}(.*?)$/,
				//required: true,
				
			},
			eventPintrest: {
                checkRegx:/^(?!.*:(ftp|http|https):\/\/)?(?:[\w-]+\.)+[a-z\/a-z]{3,6}(.*?)$/,
				//required: true,
				
			},
			
		},
		messages: {
            eventWebsite: {
				checkRegx:"Valid url required."
			},
            eventFacebook: {
				checkRegx:"Valid url required."
			},
            eventInstagram: {
				checkRegx:"Valid url required."
			},
            eventTwitter: {
				checkRegx:"Valid url required."
			},
            eventYoutube: {
				checkRegx:"Valid url required."
			},
            eventPintrest: {
				checkRegx:"Valid url required."
			},
            
		},
		errorPlacement: function ($error, $element) {
			var name = $element.attr("name");
			$("#error-" + name).append($error);
		}
});


$('#form_changepwd').validate({ 
		rules: {
			newpassword: {
				required: true,
				checkRegx:"/^(?=.*[_$@#%&*!%.])(?=.*[^_$@#%&*!%.])[\w$@#&*!%.]{6,25}$/",
				minlength:6
			},
			newpassword2: {
				required: true,
				equalTo: "#newpassword"
			}
		},
		messages: {
            newpassword: {
				minlength:"Minimum password length is 6.",
				required:"Password required.",
				checkRegx: "The password needs to be minimum 6 alphanumeric with special character combination."
			},
			newpassword2: {
				required:"Confirm password required.",
				equalTo: "Please enter the same value again."
			}
            
		},
		errorPlacement: function ($error, $element) {
			var name = $element.attr("name");
			$("#error-" + name).append($error);
		}
	});	
    

$('.formInvitationTypeCheck').validate({ 
		rules: {
			invitationName: {
				required: true,
			},
		},
		
		messages: {
            required : "This is required field",
		},
		errorPlacement: function ($error, $element) {
			var name = $element.attr("name");
			$("#error-" + name).append($error);
		}
});
$('#formAddCustomeField').validate({ 
		rules: {
			fieldName: {
				required: true,
			},
		},
		
		messages: {
            required : "This is required field",
		},
		errorPlacement: function ($error, $element) {
			var name = $element.attr("name");
			$("#error-" + name).append($error);
		}
});


/* It will come on last */
$(document).ready(function(){
    $.validator.addMethod("checkRegx", function(value, element, regexpr) {          
        // fast exit on empty optional
        if (this.optional(element)) {
            return true;
        }
        return regexpr.test(value);
    }, "");
});

$('.large_input').keyup(function(){
    $('.parsley-errors-list').remove();
});

$('.large_input').click(function(){
    $('.parsley-errors-list').remove();
});
$('.large_input').blur(function(){
    $('.parsley-errors-list').remove();
});

 $('#eventStartDate').change(function(){
		if($(this).val()!="") {
			$('#eventStartDate-error').css('display','none');
			$( "#eventStartDate" ).removeClass( "error" );
		} else {
			$('#eventStartDate-error').css('display','inline');
		}
	});
	
  $('#eventEndDate').change(function(){
    if($(this).val()!="") {
        $('#eventEndDate-error').css('display','none');
        $( "#eventEndDate" ).removeClass( "error" ).addClass("valid");
    } else {
        $('#eventEndDate-error').css('display','inline');
    }
});
$('#registerStartDate').change(function(){
    if($(this).val()!="") {
        $('#registerStartDate-error').css('display','none');
        $( "#registerStartDate" ).removeClass( "error" ).addClass("valid");
    } else {
        $('#registerStartDate-error').css('display','inline');
    }
});

$('#newpassword').change(function(){
    if($(this).val()!="") {
        $('#newpassword-error').css('display','none');
        $( "#newpassword" ).removeClass( "error" ).addClass("valid");
    } else {
        $('#newpassword-error').css('display','inline');
    }
});
