﻿$('.cal1').click(function(){
    $('#eventStartDate').focus();
});
$('.cal2').click(function(){
    $('#eventEndDate').focus();
});
$('.cal3').click(function(){
    $('#eventStartDate').focus();
});
$('.cal4').click(function(){
    $('#eventEndDate').focus();
});
$('.cal5').click(function(){
    $('#registerStartDate').focus();
});
$('.cal6').click(function(){
    $('#registerLastDate').focus();
});
$('#eventName').change(function(){
    var event_title = $(this).val();
    $('.event_title_event').html(event_title);
});

/* Publish event if all mandotory field is completed */
/* Confirm details page */

$('.publish_event_check').on('click',function(){
    $('.publish_event_check').addClass('disabled_btn');
    var event_id                            = $('#event_id').val();
    var event_title                         = $('#event_title').val();
    var event_details_mandatory_status      = $('#event_details_mandatory_status').val();
    var event_invitations_mandatory_status  = $('#event_invitations_mandatory_status').val();
    var event_theme_mandatory_status        = $('#event_theme_mandatory_status').val();
    var terms                               = false;
    if($('#confirm_terms_check').prop('checked')) { terms = true; }
    if( (event_details_mandatory_status == '1') && (event_invitations_mandatory_status == '1') && (event_theme_mandatory_status == '1') && (terms == '1') ) {
        $.ajax({
				type:'POST',
				data:'event_id='+event_id+'&event_title='+event_title,
				url: baseUrl+'event/publishEventAction',
				cache: false,
				success: function(data){ 
                    
					if(data=='success'){
                        $('.publish_event_check').css('display','none');
						$('.publishMessage').html('Your event is published');
                        $('.show_invitation').trigger('click');
						
					}else{
                        $('.publish_event_check').removeClass('disabled_btn');
						$('.error_message_show').html('Please fill up all required fields for event.');
                        $('.show_confirm_error').trigger('click');
					}
				}
			});

    }else {		
        $('.publish_event_check').removeClass('disabled_btn');
        $('.error_message_show').html('Please fill up all required fields for event.');
        $('.show_confirm_error').trigger('click');
    }
});

$('.confirm_email').bind("cut copy paste",function(e) {
    e.preventDefault();
});

function showHideInfoBar(info_msg_id) {
    $('#'+info_msg_id).toggle();
}

$("#confirm_terms_check").change( function() {
    var checkedstatus   = $(this).is(":checked");
    var termsstatus     = $('#termscomplete').val();
    if(checkedstatus == true && termsstatus=='0'){
        $('.incompletebtn').hide();
        $('.completebtn').show();
    }else {
        if(termsstatus=='0'){
            $('.incompletebtn').show();
            $('.completebtn').hide();
        }
    }
});

$('.accept_event_terms').click(function(){ 
    $('#confirm_terms_check').attr('checked',true);
    var termsstatus     = $('#termscomplete').val();
    if(termsstatus=='0'){
        $('.incompletebtn').hide();
        $('.completebtn').show();
    }
});

$('.cancelEventAction').on('click',function(){
    var event_id  = $('#event_id').val();
    var event_title  = $('#event_title').val();
    var status = confirm('Are you sure you want to cancel your event?');
    if(status){
        $.ajax({
            type:'POST',
            data:'event_id='+event_id+'&event_title='+event_title,
            url: baseUrl+'event/cancelEventAction',
            cache: false,
            success: function(data){ 
                if(data=='success'){
                    $('.cancelMessage').html('Your event is cancelled and all attendee has been notified!');
                    $('.invite_people').hide();
                    $('.cancelEventAction').hide();
                    $('.cancelledpopup').trigger('click');
                }
            }
        });
    }
    
});
