﻿
$('#invitation_quantity').change(function(){
    var inv_qty     =   $(this).val();
   
    if(inv_qty < 1) {
        $('#invitation_quantity').val(0);
        $('.set_invitations').html('');
        return false;
        
    }
    var selcted_invitation_type = $('#invitation_types').val();
    var element = $('#invitation_types').find('option:selected'); 
    var limitQuantity = parseInt(element.attr("quantity")); 
    var eventsid                = $('#eventsid').val();
    var selcted_quantity        = parseInt($(this).val());
    if(selcted_invitation_type!='') {
        if( (selcted_quantity!=0) && (selcted_quantity <= limitQuantity) ) {
            $.post( baseUrl+"events/getInvitationHtml", { event_id: eventsid, quantity: selcted_quantity, invitationTypeId: selcted_invitation_type } , function( data ) {
                $('.set_invitations').html(data);
            });
        }else {
            if(selcted_quantity<=1) {
                $('#invitation_quantity').val(1);
            }
            if(selcted_quantity >= limitQuantity) {
                $('#invitation_quantity').val(limitQuantity);
            }
            return false;
        }
    }else {
        $('.set_invitations').html('<span id="error-invitation"><label id="invitation-error" class="error" for="">Please select at least one INVITATION TYPE.</label></span>');
    }
});



$('#invitation_types').change(function(){
    $('#invitation_quantity').val(1);
    $('#invitation_quantity').trigger('change');
    //$('#invitation_quantity').val(0);
    //$('.set_invitations').html('');
});


//call input file on change overlay
$(".customIinputFileLogo").change(function(){
   var filename = $(this).val();
   console.log(filename);
   $('#bookerFileValue').val(filename);
   //var mysize = this.files[0].size;
   //var file_status = checkFileSizeOrType(filename,mysize);
   //if(!file_status) {
   // return false;
  // } 
   //readImageURL(this,'logo');
});

$('.isCheckedEvent').click(function(){
   if($(this).attr('lang')=='media') {
        $('.manullayEmail').hide();
        $('.mediaEmail').show();
   }else {
        $('.mediaEmail').hide();
        $('.manullayEmail').show();
   }
});


$('#formContactSetup').validate({ 
		rules: {
        firstName: {
				required: true
         },    
        lastName: {
				required: true
         },    
        contactEmail: {
                required: true,
				email: true
         },    
        contactMessage: {
				required: true
         },    
        
    },
    
    messages: {
        firstName: {
            required:"First name required.",
        },
        lastName: {
            required:"Last name required.",
        },
        contactEmail: {
            required:"Valid email required.",
        },
        contactMessage: {
            required:"Message field required.",
        },
        
    },
    errorPlacement: function ($error, $element) {
        var name = $element.attr("name");
        $("#error-" + name).append($error);
        $(".parsley-errors-list").remove();
    }
});	

$('.inputtext').keypress(function(){
    var inputVal = $(this).val();
    if($(this).hasClass('fname')) {
        $('.fname1').val(inputVal);
    }
    if($(this).hasClass('lname')) {
        $('.lname1').val(inputVal);
    }
    if($(this).hasClass('email')) {
        $('.email1').val(inputVal);
    }
    
});

$('.inputtext').change(function(){
    var inputVal = $(this).val();
    if($(this).hasClass('fname')) {
        $('.fname1').val(inputVal);
    }
    if($(this).hasClass('lname')) {
        $('.lname1').val(inputVal);
    }
    if($(this).hasClass('email')) {
        $('.email1').val(inputVal);
    }
    if($(this).hasClass('title')) {
        $(".title1 option[value="+inputVal+"]").attr('selected','selected');
        $(".title_wrapper_1").html(inputVal);
    }
});


$('#formEmailInvitationSetup').validate({ 
		rules: {
        nameOfSender: {
				required: true
         },    
        subjectLine: {
				required: true
         },    
        
    },
    messages: {
        nameOfSender: {
            required:"This is required field.",
        },
        subjectLine: {
            required:"This is required field.",
        },
        
    },
    errorPlacement: function ($error, $element) {
        var name = $element.attr("name");
        $("#error-" + name).append($error);
        $(".parsley-errors-list").remove();
    }
});	
