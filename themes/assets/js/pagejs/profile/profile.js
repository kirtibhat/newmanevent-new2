jQuery(document).ready(function(){
	
	password_validate = $('#userPassword').validate({ 
		rules: {
			password: {
				required: true
			},
			new_password: {
				required: false
			},
			confirm_new_password: {
				required: false,
				equalTo: "#new_password"
			}
		},
		messages: {
            password: "Current Password field required.",
            confirm_new_password: "Please enter the same value again."
		},
		errorPlacement: function ($error, $element) {
			var name = $element.attr("name");
			$("#error-" + name).append($error);
		}
	});
	
	$('#userNotification').validate({ 
		rules: {
			alert_mobile_no: {
				checkRegx:/^[ +]*[0-9][ +0-9]*$/
			},
			alert_emailid: {
				checkRegx: /^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i
			}
		},
		messages: {
            alert_mobile_no: {
				checkRegx:"Enter valid phone no."
			},
			alert_emailid: {
				checkRegx:"Enter valid email."
			}
		},
		errorPlacement: function ($error, $element) {
			var name = $element.attr("id");
			$("#error-" + name).append($error);
		}
});

	
	$("#show_password").on("click", function(e){
		if($(this).prop('checked')){
			$('#password').attr('type','text');
			$('#new_password').attr('type','text');
			$('#confirm_new_password').attr('type','text');
		}else {
			$('#password').attr('type','password');
			$('#new_password').attr('type','password');
			$('#confirm_new_password').attr('type','password');
		}
	});
	
	
	/*
	* @description: reset space additional details
	*/
	
	
	$(".showHideUserProfile").on("click", function(e){
		$('#userPassword').children().find('.reset_form').trigger('click');
		$('#password').attr('type','password');
		$('#new_password').attr('type','password');
		$('#confirm_new_password').attr('type','password');
		password_validate.resetForm();
	});
	
	$('#userPassword').children().find('.reset_form').on("click", function(e){
		$('#password').attr('type','password');
		$('#new_password').attr('type','password');
		$('#confirm_new_password').attr('type','password');
	});
	
	$(".reset_basic_info").on("click", function(e){
		$('#company_name').val('');
	});
	
	$(".reset_profile_logo").on("click", function(e){
		$('#nick_name').val('');
		$('#user_logo').val('');
		$('#remove_user_logo').val('1');
		$('.upload_logo_preview').hide();
		$('.upload_logo_name').text('');
	});
	
	//delete floor plan
	$('.remove_upload_logo').on('click',function(){
		$('#user_logo').val('');
		$('#remove_user_logo').val('1');
		$('.upload_logo_preview').hide();
		$('.upload_logo_name').text('');
	});
	
	$("#user_logo").on("change",function(){
		
		var imgPath = $(this)[0].value;
		var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
		if (extn == "png" || extn == "jpg" || extn == "jpeg") {
			var checkFile = checkFileSize(this);
				if(checkFile){
			readURL3(this);
		}
		}else{
			$("#user_logo").val('');
			$('.show_error').trigger('click');
			$('.error_message_show').html('Please upload image only.');
		}
	});
	
	$("input[name='email_alert']").on("change",function(){
		if($(this).val()=='yes'){
			$('.alertEmailWrap').show();
			$('#alert_emailid').addClass('required');
			$('#alert_emailid').addClass('trim_check');
		}else {
			$('.alertEmailWrap').hide();
			$('#alert_emailid').val('');
			$('#alert_emailid').removeClass('required');
			$('#alert_emailid').removeClass('trim_check');
		}
	});
	
	$("input[name='sms_alert']").on("change",function(){
		if($(this).val()=='yes'){
			$('.alertSMSWrap').show();
			$('#alert_mobile_no').addClass('required');
			$('#alert_mobile_no').addClass('trim_check');
		}else {
			$('.alertSMSWrap').hide();
			$('#alert_mobile_no').val('');
			$('#alert_mobile_no').removeClass('required');
			$('#alert_mobile_no').removeClass('trim_check');
		}
	});
	
	media_validate = $('#userCustomSocialMedia').validate({ 
		rules: {
			customSocialName: {
				required: true
			},
			customSocialValue: {
				required: true
			}
		},
		messages: {
            customSocialName: "Name field required.",
            customSocialValue: "Value field required."
		},
		errorPlacement: function ($error, $element) {
			var name = $element.attr("name");
			$("#error-" + name).append($error);
		}
	});
	
	
	//add custom social media
		$(document).on("click", ".add_social_link_label", function(e){
			media_validate.resetForm();
			$('.social_link_label').text('Add Custom Social Media Link');
			$('#customSocialName').val('');
			$('#customSocialValue').val('');
			$('#setFormType').val('');
			$('#customSocialMainId').val('');
		});
		
	//add social media fields
		$(document).on("submit", "#userCustomSocialMedia", function(e){
			var customSocialName = $('#customSocialName').val();
			var customSocialValue = $('#customSocialValue').val();
			var customSocialId = $('#customSocialMainId').val();
			var setFormType = $('#setFormType').val();
			if(setFormType){
				var temp_id = setFormType;
				$('#'+temp_id).html('<input type="hidden" name="customSocialId[]" value="'+customSocialId+'"><div class="col-3"><div class="labelDiv"><label class="form-label text-right">'+customSocialName+'</label><input type="hidden" name="customSocialName[]" value="'+customSocialName+'"></div></div><div class="col-5"><input type="text" class="medium_input hasEditButton pull-left" name="customSocialValue[]" value="'+customSocialValue+'"><div class="editButtonGroup"><button type="button" data-toggle="modal" data-target=".addField_popup" class="btn editSocialCustom" customSocialMainId="'+customSocialId+'" customSocialId="'+temp_id+'" linkname="'+customSocialName+'" linkvalue="'+customSocialValue+'"><span class="medium_icon"> <i class="icon-edit"></i> </span></button><button type="button" data-toggle="modal" data-target=".temp_confirm_popup" class="btn red delete_personal_form_field"  customSocialMainId="'+customSocialId+'" customSocialId="'+temp_id+'"><span class="medium_icon"> <i class="icon-delete"></i> </span></button></div></div>');
				$('#addCustomSocialClose').trigger('click');
				$('#customSocialName').val('');
				$('#customSocialValue').val('');
				$('#setFormType').val();
			} else{
				var temp_id = tempId();
				$('.customSocialContainer').append('<div class="row" id="'+temp_id+'"><input type="hidden" name="customSocialId[]" value="'+temp_id+'"><div class="col-3"><div class="labelDiv"><label class="form-label text-right">'+customSocialName+'</label><input type="hidden" name="customSocialName[]" value="'+customSocialName+'"></div></div><div class="col-5"><input type="text" class="medium_input hasEditButton pull-left" name="customSocialValue[]" value="'+customSocialValue+'"><div class="editButtonGroup"><button type="button" data-toggle="modal" data-target=".addField_popup" class="btn editSocialCustom" customSocialMainId="'+temp_id+'" customSocialId="'+temp_id+'" linkname="'+customSocialName+'" linkvalue="'+customSocialValue+'"><span class="medium_icon"> <i class="icon-edit"></i> </span></button><button type="button" data-toggle="modal" data-target=".temp_confirm_popup" class="btn red delete_personal_form_field"  customSocialMainId="" customSocialId="'+temp_id+'"><span class="medium_icon"> <i class="icon-delete"></i> </span></button></div></div></div>');
				$('#addCustomSocialClose').trigger('click');
				$('#customSocialName').val('');
				$('#customSocialValue').val('');
				$('#setFormType').val();
			}
			
			return false;
		});
	
		//edit custom social media
		$(document).on("click", ".editSocialCustom", function(e){
			media_validate.resetForm();
			$('.social_link_label').text('Edit Custom Social Media Link');
			var name = $(this).attr('linkname');
			var value = $(this).attr('linkvalue');
			var customSocialId = $(this).attr('customSocialId');
			var customSocialMainId = $(this).attr('customSocialMainId');
			$('#customSocialName').val(name);
			$('#customSocialValue').val(value);
			$('#setFormType').val(customSocialId);
			$('#customSocialMainId').val(customSocialMainId);
		});
		
		//delete custom social media
		$(document).on("click", ".deleteSocialCustom", function(e){
			var customSocialId = $(this).attr('customSocialId');
			$('.error_message_show').text('Are you sure you want to delete this entry.');
			$('.confirm_temp_delete').attr('customSocialId',customSocialId);
		});
		
		$(document).on("click", ".confirm_temp_delete", function(e){
			var customSocialId = $(this).attr('customSocialId');
			$('#'+customSocialId).remove();
		});
		
		$(document).on("keyup",'input[name="customSocialValue[]"]', function(e){
			var customSocialValue = $(this).val();
			$(this).next('.editButtonGroup').children('.editSocialCustom').attr('linkvalue',customSocialValue);
		});
		
		
});

function readURL3(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();
		reader.onload = function (e) {
			$('.upload_logo_preview').show();
			var str = input.files[0].name;
			if(str.length >20){
				var res = str.substr(0, 20)+'..';
			}else {
				var res = str;
			}
			$('.upload_logo_name').text(res);
		};
		reader.readAsDataURL(input.files[0]);
	}
}
