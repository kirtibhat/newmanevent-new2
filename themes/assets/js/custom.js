$(document).ready(function(e) {
  // initialize tooltips
  ini_tooltips(); 
  ini_tooltipsGroup(); 
  $("body").append('<div id="tooltip_wrapper" class="group"></div>');
  $("body").append('<div id="tooltip_wrapper" class="group"></div>');
  setTimeout(function(){
    $(".custom-select").wrap('<div class="customSelectWrapper"></div>');  
  }, 2000);
  
  //$('input[type="file"]').wrap('<div class="customFileWrapper"></div>'); 
  //$(".customFileWrapper").append('<button class="inputFileBtn">Select or Drop File</button>');
  $('.btn').each(function(){var center = $(this).html();$(this).html('<div class="btn_center">'+center+'</div>');});
  
});

//for accordian panel
$(".panel_header").on('click', function(){		
	$(".panel_header").next(".panel_contentWrapper").slideUp(300);
    $(this).parent(".panel").toggleClass("open").siblings().removeClass('open');
	if ($(".panel").hasClass("open")) {
	  $(".panel.open").children(".panel_contentWrapper:hidden").slideDown(300);	  
	}
	else{
		$(".panel").children(".panel_contentWrapper").slideUp(300);
	}
	setTimeout(function(){
		if (
			$(".panel.open").height() > 400) {
			$(".panel.open").find(".scrollTopTrg").show();
		}
		else{		
			$(".panel.open").find(".scrollTopTrg").hide();
		}		
	},250);
});
$('.panel').each(function(i) {
	$(this).attr('id', 'panel' + i);
});
$(".scrollTopTrg").click(function(){
    $("html, body").animate({      
	   scrollTop: $(this).parents(".panel").offset().top
    }, 500);
});
$(".sabPanel_header").on('click', function(proper){		
	$(".sabPanel_header").next(".sabPanel_contentWrapper:visible").slideUp(300);
	$(".sabPanel").children(".sabPanel_contentWrapper:visible").slideUp(300);
    $(this).parent(".sabPanel").toggleClass("open").siblings().removeClass('open');
	if ($(".sabPanel").hasClass("open")) {
	  $(".sabPanel.open").children(".sabPanel_contentWrapper:hidden").slideDown(300);
	}
	else{
		$(".sabPanel").children(".sabPanel_contentWrapper:visible").slideUp(300);
	}
	setTimeout(function(){
		if (
			$(".sabPanel.open").height() > 400) {
			$(".sabPanel.open").find(".sabPanelScrollTopTrg").show();
		}
		else{		
			$(".sabPanel.open").find(".sabPanelScrollTopTrg").hide();
		}		
		},250);
});

//~ $('.editButtonGroup').click(function (evt) {
    //~ //evt.stopPropagation();
//~ });

$('.sabPanel').each(function(i) {
	$(this).attr('id', 'sabPanel' + i);
});
$(".sabPanelScrollTopTrg").click(function(){
    $("html, body").animate({       
	   scrollTop: $(this).parents(".sabPanel").offset().top
    }, 500);
});
//for accordina panel end

//input type file
$('input[type="file"]').one('click',function(){	
	$(this).parent().after('<div class="inputFileWrapper"></div>');
}); 

//top navigation dropdown menu
$(".userLoginBlock").on('mouseover', function(){
	$(".profileMenu").addClass("menuShow");
});
$(".userLoginBlock").on('mouseout', function(){
	$(".profileMenu").removeClass("menuShow");
});

//for input type number
$(window).load(function(){
    $("input[type=number]").wrap('<div class="numberInputWrapper"></div>'); 
    $("input[type=number]").addClass("form-number");   
    $("input[type=number]").after('<div class="form-number-value"></div>');
	$("input[type=number]").click(function() {
    	var text = $(this).val();   
		$(this).next(".form-number-value").html(text);		
	});	
	var numTextInput = $("input[type=number]")
	var numText = numTextInput.val(); 
	var numWidthInput = $("input[type=number]").width();
	var numWidthx = numWidthInput + 22;
	numTextInput.parents(".numberInputWrapper").css("width",numWidthx + "px");	
	numTextInput.next(".form-number-value").html(numText);
	$("input[type=number]").bind('keyup mouseup', function () {
		var text = $(this).val();   
		$(this).next(".form-number-value").html(text);
	});
});

//for radio button
function customRadiobutton(radiobuttonName){
	var radioButton = $('input[name="'+ radiobuttonName +'"]');
	$(radioButton).each(function(){
		$(this).wrap( "<span class='custom-rating'></span>" );
		if($(this).is(':checked')){				
			$(this).parent().addClass("selected");
		}
	});
	$(radioButton).click(function(){
		radioname = $(this).attr("name");
		$("input.custom-rating[type=radio]").each(function(index, element) {
			if ( $(this).attr("name") == radioname ){
				$(this).parent("span").removeClass("selected");
			}
		});
			$(this).parent("span").addClass("selected")
	});
}
$(document).ready(function (){
	customRadiobutton("rating-input-1-5");
	customRadiobutton("rating-input-1-4");
	customRadiobutton("rating-input-1-3");
	customRadiobutton("rating-input-1-2");
	customRadiobutton("rating");
})
//for custom tooltip
function ini_tooltips(){
  if($(window).width() < 629) {		
		$(".info_btn").click(function(e){
			var topposition = $(this).offset().top,
			leftposition = $(this).offset().left;
			$("#tooltip_wrapper").html( $(this).html() );
			$("#tooltip_wrapper").css({"left":leftposition + 20 ,"top":topposition + 18 });
			e.stopPropagation();
		  });
		  $("body").click(function(e){
			  $("#tooltip_wrapper").html(" ");
			$("#tooltip_wrapper").css({"left":0,"top":0});
			  });			  
	}
	else{
		$(".info_btn").hover(function(){
			var topposition = $(this).offset().top,
			leftposition = $(this).offset().left;
			$("#tooltip_wrapper").html( $(this).html() );
			$("#tooltip_wrapper").css({"left":leftposition + 20 ,"top":topposition + 18 });
		  },function(){
			$("#tooltip_wrapper").html(" ");
			$("#tooltip_wrapper").css({"left":0,"top":0});
		  })
	}
}

function ini_tooltipsGroup(){
  if($(window).width() < 629) {		
		$(".info_btn.group").click(function(e){
			var topposition = $(this).offset().top,
			leftposition = $(this).offset().left;
			$("#tooltip_wrapper.group").html( $(this).html() );
			$("#tooltip_wrapper.group").css({"left":leftposition + 20 ,"top":topposition + 18 });
			e.stopPropagation();
		  });
		  $("body").click(function(e){
			  $("#tooltip_wrapper.group").html(" ");
			$("#tooltip_wrapper.group").css({"left":0,"top":0});
			  });			  
	}
	else{
		$(".info_btn.group").hover(function(){
			var topposition = $(this).offset().top,
			leftposition = $(this).offset().left;
			$("#tooltip_wrapper.group").html( $(this).html() );
			$("#tooltip_wrapper").css({"left":leftposition + 20 ,"top":topposition + 18 });
		  },function(){
			$("#tooltip_wrapper.group").html(" ");
			$("#tooltip_wrapper.group").css({"left":0,"top":0});
		  })
	}
}
//for custom tooltip ends

//for toggle menu
$(".navbar-toggle").click(function(){
	$(this).toggleClass("collapsed");
	if ($(".navbar-toggle").hasClass("collapsed")) {
	  $(".top_nav").slideDown();
	}
	else{
		$(".top_nav").slideUp();
	}
});
function destroyNav() {
	$(".navbar-toggle").removeClass("collapsed");
	if ($(window).width() > 630) {
		$(".top_nav").slideDown();		
	}
	else{		
		$(".top_nav").slideUp();
	}
}
$(window).on("orientationchange", destroyNav);
$(window).on("resize", destroyNav);
//for toggle menu end

//for custom select tag
(function(a){a.fn.extend({customSelect:function(c){if(typeof document.body.style.maxHeight==="undefined"){return this}var e={customClass:"customSelect",mapClass:true,mapStyle:true},c=a.extend(e,c),d=c.customClass,f=function(h,k){var g=h.find(":selected"),j=k.children(":first"),i=g.html()||"&nbsp;";j.html(i);if(g.attr("disabled")){k.addClass(b("DisabledOption"))}else{k.removeClass(b("DisabledOption"))}setTimeout(function(){k.removeClass(b("Open"));a(document).off("mouseup.customSelect")},60)},b=function(g){return d+g};return this.each(function(){var g=a(this),i=a("<span />").addClass(b("Inner")),h=a("<span />");g.after(h.append(i));h.addClass(d);if(c.mapClass){h.addClass(g.attr("class"))}if(c.mapStyle){h.attr("style",g.attr("style"))}g.addClass("hasCustomSelect").on("render.customSelect",function(){f(g,h);g.css("width","");var k=parseInt(g.outerWidth(),10)-(parseInt(h.outerWidth(),10)-parseInt(h.width(),10));h.css({display:"inline-block"});var j=h.outerHeight();if(g.attr("disabled")){h.addClass(b("Disabled"))}else{h.removeClass(b("Disabled"))}i.css({width:k,display:"inline-block"});g.css({"-webkit-appearance":"menulist-button",width:h.outerWidth(),position:"absolute",opacity:0,height:j,fontSize:h.css("font-size")})}).on("change.customSelect",function(){h.addClass(b("Changed"));f(g,h)}).on("keyup.customSelect",function(j){if(!h.hasClass(b("Open"))){g.trigger("blur.customSelect");g.trigger("focus.customSelect")}else{if(j.which==13||j.which==27){f(g,h)}}}).on("mousedown.customSelect",function(){h.removeClass(b("Changed"))}).on("mouseup.customSelect",function(j){if(!h.hasClass(b("Open"))){if(a("."+b("Open")).not(h).length>0&&typeof InstallTrigger!=="undefined"){g.trigger("focus.customSelect")}else{h.addClass(b("Open"));j.stopPropagation();a(document).one("mouseup.customSelect",function(k){if(k.target!=g.get(0)&&a.inArray(k.target,g.find("*").get())<0){g.trigger("blur.customSelect")}else{f(g,h)}})}}}).on("focus.customSelect",function(){h.removeClass(b("Changed")).addClass(b("Focus"))}).on("blur.customSelect",function(){h.removeClass(b("Focus")+" "+b("Open"))}).on("mouseenter.customSelect",function(){h.addClass(b("Hover"))}).on("mouseleave.customSelect",function(){h.removeClass(b("Hover"))}).trigger("render.customSelect")})}})})(jQuery);;
//for custom select tag end


//for select input resize below 630px screen
function selectInputResizer(){
	if ($(window).width() < 629) {		
		$(".xlarge_select, .medium_select, .large_select").css("width","100%");
		$(".xlarge_select, .medium_select, .large_select").parent(".customSelectWrapper").css("width","100%");		
		$(".hasEditButton").parent(".customSelectWrapper").css("max-width","calc(100% - 75px)");
		$(".hasEditButton").css("width","100%");	
		$("input.hasEditButton").css("width","calc(100% - 75px)");	
		$(".customSelectWrapper").find("select.hasEditButton").css("min-width","100%");						
	}
	else{
		$(".xlarge_select, .medium_select, .large_select").parent(".customSelectWrapper").css("width","auto");
		$(".hasEditButton").parent(".customSelectWrapper").css("width","100%");
		$(".modal-body").find(".hasEditButton:hidden").css("width","calc(100% - 75px)");
		$(".inputFileWrapper").find(".fileName span:even").css("width","calc(100% - 27px)");	
	}
}
$(window).on("orientationchange", selectInputResizer);
$(window).on("resize", selectInputResizer)
//for select input resize below 630px screen end
  
$(window).bind("load", function() {
$(".modal .input100").css("width","100%");
	$(".modal .textarea100").css("width","100%");
	$(".modal .select100").css("min-width","100%")
	$(".modal .select100").parents(".customSelectWrapper").css("width","100%");
	$(".modal .numberInput100").parents(".numberInputWrapper").css("width","100%");
	$(".modal .numberInput100").css("min-width","100%");
});

//ie9
function detectIE() {
    var ua = window.navigator.userAgent;

    var msie = ua.indexOf('MSIE ');
    if (msie > 0) {
        // IE 10 or older => return version number
       $("input[type='text']").css('height','28px')
    }
}
$(window).on("load", detectIE)
